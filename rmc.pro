TEMPLATE = lib
CONFIG += warn_on dll
CONFIG += no_rmiconn
CONFIG -= qt
TARGET = rmc
DEPENDPATH += mm core
INCLUDEPATH += include src/mhash/include ../oscore/include .
LANGUAGE = C++
OBJECTS_DIR = .objs/rmclib/release
DESTDIR = lib
VERSION = 1.0

CONFIG(debug, debug|release) {
  LIB_SUFFIX=d
  TARGET = $${TARGET}$${LIB_SUFFIX}
  OBJECTS_DIR = .objs/rmclib/debug
}

# Platform Defines
win32:DEFINES += LIBRARY
win32:DEFINES -= _MBCS UNICODE
win32-msvc2008 | win32-msvc2013 | win32-msvc2015 {
  LIBS += ../oscore/lib/oscore$${LIB_SUFFIX}1.lib ws2_32.lib
} else:win32 {
  LIBS += ../oscore/lib/oscore$${LIB_SUFFIX}10.lib ws2_32.lib
} else: unix {
  LIBS += -L../oscore/lib -loscore$${LIB_SUFFIX}
}
win32-msvc:QMAKE_CFLAGS += /MDd /Zi
win32-msvc:QMAKE_CXXFLAGS += /GX /GR /MDd /Zi
win32-msvc:QMAKE_CXXFLAGS -= /GZ /ZI
win32-msvc2008:QMAKE_CXXFLAGS *= /Zc:wchar_t
win32-msvc2008:QMAKE_CXXFLAGS -= -Zc:wchar_t-
win32-msvc2015:LIBS += legacy_stdio_definitions.lib
win32-g++:DEFINES += STATIC
win32-g++:QMAKE_LFLAGS += -shared
linux-g++:DEFINES += __SMALL_ENDIAN_
linux-icc:QMAKE_LFLAGS += -cxxlib-icc
linux-icc:QMAKE_CXXFLAGS += -cxxlib-icc
linux-icc:QMAKE_CFLAGS += -cxxlib-icc
hpux-acc:DEFINES += _HPUX_SOURCE
irix-cc:QMAKE_CXXFLAGS += -LANG:std
tru64-cxx:DEFINES += __USE_STD_IOSTREAM NO_RTTI
tru64-cxx:QMAKE_CXXFLAGS += -nortti
vxworks-*:CONFIG += no_sm
vxworks-ccppc:CONFIG += no_rtti
vxworks-ccppc:DEFINES += NO_MEMBER_TEMPLATE NO_DEF_ALLOCATOR NO_WCHAR
macx:CONFIG += no_scramnet
linux-g++:QMAKE_LFLAGS += -Wl,-rpath,$(QTDIR)/lib

# Input
HEADERS +=    src/rm/rmc.h\
              include/rmcdata.h\
              include/cache.h \
              include/outbuf.h \
              include/rmglobal.h \
              src/rm/rmsocket.h

SOURCES  +=   src/rm/rmc.cpp\
              src/rmcdata.cpp\
              src/cache.cpp \
              src/outbuf.cpp \
              src/rm/rmsocket.cpp

# OPTIONAL COMPONENTS 

#DISABLE MULTI THREADING IN LIBRARY
no_thread {
DEFINES += NO_THREAD
}

#DISABLE THE USE OF PCRE REGULAR EXPRESSION LIBRARY. CERTAIN FEATURES THAT REQUIRE REGULAR EXPRESSIONS WILL FAIL TO WORK
no_pcre {
DEFINES += NO_PCRE
} else {
SOURCES +=  src/defaults.cpp
HEADERS +=  include/defaults.h
}

#DISABLE THE USE OF RTTI
no_rtti {
DEFINES += NO_RTTI
vxworks-*: QMAKE_CXXFLAGS += -fno-rtti
#vxworks-*: QMAKE_CXXFLAGS += -fno-rtti -fno-exception
}

#DISABLE THE USE OF dout. CALLS WILL BE ROUTED TO cout.
no_dout {
DEFINES += NO_DOUT
}

#DISABLE THE USE OF FILESYSTEM. FILE OPERATIONS WILL FAIL
no_fs:DEFINES += NO_FS

#IF RM3 IS ENABLED THEN TCP PROTOCOL DISABLED
no_rm3 {
DEFINES += NO_RM3
} else {
HEADERS += src/rm/rmoldtcp.h
SOURCES += src/rm/rmoldtcp.cpp
}

#SHARED MEMORY - SHARED MEMORY PROTOCOL
no_sm {
DEFINES += NO_SM
} else {
SOURCES += src/rm/rmsm.cpp \
           src/rm/storage.cpp \
           src/rm/queue.cpp \
           src/rm/varcol.cpp
HEADERS += src/rm/rmsm.h \
           src/rm/queue.h \
           src/rm/varcol.h
SOURCES *= src/rm/rmname.cpp \
           src/rm/varitem.cpp \
           src/rm/smcache.cpp
HEADERS *= src/rm/rmname.h \
           src/rm/varitem.h \
           src/rm/rel_ptr.h \
           src/rm/storage.h \
           src/rm/smcache.h
}

#TCP - TCP PROTOCOL
no_rmtcp {
DEFINES += NO_RMTCP
CONFIG *= no_rm3
} else {
SOURCES += src/rm/rmtcp.cpp \
           src/rm/roamcli.cpp
HEADERS += src/rm/rmtcp.h \
           src/rm/roamcli.h
}

#UDP - UDP PROTOCOL
no_rmudp {
DEFINES += NO_RMUDP
} else {
SOURCES += src/rm/rmudp.cpp
HEADERS += src/rm/rmudp.h
}

#RM DUAL UDP PROTOCOL
no_rmdudp{
DEFINES += NO_RMDUDP
} else {
SOURCES += src/rm/rmdudp.cpp
HEADERS += src/rm/rmdudp.h
}

#SERIAL PROTOCOL
no_rmserial {
DEFINES += NO_RMSERIAL
} else {
SOURCES += src/rm/rmserial.cpp
HEADERS += src/rm/rmserial.h
SOURCES *= src/mhash/src/crc32.c \
           src/mhash/src/stdfns.c
}

#DUAL SERIAL PROTOCOL
no_rmdserial {
DEFINES += NO_RMDSERIAL
} else {
SOURCES += src/rm/rmdserial.cpp
HEADERS += src/rm/rmdserial.h
SOURCES *= src/mhash/src/crc32.c \
           src/mhash/src/stdfns.c
}

#STATIC SHARED MEMORY POOL PROTOCOL
no_ssm {
DEFINES += NO_SSM
} else {
SOURCES += src/rm/rmstaticsm.cpp
SOURCES *= src/rm/rmname.cpp \
           src/rm/varitem.cpp \
           src/rm/smcache.cpp
HEADERS += src/rm/rmstaticsm.h
HEADERS *= src/rm/rmname.h \
           src/rm/varitem.h \
           src/rm/rel_ptr.h \
           src/rm/storage.h \
           src/rm/smcache.h
}

#SCRAMNET PROTOCOL
no_scramnet {
DEFINES += NO_SCRAMNET
} else {
win32:DEFINES *= WIN32
SOURCES += src/rm/rmscram.cpp
SOURCES *= src/rm/rmname.cpp \
           src/rm/varitem.cpp
HEADERS +=  src/rm/rmscram.h
HEADERS *=  src/rm/rmname.h \
            src/rm/varitem.h \
            src/rm/rel_ptr.h \
            src/rm/directory.h \
            src/rm/lockfreequeue.h
 win32: {
  contains(QMAKE_TARGET.arch, x86_64) {
    LIBS += src/gt200/scgtapi_64.lib
  } else {
    LIBS += src/gt200/scgtapi.lib
  }
  QMAKE_LFLAGS += /NODEFAULTLIB:libcmt
 } else:!qnx-*:unix:{
  contains(QMAKE_TARGET.arch, x86_64) | contains(QMAKE_HOST.arch, x86_64) {
    LIBS += -L../rm5/src/gt200/scgt/linux-2.6/lib -lscgtapi_x86_64
  } else {
    LIBS += -L../rm5/src/gt200/scgt/linux-2.6/lib -lscgtapi
  }
 }
}

#SPREAD PROTOCOL
no_spread {
DEFINES += NO_SPREAD
} else {
SOURCES += src/rm/rmspread.cpp \
           src/spread/alarm.c \
           src/spread/arch.c \
           src/spread/events.c \
           src/spread/memory.c \
           src/spread/sp.c
SOURCES *= src/rm/rmname.cpp
HEADERS += src/rm/rmspread.h \
           src/spread/acm.h \
           src/spread/arch.h \
           src/spread/config.h \
           src/spread/defines.h \
           src/spread/errors.h \
           src/spread/mutex.h \
           src/spread/sess_types.h \
           src/spread/sp.h \
           src/spread/spread_params.h \
           src/spread/spu_alarm.h \
           src/spread/spu_alarm_types.h \
           src/spread/spu_data_link.h \
           src/spread/spu_events.h \
           src/spread/spu_memory.h \
           src/spread/spu_objects.h \
           src/spread/spu_objects_local.h \
           src/spread/spu_scatter.h \
           src/spread/spu_system.h \
           src/spread/spu_system_defs.h \
           src/spread/spu_system_defs_autoconf.h \
           src/spread/spu_system_defs_windows.h \
           src/spread/sp_events.h \
           src/spread/sp_func.h
unix:HEADERS += src/spread/config.h \
                src/spread/defines.h
HEADERS *= src/rm/rmname.h
win32:DEFINES += ARCH_PC_WIN95 _REENTRANT DISABLE_FUNCTION_NAME_LOOKUP
}

#RM INTEGRITY CONNECTION PROTOCOL
no_rmiconn {
DEFINES += NO_RMICONN
} else {
SOURCES += src/rm/rmconn.cpp
HEADERS += src/rm/rmconn.h
}

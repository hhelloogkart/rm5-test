#!/bin/sh

version="5.0"
host=`hostname`
if [ "$host" = "" ]; then host=`uname -n`; fi
if [ "$host" = "" ]; then host=$HOST; fi
if [ "$host" = "" ]; then host="unknown"; fi

user=$USER
if [ "$user" = "" ]; then user=$LOGNAME; fi
if [ "$user" = "" ]; then user=`logname`; fi
if [ "$user" = "" ]; then user="unknown"; fi

system=`uname -s`
if [ "$machine" = "" ]; then machine=$MACHINE; fi
if [ "$machine" = "" ]; then machine="unknown"; fi

machine=`uname -m`
if [ "$machine" = "" ]; then machine=$MACHINE; fi
if [ "$machine" = "" ]; then machine=$HOSTTYPE; fi
if [ "$machine" = "" ]; then machine=$hosttype; fi
if [ "$machine" = "" ]; then machine="unknown"; fi

echo "#define COMPILE_VER \"$version\""
echo "#define COMPILE_BY \"$user\""
echo "#define COMPILE_HOST \"$host\""
echo "#define COMPILE_OSNAME \"$system\""
echo "#define COMPILE_HOSTTYPE \"$machine\""
echo "#define COMPILE_DATE \"`date +%D`\""
echo "#define COMPILE_TIME \"`date +%T`\""

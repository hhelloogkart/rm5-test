/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _VLU_VAR_H_
#define _VLU_VAR_H_

#include <mpt_var.h>

/**
 \class     value_var
 \brief     The base class for variables that contain a single basic data type
 \details   This is the base class for basic data types: int, short, char, unsigned int, unsigned
            short, unsigned char, double and float. This class provides a universal interface to
            convert between data of different types. For example, to get a double value, we can
            use to_double() on any subclass and conversion routines will be called if required.
            This means that a double stored as a string in string_var will be interpreted and
            returned in the extreme case; in fact this makes the whole concept of storing data
            similar to interpreted languages such as Perl.

            There are 4 main types of methods, the virtual to_* and from_* functions that are
            used to force the return of a certain type. toValue and fromValue functions with
            overloads for different types so that it can be used in a template, or have the
            compiler determine the correct overload based on the parameter type.
 */
class RM_EXPORT value_var : public mpt_var
{
public:
    /**
     \fn    virtual char value_var::to_char() const = 0;
     \brief Abstract function. Converts this object to a char value. Rounding and clipping the value
            at the boundary will be applied.
     \returns   This object as a char.
     */
    virtual char to_char() const = 0;
    /**
     \fn    virtual unsigned char value_var::to_unsigned_char() const = 0;
     \brief Abstract function. Converts this object to an unsigned char value. Rounding and clipping
            the value at the boundary will be applied.
     \returns   This object as a unsigned char.
     */
    virtual unsigned char to_unsigned_char() const = 0;
    /**
     \fn    virtual short value_var::to_short() const = 0;
     \brief Abstract function. Converts this object to a short. Rounding and clipping the value at the
            boundary will be applied.
     \returns   This object as a short.
     */
    virtual short to_short() const = 0;
    /**
     \fn    virtual unsigned short value_var::to_unsigned_short() const = 0;
     \brief Abstract function. Converts this object to an unsigned short. Rounding and clipping the
            value at the boundary will be applied.
     \returns   This object as a unsigned short.
     */
    virtual unsigned short to_unsigned_short() const = 0;
    /**
     \fn    virtual int value_var::to_int() const = 0;
     \brief Abstract function. Converts this object to an int Rounding and clipping the value at the
            boundary will be applied.
     \returns   This object as an int.
     */
    virtual int to_int() const = 0;
    /**
     \fn    virtual unsigned int value_var::to_unsigned_int() const = 0;
     \brief Abstract function. Converts this object to an unsigned int. Rounding and clipping the
            value at the boundary will be applied.
     \returns   This object as an unsigned int.
     */
    virtual unsigned int to_unsigned_int() const = 0;
    /**
     \fn    virtual long value_var::to_long() const = 0;
     \brief Abstract function. Converts this object to a long. Rounding and clipping the value at the
            boundary will be applied.
     \returns   This object as a long.
     */
    virtual long to_long() const = 0;
    /**
     \fn    virtual unsigned long value_var::to_unsigned_long() const = 0;
     \brief Abstract function. Converts this object to an unsigned long. Rounding and clipping the
            value at the boundary will be applied.
     \returns   This object as a unsigned long.
     */
    virtual unsigned long to_unsigned_long() const = 0;
    /**
     \fn    virtual long long value_var::to_long_long() const = 0;
     \brief Abstract function. Converts this object to a long long. Rounding and clipping the value at the
            boundary will be applied.
     \returns   This object as a long long.
     */
    virtual long long to_long_long() const = 0;
    /**
     \fn    virtual unsigned long long value_var::to_unsigned_long_long() const = 0;
     \brief Abstract function. Converts this object to an unsigned long long. Rounding and clipping the
            value at the boundary will be applied.
     \returns   This object as a unsigned long long.
     */
    virtual unsigned long long to_unsigned_long_long() const = 0;
    /**
     \fn    virtual float value_var::to_float() const = 0;
     \brief Abstract function. Converts this object to a float Clipping the value at the boundary
            will be applied.
     \returns   This object as a float.
     */
    virtual float to_float() const = 0;
    /**
     \fn    virtual double value_var::to_double() const = 0;
     \brief Abstract function. Converts this object to a double Clipping the value at the boundary
            will be applied.
     \returns   This object as a double.
     */
    virtual double to_double() const = 0;
    /**
     \fn    virtual char* value_var::to_char_ptr() = 0;
     \brief Abstract function. Either stringify the value or if it is already a string pass it out.
            It is a bad idea to modify this string unless you are sure the contents is a string,
            otherwise it will not affect the actual value.
     \returns   Null terminated string.
     */
    virtual char* to_char_ptr() = 0;
    /**
     \fn    virtual const char* value_var::to_const_char_ptr() const = 0;
     \brief Abstract function. Either stringify the value or if it is already a string pass it out.
     \returns   Null terminated string.
     */
    virtual const char* to_const_char_ptr() const = 0;
    /**
     \fn    virtual void value_var::from_char(char parameter1) = 0;
     \brief Abstract function. Initializes this object from the given char, clipping the value
            at the boundaries if required, or stringify if this object is a string.
     \param     value  The value to use for initialization.
     */
    virtual void from_char(char) = 0;
    /**
     \fn    virtual void value_var::from_unsigned_char(unsigned char) = 0;
     \brief Abstract function. Initializes this object from the given unsigned char, clipping
            the value at the boundaries if required, or stringify if this object is a string.
     \param     value  The value to use for initialization.
     */
    virtual void from_unsigned_char(unsigned char) = 0;
    /**
     \fn    virtual void value_var::from_short(short parameter1) = 0;
     \brief Abstract function. Initializes this object from the given short, clipping the value at
            the boundaries if required, or stringify if this object is a string.
     \param     value  The value to use for initialization.
     */
    virtual void from_short(short) = 0;
    /**
     \fn    virtual void value_var::from_unsigned_short(unsigned short) = 0;
     \brief Abstract function. Initializes this object from the given unsigned short, clipping the
            value at the boundaries if required, or stringify if this object is a string.
     \param     value  The value to use for initialization.
     */
    virtual void from_unsigned_short(unsigned short) = 0;
    /**
     \fn    virtual void value_var::from_int(int parameter1) = 0;
     \brief Abstract function. Initializes this object from the given int, clipping the value at the
            boundaries if required, or stringify if this object is a string.
     \param     value  The value to use for initialization.
     */
    virtual void from_int(int) = 0;
    /**
     \fn    virtual void value_var::from_unsigned_int(unsigned int) = 0;
     \brief Abstract function. Initializes this object from the given unsigned int, clipping the
            value at the boundaries if required, or stringify if this object is a string.
     \param     value  The value to use for initialization.
     */
    virtual void from_unsigned_int(unsigned int) = 0;
    /**
     \fn    virtual void value_var::from_long(long parameter1) = 0;
     \brief Abstract function. Initializes this object from the given long, clipping the value at the
            boundaries if required, or stringify if this object is a string.
     \param     value  The value to use for initialization.
     */
    virtual void from_long(long) = 0;
    /**
     \fn    virtual void value_var::from_unsigned_long(unsigned long) = 0;
     \brief Abstract function. Initializes this object from the given unsigned long, clipping the
            value at the boundaries if required, or stringify if this object is a string.
     \param     value  The value to use for initialization.
     */
    virtual void from_unsigned_long(unsigned long) = 0;
    /**
     \fn    virtual void value_var::from_long_long(long long) = 0;
     \brief Abstract function. Initializes this object from the given long, clipping the value at the
            boundaries if required, or stringify if this object is a string.
     \param     value  The value to use for initialization.
     */
    virtual void from_long_long(long long) = 0;
    /**
     \fn    virtual void value_var::from_unsigned_long_long(unsigned long long) = 0;
     \brief Abstract function. Initializes this object from the given unsigned long, clipping the
            value at the boundaries if required, or stringify if this object is a string.
     \param     value  The value to use for initialization.
     */
    virtual void from_unsigned_long_long(unsigned long long) = 0;
    /**
     \fn    virtual void value_var::from_float(float parameter1) = 0;
     \brief Abstract function. Initializes this object from the given float, rounding and clipping
            the value at the boundaries if required, or stringify if this object is a string.
     \param     value  The value to use for initialization.
     */
    virtual void from_float(float) = 0;
    /**
     \fn    virtual void value_var::from_double(double parameter1) = 0;
     \brief Abstract function. Initializes this object from the given double, rounding and clipping
            the value at the boundaries if required, or stringify if this object is a string.
     \param     value  The value to use for initialization.
     */
    virtual void from_double(double) = 0;
    /**
     \fn    virtual void value_var::from_char_ptr(const char* parameter1) = 0;
     \brief Abstract function. Initializes this object from the given character pointer, parsing the
            string for its value if needed, rounding and clipping the value.
     \param     value  The null terminated string to use for initialization.
     */
    virtual void from_char_ptr(const char*) = 0;
    /**
     \fn    void value_var::toValue(long long& value) const;
     \brief Converts and writes the value into the reference. Conversion includes rounding and
            clipping at the boundary, or parsing into a value.
     \param [out]   value   The reference to write the value into.
     */
    void toValue(long long& value) const;
    /**
     \fn    void value_var::toValue(long& value) const;
     \brief Converts and writes the value into the reference. Conversion includes rounding and
            clipping at the boundary, or parsing into a value.
     \param [out]   value   The reference to write the value into.
     */
    void toValue(long& value) const;
    /**
     \fn    void value_var::toValue(int& value) const;
     \brief Converts and writes the value into the reference. Conversion includes rounding and
            clipping at the boundary, or parsing into a value.
     \param [out]   value   The reference to write the value into.
     */
    void toValue(int& value) const;
    /**
     \fn    void value_var::toValue(short& value) const;
     \brief Converts and writes the value into the reference. Conversion includes rounding and
            clipping at the boundary, or parsing into a value.
     \param [out]   value   The reference to write the value into.
     */
    void toValue(short& value) const;
    /**
     \fn    void value_var::toValue(char& value) const;
     \brief Converts and writes the value into the reference. Conversion includes rounding and
            clipping at the boundary, or parsing into a value.
     \param [out]   value   The reference to write the value into.
     */
    void toValue(char& value) const;
    /**
     \fn    void value_var::toValue(unsigned long long& value) const;
     \brief Converts and writes the value into the reference. Conversion includes rounding and
            clipping at the boundary, or parsing into a value.
     \param [out]   value   The reference to write the value into.
     */
    void toValue(unsigned long long& value) const;
    /**
     \fn    void value_var::toValue(unsigned long& value) const;
     \brief Converts and writes the value into the reference. Conversion includes rounding and
            clipping at the boundary, or parsing into a value.
     \param [out]   value   The reference to write the value into.
     */
    void toValue(unsigned long& value) const;
    /**
     \fn    void value_var::toValue(unsigned int& value) const;
     \brief Converts and writes the value into the reference. Conversion includes rounding and
            clipping at the boundary, or parsing into a value.
     \param [out]   value   The reference to write the value into.
     */
    void toValue(unsigned int& value) const;
    /**
     \fn    void value_var::toValue(unsigned short& value) const;
     \brief Converts and writes the value into the reference. Conversion includes rounding and
            clipping at the boundary, or parsing into a value.
     \param [out]   value   The reference to write the value into.
     */
    void toValue(unsigned short& value) const;
    /**
     \fn    void value_var::toValue(unsigned char& value) const;
     \brief Converts and writes the value into the reference. Conversion includes rounding and
            clipping at the boundary, or parsing into a value.
     \param [out]   value   The reference to write the value into.
     */
    void toValue(unsigned char& value) const;
    /**
     \fn    void value_var::toValue(float& value) const;
     \brief Converts and writes the value into the reference. Conversion includes clipping at the
            boundary, or parsing into a value.
     \param [out]   value   The reference to write the value into.
     */
    void toValue(float& value) const;
    /**
     \fn    void value_var::toValue(double& value) const;
     \brief Converts and writes the value into the reference. Conversion includes clipping at the
            boundary, or parsing into a value.
     \param [out]   value   The reference to write the value into.
     */
    void toValue(double& value) const;
    /**
     \fn    void value_var::fromValue(long long value);
     \brief Initializes this object from the given value. Conversion includes rounding and clipping
            at the boundary, or parsing into a value.
     \param     value   The value to convert and copy from.
     */
    void fromValue(long long value);
    /**
     \fn    void value_var::fromValue(long value);
     \brief Initializes this object from the given value. Conversion includes rounding and clipping
            at the boundary, or stringification.
     \param     value   The value to convert and copy from.
     */
    void fromValue(long value);
    /**
     \fn    void value_var::fromValue(int value);
     \brief Initializes this object from the given value. Conversion includes rounding and clipping
            at the boundary, or stringification.
     \param     value   The value to convert and copy from.
     */
    void fromValue(int value);
    /**
     \fn    void value_var::fromValue(short value);
     \brief Initializes this object from the given value. Conversion includes rounding and clipping
            at the boundary, or stringification.
     \param     value   The value to convert and copy from.
     */
    void fromValue(short value);
    /**
     \fn    void value_var::fromValue(char value);
     \brief Initializes this object from the given value. Conversion includes rounding and clipping
            at the boundary, or stringification.
     \param     value   The value to convert and copy from.
     */
    void fromValue(char value);
    /**
     \fn    void value_var::fromValue(unsigned long long value);
     \brief Initializes this object from the given value. Conversion includes rounding and clipping
            at the boundary, or stringification.
     \param     value   The value to convert and copy from.
     */
    void fromValue(unsigned long long value);
    /**
     \fn    void value_var::fromValue(unsigned long value);
     \brief Initializes this object from the given value. Conversion includes rounding and clipping
            at the boundary, or stringification.
     \param     value   The value to convert and copy from.
     */
    void fromValue(unsigned long value);
    /**
     \fn    void value_var::fromValue(unsigned int value);
     \brief Initializes this object from the given value. Conversion includes rounding and clipping
            at the boundary, or stringification.
     \param     value   The value to convert and copy from.
     */
    void fromValue(unsigned int value);
    /**
     \fn    void value_var::fromValue(unsigned short value);
     \brief Initializes this object from the given value. Conversion includes rounding and clipping
            at the boundary, or stringification.
     \param     value   The value to convert and copy from.
     */
    void fromValue(unsigned short value);
    /**
     \fn    void value_var::fromValue(unsigned char value);
     \brief Initializes this object from the given value. Conversion includes rounding and clipping
            at the boundary, or stringification.
     \param     value   The value to convert and copy from.
     */
    void fromValue(unsigned char value);
    /**
     \fn    void value_var::fromValue(float value);
     \brief Initializes this object from the given value. Conversion includes rounding and clipping
            at the boundary, or stringification.
     \param     value   The value to convert and copy from.
     */
    void fromValue(float value);
    /**
     \fn    void value_var::fromValue(double value);
     \brief Initializes this object from the given value. Conversion includes clipping at the
            boundary, or stringification.
     \param     value   The value to convert and copy from.
     */
    void fromValue(double value);
    /**
     \fn    void value_var::fromValue(const char* value);
     \brief Initializes this object from the given value. Conversion includes clipping at the
            boundary, or stringification.
     \param     value   The value to convert and copy from.
     */
    void fromValue(const char* value);
    /**
     \fn    static void value_var::str_to_value(const char* str, long& value);
     \brief Parses a string into a value. Rounding and clipping at boundaries will be applied during
            conversion.
     \param         str     The string.
     \param [out]   value   The value.
     */
    static void str_to_value(const char* str, long& value);
    /**
     \fn    static void value_var::str_to_value(const char* str, int& value);
     \brief Parses a string into a value. Rounding and clipping at boundaries will be applied during
            conversion.
     \param         str     The string.
     \param [out]   value   The value.
     */
    static void str_to_value(const char* str, int& value);
    /**
     \fn    static void value_var::str_to_value(const char* str, short& value);
     \brief Parses a string into a value. Rounding and clipping at boundaries will be applied during
            conversion.
     \param         str     The string.
     \param [out]   value   The value.
     */
    static void str_to_value(const char* str, short& value);
    /**
     \fn    static void value_var::str_to_value(const char* str, char& value);
     \brief Parses a string into a value. Rounding and clipping at boundaries will be applied during
            conversion.
     \param         str     The string.
     \param [out]   value   The value.
     */
    static void str_to_value(const char* str, char& value);
    /**
     \fn    static void value_var::str_to_value(const char* str, unsigned long& value);
     \brief Parses a string into a value. Rounding and clipping at boundaries will be applied during
            conversion.
     \param         str     The string.
     \param [out]   value   The value.
     */
    static void str_to_value(const char* str, unsigned long& value);
    /**
     \fn    static void value_var::str_to_value(const char* str, unsigned int& value);
     \brief Parses a string into a value. Rounding and clipping at boundaries will be applied during
            conversion.
     \param         str     The string.
     \param [out]   value   The value.
     */
    static void str_to_value(const char* str, unsigned int& value);
    /**
     \fn    static void value_var::str_to_value(const char* str, unsigned short& value);
     \brief Parses a string into a value. Rounding and clipping at boundaries will be applied during
            conversion.
     \param         str     The string.
     \param [out]   value   The value.
     */
    static void str_to_value(const char* str, unsigned short& value);
    /**
     \fn    static void value_var::str_to_value(const char* str, unsigned char& value);
     \brief Parses a string into a value. Rounding and clipping at boundaries will be applied during
            conversion.
     \param         str     The string.
     \param [out]   value   The value.
     */
    static void str_to_value(const char* str, unsigned char& value);
    /**
     \fn    static void value_var::str_to_value(const char* str, float& value);
     \brief Parses a string into a value. Rounding and clipping at boundaries will be applied during
            conversion.
     \param         str     The string.
     \param [out]   value   The value.
     */
    static void str_to_value(const char* str, float& value);
    /**
     \fn    static void value_var::str_to_value(const char* str, double& value);
     \brief Parses a string into a value. Rounding and clipping at boundaries will be applied during
            conversion.
     \param         str     The string.
     \param [out]   value   The value.
     */
    static void str_to_value(const char* str, double& value);
    /**
     \fn    static void value_var::str_to_value(const char* str, unsigned long long& value);
     \brief Parses a string into a value. Rounding and clipping at boundaries will be applied during
            conversion.
     \param         str     The string.
     \param [out]   value   The value.
     */
    static void str_to_value(const char* str, unsigned long long& value);
    /**
     \fn    static void value_var::str_to_value(const char* str, long long& value);
     \brief Parses a string into a value. Rounding and clipping at boundaries will be applied during
            conversion.
     \param         str     The string.
     \param [out]   value   The value.
     */
    static void str_to_value(const char* str, long long& value);
    /**
     \fn    static char* value_var::value_to_str(long value);
     \brief Parses a string into a value. Rounding and clipping at boundaries will be applied during
            conversion.
     \param [out]   value   The value.
     \returns   An internally managed null terminated string that is shared with other calls.
     */
    static char* value_to_str(long value);
    /**
     \fn    static char* value_var::value_to_str(int value);
     \brief Stringifies a value. The string must not exceed 256 characters. Non thread-safe, non re-
            entrant.
     \param     value   The value.
     \returns   An internally managed null terminated string that is shared with other calls.
     */
    static char* value_to_str(int value);
    /**
     \fn    static char* value_var::value_to_str(short value);
     \brief Stringifies a value. The string must not exceed 256 characters. Non thread-safe, non re-
            entrant.
     \param     value   The value.
     \returns   An internally managed null terminated string that is shared with other calls.
     */
    static char* value_to_str(short value);
    /**
     \fn    static char* value_var::value_to_str(char value);
     \brief Stringifies a value. The string must not exceed 256 characters. Non thread-safe, non re-
            entrant.
     \param     value   The value.
     \returns   An internally managed null terminated string that is shared with other calls.
     */
    static char* value_to_str(char value);
    /**
     \fn    static char* value_var::value_to_str(unsigned long value);
     \brief Stringifies a value. The string must not exceed 256 characters. Non thread-safe, non re-
            entrant.
     \param     value   The value.
     \returns   An internally managed null terminated string that is shared with other calls.
     */
    static char* value_to_str(unsigned long value);
    /**
     \fn    static char* value_var::value_to_str(unsigned int value);
     \brief Stringifies a value. The string must not exceed 256 characters. Non thread-safe, non re-
            entrant.
     \param     value   The value.
     \returns   An internally managed null terminated string that is shared with other calls.
     */
    static char* value_to_str(unsigned int value);
    /**
     \fn    static char* value_var::value_to_str(unsigned short value);
     \brief Stringifies a value. The string must not exceed 256 characters. Non thread-safe, non re-
            entrant.
     \param     value   The value.
     \returns   An internally managed null terminated string that is shared with other calls.
     */
    static char* value_to_str(unsigned short value);
    /**
     \fn    static char* value_var::value_to_str(unsigned char value);
     \brief Stringifies a value. The string must not exceed 256 characters. Non thread-safe, non re-
            entrant.
     \param     value   The value.
     \returns   An internally managed null terminated string that is shared with other calls.
     */
    static char* value_to_str(unsigned char value);
    /**
     \fn    static char* value_var::value_to_str(float value);
     \brief Stringifies a value. The string must not exceed 256 characters. Non thread-safe, non re-
            entrant.
     \param     value   The value.
     \returns   An internally managed null terminated string that is shared with other calls.
     */
    static char* value_to_str(float value);
    /**
     \fn    static char* value_var::value_to_str(double value);
     \brief Stringifies a value. The string must not exceed 256 characters. Non thread-safe, non re-
            entrant.
     \param     value   The value.
     \returns   An internally managed null terminated string that is shared with other calls.
     */
    static char* value_to_str(double value);
    /**
     \fn    static char* value_var::value_to_str(unsigned long long value);
     \brief Stringifies a value. The string must not exceed 256 characters. Non thread-safe, non re-
            entrant.
     \param     value   The value.
     \returns   An internally managed null terminated string that is shared with other calls.
     */
    static char* value_to_str(unsigned long long value);
    /**
     \fn    static char* value_var::value_to_str(long long value);
     \brief Stringifies a value. The string must not exceed 256 characters. Non thread-safe, non re-
            entrant.
     \param     value   The value.
     \returns   An internally managed null terminated string that is shared with other calls.
     */
    static char* value_to_str(long long value);
    /**
     \fn    static void value_var::set_uint_format(const char* ufmt);
     \brief Sets the letter to use for unsigned integers during stringify with sprintf. Default is
            %lu.
     \param     ufmt    The format string.
     */
    static void set_uint_format(const char* ufmt);
    /**
     \fn    static void value_var::set_int_format(const char* ifmt);
     \brief Sets the letter to use for integers during stringify with sprintf. Default is %ld.
     \param     ifmt    The format string.
     */
    static void set_int_format(const char* ifmt);
    /**
     \fn    static void value_var::set_float_format(const char* ffmt);
     \brief Sets the letter to use for floating point numbers during stringify with sprintf. Default
            is %.4g.
     \param     ffmt    The format string.
     */
    static void set_float_format(const char* ffmt);

protected:

    /**
     \fn    static char* value_var::get_char_buffer();
     \brief Gets shared temporary character buffer used for stringify operations.
     \returns   Pointer to a 256 byte buffer.
     */
    static char* get_char_buffer();
    /**
     \fn    static char* value_var::get_uint_format();
     \brief Gets unsigned int format string for stringify operations.
     \returns   Format string.
     */
    static char* get_uint_format();
    /**
     \fn    static char* value_var::get_int_format();
     \brief Gets int format string for stringify operations.
     \returns   Format string.
     */
    static char* get_int_format();
    /**
     \fn    static char* value_var::get_float_format();
     \brief Gets float format string for stringify operations.
     \returns   Format string.
     */
    static char* get_float_format();

};

#endif /* _VLU_VAR_H_ */

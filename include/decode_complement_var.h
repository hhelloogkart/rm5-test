/******************************************************************/
/* Copyright DSO National Laboratories 2016. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _DECODE_COMPLEMENT_VAR_H_
#define _DECODE_COMPLEMENT_VAR_H_

#include <decode_var.h>

//! The variable that represent signed data in non-complement form
/*!
    This decode data to a double but extract and packs to the number
    of bytes specified in the constructor. It is in the non-complement
    form, where the msb is the signed bit with 1 = negative.

    If byte size if negative, the reverse endian order is used
 */
class RM_EXPORT decode_complement_var : public vdecode_var
{
public:
    decode_complement_var(int _byte_size, double _range, double _offset = 0.0, int _scale_offset = 1);
    double operator=(double right);
    const decode_complement_var& operator=(const decode_complement_var& right);
    virtual const mpt_var& operator=(const mpt_var& right);
    virtual mpt_var* getNext();

protected:
    virtual int encodeValueSigned(double);
    virtual double decodeValueSigned(int);
    /* There is no overload for unsigned versions because decode_complement can only be signed,
       for unsigned version, just use decode_var in unsigned mode, because there is no complement */

private:
    explicit decode_complement_var(const decode_complement_var& cpy);

    const unsigned int sign_mask;

};

template<typename C>
class templ_decode_complement_var : public decode_complement_var
{
public:
    templ_decode_complement_var() : decode_complement_var(C::SIZE, C::RANGE, C::OFFSET, C::SCALE_OFFSET)
    {
    }
    const double& operator=(double right)
    {
        return generic_var<double>::operator=(right);
    }
    const templ_decode_complement_var<C>& operator=(const templ_decode_complement_var<C>& right)
    {
        generic_var<double>::operator=(right);
        return *this;
    }
    virtual const mpt_var& operator=(const mpt_var& right)
    {
        return generic_var<double>::operator=(right);
    }
private:
    explicit templ_decode_complement_var(const templ_decode_complement_var<C>&);
};
#define DEFINE_DECODE_COMPLEMENT_VAR(clsnm, byte_size, range, offset, soffset)  \
    class clsnm : public decode_complement_var \
    { \
public: \
        clsnm() : decode_complement_var(byte_size, range, offset, soffset) {} \
        mpt_var* getNext() { return this + 1; } \
    };
#endif /* _DECODE_COMPLEMENT_VAR_H_ */

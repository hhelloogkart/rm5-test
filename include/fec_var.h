/******************************************************************/
/* Copyright DSO National Laboratories 2010. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _FEC_VAR_H_
#define _FEC_VAR_H_

#include <cplx_var.h>

struct fec_var_private;

/// This composite container class provides error correction codes for its child object
class RM_EXPORT fec_var : public complex_var
{
public:
    ///@param bs the size of each block of data
    ///@param c percentage of the input data that this FEC is able to correct (0-100%)
    fec_var(unsigned int bs, unsigned int c);
    fec_var(const fec_var& right);
    virtual ~fec_var();

    virtual int extract(int len, const unsigned char* buf);
    virtual void output(outbuf& strm);
    virtual int size() const;

    virtual const mpt_var& operator=(const mpt_var& right);
    const fec_var& operator=(const fec_var& right);

    unsigned int getRxErrors();

protected:
    fec_var_private* prvt;

};

#define FECCONST(t, bs, c) t(const t& arg) : fec_var(arg) \
{ \
        child_copy_constructor(arg); \
} \
    t() : fec_var(bs,c) \
    { \
        pop(); \
    } \
    const t& operator=(const t& right) \
    { \
        fec_var::operator=(right); \
        return *this; \
    } \
    const mpt_var& operator=(const mpt_var& right) \
    { \
        fec_var::operator=(right); \
        return *this; \
    } \
    virtual mpt_var* getNext() \
    { \
        return this + 1; \
    } \
    RUNTIME_TYPE(t)

#endif /* _FEC_VAR_H_ */

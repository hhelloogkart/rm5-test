#ifndef _BLK_VAR_H_
#define _BLK_VAR_H_

#include <gen_var.h>
#include <str_var.h>
#include <cplx_var.h>

struct trigger_list;
struct data_mapping;
class sender_base;
class receiver_base;
class mpt_baseshow;
class every_beat_show;
class skip_beat_show;
class interval_show;
/*!
\typedef    char*(* block_var_callback)(int& len)
\brief  block_var_callback
    Callbacks is one of the methods supported by block_var to transfer data from one application
    to another. This allows the application to fully manage the data to be transferred in memory
    without additional copying.

    When used by a receiver: Returns an address to an area of memory of at least
        len bytes long for block var to write into. Or null if it is unable to provide any
        memory, where the data will be discarded. Note that the len in the argument does
        not imply the data received will be of that length, as this information is not
        avialable. The length can be from len - block_sz -1 to len. The application should
        have its own arrangement to retreive the actual length if that is important.
    When used by a sender: Returns an address and the len in bytes of the data to
        send out. If either is 0, an error packet will be sent.
 */
typedef char*(* block_var_callback)(int& len);

/*!
\struct block_var_ack_queue
\brief  Internal queue to store acknowlegements of blocks received. This allows multiple
        packet acknowledgement in the scenario where the links have asymmetrical bandwidth.
 */
struct block_var_ack_queue
{
    static const int RING_SIZE = 48;    /*!< Size of the ring */
    static const int ELEMENT_SIZE = sizeof(int);    /*!< Size of the element */

    unsigned int writer;    /*!< The writer index, next location to write to */
    unsigned int reader;    /*!< The reader index, next location to read from if reader != writer */
    char ring_buffer[RING_SIZE];    /*!< The ring buffer[ring size] */
    /*!
    \fn block_var_ack_queue()
    \brief  Default constructor creates an empty queue
     */
    block_var_ack_queue() : writer(0), reader(0)
    {
    }
    /*!
    \fn void push(int ackno);
    \brief  Pushes acknowledgment number onto the end of this queue
    \param  ackno  Block ID that is acknowledged.
     */
    void push(int);
    /*!
    \fn int pop();
    \brief  Removes and returns the ID at the head of the queue
    \returns    Block ID.
     */
    int pop();
    /*!
    \fn bool isFull() const;
    \brief  Query if the queue is full
    \returns    True if full, false if not.
     */
    bool isFull() const;
    /*!
    \fn bool isEmpty() const;
    \brief  Query if the queue is empty
    \returns    True if empty, false if not.
     */
    bool isEmpty() const;
    /*!
    \fn void clear();
    \brief  Clears the queue to its empty state
     */
    void clear();
};

/*!
\class   block_var
\brief   Facilitates the transfer of files or large amounts of data by dividing the data into
         smaller blocks of with a size limit. This limit can be configured at runtime, but
         both end-points need to use the same block size for the protocol to work.
\details This is a class for block transfers. It supports transfer of files, vars, or
         blocks of memory. The sender and receiver can use asymmetric mechanisms, for
         example, you can send a var and receive as a file. It supports push, ie. sender
         active to send the data out, or pull, ie. receiver active to request and
         retreive data. Pull can additionally request directory listings, execute commands
         on the sender and getting the outputs and get the SHA1 of file objects on the
         sender. Transfers can be bi-directional and will proceed independently (efficiently
         as well by sharing space on the block.)

         Block_vars are used to send different data identified by a name (C string). The name
         can correspond to any type of data: file, vars, memory depending on how it is configured.
         There only needs to be one block_var between two end-points, unless the idea is to
         send data in parallel.

         Blocks sent by block_var looks like array_str_var. They can be forwarded through
         intermediataries using array_str_var, for example, to tunnel through other protocols.
         You should only declare block_var on both endpoints. Block_var does not guarantee the
         integrity of the blocks. You need to do so yourself by making it a child of a
         chksum_var or hash_var if the underlying protocol is prone to corruption, eg. RS-232.
         Finally, you need to package everything in a message, eg. msg_var, serial_var, rm_var.
         Both endpoints should declare the same message parameters, and intermediataries as
         well.

         Blocks can be scheduled to go out every outgoing cycle (default), every x cycle, or
         every time interval. Sending out of data requires your application to call mpt_show
         to trigger its internal processing, then outgoing - which is similar to how other vars
         require from the application.

         In summary, this class can fuction as:
             1. sender or receiver
             2. pull or push
             3. file, var, blocks of memory, or special commands
 */
class RM_EXPORT block_var : public complex_var
{
    friend class every_beat_show;
    friend class skip_beat_show;
    friend class interval_show;
    friend class wait_sender;
public:
    RUNTIME_TYPE(block_var)
    /*!
    \fn block_var::block_var();
    \brief  Default constructor, sits there waiting for a push or pull from the application or the other endpoint
     */
    block_var();
    /*!
    \fn block_var::~block_var();
    \brief  Destructor, all unfinished transfers will be aborted
     */
    ~block_var();
    /*!
    \fn virtual int block_var::extract(int buflen, const unsigned char* buf);
    \brief  Overloaded to handle blocks from the other endpoint.
    \param  buflen  The buffer length in bytes.
    \param  buf     The buffer address.
    \returns    The number of bytes remaining unprocessed.
     */
    virtual int extract(int buflen, const unsigned char* buf);
    /*!
    \fn virtual void block_var::output(outbuf& strm);
    \brief  Overloaded to output blocks
    \param [in,out] strm    The byte string to pack the output blocks.
     */
    virtual void output(outbuf& strm);
    /*!
    \fn virtual int block_var::size() const;
    \brief  Overloaded to return the size in bytes of the next output block. This can be quite
            small if it is just the acknowledgement packet.
    \returns    Size of output block.
     */
    virtual int size() const;
    /*!
    \fn virtual void block_var::setRMDirty();
    \brief  Overloaded to disable the default behaviour because RMDirty should not be set by the
            application. It does nothing. Do not call.
     */
    virtual void setRMDirty();
    /*!
    \fn bool block_var::isValid() const;
    \brief  Overloaded because this is a complex_var and the children is always valid
    \returns    True if it is in the middle of a transfer, either sending or receiving.
     */
    /*reimp*/ bool isValid() const;
    /*!
    \fn bool block_var::setInvalid();
    \brief  Stops all transfers in progress and resets. This makes isValid return true.
    \returns    True if it succeeds in stopping a transfer, false if there were no transfers in progress.
     */
    /*reimp*/ bool setInvalid();
    /*!
    \fn void block_var::pause_sending(bool parameter1);
    \brief  Pause/resume sending packets. Used if the data channel (handled outside block_var) is
            overloaded.
    \param  mode  True to pause, false the resume.
     */
    void pause_sending(bool);
    /*!
    \fn void block_var::set_block_size(int sz);
    \brief  Sets block size. This must be set before transfers are started and both endpoints must have
            the same value, otherwise the behaviour is undefined. Endpoints should use methods outside
            of block_var to sync block sizes.
    \param  sz  Block size in bytes.
     */
    void set_block_size(int);
    /*!
    \fn void block_var::regulate_sending_by_count(int cnt);
    \brief  Regulate sending by count. Will countdown from this value, when it reaches zero a packet
            will be output, if there is pending data, and the count will reset back to this value.
            Hence, for example if cnt is 1, data will be sent out every alternate outgoing.
    \param  parameter1  The first parameter.
     */
    void regulate_sending_by_count(int);
    /*!
    \fn void block_var::regulate_sending_by_time(unsigned int, unsigned int);
    \brief  Regulate sending by time. Will send out packets at this interval if there is pending data.
    \param  sec The second component of the interval
    \param  usec The microsec component of the interval
     */
    void regulate_sending_by_time(unsigned int, unsigned int);
    /*!
    \fn void block_var::set_block_unmapped_sends(bool parameter1);
    \brief  Prevents sending of data that have not been mapped using push_data_on_pull()
    \param  parameter1 True to block, else release the block.
     */
    void set_block_unmapped_sends(bool);
    /*!
    \fn void block_var::set_block_unmapped_recvs(bool parameter1);
    \brief  Prevents receiving of data that have not been mapped using recv_data()
    \param  parameter1 True to block, else release the block.
    */
    void set_block_unmapped_recvs(bool);
    /*!
    \fn void block_var::set_block_dir_pulls(bool parameter1);
    \brief  Prevents requests for directory listing
    \param  parameter1 True to block, else release the block.
    */
    void set_block_dir_pulls(bool);
    /*!
    \fn void block_var::set_block_cmd_pulls(bool parameter1);
    \brief  Prevents requests for execution of shell commands
    \param  parameter1 True to block, else release the block.
    */
    void set_block_cmd_pulls(bool);
    /*!
    \fn	void block_var::set_timeout(int parameter1);
    \brief  Sets a timeout in sec for the transfer. The tranfer will be terminated if nothing is heard from
            the other party after a certain time.
    \param  timeout Timeout in secs.
     */
    void set_timeout(int);
    /*!
    \fn bool block_var::push_data(const char* name, const char* buf, size_t len);
    \brief  Sends memory using push to the other endpoint. It will fail if it is currently sending data.
    \param  name    The name of the data in memory.
    \param  buf     The memory buffer.
    \param  len     The length in bytes.
    \returns    True if it succeeds, false if it fails.
     */
    bool push_data(const char* name, const char* buf, size_t len);
    /*!
    \fn bool block_var::push_data(const char* name, mpt_var* var);
    \brief  Sends a var using push to the other endpoint. It will fail if it is currently sending data.
    \param name    The name of the var.
    \param var     Pointer to the var. This will perform an output call on the var to generate its byte
                   stream and sends it.
    \returns    True if it succeeds, false if it fails.
     */
    bool push_data(const char* name, mpt_var* var);
    /*!
    \fn bool block_var::push_data(const char* name, const char* filepath);
    \brief  Sends a file using push to the other endpoint. It will fail if it is currently sending data.
    \param  name        The name of the file.
    \param  filepath    The path to file.
    \returns    True if it succeeds, false if it fails.
     */
    bool push_data(const char* name, const char* filepath);
    /*!
    \fn void block_var::push_data_on_trigger(const char* name, mpt_var* var);
    \brief  Monitors the provided var by installing shows to watch for setDirty or notDirty triggers. Once
            trigger it will use its default rules to find the data, and to send it using push to the other
            endpoint. If it is currently sending data, the show will remain dirty, which will cause the
            transfer to start immediately after the current transfer is completed.

            Default rules are used when a send is initiated by a push_data_on_trigger, or a pull request
            arriving from the other endpoint. The rules are:
                1. Look for a match in its structures, installed by push_data_on_pull calls
                2. Look for a match in RM Name
                3. Tries to read from a file with the name.
                4. Gives up.
    \param  name    The name of the data.
    \param  var     The trigger variable to watch for.
     */
    void push_data_on_trigger(const char* name, mpt_var* var);
    /*!
    \fn void block_var::push_data_on_pull(const char* name, const char* filepath, bool onceonly = false);
    \brief  Associates a file with a name of the data and stores it in its internal structures. This installs
            a rule of the highest priority when block_var needs to convert a name into data to be sent out.
            Previous association to this name will be removed.
    \param  name        The name of the file.
    \param  filepath    The path of the file.
    \param  onceonly    (Optional, default is false) True to remove the association after it has been used, which
                        makes the association a temporary one.
     */
    void push_data_on_pull(const char* name, const char* filepath, bool onceonly = false);
    /*!
    \fn void block_var::push_data_on_pull(const char* name, mpt_var* var, bool onceonly = false);
    \brief  Associates a var with a name of the data and stores it in its internal structures. This installs
            a rule of the highest priority when block_var needs to convert a name into data to be sent out.
            Previous association to this name will be removed. If name ends with a *, this is a wildcard
            and all pulls that matches the name in front of the wildcard character will result in the filepath
            being pushed. If the filepath also ends with a *, the wildcard in the filepath will be substituted
            with the substring after name wildcard match. For example, push_data_on_pull("session*", "file*"), when
            a pull with session01 is recieved, file01 will be pushed out.
    \param  name        The name of the var.
    \param  var         The pointer to the var.
    \param  onceonly    (Optional, default is false) True to remove the association after it has been used, which
                        makes the association a temporary one.
     */
    void push_data_on_pull(const char* name, mpt_var* var, bool onceonly = false);
    /*!
    \fn void block_var::push_data_on_pull(const char* name, block_var_callback cb, bool onceonly = false);
    \brief  Pushes a data on pull
    \param  name        Associates a callback function with a name of the data and stores it in its internal
                        structures. This installs a rule of the highest priority when block_var needs to convert a
                        name into data to be sent out. Previous association to this name will be removed.
    \param  cb          The callback function that will return the memory to be sent.
    \param  onceonly    (Optional, default is false) True to remove the association after it has been used, which
                        makes the association a temporary one.
     */
    void push_data_on_pull(const char* name, block_var_callback cb, bool onceonly = false);
    /*!
    \fn bool block_var::pull_data(const char* name, const char* filepath);
    \brief  Recieves a file using pull from the other endpoint. This will fail if it is currently receiving data.
            If the other endpoint is unable to resolve the name using its default rules, it will return an
            empty file. This function will perform a once only association of the name to the file, so that
            when the data arrives, it gets written to the correct destination.
    \param  name        The name of the file.
    \param  filepath    The path of the file to write to.
    \returns    True if it succeeds, false if it fails.
     */
    bool pull_data(const char* name, const char* filepath);
    /*!
    \fn bool block_var::pull_data(const char* name, mpt_var* var);
    \brief  Recieves a var using pull from the other endpoint. This will fail if it is currently receiving data.
            If the other endpoint is unable to resolve the name using its default rules, it will return no data.
            This function will perform a once only association of the name to the var, so that when the
            data arrives, it gets written to the correct destination.
    \param  name    The name of the var.
    \param  var     The var to write to. Extract will be called when the full data is received.
    \returns    True if it succeeds, false if it fails.
     */
    bool pull_data(const char* name, mpt_var* var); // extract will be called
    /*!
    \fn bool block_var::pull_data(const char* name, block_var_callback cb);
    \brief  Recieves to a memory location using pull from the other endpoint. This will fail if it is currently
            receiving data. If the other endpoint is unable to resolve the name using its default rules, it will
            return no data. This function will perform a once only association of the name to the callback, so that
            when the data arrives, it gets written to the correct destination.
    \param  name    The name of the data.
    \param  cb      The callback function will be called to get the memory address to write to.
    \returns    True if it succeeds, false if it fails.
     */
    bool pull_data(const char* name, block_var_callback cb);
    /*!
    \fn bool block_var::get_dir(const char* name, mpt_var* var);
    \brief  Pulls a directory listing from the other endpoint. The output is a string containing the list of files,
            separated with newlines. This function will perform a once only association of the name to the var, so
            that when the data arrives, it gets written to the correct destination.
    \param  name    The path to the directory.
    \param  var     The var, must be value_var or array_value_var where the string will be written.
    \returns    True if it succeeds, false if it fails.
     */
    bool get_dir(const char* name, mpt_var* var);
    /*!
    \fn bool block_var::exec_cmd(const char* name, mpt_var* var);
    \brief  Executes the 'command' at the other endpoint and pulls the results. The output is a string containing
            the standard output of the command that was executed. This function will perform a once only association
            of the name to the var, so that when the data arrives, it gets written to the correct destination.
    \param  name    The name of the command and arguments, separated by spaces, if needed.
    \param  var     The var, must be value_var or array_value_var where the string will be written.
    \returns    True if it succeeds, false if it fails.
     */
    bool exec_cmd(const char* name, mpt_var* var); // must be value_var or array_value_var
    /*!
    \fn bool block_var::get_sha1(const char* names, mpt_var* var);
    \brief  Computes SHA1 hash of the file at the other endpoint and pulls the results. The output is a string
            containing the hash. This function will perform a once only association of the name to the var, so
            that when the data arrives, it gets written to the correct destination. (Not implemented)
    \param  names   The names of the files.
    \param  var     The var, must be value_var or array_value_var where the string will be written.
    \returns    True if it succeeds, false if it fails.
     */
    bool get_sha1(const char* names, mpt_var* var); // extract will be called
    /*!
    \fn void block_var::recv_data(const char* name, const char* filepath, bool onceonly = false);
    \brief  Associates a file with a name of the data and stores it in its internal structures. This installs
            a rule of the highest priority when block_var needs to convert a name from the other endpoint into
            a local resource that it should write to. Previous association to this name will be removed.

            Block_var will always wait for data automatically, processing them with the usual extract. When
            it recieves new data from the other endpoint, it uses the default rules to determine where to
            write the data to. The rules are:
                 1. Look for a match in its structures, created from pull and recv calls.
                 2. Look for a match in RM Name
                 3. Tries to write to file with the name
                 4. Throw away
    \param  name        The name of the file.
    \param  filepath    The path of the file.
    \param  onceonly    (Optional, default is false) True to remove the association after it has been used, which
                        makes the association a temporary one.
     */
    void recv_data(const char* name, const char* filepath, bool onceonly = false);
    /*!
    \fn void block_var::recv_data(const char* name, mpt_var* var, bool onceonly = false);
    \brief  Associates a var with a name of the data and stores it in its internal structures. This installs
            a rule of the highest priority when block_var needs to convert a name from the other endpoint into
            a local resource that it should write to. Previous association to this name will be removed. If
            name ends with a *, this is a wildcard, and all data received matching the string before the wildcard
            character will match, and saved to filepath. If filepath also ends with a *, the wildcard in the
            filepath will be substituted with the substring after name wildcard match. For example,
            recv_data("session*", "file*"), when session01 is recieved, the file will be saved to file01.
    \param  name        The name of the var.
    \param  var         The var to write to. Extract will be called on the var when the full data is received.
    \param  onceonly    (Optional, default is false) True to remove the association after it has been used, which
                        makes the association a temporary one.
     */
    void recv_data(const char* name, mpt_var* var, bool onceonly = false);
    /*!
    \fn void block_var::recv_data(const char* name, block_var_callback cb, bool onceonly = false);
    \brief  Associates a var with a name of the data and stores it in its internal structures. This installs
            a rule of the highest priority when block_var needs to convert a name from the other endpoint into
            a local resource that it should write to. Previous association to this name will be removed.
    \param  name        The name of the data.
    \param  cb          The callback function that will be called to get the address and size to write to.
    \param  onceonly    (Optional, default is false) True to remove the association after it has been used, which
                        makes the association a temporary one.
     */
    void recv_data(const char* name, block_var_callback cb, bool onceonly = false);
    /*!
    \fn void block_var::pull_data_on_trigger(mpt_var* name, mpt_var* var);
    \brief  This function eliminates the need to make calls directly to block_var. Block_var will
            watch another var, and when triggered by the var, it stringifies the content of the
            var and uses it as name to create a pull request. The data that is received will be
            written to the output var.
    \param  name    The var that will provide the name, with to_const_char_ptr, and the trigger to start the transfer
    \param  var     The var the data will be written to. Extract will be called when the complete data is received.
     */
    void pull_data_on_trigger(mpt_var* name, mpt_var* var);
    /*!
    \fn void block_var::get_dir_on_trigger(mpt_var* name, mpt_var* var);
    \brief  This function eliminates the need to make calls directly to block_var. Block_var will
            watch another var, and when triggered by the var, it stringifies the content of the
            var and uses it as name to create a directory listing request on the other endpoint. The data that is
            received will be written to the output var.
    \param  name    The var that will provide the name, with to_const_char_ptr, and the trigger to start the transfer
    \param  var     The var the data will be written to. Extract will be called when the complete data is received.
     */
    void get_dir_on_trigger(mpt_var* name, mpt_var* var);
    /*!
    \fn void block_var::exec_cmd_on_trigger(mpt_var* name, mpt_var* var);
    \brief  This function eliminates the need to make calls directly to block_var. Block_var will
            watch another var, and when triggered by the var, it stringifies the content of the
            var and uses it as name to create a command execution request on the other endpoint. The standard output
            of the command execution will be received and written to the output var.
    \param  name    The var that will provide the name, with to_const_char_ptr, and the trigger to start the transfer
    \param  var     The var the data will be written to. Extract will be called when the complete data is received.
     */
    void exec_cmd_on_trigger(mpt_var* name, mpt_var* var);
    /*!
    \fn void block_var::get_sha1_on_trigger(mpt_var* names, mpt_var* var);
    \brief  This function eliminates the need to make calls directly to block_var. Block_var will
            watch another var, and when triggered by the var, it stringifies the content of the
            var and uses it as name to create a SHA1 hash request for the file of the given name on the other endpoint.
            The hash will be received and written to the output var.
    \param  name    The var that will provide the name, with to_const_char_ptr, and the trigger to start the transfer
    \param  var     The var the data will be written to. Extract will be called when the complete data is received.
     */
    void get_sha1_on_trigger(mpt_var* names, mpt_var* var);

    /* Miscellaneous overrides */
    /*!
    \fn mpt_var* block_var::getNext();
    \brief  Gets the next item if this item was created in an array.
    \returns    Pointer to next item in array.
     */
    /*reimp*/ mpt_var* getNext();
    /*!
    \fn const mpt_var& block_var::operator=(const mpt_var& right);
     \brief Assignment operator, virtual override so that it works without needing to upcast. This
            function will only copy the block size and reset any existing transfers.
     \param     right   The object to be copied from, a deep copy will be made if the type is
                        also a block_var.
     \returns   A const reference to this object.
     */
    /*reimp*/ const mpt_var& operator=(const mpt_var& right);
    /*!
    \fn bool block_var::operator==(const mpt_var& right) const;
    \brief Equality operator, virtual override so that it works without needing to upcast. This base
           class implementation will test the right object for link_var and invert the comparision.
    \param     right   The right object to be compared with.
    \returns   False at all times.
     */
    /*reimp*/ bool operator==(const mpt_var& right) const;
    /*!
    \fn istream& block_var::operator>>(istream& s);
    \brief Global stream extraction operator supporting mpt_var. Will redirect to the class
           virtual extraction operator function.
    \param [in]    s       an istream to process.
    \param [out]   right   The mpt_var object.
    \returns   The input stream.
     */
    /*reimp*/ istream& operator>>(istream& s);
    /*!
    \fn    extern RM_EXPORT ostream& operator<<(ostream& s, const mpt_var& right);
    \brief Global stream insertion operator supporting mpt_var. Will redirect to the class
           virtual insertion operator function.
    \param [in]    s       an ostream to process.
    \param [in]    right   The const mpt_var object.
    \returns   The output stream.
    */
    /*reimp*/ ostream& operator<<(ostream& s) const;    /*!< . */

    /* Status - these variables are only to be used to add and remove callback */
    generic_var<unsigned char> send_percent;    /*!< The send percent */
    generic_var<unsigned char> recv_percent;    /*!< The receive percent */
    string_var<256> send_name;  /*!< Name of the send */
    string_var<256> recv_name;  /*!< Name of the receive */

private:
    /*!
    \fn void block_var::processing();
    \brief  Called based on the mode of send processing to compute the next packet
            that should be sent out.
     */
    void processing();

    int block_size; /*!< Size of the block */
    int send_session_id;    /*!< Identifier for the send session */
    int recv_session_id;    /*!< Identifier for the receive session */
    sender_base* active_sender; /*!< The active sender */
    receiver_base* active_receiver; /*!< The active receiver */
    int nack;   /*!< The nack */
    char* req_block;    /*!< The request block */
    int req_block_sz;   /*!< includes type field. If have ack need to insert ack_block_no at byte 1. */
    block_var_ack_queue ack;    /*!< The acknowledge queue */
    bool pause; /*!< True to pause transfer */
    trigger_list* triggers; /*!< The triggers that will initiate a pull or push */
    data_mapping* recv_mapper;  /*!< The receive mapper that associates names to resources */
    data_mapping* send_mapper;  /*!< The send mapper that associates names to resoures */
    mpt_baseshow* show; /*!< The show that governs the rate of output */
    bool block_unmapped_sends; /*!< If true, unmapped sends are not permitted */
    bool block_unmapped_recvs; /*!< If true, unmapped recvs are not permitted */
    bool block_dir_pulls; /*!< If true, directory listings are not permitted */
    bool block_cmd_pulls; /*!< If true, shell commands are not permitted */
    int timeout; /*!< Time in sec to wait before transfer times out */

};

#endif /* _BLK_VAR_H_ */

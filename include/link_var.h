/******************************************************************/
/* Copyright DSO National Laboratories 2005. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _LINK_VAR_H_
#define _LINK_VAR_H_

#include <mpt_var.h>

class RM_EXPORT link_var : public mpt_var
{
public:
    RUNTIME_TYPE(link_var)

    link_var();

    // Virtual functions
    virtual int extract(int len, const unsigned char* buf);
    virtual int size() const;
    virtual void output(outbuf& strm);
    virtual bool isValid() const;
    virtual bool setInvalid();
    virtual mpt_var* getNext();
    virtual const mpt_var& operator=(const mpt_var& right);
    virtual bool operator==(const mpt_var& right) const;
    virtual istream& operator>>(istream& s);
    virtual ostream& operator<<(ostream& s) const;
    virtual void refresh();

protected:
    mpt_var* link;
};

#endif /* _LINK_VAR_H_ */

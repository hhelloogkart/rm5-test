/******************************************************************/
/* Copyright DSO National Laboratories 2019. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _PAD_VAR_H_
#define _PAD_VAR_H_

#include <cplx_var.h>

//! Pads the output to a fixed number of bytes
class RM_EXPORT vpad_var : public complex_var
{
public:
    vpad_var(int);
    /*reimp*/ int size() const;
    /*reimp*/ void output(outbuf& strm);

private:
    vpad_var();
    int desired_size;

};

template<int sz>
class pad_var : public vpad_var
{
public:
    pad_var() : vpad_var(sz)
    {
    }

    bool operator==(const mpt_var& right) const
    {
        return complex_var::operator==(right);
    }

    bool operator==(const pad_var<sz>& right) const
    {
        return complex_var::operator==(static_cast<const complex_var&>(right));
    }
};

#define PADCONST(t) t(const t& arg) \
{ \
    child_copy_constructor(arg); \
} \
t() \
{ \
    pop(); \
} \
const t& operator=(const t& right) \
{ \
    complex_var::operator=(right); \
    return *this; \
} \
const mpt_var& operator=(const mpt_var& right) \
{ \
    complex_var::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)
#endif // _PAD_VAR_H_
#ifndef _RM_CDATA_H_
#define _RM_CDATA_H_

#ifndef NAME_LEN
#define NAME_LEN 32
#endif

#ifndef RM_EXPORT
#ifdef _WIN32
# pragma warning(disable:4786)
# ifdef LIBRARY
#  define RM_EXPORT __declspec(dllexport)
#  pragma warning( disable : 4251 4275 )
# elif defined(STATIC)
#  define RM_EXPORT
# else
#  define RM_EXPORT __declspec(dllimport)
#  pragma warning( disable : 4251 4275 )
# endif
#else
# define RM_EXPORT
#endif
#endif

typedef unsigned int (* virtual_method)(void* dest, int num, unsigned char* buf, int* len);
typedef int (* size_method)(void* dest);    // type 'size_method' which is pointer to function, accepts a void pointer, and returning an 'int'

typedef struct
{
    char host[64];
    unsigned short port;
    unsigned short flow;
    char access[16];
} connection_info;

typedef struct
{
    virtual_method extract;     // 'extract' is a pointer to a function of type virtual_method which accepts 4 arguments and returns an unsigned int
    virtual_method pack;        // 'pack' is a pointer to a function of type virtual_method which accepts 4 arguments and returns an unsigned int
    size_method size;           // Necessary because the size is dynamic due to the use of flex_var
    unsigned char dirty;
    unsigned char rmdirty;      // This is NOT defined in the original C++ codes. What is the intention and how different is it from 'dirty' flag?
    char rmname[NAME_LEN];      // 'vtbl[0].rmname' stores the client name
    int regflag;                // 'vtbl[0].regflag' stores the size of the virtual table. Bit 0 - register flag, Bit 1 - read-only flag, Bit 2 - message flag, Bit 3 - data flag
    void* dest;                 // pointer to the 'connection_info' struct. Refer to 'data_init(...), vtbl[0].dest' in data.c
} virtual_table;

enum
{
    typ_rm_var = 0,
    typ_rm_message_var,
    typ_rm_data_var
};

// The following declaration is directly related to the bitfield definitions of 'regflag'
// Assumption: Only the 4 least significant bits are defined
//		+---+---+---+---+---+---+---+---+
//	bit	| X | X	| X	| X	| 3	| 2 | 1	| 0	|
//		+---+---+---+---+---+---+---+---+
// bit 0: Register flag			bit 1: Read-only flag
// bit 2: RM message flag		bit 4: RM data flag
#define REG_FLG_MASK    (0x01)  // 0000 0001
#define ACCESS_FLG_MASK (0x02)  // 0000 0010
#define MSG_FLG_MASK    (0x04)  // 0000 0100
#define DATA_FLG_MASK   (0x08)  // 0000 1000

#ifdef __cplusplus
extern "C"
{
#endif

RM_EXPORT unsigned int generic_extract_pack(void*, int, const void*, int*);

RM_EXPORT unsigned int char_extract(char* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int unsigned_char_extract(unsigned char* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int short_extract(short* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int unsigned_short_extract(unsigned short* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int int_extract(int* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int unsigned_int_extract(unsigned int* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int long_extract(long* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int unsigned_long_extract(unsigned long* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int long_long_extract(long long* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int unsigned_long_long_extract(unsigned long long* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int float_extract(float* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int double_extract(double* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int decode_extract(double* dest, int num, int byte_size, double range, double offset, int scale_offset, const unsigned char* buf, int* len);
RM_EXPORT unsigned int bit_extract(int* dest, int num, int length, int offset, const unsigned char* buf, int* len);
RM_EXPORT unsigned int decode_bit_extract(double* dest, int num, int bit_size, int bit_offset, double range, double offset, int scale_offset, const unsigned char* buf, int* len);
RM_EXPORT unsigned int string_extract(char* dest, int num, int length, const unsigned char* buf, int* len);
RM_EXPORT unsigned int ttag_extract(int* dest, int num, const unsigned char* buf, int* len);

RM_EXPORT unsigned int char_r_extract(char* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int unsigned_char_r_extract(unsigned char* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int short_r_extract(short* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int unsigned_short_r_extract(unsigned short* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int int_r_extract(int* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int unsigned_int_r_extract(unsigned int* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int long_r_extract(long* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int unsigned_long_r_extract(unsigned long* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int long_long_r_extract(long long* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int unsigned_long_long_r_extract(unsigned long long* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int float_r_extract(float* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int double_r_extract(double* dest, int num, const unsigned char* buf, int* len);
RM_EXPORT unsigned int decode_r_extract(double* dest, int num, int byte_size, double range, double offset, int scale_offset, const unsigned char* buf, int* len);
RM_EXPORT unsigned int bit_r_extract(int* dest, int num, int length, int offset, const unsigned char* buf, int* len);
RM_EXPORT unsigned int decode_bit_r_extract(double* dest, int num, int bit_size, int bit_offset, double range, double offset, int scale_offset, const unsigned char* buf, int* len);

RM_EXPORT unsigned int char_pack(char* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int unsigned_char_pack(unsigned char* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int short_pack(short* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int unsigned_short_pack(unsigned short* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int int_pack(int* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int unsigned_int_pack(unsigned int* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int long_pack(long* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int unsigned_long_pack(unsigned long* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int long_long_pack(long long* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int unsigned_long_long_pack(unsigned long long* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int float_pack(float* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int double_pack(double* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int decode_pack(double* dest, int num, int byte_size, double range, double offset, int scale_offset, unsigned char* buf, int* len);
RM_EXPORT unsigned int bit_pack(int* dest, int num, int length, int offset, unsigned char* buf, int* len);
RM_EXPORT unsigned int decode_bit_pack(double* dest, int num, int bit_size, int bit_offset, double range, double offset, int scale_offset, unsigned char* buf, int* len);
RM_EXPORT unsigned int string_pack(char* dest, int num, int length, unsigned char* buf, int* len);
RM_EXPORT unsigned int ttag_pack(int* dest, int num, unsigned char* buf, int* len);

RM_EXPORT unsigned int char_r_pack(char* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int unsigned_char_r_pack(unsigned char* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int short_r_pack(short* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int unsigned_short_r_pack(unsigned short* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int int_r_pack(int* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int unsigned_int_r_pack(unsigned int* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int long_r_pack(long* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int unsigned_long_r_pack(unsigned long* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int long_long_r_pack(long* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int unsigned_long_long_r_pack(unsigned long* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int float_r_pack(float* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int double_r_pack(double* dest, int num, unsigned char* buf, int* len);
RM_EXPORT unsigned int decode_r_pack(double* dest, int num, int byte_size, double range, double offset, int scale_offset, unsigned char* buf, int* len);
RM_EXPORT unsigned int bit_r_pack(int* dest, int num, int length, int offset, unsigned char* buf, int* len);
RM_EXPORT unsigned int decode_bit_r_pack(double* dest, int num, int bit_size, int bit_offset, double range, double offset, int scale_offset, unsigned char* buf, int* len);

RM_EXPORT void rm_var_init(virtual_table*);

#ifdef __cplusplus
}
#endif

#endif /* _RM_CDATA_H_ */

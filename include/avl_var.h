/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _AVL_VAR_H_
#define _AVL_VAR_H_

#include <mpt_var.h>
/**
 \class   array_value_var
 \brief   Base class for all array_vars and r_array_vars This is a pure virtual class, as such
          it does not have rtti or getNext overrides.
 \details This is the base class for variable length arrays of basic data types: int, short, char,
          unsigned int, unsigned short, unsigned char, double and float. Data is streamed with the
          cardinal first, which represents the number of elements following, then each element
          in turn.

          This class provides a universal interface to convert between data of different types,
          similar to value_var.

          For example, to get a double value, we can use to_double(index) on any subclass and
          conversion routines will be called if required. Unlike value_var as it needs an index.
          This means that a double stored as a string in string_var will be interpreted and
          returned in the extreme case; in fact this makes the whole concept of storing data
          similar to interpreted languages such as Perl. However, if the index is not valid
          zero will be returned.

          There are 4 main types of methods, the virtual to_* and from_* functions that are
          used to force the return of a certain type. toValue and fromValue functions with
          overloads for different types so that it can be used in a template, or have the
          compiler determine the correct overload based on the parameter type.

          The from_* functions will call setDirty if the value is updated, or notDirty, if
          the value is unchanged. The to_* functions will generally not call any of the dirty
          functions with a single exception, array_str_var::to_char_ptr. This is the only instance
          where the actual string buffer is provided, and there is a potential for modification.
 */
class RM_EXPORT array_value_var : public mpt_var
{
protected:
    char* data; /*!< The pointer to the data */
    unsigned int sz;    /*!< The size of data in bytes */

public:

    /**
     \fn    array_value_var::array_value_var();
     \brief Default constructor that builds a zero element array.
     */
    array_value_var();
    /**
     \fn    array_value_var::array_value_var(const array_value_var& cpy);
     \brief Copy constructor makes a deep copy of the array.
     \param     cpy The copy.
     */
    array_value_var(const array_value_var& cpy);
    /**
     \fn    array_value_var::~array_value_var();
     \brief Destructor
     */
    ~array_value_var();
    /**
     \fn    operator const array_value_var::void* () const;
     \brief Gets the pointer to the array, could be null if the size is zero.
     \returns   A const pointer to the array.
     */
    operator const void* () const;
    /**
     \fn    operator array_value_var::void* ();
     \brief Gets the pointer to the array, could be null if the size is zero. This will cause
            setDirty to be called unless the size is zero.
     \returns   A pointer to the array.
     */
    operator void* ();
    /**
     \fn    int array_value_var::cardinal() const;
     \brief Gets the cardinal, which is the number of elements in the array
     \returns   An int.
     */
    int cardinal() const;
    /**
     \fn    void array_value_var::resize(int len);
     \brief Resizes the number of elements in the array. If the size is the same as the previous
            size, nothing happens and setDirty and setRMDirty will not be called. If the size
            is increased, the additional elements have all bits set to 1, in accordance with how
            all vars are initialized. If the size is reduced, the excess elements are lost.
     \param     len The new number of elements.
     */
    void resize(int len);

protected:

    /**
     \fn    virtual int array_value_var::width() const = 0;
     \brief Gets the width in bytes of one element in the array. This is an abstract function
            that derived classes have to override.
     \returns   Number of bytes used by one element.
     */
    virtual int width() const = 0;
    /**
     \fn    virtual int array_value_var::cardinal_width() const;
     \brief Get the width in bytes used to represent the cardinal value. The default implementation
            is 2 bytes, meaning that up to 65535 elements could be represented. A negative width is
            used to indicate that the endian needs to be reversed in the byte stream.
     \returns   Number of bytes used to represent the cardinal.
     */
    virtual int cardinal_width() const;
    /**
     \fn    virtual int array_value_var::cardinal_offset() const;
     \brief Offset in bytes of the cardinal representation from the start of the cardinal boundary
            in the byte stream. The default value is 0.
     \returns   An int.
     */
    virtual int cardinal_offset() const;
    /**
     \fn    virtual int array_value_var::size_cardinal() const;
     \brief Gets the size in bytes that the cardinal will occupy in the byte stream. This size
            adds to the overhead of the byte stream, and must be equal or larger than the cardinal
            width. The default value is 2.
     \returns   Number of bytes occupied by the cardinal in the byte stream.
     */
    virtual int size_cardinal() const;

public:

    /**
     \fn    virtual void array_value_var::output_cardinal(outbuf& strm);
     \brief Output cardinal into the byte stream, based on the cardinal width, offset and size properties.
     \param [in,out]    strm    The byte stream to push bytes into.
     */
    virtual void output_cardinal(outbuf& strm);
    /**
     \fn    virtual int array_value_var::extract_cardinal(int buflen, const unsigned char* buf, int& car);
     \brief Extracts the cardinal from the byte stream, based on the cardinal width, offset and size
            properties.
     \param          buflen  The length of the buffer in bytes.
     \param          buf     The buffer.
     \param [out]    car     The reference to write the cardinal value into.
     \returns   Remaining bytes in the buffer still unprocessed.
     */
    virtual int extract_cardinal(int buflen, const unsigned char* buf, int& car);
    /**
     \fn    virtual int array_value_var::extract(int len, const unsigned char* buf);
     \brief Extracts the data from a byte stream. It calls extract_cardinal first to retreive the number
            of elements, before iteratively reading each elements. If the number of elements is changed,
            or if the value of any element is changed, setDirty will be called. Otherwise notDirty will
            be called.
     \param     len The length of the buffer in bytes.
     \param     buf The buffer.
     \returns   Remaining bytes in the buffer still unprocessed.
     */
    virtual int extract(int len, const unsigned char* buf);
    /**
     \fn    virtual int array_value_var::size() const;
     \brief Returns the size in bytes that this object will need to serialize itself into a byte stream.
     \returns   Size in bytes.
     */
    virtual int size() const;
    /**
     \fn    virtual void array_value_var::output(outbuf& strm);
     \brief Outputs the values of the object to the byte stream.
     \param [in,out]    strm    The byte stream stream to push bytes into.
     */
    virtual void output(outbuf& strm);
    /**
     \fn    virtual bool array_value_var::isValid() const;
     \brief Check if the array has any elements in it.
     \returns   True if number of elements is one or more, false if zero.
     */
    virtual bool isValid() const;
    /**
     \fn    virtual bool array_value_var::setInvalid();
     \brief Clears all elements in the array. This is the same as resize(0).
     \returns   True if it previously had elements which were then cleared, false if it was already empty.
     */
    virtual bool setInvalid();
    /**
     \fn    const array_value_var& array_value_var::operator=(const array_value_var& right);
     \brief Assignment operator, a deep copy of the variable length array is made even if width is not
            the same.
     \param     right   The value to make a copy from.
     \returns   A reference of this object.
     */
    const array_value_var& operator=(const array_value_var& right);
    /**
     \fn    virtual const mpt_var& array_value_var::operator=(const mpt_var& right);
     \brief Assignment operator, a deep copy of the variable length array is made, if it can be cast
            to the correct type, otherwise it does nothing.
     \param     right   The value to make a copy from.
     \returns   A reference of this object.
     */
    virtual const mpt_var& operator=(const mpt_var& right);
    /**
     \fn    bool array_value_var::operator==(const array_value_var& right) const;
     \brief Equality operator
     \param     right   The other object to compare with.
     \returns   True if both variable arrays are byte equivalent, false otherwise or if
                the argument cannot be cast into a variable length array.
     */
    bool operator==(const array_value_var& right) const;
    /**
     \fn    virtual bool array_value_var::operator==(const mpt_var& right) const;
     \brief Equality operator
     \param     right   The other object to compare with.
     \returns   True if both variable arrays are byte equivalent.
     */
    virtual bool operator==(const mpt_var& right) const;
     /**
     \fn    virtual istream& array_value_var::operator>>(istream& s);
     \brief Stream extraction operator that reads the cardinal, followed by each
            array value.
     \param [in,out]    s   an istream to process.
     \returns   The input stream.
     */
    virtual istream& operator>>(istream& s);
    /**
     \fn    virtual istream& array_value_var::operator>>(istream& s);
     \brief Stream insertion operator that writes the cardinal, followed by each
            array value.
     \param [in,out]    s   an ostream to process.
     \returns   The output stream.
    */
    virtual ostream& operator<<(ostream& s) const;  /*!< . */
    /**
     \fn    virtual char array_value_var::to_char(unsigned int idx) const = 0;
     \brief Abstract function. Converts the element at index idx to a char value. Rounding and
            clipping the value at the boundary will be applied. If the index is out of range,
            zero will be returned.
     \param     idx Zero-based index of the element to reference.
     \returns   The value converted into a char.
     */
    virtual char to_char(unsigned int idx) const = 0;
    /**
     \fn    virtual unsigned char array_value_var::to_unsigned_char(unsigned int idx) const = 0;
     \brief Abstract function. Converts the element at index idx to an unsigned char value. Rounding
            and clipping the value at the boundary will be applied. If the index is out of range,
            zero will be returned.
     \param     idx Zero-based index of the element to reference.
     \returns   The value converted into a char.
     */
    virtual unsigned char to_unsigned_char(unsigned int idx) const = 0;
    /**
     \fn    virtual short array_value_var::to_short(unsigned int idx) const = 0;
     \brief Abstract function. Converts the element at index idx to a short value. Rounding and
            clipping the value at the boundary will be applied. If the index is out of range,
            zero will be returned.
     \param     idx Zero-based index of the element to reference.
     \returns   The value converted into a short.
     */
    virtual short to_short(unsigned int idx) const = 0;
    /**
     \fn    virtual unsigned short array_value_var::to_unsigned_short(unsigned int idx) const = 0;
     \brief Converts an idx to an unsigned short
     \param     idx Zero-based index of the.
     \returns   The value converted into a short.
     */
    virtual unsigned short to_unsigned_short(unsigned int idx) const = 0;
    /**
     \fn    virtual int array_value_var::to_int(unsigned int idx) const = 0;
     \brief Abstract function. Converts the element at index idx to an int value. Rounding and
            clipping the value at the boundary will be applied. If the index is out of range,
            zero will be returned.
     \param     idx Zero-based index of the element to reference.
     \returns   The value converted into an int.
     */
    virtual int to_int(unsigned int idx) const = 0;
    /**
     \fn    virtual unsigned int array_value_var::to_unsigned_int(unsigned int idx) const = 0;
     \brief Converts an idx to an unsigned int
     \param     idx Zero-based index of the.
     \returns   The value converted into an int.
     */
    virtual unsigned int to_unsigned_int(unsigned int idx) const = 0;
    /**
     \fn    virtual long array_value_var::to_long(unsigned int idx) const = 0;
     \brief Abstract function. Converts the element at index idx to a long value. Rounding and
            clipping the value at the boundary will be applied. If the index is out of range,
            zero will be returned.
     \param     idx Zero-based index of the element to reference.
     \returns   The value converted into a long.
     */
    virtual long to_long(unsigned int idx) const = 0;
    /**
     \fn    virtual unsigned long array_value_var::to_unsigned_long(unsigned int idx) const = 0;
     \brief Converts an idx to an unsigned long
     \param     idx Zero-based index of the.
     \returns   The value converted into a long.
     */
    virtual unsigned long to_unsigned_long(unsigned int idx) const = 0;
    /**
     \fn    virtual long long array_value_var::to_long_long(unsigned int idx) const = 0;
     \brief Converts an idx to a long
     \param     idx Zero-based index of the.
     \returns   The value converted into a long.
     */
    virtual long long to_long_long(unsigned int idx) const = 0;
    /**
     \fn    virtual unsigned long long array_value_var::to_unsigned_long_long(unsigned int idx) const = 0;
     \brief Abstract function. Converts the element at index idx to a long long value. Rounding and
            clipping the value at the boundary will be applied. If the index is out of range,
            zero will be returned.
     \param     idx Zero-based index of the element to reference.
     \returns   The value converted into a long long.
     */
    virtual unsigned long long to_unsigned_long_long(unsigned int idx) const = 0;
    /**
     \fn    virtual float array_value_var::to_float(unsigned int idx) const = 0;
     \brief Abstract function. Converts the element at index idx to a float value. Clipping the
            value at the boundary will be applied. If the index is out of range,
            zero will be returned.
     \param     idx Zero-based index of the element to reference.
     \returns   The value converted into a float.
     */
    virtual float to_float(unsigned int idx) const = 0;
    /**
     \fn    virtual double array_value_var::to_double(unsigned int idx) const = 0;
     \brief Abstract function. Converts the element at index idx to a double value. Clipping the
            value at the boundary will be applied. If the index is out of range,
            zero will be returned.
     \param     idx Zero-based index of the element to reference.
     \returns   The value converted into a double.
     */
    virtual double to_double(unsigned int idx) const = 0;
    /**
     \fn    virtual char* array_value_var::to_char_ptr(unsigned int idx) = 0;
     \brief Abstract function. Stringify the element at index idx to a string value. If the index is out
            of range, zero will be returned. This is not thread-safe or reentrant. If the concrete class
            happens to be an array_str_var, the actual string buffer will be passed out and can be modified.
     \param     idx Zero-based index of the element to reference.
     \returns   The string value.
     */
    virtual char* to_char_ptr(unsigned int idx) = 0;
    /**
     \fn    virtual const char* array_value_var::to_const_char_ptr(unsigned int idx) const = 0;
     \brief Abstract function. Stringify the element at index idx to a string value. If the index is out
            of range, zero will be returned. This is not thread-safe or reentrant.
     \param     idx Zero-based index of the element to reference.
     \returns   The string value.
     */
    virtual const char* to_const_char_ptr(unsigned int idx) const = 0;
    /**
     \fn    virtual void array_value_var::from_char(unsigned int idx, char parameter2) = 0;
     \brief Abstract function. Initializes the element at index idx from the given char value, clipping
            the value at the boundaries if required, or stringify if this object is a string.
     \param     idx    Zero-based index of the element to initialize.
     \param     value  The value to use for initialization.
     */
    virtual void from_char(unsigned int idx, char) = 0;
    /**
     \fn    virtual void array_value_var::from_unsigned_char(unsigned int idx, unsigned char) = 0;
     \brief Abstract function. Initializes the element at index idx from the given unsigned char value, clipping
            the value at the boundaries if required, or stringify if this object is a string.
     \param     idx    Zero-based index of the element to initialize.
     \param     value  The value to use for initialization.
     */
    virtual void from_unsigned_char(unsigned int idx, unsigned char) = 0;
    /**
     \fn    virtual void array_value_var::from_short(unsigned int idx, short parameter2) = 0;
     \brief Abstract function. Initializes the element at index idx from the given short value, clipping
            the value at the boundaries if required, or stringify if this object is a string.
     \param     idx    Zero-based index of the element to initialize.
     \param     value  The value to use for initialization.
     */
    virtual void from_short(unsigned int idx, short) = 0;
    /**
     \fn    virtual void array_value_var::from_unsigned_short(unsigned int idx, unsigned short) = 0;
     \brief Abstract function. Initializes the element at index idx from the given unsigned short value, clipping
            the value at the boundaries if required, or stringify if this object is a string.
     \param     idx    Zero-based index of the element to initialize.
     \param     value  The value to use for initialization.
     */
    virtual void from_unsigned_short(unsigned int idx, unsigned short) = 0;
    /**
     \fn    virtual void array_value_var::from_int(unsigned int idx, int parameter2) = 0;
     \brief Abstract function. Initializes the element at index idx from the given int value, clipping
            the value at the boundaries if required, or stringify if this object is a string.
     \param     idx    Zero-based index of the element to initialize.
     \param     value  The value to use for initialization.
     */
    virtual void from_int(unsigned int idx, int) = 0;
    /**
     \fn    virtual void array_value_var::from_unsigned_int(unsigned int idx, unsigned int) = 0;
     \brief Abstract function. Initializes the element at index idx from the given unsigned int value, clipping
            the value at the boundaries if required, or stringify if this object is a string.
     \param     idx    Zero-based index of the element to initialize.
     \param     value  The value to use for initialization.
     */
    virtual void from_unsigned_int(unsigned int idx, unsigned int) = 0;
    /**
     \fn    virtual void array_value_var::from_long(unsigned int idx, long parameter2) = 0;
     \brief Abstract function. Initializes the element at index idx from the given long value, clipping
            the value at the boundaries if required, or stringify if this object is a string.
     \param     idx    Zero-based index of the element to initialize.
     \param     value  The value to use for initialization.
     */
    virtual void from_long(unsigned int idx, long) = 0;
    /**
     \fn    virtual void array_value_var::from_unsigned_long(unsigned int idx, unsigned long) = 0;
     \brief Abstract function. Initializes the element at index idx from the given unsigned long value, clipping
            the value at the boundaries if required, or stringify if this object is a string.
     \param     idx    Zero-based index of the element to initialize.
     \param     value  The value to use for initialization.
     */
    virtual void from_unsigned_long(unsigned int idx, unsigned long) = 0;
    /**
     \fn    virtual void array_value_var::from_long_long(unsigned int idx, long long) = 0;
     \brief Abstract function. Initializes the element at index idx from the given long long value, clipping
            the value at the boundaries if required, or stringify if this object is a string.
     \param     idx    Zero-based index of the element to initialize.
     \param     value  The value to use for initialization.
     */
    virtual void from_long_long(unsigned int idx, long long) = 0;
    /**
     \fn    virtual void array_value_var::from_unsigned_long_long(unsigned int idx, unsigned long long) = 0;
     \brief Abstract function. Initializes the element at index idx from the given unsigned long long value,
            clipping the value at the boundaries if required, or stringify if this object is a string.
     \param     idx    Zero-based index of the element to initialize.
     \param     value  The value to use for initialization.
     */
    virtual void from_unsigned_long_long(unsigned int idx, unsigned long long) = 0;
    /**
     \fn    virtual void array_value_var::from_float(unsigned int idx, float parameter2) = 0;
     \brief Abstract function. Initializes the element at index idx from the given float value, rounding,
            clipping the value at the boundaries if required, or stringify if this object is a string.
     \param     idx    Zero-based index of the element to initialize.
     \param     value  The value to use for initialization.
     */
    virtual void from_float(unsigned int idx, float) = 0;
    /**
     \fn    virtual void array_value_var::from_double(unsigned int idx, double parameter2) = 0;
     \brief Abstract function. Initializes the element at index idx from the given double value, rounding,
            clipping the value at the boundaries if required, or stringify if this object is a string.
     \param     idx    Zero-based index of the element to initialize.
     \param     value  The value to use for initialization.
     */
    virtual void from_double(unsigned int idx, double) = 0;
    /**
     \fn    virtual void array_value_var::from_char_ptr(unsigned int idx, const char* parameter2) = 0;
     \brief Abstract function. Initializes the element at index idx from the given string value, by converting
            the string from its ASCII representation to a numeric value, rounding, clipping the value at the
            boundaries if required. If this object is an array_str_var, a deep copy of the string is
            performed. This method cannot be used if there are nulls within the string to be copied. Resize
            the array_str_var to the desired length, then use to_char_ptr to retrieve the pointer to the
            internal buffer and copy the data in yourself.
     \param     idx    Zero-based index of the element to initialize.
     \param     value  The value to use for initialization.
     */
    virtual void from_char_ptr(unsigned int idx, const char*) = 0;
    /**
     \fn    void array_value_var::toValue(unsigned int idx, long& value) const;
     \brief Converts the value of the array element at index idx and writes it into the reference.
            Conversion includes rounding, clipping at the boundary, or parsing into a value.
     \param         idx     Zero-based index of the source array element to read from.
     \param [out]   value   Destination reference to copy the value into.
     */
    void toValue(unsigned int idx, long& value) const;
    /**
     \fn    void array_value_var::toValue(unsigned int idx, int& value) const;
     \brief Converts the value of the array element at index idx and writes it into the reference.
            Conversion includes rounding, clipping at the boundary, or parsing into a value.
     \param         idx     Zero-based index of the source array element to read from.
     \param [out]   value   Destination reference to copy the value into.
     */
    void toValue(unsigned int idx, int& value) const;
    /**
     \fn    void array_value_var::toValue(unsigned int idx, short& value) const;
     \brief Converts the value of the array element at index idx and writes it into the reference.
            Conversion includes rounding, clipping at the boundary, or parsing into a value.
     \param         idx     Zero-based index of the source array element to read from.
     \param [out]   value   Destination reference to copy the value into.
     */
    void toValue(unsigned int idx, short& value) const;
    /**
     \fn    void array_value_var::toValue(unsigned int idx, char& value) const;
     \brief Converts the value of the array element at index idx and writes it into the reference.
            Conversion includes rounding, clipping at the boundary, or parsing into a value.
     \param         idx     Zero-based index of the source array element to read from.
     \param [out]   value   Destination reference to copy the value into.
     */
    void toValue(unsigned int idx, char& value) const;
    /**
     \fn    void array_value_var::toValue(unsigned int idx, unsigned long& value) const;
     \brief Converts the value of the array element at index idx and writes it into the reference.
            Conversion includes rounding, clipping at the boundary, or parsing into a value.
     \param         idx     Zero-based index of the source array element to read from.
     \param [out]   value   Destination reference to copy the value into.
     */
    void toValue(unsigned int idx, unsigned long& value) const;
    /**
     \fn    void array_value_var::toValue(unsigned int idx, unsigned int& value) const;
     \brief Converts the value of the array element at index idx and writes it into the reference.
            Conversion includes rounding, clipping at the boundary, or parsing into a value.
     \param         idx     Zero-based index of the source array element to read from.
     \param [out]   value   Destination reference to copy the value into.
     */
    void toValue(unsigned int idx, unsigned int& value) const;
    /**
     \fn    void array_value_var::toValue(unsigned int idx, unsigned short& value) const;
     \brief Converts the value of the array element at index idx and writes it into the reference.
            Conversion includes rounding, clipping at the boundary, or parsing into a value.
     \param         idx     Zero-based index of the source array element to read from.
     \param [out]   value   Destination reference to copy the value into.
     */
    void toValue(unsigned int idx, unsigned short& value) const;
    /**
     \fn    void array_value_var::toValue(unsigned int idx, unsigned char& value) const;
     \brief Converts the value of the array element at index idx and writes it into the reference.
            Conversion includes rounding, clipping at the boundary, or parsing into a value.
     \param         idx     Zero-based index of the source array element to read from.
     \param [out]   value   Destination reference to copy the value into.
     */
    void toValue(unsigned int idx, unsigned char& value) const;
    /**
     \fn    void array_value_var::toValue(unsigned int idx, float& value) const;
     \brief Converts the value of the array element at index idx and writes it into the reference.
            Conversion includes clipping at the boundary, or parsing into a value.
     \param         idx     Zero-based index of the source array element to read from.
     \param [out]   value   Destination reference to copy the value into.
     */
    void toValue(unsigned int idx, float& value) const;
    /**
     \fn    void array_value_var::toValue(unsigned int idx, double& value) const;
     \brief Converts the value of the array element at index idx and writes it into the reference.
            Conversion includes clipping at the boundary, or parsing into a value.
     \param         idx     Zero-based index of the source array element to read from.
     \param [out]   value   Destination reference to copy the value into.
     */
    void toValue(unsigned int idx, double& value) const;
    /**
     \fn    void array_value_var::toValue(unsigned int idx, long long& value) const;
     \brief Converts the value of the array element at index idx and writes it into the reference.
            Conversion includes rounding, clipping at the boundary, or parsing into a value.
     \param         idx     Zero-based index of the source array element to read from.
     \param [out]   value   Destination reference to copy the value into.
     */
    void toValue(unsigned int idx, long long& value) const;
    /**
     \fn    void array_value_var::toValue(unsigned int idx, unsigned long long& value) const;
     \brief Converts the value of the array element at index idx and writes it into the reference.
            Conversion includes rounding, clipping at the boundary, or parsing into a value.
     \param         idx     Zero-based index of the source array element to read from.
     \param [out]   value   Destination reference to copy the value into.
     */
    void toValue(unsigned int idx, unsigned long long& value) const;
    /**
     \fn    void array_value_var::fromValue(unsigned int idx, long value);
     \brief Initializes array element at index idx from the given value. Conversion includes
            clipping at the boundary, or stringification.
     \param     idx     Zero-based index of the array element to write to.
     \param     value   The value to convert and copy from.
     */
    void fromValue(unsigned int idx, long value);
    /**
     \fn    void array_value_var::fromValue(unsigned int idx, int value);
     \brief Initializes array element at index idx from the given value. Conversion includes
            clipping at the boundary, or stringification.
     \param     idx     Zero-based index of the array element to write to.
     \param     value   The value to convert and copy from.
     */
    void fromValue(unsigned int idx, int value);
    /**
     \fn    void array_value_var::fromValue(unsigned int idx, short value);
     \brief Initializes array element at index idx from the given value. Conversion includes
            clipping at the boundary, or stringification.
     \param     idx     Zero-based index of the array element to write to.
     \param     value   The value to convert and copy from.
     */
    void fromValue(unsigned int idx, short value);
    /**
     \fn    void array_value_var::fromValue(unsigned int idx, char value);
     \brief Initializes array element at index idx from the given value. Conversion includes
            clipping at the boundary, or stringification.
     \param     idx     Zero-based index of the array element to write to.
     \param     value   The value to convert and copy from.
     */
    void fromValue(unsigned int idx, char value);
    /**
     \fn    void array_value_var::fromValue(unsigned int idx, unsigned long value);
     \brief Initializes array element at index idx from the given value. Conversion includes
            clipping at the boundary, or stringification.
     \param     idx     Zero-based index of the array element to write to.
     \param     value   The value to convert and copy from.
     */
    void fromValue(unsigned int idx, unsigned long value);
    /**
     \fn    void array_value_var::fromValue(unsigned int idx, unsigned int value);
     \brief Initializes array element at index idx from the given value. Conversion includes
            clipping at the boundary, or stringification.
     \param     idx     Zero-based index of the array element to write to.
     \param     value   The value to convert and copy from.
     */
    void fromValue(unsigned int idx, unsigned int value);
    /**
     \fn    void array_value_var::fromValue(unsigned int idx, unsigned short value);
     \brief Initializes array element at index idx from the given value. Conversion includes
            clipping at the boundary, or stringification.
     \param     idx     Zero-based index of the array element to write to.
     \param     value   The value to convert and copy from.
     */
    void fromValue(unsigned int idx, unsigned short value);
    /**
     \fn    void array_value_var::fromValue(unsigned int idx, unsigned char value);
     \brief Initializes array element at index idx from the given value. Conversion includes
            clipping at the boundary, or stringification.
     \param     idx     Zero-based index of the array element to write to.
     \param     value   The value to convert and copy from.
     */
    void fromValue(unsigned int idx, unsigned char value);
    /**
     \fn    void array_value_var::fromValue(unsigned int idx, float value);
     \brief Initializes array element at index idx from the given value. Conversion includes
            clipping at the boundary, or stringification.
     \param     idx     Zero-based index of the array element to write to.
     \param     value   The value to convert and copy from.
     */
    void fromValue(unsigned int idx, float value);
    /**
     \fn    void array_value_var::fromValue(unsigned int idx, double value);
     \brief Initializes array element at index idx from the given value. Conversion includes
            rounding and clipping at the boundary, or stringification.
     \param     idx     Zero-based index of the array element to write to.
     \param     value   The value to convert and copy from.
     */
    void fromValue(unsigned int idx, double value);
    /**
     \fn    void array_value_var::fromValue(unsigned int idx, const char* value);
     \brief Initializes array element at index idx from the given value. Conversion includes
            rounding and clipping at the boundary, or stringification.
     \param     idx     Zero-based index of the array element to write to.
     \param     value   The value to convert and copy from.
     */
    void fromValue(unsigned int idx, const char* value);
    /**
     \fn    void array_value_var::fromValue(unsigned int idx, long long value);
     \brief Initializes array element at index idx from the given value. Conversion includes
            clipping at the boundary, or stringification.
     \param     idx     Zero-based index of the array element to write to.
     \param     value   The value to convert and copy from.
     */
    void fromValue(unsigned int idx, long long value);
    /**
     \fn    void array_value_var::fromValue(unsigned int idx, unsigned long long value);
     \brief Initializes array element at index idx from the given value. Conversion includes
            clipping at the boundary, or stringification.
     \param     idx     Zero-based index of the array element to write to.
     \param     value   The value to convert and copy from.
     */
    void fromValue(unsigned int idx, unsigned long long value);
    /**
     \fn    int array_value_var::flip_extract(int len, const unsigned char* buf);
     \brief Extract its data from a byte stream with endian reversed according to width.
            Note that this only affects the variable length array data, the cardinal endian
            is still governed by cardinal width (negative to reverse). Like extract, setDirty
            will be called if there are changes, or notDirty if there are no changes.
     \param     len The length of the buffer in bytes.
     \param     buf The buffer.
     \returns   Remaining bytes in the buffer unprocessed.
     */
    int flip_extract(int len, const unsigned char* buf);
    /**
     \fn    void array_value_var::flip_output(outbuf& strm);
     \brief Output its data into a byte stream with the endian reversed according to width.
            Note that this only affects the variable length array data, the cardinal endian
            is still governed by cardinal width (negative to reverse).
     \param [in,out]    strm    The byte stream object where bytes are pushed into.
     */
    void flip_output(outbuf& strm);
};

#endif /* _AVL_VAR_H_ */

#ifndef _MPT_STRM_H_
#define _MPT_STRM_H_

#ifndef OSPACE
#if (defined(__GNUG__) && (__GNUG__ < 3)) || (defined(_HPUX_SOURCE) && !defined(__ia64)) || defined(OLD_STREAMS)
#  include <iostream.h>
# else
#  if defined(__osf__) && !defined(__USE_STD_IOSTREAM)
#    define __USE_STD_IOSTREAM
#  endif
#  include <istream>
#  include <ostream>
#  ifndef RWSTD_NO_NAMESPACE
using std::istream;
using std::ostream;
using std::ios;
using std::endl;
using std::flush;
#  endif
# endif

#endif

#endif  /* _MPT_STRM_H_ */

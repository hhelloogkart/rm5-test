/******************************************************************/
/* Copyright DSO National Laboratories 2019. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _CHKSUM_VAR_H_
#define _CHKSUM_VAR_H_

#include <chk_var.h>

class outbuf;

/**
 \class   chksum_var
 \brief   Checks the integrity using checksums.
 \details Implements various types of checksums, selected by using the enumeration.
          See the documentation for chk_var on the specification of the block of
          data that the checksum will be computed, and the location of the checksum
          in the packet.

           Enumeration        | Description
          --------------------|------------------------------
           CHKSUM_8           | 8-bit checksum
           CHKSUM_16          | 16-bit little endian checksum
           CHKSUM_16b         | 16-bit big endian checksum
           CHKSUM_32          | 32-bit little endian checksum
           CHKSUM_32b         | 32-bit big endian checksum
           CHKSUM_8_XSENS     | XSENS IMU checksum
           CHKSUM_8_XOR       | 8-bit XOR
           CHKSUM_8_XOR_NMEA  | 8-bit XOR converted to Hex-ASCII
           CHKSUM_8_TWO_COMP  | 8-bit XOR followed by 2's complement
           CHKSUM_8_SUM_INT   | Checksum using 32-bit integer and take LSB
           CHKSUM_16_IG500    | IG500 checksum
           CHKSUM_16_1553B    | MIL-STD-1553B checksum
           CHKSUM_16_FAS      | FAS checksum
           CHKSUM_16_XOR_FOG  | 16-bit XOR
           CHKSUM_16b_XOR_FOG | 16-bit XOR in big endian
           CHKSUM_16_UBLOX    | UBLOX

 */
class RM_EXPORT chksum_var : public chk_var
{
public:
    /**
     \enum  __chksumid
     \brief Values that represent various types of checksums
     */
    enum __chksumid
    {
        CHKSUM_8,   /* 0 */
        CHKSUM_16,  /* 1 */
        CHKSUM_16b, /* 2 */
        CHKSUM_32,  /* 3 */
        CHKSUM_32b, /* 4 */
        CHKSUM_8_XSENS,    /* 5 */
        CHKSUM_8_XOR,      /* 6 */
        CHKSUM_8_XOR_NMEA, /* 7 */
        CHKSUM_8_TWO_COMP, /* 8 */
        CHKSUM_8_COMP,     /* 9 */
        CHKSUM_8_SUM_INT,  /* 10 */
        CHKSUM_16_IG500,   /* 11 */
        CHKSUM_16_1553B,   /* 12 */
        CHKSUM_16_FAS,     /* 13 */
        CHKSUM_16_XOR_FOG, /* 14 */
        CHKSUM_16b_XOR_FOG, /* 15 */
        CHKSUM_16_UBLOX,    /* 16 */
        CHKSUM_16_CCITT,    /* 17 */
        CHKSUM_16_CCITT_FFFFSEED /* 18 */
    };
    /**
     \fn    chksum_var::chksum_var(int _hash, unsigned char chk_loc, unsigned char front_off, unsigned char back_off);
     \brief Constructor
     \param     _hash       The checksum to use.
     \param     chk_loc     (Optional) The check location. Default is 1, hash at rear of payload.
     \param     front_off   (Optional) The front offset. Default is 0.
     \param     back_off    (Optional) The back offset. Default is 0.
     */
    chksum_var(int _hash, unsigned char chk_loc, unsigned char front_off, unsigned char back_off);
    /**
     \fn    chksum_var::chksum_var(const chksum_var& right);
     \brief Copy constructor, copies the check properties and values of its children.
     \param     right   The object to be copied.
     */
    chksum_var(const chksum_var& right);
    /**
     \fn    const chksum_var& chksum_var::operator=(const chksum_var& right);
     \brief Assignment operator. Copies the values of its children.
     \param     right   The right object to be copied from.
     \returns   A reference of this object.
     */
    const chksum_var& operator=(const chksum_var& right);
    /**
     \fn    const mpt_var& chksum_var::operator=(const mpt_var& right);
     \brief Assignment operator. Copies the values of its children if casting succeeds.
     \param     right   The right object to be copied from.
     \returns   A reference of this object.
     */
    /*reimp*/ const mpt_var& operator=(const mpt_var& right);
    /**
     \fn    int chksum_var::checksum_size() const;
     \brief Checksum size in bytes.
     \returns   Size in bytes.
     */
    /*reimp*/ int checksum_size() const;
    /**
     \fn    void chksum_var::make_chksum(int len, const unsigned char* buf, outbuf& strm);
     \brief Makes a checksum given a buffer.
     \param          len     The length of the buffer.
     \param          buf     The buffer.
     \param [out]    strm    The output buffer for the generated checksum.
     */
    /*reimp*/ void make_chksum(int len, const unsigned char* buf, outbuf& strm);

private:
    chksum_var();

protected:
    int hashtype;   /*!< The selected checksum */
};

/**
 \class   v_chksum_var
 \brief   Template class to access the specific checksum algorithms
 \tparam  ht  The checksum enumeration to use
 \tparam  cl  The check location enumeration (see chk_var)
 \tparam  fo  The front offset (see chk_var)
 \tparam  bo  The back offset (see chk_var)
 */
template<int ht, unsigned int cl, unsigned int fo, unsigned int bo>
class v_chksum_var : public chksum_var
{
public:

    /**
     \fn    v_chksum_var::v_chksum_var()
     \brief Default constructor
     */
    v_chksum_var() : chksum_var(ht, cl, fo, bo)
    {
    }
    /**
     \fn    v_chksum_var::v_chksum_var(const v_chksum_var& right)
     \brief Copy constructor, copies the check properties and values of its children.
     \param     right   The object to be copied.
     */
    v_chksum_var(const v_chksum_var& right) : chksum_var(right)
    {
    }
    /**
     \fn    const v_chksum_var& v_chksum_var::operator=(const v_chksum_var& right)
     \brief Assignment operator. Copies the values of its children.
     \param     right   The right object to be copied from.
     \returns   A reference of this object.
     */
    const v_chksum_var& operator=(const v_chksum_var& right)
    {
        chksum_var::operator=(static_cast<const chksum_var&>(right));
        return *this;
    }
    /**
     \fn    const mpt_var& v_chksum_var::operator=(const mpt_var& right)
     \brief Assignment operator. Copies the values of its children if casting succeeds.
     \param     right   The right object to be copied from.
     \returns   A reference of this object.
     */
    /*reimp*/ const mpt_var& operator=(const mpt_var& right)
    {
        return chksum_var::operator=(right);
    }
};

typedef v_chksum_var<chksum_var::CHKSUM_8, 1, 0, 0> chksum8_var;
typedef v_chksum_var<chksum_var::CHKSUM_16, 1, 0, 0> chksum16le_var;
typedef v_chksum_var<chksum_var::CHKSUM_16b, 1, 0, 0> chksum16be_var;
typedef v_chksum_var<chksum_var::CHKSUM_32, 1, 0, 0> chksum32le_var;
typedef v_chksum_var<chksum_var::CHKSUM_32b, 1, 0, 0> chksum32be_var;
typedef v_chksum_var<chksum_var::CHKSUM_8_XSENS, 1, 0, 0> xsens_chksum8_var;
typedef v_chksum_var<chksum_var::CHKSUM_8_XOR, 1, 0, 0> xor_chksum8_var;
typedef v_chksum_var<chksum_var::CHKSUM_8_XOR_NMEA, 1, 0, 0> nmea_xor_chksum8_var;
typedef v_chksum_var<chksum_var::CHKSUM_8_XOR_NMEA, 1, 0, 0> xor_split_chksum16_var;
typedef v_chksum_var<chksum_var::CHKSUM_8_TWO_COMP, 1, 0, 0> two_complement_chksum8_var;
typedef v_chksum_var<chksum_var::CHKSUM_8_COMP, 1, 0, 0> complement_chksum8_var;
typedef v_chksum_var<chksum_var::CHKSUM_16_IG500, 1, 0, 0> ig500_chksum16_var;
typedef v_chksum_var<chksum_var::CHKSUM_16_1553B, 1, 0, 0> bus1553_chksum16le_var;
typedef v_chksum_var<chksum_var::CHKSUM_16_FAS, 1, 0, 0> fas_chksum16_var;
typedef v_chksum_var<chksum_var::CHKSUM_16_XOR_FOG, 1, 0, 0> xor_fogchksum16le_var;
typedef v_chksum_var<chksum_var::CHKSUM_16b_XOR_FOG, 1, 0, 0> xor_fogchksum16be_var;
typedef v_chksum_var<chksum_var::CHKSUM_16_UBLOX, 1, 0, 0> ublox_chksum_var;
typedef v_chksum_var<chksum_var::CHKSUM_16_CCITT, 1, 0, 0> ccitt_chksum16le_var;

typedef v_chksum_var<chksum_var::CHKSUM_8, 2, 0, 0> pre_chksum8_var;
typedef v_chksum_var<chksum_var::CHKSUM_16, 2, 0, 0> pre_chksum16le_var;
typedef v_chksum_var<chksum_var::CHKSUM_16b, 2, 0, 0> pre_chksum16be_var;
typedef v_chksum_var<chksum_var::CHKSUM_32, 2, 0, 0> pre_chksum32le_var;
typedef v_chksum_var<chksum_var::CHKSUM_32b, 2, 0, 0> pre_chksum32be_var;
typedef v_chksum_var<chksum_var::CHKSUM_8_SUM_INT, 2, 0, 0> pre_chksum32to8_var;
typedef v_chksum_var<chksum_var::CHKSUM_8_SUM_INT, 2, 3, 0> rdr_atp_chksum_var;

typedef v_chksum_var<chksum_var::CHKSUM_16, 4, 0, 0> chksum16le_atp_var;
typedef v_chksum_var<chksum_var::CHKSUM_32, 4, 0, 0> chksum32le_atp_var;

/**
 \def   EXTCHKSUMCONST(t, hash, loc, front, back)
 \brief A macro that should be placed in derived classes to implement the correct constructors,
        assignment operators and other methods. This should be used when deriving from chksum_var.
 \param     t       Class name of the derived class.
 \param     hash    The hash enumeration
 \param     loc     The check location.
 \param     front   The front offset.
 \param     back    The back offset.
 */
#define EXTCHKSUMCONST(t, hash, loc, front, back) t(const t& arg) : chksum_var(arg) \
{ \
        child_copy_constructor(arg); \
} \
t() : chksum_var(hash, loc, front, back) \
{ \
    pop(); \
} \
const t& operator=(const t& right) \
{ \
    chksum_var::operator=(right); \
    return *this; \
} \
const mpt_var& operator=(const mpt_var& right) \
{ \
    chksum_var::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)
/**
 \def   CHKSUMCONST(t, b)
 \brief A macro that should be placed in derived classes to implement the correct constructors,
        assignment operators and other methods. This should be used when deriving from
        v_chksum_var or one of the typedefs to v_chksum_var.
 \param     t   Class name of the derived class.
 \param     b   Baseclass, either v_chksum_var or one of its typedefs.
 */
#define CHKSUMCONST(t, b) t(const t& arg) : b(arg) \
{ \
        child_copy_constructor(arg); \
} \
t() : b() \
{ \
    pop(); \
} \
const t& operator=(const t& right) \
{ \
    b::operator=(right); \
    return *this; \
} \
const mpt_var& operator=(const mpt_var& right) \
{ \
    b::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)

#endif /* _CHKSUM_VAR_H_ */
/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _MSG_VAR_H_
#define _MSG_VAR_H_

#include <cplx_var.h>

//! Interface to message queue. This class represents a message, and its contained objects describe its data structure.
/*
   Messages can be for different fifo interfaces on the same application, the library
   will distinguish them.
   The messages that can be handled by msg_var fall into this structure:
       - message identifier to identify the packet [can be zero to indefinite length]
       - length of complete packet including header and checksums [optional, used for checking can be fixed length or variable length]
       - data payload
       - checksum (computed as part of payload)
   Parameters needed to be passed into msg_var:
       - [I] const char *mask = contain the mask for identifying the message ID
       - [M] const char *header = contain message ID
       - [I] unsigned short headerlen = length of the header, each msg interface must have headers of the same length
       - [M] unsigned short fixedmsglen = fixed message length from header to end of checksum in bytes, 0 if not-fixed
       - [I] unsigned short msglenoffset = if not fixed length, byte offset to recover the length of packet, 0 if N/A
       - [I] unsigned short msglenwidth = if not fixed length, number of bytes to recover the length of packet, 0 if N/A
       - [I] unsigned short msglenbias = the bias to apply to extracted length to convert it to number of bytes in the entire packet, 0 if N/A
       - [I] const char *trailer = contain the common message trailer; used only in packing output
       - [I] unsigned short trailerlen = if trailer is present the number of bytes in the trailer; used only in packing output
       - [I] unsigned short msgoffset = number of bytes from the beginning of msg packet to offset for extraction by child objects
    [I] - interface common parameters [M] - message specific parameters
   Packing of data for output:
       0. The buffer is locked to prevent receives
       1. Based on the required size of payload, the size of the unoffsetted header
          is added. Memory is taken from the common buffer and returned.
       2. Child objects are called to pack into the buffer
       3. The full header (both non-offsetted and offsetted, and the length will be packed,
          overwriting anything that is at that location
       4. The packet is sent to the message queue
       5. The buffer is unlocked
   Extraction of data:
       0. The buffer is locked to prevent writes
       1. Data is read from the message queue
       2. An attempt is made to match a header, if more than 1 packet is expected
       3. Extraction of the length from the message is attempted, if msglenxxx and fix length are set. A check with the fix length is performed. If failed it is discarded.
       4. Otherwise, it is offsetted and passed to extract.
 */
class msg_interf_typ;
class defaults;

class RM_EXPORT msg_var : public complex_var
{
public:
    friend class msg_interf_typ;

    msg_var(const char* hdr, unsigned short hdrlen, unsigned short fixmsglen,
            unsigned short msglenoffset, unsigned short msglenwidth, unsigned short msglenbias,
            const char* trailer, unsigned short trailerlen,
            unsigned short msgoffset, int interf_num = 0, const char* mask = 0);
    virtual void refreshRM();

    // Overloaded Virtual Functions
    virtual void setRMDirty();
    virtual bool isRMDirty() const;
    virtual mpt_var* getNext();
    virtual istream& operator>>(istream& s);
    virtual ostream& operator<<(ostream& s) const;

    // Static calls
    static void initialize(defaults* def=0);
    static void outgoing();
    static void incoming();
    static void set_msg_param(const char* host, int port, unsigned int timeout, int interf_num=0);
    static void set_msg_param(defaults* def);

protected:
    msg_var();
    void assistedConstruct(const char* hdr, unsigned short hdrlen, unsigned short fixmsglen,
        unsigned short msglenoffset, unsigned short msglenwidth, unsigned short msglenbias,
        const char* trailer, unsigned short trailerlen,
        unsigned short msgoffset, int interf_num = 0, const char* mask = 0);

    int msgidx;
    unsigned short fixedmsglen;
    bool dirty;
    msg_interf_typ* interf;

};

#define STD_MSGVAR_METHODS(t) \
const t& operator=(const t& right) \
{ \
    complex_var::operator=(right); \
    return *this; \
} \
const mpt_var& operator=(const mpt_var& right) \
{ \
    complex_var::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)

#define MSGCONST(t,hdr,hdrlen,fix,mlof,mlwi,mlbi,tr,trlen,msgof,ifnum,mask) t() : msg_var(hdr,hdrlen,fix,mlof,mlwi,mlbi,tr,trlen,msgof,ifnum,mask) \
{ \
    pop(); \
} \
t(mpt_var &link) : msg_var(hdr,hdrlen,fix,mlof,mlwi,mlbi,tr,trlen,msgof,ifnum,mask) \
{ \
    pop(); \
    *((*this)[0]) = link; \
} \
STD_MSGVAR_METHODS(t)

#define MSGCONST_ARRAY(t,hdr,hdrlen,fix,mlof,mlwi,mlbi,tr,trlen,msgof,ifnum,mask) t() \
{ \
    pop(); \
    assistedConstruct(hdr,hdrlen,fix,mlof,mlwi,mlbi,tr,trlen,msgof,ifnum+classIndex(),mask); \
} \
t(mpt_var &link) : msg_var(hdr,hdrlen,fix,mlof,mlwi,mlbi,tr,trlen,msgof,ifnum,mask) \
{ \
    pop(); \
    assistedConstruct(hdr,hdrlen,fix,mlof,mlwi,mlbi,tr,trlen,msgof,ifnum+classIndex(),mask); \
    *((*this)[0]) = link; \
} \
STD_MSGVAR_METHODS(t)

#endif /* _MSG_VAR_H_ */

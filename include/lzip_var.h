/******************************************************************/
/* Copyright DSO National Laboratories 2009. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _LZIP_VAR_H_
#define _LZIP_VAR_H_

#include <compress_var.h>

struct LZ_Encoder;
struct LZ_Decoder;

/**
 \class   lzip_var
 \brief   Compresses the serialized byte stream using the LZMA algorithm provided
          by the LZIP library by Antonio Diaz Diaz.
 */
class RM_EXPORT lzip_var : public compress_var
{
public:
    /**
     \fn    lzip_var::lzip_var();
     \brief Default constructor
     */
    lzip_var();
    /**
     \fn    lzip_var::lzip_var(const lzip_var& right);
     \brief Copy constructor
     \param     right   The object to copy.
     */
    lzip_var(const lzip_var& right);
    /**
     \fn    virtual const mpt_var& lzip_var::operator=(const mpt_var& right);
     \brief Assignment operator, virtual override so that it works without needing to upcast. It
            attempts to cast the argument to a complex_var and if successful, it will assign the
            values of each child in turn.
     \param     right   The object to be copied from, a deep copy will be made if the type is
                        correct. If the value is changed, setDirty() will be called.
     \returns   A const reference to this object.
     */
    virtual const mpt_var& operator=(const mpt_var& right);
    /**
     \fn    const lzip_var& lzip_var::operator=(const lzip_var& right);
     \brief Assignment operator
     \param     right   The object to be copied from, a deep copy will be made. If the value is
                        changed, setDirty() will be called.
     \returns   A const reference to this object.
     */
    const lzip_var& operator=(const lzip_var& right);

protected:
    /**
     \fn    virtual int lzip_var::compressionOverhead() const;
     \brief Returns the compression overhead so that it can be added to the worst
            case compressed buffer size. This depends on the dictionary size and
            extra overhead.
     \returns   Size in bytes.
     */
    virtual int compressionOverhead() const;
    /**
     \fn    virtual int lzip_var::compress(outbuf& buf, outbuf& rbuf);
     \brief Compress data from the raw buffer.
     \param [out]   buf     The buffer to store compressed data.
     \param [in]    rbuf    The raw data buffer.
     */
    virtual int compress(outbuf& buf, outbuf& rbuf);
    /**
     \fn    virtual int lzip_var::decompress(outbuf& dbuf, int len, const unsigned char* sbuf);
     \brief Decompresses the buffer passed in and writes to decompressed data buffer.
     \param [out]    dbuf    The decompressed data buffer to write to.
     \param          len     The length of the buffer passed in.
     \param          sbuf    The buffer, contains compressed data.
     \returns   The remaining bytes of the buffer that was left unused. If this equals
                len, it means that the decompression failed completely.
     */
    virtual int decompress(outbuf& dbuf, int len, const unsigned char* sbuf);

};

/**
 \def   _LZIPCONST(t)
 \brief A macro that used internally, do not use.
 \param     t   Class name of the derived class.
 */
#define _LZIPCONST(t) \
t(const t& arg) : lzip_var(arg) \
{ \
    child_copy_constructor(arg); \
    init(); \
} \
const t& operator=(const t& right) \
{ \
    lzip_var::operator=(right); \
    return *this; \
} \
const mpt_var& operator=(const mpt_var& right) \
{ \
    lzip_var::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)

 /**
 \def   LZIPCONST(t)
 \brief A macro that should be placed in derived classes to implement the correct constructors,
        assignment operators and other methods.
 \param     t   Class name of the derived class.
 */
#define LZIPCONST(t) t() : lzip_var() \
{ \
        pop(); \
        init(); \
} \
_LZIPCONST(t)

 /**
 \def   LZIPCONST2(t,clevel)
 \brief A macro that should be placed in derived classes to implement the correct constructors,
 assignment operators and other methods.
 \param     t      Class name of the derived class.
 \param     clevel Compression level, typically 0 to 9 where 0 is no compression, 9 is maximum
                   compression.
 */
#define LZIPCONST2(t, clevel) t() : lzip_var() \
{ \
        pop(); \
        init(); \
        setCompressionLevel(clevel); \
} \
_LZIPCONST(t)

#endif /* _LZIP_VAR_H_ */

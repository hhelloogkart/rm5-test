/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _RMDATA_VAR_H_
#define _RMDATA_VAR_H_

#include <rm_var.h>

//! A RM message that is not guarenteed to reach RM. These are usually used for exchanging periodic updates.
/*!
    It has the same RTTI as rm_var and should not introduce added properties
 */
class RM_EXPORT rm_data_var : public rm_var
{
public:
    rm_data_var(const char nm[], bool regflag);
    virtual bool write(outbuf& buf);
    virtual cache_typ* writebuffer(unsigned int id, size_t len);

private:
    rm_data_var();

};

#define RMDATACONST_1(t,mode) t(const char* nm) : rm_data_var(nm, mode) \
{ \
        pop(); \
} \
STD_RMVAR_METHODS(t)

#define RMDATACONST(t,nm,mode) t() : rm_data_var(nm, mode) \
{ \
        pop(); \
} \
t(mpt_var &link) : rm_data_var(nm, mode) \
{ \
    pop(); \
    *((*this)[0]) = link; \
} \
STD_RMVAR_METHODS(t)

#define RMDATACONST_ARRAY(t,nm,mode)  t() : rm_data_var("", mode) \
{ \
    pop(); \
    rm_minisprintf(rmmsgname, nm, classIndex()+1); \
} \
t(mpt_var &link) : rm_data_var("", mode) \
{ \
    pop(); \
    rm_minisprintf(rmmsgname, nm, classIndex()+1); \
    *((*this)[0]) = link; \
} \
STD_RMVAR_METHODS(t)

#define RMDATACONST_ARRAY2(t,nm,mode,div) t() : rm_data_var("", mode) \
{ \
    pop(); \
    int cli = classIndex(); \
    rm_minisprintf(rmmsgname, nm, (cli / div) + 1, (cli % div) + 1); \
} \
t(mpt_var &link) : rm_data_var("", mode) \
{ \
    pop(); \
    int cli = classIndex(); \
    rm_minisprintf(rmmsgname, nm, (cli / div) + 1, (cli % div) + 1); \
    *((*this)[0]) = link; \
} \
STD_RMVAR_METHODS(t)

#endif /* _RMDATA_VAR_H_ */

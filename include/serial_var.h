/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _SERIAL_VAR_H_
#define _SERIAL_VAR_H_

#include <cplx_var.h>

class serial_var;

//! Interface to serial. This class represents a serial message, and its contained objects describe its data structure.
/*
   Messages can be for different serial interfaces on the same application, the library
   will distinguish them.
   The messages that can be handled by serial_var fall into this structure:
       - common identifier to denote beginning of packet [can be zero to indefinite length]
       - message identifier to identify the packet [can be zero to indefinite length]
       - length of complete packet including header and checksums [optional, can be fixed length or based on trailer]
       - data payload
       - checksum (computed as part of payload)
       - common trailer [optional, common for each interface]
   Parameters needed to be passed into serial_var:
       - [I] const char *host = identifier for COM port, optional will use the default otherwise
       - [I] int port = baud rate for COM port, optional will use the default otherwise
       - [I] const char *mask = contain the mask for identifying the message ID
       - [I+M] const char *header = contain both the common and message ID, the library will guess
       - [I] unsigned short headerlen = length of the header, each serial interface must have headers of the same length
       - [M] unsigned short fixedmsglen = fixed message length from header to end of checksum in bytes, 0 if not-fixed
       - [I] unsigned short msglenoffset = if not fixed length, byte offset to recover the length of packet, 0 if N/A
       - [I] unsigned short msglenwidth = if not fixed length, number of bytes to recover the length of packet, 0 if N/A
       - [I] unsigned short msglenbias = the bias to apply to extracted length to convert it to number of bytes in the entire packet, 0 if N/A
       - [I] const char *trailer = contain the common message trailer
       - [I] unsigned short trailerlen = if trailer is present the number of bytes in the trailer
       - [I] unsigned short msgoffset = number of bytes from the beginning of serial packet to offset for extraction by child objects
    [I] - interface common parameters [M] - message specific parameters
   Packing of data for output:
       0. The buffer is locked to prevent receives
       1. Based on the required size of payload, the size of the unoffsetted header, and trailer
          is added. Memory is taken from the common buffer and returned.
       2. Child objects are called to pack into the buffer
       3. The full header (both non-offsetted and offsetted, the trailer, and the length will be packed,
          overwriting anything that is at that location
       4. The packet is sent to the serial port
       5. The buffer is unlocked
   Extraction of data:
       0. The buffer is locked to prevent writes
       1. Data is read from the serial port
       2. The data is searched for a suitable common header
       3. An attempt is made to match a specific header, if failed, the search in 2 is repeated until all buffer is processed
       4. Extraction of the length from the message is attempted, if msglenxxx are set. A check with the fix length is performed if set.
       5. Otherwise a fixed length is used, if set.
       6. Otherwise the trailer is used.
       7. If the complete packet has not been received, it is buffered.
       8. If a complete packet has been received, it is offsetted and passed to extract.
       9. If there is still leftover data, the search in 2 is repeated until all buffer is processed.
       10. If there is unprocessed data leftover, headerlen-1 is stored in the buffer, in case it contains the header.
 */
class serial_interf_typ;
class defaults;

class serial_helper_typ
{
public:
    serial_helper_typ(unsigned short msgoffset);
    virtual ~serial_helper_typ();
    virtual int calculateSize(int sz) const;
    virtual int write(char* buf, int sz);
    virtual int extract(char* buf, int left, serial_var* parent);
    static serial_helper_typ* provideHelper(unsigned short msgoffset);
    static void initHelpers();
    unsigned short msgOffset() const;
    serial_helper_typ* attach();
    void detach();
protected:
    // message offset to pass to child for extract
    unsigned short msgoffset;
    unsigned short attachers;
};

class RM_EXPORT serial_var : public complex_var
{
public:
    friend class serial_interf_typ;

    serial_var(const char* hdr, unsigned short hdrlen, unsigned short fixmsglen,
               unsigned short msglenoffset, unsigned short msglenwidth, unsigned short msglenbias,
               const char* trailer, unsigned short trailerlen, unsigned short msgoffset, int interf_num=0, bool ddle_flg = false, const char* mask=0, const char* mask_output=0);
    ~serial_var();
    bool isRMDirty() const;

    // Overloaded Virtual Functions
    /*reimp*/ void setRMDirty();
    /*reimp*/ mpt_var* getNext();
    /*reimp*/ istream& operator>>(istream& s);
    /*reimp*/ ostream& operator<<(ostream& s) const;

    // New Virtual Function
    virtual void refreshRM();

    // Static calls
    static void initialize(defaults* def=0);
    static void outgoing();
    static void incoming();
    static void set_serial_param(const char* host, int port, unsigned int timeout, int interf_num=0);
    static void set_serial_param(defaults* def);
    static void close();
    static void reconnect();

protected:
    serial_var(); // needed for serial_var arrays
    void assistedConstruct(const char* hdr, unsigned short hdrlen, unsigned short fixmsglen,
                           unsigned short msglenoffset, unsigned short msglenwidth, unsigned short msglenbias,
                           const char* trailer, unsigned short trailerlen, unsigned short msgoffset, int interf_num, bool ddle_flg, const char* mask, const char* mask_output);
    int assistedExtract(char* buf, int len);

    int msgidx;
    serial_interf_typ* interf;
    serial_helper_typ* helper;
    bool dirty;
};

inline bool serial_var::isRMDirty(void) const
{
    return dirty;
}

#define STD_SERIALVAR_METHODS(t) \
const t& operator=(const t& right) \
{ \
    complex_var::operator=(right); \
    return *this; \
} \
const mpt_var& operator=(const mpt_var& right) \
{ \
    complex_var::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)

#define SERIALCONST(t,hdr,hdrlen,fix,mlof,mlwi,mlbi,tlr,tlrlen,msgof,ifnum,ddle,mask,mask_output) t() : serial_var(hdr,hdrlen,fix,mlof,mlwi,mlbi,tlr,tlrlen,msgof,ifnum,ddle,mask,mask_output) \
{ \
    pop(); \
} \
t(mpt_var &link) : serial_var(hdr,hdrlen,fix,mlof,mlwi,mlbi,tlr,tlrlen,msgof,ifnum,ddle,mask,mask_output) \
{ \
    pop(); \
    *((*this)[0]) = link; \
} \
STD_SERIALVAR_METHODS(t)

#define SERIALCONST_ARRAY(t,hdr,hdrlen,fix,mlof,mlwi,mlbi,tlr,tlrlen,msgof,ifnum,ddle,mask,mask_output) t() \
{ \
    pop(); \
    assistedConstruct(hdr,hdrlen,fix,mlof,mlwi,mlbi,tlr,tlrlen,msgof,ifnum+classIndex(),ddle,mask,mask_output); \
} \
t(mpt_var &link) \
{ \
    pop(); \
    assistedConstruct(hdr,hdrlen,fix,mlof,mlwi,mlbi,tlr,tlrlen,msgof,ifnum+classIndex(),ddle,mask,mask_output); \
    *((*this)[0]) = link; \
} \
STD_SERIALVAR_METHODS(t)
#endif /* _SERIAL_VAR_H_ */

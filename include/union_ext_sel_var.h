/******************************************************************/
/* Copyright DSO National Laboratories 2009. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _UNION_EXT_SEL_VAR_H_
#define _UNION_EXT_SEL_VAR_H_

#include <deci_var.h>

//! The union_var with selector external of objects
class RM_EXPORT union_ext_sel_var : public decision_var
{
public:
    union_ext_sel_var(unsigned int (*selfx)(void), void (*writefx)(unsigned int) = 0);
    union_ext_sel_var(const union_ext_sel_var& right);

    virtual int extract(int len, const unsigned char* buf);
    virtual void output(outbuf& strm);

    virtual const mpt_var& operator=(const mpt_var& right);
    virtual bool operator==(const mpt_var& right) const;
    const union_ext_sel_var& operator=(const union_ext_sel_var& right);
    bool operator==(const union_ext_sel_var& right) const;

protected:
    unsigned int (* selector_fx)(void);
    void (* write_selector_fx)(unsigned int);

private:
    union_ext_sel_var();

};

#define UNIONEXTSELCONST(t,selfx,wrfx) t(const t& arg) : union_ext_sel_var(arg) \
{ \
    child_copy_constructor(arg); \
    init_child_observers(); \
} \
t() : union_ext_sel_var(selfx,wrfx) \
{ \
    pop(); \
    init_child_observers(); \
} \
const t& operator=(const t& right) \
{ \
    union_ext_sel_var::operator=(right); \
    return *this; \
} \
const mpt_var& operator=(const mpt_var& right) \
{ \
    union_ext_sel_var::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)

#endif /* _UNION_EXT_SEL_VAR_H_ */

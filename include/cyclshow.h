/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _CYCLSHOW_H_
#define _CYCLSHOW_H_

#include <delyshow.h>

//! This class does not attach to a observable. Instead it triggers periodically.
class RM_EXPORT cycle_show : public delay_show
{
public:
    cycle_show(unsigned int _sec, unsigned int _usec);
    virtual void setDirty(mpt_var*);
    bool getDirty();

};

#endif /* _CYCLSHOW_H_ */

/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _MPT_VAR_H_
#define _MPT_VAR_H_

#include <mpt_strm.h>
#include <mpt_show.h>

#ifdef FLIP
/**
 \fn    inline int flipint(int value)
 \brief Flip or swap the endian of an integer
 \param     value   The value.
 \returns   The value with endian swapped.
 */
inline int flipint(int value)
{
    char c, * p = reinterpret_cast< char* >(&value);
    c    = p[0];
    p[0] = p[3];
    p[3] = c;
    c    = p[1];
    p[1] = p[2];
    p[2] = c;
    return value;
}
#else
#define flipint(value) value
#endif

class mpt_var;
class complex_var;
class outbuf;
struct mpt_var_private;

/**
 \class    mpt_var
 \brief    The abstract base class for variables that are capable of self-extraction and packing
 \details  This is the abstract base class for data that is shared with other clients. Instances
           of subclasses provide the actual data that is shared. This class defines the interface
           for self-extraction and packing, based on the Composite design pattern. In addition,
           it allows the registration of callback objects derived from mpt_baseshow. These
           callback objects will be triggered when new data is received. The mpt_var-mpt_observer
           relationship is based on the Observer design pattern.
 */
class RM_EXPORT mpt_var
{
public:
    friend class complex_var;
    typedef int mpt_var_cl;
    /**
     \fn    virtual unsigned int mpt_var::rtti() const = 0;
     \brief Abstract function. Gets the library unique runtime type identifier, the value will be
            different for different classes derived from mpt_var, and derived template
            specializations.
     \returns   Integer that is unique for each mpt_var derived class.
     */
    virtual unsigned int rtti() const = 0;
    /**
     \fn    mpt_var::mpt_var();
     \brief Default constructor
     */
    mpt_var();
    /**
     \fn    virtual mpt_var::~mpt_var();
     \brief Destructor
     */
    virtual ~mpt_var();
    /**
     \fn    void mpt_var::addCallback(mpt_baseshow* show);
     \brief Install mpt_show object as a callback for notification when there is new data
     \param [in]    show    The mpt_show object to receive the notification.
     */
    void addCallback(mpt_baseshow* show);
    /**
     \fn    void mpt_var::removeCallback(mpt_baseshow* show);
     \brief Removes the mpt_show object as a callback
     \param [in]    show    The mpt_show object to be removed.
     */
    void removeCallback(mpt_baseshow* show);
    /**
     \fn    void mpt_var::setDirty();
     \brief Notifies setDirty to all callbacks in this object and all ancestors.
     */
    void setDirty();
    /**
     \fn    void mpt_var::notDirty();
     \brief Notifies notDirty to all callbacks in this object and all ancestors including foster
            parents.
     */
    void notDirty();
    /**
     \fn    void mpt_var::nullValue();
     \brief Resets the data to an invalid value. If the data was previously valid, setRMDirty will be
            called so that the packet can be scheduled to be sent out.
     */
    void nullValue();
    /**
     \fn    bool mpt_var::operator!=(const mpt_var& right) const
     \brief Inequality operator template for all derived classes.
     \param     right   The right object.
     \returns   True if the parameters are not considered equivalent.
     */
    bool operator!=(const mpt_var& right) const
    {
        return !operator==(right);
    }
    /**
     \fn    void mpt_var::installFosterParent(mpt_var* fparent);
     \brief Function to install foster parent. Foster parents allow a mpt_var to have more than one
            parent. They are used to facilitate link_var, so that the mpt_var that link_var links to
            will have the link_var's parent as a foster parent. This will allow setDirty, notDirty
            and setRMDirty calls to propagate up to both parents and foster parents.
     \param [in]    fparent The foster parent.
     */
    void installFosterParent(mpt_var* fparent);

protected:

    /**
     \fn    int mpt_var::classIndex();
     \brief Used by arrays to count which index of the array is the current object.
     \returns   The number of instances of objects of the same RTTI.
     */
    int classIndex();
    mpt_var* parent;    /*!< The parent. All setDirty, notDirty, setRMDirty calls are propagated to the parent */
    mpt_var_private* prvt;  /*!< Internal private data including callback lists and foster parent lists  */

public:

    /**
     \fn    virtual int mpt_var::extract(int len, const unsigned char* buf) = 0;
     \brief Abstract function. Extracts its data from a byte stream.
     \param     len The length.
     \param     buf The buffer.
     \returns   Number of bytes remaining that was not extracted.
     */
    virtual int extract(int len, const unsigned char* buf) = 0;
    /**
     \fn    virtual bool mpt_var::extract_zero(const unsigned char* buf);
     \brief Extracts the last byte from the byte stream. This is only for variable representations of
            bits, as it allows for extraction to proceed even when the number of bytes remaining
            drops to 0. If this returns false, the extraction will finish. Most implementations
            represent data in bytes and hence does not override this base class implementation which
            does nothing and returns false.
     \param     buf Pointer to the last byte.
     \returns   True if it was able to handle the data and if there are bits remaining. This base
                class implementation returns false.
     */
    virtual bool extract_zero(const unsigned char* buf);
    /**
     \fn    virtual int mpt_var::size() const = 0;
     \brief Abstract function. Returns the size in bytes that this object will need to serialize
            itself into a byte stream. For bit representation, the first object will return 1 and
            subsequent bit objects will return 0, so that it sums up correctly. Constant for fixed
            length data representations, while for variable length data representations such as
            array_var and flex_var, the value will depend on the cardinal. (Number of elements)
     \returns   Size in bytes.
     */
    virtual int size() const = 0;
    /**
     \fn    virtual void mpt_var::output(outbuf& strm) = 0;
     \brief Abstract function. Outputs data into a byte stream writing into outbuf. The number of
            bytes written must be equal to size().
     \param [in,out]    strm    Output buffer.
     */
    virtual void output(outbuf& strm) = 0;
    /**
     \fn    virtual bool mpt_var::isValid() const = 0;
     \brief Abstract function. Query if this object contains data that is valid
     \returns   True if valid, false if not.
     */
    virtual bool isValid() const = 0;
    /**
     \fn    virtual bool mpt_var::setInvalid() = 0;
     \brief Abstract function. Resets the data to invalid, which usually means zero length, or NaN or
            all bits filled to 1.
     \returns   True if it succeeds because previously the data was valid, false if the data was
                already invalid.
     */
    virtual bool setInvalid() = 0;
    /**
     \fn    virtual void mpt_var::setRMDirty();
     \brief Propagate or set the remote dirty flag to parents and foster parents. The remote dirty
            flag is an indicator for the message to be packed out at the next outgoing call.
     */
    virtual void setRMDirty();
    /**
     \fn    virtual bool mpt_var::isRMDirty() const;
     \brief Query or propagate to parents if this object's remote dirty flag is set.
     \returns   True if remote dirty, false if not.
     */
    virtual bool isRMDirty() const;
    /**
     \fn    virtual mpt_var* mpt_var::getNext() = 0;
     \brief Abstract function. Gets the next item if this item was created in an array, otherwise
            undefined.
     \returns   Pointer to next item in array.
     */
    virtual mpt_var* getNext() = 0;
    /**
     \fn    virtual const mpt_var& mpt_var::operator=(const mpt_var& right);
     \brief Assignment operator, virtual override so that it works without needing to upcast. This
            function needs to be overritten and it is an error for this to be called as the base
            class does not contain any data.
     \param     right   The object to be copied from, a deep copy will be made if the type is
                        correct. If the value is changed, setDirty will be called.
     \returns   A const reference to this object.
     */
    virtual const mpt_var& operator=(const mpt_var& right);
    /**
     \fn    virtual bool mpt_var::operator==(const mpt_var& right) const;
     \brief Equality operator, virtual override so that it works without needing to upcast. This base
            class implementation will test the right object for link_var and invert the comparision.
     \param     right   The right object to be compared with.
     \returns   True if the parameters are considered equivalent.
     */
    virtual bool operator==(const mpt_var& right) const;
    /**
     \fn    virtual istream& mpt_var::operator>>(istream& s) = 0;
     \brief Abstract function. Stream extraction operator that reads its own data. This function is
            called by the global extraction operator.
     \param [in]    s   an istream to process.
     \returns   The input stream.
     */
    virtual istream& operator>>(istream& s) = 0;
    /**
     \fn    virtual ostream& mpt_var::operator<<(ostream& s) = 0;
     \brief Abstract function. Stream insertion operator that writes its own data.
            This function is called by the global insertion operator.
     \param [in]    s   an ostream to process.
     \returns   The output stream.
     */
    virtual ostream& operator<<(ostream& s) const = 0;  /*!< . */
    /**
     \fn    virtual void mpt_var::refresh();
     \brief Calls all mpt_show callbacks to perform operator() processing.
     */
    virtual void refresh();
    /**
     \fn    void mpt_var::push();
     \brief Push this object into the top of create stack so that any subsequent mpt_var object
            created will recognise this as its parent. This should only be called by complex_var
            derived classes.
     */
    void push();
    /**
     \fn    void mpt_var::pop();
     \brief Removes the top of of create stack. An empty create stack would cause subsequent mpt_var
            objects to have no parent, ie. they become top-level objects.
     */
    void pop();
    /**
     \fn    static bool mpt_var::check_stack();
     \brief Checks the create stack to see if it is empty. This must always be the case when in the
            normal context of your program as the create stack will be empty when the instantiation
            of mpt_vars have completed. If this returns true, it is an indication that a derived
            class failed to call pop().
     \returns   True if the create stack is empty.
     */
    static bool check_stack();

};
/**
 \fn    extern RM_EXPORT bool isValid(const double& data);
 \brief Check if the double value is not a NaN
 \param     data    The data to be checked.
 \returns   True if not NaN.
 */
extern RM_EXPORT bool isValid(const double& data);
/**
 \fn    extern RM_EXPORT bool isValid(const float& data);
 \brief Check if the float value is not a NaN
 \param     data    The data to be checked.
 \returns   True if not NaN.
 */
extern RM_EXPORT bool isValid(const float& data);
/**
 \fn    template <class C > bool isValid(const C& parameter1)
 \brief Check if integer is valid; but all integers are always valid.
 \tparam    C   Integer types (eg. int, short, char, unsigned long)
 \param     data Ignored.
 \returns   True at all times.
 */
template< class C >
bool isValid(const C&)
{
    return true;
}
/**
 \fn    extern RM_EXPORT istream& operator>>(istream& s, mpt_var& right);
 \brief Global stream extraction operator supporting mpt_var. Will redirect to the class
        virtual extraction operator function.
 \param [in]    s       an istream to process.
 \param [out]    right   The mpt_var object.
 \returns   The input stream.
 */
extern RM_EXPORT istream& operator>>(istream& s, mpt_var& right);
/**
 \fn    extern RM_EXPORT ostream& operator<<(ostream& s, const mpt_var& right);
 \brief Global stream insertion operator supporting mpt_var. Will redirect to the class
        virtual insertion operator function.
 \param [in]    s       an ostream to process.
 \param [in]    right   The const mpt_var object.
 \returns   The output stream.
 */
extern RM_EXPORT ostream& operator<<(ostream& s, const mpt_var& right);

#endif /* _MPT_VAR_H_ */

/******************************************************************/
/* Copyright DSO National Laboratories 2008. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _BIT_VAR_H_
#define _BIT_VAR_H_

#ifdef _WIN32
#pragma warning(disable : 4146)
#endif

#include <gen_var.h>

/**
 \class   vbit_var
 \brief   The base class for all variables that process data in bits.
 \details This class implements most of the functions for bit manipulations
          allowing it to operate at the bit level, and across byte boundaries.
          When placed beside siblings that are also derived from vbit_var,
          it is able to detect their presence to offset its bit location
          with respect from the previous sibling by using rtti() to identify
          it. This allows bitfields, such as bit_var to work correctly with
          decode_bit_var side by side. Note that rvbit_var derived classes
          have a different rtti() signature as placing vbit_var and rvbit_var
          side by side does not make sense, so the new variable will immediately
          start on a byte boundary.
 */
class RM_EXPORT vbit_var : public generic_var<unsigned int>
{
protected:
    unsigned char offset;   /*!< The bit offset from the last byte boundary */
    unsigned char bit_size; /*!< Number of bits used by this variable in the byte stream */
    unsigned char use_bit;  /*!< Number of usable bits per byte */
    unsigned int sign_ext;  /*!< A bitfield used to sign extend if the most significant bit is 1 */
public:

    /**
     \fn    virtual unsigned int vbit_var::rtti() const;
     \brief Gets the library unique runtime type identifier, the value will be the same
            for all situations where bits are to be extracted in the normal bit order.
     \returns   Integer that is unique for normal bit order variables.
     */
    virtual unsigned int rtti() const;
    /**
     \fn    vbit_var::vbit_var(int sz, int bz = 8);
     \brief Constructor, it will query the sibling before it to determine its bit offset.
     \param     sz  The bit size.
     \param     bz  (Optional) The number of usable bits in a byte.
     */
    vbit_var(int sz, int bz = 8);
    /**
     \fn    vbit_var::vbit_var(const vbit_var& cpy);
     \brief Copy constructor, the bit size and usable bits in a byte will be copied along
            with the value. It will also query the sibling before it to determine its bit
            offset.
     \param     cpy The copy.
     */
    vbit_var(const vbit_var& cpy);
    /**
     \fn    virtual int vbit_var::extract(int buflen, const unsigned char* buf);
     \brief Extracts its data from a byte stream. If the previous object is also a vbit_var
            and it has a non-zero bit offset, it will take its data from the byte before
            the start of the buffer. It is entirely possible that it returns the remaining
            bytes that is equal to the buffer length passed in as it may only consume bits
            from the previous byte. As long is it consumes any bits from the bytes from
            the start of the buffer, it will return a remaining bytes that is less. This is
            so that if its following sibling is not a vbit_var, it will resume extraction
            on the next byte boundary.
     \param     buflen  The buffer length in bytes.
     \param     buf     The buffer.
     \returns   Remaining bytes in the buffer that was not extracted.
     */
    virtual int extract(int buflen, const unsigned char* buf);
    /**
     \fn    virtual bool vbit_var::extract_zero(const unsigned char* buf);
     \brief Extracts the last byte from the byte stream if the bit offset is non zero.
            Otherwise it does nothing and returns false.
            Returning true allows the possibility of the next sibling, which
            could also be a vbit_var, extracting something from the last byte
            as well.
     \param     buf The buffer.
     \returns   True if it succeeds, false if it fails.
     */
    virtual bool extract_zero(const unsigned char* buf);
    /**
     \fn    virtual int vbit_var::size() const;
     \brief  Returns the size in bytes that this object will need to serialize itself into
             a byte stream. As long as this object inpinges into new bytes it will return
             the new bytes that it impinges. Otherwise it will return 0, so that it sums
             up correctly.
     \returns   Size in bytes.
     */
    virtual int size() const;
    /**
     \fn    virtual void vbit_var::output(outbuf& strm);
     \brief Outputs its data into a byte stream writing into outbuf. If its bit offset is
            non zero, it will pull out the last byte in the stream and add its bits into
            it. New bytes will be pushed into the stream whenever this object starts a
            new byte.
     \param [out]    strm    Output buffer.
     */
    virtual void output(outbuf& strm);
    /**
     \fn    virtual mpt_var* vbit_var::getNext();
     \brief Gets the next item if this item was created in an array, otherwise undefined.
     \returns   Pointer to next item in array.
     */
    virtual mpt_var* getNext();
    /**
     \fn    unsigned int vbit_var::operator=(unsigned int right);
     \brief Assignment operator. There are no checks to see if the value is within the
            range for the bit size.
     \param     right   The value to be copied.
     \returns   A reference of this object.
     */
    unsigned int operator=(unsigned int right);
    /**
     \fn    const vbit_var& vbit_var::operator=(const vbit_var& right);
     \brief Assignment operator. Only the value is copied, other parameters such as
            bit size, usable bits per byte are not copied. There are no checks to see
            if the value is within the range for the bit size.
     \param     right   The object to be copied.
     \returns   A reference of this object.
     */
    const vbit_var& operator=(const vbit_var& right);
    /**
     \fn    virtual const mpt_var& vbit_var::operator=(const mpt_var& right);
     \brief Assignment operator. Value will only be copied if it can be transformed
            into an unsigned int. There are no checks to see if the value is within
            range for the bit size.
     \param     right   The object to be copied.
     \returns   A reference of this object.
     */
    virtual const mpt_var& operator=(const mpt_var& right);
    /**
     \fn    bool vbit_var::operator==(unsigned int right) const;
     \brief Equality operator.
     \param     right   The right value.
     \returns   True if both values are equal.
     */
    bool operator==(unsigned int right) const;
    /**
     \fn    bool vbit_var::operator==(const vbit_var& right) const;
     \brief Equality operator.
     \param     right   The right object.
     \returns   True if both values are equal. This does not compare other parameters
                such as bit size.
     */
    bool operator==(const vbit_var& right) const;
    /**
     \fn    virtual bool vbit_var::operator==(const mpt_var& right) const;
     \brief Equality operator.
     \param     right   The right object.
     \returns   True if the parameters are considered equivalent. False if not equal
                or it is not possible to transform the right object into an unsigned
                int.
     */
    virtual bool operator==(const mpt_var& right) const;
    /**
     \fn    operator unsigned vbit_var::int() const;
     \brief Casting operator to get the value.
     \returns   Value as an unsigned int.
     */
    operator unsigned int() const;
    /**
     \fn    unsigned char vbit_var::get_offset() const
     \brief Gets the bit offset
     \returns   The offset.
     */
    unsigned char get_offset() const
    {
        return offset;
    }
    /**
     \fn    unsigned char vbit_var::get_bit_size() const
     \brief Gets bit size
     \returns   The bit size.
     */
    unsigned char get_bit_size() const
    {
        return bit_size;
    }
private:
    vbit_var();
};

/**
 \class   rvbit_var
 \brief   Derived class from vbit_var to handle bit data in reverse bit order
 \details This class functions in the same way as vbit_var to handle bit data,
          but reading and writing data in reverse bit order to the byte stream.
          It uses a different rtti() from vbit_var so that if placed beside
          a vbit_var, data processing will immediately start at the byte boundary.
*/
class RM_EXPORT rvbit_var : public vbit_var
{
public:

    /**
     \fn    virtual unsigned int rvbit_var::rtti() const;
     \brief Gets the library unique runtime type identifier, the value will be the same
            for all situations where bits are to be extracted in the reverse bit order.
     \returns   Integer that is unique for normal bit order variables.
     */
    virtual unsigned int rtti() const;
    /**
     \fn    rvbit_var::rvbit_var(int sz, int bz = 8);
     \brief Constructor, it will query the sibling before it to determine its bit offset.
     \param     sz  The bit size.
     \param     bz  (Optional) The number of usable bits in a byte.
     */
    rvbit_var(int sz, int bz = 8);
    /**
     \fn    rvbit_var::rvbit_var(const rvbit_var& cpy);
     \brief Copy constructor, the bit size and usable bits in a byte will be copied along
            with the value. It will also query the sibling before it to determine its bit
            offset.
     \param     cpy The copy.
     */
    rvbit_var(const rvbit_var& cpy);
    /**
     \fn    virtual int rvbit_var::extract(int buflen, const unsigned char* buf);
     \brief Extracts its data from a byte stream. If the previous object is also a rvbit_var
            and it has a non-zero bit offset, it will take its data from the byte before
            the start of the buffer. It is entirely possible that it returns the remaining
            bytes that is equal to the buffer length passed in as it may only consume bits
            from the previous byte. As long is it consumes any bits from the bytes from
            the start of the buffer, it will return a remaining bytes that is less. This is
            so that if its following sibling is not a rvbit_var, it will resume extraction
            on the next byte boundary.
     \param     buflen  The buffer length in bytes.
     \param     buf     The buffer.
     \returns   Remaining bytes in the buffer that was not extracted.
     */
    virtual int extract(int buflen, const unsigned char* buf);
    /**
     \fn    virtual void rvbit_var::output(outbuf& strm);
          \brief Outputs its data into a byte stream writing into outbuf. If its bit offset is
            non zero, it will pull out the last byte in the stream and add its bits into
            it. New bytes will be pushed into the stream whenever this object starts a
            new byte.
     \param [out]    strm    Output buffer.
     */
    virtual void output(outbuf& strm);
private:

    /**
     \fn    void rvbit_var::reverse_byte(unsigned char* byte);
     \brief Reverse the bit order in a byte
     \param [in,out]    byte    The byte to be bit reversed. The operation will be performed
                                in place.
     */
    void reverse_byte(unsigned char* byte);
    /**
     \fn    void rvbit_var::reverse_uint(unsigned int* ndata);
     \brief Reverse an unsigned int. This is used on the native value property
            because this class is derived from generic_var<unsigned int>.
     \param [in,out]    ndata   The unsigned int to be big reversed. The operation will be
                                performed in place.
     */
    void reverse_uint(unsigned int* ndata);
};

/**
 \class bit_var
 \brief This template class is used to handle bit fields in the normal bit order. It extends
        generic_var<unsigned int>. sz cannot be more than 32.
 \tparam    sz    The number of bits.
*/
template<int sz>
class bit_var : public vbit_var
{
public:

    /**
     \fn    bit_var::bit_var()
     \brief Default constructor
     */
    bit_var() : vbit_var(sz)
    {
    }
    /**
     \fn    unsigned int bit_var::operator=(unsigned int right)
     \brief Assignment operator. There are no checks to see if the value is within the
            range for the bit size.
     \param     right   The value to be copied.
     \returns   A reference of this object.
     */
    unsigned int operator=(unsigned int right)
    {
        return vbit_var::operator=(right);
    }
    /**
     \fn    const bit_var<sz>& bit_var::operator=(const bit_var<sz>& right)
     \brief Assignment operator. Only the value is copied, other parameters such as
            bit size, usable bits per byte are not copied. There are no checks to see
            if the value is within the range for the bit size.
     \param     right   The object to be copied.
     \returns   A reference of this object.
     */
    const bit_var<sz>& operator=(const bit_var<sz>& right)
    {
        vbit_var::operator=(right);
        return *this;
    }
    /**
     \fn    virtual const mpt_var& bit_var::operator=(const mpt_var& right)
     \brief Assignment operator. Value will only be copied if it can be transformed
            into an unsigned int. There are no checks to see if the value is within
            range for the bit size.
     \param     right   The object to be copied.
     \returns   A reference of this object.
     */
    virtual const mpt_var& operator=(const mpt_var& right)
    {
        return vbit_var::operator=(right);
    }
};

/**
 \class r_bit_var
 \brief This template class is used to handle bit fields in the reverse bit order. It extends
        generic_var<unsigned int>. sz cannot be more than 32.
 \tparam    sz    The number of bits.
*/
template<int sz>
class r_bit_var : public rvbit_var
{
public:

    /**
     \fn    r_bit_var::r_bit_var()
     \brief Default constructor
     */
    r_bit_var() : rvbit_var(sz)
    {
    }
    /**
     \fn    unsigned int r_bit_var::operator=(unsigned int right)
     \brief Assignment operator. There are no checks to see if the value is within the
            range for the bit size.
     \param     right   The value to be copied.
     \returns   A reference of this object.
     */
    unsigned int operator=(unsigned int right)
    {
        return vbit_var::operator=(right);
    }
    /**
     \fn    const r_bit_var<sz>& r_bit_var::operator=(const r_bit_var<sz>& right)
     \brief Assignment operator. Only the value is copied, other parameters such as
            bit size, usable bits per byte are not copied. There are no checks to see
            if the value is within the range for the bit size.
     \param     right   The object to be copied.
     \returns   A reference of this object.
     */
    const r_bit_var<sz>& operator=(const r_bit_var<sz>& right)
    {
        vbit_var::operator=(right);
        return *this;
    }
    /**
     \fn    virtual const mpt_var& r_bit_var::operator=(const mpt_var& right)
     \brief Assignment operator. Value will only be copied if it can be transformed
            into an unsigned int. There are no checks to see if the value is within
            range for the bit size.
     \param     right   The object to be copied.
     \returns   A reference of this object.
     */
    virtual const mpt_var& operator=(const mpt_var& right)
    {
        return vbit_var::operator=(right);
    }
};

/**
 \class r_ext_bit_var
 \brief This template class is used to handle bit fields in the normal bit order with added
        option of specifying the usable bits in a byte. This is for situations where the data bits
        is not 8, for example because 1 bit has been used for parity. It extends generic_var<unsigned int>.
        sz cannot be more than 32, and bz cannot be more than 8.
 \tparam    sz    The number of bits.
 \tparam    bz    The number of data bits in a byte.
*/
template<int sz, int bz>
class ext_bit_var : public vbit_var
{
public:

    /**
     \fn    ext_bit_var::ext_bit_var()
     \brief Default constructor
     */
    ext_bit_var() : vbit_var(sz, bz)
    {
    }
    /**
     \fn    unsigned int ext_bit_var::operator=(unsigned int right)
     \brief Assignment operator. There are no checks to see if the value is within the
            range for the bit size.
     \param     right   The value to be copied.
     \returns   A reference of this object.
     */
    unsigned int operator=(unsigned int right)
    {
        return vbit_var::operator=(right);
    }
    /**
     \fn    const ext_bit_var<sz, bz>& ext_bit_var::operator=(const ext_bit_var<sz, bz>& right)
     \brief Assignment operator. Only the value is copied, other parameters such as
            bit size, usable bits per byte are not copied. There are no checks to see
            if the value is within the range for the bit size.
     \param     right   The object to be copied.
     \returns   A reference of this object.
     */
    const ext_bit_var<sz, bz>& operator=(const ext_bit_var<sz, bz>& right)
    {
        vbit_var::operator=(right);
        return *this;
    }
    /**
     \fn    virtual const mpt_var& ext_bit_var::operator=(const mpt_var& right)
     \brief Assignment operator. Value will only be copied if it can be transformed
            into an unsigned int. There are no checks to see if the value is within
            range for the bit size.
     \param     right   The object to be copied.
     \returns   A reference of this object.
     */
    virtual const mpt_var& operator=(const mpt_var& right)
    {
        return vbit_var::operator=(right);
    }
};

/**
 \class r_ext_bit_var
 \brief This template class is used to handle bit fields in the reverse bit order with added
        option of specifying the usable bits in a byte. This is for situations where the data bits
        is not 8, for example because 1 bit has been used for parity. It extends generic_var<unsigned int>.
        sz cannot be more than 32, and bz cannot be more than 8.
 \tparam    sz    The number of bits.
 \tparam    bz    The number of data bits in a byte.
*/
template<int sz, int bz>
class r_ext_bit_var : public rvbit_var
{
public:

    /**
     \fn    r_ext_bit_var::r_ext_bit_var()
     \brief Default constructor
     */
    r_ext_bit_var() : rvbit_var(sz, bz)
    {
    }
    /**
     \fn    unsigned int r_ext_bit_var::operator=(unsigned int right)
     \brief Assignment operator. There are no checks to see if the value is within the
            range for the bit size.
     \param     right   The value to be copied.
     \returns   A reference of this object.
     */
    unsigned int operator=(unsigned int right)
    {
        return vbit_var::operator=(right);
    }
    /**
     \fn    const r_ext_bit_var<sz, bz>& r_ext_bit_var::operator=(const r_ext_bit_var<sz, bz>& right)
     \brief Assignment operator. Only the value is copied, other parameters such as
            bit size, usable bits per byte are not copied. There are no checks to see
            if the value is within the range for the bit size.
     \param     right   The object to be copied.
     \returns   A reference of this object.
     */
    const r_ext_bit_var<sz, bz>& operator=(const r_ext_bit_var<sz, bz>& right)
    {
        vbit_var::operator=(right.data);
        return *this;
    }
    /**
     \fn    virtual const mpt_var& r_ext_bit_var::operator=(const mpt_var& right)
     \brief Assignment operator. Value will only be copied if it can be transformed
            into an unsigned int. There are no checks to see if the value is within
            range for the bit size.
     \param     right   The object to be copied.
     \returns   A reference of this object.
     */
    virtual const mpt_var& operator=(const mpt_var& right)
    {
        return vbit_var::operator=(right);
    }
};

#endif /* _BIT_VAR_H_ */

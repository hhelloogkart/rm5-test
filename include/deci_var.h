/******************************************************************/
/* Copyright DSO National Laboratories 2009. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _DECISION_VAR_H_
#define _DECISION_VAR_H_

#include <cplx_var.h>

class decision_var_private_show;

//! The composite class that acts as a container for multiple mpt_var objects, with only one active
class RM_EXPORT decision_var : public complex_var
{
public:
    decision_var();
    decision_var(const decision_var& right);
    virtual ~decision_var();

    virtual int extract(int len, const unsigned char* buf);
    virtual void output(outbuf& strm);
    virtual int size() const;
    virtual bool setInvalid();

    virtual const mpt_var& operator=(const mpt_var& right);
    virtual bool operator==(const mpt_var& right) const;
    const decision_var& operator=(const decision_var& right);
    bool operator==(const decision_var& right) const;

    unsigned int getOutputChild() const;
    void init_child_observers();

protected:
    int active;
    decision_var_private_show* shw;
    mutable int lastout;
};

#define DECISIONCONST(t) t(const t& arg) : decision_var(arg) \
{ \
        child_copy_constructor(arg); \
        init_child_observers(); \
} \
t() : decision_var() \
{ \
    pop(); \
    init_child_observers(); \
} \
const t& operator=(const t& right) \
{ \
    decision_var::operator=(right); \
    return *this; \
} \
const mpt_var& operator=(const mpt_var& right) \
{ \
    decision_var::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)

#endif /* _DECISION_VAR_H_ */

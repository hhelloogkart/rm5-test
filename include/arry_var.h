/******************************************************************/
/* Copyright DSO National Laboratories 2011. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _ARRAY_VAR_H_
#define _ARRAY_VAR_H_

#include <avl_var.h>
#include <gen_var.h>
#include <outbuf.h>
#include <string.h>
/**
 \class array_var
 \brief A variable length array of basic data type (plain old data, POD).
 \tparam    C   Type of the POD, eg. int, short, float, etc.
 */
template<class C, class D = typename rm_internal::pod_check<C>::ok>
class array_var : public array_value_var
{
public:
    /**
     \fn    virtual unsigned int array_var::rtti() const
     \brief Gets the library unique runtime type identifier, the value will be different for
            different basic data types
     \returns   Integer that is unique for each mpt_var derived class.
     */
    virtual unsigned int rtti() const
    {
        return rm_hash_string("array_var") ^ var_rtti(C()); /* parasoft-suppress  JSF-185 "Tool error" */
    }
    /**
     \fn    array_var::array_var()
     \brief Default constructor, will initialize it to invalid
     */
    array_var()
    {
    }
    /**
     \fn    array_var::array_var(const array_var<C>& cpy)
     \brief Copy constructor, will make a deep copy of the contents
     \param     cpy The copy.
     */
    array_var(const array_var<C>& cpy) : array_value_var(cpy)
    {
    }
    /**
     \fn    operator const array_var::C*() const
     \brief Gets the constant pointer to the beginning of data, returns null if there is no data
     \returns   Const pointer to the start of data, cast to the basic data type.
     */
    operator const C*() const
    {
        return static_cast<const C*>(this->array_value_var::operator const void* ());
    }
    /**
     \fn    operator array_var::C*()
     \brief C* Gets the non-constant pointer to the beginning of data. Call setDirty unless there is
            no data. Returns null if there is no data
     \returns   Pointer to the start of data, cast to the basic data type.
     */
    operator C*()
    {
        return static_cast<C*>(this->array_value_var::operator void* ());
    }
    /**
     \fn    virtual int array_var::width() const
     \brief Gets the width in bytes of the basic data type
     \returns   Width in bytes.
     */
    virtual int width() const
    {
        return sizeof(C);
    }
    /**
     \fn    virtual mpt_var* array_var::getNext()
     \brief Gets the next item if this item was created in an array, otherwise undefined.
     \returns   Pointer to next item in array.
     */
    virtual mpt_var* getNext()
    {
        return this+1;
    }
    /**
     \fn    const array_var<C>& array_var::operator=(const array_var<C>& right)
     \brief Assignment operator, virtual override so that it works without needing to upcast.
            If the value is changed, setDirty() and setRMDirty() will be called, otherwise
            notDirty() will be called.
     \param     right   The object to be copied from, a deep copy will be made.
     \returns   A const reference to this object.
     */
    const array_var<C>& operator=(const array_var<C>& right)
    {
        return static_cast<const array_var<C>&>(array_value_var::operator=(right));
    }
    /**
     \fn    virtual const mpt_var& array_var::operator=(const mpt_var& right)
     \brief Assignment operator, virtual override so that it works without needing to upcast
     \param     right   The object to be copied from, a deep copy will be made if the type is
                        correct. If the value is changed, setDirty will be called.
     \returns   A const reference to this object.
     */
    virtual const mpt_var& operator=(const mpt_var& right)
    {
        return array_value_var::operator=(right);
    }
    /**
     \fn    C array_var::operator[](int idx) const
     \brief Array indexer operator
     \param     idx Zero-based index of the array.
     \returns   The indexed value.
     */
    C operator[](int idx) const
    {
        C value;
        array_value_var::toValue(idx, value);
        return value;
    }
    /**
     \fn    virtual char array_var::to_char(unsigned int idx) const
     \brief Returns the value at the given index. Upper and lower bound checks, rounding and limits
            clamping will be applied.
     \param     idx Zero-based index.
     \returns   Value at the index as a char or zero if the index is out of bounds.
     */
    virtual char to_char(unsigned int idx) const
    {
        char ret = 0;
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
#ifdef FLIP
            r_generic_var<C> tmp;
#else
            generic_var<C> tmp;
#endif

            tmp.extract(sz-offset, reinterpret_cast<const unsigned char*>(this->data)+offset);
            ret = tmp.to_char();
        }
        return ret;
    }
    /**
     \fn    virtual unsigned char array_var::to_unsigned_char(unsigned int idx) const
     \brief Returns the value at the given index. Upper and lower bound checks, rounding and limits
            clamping will be applied.
     \param     idx Zero-based index.
     \returns   Value at the index as an unsigned char or zero if the index is out of bounds.
     */
    virtual unsigned char to_unsigned_char(unsigned int idx) const
    {
        unsigned char ret = 0;
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
#ifdef FLIP
            r_generic_var<C> tmp;
#else
            generic_var<C> tmp;
#endif
            tmp.extract(sz-offset, reinterpret_cast<const unsigned char*>(this->data)+offset);
            ret = tmp.to_unsigned_char();
        }
        return ret;
    }
    /**
     \fn    virtual short array_var::to_short(unsigned int idx) const
     \brief Returns the value at the given index. Upper and lower bound checks, rounding and limits
            clamping will be applied.
     \param     idx Zero-based index.
     \returns   Value at the index as a short or zero if the index is out of bounds.
     */
    virtual short to_short(unsigned int idx) const
    {
        short ret = 0;
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
#ifdef FLIP
            r_generic_var<C> tmp;
#else
            generic_var<C> tmp;
#endif
            tmp.extract(sz-offset, reinterpret_cast<const unsigned char*>(this->data)+offset);
            ret = tmp.to_short();
        }
        return ret;
    }
    /**
     \fn    virtual unsigned short array_var::to_unsigned_short(unsigned int idx) const
     \brief Returns the value at the given index. Upper and lower bound checks, rounding and limits
            clamping will be applied.
     \param     idx Zero-based index.
     \returns   Value at the index as a unsigned short or zero if the index is out of bounds.
     */
    virtual unsigned short to_unsigned_short(unsigned int idx) const
    {
        unsigned short ret = 0;
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
#ifdef FLIP
            r_generic_var<C> tmp;
#else
            generic_var<C> tmp;
#endif
            tmp.extract(sz-offset, reinterpret_cast<const unsigned char*>(this->data)+offset);
            ret = tmp.to_unsigned_short();
        }
        return ret;
    }
    /**
     \fn    virtual int array_var::to_int(unsigned int idx) const
     \brief Returns the value at the given index. Upper and lower bound checks, rounding and limits
            clamping will be applied.
     \param     idx Zero-based index.
     \returns   Value at the index as an int or zero if the index is out of bounds.
     */
    virtual int to_int(unsigned int idx) const
    {
        int ret = 0;
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
#ifdef FLIP
            r_generic_var<C> tmp;
#else
            generic_var<C> tmp;
#endif
            tmp.extract(sz-offset, reinterpret_cast<const unsigned char*>(this->data)+offset);
            ret = tmp.to_int();
        }
        return ret;
    }
    /**
     \fn    virtual unsigned int array_var::to_unsigned_int(unsigned int idx) const
     \brief Returns the value at the given index. Upper and lower bound checks, rounding and limits
            clamping will be applied.
     \param     idx Zero-based index.
     \returns   Value at the index as an unsigned int or zero if the index is out of bounds.
     */
    virtual unsigned int to_unsigned_int(unsigned int idx) const
    {
        unsigned int ret = 0;
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
#ifdef FLIP
            r_generic_var<C> tmp;
#else
            generic_var<C> tmp;
#endif
            tmp.extract(sz-offset, reinterpret_cast<const unsigned char*>(this->data)+offset);
            ret = tmp.to_unsigned_int();
        }
        return ret;
    }
    /**
     \fn    virtual long array_var::to_long(unsigned int idx) const
     \brief Returns the value at the given index. Upper and lower bound checks, rounding and limits
            clamping will be applied.
     \param     idx Zero-based index.
     \returns   Value at the index as a long or zero if the index is out of bounds.
     */
    virtual long to_long(unsigned int idx) const
    {
        long ret = 0;
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
#ifdef FLIP
            r_generic_var<C> tmp;
#else
            generic_var<C> tmp;
#endif
            tmp.extract(sz-offset, reinterpret_cast<const unsigned char*>(this->data)+offset);
            ret = tmp.to_long();
        }
        return ret;
    }
    /**
     \fn    virtual unsigned long array_var::to_unsigned_long(unsigned int idx) const
     \brief Returns the value at the given index. Upper and lower bound checks, rounding and limits
            clamping will be applied.
     \param     idx Zero-based index.
     \returns   Value at the index as an unsigned long or zero if the index is out of bounds.
     */
    virtual unsigned long to_unsigned_long(unsigned int idx) const
    {
        unsigned long ret = 0;
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
#ifdef FLIP
            r_generic_var<C> tmp;
#else
            generic_var<C> tmp;
#endif
            tmp.extract(sz-offset, reinterpret_cast<const unsigned char*>(this->data)+offset);
            ret = tmp.to_unsigned_long();
        }
        return ret;
    }
    /**
     \fn    virtual long long array_var::to_long_long(unsigned int idx) const
     \brief Returns the value at the given index. Upper and lower bound checks, rounding and limits
            clamping will be applied.
     \param     idx Zero-based index.
     \returns   Value at the index as a long long or zero if the index is out of bounds.
     */
    virtual long long to_long_long(unsigned int idx) const
    {
        long long ret = 0;
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
#ifdef FLIP
            r_generic_var<C> tmp;
#else
            generic_var<C> tmp;
#endif
            tmp.extract(sz-offset, reinterpret_cast<const unsigned char*>(this->data)+offset);
            ret = tmp.to_long_long();
        }
        return ret;
    }
    /**
     \fn    virtual unsigned long long array_var::to_unsigned_long_long(unsigned int idx) const
     \brief Returns the value at the given index. Upper and lower bound checks, rounding and limits
            clamping will be applied.
     \param     idx Zero-based index.
     \returns   Value at the index as an unsigned long long or zero if the index is out of bounds.
     */
    virtual unsigned long long to_unsigned_long_long(unsigned int idx) const
    {
        unsigned long long ret = 0;
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
#ifdef FLIP
            r_generic_var<C> tmp;
#else
            generic_var<C> tmp;
#endif
            tmp.extract(sz-offset, reinterpret_cast<const unsigned char*>(this->data)+offset);
            ret = tmp.to_unsigned_long_long();
        }
        return ret;
    }
    /**
     \fn    virtual float array_var::to_float(unsigned int idx) const
     \brief Returns the value at the given index. Upper and lower bound checks and limits clamping
            will be applied.
     \param     idx Zero-based index.
     \returns   Value at the index as a float or zero if the index is out of bounds.
     */
    virtual float to_float(unsigned int idx) const
    {
        float ret = 0;
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
#ifdef FLIP
            r_generic_var<C> tmp;
#else
            generic_var<C> tmp;
#endif
            tmp.extract(sz-offset, reinterpret_cast<const unsigned char*>(this->data)+offset);
            ret = tmp.to_float();
        }
        return ret;
    }
    /**
     \fn    virtual double array_var::to_double(unsigned int idx) const
     \brief Returns the value at the given index. Upper and lower bound checks and limits clamping
            will be applied.
     \param     idx Zero-based index.
     \returns   Value at the index as a double or zero if the index is out of bounds.
     */
    virtual double to_double(unsigned int idx) const
    {
        double ret = 0;
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
#ifdef FLIP
            r_generic_var<C> tmp;
#else
            generic_var<C> tmp;
#endif
            tmp.extract(sz-offset, reinterpret_cast<const unsigned char*>(this->data)+offset);
            ret = tmp.to_double();
        }
        return ret;
    }
    /**
     \fn    virtual char* array_var::to_char_ptr(unsigned int idx)
     \brief Returns the string value at the given index. Upper and lower bound checks and limits
            clamping will be applied.
     \param     idx Zero-based index.
     \returns   Value at the index as a string, non-reentrant, non-threadsafe.
     */
    virtual char* to_char_ptr(unsigned int idx)
    {
        char* ret = 0;
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
#ifdef FLIP
            r_generic_var<C> tmp;
#else
            generic_var<C> tmp;
#endif
            tmp.extract(sz-offset, reinterpret_cast<const unsigned char*>(this->data)+offset);
            ret = tmp.to_char_ptr();
        }
        return ret;
    }
    /**
     \fn    virtual const char* array_var::to_const_char_ptr(unsigned int idx) const
     \brief Returns the string value at the given index. Upper and lower bound checks and limits
            clamping will be applied.
     \param     idx Zero-based index.
     \returns   Value at the index as a string, non-reentrant, non-threadsafe.
     */
    virtual const char* to_const_char_ptr(unsigned int idx) const
    {
        const char* ret = 0;
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
#ifdef FLIP
            r_generic_var<C> tmp;
#else
            generic_var<C> tmp;
#endif
            tmp.extract(sz-offset, reinterpret_cast<const unsigned char*>(this->data)+offset);
            ret = tmp.to_const_char_ptr();
        }
        return ret;
    }
    /**
     \fn    virtual void array_var::from_char(unsigned int idx, char val)
     \brief Set the value at the given index to the given value. Upper and lower bound checks,
            rounding and limits clamping will be applied.
     \param     idx Zero-based index.
     \param     val The value to be set.
     */
    virtual void from_char(unsigned int idx, char val)
    {
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
            const char oval = to_char(idx);
            if (oval != val)
            {
#ifdef FLIP
                r_generic_var<C> tmp;
#else
                generic_var<C> tmp;
#endif
                tmp.from_char(val);
                outbuf ob;
                ob.set(reinterpret_cast<unsigned char*>(data+offset), width());
                tmp.output(ob);
                setDirty();
                setRMDirty();
            }
            else
            {
                notDirty();
            }
        }
    }
    /**
     \fn    virtual void array_var::from_unsigned_char(unsigned int idx, unsigned char val)
     \brief Set the value at the given index to the given value. Upper and lower bound checks,
            rounding and limits clamping will be applied.
     \param     idx Zero-based index.
     \param     val The value to be set.
     */
    virtual void from_unsigned_char(unsigned int idx, unsigned char val)
    {
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
            const unsigned char oval = to_unsigned_char(idx);
            if (oval != val)
            {
#ifdef FLIP
                r_generic_var<C> tmp;
#else
                generic_var<C> tmp;
#endif
                tmp.from_unsigned_char(val);
                outbuf ob;
                ob.set(reinterpret_cast<unsigned char*>(data+offset), width());
                tmp.output(ob);
                setDirty();
                setRMDirty();
            }
            else
            {
                notDirty();
            }
        }
    }
    /**
     \fn    virtual void array_var::from_short(unsigned int idx, short val)
     \brief Set the value at the given index to the given value. Upper and lower bound checks,
            rounding and limits clamping will be applied.
     \param     idx Zero-based index.
     \param     val The value to be set.
     */
    virtual void from_short(unsigned int idx, short val)
    {
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
            const short oval = to_short(idx);
            if (oval != val)
            {
#ifdef FLIP
                r_generic_var<C> tmp;
#else
                generic_var<C> tmp;
#endif
                tmp.from_short(val);
                outbuf ob;
                ob.set(reinterpret_cast<unsigned char*>(data+offset), width());
                tmp.output(ob);
                setDirty();
                setRMDirty();
            }
            else
            {
                notDirty();
            }
        }
    }
    /**
     \fn    virtual void array_var::from_unsigned_short(unsigned int idx, unsigned short val)
     \brief Set the value at the given index to the given value. Upper and lower bound checks,
            rounding and limits clamping will be applied.
     \param     idx Zero-based index.
     \param     val The value to be set.
     */
    virtual void from_unsigned_short(unsigned int idx, unsigned short val)
    {
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
            const unsigned short oval = to_unsigned_short(idx);
            if (oval != val)
            {
#ifdef FLIP
                r_generic_var<C> tmp;
#else
                generic_var<C> tmp;
#endif
                tmp.from_unsigned_short(val);
                outbuf ob;
                ob.set(reinterpret_cast<unsigned char*>(data+offset), width());
                tmp.output(ob);
                setDirty();
                setRMDirty();
            }
            else
            {
                notDirty();
            }
        }
    }
    /**
     \fn    virtual void array_var::from_int(unsigned int idx, int val)
     \brief Set the value at the given index to the given value. Upper and lower bound checks,
            rounding and limits clamping will be applied.
     \param     idx Zero-based index.
     \param     val The value to be set.
     */
    virtual void from_int(unsigned int idx, int val)
    {
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
            const int oval = to_int(idx);
            if (oval != val)
            {
#ifdef FLIP
                r_generic_var<C> tmp;
#else
                generic_var<C> tmp;
#endif
                tmp.from_int(val);
                outbuf ob;
                ob.set(reinterpret_cast<unsigned char*>(data+offset), width());
                tmp.output(ob);
                setDirty();
                setRMDirty();
            }
            else
            {
                notDirty();
            }
        }
    }
    /**
     \fn    virtual void array_var::from_unsigned_int(unsigned int idx, unsigned int val)
     \brief Set the value at the given index to the given value. Upper and lower bound checks,
            rounding and limits clamping will be applied.
     \param     idx Zero-based index.
     \param     val The value to be set.
     */
    virtual void from_unsigned_int(unsigned int idx, unsigned int val)
    {
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
            const unsigned int oval = to_unsigned_int(idx);
            if (oval != val)
            {
#ifdef FLIP
                r_generic_var<C> tmp;
#else
                generic_var<C> tmp;
#endif
                tmp.from_unsigned_int(val);
                outbuf ob;
                ob.set(reinterpret_cast<unsigned char*>(data+offset), width());
                tmp.output(ob);
                setDirty();
                setRMDirty();
            }
            else
            {
                notDirty();
            }
        }
    }
    /**
     \fn    virtual void array_var::from_long(unsigned int idx, long val)
     \brief Set the value at the given index to the given value. Upper and lower bound checks,
            rounding and limits clamping will be applied.
     \param     idx Zero-based index.
     \param     val The value to be set.
     */
    virtual void from_long(unsigned int idx, long val)
    {
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
            const long oval = to_long(idx);
            if (oval != val)
            {
#ifdef FLIP
                r_generic_var<C> tmp;
#else
                generic_var<C> tmp;
#endif
                tmp.from_long(val);
                outbuf ob;
                ob.set(reinterpret_cast<unsigned char*>(data+offset), width());
                tmp.output(ob);
                setDirty();
                setRMDirty();
            }
            else
            {
                notDirty();
            }
        }
    }
    /**
     \fn    virtual void array_var::from_unsigned_long(unsigned int idx, unsigned long val)
     \brief Set the value at the given index to the given value. Upper and lower bound checks,
            rounding and limits clamping will be applied.
     \param     idx Zero-based index.
     \param     val The value to be set.
     */
    virtual void from_unsigned_long(unsigned int idx, unsigned long val)
    {
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
            const unsigned long oval = to_unsigned_long(idx);
            if (oval != val)
            {
#ifdef FLIP
                r_generic_var<C> tmp;
#else
                generic_var<C> tmp;
#endif
                tmp.from_unsigned_long(val);
                outbuf ob;
                ob.set(reinterpret_cast<unsigned char*>(data+offset), width());
                tmp.output(ob);
                setDirty();
                setRMDirty();
            }
            else
            {
                notDirty();
            }
        }
    }
    /**
     \fn    virtual void array_var::from_long_long(unsigned int idx, long long val)
     \brief Set the value at the given index to the given value. Upper and lower bound checks,
            rounding and limits clamping will be applied.
     \param     idx Zero-based index.
     \param     val The value to be set.
     */
    virtual void from_long_long(unsigned int idx, long long val)
    {
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
            const long long oval = to_long_long(idx);
            if (oval != val)
            {
#ifdef FLIP
                r_generic_var<C> tmp;
#else
                generic_var<C> tmp;
#endif
                tmp.from_long_long(val);
                outbuf ob;
                ob.set(reinterpret_cast<unsigned char*>(data+offset), width());
                tmp.output(ob);
                setDirty();
                setRMDirty();
            }
            else
            {
                notDirty();
            }
        }
    }
    /**
     \fn    virtual void array_var::from_unsigned_long_long(unsigned int idx, unsigned long long val)
     \brief Set the value at the given index to the given value. Upper and lower bound checks,
            rounding and limits clamping will be applied.
     \param     idx Zero-based index.
     \param     val The value to be set.
     */
    virtual void from_unsigned_long_long(unsigned int idx, unsigned long long val)
    {
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
            const unsigned long long oval = to_unsigned_long_long(idx);
            if (oval != val)
            {
#ifdef FLIP
                r_generic_var<C> tmp;
#else
                generic_var<C> tmp;
#endif
                tmp.from_unsigned_long_long(val);
                outbuf ob;
                ob.set(reinterpret_cast<unsigned char*>(data+offset), width());
                tmp.output(ob);
                setDirty();
                setRMDirty();
            }
            else
            {
                notDirty();
            }
        }
    }
    /**
     \fn    virtual void array_var::from_float(unsigned int idx, float val)
     \brief Set the value at the given index to the given value. Upper and lower bound checks,
            rounding and limits clamping will be applied.
     \param     idx Zero-based index.
     \param     val The value to be set.
     */
    virtual void from_float(unsigned int idx, float val)
    {
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
            const float oval = to_float(idx);
            if (oval != val)
            {
#ifdef FLIP
                r_generic_var<C> tmp;
#else
                generic_var<C> tmp;
#endif
                tmp.from_float(val);
                outbuf ob;
                ob.set(reinterpret_cast<unsigned char*>(data+offset), width());
                tmp.output(ob);
                setDirty();
                setRMDirty();
            }
            else
            {
                notDirty();
            }
        }
    }
    /**
     \fn    virtual void array_var::from_double(unsigned int idx, double val)
     \brief Set the value at the given index to the given value. Upper and lower bound checks,
            rounding and limits clamping will be applied.
     \param     idx Zero-based index.
     \param     val The value to be set.
     */
    virtual void from_double(unsigned int idx, double val)
    {
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
            const double oval = to_double(idx);
            if (oval != val)
            {
#ifdef FLIP
                r_generic_var<C> tmp;
#else
                generic_var<C> tmp;
#endif
                tmp.from_double(val);
                outbuf ob;
                ob.set(reinterpret_cast<unsigned char*>(data+offset), width());
                tmp.output(ob);
                setDirty();
                setRMDirty();
            }
            else
            {
                notDirty();
            }
        }
    }
    /**
     \fn    virtual void array_var::from_char_ptr(unsigned int idx, const char* val)
     \brief Set the value at the given index to the value derived from parsing the string. Upper and
            lower bound checks, rounding and limits clamping will be applied.
     \param     idx Zero-based index.
     \param     val The string value to be parsed.
     */
    virtual void from_char_ptr(unsigned int idx, const char* val)
    {
        const int offset = idx * this->width();
        if (offset < static_cast<int>(this->sz))
        {
            const char* oval = to_const_char_ptr(idx);
            if (strcmp(oval,val)!=0)
            {
#ifdef FLIP
                r_generic_var<C> tmp;
#else
                generic_var<C> tmp;
#endif
                tmp.from_char_ptr(val);
                outbuf ob;
                ob.set(reinterpret_cast<unsigned char*>(data+offset), width());
                tmp.output(ob);
                setDirty();
                setRMDirty();
            }
            else
            {
                notDirty();
            }
        }
    }
#ifdef FLIP
protected:

    /**
     \fn    virtual int array_var::cardinal_width() const
     \brief Returns the width of the cardinal value. A negative number is used to imply an endian
            reversal is needed.
     \returns   Width of cardial value.
     */
    virtual int cardinal_width() const
    {
        return -2;
    }
#endif
};
/**
 \class   array_str_var
 \brief   Specialization of array_var&lt;char&gt; that behaves like a variable length string.
 \details Represents a variable length string. As it maintains it own size, just like
          array_value_var, it can handle correctly even if the data is not null-terminated.
          It can be used to store binary data with nulls within the string. Certain methods and
          constructors have been written with the assumption that arguments will be null-terminated
          so that it can behave like a string. Regardless of the content, binary or string, the
          method to_const_char_ptr will always ensure null-termination in line with the
          implementation in other classes.
 */
class RM_EXPORT array_str_var : public array_var<char>
{
public:

    /**
     \fn    virtual unsigned int array_str_var::rtti() const
     \brief Gets the library unique runtime type identifier, the value will be different for
            different basic data types
     \returns   Integer that is unique for each mpt_var derived class.
     */
    virtual unsigned int rtti() const;
    /**
     \fn    array_str_var::array_str_var();
     \brief Default constructor, will initialize it to invalid
     */
    array_str_var();
    /**
     \fn    array_str_var::array_str_var(const array_str_var& cpy);
     \brief Copy constructor, will make a deep copy of the contents
     \param     cpy The copy.
     */
    array_str_var(const array_str_var& cpy);
    /**
     \fn    array_str_var::array_str_var(const char* cpy);
     \brief Copy constructor, will make a deep copy of the contents of the null terminated string
     \param     cpy The null terminated string.
     */
    array_str_var(const char* cpy);
    /**
     \fn    virtual array_str_var::~array_str_var();
     \brief Destructor
     */
    virtual ~array_str_var();
    /**
     \fn    virtual mpt_var* array_str_var::getNext();
     \brief Gets the next item if this item was created in an array, otherwise undefined.
     \returns   Pointer to next item in array.
     */
    virtual mpt_var* getNext();
    /**
     \fn    const array_str_var& array_str_var::operator=(const array_str_var& right);
     \brief Assignment operator
     \param     right   The object to be copied from, a deep copy will be made. If the value is
                        changed, setDirty will be called.
     \returns   A const reference to this object.
     */
    const array_str_var& operator=(const array_str_var& right);
    /**
     \fn    const array_str_var& array_str_var::operator=(const char* right);
     \brief Assignment operator
     \param     right   The null terminated string to be copied from, a deep copy will be made. If
                        the value is changed setDirty is called.
     \returns   A const reference to this object.
     */
    const array_str_var& operator=(const char* right);
    /**
     \fn    virtual const mpt_var& array_str_var::operator=(const mpt_var& right);
     \brief Assignment operator, virtual override so that it works without needing to upcast
     \param     right   The object to be copied from, a deep copy will be made if the type is
                        correct. If the value is changed, setDirty will be called.
     \returns   A const reference to this object.
     */
    virtual const mpt_var& operator=(const mpt_var& right);
    /**
     \fn    bool array_str_var::operator==(const array_str_var& right) const;
     \brief Equality operator
     \param     right   The right object to be compared with.
     \returns   True if the parameters are considered equivalent.
     */
    bool operator==(const array_str_var& right) const;
    /**
     \fn    virtual bool array_str_var::operator==(const mpt_var& right) const;
     \brief Equality operator, virtual override so that it works without needing to upcast
     \param     right   The right object to be compared with,.
     \returns   True if the parameters are considered equivalent.
     */
    virtual bool operator==(const mpt_var& right) const;
    /**
     \fn    bool array_str_var::operator==(const char* right) const;
     \brief Equality operator
     \param     right   The const null terminated string to be compared with.
     \returns   True if the parameters are considered equivalent.
     */
    bool operator==(const char* right) const;
    /**
     \fn    bool array_str_var::operator==(char* right) const;
     \brief Equality operator
     \param [in,out]    right   The null termiated string to be compared with.
     \returns   True if the parameters are considered equivalent.
     */
    bool operator==(char* right) const;
    /**
     \fn    virtual istream& array_str_var::operator>>(istream& s);
     \brief Stream extraction operator that treats the input as a variable length string.
     \param [in,out]    s   an istream to process.
     \returns   The input stream.
     */
    virtual istream& operator>>(istream& s);
    /**
    \fn    virtual ostream& array_str_var::operator>>(ostream& s);
    \brief Stream insertion operator that outputs data as a variable length string.
    \param [in,out]    s   an ostream to process.
    \returns   The output stream.
    */
    virtual ostream& operator<<(ostream& s) const;  /*!< . */
    /**
     \fn    char array_str_var::operator[](int idx) const;
     \brief Array indexer operator
     \param     idx Zero-based index of the string.
     \returns   The indexed value or zero if out of bounds.
     */
    char operator[](int idx) const;
    /**
     \fn    virtual char array_str_var::to_char(unsigned int) const;
     \brief Parses the string as a decimal number and returns the value as a char. Upper and lower
            bound checks, rounding and limits clamping will be applied.
     \param     int Ignored parameter, for compatability with parent.
     \returns   Numerical value as a char.
     */
    virtual char to_char(unsigned int) const;
    /**
     \fn    virtual unsigned char array_str_var::to_unsigned_char(unsigned int) const;
     \brief Parses the string as a decimal number and returns the value as an unsigned char. Upper
            and lower bound checks, rounding and limits clamping will be applied.
     \param     int Ignored parameter, for compatability with parent.
     \returns   Numerical value as a unsigned char.
     */
    virtual unsigned char to_unsigned_char(unsigned int) const;
    /**
     \fn    virtual short array_str_var::to_short(unsigned int) const;
     \brief Parses the string as a decimal number and returns the value as a short. Upper and lower
            bound checks, rounding and limits clamping will be applied.
     \param     int Ignored parameter, for compatability with parent.
     \returns   Numerical value as a short.
     */
    virtual short to_short(unsigned int) const;
    /**
     \fn    virtual unsigned short array_str_var::to_unsigned_short(unsigned int) const;
     \brief Parses the string as a decimal number and returns the value as an unsigned short. Upper
            and lower bound checks, rounding and limits clamping will be applied.
     \param     int Ignored parameter, for compatability with parent.
     \returns   Numerical value as a unsigned short.
     */
    virtual unsigned short to_unsigned_short(unsigned int) const;
    /**
     \fn    virtual int array_str_var::to_int(unsigned int) const;
     \brief Parses the string as a decimal number and returns the value as an int. Upper and lower
            bound checks, rounding and limits clamping will be applied.
     \param     int Ignored parameter, for compatability with parent.
     \returns   Numerical value as an int.
     */
    virtual int to_int(unsigned int) const;
    /**
     \fn    virtual unsigned int array_str_var::to_unsigned_int(unsigned int) const;
     \brief Parses the string as a decimal number and returns the value as an unsigned int. Upper and
            lower bound checks, rounding and limits clamping will be applied.
     \param     int Ignored parameter, for compatability with parent.
     \returns   Numerical value as a unsigned int.
     */
    virtual unsigned int to_unsigned_int(unsigned int) const;
    /**
     \fn    virtual long array_str_var::to_long(unsigned int) const;
     \brief Parses the string as a decimal number and returns the value as a long. Upper and lower
            bound checks, rounding and limits clamping will be applied.
     \param     int Ignored parameter, for compatability with parent.
     \returns   Numerical value as a long.
     */
    virtual long to_long(unsigned int) const;
    /**
     \fn    virtual unsigned long array_str_var::to_unsigned_long(unsigned int) const;
     \brief Parses the string as a decimal number and returns the value as an unsigned long. Upper
            and lower bound checks, rounding and limits clamping will be applied.
     \param     int Ignored parameter, for compatability with parent.
     \returns   Numerical value as a unsigned long.
     */
    virtual unsigned long to_unsigned_long(unsigned int) const;
    /**
     \fn    virtual long long array_str_var::to_long_long(unsigned int) const;
     \brief Parses the string as a decimal number and returns the value as a long long. Upper and
            lower bound checks, rounding and limits clamping will be applied.
     \param     int Ignored parameter, for compatability with parent.
     \returns   Numerical value as a long long.
     */
    virtual long long to_long_long(unsigned int) const;
    /**
     \fn    virtual unsigned long long array_str_var::to_unsigned_long_long(unsigned int) const;
     \brief Parses the string as a decimal number and returns the value as an unsigned long long.
            Upper and lower bound checks, rounding and limits clamping will be applied.
     \param     int Ignored parameter, for compatability with parent.
     \returns   Numerical value as a unsigned long long.
     */
    virtual unsigned long long to_unsigned_long_long(unsigned int) const;
    /**
     \fn    virtual float array_str_var::to_float(unsigned int) const;
     \brief Parses the string as a decimal number and returns the value as a float. Upper and lower
            bound checks and limits clamping will be applied.
     \param     int Ignored parameter, for compatability with parent.
     \returns   Numerical value as a float.
     */
    virtual float to_float(unsigned int) const;
    /**
     \fn    virtual double array_str_var::to_double(unsigned int) const;
     \brief Parses the string as a decimal number and returns the value as a double. Upper and lower
            bound checks and limits clamping will be applied.
     \param     int Ignored parameter, for compatability with parent.
     \returns   Numerical value as a double.
     */
    virtual double to_double(unsigned int) const;
    /**
     \fn    virtual char* array_str_var::to_char_ptr(unsigned int);
     \brief Returns the pointer to the string data, calling setDirty if the data is valid. Note that
            this is the only function that is declared non-const.
     \param     int Ignored parameter, for compatability with parent.
     \returns   Pointer to the string data, or null if there is no data.
     */
    virtual char* to_char_ptr(unsigned int);
    /**
     \fn    virtual const char* array_str_var::to_const_char_ptr(unsigned int) const;
     \brief Returns a copy of the string data in a temporary variable. Non-reentrant, non-thread safe.
     \param     int Ignored parameter, for compatability with parent.
     \returns   Pointer to a null terminated string, even if there is no data.
     */
    virtual const char* to_const_char_ptr(unsigned int) const;
    /**
     \fn    virtual void array_str_var::from_char(unsigned int, char val);
     \brief Stringify the numerical value passed in into a string and store internally
     \param     int Ignored parameter, for compatability with parent.
     \param     val The value.
     */
    virtual void from_char(unsigned int, char val);
    /**
     \fn    virtual void array_str_var::from_unsigned_char(unsigned int, unsigned char val);
     \brief Stringify the numerical value passed in into a string and store internally
     \param     int Ignored parameter, for compatability with parent.
     \param     val The value.
     */
    virtual void from_unsigned_char(unsigned int, unsigned char val);
    /**
     \fn    virtual void array_str_var::from_short(unsigned int, short val);
     \brief Stringify the numerical value passed in into a string and store internally
     \param     int Ignored parameter, for compatability with parent.
     \param     val The value.
     */
    virtual void from_short(unsigned int, short val);
    /**
     \fn    virtual void array_str_var::from_unsigned_short(unsigned int, unsigned short val);
     \brief Stringify the numerical value passed in into a string and store internally
     \param     int Ignored parameter, for compatability with parent.
     \param     val The value.
     */
    virtual void from_unsigned_short(unsigned int, unsigned short val);
    /**
     \fn    virtual void array_str_var::from_int(unsigned int, int val);
     \brief Stringify the numerical value passed in into a string and store internally
     \param     int Ignored parameter, for compatability with parent.
     \param     val The value.
     */
    virtual void from_int(unsigned int, int val);
    /**
     \fn    virtual void array_str_var::from_unsigned_int(unsigned int, unsigned int val);
     \brief Stringify the numerical value passed in into a string and store internally
     \param     int Ignored parameter, for compatability with parent.
     \param     val The value.
     */
    virtual void from_unsigned_int(unsigned int, unsigned int val);
    /**
     \fn    virtual void array_str_var::from_long(unsigned int, long val);
     \brief Stringify the numerical value passed in into a string and store internally
     \param     int Ignored parameter, for compatability with parent.
     \param     val The value.
     */
    virtual void from_long(unsigned int, long val);
    /**
     \fn    virtual void array_str_var::from_unsigned_long(unsigned int, unsigned long val);
     \brief Stringify the numerical value passed in into a string and store internally
     \param     int Ignored parameter, for compatability with parent.
     \param     val The value.
     */
    virtual void from_unsigned_long(unsigned int, unsigned long val);
    /**
     \fn    virtual void array_str_var::from_long_long(unsigned int, long long val);
     \brief Stringify the numerical value passed in into a string and store internally
     \param     int Ignored parameter, for compatability with parent.
     \param     val The value.
     */
    virtual void from_long_long(unsigned int, long long val);
    /**
     \fn    virtual void array_str_var::from_unsigned_long_long(unsigned int, unsigned long long val);
     \brief Stringify the numerical value passed in into a string and store internally
     \param     int Ignored parameter, for compatability with parent.
     \param     val The value.
     */
    virtual void from_unsigned_long_long(unsigned int, unsigned long long val);
    /**
     \fn    virtual void array_str_var::from_float(unsigned int, float val);
     \brief Stringify the numerical value passed in into a string and store internally
     \param     int Ignored parameter, for compatability with parent.
     \param     val The value.
     */
    virtual void from_float(unsigned int, float val);
    /**
     \fn    virtual void array_str_var::from_double(unsigned int, double val);
     \brief Stringify the numerical value passed in into a string and store internally
     \param     int Ignored parameter, for compatability with parent.
     \param     val The value.
     */
    virtual void from_double(unsigned int, double val);
    /**
     \fn    virtual void array_str_var::from_char_ptr(unsigned int, const char* parameter2);
     \brief Deep copy of the string passed in, resizing itself accordingly. This method
            should only be used if there are no nulls expected in the data. If the data is
            binary, the resize should be called to set the length correctly and then to_char_ptr
            used to retrieve the internal buffer to make the copy manually.
     \param     int         Ignored parameter, for compatability with parent.
     \param     parameter2  A null terminated string.
     */
    virtual void from_char_ptr(unsigned int, const char*);
private:
    void* pvt;  /*!< The pvt */
};
/**
 \class r_array_var
 \brief A variable length array of basic data type (plain old data, POD). The POD and unpacked
        and packed in reverse endian format.
 \tparam    C   Type of the POD, eg. int, short, float, etc.
 */
template<class C, class D = typename rm_internal::pod_check<C>::ok>
class r_array_var : public array_var<C, D>
{
public:

    /**
     \fn    virtual unsigned int r_array_var::rtti() const
     \brief Gets the library unique runtime type identifier, the value will be different for
            different basic data types
     \returns   Integer that is unique for each mpt_var derived class.
     */
    virtual unsigned int rtti() const
    {
        return rm_hash_string("r_array_var") ^ var_rtti(C());
    }
    /**
     \fn    r_array_var::r_array_var()
     \brief Default constructor, will initialize it to invalid
     */
    r_array_var()
    {
    }
    /**
     \fn    r_array_var::r_array_var(const r_array_var<C,D>& cpy)
     \brief Copy constructor, will make a deep copy of the contents
     \param     cpy The copy.
     */
    r_array_var(const r_array_var<C, D>& cpy) : array_var<C, D>(cpy)
    {
    }
    /**
     \fn    const r_array_var<C>& r_array_var::operator=(const r_array_var<C>& right)
     \brief Assignment operator
     \param     right   The object to be copied from, a deep copy will be made. If the value is
                        changed, setDirty will be called.
     \returns   A const reference to this object.
     */
    const r_array_var<C>& operator=(const r_array_var<C>& right)
    {
        array_value_var::operator=(right);
        return *this;
    }
    /**
     \fn    virtual const mpt_var& r_array_var::operator=(const mpt_var& right)
     \brief Assignment operator, virtual override so that it works without needing to upcast
     \param     right   The object to be copied from, a deep copy will be made if the type is
                        correct. If the value is changed, setDirty will be called.
     \returns   A const reference to this object.
     */
    virtual const mpt_var& operator=(const mpt_var& right)
    {
        return array_value_var::operator=(right);
    }
    /**
     \fn    virtual int r_array_var::extract(int len, const unsigned char* buf)
     \brief Extracts its data from a byte stream in reverse endian format
     \param     len The length.
     \param     buf The buffer.
     \returns   Number of bytes remaining that was not extracted.
     */
    virtual int extract(int len, const unsigned char* buf)
    {
        return array_value_var::flip_extract(len, buf);
    }
    /**
     \fn    virtual void r_array_var::output(outbuf& strm)
     \brief Outputs its data into a byte stream, writing into outbuf (the output buffer)
     \param [in,out]    strm    Output buffer.
     */
    virtual void output(outbuf& strm)
    {
        array_value_var::flip_output(strm);
    }
    /**
     \fn    virtual int r_array_var::cardinal_width() const
     \brief Returns cardinal width which is used by array_value_var to perform operations
     \returns   An int where a negative number indicates a reverse endian cardinal in the byte stream.
     */
    virtual int cardinal_width() const
    {
#ifdef FLIP
        return 2;
#else
        return -2;
#endif
    }
};
/**
 \class ext_array_var
 \brief An extended array variable with additional configurable parameters
 \tparam    C           Type of the POD, eg. int, short, float, etc.
 \tparam    CAR_WIDTH   Cardinal width. Number of bytes used to represent the size of the array.
                        A negative number indicates a reverse endian cardinal in the byte stream.
 \tparam    CAR_OFFSET  Cardinal offset. Offset from zero where the cardinal is to be unpacked or
                        packed.
 \tparam    CAR_SIZE    Cardinal size. Number of bytes taken up to store the cardinal, including
                        reserved or padding bytes.
 */
template<class C, int CAR_WIDTH, int CAR_OFFSET, int CAR_SIZE, class D = typename rm_internal::pod_check<C>::ok>
class ext_array_var : public array_var<C, D>
{
public:

    /**
     \fn    virtual int ext_array_var::cardinal_width() const
     \brief Returns cardinal width which is used by array_value_var to perform operations
     \returns   An int where a negative number indicates a reverse endian cardinal in the byte stream.
     */
    virtual int cardinal_width() const
    {
        return CAR_WIDTH;
    }
    /**
     \fn    virtual int ext_array_var::cardinal_offset() const
     \brief Returns offset from zero where the cardinal is to be unpacked or packed.
     \returns   An int.
     */
    virtual int cardinal_offset() const
    {
        return CAR_OFFSET;
    }
    /**
     \fn    virtual int ext_array_var::size_cardinal() const
     \brief Returns number of bytes taken up to store the cardinal, including reserved or padding
            bytes.
     \returns   An int.
     */
    virtual int size_cardinal() const
    {
        return CAR_SIZE;
    }
    /**
     \fn    ext_array_var::ext_array_var()
     \brief Default constructor, will initialize it to invalid
     */
    ext_array_var()
    {
    }
    /**
     \fn    ext_array_var::ext_array_var(const ext_array_var<C,CAR_WIDTH,CAR_OFFSET,CAR_SIZE,D>& cpy)
     \brief Copy constructor, will make a deep copy of the contents
     \param     cpy The copy.
     */
    ext_array_var(const ext_array_var<C, CAR_WIDTH, CAR_OFFSET, CAR_SIZE, D>& cpy) : array_var<C>(cpy)
    {
    }
    /**
     \fn    const ext_array_var<C,CAR_WIDTH,CAR_OFFSET,CAR_SIZE,D>& ext_array_var::operator=(const ext_array_var<C, CAR_WIDTH, CAR_OFFSET, CAR_SIZE>& right)
     \brief Assignment operator
     \param     right   The object to be copied from, a deep copy will be made. If the value is
                        changed, setDirty will be called.
     \returns   A const reference to this object.
     */
    const ext_array_var<C, CAR_WIDTH, CAR_OFFSET, CAR_SIZE, D>& operator=(const ext_array_var<C, CAR_WIDTH, CAR_OFFSET, CAR_SIZE, D>& right)
    {
        array_value_var::operator=(right);
        return *this;
    }
    /**
     \fn    virtual const mpt_var& ext_array_var::operator=(const mpt_var& right)
     \brief Assignment operator, virtual override so that it works without needing to upcast
     \param     right   The object to be copied from, a deep copy will be made if the type is
                        correct. If the value is changed, setDirty will be called.
     \returns   A const reference to this object.
     */
    virtual const mpt_var& operator=(const mpt_var& right)
    {
        return array_value_var::operator=(right);
    }
};
/**
 \class r_ext_array_var
 \brief A reverse endian extended array variable with additional configurable parameters
 \tparam    C           Type of the POD, eg. int, short, float, etc.
 \tparam    CAR_WIDTH   Cardinal width. Number of bytes used to represent the size of the array.
                        A negative number indicates a reverse endian cardinal in the byte stream.
 \tparam    CAR_OFFSET  Cardinal offset. Offset from zero where the cardinal is to be unpacked or
                        packed.
 \tparam    CAR_SIZE    Cardinal size. Number of bytes taken up to store the cardinal, including
                        reserved or padding bytes.
 */
template<class C, int CAR_WIDTH, int CAR_OFFSET, int CAR_SIZE, class D = typename rm_internal::pod_check<C>::ok>
class r_ext_array_var : public r_array_var<C, D>
{
public:

    /**
     \fn    virtual int r_ext_array_var::cardinal_width() const
     \brief Returns cardinal width which is used by array_value_var to perform operations
     \returns   An int where a negative number indicates a reverse endian cardinal in the byte stream.
     */
    virtual int cardinal_width() const
    {
        return CAR_WIDTH;
    }
    /**
     \fn    virtual int r_ext_array_var::cardinal_offset() const
     \brief Returns offset from zero where the cardinal is to be unpacked or packed.
     \returns   An int.
     */
    virtual int cardinal_offset() const
    {
        return CAR_OFFSET;
    }
    /**
     \fn    virtual int r_ext_array_var::size_cardinal() const
     \brief Returns number of bytes taken up to store the cardinal, including reserved or padding
            bytes.
     \returns   An int.
     */
    virtual int size_cardinal() const
    {
        return CAR_SIZE;
    }
    /**
     \fn    r_ext_array_var::r_ext_array_var()
     \brief Default constructor, will initialize it to invalid
     */
    r_ext_array_var()
    {
    }
    /**
     \fn    r_ext_array_var::r_ext_array_var(const r_ext_array_var<C,CAR_WIDTH,CAR_OFFSET,CAR_SIZE,D>& cpy)
     \brief Copy constructor, will make a deep copy of the contents
     \param     cpy The copy.
     */
    r_ext_array_var(const r_ext_array_var<C, CAR_WIDTH, CAR_OFFSET, CAR_SIZE, D>& cpy) : r_array_var<C>(cpy)
    {
    }
    /**
     \fn    const r_ext_array_var<C,CAR_WIDTH,CAR_OFFSET,CAR_SIZE,D>& r_ext_array_var::operator=(const r_ext_array_var<C, CAR_WIDTH, CAR_OFFSET, CAR_SIZE>& right)
     \brief Assignment operator
     \param     right   The object to be copied from, a deep copy will be made. If the value is
                        changed, setDirty will be called.
     \returns   A const reference to this object.
     */
    const r_ext_array_var<C, CAR_WIDTH, CAR_OFFSET, CAR_SIZE, D>& operator=(const r_ext_array_var<C, CAR_WIDTH, CAR_OFFSET, CAR_SIZE>& right)
    {
        array_value_var::operator=(right);
        return *this;
    }
    /**
     \fn    virtual const mpt_var& r_ext_array_var::operator=(const mpt_var& right)
     \brief Assignment operator, virtual override so that it works without needing to upcast
     \param     right   The object to be copied from, a deep copy will be made if the type is
                        correct. If the value is changed, setDirty will be called.
     \returns   A const reference to this object.
     */
    virtual const mpt_var& operator=(const mpt_var& right)
    {
        return array_value_var::operator=(right);
    }
};
/**
 \class ext_array_str_var
 \brief An extended variable length string with additional configurable parameters
 \tparam    CAR_WIDTH   Cardinal width. Number of bytes used to represent the size of the array.
                        A negative number indicates a reverse endian cardinal in the byte stream.
 \tparam    CAR_OFFSET  Cardinal offset. Offset from zero where the cardinal is to be unpacked or
                        packed.
 \tparam    CAR_SIZE    Cardinal size. Number of bytes taken up to store the cardinal, including
                        reserved or padding bytes.

 ### tparam C   Type of the POD, eg. int, short, float, etc.
 */
template<int CAR_WIDTH, int CAR_OFFSET, int CAR_SIZE>
class ext_array_str_var : public array_str_var
{
public:

    /**
     \fn    virtual int ext_array_str_var::cardinal_width() const
     \brief Returns cardinal width which is used by array_value_var to perform operations
     \returns   An int where a negative number indicates a reverse endian cardinal in the byte stream.
     */
    virtual int cardinal_width() const
    {
        return CAR_WIDTH;
    }
    /**
     \fn    virtual int ext_array_str_var::cardinal_offset() const
     \brief Returns offset from zero where the cardinal is to be unpacked or packed.
     \returns   An int.
     */
    virtual int cardinal_offset() const
    {
        return CAR_OFFSET;
    }
    /**
     \fn    virtual int ext_array_str_var::size_cardinal() const
     \brief Returns number of bytes taken up to store the cardinal, including reserved or padding
            bytes.
     \returns   An int.
     */
    virtual int size_cardinal() const
    {
        return CAR_SIZE;
    }
    /**
     \fn    ext_array_str_var::ext_array_str_var()
     \brief Default constructor, will initialize it to invalid
     */
    ext_array_str_var()
    {
    }
    /**
     \fn    ext_array_str_var::ext_array_str_var(const ext_array_str_var<CAR_WIDTH, CAR_OFFSET, CAR_SIZE>& cpy)
     \brief Copy constructor, will make a deep copy of the contents
     \param     cpy The copy.
     */
    ext_array_str_var(const ext_array_str_var<CAR_WIDTH, CAR_OFFSET, CAR_SIZE>& cpy) : array_str_var(cpy)
    {
    }
    /**
     \fn    ext_array_str_var::ext_array_str_var(const char* cpy)
     \brief Copy constructor, will make a deep copy of the contents of the null terminated string
     \param     cpy The null terminated string.
     */
    ext_array_str_var(const char* cpy) : array_str_var(cpy)
    {
    }
    /**
     \fn    const ext_array_str_var<CAR_WIDTH, CAR_OFFSET, CAR_SIZE>& ext_array_str_var::operator=(const ext_array_str_var<CAR_WIDTH, CAR_OFFSET, CAR_SIZE>& right)
     \brief Assignment operator
     \param     right   The object to be copied from, a deep copy will be made. If the value is
                        changed, setDirty will be called.
     \returns   A const reference to this object.
     */
    const ext_array_str_var<CAR_WIDTH, CAR_OFFSET, CAR_SIZE>& operator=(const ext_array_str_var<CAR_WIDTH, CAR_OFFSET, CAR_SIZE>& right)
    {
        array_value_var::operator=(right);
        return *this;
    }
    /**
     \fn    const ext_array_str_var<CAR_WIDTH, CAR_OFFSET, CAR_SIZE>& ext_array_str_var::operator=(const char* right)
     \brief Assignment operator
     \param     right   The null terminated string to be copied from, a deep copy will be made. If
                        the value is changed setDirty is called.
     \returns   A const reference to this object.
     */
    const ext_array_str_var<CAR_WIDTH, CAR_OFFSET, CAR_SIZE>& operator=(const char* right)
    {
        array_str_var::operator=(right);
        return *this;
    }
    /**
     \fn    virtual const mpt_var& ext_array_str_var::operator=(const mpt_var& right)
     \brief Assignment operator, virtual override so that it works without needing to upcast
     \param     right   The object to be copied from, a deep copy will be made if the type is
                        correct. If the value is changed, setDirty will be called.
     \returns   A const reference to this object.
     */
    virtual const mpt_var& operator=(const mpt_var& right)
    {
        return array_value_var::operator=(right);
    }
};
#endif /* _ARRAY_VAR_H_ */

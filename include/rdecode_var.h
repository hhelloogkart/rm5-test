/******************************************************************/
/* Copyright DSO National Laboratories 2016. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _RDECODE_VAR_H_
#define _RDECODE_VAR_H_

#include <decode_var.h>

//! The reverse endian decode variable
/*!
    This decode data to a double but extract and packs to the number
    of bytes specified in the constructor.
 */
class RM_EXPORT rdecode_var : public vdecode_var
{
public:
    rdecode_var(int _byte_size, double _range, double _offset = 0.0, int _scale_offset = 1);
    virtual const mpt_var& operator=(const mpt_var& right);
    double operator=(double right);
    const rdecode_var& operator=(const rdecode_var& right);

private:
    explicit rdecode_var(const rdecode_var& cpy);
};

template<typename C>
class templ_rdecode_var : public rdecode_var
{
public:
    templ_rdecode_var() : rdecode_var(C::SIZE, C::RANGE, C::OFFSET, C::SCALE_OFFSET)
    {
    }
    double operator=(double right)
    {
        return generic_var<double>::operator=(right);
    }
    const templ_rdecode_var<C>& operator=(const templ_rdecode_var<C>& right)
    {
        generic_var<double>::operator=(right);
        return *this;
    }
    virtual const mpt_var& operator=(const mpt_var& right)
    {
        return generic_var<double>::operator=(right);
    }
private:
    explicit templ_rdecode_var(const templ_rdecode_var&);
};
#define DEFINE_RDECODE_VAR(clsnm, byte_size, range, offset, soffset)  \
    class clsnm : public rdecode_var \
    { \
public: \
        clsnm() : rdecode_var(byte_size, range, offset, soffset) {} \
        mpt_var* getNext() { return this + 1; } \
    };
#endif /* _RDECODE_VAR_H_ */

/******************************************************************/
/* Copyright DSO National Laboratories 2011. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _FLEX_VAR_H_
#define _FLEX_VAR_H_

#include <fcpl_var.h>

//! A container that has a variable number of contained objects of type C. C must be derived from mpt_var.
template<typename C, class D = typename C::mpt_var_cl>
class flex_var : public flex_complex_var
{
public:
    flex_var()
    {
    }
    flex_var(const flex_var<C,D>& cpy) : flex_complex_var()
    {
        *this = cpy;
    }
    void add(const C& p)
    {
        mpt_var::push();
        new C(p);
        mpt_var::pop();
        mpt_var::setDirty();
        mpt_var::setRMDirty();
    }
    void add(mpt_var* p)
    {
        flex_complex_var::add(p);
    }
    void insert(int idx, const C& p)
    {
        if (idx < this->cardinal())
        {
            mpt_var::push();
            new C(p);
            mpt_var::pop();
            if (idx <= 0)
            {
                complex_var::shuffle_index(0);
            }
            else
            {
                complex_var::shuffle_index(idx);
            }
            mpt_var::setDirty();
            mpt_var::setRMDirty();
        }
        else
        {
            add(p);
            mpt_var::setDirty();
            mpt_var::setRMDirty();
        }
    }
    void insert(mpt_var* p, int idx)
    {
        flex_complex_var::insert(p, idx);
    }
    C* operator[](unsigned int idx) const
    {
        return static_cast<C*>(complex_var::operator[](idx));
    }
    virtual mpt_var* getNext()
    {
        return this + 1;
    }
    virtual const mpt_var& operator=(const mpt_var& right)
    {
        return flex_complex_var::operator=(right);
    }
    const flex_var<C,D>& operator=(const flex_var<C,D>& right)
    {
        return static_cast<const flex_var<C,D>&>(flex_complex_var::operator=(right));
    }
protected:
#ifdef FLIP
    virtual int cardinal_width() const
    {
        return -4;
    }
#endif
    virtual int width() const
    {
        return C().size();
    }
    virtual mpt_var* create(mpt_var* cpy=0) const
    {
        C* ptr = new C;
        if (cpy)
        {
            *ptr = *cpy;
        }
        return ptr;
    }
};

template<class C, class D = typename C::mpt_var_cl>
class r_flex_var : public flex_var<C, D>
{
public:
    r_flex_var()
    {
    }
    r_flex_var(const r_flex_var<C, D>& cpy) : flex_var<C, D>(cpy)
    {
    }
    const r_flex_var<C, D>& operator=(const r_flex_var<C, D>& right)
    {
        flex_complex_var::operator=(right);
        return *this;
    }
    virtual const mpt_var& operator=(const mpt_var& right)
    {
        return flex_complex_var::operator=(right);
    }
protected:
    virtual int cardinal_width() const
    {
#ifdef FLIP
        return 4;
#else
        return -4;
#endif
    }
};

template<class C, int CAR_WIDTH, int CAR_OFFSET, int CAR_SIZE, class D = typename C::mpt_var_cl>
class ext_flex_var : public flex_var<C, D>
{
public:
    ext_flex_var()
    {
    }
    ext_flex_var(const ext_flex_var<C, CAR_WIDTH, CAR_OFFSET, CAR_SIZE, D>& cpy) : flex_var<C, D>(cpy)
    {
    }
    const ext_flex_var<C, CAR_WIDTH, CAR_OFFSET, CAR_SIZE, D>& operator=(const ext_flex_var<C, CAR_WIDTH, CAR_OFFSET, CAR_SIZE, D>& right)
    {
        flex_var<C, D>::operator=(right);
        return *this;
    }
    virtual const mpt_var& operator=(const mpt_var& right)
    {
        return flex_var<C, D>::operator=(right);
    }
protected:
    virtual int cardinal_width() const
    {
        return CAR_WIDTH;
    }
    virtual int cardinal_offset() const
    {
        return CAR_OFFSET;
    }
    virtual int size_cardinal() const
    {
        return CAR_SIZE;
    }
};
#endif

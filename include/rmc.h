#ifndef _RM_C_H_
#define _RM_C_H_

#ifndef RM_EXPORT
#ifdef _WIN32
# pragma warning(disable:4786)
# ifdef LIBRARY
#  define RM_EXPORT __declspec(dllexport)
#  pragma warning( disable : 4251 4275 )
# elif defined(STATIC)
#  define RM_EXPORT
# else
#  define RM_EXPORT __declspec(dllimport)
#  pragma warning( disable : 4251 4275 )
# endif
#else
# define RM_EXPORT
#endif
#endif

#ifdef __cplusplus
extern "C"
{
#endif

RM_EXPORT int find_index(void*);
RM_EXPORT int rm_message_verify(int);

/*----------------------------------*/
/* Functions global to all messages */
/*----------------------------------*/
RM_EXPORT void rm_var_incoming(void);
RM_EXPORT void rm_var_outgoing(void);
RM_EXPORT int rm_var_check_connect(void);
RM_EXPORT void rm_var_set_host(const char*);
RM_EXPORT void rm_var_set_port(unsigned short);
RM_EXPORT void rm_var_set_client(const char*);
RM_EXPORT void rm_var_set_flow(unsigned short);
RM_EXPORT void rm_var_set_wait(unsigned int sec, unsigned int usec);

/*---------------------------------*/
/* Functions specific to a message */
/*---------------------------------*/
RM_EXPORT void rm_var_setRMDirty(void*);
RM_EXPORT void rm_var_refreshRM(void*);
RM_EXPORT void rm_var_clearDirty(void*);
RM_EXPORT void rm_var_registerIDX(void*);
RM_EXPORT void rm_var_unregisterIDX(void*);
RM_EXPORT int rm_var_isDirty(void*);
RM_EXPORT int rm_var_getregister(void*);

#ifdef __cplusplus
}
#endif

#endif /* _RM_C_H_ */



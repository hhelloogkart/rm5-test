/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _CHK_VAR_H_
#define _CHK_VAR_H_

#include <cplx_var.h>

extern RM_EXPORT int rm_BE(int val);
extern RM_EXPORT unsigned int rm_BE(unsigned int val);
extern RM_EXPORT unsigned short rm_BE(unsigned short val);
extern RM_EXPORT short rm_BE(short val);
extern RM_EXPORT int rm_LE(int val);
extern RM_EXPORT unsigned int rm_LE(unsigned int val);
extern RM_EXPORT unsigned short rm_LE(unsigned short val);
extern RM_EXPORT short rm_LE(short val);

/**
 \class   chk_var
 \brief   Abstract base class for variables that checks incoming data for corruption, or
          duplication, or other criteria before passing on to its children for extract.
 \details Abstract class that provides the supporting code for checking incoming data and
          also to output check data, such as checksums, so that it can be verified by
          the receiver. Derived classes will implement different checksums or check methods.

          The location of the check data and the portion of the buffer
          to check is implemented by this class, using 3 parameters:
              1. Check location
              2. Front offset
              3. Back offset
          \image html image056.png width=517
 */
class RM_EXPORT chk_var : public complex_var
{
public:

    /**
     \fn    chk_var::chk_var(unsigned char chk_loc=1U, unsigned char front_off=0, unsigned char back_off=0);
     \brief Constructor
     \param     chk_loc     (Optional) The check location. Default is 1, hash at rear of payload.
     \param     front_off   (Optional) The front offset. Default is 0.
     \param     back_off    (Optional) The back offset. Default is 0.
     */
    chk_var(unsigned char chk_loc=1U, unsigned char front_off=0, unsigned char back_off=0);
    /**
     \fn    chk_var::chk_var(const chk_var& right);
     \brief Copy constructor, copies the check properties and values of its children.
     \param     right   The object to be copied.
     */
    chk_var(const chk_var& right);
    /**
     \fn    virtual int chk_var::extract(int len, const unsigned char* buf);
     \brief Extracts its data from a byte stream by checking the integrity before pass it
            on to its children.
     \param     len The length of the buffer in bytes.
     \param     buf The buffer.
     \returns   Number of bytes remaining that was not extracted.
     */
    virtual int extract(int len, const unsigned char* buf);
    /**
     \fn    virtual int chk_var::size() const;
     \brief Returns the size in bytes that this object will need to serialize
            itself into a byte stream. This depends on the check properties,
            checksum_size() and size of its children.
     \returns   Size in bytes.
     */
    virtual int size() const;
    /**
     \fn    virtual void chk_var::output(outbuf& strm);
     \brief Outputs data into a byte stream writing into outbuf. It will also pack
            the check data into the packet needed by the receiver.
     \param [out]    strm     Output buffer.
     */
    virtual void output(outbuf& strm);
    /**
     \fn    virtual const mpt_var& chk_var::operator=(const mpt_var& right);
     \brief Assignment operator. Copies the values of its children if casting succeeds.
     \param     right   The right object to be copied from.
     \returns   A reference of this object.
     */
    virtual const mpt_var& operator=(const mpt_var& right);
    /**
     \fn    const chk_var& chk_var::operator=(const chk_var& right);
     \brief Assignment operator. Copies the values of its children.
     \param     right   The right object to be copied from.
     \returns   A reference of this object.
     */
    const chk_var& operator=(const chk_var& right);
    /**
     \fn    int chk_var::received_packets() const;
     \brief Gets the total number received packets
     \returns   Number of received packets.
     */
    int received_packets() const;
    /**
     \fn    int chk_var::failed_packets() const;
     \brief Get the total number of failed packets
     \returns   Number of failed packets.
     */
    int failed_packets() const;
    /**
     \fn    virtual int chk_var::checksum_size() const = 0;
     \brief Checksum size in bytes. Abstract function that must be overriden by derived classes. This
            is to facilitate this class to call for the checksum to be computed and comparison of the
            checksums.
     \returns   Size in bytes.
     */
    virtual int  checksum_size() const = 0;
    /**
     \fn    virtual void chk_var::make_chksum(int len, const unsigned char* buf, outbuf& strm) = 0;
     \brief Makes a checksum given a buffer. Abstract function that must be overriden by derived classes.
     \param          len     The length of the buffer.
     \param          buf     The buffer.
     \param [out]    strm    The output buffer for the generated checksum.
     */
    virtual void make_chksum(int len, const unsigned char* buf, outbuf& strm) = 0;

protected:

    /**
     \fn    virtual bool chk_var::check(const unsigned char* left, const unsigned char* right, int len);
     \brief Compares the result of make_chksum() and the packet checksum. Override this if its not
            a straightforward byte for byte comparison, for example checking of sequence number to
            admit new packets.
     \param     left    The checksum that was computed.
     \param     right   The checksum extracted from the packet.
     \param     len     The length to compare in bytes.
     \returns   True if it compares favourably.
     */
    virtual bool check(const unsigned char* left, const unsigned char* right, int len);

    int total_in_packets;   /*!< The total packets received */
    int total_failed_packets;   /*!< The total failed packets */
    unsigned char front_offset; /*!< The front offset */
    unsigned char back_offset;  /*!< The back offset */
    unsigned char chk_location; /* 1 - back, 2 - front, 3 - header, 4 - back ignore size */ /*!< The check location */

};

#endif /* _CHK_VAR_H_ */
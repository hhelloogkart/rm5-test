/******************************************************************/
/* Copyright DSO National Laboratories 2013. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/
#ifndef _FLUN_SHOW_H_
#define _FLUN_SHOW_H_

#include <mpt_show.h>

struct flex_union_show_private;
class flex_union_var;

class RM_EXPORT flex_union_show : public mpt_baseshow
{

public:
    flex_union_show();
    virtual ~flex_union_show();
    /*reimp*/ void setDirty(mpt_var*);
    /*reimp*/ void operator()();

    void setIDDirty(int id);
    void addCallback(flex_union_var* var);
    void removeCallback(flex_union_var* var);
    virtual void compute(int id) = 0;

protected:
    flex_union_var* obj;
    flex_union_show_private* prvt;

};

#endif /* _FLUN_SHOW_H_ */
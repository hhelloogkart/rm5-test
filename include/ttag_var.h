/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _TTAG_VAR_H_
#define _TTAG_VAR_H_

#ifdef _WIN32
#pragma warning(disable : 4146)
#endif

#include <gen_var.h>

// The three byte time_tag

class RM_EXPORT ttag_var : public generic_var<unsigned int>
{
public:
    RUNTIME_TYPE(ttag_var)
    ttag_var();
    ttag_var(const ttag_var& cpy);
    virtual int extract(int buflen, const unsigned char* buf);
    virtual int size() const;
    virtual void output(outbuf& strm);
    virtual mpt_var* getNext();
    const ttag_var& operator=(const ttag_var& right);
    bool operator==(const ttag_var& right) const;

    // call the parent class
    unsigned int operator=(unsigned int right)
    {
        return generic_var<unsigned int>::operator=(right);
    }
    const generic_var<unsigned int>& operator=(const generic_var<unsigned int>& right)
    {
        return generic_var<unsigned int>::operator=(right);
    }
    virtual const mpt_var& operator=(const mpt_var& right)
    {
        return generic_var<unsigned int>::operator=(right);
    }
    bool operator==(const generic_var<unsigned int>& right) const
    {
        return generic_var<unsigned int>::operator==(right);
    }
    virtual bool operator==(const mpt_var& right) const
    {
        return generic_var<unsigned int>::operator==(right);
    }
};

#endif /* _TTAG_VAR_H_ */

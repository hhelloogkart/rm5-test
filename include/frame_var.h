/******************************************************************/
/* Copyright DSO National Laboratories 2009. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _FRAME_VAR_H_
#define _FRAME_VAR_H_

#include <cplx_var.h>

class frame_var_private_show;

//! The composite class that acts as a container for multiple mpt_var objects that are output in different subframes
class RM_EXPORT frame_var : public complex_var
{
public:
    frame_var(unsigned int _frame_sz);
    frame_var(const frame_var& right);
    virtual ~frame_var();

    virtual int extract(int len, const unsigned char* buf);
    virtual void output(outbuf& strm);
    virtual int size() const;

    virtual const mpt_var& operator=(const mpt_var& right);
    virtual bool operator==(const mpt_var& right) const;
    const frame_var& operator=(const frame_var& right);
    bool operator==(const frame_var& right) const;

    unsigned int data_size() const;

    void init_child_observers();

protected:
    unsigned int frame_sz;
    frame_var_private_show* shw;
};


#define FRAMECONST(t, fsz) t(const t& arg) : frame_var(arg) \
{ \
    child_copy_constructor(arg); \
    init_child_observers(); \
} \
t() : frame_var(fsz) \
{ \
    pop(); \
    init_child_observers(); \
} \
const t& operator=(const t& right) \
{ \
    frame_var::operator=(right); \
    return *this; \
} \
const mpt_var& operator=(const mpt_var& right) \
{ \
    frame_var::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)

#endif /* _DECISION_VAR_H_ */

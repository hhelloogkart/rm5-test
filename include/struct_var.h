/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _STRUCT_VAR_H_
#define _STRUCT_VAR_H_

#include <mpt_var.h>

/*!
    Packs and unpacks the content into
    a struct. This class cannot be used as it
    is. You need to derive from this class and
    implement the logic for extract, size and output.

    These 3 functions are automatically generated
    by the C version of Data Explorer.
 */
class RM_EXPORT struct_var : public mpt_var
{
public:
    RUNTIME_TYPE(struct_var)

    explicit struct_var(void*);

    virtual bool isValid() const;
    virtual bool setInvalid();
    virtual mpt_var* getNext();
    virtual bool operator==(const mpt_var& right) const;
    virtual istream& operator>>(istream& s);
    virtual ostream& operator<<(ostream& s) const;

    const struct_var& operator=(const struct_var& right);

protected:
    void* my_struct;

private:
    struct_var();

};

#define MARSHALLER_BEGIN(t,n) class t ## _marshaller : public struct_var { \
public: \
        t ## _marshaller() : struct_var(&n) {} \
        int extract(int len, const unsigned char* buf) { t ## _extract((t*)my_struct, 1, buf, &len); setDirty(); return len; } \
        int size() const { return t ## _size((t*)my_struct); } \
        void output(outbuf &strm) { int sz=strm.maxsize()-strm.size(); t ## _pack((t*)mystruct, 1, strm.getcur(),sz); strm.setsize(strm.maxsize()-sz); }
#define MARSHALLER_END };

#endif /* _STRUCT_VAR_H_ */

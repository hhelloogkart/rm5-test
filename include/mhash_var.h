/******************************************************************/
/* Copyright DSO National Laboratories 2019. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _MHASH_VAR_H_
#define _MHASH_VAR_H_

#include <chk_var.h>

/**
 \class   mhash_var
 \brief   Checks the integrity using hashes provided by mhash library by Nikos Mavroyanopoulos
          and Sascha Schumman.
 \details Implements various types of checksums, selected by using the enumeration.
          See the documentation for chk_var on the specification of the block of
          data that the checksum will be computed, and the location of the checksum
          in the packet.

           Enumeration        | Description
          --------------------|------------------------------
           MHASH_CRC16        | CRC-16 CCITT used in X.25, HDLC, XMODEM, Bluetooth
           MHASH_CRC16B       | CRC-16 CCITT big endian
           MHASH_CRC32        | The one used in Ethernet
           MHASH_CRC32B       | The one used in ZIP, Zmodem
           MHASH_CRC32B_SPAN  | Big endian short word arranged little endian
           MHASH_MD5          | The RSA algo (depreciated)
           MHASH_SHA1         | The NSA algo (depreciated)
           MHASH_SHA512       | The NSA algo SHA2-512
           MHASH_SHA3         | Latest NSA standard SHA-3
 */
class RM_EXPORT mhash_var : public chk_var
{
public:
    /**
     \enum  __mhashid
     \brief Values that represent various types of hashes
     */
    enum __mhashid
    {
        MHASH_CRC32 = 0,
        MHASH_MD5 = 1,
        MHASH_SHA1 = 2,
        MHASH_CRC32B = 9,
        MHASH_SHA512 = 20,
        MHASH_CRC32B_SPAN = 73,
        MHASH_CRC16 = 74,
        MHASH_CRC16B = 75,
        MHASH_SHA3 = 76
    };
    /**
     \fn    mhash_var::mhash_var(int _hash, unsigned char chk_loc, unsigned char front_off, unsigned char back_off);
     \brief Constructor
     \param     _hash       The hash to use.
     \param     chk_loc     (Optional) The check location. Default is 1, hash at rear of payload.
     \param     front_off   (Optional) The front offset. Default is 0.
     \param     back_off    (Optional) The back offset. Default is 0.
     */
    mhash_var(int _hash, unsigned char chk_loc, unsigned char front_off, unsigned char back_off);
    /**
     \fn    mhash_var::mhash_var(const mhash_var& right);
     \brief Copy constructor, copies the check properties and values of its children.
     \param     right   The object to be copied.
     */
    mhash_var(const mhash_var& right);
    /**
     \fn    const mhash_var& mhash_var::operator=(const mhash_var& right);
     \brief Assignment operator. Copies the values of its children.
     \param     right   The right object to be copied from.
     \returns   A reference of this object.
     */
    const mhash_var& operator=(const mhash_var& right);
    /**
     \fn    const mpt_var& mhash_var::operator=(const mpt_var& right);
     \brief Assignment operator. Copies the values of its children if casting succeeds.
     \param     right   The right object to be copied from.
     \returns   A reference of this object.
     */
    /*reimp*/ const mpt_var& operator=(const mpt_var& right);
    /**
     \fn    int mhash_var::checksum_size() const;
     \brief Checksum size in bytes.
     \returns   Size in bytes.
     */
    /*reimp*/ int checksum_size() const;
    /**
     \fn    void mhash_var::make_chksum(int len, const unsigned char* buf, outbuf& strm);
     \brief Makes a checksum given a buffer.
     \param          len     The length of the buffer.
     \param          buf     The buffer.
     \param [out]    strm    The output buffer for the generated checksum.
     */
    /*reimp*/ void make_chksum(int len, const unsigned char* buf, outbuf& strm);

private:
    mhash_var();

protected:
    int hashtype;   /*!< The selected hashtype */
};

//! Adapation for hashes computed as big-endian integers
/*!
    Some developers do not follow the standards strictly and compute
    hashes in the form of integers and transmit them as such. As a result
    this class is used to adapt when the other communication partner is
    of a different endian system, in this case big-endian.
 */
class RM_EXPORT mhash_be_int_var : public mhash_var
{
public:
    /**
     \fn    mhash_be_int_var::mhash_be_int_var(int _hash, unsigned char chk_loc, unsigned char front_off, unsigned char back_off);
     \brief Constructor
     \param     _hash       The hash to use.
     \param     chk_loc     (Optional) The check location. Default is 1, hash at rear of payload.
     \param     front_off   (Optional) The front offset. Default is 0.
     \param     back_off    (Optional) The back offset. Default is 0.
     */
    mhash_be_int_var(int _hash, unsigned char chk_loc, unsigned char front_off, unsigned char back_off);
    /**
     \fn    mhash_be_int_var::mhash_be_int_var(const mhash_be_int_var& right);
     \brief Copy constructor, copies the check properties and values of its children.
     \param     right   The object to be copied.
     */
    mhash_be_int_var(const mhash_be_int_var& right);
    /**
     \fn    const mhash_be_int_var& mhash_be_int_var::operator=(const mhash_be_int_var& right);
     \brief Assignment operator. Copies the values of its children.
     \param     right   The right object to be copied from.
     \returns   A reference of this object.
     */
    const mhash_be_int_var& operator=(const mhash_be_int_var& right);
    /**
     \fn    const mpt_var& mhash_be_int_var::operator=(const mpt_var& right);
     \brief Assignment operator. Copies the values of its children if casting succeeds.
     \param     right   The right object to be copied from.
     \returns   A reference of this object.
     */
    /*reimp*/ const mpt_var& operator=(const mpt_var& right);
    /**
     \fn    void mhash_be_int_var::make_chksum(int len, const unsigned char* buf, outbuf& strm);
     \brief Makes a checksum given a buffer.
     \param          len     The length of the buffer.
     \param          buf     The buffer.
     \param [out]    strm    The output buffer for the generated checksum.
     */
    /*reimp*/ void make_chksum(int len, const unsigned char* buf, outbuf& strm);

private:
    mhash_be_int_var();
};

//! Adapation for hashes computed as little-endian integers
/*!
    Some developers do not follow the standards strictly and compute
    hashes in the form of integers and transmit them as such. As a result
    this class is used to adapt when the other communication partner is
    of a different endian system, in this case little-endian.
 */
class RM_EXPORT mhash_le_int_var : public mhash_var
{
public:
    /**
     \fn    mhash_le_int_var::mhash_le_int_var(int _hash, unsigned char chk_loc, unsigned char front_off, unsigned char back_off);
     \brief Constructor
     \param     _hash       The hash to use.
     \param     chk_loc     (Optional) The check location. Default is 1, hash at rear of payload.
     \param     front_off   (Optional) The front offset. Default is 0.
     \param     back_off    (Optional) The back offset. Default is 0.
     */
    mhash_le_int_var(int _hash, unsigned char chk_loc, unsigned char front_off, unsigned char back_off);
    /**
     \fn    mhash_le_int_var::mhash_le_int_var(const mhash_le_int_var& right);
     \brief Copy constructor, copies the check properties and values of its children.
     \param     right   The object to be copied.
     */
    mhash_le_int_var(const mhash_le_int_var& right);
    /**
     \fn    const mhash_le_int_var& mhash_le_int_var::operator=(const mhash_le_int_var& right);
     \brief Assignment operator. Copies the values of its children.
     \param     right   The right object to be copied from.
     \returns   A reference of this object.
     */
    const mhash_le_int_var& operator=(const mhash_le_int_var& right);
    /**
     \fn    const mpt_var& mhash_le_int_var::operator=(const mpt_var& right);
     \brief Assignment operator. Copies the values of its children if casting succeeds.
     \param     right   The right object to be copied from.
     \returns   A reference of this object.
     */
    /*reimp*/ const mpt_var& operator=(const mpt_var& right);
    /**
     \fn    void mhash_le_int_var::make_chksum(int len, const unsigned char* buf, outbuf& strm);
     \brief Makes a checksum given a buffer.
     \param          len     The length of the buffer.
     \param          buf     The buffer.
     \param [out]    strm    The output buffer for the generated checksum.
     */
    /*reimp*/ void make_chksum(int len, const unsigned char* buf, outbuf& strm);

private:
    mhash_le_int_var();
};

/**
 \class   v_mhash_var
 \brief   Template class to access the specific mhash algorithms
 \tparam  ht  The hash enumeration to use
 \tparam  cl  The check location enumeration (see chk_var)
 \tparam  fo  The front offset (see chk_var)
 \tparam  bo  The back offset (see chk_var)
 */
template<int ht,unsigned int cl,unsigned int fo,unsigned int bo>
class v_mhash_var : public mhash_var
{
public:
    /**
     \fn    v_mhash_var::v_mhash_var()
     \brief Default constructor
     */
    v_mhash_var() : mhash_var(ht, cl, fo, bo)
    {
    }
    /**
     \fn    v_mhash_var::v_mhash_var(const v_mhash_var& right)
     \brief Copy constructor, copies the check properties and values of its children.
     \param     right   The object to be copied.
     */
    v_mhash_var(const v_mhash_var& right) : mhash_var(right)
    {
    }
    /**
     \fn    const v_mhash_var& v_mhash_var::operator=(const v_mhash_var& right)
     \brief Assignment operator. Copies the values of its children.
     \param     right   The right object to be copied from.
     \returns   A reference of this object.
     */
    const v_mhash_var& operator=(const v_mhash_var& right)
    {
        mhash_var::operator=(static_cast<const mhash_var&>(right));
        return *this;
    }
    /**
     \fn    const mpt_var& v_mhash_var::operator=(const mpt_var& right)
     \brief Assignment operator. Copies the values of its children if casting succeeds.
     \param     right   The right object to be copied from.
     \returns   A reference of this object.
     */
    /*reimp*/ const mpt_var& operator=(const mpt_var& right)
    {
        return mhash_var::operator=(right);
    }
};

/**
 \class   v_mhash_be_int_var
 \brief   Template class to access the specific mhash algorithms using big endian adaptation
 \tparam  ht  The hash enumeration to use
 \tparam  cl  The check location enumeration (see chk_var)
 \tparam  fo  The front offset (see chk_var)
 \tparam  bo  The back offset (see chk_var)
 */
template<int ht, unsigned int cl, unsigned int fo, unsigned int bo>
class v_mhash_be_int_var : public mhash_be_int_var
{
public:
    /**
     \fn    v_mhash_be_int_var::v_mhash_be_int_var()
     \brief Default constructor
     */
    v_mhash_be_int_var() : mhash_be_int_var(ht, cl, fo, bo)
    {
    }
    /**
     \fn    v_mhash_be_int_var::v_mhash_be_int_var(const v_mhash_be_int_var& right)
     \brief Copy constructor, copies the check properties and values of its children.
     \param     right   The object to be copied.
     */
    v_mhash_be_int_var(const v_mhash_be_int_var& right) : mhash_be_int_var(right)
    {
    }
    /**
     \fn    const v_mhash_be_int_var& v_mhash_be_int_var::operator=(const v_mhash_be_int_var& right)
     \brief Assignment operator. Copies the values of its children.
     \param     right   The right object to be copied from.
     \returns   A reference of this object.
     */
    const v_mhash_be_int_var& operator=(const v_mhash_be_int_var& right)
    {
        mhash_be_int_var::operator=(static_cast<const mhash_be_int_var&>(right));
        return *this;
    }
    /**
     \fn    const mpt_var& v_mhash_be_int_var::operator=(const mpt_var& right)
     \brief Assignment operator. Copies the values of its children if casting succeeds.
     \param     right   The right object to be copied from.
     \returns   A reference of this object.
     */
    /*reimp*/ const mpt_var& operator=(const mpt_var& right)
    {
        return mhash_be_int_var::operator=(right);
    }
};

/**
 \class   v_mhash_le_int_var
 \brief   Template class to access the specific mhash algorithms using little endian adaptation
 \tparam  ht  The hash enumeration to use
 \tparam  cl  The check location enumeration (see chk_var)
 \tparam  fo  The front offset (see chk_var)
 \tparam  bo  The back offset (see chk_var)
 */
template<int ht, unsigned int cl, unsigned int fo, unsigned int bo>
class v_mhash_le_int_var : public mhash_le_int_var
{
public:
    /**
     \fn    v_mhash_le_int_var::v_mhash_le_int_var()
     \brief Default constructor
     */
    v_mhash_le_int_var() : mhash_le_int_var(ht, cl, fo, bo)
    {
    }
    /**
     \fn    v_mhash_le_int_var::v_mhash_le_int_var(const v_mhash_le_int_var& right)
     \brief Copy constructor, copies the check properties and values of its children.
     \param     right   The object to be copied.
     */
    v_mhash_le_int_var(const v_mhash_le_int_var& right) : mhash_le_int_var(right)
    {
    }
    /**
     \fn    const v_mhash_le_int_var& v_mhash_le_int_var::operator=(const v_mhash_le_int_var& right)
     \brief Assignment operator. Copies the values of its children.
     \param     right   The right object to be copied from.
     \returns   A reference of this object.
     */
    const v_mhash_le_int_var& operator=(const v_mhash_le_int_var& right)
    {
        mhash_le_int_var::operator=(static_cast<const mhash_le_int_var&>(right));
        return *this;
    }
    /**
     \fn    const mpt_var& v_mhash_le_int_var::operator=(const mpt_var& right)
     \brief Assignment operator. Copies the values of its children if casting succeeds.
     \param     right   The right object to be copied from.
     \returns   A reference of this object.
     */
    /*reimp*/ const mpt_var& operator=(const mpt_var& right)
    {
        return mhash_le_int_var::operator=(right);
    }
};

typedef v_mhash_var<mhash_var::MHASH_CRC16, 1, 0, 0> crc16_var;
typedef v_mhash_var<mhash_var::MHASH_CRC16B, 1, 0, 0> crc16b_var;
typedef v_mhash_var<mhash_var::MHASH_CRC32, 1, 0, 0> crc32_var;
typedef v_mhash_var<mhash_var::MHASH_CRC32B, 1, 0, 0> crc32b_var;
typedef v_mhash_var<mhash_var::MHASH_CRC32B_SPAN, 1, 0, 0> crc32b_span_var;
typedef v_mhash_be_int_var<mhash_var::MHASH_CRC32, 1, 0, 0> be_crc32_var;
typedef v_mhash_var<mhash_var::MHASH_MD5, 1, 0, 0> md5_var;
typedef v_mhash_var<mhash_var::MHASH_SHA1, 1, 0, 0> sha1_var;
typedef v_mhash_var<mhash_var::MHASH_SHA512, 1, 0, 0> sha512_var;
typedef v_mhash_var<mhash_var::MHASH_SHA3, 1, 0, 0> sha3_var;

/**
 \def   EXTMHASHCONST(t, ht, cl, front, back)
 \brief A macro that should be placed in derived classes to implement the correct constructors,
        assignment operators and other methods. This should be used when deriving from mhash_var.
 \param     t       Class name of the derived class.
 \param     ht      The hash enumeration
 \param     loc     The check location.
 \param     front   The front offset.
 \param     back    The back offset.
 */
#define EXTMHASHCONST(t, ht, cl, front, back) t(const t& arg) : mhash_var(arg) \
{ \
    child_copy_constructor(arg); \
} \
t() : mhash_var(ht, cl, front, back) \
{ \
    pop(); \
} \
const t& operator=(const t& right) \
{ \
    mhash_var::operator=(right); \
    return *this; \
} \
const mpt_var& operator=(const mpt_var& right) \
{ \
    mhash_var::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)
/**
 \def   MHASHCONST(t, b)
 \brief A macro that should be placed in derived classes to implement the correct constructors,
        assignment operators and other methods. This should be used when deriving from
        v_mhash_var, v_mhash_be_int_var, v_mhash_le_int_var or one of the typedefs.
 \param     t   Class name of the derived class.
 \param     b   Baseclass, either v_mhash_var, v_mhash_be_int_var, v_mhash_le_int_var or one of the typedefs.
 */
#define MHASHCONST(t, b) t(const t& arg) : b(arg) \
{ \
    child_copy_constructor(arg); \
} \
t() : b() \
{ \
    pop(); \
} \
const t& operator=(const t& right) \
{ \
    b::operator=(right); \
    return *this; \
} \
virtual const mpt_var& operator=(const mpt_var& right) \
{ \
    b::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)

#endif /* _MHASH_VAR_H_ */

/******************************************************************/
/* Copyright DSO National Laboratories 2019. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _COUNTER_VAR_H_
#define _COUNTER_VAR_H_
#include "chk_var.h"

/**
 \class   counter_var
 \brief   Counter/sequence number check for packets
 \details Checks counter or sequence number and discards duplicate or
          old packets. The current default is to reject packets that are
          up to 100 sequence number older than the last received packet.

          Warning: setInvalid will reset the counter.
          Note: Unlike most checksums, counters are in front, ie. position 2
 */
class RM_EXPORT counter_var : public chk_var
{
public:
    /**
     \fn    counter_var::counter_var(unsigned int counter_id = 0, unsigned int counter_sz = 2U, unsigned char chk_loc = 2U, unsigned char front_off = 0, unsigned char back_off = 0);
     \brief Constructor
     \param     counter_id  (Optional) Identifier for the counter. This is to allow different counters
                            for different groups of messages.
     \param     counter_sz  (Optional) Size of the counter in bytes. This will be treated as unsigned
                            for roll-over.
     \param     chk_loc     (Optional) The check location. Default is 2, which is at the front.
     \param     front_off   (Optional) The front offset. Default is 0.
     \param     back_off    (Optional) The back offset. Default is 0.
     */
    counter_var(unsigned int counter_id = 0, unsigned int counter_sz = 2U, unsigned char chk_loc = 2U, unsigned char front_off = 0, unsigned char back_off = 0);
    /**
     \fn    counter_var::counter_var(const counter_var& right);
     \brief Copy constructor, copies the check properties and values of its children.
     \param     right   The object to be copied.
     */
    counter_var(const counter_var& right);
    /**
     \fn    virtual int counter_var::extract(int len, const unsigned char* buf);
     \brief Extracts its data from a byte stream by checking the counter. The override is needed
            because there is only one make_checksum() but the counter to produce is different
            for extract and output as they are separate and independent.
     \param     len The length of the buffer in bytes.
     \param     buf The buffer.
     \returns   Number of bytes remaining that was not extracted.
     */
    virtual int extract(int len, const unsigned char* buf);
    /**
     \fn    virtual bool counter_var::setInvalid();
     \brief Sets the counter back to 0, and also calls setInvalid() on all children.
     \returns   True if it succeeds, false if it fails.
     */
    virtual bool setInvalid();
    /**
     \fn    virtual void counter_var::setRMDirty();
     \brief Overloaded in order to detect a new packet to be output so that the output
            counter can be increased on the next call. This is to allow redundant outputs
            to function correctly.
     */
    virtual void setRMDirty();
    /**
     \fn    virtual int counter_var::checksum_size() const;
     \brief Counter size in bytes.
     \returns   An int.
     */
    virtual int  checksum_size() const;
    /**
     \fn    virtual void counter_var::make_chksum(int len, const unsigned char* buf, outbuf& strm);
     \brief Returns a counter. There are separate counters for input and output and for
            different IDs. The buffer is ignored.
     \param          len     The length of the buffer.
     \param          buf     The buffer, which is ignored.
     \param [out]    strm    The output buffer for the selected counter.
     */
    virtual void make_chksum(int len, const unsigned char* buf, outbuf& strm);
    /**
     \fn    const counter_var& counter_var::operator=(const counter_var& right);
     \brief Assignment operator. Copies the values of its children.
     \param     right   The right object to be copied from.
     \returns   A reference of this object.
     */
    const counter_var& operator=(const counter_var& right);
    /**
     \fn    const mpt_var& counter_var::operator=(const mpt_var& right);
     \brief Assignment operator. Copies the values of its children if casting succeeds.
     \param     right   The right object to be copied from.
     \returns   A reference of this object.
     */
    /*reimp*/ const mpt_var& operator=(const mpt_var& right);

protected:

    /**
     \fn    virtual bool counter_var::check(const unsigned char* left, const unsigned char* right, int len);
     \brief Compares the result of make_chksum() and the packet checksum. This will return true
            if the right checksum is equal or up to 100 counts after the left checksum. It takes
            into account roll-over.
     \param     left    The counter that was computed.
     \param     right   The counter extracted from the packet.
     \param     len     The length to compare in bytes.
     \returns   True if it compares favourably.
     */
    virtual bool check(const unsigned char* left, const unsigned char* right, int len);

    unsigned int id;    /*!< The identifier */
    unsigned int sz;    /*!< The size */
    bool in_extract;    /*!< True to in extract */
    bool cached;    /*!< True if cached */
    unsigned int cached_output_counter; /*!< The cached output counter */

};

/**
  \class   ext_counter_var
  \brief   Template class to use additional counters and/or non-default byte size (of 2)
  \tparam  cid The counter ID
  \tparam  csz The counter size in bytes
  \tparam  cl  The check location enumeration (see chk_var)
  \tparam  fo  The front offset (see chk_var)
  \tparam  bo  The back offset (see chk_var)
 */
template<int cid, int csz, int cl = 2, int fo = 0, int bo = 0>
class ext_counter_var : public counter_var
{
public:
    /**
     \fn    ext_counter_var::ext_counter_var()
     \brief Default constructor
     */
    ext_counter_var() : counter_var(cid, csz, cl, fo, bo)
    {
    }
    /**
     \fn    ext_counter_var::ext_counter_var(const counter_var& right)
     \brief Copy constructor, copies the check properties and values of its children.
     \param     right   The object to be copied.
     */
    ext_counter_var(const counter_var& right) : counter_var(right)
    {
    }
    /**
     \fn    const ext_counter_var& ext_counter_var::operator=(const ext_counter_var& right)
     \brief Assignment operator. Copies the values of its children.
     \param     right   The right object to be copied from.
     \returns   A reference of this object.
     */
    const ext_counter_var& operator=(const ext_counter_var& right)
    {
        counter_var::operator=(static_cast<const counter_var&>(right));
        return *this;
    }
    /**
     \fn    const mpt_var& ext_counter_var::operator=(const mpt_var& right)
     \brief Assignment operator. Copies the values of its children if casting succeeds.
     \param     right   The right object to be copied from.
     \returns   A reference of this object.
     */
    /*reimp*/ const mpt_var& operator=(const mpt_var& right)
    {
        return counter_var::operator=(right);
    }
};

/**
\def   EXTCOUNTERCONST(t, cntid, cntsz, cl, front, back)
\brief A macro that should be placed in derived classes to implement the correct constructors,
       assignment operators and other methods. This should be used when deriving from counter_var.
\param     t       Class name of the derived class.
\param     cntid   The counter ID.
\param     cntsz   The counter size in bytes.
\param     loc     The check location.
\param     front   The front offset.
\param     back    The back offset.
*/
#define EXTCOUNTERCONST(t, cntid, cntsz, cl, front, back) t(const t& arg) : counter_var(arg) \
{ \
        child_copy_constructor(arg); \
} \
t() : counter_var(cntid, cntsz, cl, front, back) \
{ \
    pop(); \
} \
const t& operator=(const t& right) \
{ \
    counter_var::operator=(right); \
    return *this; \
} \
const mpt_var& operator=(const mpt_var& right) \
{ \
    counter_var::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)

/**
\def   COUNTERCONST(t)
\brief A macro that should be placed in derived classes to implement the correct constructors,
       assignment operators and other methods. This should be used when deriving from
       counter_var.
\param     t   Class name of the derived class.
 */
#define COUNTERCONST(t) t(const t& arg) : counter_var(arg) \
{ \
        child_copy_constructor(arg); \
} \
t() : counter_var() \
{ \
    pop(); \
} \
const t& operator=(const t& right) \
{ \
    counter_var::operator=(right); \
    return *this; \
} \
const mpt_var& operator=(const mpt_var& right) \
{ \
    counter_var::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)
#endif /*_COUNTER_VAR_H_*/
/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _MUX_SHOW_H_
#define _MUX_SHOW_H_

#include <mpt_show.h>

struct mux_show_private;

//! Observer class capable of connecting to arrays of observed objects. This class can also distinguish which array position triggered.
class RM_EXPORT mux_show : public mpt_baseshow
{
public:
    mux_show(mpt_var* st = (mpt_var*)0, unsigned int cnt = 0, mpt_var* off = (mpt_var*)0);
    virtual ~mux_show();
    virtual void setDirty(mpt_var*);
    mpt_var* advance(mpt_var* ptr, unsigned int off);
    mpt_var* ref(unsigned int index);
    int getDirty();

protected:
    mux_show_private* prvt;
    mpt_var* array_start;
    unsigned int array_cnt;
    unsigned long offset;
    unsigned long elem_offset;

};

#endif /* _MUX_SHOW_H_ */

/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _DELYSHOW_H_
#define _DELYSHOW_H_

#include <mpt_show.h>

//! This class implements a delayed trigger after the observable changes state.
class RM_EXPORT delay_show : public mpt_baseshow
{
public:
    delay_show(unsigned int _sec, unsigned int _usec);
    virtual void setDirty(mpt_var*);
    bool getDirty();
    static void getTime(unsigned int& _sec, unsigned int& _usec);
protected:
    unsigned int sec;
    unsigned int usec;
    unsigned int trigger_sec;
    unsigned int trigger_usec;
};

#endif /* _DELYSHOW_H_ */

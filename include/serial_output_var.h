#ifndef _SERIAL_OUTPUT_VAR_H_
#define _SERIAL_OUTPUT_VAR_H_

#include <cplx_var.h>

class serial_proxy;

class RM_EXPORT serial_output_var : public complex_var
{
public:
    serial_output_var(const char* hdr, unsigned short hdrlen=0, unsigned short fixmsglen=0,
                      unsigned short msglenoffset=0, unsigned short msglenwidth=0, unsigned short msglenbias=0,
                      const char* trailer=0, unsigned short trailerlen=0, unsigned short msgoffset=0,
                      int interf_num=0, bool dle_flg=false);
    ~serial_output_var();
    void refreshRM();
    bool isRMDirty() const;
    void setProxy(serial_proxy* p, bool owner=false);

    // New Virtual Functions
    virtual int send(const char* buf, int len);

    // Overloaded Virtual Functions
    /*reimp*/ void setRMDirty();
    /*reimp*/ const mpt_var& operator=(const mpt_var& right);
    /*reimp*/ mpt_var* getNext();
    /*reimp*/ istream& operator>>(istream& s);
    /*reimp*/ ostream& operator<<(ostream& s) const;

    // Needed to prevent automatic assignment generation
    const serial_output_var& operator=(const serial_output_var& right);

private:
    serial_output_var();

protected:
    const char* m_hdr;
    unsigned short m_hdrlen;
    unsigned short m_fixmsglen;
    unsigned short m_msglenoffset;
    unsigned short m_msglenwidth;
    unsigned short m_msglenbias;
    const char* m_trailer;
    unsigned short m_trailerlen;
    unsigned short m_msgoffset;
    bool m_dle_flag;
    bool dirty;
    serial_proxy* proxy;
    bool proxy_owner;

};

inline bool serial_output_var::isRMDirty(void) const
{
    return dirty;
}

#define STD_SERIALOUTPUTVAR_METHODS(t) \
const t& operator=(const t& right) \
{ \
    serial_output_var::operator=(right); \
    return *this; \
} \
const mpt_var& operator=(const mpt_var& right) \
{ \
    serial_output_var::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)

#define SERIALOUTPUTCONST(t,hdr,hdrlen,fix,mlof,mlwi,mlbi,tlr,tlrlen,msgof,ifnum,ddle,sdraddr) t() : serial_output_var(hdr,hdrlen,fix,mlof,mlwi,mlbi,tlr,tlrlen,msgof,ifnum,ddle) \
{ \
    pop(); \
} \
t(mpt_var &link) : serial_output_var(hdr,hdrlen,fix,mlof,mlwi,mlbi,tlr,tlrlen,msgof,ifnum,ddle) \
{ \
    pop(); \
    *((*this)[0]) = link; \
} \
STD_SERIALOUTPUTVAR_METHODS(t)

#endif /* _SERIAL_OUTPUT_VAR_H_ */
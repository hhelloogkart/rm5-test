/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _STR_VAR_H_
#define _STR_VAR_H_

#include <outbuf.h>
#include <vlu_var.h>
#include <string.h>

extern RM_EXPORT void string_stream_helper_read(istream& s, char*, int);
extern RM_EXPORT void string_stream_helper_write(ostream& s, const char*, int);
extern RM_EXPORT size_t helper_strlen(const char*, size_t);

#ifdef __INTEGRITY
#pragma ghs nowarning 128       //loop is not reachable from preceding code
#endif

// The string template var
template<size_t sz>
class string_var : public value_var
{
protected:
    char data[sz+1];

public:
    RUNTIME_TYPE(string_var<sz>)
    string_var() : value_var()
    {
        data[0]  = '\0';
        data[sz] = '\0';
    }
    string_var(const value_var& right) : value_var()
    {
        const char* r = right.to_const_char_ptr();
        char* p = data;
        char* q = data + helper_strlen(r, sz);
        while (p != q)
        {
            *p = *r;
            ++p;
            ++r;
        }
        data[sz] = '\0';
    }
    string_var(const char* right) : value_var()
    {
        char* p = data;
        char* q = data + sz;

        while (p != q)
        {
            *p = *right;
            if (*p == '\0')
            {
                break;
            }
            ++p;
            ++right;
        }
        data[sz] = '\0';
    }
    string_var(const string_var<sz>& right) : value_var()
    {
        char* p = data;
        char* q = data + sz;
        const char* r = right.data;
        while (p != q)
        {
            *p = *r;
            ++p;
            ++r;
        }
        data[sz] = '\0';
    }
    ~string_var()
    {
    }
    virtual int extract(int buflen, const unsigned char* buf)
    {
        int cnt1, max1 = (static_cast< size_t >(buflen)) > sz ? sz : (buflen);

        for (cnt1 = 0; cnt1 < max1; ++cnt1)
        {
            if ((unsigned char)data[cnt1] != buf[cnt1])
            {
                goto svchg0;
            }
        }
        if ((max1 < sz) && data[max1] != '\0')
        {
            data[max1] = '\0';
            mpt_var::setDirty();
        }
        else
        {
            mpt_var::notDirty();
        }
        return buflen - max1;

        for (; cnt1 < max1; ++cnt1)
        {
svchg0:
            data[cnt1] = buf[cnt1];
        }
        mpt_var::setDirty();
        return buflen - max1;
    }
    virtual int size() const
    {
        return sz;
    }
    virtual void output(outbuf& strm)
    {
        char* p = data;
        char* q = data + (sz * sizeof(char));
        while (p != q)
        {
            strm += *p;
            ++p;
        }
    }
    virtual bool isValid() const
    {
        return data[0] != 0;
    }
    virtual bool setInvalid()
    {
        if (isValid())
        {
            data[0] = '\0';
            setDirty();
            return true;
        }
        notDirty();
        return false;
    }
    virtual mpt_var* getNext()
    {
        return this + 1;
    }
    const char* operator=(const char* right)
    {
        char* p = data;
        char* q = data + sz;

        // Need to check if there is any change
        while (p != q)
        {
            if (*p != *right)
            {
                goto svchg1;
            }
            if (*p == '\0')
            {
                break;
            }
            ++p;
            ++right;
        }
        mpt_var::notDirty();
        return data;

        // There are changes
        while (p != q)
        {
svchg1:
            *p = *right;
            if (*p == '\0')
            {
                break;
            }
            ++p;
            ++right;
        }
        mpt_var::setDirty();
        mpt_var::setRMDirty();
        return data;
    }
    virtual const mpt_var& operator=(const mpt_var& right)
    {
        char* p = data;
        char* q;
        const char* r;

        const string_var<sz>* gen1 = RECAST(const string_var<sz>*,&right);
        if (gen1 == 0)
        {
            const value_var* gen2 = RECAST(const value_var*,&right);
            if (gen2 == 0)
            {
                return *this;
            }
            r = gen2->to_const_char_ptr();
            q = data + helper_strlen(r, sz);
        }
        else
        {
            r = gen1->data;
            q = data + sz;
        }

        // Need to check if there is any change
        while (p != q)
        {
            if (*p != *r)
            {
                goto svchg2;
            }
            ++p;
            ++r;
        }
        mpt_var::notDirty();
        return *this;

        // There are changes
        while (p != q)
        {
svchg2:
            *p = *r;
            ++p;
            ++r;
        }
        mpt_var::setDirty();
        mpt_var::setRMDirty();
        return *this;
    }
    const string_var<sz>& operator=(const string_var<sz>& right)
    {
        char* p = data;
        char* q = data + sz;
        const char* r = right.data;

        // Need to check if there is any change
        while (p != q)
        {
            if (*p != *r)
            {
                goto svchg3;
            }
            ++p;
            ++r;
        }
        mpt_var::notDirty();
        return *this;

        // There are changes
        while (p != q)
        {
svchg3:
            *p = *r;
            ++p;
            ++r;
        }
        mpt_var::setDirty();
        mpt_var::setRMDirty();
        return *this;
    }
    bool operator==(const string_var<sz>& right) const
    {
        return memcmp(this->data, right.data, sz+1)==0;
    }
    virtual bool operator==(const mpt_var& right) const
    {
        const string_var<sz>* gen1 = RECAST(const string_var<sz>*,&right);
        if (gen1 == 0)
        {
            const value_var* gen2 = RECAST(const value_var*, &right);
            if (gen2 == 0)
            {
                return mpt_var::operator==(right);
            }
            return operator==(gen2->to_const_char_ptr());
        }
        else
        {
            return operator==(*gen1);
        }
    }
    bool operator==(const char* right) const
    {
        const char* p = data;
        const char* q = data + sz + 1; // include null

        while (p != q)
        {
            if (*p != *right)
            {
                return false;
            }
            if (*p == '\0')
            {
                break;
            }
            ++p;
            ++right;
        }
        return true;
    }
    bool operator==(char* right) const
    {
        return operator==((const char*)right);
    }
    operator const char* () const
    {
        return data;
    }
    operator char* ()
    {
        //  mpt_var::setDirty();
        //  mpt_var::setRMDirty();
        return data;
    }
    virtual char to_char() const
    {
        char val;
        str_to_value(data, val);
        return val;
    }
    virtual unsigned char to_unsigned_char() const
    {
        unsigned char val;
        str_to_value(data, val);
        return val;
    }
    virtual short to_short() const
    {
        short val;
        str_to_value(data, val);
        return val;
    }
    virtual unsigned short to_unsigned_short() const
    {
        unsigned short val;
        str_to_value(data, val);
        return val;
    }
    virtual int to_int() const
    {
        int val;
        str_to_value(data, val);
        return val;
    }
    virtual unsigned int to_unsigned_int() const
    {
        unsigned int val;
        str_to_value(data, val);
        return val;
    }
    virtual long to_long() const
    {
        long val;
        str_to_value(data, val);
        return val;
    }
    virtual unsigned long to_unsigned_long() const
    {
        unsigned long val;
        str_to_value(data, val);
        return val;
    }
    virtual long long to_long_long() const
    {
        long long val;
        str_to_value(data, val);
        return val;
    }
    virtual unsigned long long to_unsigned_long_long() const
    {
        unsigned long long val;
        str_to_value(data, val);
        return val;
    }
    virtual float to_float() const
    {
        float val;
        str_to_value(data, val);
        return val;
    }
    virtual double to_double() const
    {
        double val;
        str_to_value(data, val);
        return val;
    }
    virtual char* to_char_ptr()
    {
        return data;
    }
    virtual const char* to_const_char_ptr() const
    {
        return data;
    }
    virtual void from_char(char v)
    {
        this->operator=((const char*)value_to_str(v));
    }
    virtual void from_unsigned_char(unsigned char v)
    {
        this->operator=((const char*)value_to_str(v));
    }
    virtual void from_short(short v)
    {
        this->operator=((const char*)value_to_str(v));
    }
    virtual void from_unsigned_short(unsigned short v)
    {
        this->operator=((const char*)value_to_str(v));
    }
    virtual void from_int(int v)
    {
        this->operator=((const char*)value_to_str(v));
    }
    virtual void from_unsigned_int(unsigned int v)
    {
        this->operator=((const char*)value_to_str(v));
    }
    virtual void from_long(long v)
    {
        this->operator=((const char*)value_to_str(v));
    }
    virtual void from_unsigned_long(unsigned long v)
    {
        this->operator=((const char*)value_to_str(v));
    }
    virtual void from_long_long(long long v)
    {
        this->operator=((const char*)value_to_str(v));
    }
    virtual void from_unsigned_long_long(unsigned long long v)
    {
        this->operator=((const char*)value_to_str(v));
    }
    virtual void from_float(float v)
    {
        this->operator=((const char*)value_to_str(v));
    }
    virtual void from_double(double v)
    {
        this->operator=((const char*)value_to_str(v));
    }
    virtual void from_char_ptr(const char* s)
    {
        this->operator=(s);
    }
    virtual istream& operator>>(istream& s)
    {
        string_stream_helper_read(s, data, sz);
        mpt_var::setDirty();
        mpt_var::setRMDirty();
        return s;
    }
    virtual ostream& operator<<(ostream& s) const
    {
        string_stream_helper_write(s, data, sz);
        return s;
    }
    char& operator[](int _sz)
    {
        static char tc = '\0';
        return (_sz < (int)sz) ? data[_sz] : tc;
    }

};

#endif /* _STR_VAR_H_ */

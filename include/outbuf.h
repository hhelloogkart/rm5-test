/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/
#ifndef _OUTBUF_H_
#define _OUTBUF_H_

#include <rmglobal.h>

class cache_typ;

class RM_EXPORT outbuf
{
public:
    outbuf();
    void set(unsigned char* _buf, unsigned int _len);
    void set(cache_typ* _cache);
    void set(cache_typ* _cache, unsigned int _len);
    void append(const unsigned char* _buf, unsigned int _len);
    void append(const unsigned char* _buf);
    void append(unsigned int fillcnt, unsigned char fillch);
    void append(const char* _buf, unsigned int _len);
    void append(const char* _buf);
    void append(unsigned int fillcnt, char fillch);
    unsigned char* get() const;
    unsigned char* getcur() const;
    cache_typ* getcache() const;
    void clr();
    void setsize(unsigned _sz);
    unsigned size() const;
    unsigned maxsize() const;
    void operator+=(unsigned char ch);
    void operator+=(outbuf& b);
protected:
    cache_typ* cache;
    unsigned char* start;
    unsigned char* buf;
    unsigned len;
};

#endif /* _OUTBUF_H_ */

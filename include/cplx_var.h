/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _CPLX_VAR_H_
#define _CPLX_VAR_H_

#include <mpt_var.h>

struct complex_var_private;

/**
 \class complex_var
 \brief   Composite class that acts as a container for mpt_var objects.
 \details This is a base class for complex data types. A complex_var may have zero
          or more child entity objects. It will keep track of all its children and
          many of its overloaded functions such as extract, output, size will call
          the corresponding functions in each of its children. The
          complex_var / mpt_var implement the composite design pattern.
 */
class RM_EXPORT complex_var : public mpt_var
{
public:
    friend class mpt_var;
    /**
     \fn    complex_var::complex_var();
     \brief Default constructor, it performs a push of itself into the create stack so that when
            derived class mpt_var children instantiate, it will recognise this object as the parent.
            The derived class constructor needs to pop itself from the create stack, but this is
            normally part of the macro provided.
     */
    complex_var();
    /**
     \fn    complex_var::complex_var(const complex_var& cpy);
     \brief Copy constructor. The same create stack mechanisms apply as the default constructor. The
            values of all its children will be copied from the copy provided in the argument.
     \param     cpy The copy.
     */
    complex_var(const complex_var& cpy);
    /**
     \fn    virtual complex_var::~complex_var();
     \brief Destructor
     */
    virtual ~complex_var();
    /**
     \fn    virtual void complex_var::refresh();
     \brief Extends the base class method to calls all mpt_show callbacks to perform operator()
            processing for all children as well.
     */
    virtual void refresh();
    /**
     \fn    virtual int complex_var::extract(int len, const unsigned char* buf);
     \brief Calls every child to extract their data from a byte stream. Each child is called in turn,
            from the first to the last. The remaining bytes leftover is passed as the buffer length
            to the subsequent child.
     \param     len The length.
     \param     buf The buffer.
     \returns   Number of bytes remaining that was not extracted.
     */
    virtual int extract(int len, const unsigned char* buf);
    /**
     \fn    virtual int complex_var::size() const;
     \brief Returns the size in bytes that this object will need to serialize itself into a byte
            stream. It calls and sums up the size of each of its children.
     \returns   Size in bytes.
     */
    virtual int size() const;
    /**
     \fn    virtual void complex_var::output(outbuf& strm);
     \brief Outputs data into a byte stream writing into outbuf. The number of bytes written must be
            equal to size(). Each child's output() is called in turn from the first to the last,
            which will write into outbuf.
     \param [out]   strm    The byte stream where bytes are pushed into.
     */
    virtual void output(outbuf& strm);
    /**
     \fn    virtual bool complex_var::isValid() const;
     \brief Query if this object contains data that is valid. It is valid only when all children are
            valid, hence it calls each child in turn to check. In this implementation, it is assumed
            that there will be at least one child, otherwise it will just return true for the zero
            children case.
     \returns   True if valid, false if not.
     */
    virtual bool isValid() const;
    /**
     \fn    virtual bool complex_var::setInvalid();
     \brief Calls each child in turn to resets the data to invalid. As long as one child successfully
            resets and returns true, the return value will be true.
     \returns   True if it succeeds, false if it fails.
     */
    virtual bool setInvalid();
    /**
     \fn    virtual const mpt_var& complex_var::operator=(const mpt_var& right);
     \brief Assignment operator, virtual override so that it works without needing to upcast. It
            attempts to cast the argument to a complex_var and if successful, it will assign the
            values of each child in turn.
     \param     right   The object to be copied from, a deep copy will be made if the type is
                        correct. If the value is changed, setDirty will be called.
     \returns   A const reference to this object.
     */
    virtual const mpt_var& operator=(const mpt_var& right);
    /**
     \fn    virtual bool complex_var::operator==(const mpt_var& right) const;
     \brief Equality operator, virtual override so that it works without needing to upcast. It
            attempts to cast the argument to a complex_var and if successful, it will test for
            equivalence in every child. If it is unable to cast, or if any child returns false, this
            function will return false.
     \param     right   The right object to be compared with.
     \returns   True if the parameters are considered equivalent.
     */
    virtual bool operator==(const mpt_var& right) const;
    /**
     \fn    virtual istream& complex_var::operator>>(istream& s);
     \brief Stream extraction operator that calls each child read data from the stream.
     \param [out]   s   an istream to process.
     \returns   The input stream.
     */
    virtual istream& operator>>(istream& s);
        /**
     \fn    virtual ostream& complex_var::operator<<(ostream& s);
     \brief Stream insertion operator that calls each child in turn to output data
            into the stream.
     \param [out]    s   an ostream to process.
     \returns   The output stream.
     */
    virtual ostream& operator<<(ostream& s) const;  /*!< . */
    /**
     \fn    virtual int complex_var::cardinal() const;
     \brief Gets the number of children or cardinal
     \returns   Number of children.
     */
    virtual int cardinal() const;
    /**
     \fn    virtual void complex_var::resize(int nsize);
     \brief A placeholder for derived classes to override if applicable. Used to resize the number of
            children. This implementation fails with an assert exception.
     \param     nsize   The nsize.
     */
    virtual void resize(int nsize);
    /**
     \fn    const complex_var& complex_var::operator=(const complex_var& right);
     \brief Assignment operator. Assignment will be called for each child in turn.
     \param     right   Object to copy the data from.
     \returns   A const reference to this object.
     */
    const complex_var& operator=(const complex_var& right);
    /**
     \fn    bool complex_var::operator==(const complex_var& right) const;
     \brief Equality operator. Returns true if both have the same number of children, and the
            equality operator returns true for each child.
     \param     right   The right object to compare with.
     \returns   True if the parameters are considered equivalent.
     */
    bool operator==(const complex_var& right) const;
    /**
     \fn    void complex_var::add(mpt_var* parameter1);
     \brief Associate the object passed as argument as a child to this object. Added to the end.
            Normally called by the child passing itself when there is an object on the create state
            on instantiation.
     \param [in]    child  The pointer to the child to add.
     */
    void add(mpt_var*);
    /**
     \fn    void complex_var::remove(int parameter1);
     \brief Delete the object passed as argument as a child to this object. The child is removed and
            deleted.
     \param     childidx  The zero based index of the child to remove
     */
    void remove(int);
    /**
     \fn    void complex_var::remove(mpt_var* parameter1);
     \brief Delete the object passed as argument as a child to this object. The child is removed and
            deleted.
     \param [in]    child  The pointer to the child to remove.
     */
    void remove(mpt_var*);
    /**
     \fn    void complex_var::insert(mpt_var* parameter1, int parameter2);
     \brief Inserts the object passed as argument as a child to this object.
     \param [in]    child  The pointer to the child to insert.
     \param         childidx The index of this child after addition.
     */
    void insert(mpt_var*, int);
    /**
     \fn    mpt_var* complex_var::operator[](int parameter1) const;
     \brief Array indexer operator
     \param     childidx  The zero based index of the child to get.
     \returns   The indexed child.
     */
    mpt_var* operator[](int) const;
    /**
     \fn    void complex_var::disown(mpt_var* child);
     \brief Disowns the given child without deleting
     \param [in]    child   The pointer to the child to disown.
     */
    void disown(mpt_var* child);

protected:

    /**
     \fn    void complex_var::child_copy_constructor(const complex_var& gen);
     \brief Helper function called by derived classes to assign the values of all children during the
            copy construction of the derived class.
     \param     gen The complex_var to copy from.
     */
    void child_copy_constructor(const complex_var& gen);
    /**
     \fn    void complex_var::delete_children();
     \brief Removes and deletes all children
     */
    void delete_children();
    /**
     \fn    void complex_var::shuffle_index(int parameter1);
     \brief Move the child at the end to the given index
     \param     childidx  The zero based index to move the child to.
     */
    void shuffle_index(int);

    complex_var_private* prvt;  /*!< The prvt */

};
/**
 \fn    extern RM_EXPORT void vld1(int parameter1, const unsigned char* parameter2, const char* parameter3, unsigned int);
 \brief Helper function used by the log data macros to log in extract. Not to be called directly.
 \param     len,  Length of the buffer to log.
 \param     buf   Pointer to buffer.
 \param     name  Name of message.
 \param     rtti  Rtti of message.
 */
extern RM_EXPORT void vld1(int, const unsigned char*, const char*, unsigned int);
/**
 \class vld2
 \brief Helper temporary class used by log data macros to log in output. Not to be instantiated
        anywhere else.
 */
class RM_EXPORT vld2
{
public:

    /**
     \fn    vld2::vld2(outbuf& ob, const char* nm, unsigned int rtti);
     \brief Constructor, needs to keep track of the current location in output buffer.
     \param [in,out]    ob      Output buffer.
     \param             nm      Name of message. (Shallow copy)
     \param             rtti    Rtti of message.
     */
    vld2(outbuf& ob, const char* nm, unsigned int rtti);
    /**
     \fn    vld2::~vld2();
     \brief Destructor, needs to log data from current output buffer location to previously stored.
     */
    ~vld2();

private:
    unsigned char* p;   /*!< Pointer to beginning of output buffer */
    const char* nm; /*!< Name of message */
    unsigned int rtti;  /*!< Rtti of message */
    outbuf& ob; /*!< Output buffer */
};
/**
 \def   COMPLEXCONST(t)
 \brief A macro that should be placed in derived classes to implement the correct constructors,
        assignment operators and other methods.
 \param     t   Class name of the derived class.
 */
#define COMPLEXCONST(t) t(const t& arg) \
      : complex_var() \
{ \
    child_copy_constructor(arg); \
} \
t() \
{ \
    pop(); \
} \
const t& operator=(const t& right) \
{ \
    complex_var::operator=(right); \
    return *this; \
} \
const mpt_var& operator=(const mpt_var& right) \
{ \
    complex_var::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)
/**
 \def   LOGDATA(name)
 \brief A macro that can be placed in any complex_var derived class to automatically record
        datamon compatible log files, if datamonlog is enabled in the configuration file. This
        macro assumes that extract and output will not be overriden in the derived class using
        this macro. Otherwise, use the following two macros.
 \param     name    The variable name.
 */
#define LOGDATA(name) \
int extract(int len, const unsigned char* buf) \
{ \
    vld1(len, buf, name, rtti()); \
    return complex_var::extract(len, buf); \
} \
void output(outbuf &strm) \
{ \
    vld2 vld2_(strm, name, rtti()); \
    complex_var::output(strm); \
}
/**
 \def   LOGEXTRACT(name)
 \brief A macro that can be placed within the extract method to record datamon compatible log
        files for all incoming data.
 \param     name    The varible name.
 */
#define LOGEXTRACT(name) vld1(len, buf, name, rtti());
/**
 \def   LOGOUTPUT(name)
 \brief A macro that can be placed without the output method to record datamon compatible log
        files for all outgoing data.
 \param     name    The variable name.
 */
#define LOGOUTPUT(name) vld2 vld2_(strm, name, rtti());
#endif /* _CPLX_VAR_H_ */

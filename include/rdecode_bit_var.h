/******************************************************************/
/* Copyright DSO National Laboratories 2016. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _RDECODE_BIT_VAR_H_
#define _RDECODE_BIT_VAR_H_

#include <decode_var.h>

//! The reverse bit decode variable
/*!
    This decode data to a double but extract and packs to the number
    of bits specified in the constructor, in reverse bit order.
 */
class RM_EXPORT rdecode_bit_var : public vdecode_var
{
public:
    rdecode_bit_var(int _bit_size, double _range, double _offset = 0.0, int _scale_offset = 1);
    rdecode_bit_var(int _bit_size, int _word_size, double _range, double _offset = 0.0, int _scale_offset = 1);
    virtual const mpt_var& operator=(const mpt_var& right);
    double operator=(double right);
    const rdecode_bit_var& operator=(const rdecode_bit_var& right);

private:
    explicit rdecode_bit_var(const rdecode_bit_var& cpy);
};

template<typename C>
class templ_rdecode_bit_var : public rdecode_bit_var
{
public:
    templ_rdecode_bit_var() : rdecode_bit_var(C::SIZE, 8, C::RANGE, C::OFFSET, C::SCALE_OFFSET)
    {
    }
    double operator=(double right)
    {
        return generic_var<double>::operator=(right);
    }
    const templ_rdecode_bit_var<C>& operator=(const templ_rdecode_bit_var<C>& right)
    {
        generic_var<double>::operator=(right);
        return *this;
    }
    virtual const mpt_var& operator=(const mpt_var& right)
    {
        return generic_var<double>::operator=(right);
    }
private:
    explicit templ_rdecode_bit_var(const templ_rdecode_bit_var<C>&);
};
template<typename C>
class templw_rdecode_bit_var : public rdecode_bit_var
{
public:
    templw_rdecode_bit_var() : rdecode_bit_var(C::SIZE, C::WORD_SIZE, C::RANGE, C::OFFSET, C::SCALE_OFFSET)
    {
    }
    double operator=(double right)
    {
        return generic_var<double>::operator=(right);
    }
    const templw_rdecode_bit_var<C>& operator=(const templw_rdecode_bit_var<C>& right)
    {
        generic_var<double>::operator=(right);
        return *this;
    }
    virtual const mpt_var& operator=(const mpt_var& right)
    {
        return generic_var<double>::operator=(right);
    }
private:
    explicit templw_rdecode_bit_var(const templw_rdecode_bit_var<C>&);
};
#define DEFINE_RDECODE_BIT_VAR(clsnm, byte_size, range, offset, soffset)  \
    class clsnm : public rdecode_bit_var \
    { \
public: \
        clsnm() : rdecode_bit_var(byte_size, range, offset, soffset) {} \
        mpt_var* getNext() { return this + 1; } \
        double operator=(double right) { \
            return rdecode_bit_var::operator=(right); \
        } \
        const clsnm& operator=(const clsnm& right) { \
            rdecode_bit_var::operator=(right); \
            return *this; \
        } \
        const mpt_var& operator=(const mpt_var& right) { \
            return rdecode_bit_var::operator=(right); \
        } \
    };
#endif /* _RDECODE_BIT_VAR_H_ */

/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _RMGLOBAL_H_
#define _RMGLOBAL_H_

#define RMNAME_LEN  32U
#if defined(__INTEGRITY)
# define MAX_CLIENT  8U
# define VAR_SIZE    1048000U
# define CACHE_SIZE  2096000U
# define QUEUE_SIZE  8192U
#else
# define MAX_CLIENT  32U
# define VAR_SIZE    1826816U
# define CACHE_SIZE  33546240U
# define QUEUE_SIZE  16384U
#endif

#if defined(__INTEGRITY) || defined(__QNXNTO__)
# define FILESIZE    0x800000   //8mb
#else
# define FILESIZE    0x7800000  //120mb
#endif

#define MAGICNUM    0x30101972U
#define CONN_WAIT   2
#define PING_WAIT   1
#define PING_TIME   60
#define READ_BUF    16384
#define WRITE_BUF   16384
#define UDP_BUF     364 /* 1500(MTU) - 14(Ethernet header) - 5*4(IP header) - 2*4(UDP header) = 1458/4=364 */
#define ERR_TOLERANCE 20
#define GETLIST_RETRY 10
#define FLOW_TIMEOUT 1800
#define WAIT_TIME   50
#define FIELD_LEN   2048
#define KEY_SIZE    256

enum
{
    ALIVE = 0,
    BROADCAST,
    SETLIST,
    GETLIST,
    QUERYID,
    CLIENTID,
    REGISTERID,
    REGISTERIDX,
    UNREGISTERIDX,
    FLOWCONTROL,
    MESSAGE,
    ACKNOWLEDGE,
    RESEND,
    RESET,
    ENDCMD
};

#ifdef _WIN32
# pragma warning(disable:4786)
# ifdef LIBRARY
#  ifndef THREAD_PRIVATE
#    define THREAD_PRIVATE
#  endif
#  define RM_EXPORT __declspec(dllexport)
#  define RM_IMPORT __declspec(dllimport)
#  pragma warning( disable : 4251 4275 )
# elif defined(STATIC)
#  ifndef THREAD_PRIVATE
#    ifndef UNDER_RTSS
#      define THREAD_PRIVATE __declspec(thread)
#    else
#      define THREAD_PRIVATE
#    endif
#  endif
#  define RM_EXPORT
#  define RM_IMPORT
# else
#  ifndef THREAD_PRIVATE
#    ifndef UNDER_RTSS
#      define THREAD_PRIVATE __declspec(thread)
#    else
#      define THREAD_PRIVATE
#    endif
#  endif
#  define RM_EXPORT __declspec(dllimport)
#  define RM_IMPORT __declspec(dllimport)
#  pragma warning( disable : 4251 4275 )
# endif
# ifndef SAFESTRINGFUNCS
# define SAFESTRINGFUNCS
# if (_MSC_VER >= 1400)
#  define rm_strcpy(dest,num,src) strcpy_s(dest,num,src)
#  define rm_sprintf(dest,num,txt,arg) sprintf_s(dest,num,txt,arg)
#  define rm_sprintf3(dest,num,txt,arg,arg2,arg3) sprintf_s(dest,num,txt,arg,arg2,arg3)
#  define rm_strcat(dest,num,src) strcat_s(dest,num,src)
#  define rm_strncpy(dst,num,src,cnt) strncpy_s(dst,num,src,cnt)
# else
#  define rm_strcpy(dest,num,src) strcpy(dest,src)
#  define rm_sprintf(dest,num,txt,arg) sprintf(dest,txt,arg)
#  define rm_sprintf3(dest,num,txt,arg,arg2,arg3) sprintf(dest,txt,arg,arg2,arg3)
#  define rm_strcat(dest,num,src) strcat(dest,src)
#  define rm_strncpy(dst,num,src,cnt) strncpy(dst,src,cnt)
# endif
# endif /* SAFESTRINGFUNCS */
#else
# define RM_EXPORT
# define RM_IMPORT
# ifndef THREAD_PRIVATE
#   define THREAD_PRIVATE
# endif
# ifndef SAFESTRINGFUNCS
# define SAFESTRINGFUNCS
# define rm_strcpy(dest,num,src) strcpy(dest,src)
# define rm_sprintf(dest,num,txt,arg) sprintf(dest,txt,arg)
# define rm_sprintf3(dest,num,txt,arg,arg2,arg3) sprintf(dest,txt,arg,arg2,arg3)
# define rm_strcat(dest,num,src) strcat(dest,src)
# define rm_strncpy(dst,num,src,cnt) strncpy(dst,src,cnt)
# endif /* SAFESTRINGFUNCS */
#endif

#ifdef __cplusplus

#ifdef NO_RTTI
#define RECAST(type,variable) (rtti() == (variable)->rtti()) ? reinterpret_cast<type>(variable) : 0
#define BADCAST_BEGIN
#define BADCAST_END
#else
#define RECAST(type,variable) dynamic_cast<type>(variable)
#define BADCAST_BEGIN try {
#define BADCAST_END } catch(bad_cast) {  }

#ifndef OSPACE
# include <exception>
# include <typeinfo>
# ifndef RWSTD_NO_NAMESPACE
using std::bad_cast;
# endif
#else
# include <ospace/std/exception>
# include <ospace/std/typeinfo>
#endif

#endif

inline unsigned int var_rtti(unsigned char)
{
    return 0x11110000;
}
inline unsigned int var_rtti(char)
{
    return 0x00001111;
}
inline unsigned int var_rtti(unsigned short)
{
    return 0x22220000;
}
inline unsigned int var_rtti(short)
{
    return 0x00002222;
}
inline unsigned int var_rtti(unsigned int)
{
    return 0x33330000;
}
inline unsigned int var_rtti(int)
{
    return 0x00003333;
}
inline unsigned int var_rtti(unsigned long)
{
    return 0x44440000;
}
inline unsigned int var_rtti(long)
{
    return 0x00004444;
}
inline unsigned int var_rtti(unsigned long long)
{
    return 0x55550000;
}
inline unsigned int var_rtti(long long)
{
    return 0x00005555;
}
inline unsigned int var_rtti(float)
{
    return 0666660000;
}
inline unsigned int var_rtti(double)
{
    return 0x00006666;
}

#define RUNTIME_TYPE(type) virtual unsigned int rtti() const { return rm_hash_string(#type); }

#endif

#ifdef __cplusplus
extern "C" RM_IMPORT unsigned int hash_string(const char*);
extern "C" RM_EXPORT unsigned int rm_hash_string(const char*);
#else
extern RM_IMPORT unsigned int hash_string(const char*);
extern RM_EXPORT unsigned int rm_hash_string(const char*);
#endif


#endif /* _RMGLOBAL_H_ */

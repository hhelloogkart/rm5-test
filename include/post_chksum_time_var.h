/******************************************************************/
/* Copyright DSO National Laboratories 2019. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _CHKSUM_TIME_VAR_H_
#define _CHKSUM_TIME_VAR_H_

#include <chk_var.h>
//#include <chksum_var.h>
class outbuf;
struct chkts_item;
//! Class for checksums and additional timestamp comparision to prevent replay attack
//template <class T, bool _BE>
class RM_EXPORT chksum_time_var : public chk_var
{
public:
    enum __chksumid
    {
        CHKSUM_TIME_16,     /* 0 */
        CHKSUM_TIME_16b,    /* 1 */
        CHKSUM_TIME_32,     /* 2 */
        CHKSUM_TIME_32b     /* 3 */
    };
    chksum_time_var(int _hash, unsigned char chk_loc, unsigned char front_off, unsigned char back_off);
    chksum_time_var(const chksum_time_var& right);
    ~chksum_time_var();

    /*reimp*/ int extract(int len, const unsigned char* buf);

    const chksum_time_var& operator=(const chksum_time_var& right);
    /*reimp*/ const mpt_var& operator=(const mpt_var& right);

    /*reimp*/ int checksum_size() const;
    /*reimp*/ void make_chksum(int len, const unsigned char* buf, outbuf& strm);

    long long getLastReceivedTimestamp(void);
    void setLagTolerance(float);
    void getFailureCount(int&, int&, int&);
private:
    bool valid_timeinterval(int, const unsigned char* data);
    bool valid_chksum(int, const unsigned char*, const unsigned char*);
    chksum_time_var();
    chkts_item* chkts;

protected:
    int hashtype;
};

//! The template class to access the specific checksum algorithms
template<int ht, unsigned int cl, unsigned int fo, unsigned int bo>
class t_chksum_var : public chksum_time_var
{
public:
    t_chksum_var() : chksum_time_var(ht, cl, fo, bo)
    {
    }
    t_chksum_var(const t_chksum_var& right) : chksum_time_var(right)
    {
    }

    const t_chksum_var& operator=(const t_chksum_var& right)
    {
        chksum_time_var::operator=(static_cast<const chksum_time_var&>(right));
        return *this;
    }
    /*reimp*/ const mpt_var& operator=(const mpt_var& right)
    {
        return chksum_time_var::operator=(right);
    }

    long long getLastReceivedTimestamp(void)
    {
        return chksum_time_var::getLastReceivedTimestamp();
    }

    void setLagTolerance(float tolerance)
    {
        chksum_time_var::setLagTolerance(tolerance);
    }

    void getFailureCount(int& a, int& b, int& c)
    {
        chksum_time_var::getFailureCount(a, b, c);
    }
};


typedef t_chksum_var<chksum_time_var::CHKSUM_TIME_16, 1, 0, 0> post_chksum16le_time_var;
typedef t_chksum_var<chksum_time_var::CHKSUM_TIME_16b, 1, 0, 0> post_chksum16be_time_var;
typedef t_chksum_var<chksum_time_var::CHKSUM_TIME_32, 1, 0, 0> post_chksum32le_time_var;
typedef t_chksum_var<chksum_time_var::CHKSUM_TIME_32b, 1, 0, 0> post_chksum32be_time_var;

#define EXTCHKSUMCONST(t, hash, loc, front, back) t(const t& arg) : chksum_time_var(arg) \
{ \
    child_copy_constructor(arg); \
} \
t() : chksum_time_var(hash, loc, front, back) \
{ \
    pop(); \
} \
const t& operator=(const t& right) \
{ \
    chksum_time_var::operator=(right); \
    return *this; \
} \
const mpt_var& operator=(const mpt_var& right) \
{ \
    chksum_time_var::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)

#define CHKSUMCONST(t, b) t(const t& arg) : b(arg) \
{ \
    child_copy_constructor(arg); \
} \
t() : b() \
{ \
    pop(); \
} \
const t& operator=(const t& right) \
{ \
    b::operator=(right); \
    return *this; \
} \
const mpt_var& operator=(const mpt_var& right) \
{ \
    b::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)

#endif /* _CHKSUM_VAR_H_ */
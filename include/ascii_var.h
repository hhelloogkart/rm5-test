/******************************************************************/
/* Copyright DSO National Laboratories 2010. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _ASCII_VAR_H_
#define _ASCII_VAR_H_

#include <cplx_var.h>

/*!
\class   ascii_var
\brief   Converts ASCII data streams to binary streams for its children
\details Parses ASCII data streams that it receives into binary streams
         for both input and output. The format of the conversion must be
         specified in the constructor. The conversion is bi-directional,
         ASCII to binary in extract, and binary to ASCII in output. Like
         most complex_vars, this class needs to be derived and its children
         specified in the derived class. Macros to simplify the code for
         the derived class are provided.

         Calling size() of this class is expensive as it needs to perform the
         binary to ASCII conversion to determine the size of the ASCII
         stream. Applications should cache this value if possible.

         The conversion string is made up of fields, each which provides
         a mapping from the ASCII to binary. The detailed format is as
         follows:

         - start with the desired delimiter; unacceptable delimiters: ^ and ]
           eg. % or | make good delimiters
         - (\\x[A-F,a-f,0-9][A-F,a-f,0-9]|\d+|\S)([f|d|u|x|s]?)(\d*)%
         - each ascii field is described based on the regexp format above, and made up
           of 3 sections.

         Section 1: Wire Data Format:
         ----------------------------
         Either one of the 3 wire data formats can be specified:

         - \\x[A-F,a-f,0-9][A-F,a-f,0-9] = a double-digit hex escape character describing
           the delimiter to hunt for, this is used for a numerical delimiter, or illegal
           characters
         - \d+ = a number describing the fixed number of bytes to consume
         - \S = a non-numeral decribing the delimiter to hunt for

         The 0 is a special length which means until the end of string

         Section 2: Data Representation (optional):
         ------------------------------------------
         If no format specifiers, nothing is extracted
         - ([f|d|u|x|s]?)(\d*) = one of these format specifiers, if any

                 f4 - 4-byte float
                 f8 - 8-byte double
                 d1 - signed char
                 d2 - signed short
                 d4 - signed int
                 u1 - unsigned char
                 u2 - unsigned short
                 u4 - unsigned int
                 x1 - unsigned char in hex
                 x2 - unsigned short in hex
                 x4 - unsigned int in hex
                 sx - string of len x

         Section 3: Delimiter:
         ---------------------
         - % = delimiter. Whatever was the first char of format.
         - each ascii field is repeated until end of ASCII description

         Note: If not all ascii fields can be matched, the entire ASCII
         string will be discarded.

         Example: *,f4*a*b*c*5f4*,f8*,d1*,d2*,d4*0x4*
                  %C%M%D%!%!f8%!f8%!f8%0f8%

         Reference:
         ----------

             frmt[0] - fix length of ASCII field, if 0 then not fixed
             frmt[1] - delimiter, if variable length
             frmt[2] - data type
             frmt[3] - length of binary field
*/
class RM_EXPORT ascii_var : public complex_var
{
public:

    /**
     \fn    ascii_var::ascii_var(const char* _frmt);
     \brief Constructor. A deep copy of the format conversion string will
            be made.
     \param     _frmt   The format conversion string.
     */
    ascii_var(const char* _frmt);
    /**
     \fn    virtual ascii_var::~ascii_var();
     \brief Destructor. Deallocates all resources.
     */
    virtual ~ascii_var();
    /**
     \fn    virtual int ascii_var::extract(int len, const unsigned char* buf);
     \brief Parses the buffer from ASCII into binary and calls every child
           to extract their data from the binary byte stream.
     \param     len The length.
     \param     buf The buffer.
     \returns   Number of bytes remaining that was not extracted.
     */
    virtual int extract(int len, const unsigned char* buf);
    /**
     \fn    virtual int ascii_var::size() const;
     \brief Returns the size in bytes that this object will need to serialize itself
            into a byte stream. It calls the children to output into a binary stream
            converts it to ASCII and measures the number of bytes that it takes. This
            function will need to call output and is expensive.
     \returns   Size in bytes.
     */
    virtual int size() const;
    /**
     \fn    virtual void ascii_var::output(outbuf& strm);
     \brief Outputs data into a byte stream writing into outbuf. It calls the children
            to output, using complex_var::output(), converts it to ASCII, and writes to
            the byte stream.
     \param [out]    strm    The byte stream where bytes are pushed into.
     */
    virtual void output(outbuf& strm);

private:
    ascii_var();
    char* frmt; /*!< The conversion format */
    int frmtlen;    /*!< The format length in bytes */
    unsigned char* binbuf;  /*!< The binary buffer */
    int binlen; /*!< The binary buffer length in bytes */

};
/**
 \def   ASCIICONST(t,frmt)
 \brief A macro that should be placed in derived classes to implement the correct constructors,
        assignment operators and other methods.
 \param     t     Class name of the derived class.
 \param     frmt  The conversion format string.
 */
#define ASCIICONST(t,frmt) t(const t& arg) \
    : ascii_var(frmt) \
{ \
    child_copy_constructor(arg); \
} \
t() \
    : ascii_var(frmt) \
{ \
    pop(); \
} \
const t& operator=(const t& right) \
{ \
    complex_var::operator=(right); \
    return *this; \
} \
const mpt_var& operator=(const mpt_var& right) \
{ \
    complex_var::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)

#endif

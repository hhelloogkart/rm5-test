/******************************************************************/
/* Copyright DSO National Laboratories 2011. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

// This file is here for compatibility reasons as old code
// will include this file. The definition of r_flex_var is now
// within flex_var.h itself.

#include <flex_var.h>

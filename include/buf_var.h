/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _BUF_VAR_H_
#define _BUF_VAR_H_

#include <rm_var.h>

/**
 \class   buf_var
 \brief   Derived from rm_var that treats data as an uninterpreted
          block of buffer.
 \details This class is a type of rm_var with no children, is it does not
          behave like a composite object to pass on the data to child objects
          for interpretation. It stores everything it receives as an array
          of bytes. As RM data comes in multiples of 32 bit words, the data
          stored in this variable may be padded at the end.
 */
class RM_EXPORT buf_var : public rm_var
{
public:
    RUNTIME_TYPE(buf_var)
    /**
     \fn    buf_var::buf_var(const char nm[], bool _regflag = false);
     \brief Constructor
     \param   nm       RM Name.
     \param   regflag  True to registerId, in order to subscribe for data from RM
     */
    buf_var(const char nm[], bool _regflag = false);
    /**
     \fn    virtual buf_var::~buf_var();
     \brief Destructor
     */
    virtual ~buf_var();

protected:
    unsigned int* data; /*!< The data */
    int sz; /*!< The size of the data in number of 32-bit words */

public:

    /**
     \fn    virtual int buf_var::extract(int len, const unsigned char* buf);
     \brief Extracts its data from a byte stream.
     \param     len The length of the buffer in bytes.
     \param     buf The buffer.
     \returns   Number of bytes remaining that was not extracted.
     */
    virtual int extract(int len, const unsigned char* buf);
    /**
     \fn    virtual int buf_var::size() const;
     \brief Returns the size in bytes that this object will need to serialize
            itself into a byte stream.
     \returns   Size in bytes.
     */
    virtual int size() const;
    /**
     \fn    virtual void buf_var::output(outbuf& strm);
     \brief Outputs data into a byte stream writing into outbuf.
     \param [out]    strm  Output buffer where bytes are pushed into.
     */
    virtual void output(outbuf& strm);
    /**
     \fn    virtual bool buf_var::isValid() const;
     \brief Check if the size of the buffer is non zero.
     \returns   True if the buffer is non zero, false otherwise.
     */
    virtual bool isValid() const;
    /**
     \fn    virtual bool buf_var::setInvalid();
     \brief Sets the invalid
     \returns   True if it succeeds, false if it fails.
     */
    virtual bool setInvalid();
    /**
     \fn    virtual mpt_var* buf_var::getNext();
     \brief Gets the next item if this item was created in an array, otherwise
            undefined.
     \returns   Pointer to next item in array.
     */
    virtual mpt_var* getNext();
    /**
     \fn    virtual const mpt_var& buf_var::operator=(const mpt_var& right);
     \brief Assignment operator. setDirty() will be called if the value or size is changed,
            otherwise notDirty() is called.
     \param     right   The right object to copy from. The object must be another buf_var,
                        otherwise nothing will happen.
     \returns   A reference of this object.
     */
    virtual const mpt_var& operator=(const mpt_var& right);
    /**
     \fn    virtual bool buf_var::operator==(const mpt_var& right) const;
     \brief Equality operator
     \param     right   The right object to compare with.
     \returns   True if the size and every word compares equal.
     */
    virtual bool operator==(const mpt_var& right) const;
    /**
     \fn    virtual istream& buf_var::operator>>(istream& s);
     \brief Stream extraction operator that reads its own data. This function is
            called by the global extraction operator.
     \param [in]    s   an istream to process.
     \returns   The input stream.
     */
    virtual istream& operator>>(istream& s);
    /**
     \fn    virtual ostream& buf_var::operator<<(ostream& s) = 0;
     \brief Abstract function. Stream insertion operator that writes its own data.
            This function is called by the global insertion operator.
     \param [in]    s   an ostream to process.
     \returns   The output stream.
     */
    virtual ostream& operator<<(ostream& s) const;  /*!< . */
    /**
     \fn    const buf_var& buf_var::operator=(const buf_var& right);
     \brief Assignment operator. setDirty() will be called if the value or size is changed,
            otherwise notDirty() is called.
     \param     right   The buf_var to copy from.
     \returns   A reference of this object.
     */
    const buf_var& operator=(const buf_var& right);
    /**
     \fn    const buf_var& buf_var::operator=(const char* right);
     \brief Assignment operator. setDirty() will be called if the value or size is changed,
            otherwise notDirty() is called.
     \param     right   The null-terminated string to copy from. Padding may be added
                        to form a word.
     \returns   A reference of this object.
     */
    const buf_var& operator=(const char* right);
    /**
     \fn    const buf_var& buf_var::operator+=(const char* right);
     \brief Append to the buffer operator. The existing string will be searched for
            the first null and the new string concatenated.
     \param     right   The null-terminated string to copy from. Padding may be added
                        to form a word.
     \returns   A reference of this object.
     */
    const buf_var& operator+=(const char* right);
    /**
     \fn    void buf_var::setBuffer(const void* p, int _sz = -1);
     \brief Sets a new buffer. A deep copy is made.
     \param     p   The bufer to copy from. If a positive size is passed in, the buffer may
                    contain nulls which will be handled as part of the data. Padding may be
                    added at the end to form a word.
     \param     _sz (Optional) The size in bytes. If the size is non-positive, the function
                    will discover the size by searching for null termination.
     */
    void setBuffer(const void* p, int _sz = -1); // _sz = size in bytes, -1 means use strlen to discover
    /**
     \fn    const void* buf_var::getBuffer() const
     \brief Gets the internal buffer
     \returns   Null if the size is 0, else the buffer.
     */
    const void* getBuffer() const
    {
        return data;
    }
    /**
     \fn    unsigned int& buf_var::operator[](int idx);
     \brief Array indexer operator
     \param     idx Zero-based index of the word.
     \returns   Value of the indexed word. If the index is out of range, MAX_UINT is returned.
     */
    unsigned int& operator[](int idx);
    /**
     \fn    void buf_var::set(int _sz);
     \brief Resizes the buffer. If the size is smaller, truncation occurs. If the size
            is larger, the new indices are uninitialized. setDirty() will be called if
            the size is changed, notDirty() otherwise.
     \param     _sz The Size in 32-bit words to set.
     */
    void set(int _sz);

protected:

    /**
     \fn    bool buf_var::readData(int len, const unsigned char* buf, bool setrmdirty);
     \brief Reads a data making a deep copy. setDirty() will be called if the size or
            its values have changed, otherwise notDirty() will be called. Padding of
            zeros may be added to the end to form a 32-bit word.
     \param     len         The length in bytes.
     \param     buf         The buffer.
     \param     setrmdirty  True to setrmdirty as well if there are changes.
     \returns   True if there was a change in size or value, false otherwise.
     */
    bool readData(int len, const unsigned char* buf, bool setrmdirty);

private:
    buf_var();
};

#endif /* _BUF_VAR_H_ */

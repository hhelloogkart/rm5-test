/***********************                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                *******************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _CACHE_H_
#define _CACHE_H_

#include <rmglobal.h>
#include <stddef.h> //for size_t
/**
 \struct    header_typ
 \brief RM Packet Header
 */
struct RM_EXPORT header_typ
{
    unsigned int magic; /*!< The magic number for RM */
    unsigned int id;    /*!< The message identifier */
    unsigned short seq; /*!< The increasing sequence number */
    unsigned short len; /*!< The length in number of words */
};
/**
 \class   cache_typ
 \brief   Manages RM Packets with functions to manage its header information.
 \details Provides accessor functions to manage the RM packet header, factories
          and conversion functions for allocating the packet on the heap, and
          reference counting / garbage collection. This class should not be
          instantiated directly, but to use the appropriate factory methods
          such as h_allocate() or sm_allocate(), as the cache_typ object is
          normally larger than defined, as the variable length buffer after
          the header is also allocated.
 */
class RM_EXPORT cache_typ
{
public: /*static functions*/

    /**
     \fn    static cache_typ* cache_typ::h_allocate(size_t sz);
     \brief Allocates from the heap and returns a cache_typ object of the given size
     \param     sz  The size in 32-bit words. Zero size is perfectly valid and
                    is used to send protocol management packets.
     \returns   Null if it fails to allocate from the heap, else a pointer to a
                cache_typ.
     */
    static cache_typ* h_allocate(size_t sz);
    /**
     \fn    static cache_typ* cache_typ::h_duplicate(unsigned int id, size_t len, const unsigned int* src);
     \brief Allocates from the heap and performs a deep copy of the headers and data.
     \param     id  The identifier.
     \param     len The length.
     \param     src Source for the.
     \returns   Null if it fails, else a copy of this object.
     */
    static cache_typ* h_duplicate(unsigned int id, size_t len, const unsigned int* src);
    /**
     \fn    static void cache_typ::h_deallocate(cache_typ* ptr);
     \brief Deallocates the given pointer. This should be called automatically when
           the reference count falls to zero.
     \param [in]    Pointer to cache_typ to deallocate.
     */
    static void h_deallocate(cache_typ* ptr);

public:

    /**
     \fn    cache_typ::cache_typ(size_t _len);
     \brief Constructor, initializes the header with magic number,
            size, command and reference count.
     \param     _len    The length (number of 32 bit words).
     */
    cache_typ(size_t _len);
    /**
     \fn    void cache_typ::construct(size_t _len);
     \brief Initializes the header with magic number size, command
            and reference count. It is factored out into a
            separate function to allow it to be accessed from other
            cache_typ factories other than heap, such as shared
            memory.
     \param     _len    The length (number of 32 bit words).
     */
    void construct(size_t _len);
    /**
     \fn    const unsigned int* cache_typ::buf() const;
     \brief Gets the const pointer to the buffer, which is after the
            header. This is the method that can be called if you only
            have a const cache_typ.
     \returns   Pointer to the start of buffer.
     */
    const unsigned int* buf() const;
    /**
     \fn    unsigned int* cache_typ::buf();
     \brief Gets the pointer to the buffer, which is after the
            header.
     \returns   Pointer to the start of buffer.
     */
    unsigned int* buf();
    /**
     \fn    const unsigned int* cache_typ::packet() const;
     \brief Gets the const pointer to the packet, which starts
            with its header (3 words) and followed by its buffer
            (variable length). The packet starts after the reference
            count.
     \returns   Pointer to the start of packet.
     */
    const unsigned int* packet() const;
    /**
     \fn    unsigned int* cache_typ::packet();
     \brief Gets the pointer to the packet, which starts with its
            header (3 words) and followed by its buffer (variable
            length). The packet starts after the reference count.
     \returns   Pointer to the start of packet.
     */
    unsigned int* packet();
    /**
     \fn    void cache_typ::h_reserve();
     \brief Reserves this packet by incrementing its reference
            counter.
     */
    void h_reserve();
    /**
     \fn    void cache_typ::h_unreserve();
     \brief Unreserves this packet by decrementing its reference
            counter. If the counter is zero, the packet will be
            deallocated on the heap.
     */
    void h_unreserve();
    /**
     \fn    unsigned int cache_typ::get_refcount() const;
     \brief Gets the reference count
     \returns   The reference count.
     */
    unsigned int get_refcount() const;
    /**
     \fn    unsigned int cache_typ::unreserve();
     \brief Unreserves this packet by decrementing its reference
            counter and returning it. This is for non-heap allocated
            cache_typ objects.
     \returns   The reference count after decrement.
     */
    unsigned int unreserve();
    /**
     \fn    unsigned int cache_typ::get_id() const;
     \brief Gets the message identifier from its header.
     \returns   The message identifier.
     */
    unsigned int get_id() const;
    /**
     \fn    void cache_typ::set_id(unsigned int _id);
     \brief Sets the message identifier into its header.
     \param     _id The identifier.
     */
    void set_id(unsigned int _id);
    /**
     \fn    unsigned int cache_typ::get_len() const;
     \brief Gets the length of the buffer in number of words,
            from the header.
     \returns   The buffer length.
     */
    unsigned int get_len() const;
    /**
     \fn    void cache_typ::set_len(unsigned int _len);
     \brief Sets the length of the buffer in number of words,
            into the header.
     \param     _len    The buffer length.
     */
    void set_len(unsigned int _len);
    /**
     \fn    unsigned short cache_typ::get_seq() const;
     \brief Gets the sequence number of the packet. This is for
            detection of packet loss.
     \returns   The sequence number.
     */
    unsigned short get_seq() const;
    /**
     \fn    void cache_typ::set_seq(unsigned short _seq);
     \brief Sets a sequence number of the packet. This is
            incremented by the sender.
     \param     _seq    The sequence number.
     */
    void set_seq(unsigned short _seq);
    /**
     \fn    unsigned int cache_typ::get_cmd() const;
     \brief Gets the command from the header. The definition
            is in rm_commands_enum.
     \returns   The command.
     */
    unsigned int get_cmd() const;
    /**
     \fn    void cache_typ::set_cmd(unsigned int _cmd);
     \brief Sets the command into the header. The definition
            is in rm_commands_enum.
     \param     _cmd    The command.
     */
    void set_cmd(unsigned int _cmd);
    /**
     \fn    void cache_typ::copy_header(unsigned int* buf);
     \brief Copies the header described by buf. The header is made up of 3 words.
     \param [in]    buf The buffer to copy from.
     */
    void copy_header(unsigned int* buf);
    /**
     \fn    bool cache_typ::check_header() const;
     \brief Checks for a valid header.
     \returns   True if the header is valid.
     */
    bool check_header() const;
    /**
     \fn    size_t cache_typ::output(size_t len, unsigned int* buf) const;
     \brief Outputs the contents of the entire packet into the buffer provided, and
            returns the number of words written. If the buffer provided is too small
            the excess data is not written.
     \param          len The length in 32 bit words.
     \param [out]    buf The buffer.
     \returns   Number of words written.
     */
    size_t output(size_t len, unsigned int* buf) const;
    /**
     \fn    static cache_typ* cache_typ::CastToCache(void* buf, bool init = true);
     \brief Casts the provided memory into a cache_typ object, optionally setting
            the reference count to a very large number to prevent a deallocation.
     \param [in]    buf     The pointer to memory.
     \param         init    (Optional) True to initialize.
     \returns   Pointer to a cache_typ, which is actually the same address passed int.
     */
    static cache_typ* CastToCache(void* buf, bool init = true);
    /**
     \fn    static cache_typ* cache_typ::ConvertToCache(void* buf, bool init = true);
     \brief Converts the buffer into a cache_typ object, optionally setting
            the reference count to a very large number to prevent a deallocation.
            The buffer is located after the reference counter and the header, so
            the address returned will be backed up by 4 words.
     \param [in]    buf     The pointer to the buffer. There should be allocated
                            space of 5 words in front of it.
     \param         init    (Optional) True to initialize.
     \returns   Pointer to a cache_typ, which is the buffer address minus the size
                of cache_typ.
     */
    static cache_typ* ConvertToCache(void* buf, bool init = true);
    /**
     \fn    static cache_typ* cache_typ::ConvertToCache(void* buf, unsigned int _len, unsigned int _cmd);
     \brief  Converts the buffer into a cache_typ object, setting the reference
             count to a very large number to prevent a deallocation, magic number,
             command and length. The buffer is located after the reference counter
             and the header, so the address returned will be backed up by 4 words.
     \param [in]     buf     The pointer to the buffer. There should be allocated
                             space of 5 words in front of it.
     \param          _len    The length to set in the header.
     \param          _cmd    The command to set in the header.
     \returns   Pointer to a cache_typ, which is the buffer address minus the size
                of cache_typ.
     */
    static cache_typ* ConvertToCache(void* buf, unsigned int _len, unsigned int _cmd);

    void* dummy;    /*!< This is to eliminate chances of the next pointer of a free
                         chunk from being corrupted. */

protected:
    unsigned int ref;   /*!< The reference counter */
    header_typ header;  /*!< The header */
};

inline unsigned int cache_typ::get_refcount() const
{
    return ref;
}
inline unsigned int cache_typ::unreserve()
{
    return --ref;
}

#endif /* _CACHE_H_ */

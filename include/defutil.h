/******************************************************************/
/* Copyright DSO National Laboratories 2020. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

/*
   List of protocols:
     0 - RM
     1 - serial
     2 - msg

   New ones will be added as required.
 */

#ifndef _DEFUTIL_H_
#define _DEFUTIL_H_

#include <rmglobal.h>
class defaults;
class mpt_var;

typedef void (* init_func)(defaults*);
typedef void (* in_out_deinit_func)();

struct protocol_description
{
    int prot;
    init_func initf;
    in_out_deinit_func incomingf;
    in_out_deinit_func outgoingf;
    in_out_deinit_func deinitf;
};

class RM_EXPORT rmclient_init
{
public:
    rmclient_init(const char* defstr = 0, int argc = 0, char** argv = 0);
    rmclient_init(defaults* defptr, int argc = 0, char** argv = 0);
    ~rmclient_init();
    bool client_exit();

    defaults* client_defaults()
    {
        return _defptr;
    }
    operator defaults* ()
    {
        return _defptr;
    }
    static void incoming();
    static void outgoing();
    static void install_protocol(const protocol_description& des, bool force = false);
    static void install_exit_trigger(mpt_var* var);
    static void exit();

private:
    void rmclient_init_c();

    defaults* _defptr;
    defaults* _delonexit;

};

#endif
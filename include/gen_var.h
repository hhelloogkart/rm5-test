/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _GEN_VAR_H_
#define _GEN_VAR_H_

#include <limits>
#include <outbuf.h>
#include <vlu_var.h>

#ifdef min
#undef min
#endif

#ifdef max
#undef max
#endif

#if defined(_MSC_VER) && _MSC_VER <= 1600
#define NO_MEMBER_TEMPLATE
#endif

/*!
   Internal namespace for library use
*/
namespace rm_internal
{
/**
 \struct    mk_unsigned
 \brief Template to return the unsigned equivalent of a type. For types that are already
        unsigned, or floats, it will return the same type.
 \tparam    C   Type to get unsigned equivalent of, such as int, short, long.
 */
template<class C>
struct mk_unsigned
{
    /**
     \typedef   C uc
     \brief Unsigned equivalent, default implementation returns the same type
     */
    typedef C uc;
};
/**
 \struct    mk_unsigned<char>
 \brief Template to return the unsigned equivalent of a type. Specialized to return unsigned
        char.
 */
template<>
struct mk_unsigned<char>
{
    /**
     \typedef   unsigned char uc
     \brief Specialization for char
     */
    typedef unsigned char uc;
};
/**
 \struct    mk_unsigned<short>
 \brief Template to return the unsigned equivalent of a type. Specialized to return unsigned
        short.
 */
template<>
struct mk_unsigned<short>
{
    /**
     \typedef   unsigned short uc
     \brief Specialization for short
     */
    typedef unsigned short uc;
};
/**
 \struct    mk_unsigned<int>
 \brief Template to return the unsigned equivalent of a type. Specialized to return unsigned
        int.
 */
template<>
struct mk_unsigned<int>
{
    /**
     \typedef   unsigned int uc
     \brief Specialization for int
     */
    typedef unsigned int uc;
};
/**
 \struct    mk_unsigned<long>
 \brief Template to return the unsigned equivalent of a type. Specialized to return unsigned
        long.
 */
template<>
struct mk_unsigned<long>
{
    /**
     \typedef   unsigned long uc
     \brief Specialization for long
     */
    typedef unsigned long uc;
};
/**
 \struct    mk_unsigned<long long>
 \brief Template to return the unsigned equivalent of a type. Specialized to return unsigned
        long long.
 */
template<>
struct mk_unsigned<long long>
{
    /**
     \typedef   unsigned long long uc
     \brief Specialization for long long
     */
    typedef unsigned long long uc;
};
/**
 \struct    is_same
 \brief Template test to check if both type parameters are the same.
 \tparam    T   1st generic type parameter.
 \tparam    U   2nd generic type parameter.
 */
template<class T, class U>
struct is_same
{
    enum    /*!< An enum constant to return 0, not the same. */
    {
        value = 0
    };
};
/**
 \struct    is_same<T,T>
 \brief Template test to check if both type parameters are the same.
 \tparam    T   Generic type parameter.
 */
template<class T>
struct is_same<T, T>
{
    enum    /*!< An enum constant to return 1, the same. */
    {
        value = 1
    };
};

/*! Convert from type C to D */
/**
 \fn    template <class C,class D> D generic_var_convert(C data, char(* parameter2)
 \brief     Same type conversions. Just return same value
 \tparam    C   Source Type.
 \tparam    D   Destination Type.
 \param     data        Data for conversion.
 \returns   Converted value.
 */
template<class C, class D>
D generic_var_convert(C data, char(*)[is_same<C,D>::value] = 0)
{
    return data;
}
/**
 \fn    template <class C,class D> D generic_var_convert(C data, char(* )[std::numeric_limits<C>::is_integer, char(* parameter3, char(* )[is_same<C,D>::value)
 \brief     Floating point to integer values, we should perform rounding
 \tparam    C   Source Type.
 \tparam    D   Destination Type.
 \param     data        Data for conversion.
 \returns   Converted value.
 */
template<class C,class D>
D generic_var_convert(C data, char(*)[std::numeric_limits<C>::is_integer == 0] = 0, char(*)[std::numeric_limits<D>::is_integer] = 0, char(*)[is_same<C, D>::value == 0] = 0)
{
    if (data < std::numeric_limits<D>::min())
    {
        return std::numeric_limits<D>::min();
    }
    if (data > std::numeric_limits<D>::max())
    {
        return std::numeric_limits<D>::max();
    }
    const C delta = static_cast<C>(0.5);
    return static_cast<D>((data < 0) ? data-delta : data+delta);
}
/**
 \fn    template <class C,class D> D generic_var_convert(C data, char(* parameter2, char(* parameter3, char(* parameter4, char(* parameter5, char(* )[is_same<C,D>::value)
 \brief     Integer to integer conversions for signed-unsigned combinations. Signed to signed.
 \tparam    C   Source Type.
 \tparam    D   Destination Type.
 \param     data        Data for conversion.
 \returns   Converted value.
 */
template<class C,class D>
D generic_var_convert(C data, char(*)[std::numeric_limits<C>::is_signed] = 0, char(*)[std::numeric_limits<D>::is_signed] = 0, char(*)[std::numeric_limits<C>::is_integer] = 0, char(*)[std::numeric_limits<D>::is_integer] = 0, char(*)[is_same<C, D>::value == 0] = 0)
{
    /* signed to signed */
    if (data < std::numeric_limits<D>::min())
    {
        return std::numeric_limits<D>::min();
    }
    if (data > std::numeric_limits<D>::max())
    {
        return std::numeric_limits<D>::max();
    }
    return static_cast<D>(data);
}
/**
 \fn    template <class C,class D> D generic_var_convert(C data, char(* )[std::numeric_limits<C>::is_signed, char(* )[std::numeric_limits<D>::is_signed, char(* parameter4, char(* )[is_same<C,D>::value)
 \brief     Integer to integer conversions for signed-unsigned combinations. Unsigned to unsigned.
 \tparam    C   Source Type.
 \tparam    D   Destination Type.
 \param     data        Data for conversion.
 \returns   Converted value.
 */
template<class C, class D>
D generic_var_convert(C data, char(*)[std::numeric_limits<C>::is_signed == 0] = 0, char(*)[std::numeric_limits<D>::is_signed == 0] = 0, char(*)[std::numeric_limits<C>::is_integer] = 0, char(*)[is_same<C, D>::value == 0] = 0)
{
    /* unsigned to unsigned */
    if (data > std::numeric_limits<D>::max())
    {
        return std::numeric_limits<D>::max();
    }
    return static_cast<D>(data);
}
/**
 \fn    template <class C,class D> D generic_var_convert(C data, char(* parameter2, char(* )[std::numeric_limits<D>::is_signed, char(* parameter4, char(* )[is_same<C,D>::value)
 \brief     Integer to integer conversions for signed-unsigned combinations. Signed to unsigned.
 \tparam    C   Source Type.
 \tparam    D   Destination Type.
 \param     data        Data for conversion.
 \returns   Converted value.
 */
template<class C, class D>
D generic_var_convert(C data, char(*)[std::numeric_limits<C>::is_signed] = 0, char(*)[std::numeric_limits<D>::is_signed == 0] = 0, char(*)[std::numeric_limits<C>::is_integer] = 0, char(*)[is_same<C, D>::value == 0] = 0)
{
    /* signed to unsigned */
    if (data < 0)
    {
        return 0;
    }
    if (static_cast<typename mk_unsigned<C>::uc>(data) > std::numeric_limits<D>::max())
    {
        return std::numeric_limits<D>::max();
    }
    return static_cast<D>(data);
}
/**
 \fn    template <class C,class D> D generic_var_convert(C data, char(* )[std::numeric_limits<C>::is_signed, char(* parameter3, char(* parameter4, char(* parameter5, char(* )[is_same<C,D>::value)
 \brief     Integer to integer conversions for signed-unsigned combinations. Unsigned to signed.
 \tparam    C   Source Type.
 \tparam    D   Destination Type.
 \param     data        Data for conversion.
 \returns   Converted value.
 */
template<class C, class D>
D generic_var_convert(C data, char(*)[std::numeric_limits<C>::is_signed == 0] = 0, char(*)[std::numeric_limits<D>::is_signed] = 0, char(*)[std::numeric_limits<C>::is_integer] = 0, char(*)[std::numeric_limits<D>::is_integer] = 0, char(*)[is_same<C, D>::value == 0] = 0)
{
    /* unsigned to signed */
    if (data > static_cast<typename mk_unsigned<D>::uc>(std::numeric_limits<D>::max()))
    {
        return std::numeric_limits<D>::max();
    }
    return static_cast<D>(data);
}
/**
 \fn    template<class C, class D> D generic_var_convert(C data, char(*)[std::numeric_limits<C>::is_integer == 0] = 0, char(*)[std::numeric_limits<D>::is_integer == 0] = 0, char(*)[is_same<C, D>::value == 0] = 0)
 \brief     Float to float conversions
 \tparam    C   Source Type.
 \tparam    D   Destination Type.
 \param     data        Data for conversion.
 \returns   Converted value.
 */
template<class C, class D>
D generic_var_convert(C data, char(*)[std::numeric_limits<C>::is_integer == 0] = 0, char(*)[std::numeric_limits<D>::is_integer == 0] = 0, char(*)[is_same<C, D>::value == 0] = 0)
{
    if (data < -std::numeric_limits<D>::max())
    {
        return -std::numeric_limits<D>::max();
    }
    if (data > std::numeric_limits<D>::max())
    {
        return std::numeric_limits<D>::max();
    }
    return static_cast<D>(data);
}
/**
 \fn    template <class C,class D> D generic_var_convert(C data, char(* parameter2, char(* )[std::numeric_limits<D>::is_integer, char(* )[is_same<C,D>::value)
 \brief     Integer to float conversions
 \tparam    C   Source Type.
 \tparam    D   Destination Type.
 \param     data        Data for conversion.
 \returns   Converted value.
 */
template<class C, class D>
D generic_var_convert(C data, char(*)[std::numeric_limits<C>::is_integer] = 0, char(*)[std::numeric_limits<D>::is_integer == 0] = 0, char(*)[is_same<C, D>::value == 0] = 0)
{
    return static_cast<D>(data);
}
/**
 \fn    template <class C> C negate(C data, char(* parameter2)
 \brief Negates the value for signed data types
 \tparam    C   Type of the value.
 \param     data        Value to negate.
 \returns   Negative of the value passed in.
 */
template<class C>
C negate(C data, char(*)[std::numeric_limits<C>::is_signed] = 0)
{
    return -data;
}
/**
 \fn    template<class C> C negate(C data, char(*)[std::numeric_limits<C>::is_signed == 0] = 0)
 \brief Negates the value for unsigned data types
 \tparam    C   Type of the value.
 \param     data        Value to negate.
 \returns   Same value passed in, as unsigned types cannot be negated.
 */
template<class C>
C negate(C data, char(*)[std::numeric_limits<C>::is_signed == 0] = 0)
{
    return data;
}
/**
 \fn    inline void gv_memset(void* data, char ch, int sz)
 \brief Gv memset
 \param [in,out]    data    If non-null, the data.
 \param             ch      The ch.
 \param             sz      The size.
 */
inline void gv_memset(void* data, char ch, int sz)
{
    char* p = reinterpret_cast<char*>(data);
    char* q = p + sz;
    while (p != q)
    {
        *p = ch;
        ++p;
    }
}
/**
 \fn    template<class C> bool generic_var_isvalid(C value, char(*)[std::numeric_limits<C>::is_integer == 0] = 0)
 \brief Generic_var specific test of isvalid, which differs from the mpt_var test in that
        all bit ones in integers are also considered invalid. Non integer case calls mpt_var's
        version of isValid.
 \tparam    C   Type of the value.
 \param     value  The value to test.
 \returns   True if valid, false otherwise.
 */
template<class C>
bool generic_var_isvalid(C value, char(*)[std::numeric_limits<C>::is_integer == 0] = 0)
{
    return ::isValid(value);
}
/**
 \fn    template <class C> bool generic_var_isvalid(C value, char(* parameter2)
 \brief Generic_var specific test of isvalid, which differs from the mpt_var test in that
        all bit ones in integers are also considered invalid. Integer case checks for
        all bit ones.
 \tparam    C   Type of the value.
 \param     value  The value to test.
 \returns   True if valid, false otherwise.
 */
template<class C>
bool generic_var_isvalid(C value, char(*)[std::numeric_limits<C>::is_integer] = 0)
{
    const C inv = ~0;
    return value != inv;
}
/**
 \struct    pod_check
 \brief POD - plain old data check.
 \tparam    C   Type to check. Default does not define ok, means is not POD.
 */
template<class C>
struct pod_check {};
/**
 \struct    pod_check<unsigned char>
 \brief POD - plain old data check. Specialized to return ok.
 */
template<>
struct pod_check<unsigned char>
{
    typedef int ok;
};
/**
 \struct    pod_check<signed char>
 \brief POD - plain old data check. Specialized to return ok.
 */
template<>
struct pod_check<signed char>
{
    typedef int ok;
};
/**
 \struct    pod_check<char>
 \brief POD - plain old data check. Specialized to return ok.
 */
template<>
struct pod_check<char>
{
    typedef int ok;
};
/**
 \struct    pod_check<unsigned short>
 \brief POD - plain old data check. Specialized to return ok.
 */
template<>
struct pod_check<unsigned short>
{
    typedef int ok;
};
/**
 \struct    pod_check<short>
 \brief POD - plain old data check. Specialized to return ok.
 */
template<>
struct pod_check<short>
{
    typedef int ok;
};
/**
 \struct    pod_check<unsigned int>
 \brief POD - plain old data check. Specialized to return ok.
 */
template<>
struct pod_check<unsigned int>
{
    typedef int ok;
};
/**
 \struct    pod_check<int>
 \brief POD - plain old data check. Specialized to return ok.
 */
template<>
struct pod_check<int>
{
    typedef int ok;
};
/**
 \struct    pod_check<unsigned long>
 \brief POD - plain old data check. Specialized to return ok.
 */
template<>
struct pod_check<unsigned long>
{
    typedef int ok;
};
/**
 \struct    pod_check<long>
 \brief POD - plain old data check. Specialized to return ok.
 */
template<>
struct pod_check<long>
{
    typedef int ok;
};
/**
 \struct    pod_check<unsigned long long>
 \brief POD - plain old data check. Specialized to return ok.
 */
template<>
struct pod_check<unsigned long long>
{
    typedef int ok;
};
/**
 \struct    pod_check<long long>
 \brief POD - plain old data check. Specialized to return ok.
 */
template<>
struct pod_check<long long>
{
    typedef int ok;
};
/**
 \struct    pod_check<float>
 \brief POD - plain old data check. Specialized to return ok.
 */
template<>
struct pod_check<float>
{
    typedef int ok;
};
/**
 \struct    pod_check<double>
 \brief POD - plain old data check. Specialized to return ok.
 */
template<>
struct pod_check<double>
{
    typedef int ok;
};
}

/**
 \class generic_var
 \brief     Generic variable for storage using basic types (POD)
 \details   This template class is specialised to a basic data type. It allows the basic data
            type to take on the capabilities of mpt_var, such as extraction, packing and
            notification of callbacks when changed.
 \tparam    C   Type of the data.
 */
template<class C, class D = typename rm_internal::pod_check<C>::ok >
class generic_var : public value_var
{
protected:
    C data; /*!< The data */

public:
    /**
     \fn    virtual unsigned int generic_var::rtti() const
     \brief Gets the library unique runtime type identifier, the value will be different for
            different basic data types
     \returns   Integer that is unique for each mpt_var derived class.
     */
    virtual unsigned int rtti() const
    {
        return rm_hash_string("generic_var") ^ var_rtti(data);
    }
    /**
     \fn    generic_var::generic_var()
     \brief Default constructor, will initialize it to invalid by setting all bits to one.
     */
    generic_var()
    {
        rm_internal::gv_memset(&this->data, '\xFF', sizeof(C));
    }
    /**
     \fn    generic_var::generic_var(const generic_var<C,D>& cpy)
     \brief Copy constructor, will make a deep copy of the contents
     \param     cpy The copy.
     */
    generic_var(const generic_var<C,D>& cpy) : value_var(), data(cpy.data)
    {
    }
    /**
     \fn    generic_var::generic_var(const value_var& cpy)
     \brief Copy constructor, allowing conversion from any value_var type
     \param     cpy The copy.
     */
    generic_var(const value_var& cpy)
    {
        cpy.toValue(this->data);
    }
    /**
     \fn    explicit generic_var::generic_var(C cpy)
     \brief Constructor, making a deep copy of the data passed in.
     \param     cpy The copy.
     */
    explicit generic_var(C cpy) : data(cpy)
    {
    }
    /**
     \fn    virtual int generic_var::extract(int buflen, const unsigned char* buf)
     \brief Extracts its data from a byte stream. If FLIP is defined, it will
            reverse the endian of the data it extracts. If the value is changed,
            setDirty is called, otherwise notDirty is called.
     \param     buflen The length.
     \param     buf    The buffer.
     \returns   Number of bytes remaining that was not extracted.
     */
    virtual int extract(int buflen, const unsigned char* buf)
    {
        const C old = this->data;
        char* p = reinterpret_cast<char*>(&this->data);
        char* q = p + sizeof(C);
        const char* r = reinterpret_cast<const char*>(buf);
        if (static_cast<size_t>(buflen) < sizeof(C))
        {
            return 0;
        }
#ifndef FLIP
        while (p != q)
        {
            *p = *r;
            ++p;
            ++r;
        }
#else
        while (q != p)
        {
            *(q-1) = *r;
            --q;
            ++r;
        }
#endif
        const bool oldvalid = ::isValid(old);
        const bool newvalid = ::isValid(this->data);
        if ((newvalid != oldvalid) || /* Validity has changed */
            (newvalid && oldvalid && (this->data != old))) /* Valid and changed */
        {
            mpt_var::setDirty();
        }
        else
        {
            mpt_var::notDirty();  /* Valid not changed, or still invalid */
        }
        return buflen - sizeof(C);
    }
    /**
     \fn    virtual int generic_var::size() const
     \brief Returns the size in bytes that this object will need to serialize
            itself into a byte stream.
     \returns   Size in bytes.
     */
    virtual int size() const
    {
        return sizeof(C);
    }
    /**
     \fn    virtual void generic_var::output(outbuf& strm)
     \brief Outputs data into a byte stream writing into outbuf. The number of
            bytes written must be equal to size().
     \param [in,out]    strm     Output buffer.
     */
    virtual void output(outbuf& strm)
    {
        char* p = reinterpret_cast<char*>(&this->data);
        char* q = p + sizeof(C);
#ifndef FLIP
        while (p != q)
        {
            strm += *p;
            ++p;
        }
#else
        while (p != q)
        {
            strm += *(q-1);
            --q;
        }
#endif
    }
    /**
     \fn    virtual bool generic_var::isValid() const
     \brief Query if this object contains data that is valid. Will only return
            false for floats with NaN. Integers will always return true.
     \returns   True if valid, false if not.
     */
    virtual bool isValid() const
    {
        return ::isValid(this->data);
    }
    /**
     \fn    virtual bool generic_var::setInvalid()
     \brief Resets the data to invalid, which fills all bits to 1. If the value
            is changed with the reset, setDirty will be called, otherwise notDirty
            is called. This behaviour is not the same as assignment, because
            the remote dirty flag will not be set. For remote dirty flag to be
            set when the value is changed, call nullValue() instead.
     \returns   True if it was previously invalid or all bit 1 for integers.
                The logic for valid is not the same as the isValid result.
     */
    virtual bool setInvalid()
    {
        if (rm_internal::generic_var_isvalid(this->data))
        {
            rm_internal::gv_memset(&this->data, '\xFF', sizeof(C));
            setDirty();
            return true;
        }
        notDirty();
        return false;
    }
    /**
     \fn    virtual mpt_var* generic_var::getNext()
     \brief Gets the next item if this item was created in an array, otherwise
            undefined.
     \returns   Pointer to next item in array.
     */
    virtual mpt_var* getNext()
    {
        return this + 1;
    }
    /**
     \fn    C generic_var::operator()()
     \brief Function call operator to return the value
     \returns   The value.
     */
    C operator()()
    {
        return this->data;
    }
    /**
     \fn    C generic_var::operator=(C right)
     \brief Assignment operator.  If the value is changed, setDirty() and setRMDirty()
            will be called, otherwise notDirty() will be called.
     \param     right   The object to be copied from, a deep copy will be made.
     \returns   A const reference to this object.
     */
    C operator=(C right)
    {
        if (!isValid() || (::isValid(right) && (this->data != right)))
        {
            this->data = right;
            mpt_var::setDirty();
            mpt_var::setRMDirty();
        }
        else
        {
            mpt_var::notDirty();
        }
        return this->data;
    }
    /**
     \fn    const generic_var<C,D>& generic_var::operator=(const generic_var<C,D>& right)
     \brief Assignment operator.  If the value is changed, setDirty() and setRMDirty()
            will be called, otherwise notDirty() will be called.
     \param     right   The object to be copied from, a deep copy will be made.
     \returns   A const reference to this object.
     */
    const generic_var<C,D>& operator=(const generic_var<C,D>& right)
    {
        if (!isValid() || (::isValid(right.data) && (this->data != right.data)))
        {
            this->data = right.data;
            mpt_var::setDirty();
            mpt_var::setRMDirty();
        }
        else
        {
            mpt_var::notDirty();
        }
        return *this;
    }
    /**
     \fn    virtual const mpt_var& generic_var::operator=(const mpt_var& right)
     \brief Assignment operator, virtual override so that it works without needing to upcast.
            If the value is changed, setDirty() and setRMDirty() will be called, otherwise
            notDirty() will be called.
     \param     right   The object to be copied from, a deep copy will be made.
     \returns   A const reference to this object.
     */
    virtual const mpt_var& operator=(const mpt_var& right)
    {
        const value_var* gen = RECAST(const value_var*, &right);
        if (gen)
        {
            const bool me_valid = ::isValid(this->data);
            const bool yu_valid = gen->isValid();
            bool changed = me_valid != yu_valid;
            if (yu_valid)
            {
                C value;
                gen->toValue(value);
                if (!changed)
                {
                    /* yu_valid and me_valid */
                    changed = this->data != value;
                }
                this->data = value;
            }
            if (changed)
            {
                mpt_var::setDirty();
                mpt_var::setRMDirty();
            }
            else
            {
                mpt_var::notDirty();
            }
        }
        return *this;
    }
    /**
     \fn    bool generic_var::operator==(C right) const
     \brief Equality operator
     \param     right   The right value to be compared with.
     \returns   True if the parameters are considered equivalent.
     */
    bool operator==(C right) const
    {
        if (!::isValid(this->data) || !::isValid(right))
        {
            return false;
        }
        return this->data == right;
    }
    /**
     \fn    bool generic_var::operator==(const generic_var<C,D>& right) const
     \brief Equality operator
     \param     right   The right object to be compared with.
     \returns   True if the parameters are considered equivalent.
     */
    bool operator==(const generic_var<C,D>& right) const
    {
        if (!::isValid(this->data) || !::isValid(right.data))
        {
            return false;
        }
        return this->data == right.data;
    }
    /**
     \fn    virtual bool generic_var::operator==(const mpt_var& right) const
     \brief Equality operator, virtual override so that it works without needing to upcast
     \param     right   The right object to be compared with.
     \returns   True if the parameters are considered equivalent.
     */
    virtual bool operator==(const mpt_var& right) const
    {
        const value_var* gen = RECAST(const value_var*, &right);
        if (gen != 0)
        {
            if (!::isValid(this->data) || !gen->isValid())
            {
                return false;
            }
            C val;
            gen->toValue(val);
            return this->data == val;
        }
        return mpt_var::operator==(right);
    }
#ifndef NO_MEMBER_TEMPLATE
    /**
     \fn    template<class P, class Q=typename rm_internal::pod_check<P>::ok > bool generic_var::operator==(const P& right) const
     \brief Equality operator, for mismatched basic data types. Rounding and clipping
            will be applied before a comparison is made. Eg. (int)3 == (double)3.4.
     \param     right   The right POD value to be compared with.
     \returns   True if the parameters are considered equivalent.
     */
    template<class P, class Q=typename rm_internal::pod_check<P>::ok >
    bool operator==(const P& right) const
    {
        if (!::isValid(this->data) || !::isValid(right))
        {
            return false;
        }
        return this->data == rm_internal::generic_var_convert<P, C>(right);
    }
#endif
    /**
     \fn    bool generic_var::operator<(const generic_var<C,D>& right) const
     \brief Less-than comparison operator
     \param     right   The right object.
     \returns   True if the first parameter is less than the second.
     */
    bool operator<(const generic_var<C,D>& right) const
    {
        if (!::isValid(this->data) || !::isValid(right.data))
        {
            return false;
        }
        return this->data < right.data;
    }
    /**
     \fn    bool generic_var::operator<(C right) const
     \brief Less-than comparison operator
     \param     right   The right value.
     \returns   True if the first parameter is less than the second.
     */
    bool operator<(C right) const
    {
        if (!::isValid(this->data) || !::isValid(right))
        {
            return false;
        }
        return this->data < right;
    }
    /**
     \fn    operator generic_var::C() const
     \brief Cast that returns the value.
     \returns   The value.
     */
    operator C() const
    {
        return this->data;
    }
    /**
     \fn    C generic_var::operator-() const
     \brief Negation operator
     \returns   The negative value if applicable. Unsigned will return the same value.
     */
    C operator-() const
    {
        return rm_internal::negate<C>(this->data);
    }
    /**
     \fn    C generic_var::operator+=(C right)
     \brief Addition assignment operator. This will
            setDirty() and setRMDirty().
     \param     right   The right value.
     \returns   The final value after function has been applied.
     */
    C operator+=(C right)
    {
        if (!::isValid(this->data))
        {
            return this->data;
        }
        this->data += right;
        mpt_var::setDirty();
        mpt_var::setRMDirty();
        return this->data;
    }
    /**
     \fn    C generic_var::operator-=(C right)
     \brief Subtraction assignment operator.  This will
            setDirty() and setRMDirty().
     \param     right   The right value.
     \returns   The final value after function has been applied.
     */
    C operator-=(C right)
    {
        if (!::isValid(this->data))
        {
            return this->data;
        }
        this->data -= right;
        mpt_var::setDirty();
        mpt_var::setRMDirty();
        return this->data;
    }
    /**
     \fn    C generic_var::operator*=(C right)
     \brief Multiplication assignment operator. This will
            setDirty() and setRMDirty().
     \param     right   The right value.
     \returns   The final value after function has been applied.
     */
    C operator*=(C right)
    {
        if (!::isValid(this->data))
        {
            return this->data;
        }
        this->data *= right;
        mpt_var::setDirty();
        mpt_var::setRMDirty();
        return this->data;
    }
    /**
     \fn    C generic_var::operator/=(C right)
     \brief Division assignment operator.  This will
            setDirty() and setRMDirty().
     \param     right   The right value.
     \returns   The final value after function has been applied.
     */
    C operator/=(C right)
    {
        if (!::isValid(this->data))
        {
            return this->data;
        }
        this->data /= right;
        mpt_var::setDirty();
        mpt_var::setRMDirty();
        return this->data;
    }
    /**
     \fn    const value_var& generic_var::operator+=(const value_var& right)
     \brief Addition assignment operator This will
            setDirty() and setRMDirty().
     \param     right   The right object. Will be converted to the current type before operation.
     \returns   The final value after function has been applied.
     */
    const value_var& operator+=(const value_var& right)
    {
        C cpy;
        right.toValue(cpy);
        this->data += cpy;
        mpt_var::setDirty();
        mpt_var::setRMDirty();
        return *this;
    }
    /**
     \fn    const value_var& generic_var::operator-=(const value_var& right)
     \brief Subtraction assignment operator. This will
            setDirty() and setRMDirty().
     \param     right   The right object. Will be converted to the current type before operation.
     \returns   The final value after function has been applied.
     */
    const value_var& operator-=(const value_var& right)
    {
        C cpy;
        right.toValue(cpy);
        this->data -= cpy;
        mpt_var::setDirty();
        mpt_var::setRMDirty();
        return *this;
    }
    /**
     \fn    const value_var& generic_var::operator*=(const value_var& right)
     \brief Multiplication assignment operator. This will
            setDirty() and setRMDirty().
     \param     right   The right object. Will be converted to the current type before operation.
     \returns   The final value after function has been applied.
     */
    const value_var& operator*=(const value_var& right)
    {
        C cpy;
        right.toValue(cpy);
        this->data *= cpy;
        mpt_var::setDirty();
        mpt_var::setRMDirty();
        return *this;
    }
    /**
     \fn    const value_var& generic_var::operator/=(const value_var& right)
     \brief Division assignment operator. This will
            setDirty() and setRMDirty().
     \param     right   The right object. Will be converted to the current type before operation.
     \returns   The final value after function has been applied.
     */
    const value_var& operator/=(const value_var& right)
    {
        C cpy;
        right.toValue(cpy);
        this->data /= cpy;
        mpt_var::setDirty();
        mpt_var::setRMDirty();
        return *this;
    }
    /**
     \fn    virtual char generic_var::to_char() const
     \brief Converts this object to a char value. Rounding and clipping the value
            at the boundary will be applied.
     \returns   This object as a char.
     */
    virtual char to_char() const
    {
        return rm_internal::generic_var_convert<C,char>(this->data);
    }
    /**
     \fn    virtual unsigned char generic_var::to_unsigned_char() const
     \brief Converts this object to an unsigned char value. Rounding and clipping the value
            at the boundary will be applied.
     \returns   This object as a unsigned char.
     */
    virtual unsigned char to_unsigned_char() const
    {
        return rm_internal::generic_var_convert<C,unsigned char>(this->data);
    }
    /**
     \fn    virtual short generic_var::to_short() const
     \brief Converts this object to a short. Rounding and clipping the value
            at the boundary will be applied.
     \returns   This object as a short.
     */
    virtual short to_short() const
    {
        return rm_internal::generic_var_convert<C,short>(this->data);
    }
    /**
     \fn    virtual unsigned short generic_var::to_unsigned_short() const
     \brief Converts this object to an unsigned short. Rounding and clipping the value
            at the boundary will be applied.
     \returns   This object as a unsigned short.
     */
    virtual unsigned short to_unsigned_short() const
    {
        return rm_internal::generic_var_convert<C,unsigned short>(this->data);
    }
    /**
     \fn    virtual int generic_var::to_int() const
     \brief Converts this object to an int. Rounding and clipping the value
            at the boundary will be applied.
     \returns   This object as an int.
     */
    virtual int to_int() const
    {
        return rm_internal::generic_var_convert<C,int>(this->data);
    }
    /**
     \fn    virtual unsigned int generic_var::to_unsigned_int() const
     \brief Converts this object to an unsigned int. Rounding and clipping the value
            at the boundary will be applied.
     \returns   This object as an unsigned int.
     */
    virtual unsigned int to_unsigned_int() const
    {
        return rm_internal::generic_var_convert<C,unsigned int>(this->data);
    }
    /**
     \fn    virtual long generic_var::to_long() const
     \brief Converts this object to a long. Rounding and clipping the value
            at the boundary will be applied.
     \returns   This object as a long.
     */
    virtual long to_long() const
    {
        return rm_internal::generic_var_convert<C,long>(this->data);
    }
    /**
     \fn    virtual unsigned long generic_var::to_unsigned_long() const
     \brief Converts this object to an unsigned long. Rounding and clipping the value
            at the boundary will be applied.
     \returns   This object as a unsigned long.
     */
    virtual unsigned long to_unsigned_long() const
    {
        return rm_internal::generic_var_convert<C,unsigned long>(this->data);
    }
    /**
     \fn    virtual long long generic_var::to_long_long() const
     \brief Converts this object to a long long. Rounding and clipping the value
            at the boundary will be applied.
     \returns   This object as a long long.
     */
    virtual long long to_long_long() const
    {
        return rm_internal::generic_var_convert<C,long long>(this->data);
    }
    /**
     \fn    virtual unsigned long long generic_var::to_unsigned_long_long() const
     \brief Converts this object to an unsigned long. Rounding and clipping the value
            at the boundary will be applied.
     \returns   This object as a unsigned long long.
     */
    virtual unsigned long long to_unsigned_long_long() const
    {
        return rm_internal::generic_var_convert<C,unsigned long long>(this->data);
    }
    /**
     \fn    virtual float generic_var::to_float() const
     \brief Converts this object to a float. Clipping the value
            at the boundary will be applied.
     \returns   This object as a float.
     */
    virtual float to_float() const
    {
        return rm_internal::generic_var_convert<C,float>(this->data);
    }
    /**
     \fn    virtual double generic_var::to_double() const
     \brief Converts this object to a double. Clipping the value
            at the boundary will be applied.
     \returns   This object as a double.
     */
    virtual double to_double() const
    {
        return static_cast<double>(this->data);
    }
    /**
     \fn    virtual char* generic_var::to_char_ptr()
     \brief Stringify the value and pass out the temporary string.
            This function is non-reentrant and non-thread safe.
            Modifying the string that is pased out has no effect
            on the value.
     \returns   This object as a null terminated string.
     */
    virtual char* to_char_ptr()
    {
        return value_var::value_to_str(this->data);
    }
    /**
     \fn    virtual const char* generic_var::to_const_char_ptr() const
     \brief Stringify the value and pass out the temporary string.
            This function is non-reentrant and non-thread safe.
     \returns   This object as a null terminated string.
     */
    virtual const char* to_const_char_ptr() const
    {
        return value_var::value_to_str(this->data);
    }
    /**
     \fn    virtual void generic_var::from_char(char v)
     \brief Initializes this object from the given char value, clipping the value
            at the boundaries if required
     \param     value  The value to use for initialization.
     */
    virtual void from_char(char v)
    {
        this->operator=(rm_internal::generic_var_convert<char, C>(v));
    }
    /**
     \fn    virtual void generic_var::from_unsigned_char(unsigned char v)
     \brief Initializes this object from the given unsigned char value, clipping the value
            at the boundaries if required
     \param     value  The value to use for initialization.
     */
    virtual void from_unsigned_char(unsigned char v)
    {
        this->operator=(rm_internal::generic_var_convert<unsigned char, C>(v));
    }
    /**
     \fn    virtual void generic_var::from_short(short v)
     \brief Initializes this object from the given short, clipping the value
            at the boundaries if required
     \param     value  The value to use for initialization.
     */
    virtual void from_short(short v)
    {
        this->operator=(rm_internal::generic_var_convert<short, C>(v));
    }
    /**
     \fn    virtual void generic_var::from_unsigned_short(unsigned short v)
     \brief Initializes this object from the given unsigned short, clipping the value
            at the boundaries if required
     \param     value  The value to use for initialization.
     */
    virtual void from_unsigned_short(unsigned short v)
    {
        this->operator=(rm_internal::generic_var_convert<unsigned short, C>(v));
    }
    /**
     \fn    virtual void generic_var::from_int(int v)
     \brief Initializes this object from the given int, clipping the value
            at the boundaries if required
     \param     value  The value to use for initialization.
     */
    virtual void from_int(int v)
    {
        this->operator=(rm_internal::generic_var_convert<int, C>(v));
    }
    /**
     \fn    virtual void generic_var::from_unsigned_int(unsigned int v)
     \brief Initializes this object from the given unsigned int, clipping the value
            at the boundaries if required
     \param     value  The value to use for initialization.
     */
    virtual void from_unsigned_int(unsigned int v)
    {
        this->operator=(rm_internal::generic_var_convert<unsigned int, C>(v));
    }
    /**
     \fn    virtual void generic_var::from_long(long v)
     \brief Initializes this object from the given long, clipping the value
            at the boundaries if required
     \param     value  The value to use for initialization.
     */
    virtual void from_long(long v)
    {
        this->operator=(rm_internal::generic_var_convert<long, C>(v));
    }
    /**
     \fn    virtual void generic_var::from_unsigned_long(unsigned long v)
     \brief Initializes this object from the given unsigned long, clipping the value
            at the boundaries if required
     \param     value  The value to use for initialization.
     */
    virtual void from_unsigned_long(unsigned long v)
    {
        this->operator=(rm_internal::generic_var_convert<unsigned long, C>(v));
    }
    /**
     \fn    virtual void generic_var::from_long_long(long long v)
     \brief Initializes this object from the given long long, clipping the value
            at the boundaries if required
     \param     value  The value to use for initialization.
     */
    virtual void from_long_long(long long v)
    {
        this->operator=(rm_internal::generic_var_convert<long long, C>(v));
    }
    /**
     \fn    virtual void generic_var::from_unsigned_long_long(unsigned long long v)
     \brief Initializes this object from the given unsigned long long, clipping the value
            at the boundaries if required
     \param     value  The value to use for initialization.
     */
    virtual void from_unsigned_long_long(unsigned long long v)
    {
        this->operator=(rm_internal::generic_var_convert<unsigned long long, C>(v));
    }
    /**
     \fn    virtual void generic_var::from_float(float v)
     \brief Initializes this object from the given float, rounding and clipping the value
            at the boundaries if required
     \param     value  The value to use for initialization.
     */
    virtual void from_float(float v)
    {
        this->operator=(rm_internal::generic_var_convert<float, C>(v));
    }
    /**
     \fn    virtual void generic_var::from_double(double v)
     \brief Initializes this object from the given double, rounding and clipping the value
            at the boundaries if required
     \param     value  The value to use for initialization.
     */
    virtual void from_double(double v)
    {
        this->operator=(rm_internal::generic_var_convert<double, C>(v));
    }
    /**
     \fn    virtual void generic_var::from_char_ptr(const char* s)
     \brief Initializes this object from the given character pointer, parsing the
            string for its value if needed, rounding and clipping the value.
     \param     value  The null terminated string to use for initialization.
     */
    virtual void from_char_ptr(const char* s)
    {
        C dat;
        value_var::str_to_value(s, dat);
        this->operator=(dat);
    }
    /**
     \fn    virtual istream& generic_var::operator>>(istream& s)
     \brief Stream extraction operator that reads its value from
            the stream. It behaves like the counterpart for the
            basic data type, except that NaNs are also handled for
            floats.
     \param [in,out]    s   an istream to process.
     \returns   The input stream.
     */
    virtual istream& operator>>(istream& s)
    {
        char tmp[1024];
        s >> tmp;
        if (tmp[0] == 'N')
        {
            nullValue();
        }
        else
        {
            C dat;
            value_var::str_to_value(tmp, dat);
            this->operator=(dat);
        }
        return s;
    }
    /**
     \fn    virtual ostream& generic_var::operator<<(ostream& s) const { if (this->isValid()) { s << value_to_str(this->data);
     \brief Stream insertion operator that writes its value to
            the stream. It behaves like the counterpart for the
            basic data type, except that NaNs are also handled for
            floats.
     \param [in,out]    s   an ostream to process.
     \returns   The output stream.
     */
    virtual ostream& operator<<(ostream& s) const
    {
        if (this->isValid())
        {
            s << value_to_str(this->data);
        }
        else
        {
            s << 'N';
        }
        return s;
    }

};
/**
 \fn    template<class C> C operator+(const generic_var<C>& left, C right)
 \brief Addition operator
 \param     left    The first value.
 \param     right   A value to add to it.
 \returns   The result of the operation.
 */
template<class C>
C operator+(const generic_var<C>& left, C right)
{
    return static_cast<C>(left) + right;
}
/**
 \fn    template<class C> C operator+(C left, const generic_var<C>& right)
 \brief Addition operator
 \param     left    The first value.
 \param     right   A value to add to it.
 \returns   The result of the operation.
 */
template<class C>
C operator+(C left, const generic_var<C>& right)
{
    return left + static_cast<C>(right);
}
/**
 \fn    template<class C> C operator+(const generic_var<C>& left, const generic_var<C>& right)
 \brief Addition operator
 \param     left    The first value.
 \param     right   A value to add to it.
 \returns   The result of the operation.
 */
template<class C>
C operator+(const generic_var<C>& left, const generic_var<C>& right)
{
    return static_cast<C>(left) + static_cast<C>(right);
}
/**
 \fn    template<class C> C operator-(const generic_var<C>& left, C right)
 \brief Subtraction operator
 \param     left    The first value.
 \param     right   A value to subtract from it.
 \returns   The result of the operation.
 */
template<class C>
C operator-(const generic_var<C>& left, C right)
{
    return static_cast<C>(left) - right;
}
/**
 \fn    template<class C> C operator-(C left, const generic_var<C>& right)
 \brief Subtraction operator
 \param     left    The first value.
 \param     right   A value to subtract from it.
 \returns   The result of the operation.
 */
template<class C>
C operator-(C left, const generic_var<C>& right)
{
    return left - static_cast<C>(right);
}
/**
 \fn    template<class C> C operator-(const generic_var<C>& left, const generic_var<C>& right)
 \brief Subtraction operator
 \param     left    The first value.
 \param     right   A value to subtract from it.
 \returns   The result of the operation.
 */
template<class C>
C operator-(const generic_var<C>& left, const generic_var<C>& right)
{
    return static_cast<C>(left) - static_cast<C>(right);
}
/**
 \fn    template<class C> C operator*(const generic_var<C>& left, C right)
 \brief Multiplication operator
 \param     left    The first value to multiply.
 \param     right   The second value to multiply.
 \returns   The result of the operation.
 */
template<class C>
C operator*(const generic_var<C>& left, C right)
{
    return static_cast<C>(left) * right;
}
/**
 \fn    template<class C> C operator*(C left, const generic_var<C>& right)
 \brief Multiplication operator
 \param     left    The first value to multiply.
 \param     right   The second value to multiply.
 \returns   The result of the operation.
 */
template<class C>
C operator*(C left, const generic_var<C>& right)
{
    return left * static_cast<C>(right);
}
/**
 \fn    template<class C> C operator*(const generic_var<C>& left, const generic_var<C>& right)
 \brief Multiplication operator
 \param     left    The first value to multiply.
 \param     right   The second value to multiply.
 \returns   The result of the operation.
 */
template<class C>
C operator*(const generic_var<C>& left, const generic_var<C>& right)
{
    return static_cast<C>(left) * static_cast<C>(right);
}
/**
 \fn    template<class C> C operator/(const generic_var<C>& left, C right)
 \brief Division operator
 \param     left    The numerator.
 \param     right   The denominator.
 \returns   The result of the operation.
 */
template<class C>
C operator/(const generic_var<C>& left, C right)
{
    return static_cast<C>(left) / right;
}
/**
 \fn    template<class C> C operator/(C left, const generic_var<C>& right)
 \brief Division operator
 \param     left    The numerator.
 \param     right   The denominator.
 \returns   The result of the operation.
 */
template<class C>
C operator/(C left, const generic_var<C>& right)
{
    return left / static_cast<C>(right);
}
/**
 \fn    template<class C> C operator/(const generic_var<C>& left, const generic_var<C>& right)
 \brief Division operator
 \param     left    The numerator.
 \param     right   The denominator.
 \returns   The result of the operation.
 */
template<class C>
C operator/(const generic_var<C>& left, const generic_var<C>& right)
{
    return static_cast<C>(left) / static_cast<C>(right);
}
/**
 \fn    template<class C> bool operator==(C left, const generic_var<C>& right)
 \brief Equality operator
 \param     left    The first instance to compare.
 \param     right   The second instance to compare.
 \returns   True if the parameters are considered equivalent.
 */
template<class C>
bool operator==(C left, const generic_var<C>& right)
{
    return right.operator==(left);
}
/**
 \fn    template<class C> bool operator<(C left, const generic_var<C>& right)
 \brief Less-than comparison operator
 \param     left    The first instance to compare.
 \param     right   The second instance to compare.
 \returns   True if the first parameter is less than the second.
 */
template<class C>
bool operator<(C left, const generic_var<C>& right)
{
    return !(right.operator<(left) || right.operator==(left));
}
/**
 \fn    template<class C> bool operator!=(const generic_var<C>& left, C right)
 \brief Inequality operator
 \param     left    The first instance to compare.
 \param     right   The second instance to compare.
 \returns   True if the parameters are not considered equivalent.
 */
template<class C>
bool operator!=(const generic_var<C>& left, C right)
{
    return !left.operator==(right);
}
/**
 \fn    template<class C> bool operator!=(C left, const generic_var<C>& right)
 \brief Inequality operator
 \param     left    The first instance to compare.
 \param     right   The second instance to compare.
 \returns   True if the parameters are not considered equivalent.
 */
template<class C>
bool operator!=(C left, const generic_var<C>& right)
{
    return !right.operator==(left);
}
/**
 \fn    template<class C> bool operator!=(const generic_var<C>& left, const generic_var<C>& right)
 \brief Inequality operator
 \param     left    The first instance to compare.
 \param     right   The second instance to compare.
 \returns   True if the parameters are not considered equivalent.
 */
template<class C>
bool operator!=(const generic_var<C>& left, const generic_var<C>& right)
{
    return !left.operator==(right);
}
/**
 \fn    template<class C> bool operator<=(const generic_var<C>& left, C right)
 \brief Less-than-or-equal comparison operator
 \param     left    The first instance to compare.
 \param     right   The second instance to compare.
 \returns   True if the first parameter is less than or equal to the second.
 */
template<class C>
bool operator<=(const generic_var<C>& left, C right)
{
    return left.operator==(right) || left.operator<(right);
}
/**
 \fn    template<class C> bool operator<=(C left, const generic_var<C>& right)
 \brief Less-than-or-equal comparison operator
 \param     left    The first instance to compare.
 \param     right   The second instance to compare.
 \returns   True if the first parameter is less than or equal to the second.
 */
template<class C>
bool operator<=(C left, const generic_var<C>& right)
{
    return !right.operator<(left);
}
/**
 \fn    template<class C> bool operator<=(const generic_var<C>& left, const generic_var<C>& right)
 \brief Less-than-or-equal comparison operator
 \param     left    The first instance to compare.
 \param     right   The second instance to compare.
 \returns   True if the first parameter is less than or equal to the second.
 */
template<class C>
bool operator<=(const generic_var<C>& left, const generic_var<C>& right)
{
    return left.operator==(right) || left.operator<(right);
}
/**
 \fn    template<class C> bool operator>(const generic_var<C>& left, C right)
 \brief Greater-than comparison operator
 \param     left    The first instance to compare.
 \param     right   The second instance to compare.
 \returns   True if the first parameter is greater than to the second.
 */
template<class C>
bool operator>(const generic_var<C>& left, C right)
{
    return !operator<=(left, right);
}
/**
 \fn    template<class C> bool operator>(C left, const generic_var<C>& right)
 \brief Greater-than comparison operator
 \param     left    The first instance to compare.
 \param     right   The second instance to compare.
 \returns   True if the first parameter is greater than to the second.
 */
template<class C>
bool operator>(C left, const generic_var<C>& right)
{
    return right.operator<(left);
}
/**
 \fn    template<class C> bool operator>(const generic_var<C>& left, const generic_var<C>& right)
 \brief Greater-than comparison operator
 \param     left    The first instance to compare.
 \param     right   The second instance to compare.
 \returns   True if the first parameter is greater than to the second.
 */
template<class C>
bool operator>(const generic_var<C>& left, const generic_var<C>& right)
{
    return !operator<=(left, right);
}
/**
 \fn    template<class C> bool operator>=(const generic_var<C>& left, C right)
 \brief Greater-than-or-equal comparison operator
 \param     left    The first instance to compare.
 \param     right   The second instance to compare.
 \returns   True if the first parameter is greater than or equal to the second.
 */
template<class C>
bool operator>=(const generic_var<C>& left, C right)
{
    return !left.operator<(right) || left.operator==(right);
}
/**
 \fn    template<class C> bool operator>=(C left, const generic_var<C>& right)
 \brief Greater-than-or-equal comparison operator
 \param     left    The first instance to compare.
 \param     right   The second instance to compare.
 \returns   True if the first parameter is greater than or equal to the second.
 */
template<class C>
bool operator>=(C left, const generic_var<C>& right)
{
    return right.operator<(left) || right.operator==(left);
}
/**
 \fn    template<class C> bool operator>=(const generic_var<C>& left, const generic_var<C>& right)
 \brief Greater-than-or-equal comparison operator
 \param     left    The first instance to compare.
 \param     right   The second instance to compare.
 \returns   True if the first parameter is greater than or equal to the second.
 */
template<class C>
bool operator>=(const generic_var<C>& left, const generic_var<C>& right)
{
    return !left.operator<(right) || left.operator==(right);
}
#endif /* _GEN_VAR_H_ */

/******************************************************************/
/* Copyright DSO National Laboratories 2009. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _UNION_VAR_H_
#define _UNION_VAR_H_

#include <deci_var.h>

struct union_var_private;

//! The composite class that acts as a container for a union of multiple mpt_var objects, selected by a child gen_var
class RM_EXPORT union_var : public decision_var
{
public:
    union_var(unsigned int off = 0, unsigned int sz = 1, const char* selectors = NULL);
    union_var(const union_var& right);
    ~union_var();

    virtual int extract(int len, const unsigned char* buf);
    virtual void output(outbuf& strm);
    virtual bool extract_selector(int buflen, const unsigned char* buf, unsigned int& sel);
    virtual bool output_selector(int buflen, unsigned char* buf, unsigned int sel);

    virtual const mpt_var& operator=(const mpt_var& right);
    virtual bool operator==(const mpt_var& right) const;
    const union_var& operator=(const union_var& right);
    bool operator==(const union_var& right) const;

    int findChild(unsigned int value) const;
    unsigned int getSelector(unsigned int idx) const;
    void setSelector(unsigned int idx, unsigned int value);
    void setSelectors(const char* selstr);

protected:
    union_var_private* prvt;

};

class RM_EXPORT r_union_var : public union_var
{
public:
    r_union_var(unsigned int off = 0, unsigned int sz = 1, const char* selstr = NULL);
    r_union_var(const r_union_var& right);

    virtual const mpt_var& operator=(const mpt_var& right);
    const r_union_var& operator=(const r_union_var& right);

    virtual bool extract_selector(int buflen, const unsigned char* buf, unsigned int& sel);
    virtual bool output_selector(int buflen, unsigned char* buf, unsigned int sel);
};

#define UNIONCONST(t,off,selsz,selstr) t(const t& arg) : union_var(arg) \
{ \
    child_copy_constructor(arg); \
    init_child_observers(); \
} \
t() : union_var(off, (selsz), selstr) \
{ \
    pop(); \
    init_child_observers(); \
} \
const t& operator=(const t& right) \
{ \
    union_var::operator=(right); \
    return *this; \
} \
const mpt_var& operator=(const mpt_var& right) \
{ \
    union_var::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)

#define RUNIONCONST(t,off,selsz,selstr) t(const t& arg) : r_union_var(arg) \
{ \
    child_copy_constructor(arg); \
    init_child_observers(); \
} \
t() : r_union_var(off, (selsz), selstr) \
{ \
    pop(); \
    init_child_observers(); \
} \
const t& operator=(const t& right) \
{ \
    r_union_var::operator=(right); \
    return *this; \
} \
const mpt_var& operator=(const mpt_var& right) \
{ \
    r_union_var::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)

#endif /* _UNION_VAR_H_ */

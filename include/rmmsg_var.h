/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _RMMSG_VAR_H_
#define _RMMSG_VAR_H_

#include <rm_var.h>

//! A special RM message that is not stored persistantly in RM. These are usually used from commands between clients.
/*!
    It has the same RTTI as rm_var and should not introduce added properties
 */
class RM_EXPORT rm_message_var : public rm_var
{
public:
    rm_message_var(const char nm[], bool regflag);
    virtual int extract(int len, const unsigned char* buf);
    virtual bool write(outbuf& buf);

private:
    rm_message_var();
};

#define RMMSGCONST_1(t,mode) t(const char* nm) : rm_message_var(nm, mode) \
{ \
    pop(); \
} \
STD_RMVAR_METHODS(t)

#define RMMSGCONST(t,nm,mode) t() : rm_message_var(nm, mode) \
{ \
    pop(); \
} \
t(mpt_var &link) : rm_message_var(nm, mode) \
{ \
    pop(); \
    *((*this)[0]) = link; \
} \
STD_RMVAR_METHODS(t)

#define RMMSGCONST_ARRAY(t,nm,mode) t() : rm_message_var("", mode) \
{ \
    pop(); \
    rm_minisprintf(rmmsgname, nm, classIndex()+1); \
} \
t(mpt_var &link) : rm_message_var("", mode) \
{ \
    pop(); \
    rm_minisprintf(rmmsgname, nm, classIndex()+1); \
    *((*this)[0]) = link; \
} \
STD_RMVAR_METHODS(t)

#define RMMSGCONST_ARRAY2(t,nm,mode,div) t() : rm_message_var("", mode) \
{ \
    pop(); \
    int cli = classIndex(); \
    rm_minisprintf(rmmsgname, nm, (cli / (div)) + 1, (cli % (div)) + 1); \
} \
t(mpt_var &link) : rm_message_var("", mode) \
{ \
    pop(); \
    int cli = classIndex(); \
    rm_minisprintf(rmmsgname, nm, (cli / (div)) + 1, (cli % (div)) + 1); \
    *((*this)[0]) = link; \
} \
STD_RMVAR_METHODS(t)

#endif /* _RMMSG_VAR_H_ */

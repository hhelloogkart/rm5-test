/******************************************************************/
/* Copyright DSO National Laboratories 2005. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _RMINTERF_H_
#define _RMINTERF_H_

#include <cplx_var.h>

class RMBaseSocket;
class cache_typ;
class defaults;
struct rm_interface_private;

//! The base class for representing an interface. RM5 allows multiple RM interfaces. Messages that are aggregated into subclasses of rm_interface represent the messages that related to that interface.
class RM_EXPORT rm_interface : public complex_var
{
public:
    rm_interface();
    ~rm_interface();
    RMBaseSocket& Socket() const;
    void doSetList(unsigned int id, const cache_typ*);
    void doGetList(unsigned int id);
    void doClientID(unsigned int id, const cache_typ*);
    void doQueryID(unsigned int id, const cache_typ*);
    void doRegisterID(unsigned int id, const cache_typ*);
    bool check_connect();
    bool try_connect();
    void enable();
    void disable();
    void setHost(const char* host);

    static int interface_count();
    static void init(defaults*);
    static void set_select_timeout(struct timeval* wait);
    static void incoming();
    static void outgoing();
    static void close();

private:
    void initialize(const char* client, int port, const char* host,
                    RMBaseSocket* clientsock, unsigned int flow);

protected:
    rm_interface_private* prvt;
};

#define RMINTERFACECONST(t) t(const t& arg) \
    : rm_interface() \
{ \
    child_copy_constructor(arg); \
} \
t() \
{ \
    pop(); \
} \
const t& operator=(const t& right) \
{ \
    complex_var::operator=(right); \
    return *this; \
} \
const mpt_var& operator=(const mpt_var& right) \
{ \
    complex_var::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)

#endif /* _RMINTERF_H_ */

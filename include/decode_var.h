/******************************************************************/
/* Copyright DSO National Laboratories 2016. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _DECODE_VAR_H_
#define _DECODE_VAR_H_

#include <gen_var.h>

/**
 \class   vdecode_var
 \brief   Base class for floating point values whose data is represented integers in the byte stream
 \details This is the abstract base class that helps to decode integer data, signed or unsigned
          into a double. Users define two parameters: range and offset. The range defines the floating
          point interval that the integer will represent, and the offset is a floating point bias
          to the range. For example, range 360.0, offset 0.0, depicts a representation from 0.0 to
          360.0. Range 360.0, offset -180.0, depicts a representation from -180.0 to 180.0. The scale
          offset defines the reduction in the representation at the maximum float value, and its
          default is 1. For example, for angles 0 deg equals 360 deg, so there is no point to represent
          up to 360 deg, but 1 integer quantum less than 360 deg and so we use scale offset of 1. In
          the situtation where the full range is needed, for example -45deg to 45deg, then we should
          set scale offset to 0, range to 90.0, offset to -45.0.

          It handles all signed and unsigned integers of byte widths 1, 2, 4 and bitfields from 1 to
          32 bits, in normal or reversed bit/byte orders.

              encoded_value = value * range / (2.0^(8*abs(_byte_size)) - _scale_offset) + _offset

 */
class RM_EXPORT vdecode_var : public generic_var<double>
{
public:

    /**
     \fn    vdecode_var::vdecode_var(int _code_typ, double _range, double _offset = 0.0, int _scale_offset = 1);
     \brief Constructor
     \details When the encoding is based on twos-complement, then you should use the signed byte fields.
              Example: for -180 to 180 the range should be 360, offset 0.
              When the encoding is not based on two-complement, then you should use the unsigned byte fields,
              Example: for -180 to 180 the range should be 360, offset 180.
     \param     _code_typ       Type of the encoded data                                                <br/>
                                (code_typ & 0xff)       - lsb contains number of bits (between 1 to 32) <br/>
                                (code_typ >> 8) & 0xff  - msb contains data type                        <br/>
                                0 - byte field (unsigned)                                               <br/>
                                1 - bit field (unsigned)                                                <br/>
                                2 - reverse byte-order byte field (unsigned)                            <br/>
                                3 - reverse bit-order bit field (unsigned)                              <br/>
                                4 - byte field (signed)                                                 <br/>
                                5 - reverse byte-order byte field (signed)                              <br/>
                                6 - bit field (signed)                                                  <br/>
                                7 - reverse bit-order bit field (signed)                                <br/>
                                When a byte field is selected, bits will be rounded up to next 1,2 or 4 bytes
     \param     _range          The floating point range represented. If range is 0, scaling is 1.0
     \param     _offset         (Optional) The floating point offset represented.
     \param     _scale_offset   (Optional) The integer scale offset.
     */
    vdecode_var(int _code_typ, double _range, double _offset = 0.0, int _scale_offset = 1);
    /**
     \fn    vdecode_var::~vdecode_var();
     \brief Destructor
     */
    ~vdecode_var();
    /**
     \fn    double vdecode_var::getScaling() const;
     \brief Gets the scaling factor that incoming integer is multiplied to
     \details Formula:

                  scaling = _range / (static_cast<double>(int_range - _scale_offset) + 1);

     \returns   The scaling factor.
     */
    double getScaling() const;
    /**
     \fn    double vdecode_var::getOffset() const;
     \brief Gets the floating point offset
     \returns   The offset.
     */
    double getOffset() const;
    /**
     \fn    virtual int vdecode_var::extract(int buflen, const unsigned char* buf);
     \brief Reads the byte or bit values from the bit stream. If the value is changed,
            setDirty() is called, otherwise notDirty() is called.
     \param     buflen  The length.
     \param     buf     The buffer.
     \returns   Number of bytes remaining that was not extracted.
     */
    virtual int extract(int buflen, const unsigned char* buf);
    /**
     \fn    virtual bool vdecode_var::extract_zero(const unsigned char* buf);
     \brief Read the last byte from the byte stream if the bit offset is non zero.
     \param     buf The buffer.
     \returns   True if it succeeds, false if it fails.
     */
    virtual bool extract_zero(const unsigned char* buf);
    /**
     \fn    virtual int vdecode_var::size() const;
     \brief Returns the size in bytes that this object will need to serialize itself into
            a byte stream.
     \returns   Size in bytes.
     */
    virtual int size() const;
    /**
     \fn    virtual void vdecode_var::output(outbuf& strm);
     \brief Outputs its data into a byte stream writing into outbuf. If the double value is
            beyond the range of the integer / bitfield representation, the integer will be
            clipped at either boundaries. Note: It does not roll over.
     \param [out]    strm    Output buffer.
     */
    virtual void output(outbuf& strm);
    /**
     \fn    virtual mpt_var* vdecode_var::getNext();
     \brief Gets the next item if this item was created in an array, otherwise undefined.
     \returns   Pointer to next item in array.
     */
    virtual mpt_var* getNext();
    /**
     \fn    virtual unsigned int vdecode_var::rtti() const;
     \brief The will return the rtti() of the underlying object that is used for handling
            the byte stream extracts and output, which can be a generic_var or a vbit_var,
            or their reverse endian equavalents. This is needed so that bit_var bit position
            detection can work correctly.
     \returns   An int.
     */
    virtual unsigned int rtti() const;
    /**
     \fn    virtual const mpt_var& vdecode_var::operator=(const mpt_var& right);
     \brief Assignment operator that behaves just like generic_var<double>. There are no checks
            to see if the value is within range for the range.
     \param     right   The value to be copied.
     \returns   A reference of this object.
     */
    virtual const mpt_var& operator=(const mpt_var& right);
    /**
     \fn    double vdecode_var::operator=(double right);
     \brief Assignment operator. Value will only be copied if it can be transformed
            into an unsigned int. There are no checks to see if the value is within
            range for the range.
     \param     right   The value to be copied.
     \returns   A reference of this object.
     */
    double operator=(double right);
    /**
     \fn    const vdecode_var& vdecode_var::operator=(const vdecode_var& right);
     \brief Assignment operator. setDirty() will be called if the value is changed, and
            notDirty() otherwise. There are no checks to see if the value is within
            range for the range.
     \param     right   The value to be copied.
     \returns   A reference of this object.
     */
    const vdecode_var& operator=(const vdecode_var& right);
    /**
     \fn    const value_var* vdecode_var::getMsg() const;
     \brief Gets the underlying variable used to extract and output data from the byte
            stream.
     \returns   Const pointer to generic_var, r_generic_var, vbit_var or rvbit_var.
     */
    const value_var* getMsg() const;

protected:

    /**
     \fn    virtual int vdecode_var::encodeValueSigned(double parameter1);
     \brief Encode value a double value into signed integer.
     \param     value  The double value.
     \returns   An int.
     */
    virtual int encodeValueSigned(double);
    /**
     \fn    virtual unsigned int vdecode_var::encodeValueUnsigned(double parameter1);
     \brief Encode value a double value into unsigned integer.
     \param     value  The double value.
     \returns   An int.
     */
    virtual unsigned int encodeValueUnsigned(double);
    /**
     \fn    virtual double vdecode_var::decodeValueSigned(int parameter1);
     \brief Decodes a signed integer into a double value.
     \param     integer The signed integer.
     \returns   A double.
     */
    virtual double decodeValueSigned(int);
    /**
     \fn    virtual double vdecode_var::decodeValueUnsigned(unsigned int);
     \brief Decodes a unsigned integer into a double value.
     \param     integer The unsigned integer.
     \returns   A double.
     */
    virtual double decodeValueUnsigned(unsigned int);

private:
    explicit vdecode_var();
    explicit vdecode_var(const vdecode_var& cpy);

    double scaling;                   /*!< The scaling factor to be multiplied to integer value */
    double offset;                    /*!< The offset to be added to integer value */
    double upper_limit;               /*!< Encodes are less than this value */
    double lower_limit;               /*!< Encodes are more than or equal this value */
    bool convert_signed_to_unsigned;  /*!< Get unsigned value from underlying variable and cast it to signed */
    value_var* msg;     /*!< Underlying variable used to extract and output data from the byte stream */

};

/**
 \class   decode_var
 \brief   Functions as a double but whose data is represented by an integer in the byte stream
 \details Decodes integer data into double. The integer data may be sign and unsigned versions of char, short,
          or int. Reverse endian encoding may also be elected. Users define two parameters: range and offset.
          The range defines the floating point interval that the integer will represent, and the offset is a
          floating point bias to the range. For example, range 360.0, offset 0.0, depicts a representation from
          0.0 to 360.0. Range 360.0, offset -180.0, depicts a representation from -180.0 to 180.0. The scale
          offset defines the reduction in the representation at the maximum float value, and its
          default is 1. For example, for angles 0 deg equals 360 deg, so there is no point to represent
          up to 360 deg, but 1 integer quantum less than 360 deg and so we use scale offset of 1. In
          the situtation where the full range is needed, for example -45deg to 45deg, then we should
          set scale offset to 0, range to 90.0, offset to -45.0.

              encoded_value = value * range / (2.0^(8*abs(_byte_size)) - _scale_offset) + _offset
 */
class RM_EXPORT decode_var : public vdecode_var
{
public:

    /**
     \fn    decode_var::decode_var(int _byte_size, double _range, double _offset = 0.0, int _scale_offset = 1);
     \brief Constructor
     \details When the encoding is based on twos-complement, then you should use the signed byte fields.
              Example: for -180 to 180 the range should be 360, offset 0.
              When the encoding is not based on two-complement, then you should use the unsigned byte fields,
              Example: for -180 to 180 the range should be 360, offset 180.
     \param   _byte_size        Size of the encoded data.                                   <br/>
                                When byte_size is n or -n, the encoded data is n bytes.     <br/>
                                When byte_size is negative, the encoded data is signed.     <br/>
                                When byte_size is positive, the encoded data is unsigned.   <br/>
                                byte_size = 0 or abs(byte_size) > 4 are invalid.
     \param _range              Range of value to encoded, if range is 0, scaling is 1.0
     \param _offset             (Optional) Offset of encoded value
     \param _scale_offset       (Optional) The integer scale offset.
     */
    decode_var(int _byte_size, double _range, double _offset = 0.0, int _scale_offset = 1);
    /**
     \fn    virtual const mpt_var& decode_var::operator=(const mpt_var& right);
     \brief Assignment operator that behaves just like generic_var<double>. There are no checks
            to see if the value is within range for the range.
     \param     right   The value to be copied.
     \returns   A reference of this object.
     */
    virtual const mpt_var& operator=(const mpt_var& right);
    /**
     \fn    double decode_var::operator=(double right);
     \brief Assignment operator. Value will only be copied if it can be transformed
            into an unsigned int. There are no checks to see if the value is within
            range for the range.
     \param     right   The value to be copied.
     \returns   A reference of this object.
     */
    double operator=(double right);
    /**
     \fn    const decode_var& decode_var::operator=(const decode_var& right);
     \brief Assignment operator. setDirty() will be called if the value is changed, and
            notDirty() otherwise. There are no checks to see if the value is within
            range for the range.
     \param     right   The value to be copied.
     \returns   A reference of this object.
     */
    const decode_var& operator=(const decode_var& right);

private:
    explicit decode_var();
    explicit decode_var(const decode_var& cpy);
};

/**
 \class templ_decode_var
 \brief Template class used to declare derived classes from decode_var. It is used
        by the code generator which consolidates the decoding parameters into a separate set
        of structs.
 \tparam  C  Struct that defines decode parameters int C::SIZE, double C::RANGE, double C::OFFSET and int C::SCALE_OFFSET.
*/
template<typename C>
class templ_decode_var : public decode_var
{
public:
    /**
     \fn    templ_decode_var::templ_decode_var() : decode_var(C::SIZE, C::RANGE, C::OFFSET, C::SCALE_OFFSET)
     \brief Default constructor
     */
    templ_decode_var() : decode_var(C::SIZE, C::RANGE, C::OFFSET, C::SCALE_OFFSET)
    {
    }
    /**
     \fn    double templ_decode_var::operator=(double right)
     \brief Assignment operator. Value will only be copied if it can be transformed
            into an unsigned int. There are no checks to see if the value is within
            range for the range.
     \param     right   The value to be copied.
     \returns   A reference of this object.
     */
    double operator=(double right)
    {
        return generic_var<double>::operator=(right);
    }
    /**
     \fn    const templ_decode_var<C>& templ_decode_var::operator=(const templ_decode_var<C>& right)
     \brief Assignment operator. setDirty() will be called if the value is changed, and
            notDirty() otherwise. There are no checks to see if the value is within
            range for the range.
     \param     right   The value to be copied.
     \returns   A reference of this object.
     */
    const templ_decode_var<C>& operator=(const templ_decode_var<C>& right)
    {
        generic_var<double>::operator=(right);
        return *this;
    }
    /**
     \fn    virtual const mpt_var& templ_decode_var::operator=(const mpt_var& right)
     \brief Assignment operator that behaves just like generic_var<double>. There are no checks
            to see if the value is within range for the range.
     \param     right   The value to be copied.
     \returns   A reference of this object.
     */
    virtual const mpt_var& operator=(const mpt_var& right)
    {
        return generic_var<double>::operator=(right);
    }
private:
    explicit templ_decode_var(const templ_decode_var&);

};
/**
 \def   DEFINE_DECODE_VAR(clsnm, byte_size, range, offset, soffset)
 \brief A macro that defines define decode_vars
 \param     clsnm       The name of the class.
 \param     byte_size   Size of the byte.
 \param     range       The range.
 \param     offset      The offset.
 \param     soffset     The scale offset.
 */
#define DEFINE_DECODE_VAR(clsnm, byte_size, range, offset, soffset)  \
class clsnm : public decode_var \
{ \
public: \
    clsnm() : decode_var(byte_size, range, offset, soffset) {} \
    mpt_var* getNext() { return this + 1; } \
};
#endif /* _DECODE_VAR_H_ */
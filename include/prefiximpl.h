#ifndef _PREFIXIMPL_H_
#define _PREFIXIMPL_H_

#include <limits>
#include <string.h>

#ifdef _WIN32
#include <intrin.h>
#endif
#if __cplusplus > 201103
#include <type_traits>
#endif

#if defined(__BYTE_ORDER) && __BYTE_ORDER == __BIG_ENDIAN || \
    defined(__BIG_ENDIAN__) || \
    defined(__ARMEB__) || \
    defined(__THUMBEB__) || \
    defined(__AARCH64EB__) || \
    defined(_MIBSEB) || defined(__MIBSEB) || defined(__MIBSEB__) || \
    defined(_M_PPC)
#define DECODE_BIG_ENDIAN
#endif

#ifdef DECODE_BIG_ENDIAN
union u
{
    double dbl;
    struct
    {
        int msw;
        unsigned int lsw;
    } st;
};
#else
union u
{
    double dbl;
    struct
    {
        unsigned int lsw;
        int msw;
    } st;
};
#endif

inline void extract_words(int& msw, unsigned int& lsw, const double& v)
{
    const u* const p = reinterpret_cast<const u*>(&v);
    msw = p->st.msw;
    lsw = p->st.lsw;
}

inline void insert_words(int msw, unsigned int lsw, double& v)
{
    u* const p = reinterpret_cast<u*>(&v);
    p->st.msw = msw;
    p->st.lsw = lsw;
}

inline double decode_round(double v)
{
    int msw;
    unsigned int lsw;

    extract_words(msw, lsw, v);

    /* Extract exponent */
    const int exponent_less_1023 = ((msw & 0x7ff00000) >> 20) - 1023;

    if (exponent_less_1023 < 20)
    {
        // rounding in msw
        if (exponent_less_1023 < 0)
        {
            // less than 1.0
            msw &= 0x80000000;
            if (exponent_less_1023 == -1)
            {
                msw |= (1023 << 20); // Result is +1.0 or -1.0
            }
            lsw = 0;
        }
        else
        {
            const unsigned int exponent_mask = 0x000fffff >> exponent_less_1023;
            if ((msw & exponent_mask) == 0 && lsw == 0)
            {
                return v; /* v is an integral value */

            }
            msw += 0x00080000 >> exponent_less_1023;
            msw &= ~exponent_mask;
            lsw = 0;
        }
    }
    else if (exponent_less_1023 > 51)
    {
        // no rounding needed
        if (exponent_less_1023 == 1024)
        {
            return v + v; /* v is NaN or inf */
        }
        else
        {
            return v;
        }
    }
    else
    {
        // rounding in lsw
        const unsigned int exponent_mask = 0xffffffff >> (exponent_less_1023 - 20);
        if ((lsw & exponent_mask) == 0)
        {
            return v; /* v is an integral value */

        }
        const unsigned int tmp = lsw + (1 << (51 - exponent_less_1023));
        if (tmp < lsw)
        {
            msw += 1;
        }
        lsw = tmp;
        lsw &= ~exponent_mask;
    }

    insert_words(msw, lsw, v);
    return v;
}

/*!
   Zero Bit Counting
   =================
   Currently only Windows version implemented, but
   there is similar functions in gcc __builtin_XXX.
   These functions tranlate into an assembler instruction
   which can be found in x86, x64, ARM and so on.
 */
#ifdef _WIN32
static unsigned short detect_lzcnt16(unsigned short);
static unsigned int detect_lzcnt(unsigned int);
#ifdef _WIN64
static unsigned __int64 detect_lzcnt64(unsigned __int64);
#endif

static unsigned short (* local_lzcnt16)(unsigned short) = &detect_lzcnt16;
static unsigned int (* local_lzcnt)(unsigned int) = &detect_lzcnt;
#ifdef _WIN64
static unsigned __int64 (* local_lzcnt64)(unsigned __int64) = &detect_lzcnt64;
#endif

static unsigned short slow_lzcnt16(unsigned short v)
{
    unsigned int count = 0;
    const unsigned short one = 0x1;
    for (int i = sizeof(v) * 8 - 1; i >= 0 && (((v >> i) & one) == 0); --i, ++count)
    {
        ;
    }
    return count;
}

static unsigned short fast_lzcnt16(unsigned short v)
{
    return __lzcnt16(v);
}

static unsigned int slow_lzcnt(unsigned int v)
{
    unsigned int count = 0;
    const unsigned int one = 0x1;
    for (int i = sizeof(v) * 8 - 1; i >= 0 && (((v >> i) & one) == 0); --i, ++count)
    {
        ;
    }
    return count;
}

static unsigned int fast_lzcnt(unsigned int v)
{
    return __lzcnt(v);
}

#ifdef _WIN64
static unsigned __int64 slow_lzcnt64(unsigned __int64 v)
{
    unsigned int count = 0;
    const unsigned long long one = 0x1;
    for (int i = sizeof(v) * 8 - 1; i >= 0 && (((v >> i) & one) == 0); --i, ++count)
    {
        ;
    }
    return count;
}

static unsigned __int64 fast_lzcnt64(unsigned __int64 v)
{
    return __lzcnt64(v);
}
#endif

inline void global_detect()
{
    int cpuinfo[4];
    __cpuid(cpuinfo, 0x80000001);
    if (cpuinfo[2] & 0x20)
    {
        local_lzcnt16 = &fast_lzcnt16;
        local_lzcnt = &fast_lzcnt;
#ifdef _WIN64
        local_lzcnt64 = &fast_lzcnt64;
#endif
    }
    else
    {
        local_lzcnt16 = &slow_lzcnt16;
        local_lzcnt = &slow_lzcnt;
#ifdef _WIN64
        local_lzcnt64 = &slow_lzcnt64;
#endif
    }
}

static unsigned short detect_lzcnt16(unsigned short v)
{
    global_detect();
    return (*local_lzcnt16)(v);
}

static unsigned int detect_lzcnt(unsigned int v)
{
    global_detect();
    return (*local_lzcnt)(v);
}

#ifdef _WIN64
static unsigned __int64 detect_lzcnt64(unsigned __int64 v)
{
    global_detect();
    return (*local_lzcnt64)(v);
}
#endif
#endif

inline unsigned int count_leading_zeros(unsigned char v)
{
#ifdef __GNUC__
    return __builtin_clz(v) - 16;
#elif defined(_WIN32)
    return (*local_lzcnt16)(v) - 8;
#else
    unsigned int count = 0;
    const unsigned char one = 0x1;
    for (int i = sizeof(v) * 8 - 1; i >= 0 && (((v >> i) & one) == 0); --i, ++count)
    {
        ;
    }
    return count;
#endif
}

inline unsigned int count_leading_zeros(unsigned short v)
{
#ifdef __GNUC__
    return __builtin_clz(v) - 8;
#elif defined(_WIN32)
    return (*local_lzcnt16)(v);
#else
    unsigned int count = 0;
    const unsigned short one = 0x1;
    for (int i = sizeof(v) * 8 - 1; i >= 0 && (((v >> i) & one) == 0); --i, ++count)
    {
        ;
    }
    return count;
#endif
}

inline unsigned int count_leading_zeros(unsigned int v)
{
#ifdef __GNUC__
    return __builtin_clz(v);
#elif defined(_WIN32)
    return (*local_lzcnt)(v);
#else
    unsigned int count = 0;
    const unsigned int one = 0x1;
    for (int i = sizeof(v) * 8 - 1; i >= 0 && (((v >> i) & one) == 0); --i, ++count)
    {
        ;
    }
    return count;
#endif
}

inline unsigned int count_leading_zeros(unsigned long v)
{
#ifdef __GNUC__
    return __builtin_clzl(v);
#elif defined(_WIN32)
    return (*local_lzcnt)(v);
#else
    unsigned int count = 0;
    const unsigned long one = 0x1;
    for (int i = sizeof(v) * 8 - 1; i >= 0 && (((v >> i) & one) == 0); --i, ++count)
    {
        ;
    }
    return count;
#endif
}

inline unsigned int count_leading_zeros(unsigned long long v)
{
#ifdef __GNUC__
    return __builtin_clzll(v);
#elif defined(_WIN32) && defined(_WIN64)
    return static_cast<unsigned int>((*local_lzcnt64)(v));
#elif defined(_WIN32)
    const unsigned int cnt = (*local_lzcnt)(static_cast<unsigned int>(v >> 32));
    return (cnt == 32) ? cnt + (*local_lzcnt)(static_cast<unsigned int>(v & 0xFFFFFFFF)) : cnt;
#else
    unsigned int count = 0;
    const unsigned long long one = 0x1;
    for (int i = sizeof(v) * 8 - 1; i >= 0 && (((v >> i) & one) == 0); --i, ++count)
    {
        ;
    }
    return count;
#endif
}

inline unsigned int count_leading_zeros(signed char v)
{
#ifdef __GNUC__
    return __builtin_clz(v) - 16;
#elif defined(_WIN32)
    return (*local_lzcnt16)(static_cast<unsigned char>(v)) - 8;
#else
    unsigned int count = 0;
    const signed char one = 0x1;
    for (int i = sizeof(v) * 8 - 1; i >= 0 && (((v >> i) & one) == 0); --i, ++count)
    {
        ;
    }
    return count;
#endif
}

inline unsigned int count_leading_zeros(char v)
{
#ifdef __GNUC__
    return __builtin_clz(v) - 16;
#elif defined(_WIN32)
    return (*local_lzcnt16)(static_cast<unsigned char>(v)) - 8;
#else
    unsigned int count = 0;
    const char one = '\x1';
    for (int i = sizeof(v) * 8 - 1; i >= 0 && (((v >> i) & one) == 0); --i, ++count)
    {
        ;
    }
    return count;
#endif
}

inline unsigned int count_leading_zeros(short v)
{
#ifdef __GNUC__
    return __builtin_clz(v) - 8;
#elif defined(_WIN32)
    return (*local_lzcnt16)(v);
#else
    unsigned int count = 0;
    const short one = 0x1;
    for (int i = sizeof(v) * 8 - 1; i >= 0 && (((v >> i) & one) == 0); --i, ++count)
    {
        ;
    }
    return count;
#endif
}

inline unsigned int count_leading_zeros(int v)
{
#ifdef __GNUC__
    return __builtin_clz(v);
#elif defined(_WIN32)
    return (*local_lzcnt)(v);
#else
    unsigned int count = 0;
    const int one = 0x1;
    for (int i = sizeof(v) * 8 - 1; i >= 0 && (((v >> i) & one) == 0); --i, ++count)
    {
        ;
    }
    return count;
#endif
}

inline unsigned int count_leading_zeros(long v)
{
#ifdef __GNUC__
    return __builtin_clzl(v);
#elif defined(_WIN32)
    return (*local_lzcnt)(v);
#else
    unsigned int count = 0;
    const long one = 0x1;
    for (int i = sizeof(v) * 8 - 1; i >= 0 && (((v >> i) & one) == 0); --i, ++count)
    {
        ;
    }
    return count;
#endif
}

inline unsigned int count_leading_zeros(long long v)
{
#ifdef __GNUC__
    return __builtin_clzll(v);
#elif defined(_WIN32) && defined(_WIN64)
    return static_cast<unsigned int>((*local_lzcnt64)(v));
#elif defined(_WIN32)
    const unsigned int cnt = (*local_lzcnt)(static_cast<unsigned int>(v >> 32));
    return (cnt == 32) ? cnt + (*local_lzcnt)(static_cast<unsigned int>(v & 0xFFFFFFFF)) : cnt;
#else
    unsigned int count = 0;
    const long long one = 0x1;
    for (int i = sizeof(v) * 8 - 1; i >= 0 && (((v >> i) & one) == 0); --i, ++count)
    {
        ;
    }
    return count;
#endif
}

inline unsigned int count_trailing_zeros(unsigned long v)
{
#ifdef __GNUC__
    return __builtin_ctzl(v);
#elif defined(_WIN32)
    unsigned long ret;
    return (_BitScanForward(&ret, v) == 0) ? 8 : ret;
#else
    unsigned int count = 0;
    const unsigned long one = 0x1;
    for (int i = 0; i < sizeof(v) * 8 && (((x >> i) & one) == 0); ++i, ++count)
    {
        ;
    }
    return count;
#endif
}

/*!
   Memory Loading
   ==============
   Unaigned buffer loading to and unloading from a basic data type
   Currently, only little-endian implemented - lower address = least significant.
 */
template<class T>
void unaligned_unload(char* const output, unsigned int bytes, T value)
{
    memcpy(output, &value, bytes);
}

template<class T>
T unaligned_load(const char* const input, unsigned int bytes)
{
    T ret = 0;
    memcpy(&ret, input, bytes);
    return ret;
}

/*!
   Zigzag Encoding
   ===============
   Output  | Represented
   ZZ Int  | Value
   --------+------------
   0       | 0
   1       | -1
   2       | 1
   3       | -2
   4       | 2
   ...     | ...

   For signed integers only, does nothing for unsigned
 */
template<class C>
inline C zigzag_encode(C value, char(*)[std::numeric_limits<C>::is_signed] = 0)
{
    return (value << 1) ^ (value >> (sizeof(C) * 8 - 1));
}

template<class C>
inline C zigzag_encode(C value, char(*)[std::numeric_limits<C>::is_signed == 0] = 0)
{
    return value;
}

template<class C>
inline C zigzag_decode(C value, char(*)[std::numeric_limits<C>::is_signed] = 0)
{
#if __cplusplus > 201103
    return (static_cast<typename std::make_unsigned<C>::type >(value) >> 1) ^ -(value & '\x1');
#else
    return (value >> 1) & std::numeric_limits<C>::max() ^ -(value & '\x1');
#endif
}

template<class C>
inline C zigzag_decode(C value, char(*)[std::numeric_limits<C>::is_signed == 0] = 0)
{
    return value;
}

/*!
   Prefix Variable Integer Encoding
   ================================
   xxxxxxx1  7 bits in 1 byte
   xxxxxx10 14 bits in 2 bytes
   xxxxx100 21 bits in 3 bytes
   xxxx1000 28 bits in 4 bytes
   xxx10000 35 bits in 5 bytes
   xx100000 42 bits in 6 bytes
   x1000000 49 bits in 7 bytes
   10000000 56 bits in 8 bytes
   00000000 64 bits in 9 bytes

   Good for unsigned integer types only
 */
/* Prefix encode - return number of bytes used */
template<class T>
int prefix_encode(char* const output, T value)
{
    value = zigzag_encode(value);
    const unsigned int bits = sizeof(T) * 8 - count_leading_zeros(static_cast<T>(value | '\x1'));
    const unsigned int bytes = 1 + (bits - 1) / 7;
    if (sizeof(T) > 4 && bits > 56)
    {
        *output = 0;
        unaligned_unload(output + 1, 8, value);
        return 9;
    }
    else
    {
        const unsigned long long v = (2 * static_cast<unsigned long long>(value) + 1) << (bytes - 1);
        unaligned_unload(output, bytes, v);
        return bytes;
    }
}

template<class T>
int prefix_decode(const char* const input, size_t length, T& value)
{
    unsigned int bytes;
    if (*input & 1)
    {
        bytes = 1;
        value = static_cast<T>(static_cast<unsigned char>(*input) >> 1); // disable sign extension
    }
    else
    {
        bytes = 1 + count_trailing_zeros(*input);
        if (bytes > length)
        {
            return 0;                 // error condition
        }
        if (bytes < 9)
        {
            const unsigned long long ret = unaligned_load<unsigned long long>(input, bytes);
            value = static_cast<T>(ret >> bytes);
        }
        else
        {
            value = static_cast<T>(unaligned_load<unsigned long long>(input + 1, bytes - 1));
        }
    }
    value = zigzag_decode(value);
    return bytes;
}

#endif /* _PREFIXIMPL_H_ */
/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#if !defined(AFX_DEFAULTS_H__9BC73062_F8E8_11D3_8E3D_0060975BF473__INCLUDED_)
#define AFX_DEFAULTS_H__9BC73062_F8E8_11D3_8E3D_0060975BF473__INCLUDED_

#include <rmglobal.h>
#ifndef NO_OUTBUF
#include <outbuf.h>
#endif
#include <stddef.h>

class file_buffer;
struct defaults_private;
struct tree_node;

class RM_EXPORT defaults
{
public:
    defaults();
    defaults(const char* _data, size_t len = 0);
    ~defaults();

    void load(const char* _data, size_t len = 0);
    void load(file_buffer& filebuf);
#ifndef NO_OUTBUF
    void save(outbuf& outdata, const char* prepend_str=0, int prepend_len=0);
    void save(file_buffer& filebuf);
#endif
    void clear();
    void parse_cmdline(int argc, char** argv);

    tree_node* getChild(tree_node* node = NULL);

    const char* getString(const char* srchkey, bool* b_fail = 0);
    int         getInt   (const char* srchkey, bool* b_fail = 0);
    double      getDouble(const char* srchkey, bool* b_fail = 0);

    tree_node* getstring(const char* srchkey, char* datavalue = 0, const tree_node* node = 0);
    tree_node* getint   (const char* srchkey, int* datavalue = 0, const tree_node* node = 0);
    tree_node* getdbl   (const char* srchkey, double* datavalue = 0, const tree_node* node = 0);
    tree_node* getdata  (const char* srchkey, char* datavalue = 0, const tree_node* node = 0);

    tree_node* setValue(const char* srchkey, const char* datavalue, tree_node* node = 0);
    tree_node* setValue(const char* srchkey, int datavalue,         tree_node* node = 0);
    tree_node* setValue(const char* srchkey, double datavalue,      tree_node* node = 0);
    tree_node* setValue(const char* srchkey, const char* datavalue, int datalen, tree_node* node = 0);

    tree_node* getnode(const char* srchkey, const tree_node* node = 0, bool force = false);
    tree_node* getenvnode(const char* srchkey);

private:
    defaults_private* prvt;

};

extern RM_EXPORT defaults* load_defaults(const char* def_config = 0, const char* prog_name = 0);
extern RM_EXPORT void bin2asc(const void* binsrc, char* asctgt, unsigned int sz);
extern RM_EXPORT void asc2bin(void* bintgt, const char* ascsrc, unsigned int sz);

#endif /* !defined(AFX_DEFAULTS_H__9BC73062_F8E8_11D3_8E3D_0060975BF473__INCLUDED_) */

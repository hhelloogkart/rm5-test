/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _FCPL_VAR_H_
#define _FCPL_VAR_H_

#include <cplx_var.h>

class RM_EXPORT flex_complex_var : public complex_var
{
public:
    RUNTIME_TYPE(flex_complex_var)
    flex_complex_var();
    virtual ~flex_complex_var();
    virtual int extract(int buflen, const unsigned char* buf);
    virtual int extract_cardinal(int buflen, const unsigned char* buf, int& car);

    virtual int size() const;
    virtual void output_cardinal(outbuf& strm);
    virtual void output(outbuf& strm);

    virtual bool isValid() const;
    virtual bool setInvalid();
    virtual mpt_var* getNext();
    virtual const mpt_var& operator=(const mpt_var& right);
    const flex_complex_var& operator=(const flex_complex_var& right);
    virtual bool operator==(const mpt_var& right) const;
    virtual istream& operator>>(istream& s);
    virtual ostream& operator<<(ostream& s) const;

    void add(mpt_var* p);
    void insert(mpt_var* p, int idx);
    void remove(int idx);
    void remove(mpt_var* p);

    virtual void resize(int nsize);

private:
    flex_complex_var(const flex_complex_var& right);

protected:
    virtual int width() const = 0;
    virtual int size_cardinal() const;
    virtual int cardinal_offset() const;
    virtual int cardinal_width() const;
    virtual mpt_var* create(mpt_var* cpy=0) const = 0;

};

#endif /*_FCPL_VAR_H_*/

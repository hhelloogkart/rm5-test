/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _FLUN_VAR_H_
#define _FLUN_VAR_H_

#include <cplx_var.h>

/*!
   Abstract factory struct for complex_var
 */
class RM_EXPORT complex_factory_base
{
public:
    complex_factory_base();
    /*reimp*/ virtual complex_var* create() = 0;
    /*reimp*/ virtual int type() = 0;
};

/*!
   Concrete factory
 */
template<class C, int V>
class complex_factory : public complex_factory_base
{
    /*reimp*/ complex_var* create()
    {
        return new C;
    }
    /*reimp*/ int type()
    {
        return V;
    }
};

struct flex_union_var_private;
class flex_union_show;
class flex_union_prvt_show;

class RM_EXPORT flex_union_var : public complex_var
{
    friend class flex_union_show;
    friend class flex_union_prvt_show;
    friend class multi_typ;

public:
    RUNTIME_TYPE(flex_union_var)
    flex_union_var();
    virtual ~flex_union_var();
    /*reimp*/ int extract(int len, const unsigned char* buf);
    /*reimp*/ int size() const;
    /*reimp*/ void output(outbuf& strm);
    /*reimp*/ bool setInvalid();
    /*reimp*/ const mpt_var& operator=(const mpt_var& right);
    /*reimp*/ bool operator==(const mpt_var& right) const;

    //overload function
    const flex_union_var& operator=(const flex_union_var& right);
    bool operator==(const flex_union_var& right) const;
    complex_var* create(int type, int* id);
    void destroy(int id);

    complex_var* item(int id) const;
    int item_cardinal() const;
    int item_first_id() const;
    int item_next_id(int id) const;
    int type_cardinal(int type) const;
    int type_first_id(int type) const;
    int type_next_id(int id) const;
    int addr_to_id(mpt_var* var);
    int typeOf(int id);

private:
    void setup_factories();
    int make_unique_id() const;
    complex_var* create(int type, int id);
    void addChildCallback(flex_union_show*);
    void removeChildCallback(flex_union_show*);
    void setChildDirty(int id);

    flex_union_var_private* prvt;

};

#define FLEXUNIONCONST(t) t(const t& arg) \
    : flex_union_var() \
{ \
    child_copy_constructor(arg); \
    setup_factories(); \
} \
t() \
{ \
    pop(); \
    setup_factories(); \
} \
const t& operator=(const t& right) \
{ \
    complex_var::operator=(right); \
    return *this; \
} \
const mpt_var& operator=(const mpt_var& right) \
{ \
    complex_var::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)

#endif /* _FLUN_VAR_H_ */
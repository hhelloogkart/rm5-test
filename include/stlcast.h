/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _STLCAST_H_
#define _STLCAST_H_

#if defined(STLPORT)
#  define LIST(typ) list<typ >
#  define LIST3(typ,alloc,mem) __list<typ,alloc<typ,mem > >
#  define LIST4(typ,rel,alloc,mem) __list<relative_ptr<typ,rel >,alloc<relative_ptr<typ,rel >,mem > >
#  define VECTOR(typ) vector<typ >
#  define VECTOR2(typ,alloc) __vector<typ,alloc<typ > >
#  define VECTOR3(typ,alloc,mem) __vector<typ,alloc<typ,mem > >
#  define SET(typ,cmp) set<typ,cmp >
#  define MAP(typidx,typval,cmp) map<typidx,typval,cmp >
#  define MAP2(typidx,typval,cmp,alloc) \
    __map<typidx,typval,cmp,alloc<pair<conXSst typidx,typval > > >
#  define MAP3(typidx,typval,cmp,alloc,mem) \
    __map<typidx,typval,cmp,alloc<pair<conXSst typidx,typval >,mem > >
#elif defined(_RWSTD_VER) && (_RWSTD_VER == 0x0121)
#  define LIST(typ) list<typ,allocator >
#  define LIST3(typ,alloc,mem) list<typ,alloc<typ,mem > >
#  define LIST4(typ,rel,alloc,mem) list<relative_ptr<typ,rel >,alloc<relative_ptr<typ,rel >,mem > >
#  define VECTOR(typ) vector<typ,allocator >
#  define VECTOR2(typ,alloc) vector<typ,alloc<typ > >
#  define VECTOR3(typ,alloc,mem) vector<typ,alloc<typ,mem > >
#  define SET(typ,cmp) set<typ,cmp,allocator >
#  define MAP(typidx,typval,cmp) map<typidx,typval,cmp,allocator >
#  define MAP2(typidx,typval,cmp,alloc) \
    map<typidx,typval,cmp,alloc<pair<const typidx,typval > > >
#  define MAP3(typidx,typval,cmp,alloc,mem) \
    map<typidx,typval,cmp,alloc<pair<const typidx,typval >, mem > >
#elif defined(_WIN32)
#  define LIST(typ) list<typ >
#  define LIST3(typ,alloc,mem) list<typ,alloc<typ,mem > >
#  define LIST4(typ,rel,alloc,mem) list<relative_ptr<typ,rel >,alloc<relative_ptr<typ,rel >,mem > >
#  define VECTOR(typ) vector<typ >
#  define VECTOR2(typ,alloc) vector<typ,alloc<typ > >
#  define SET(typ,cmp) set<typ >
#  define MAP(typidx,typval,cmp) map<typidx,typval >
#  define MAP2(typidx,typval,cmp,alloc) \
    map<typidx,typval,cmp,alloc<typval > >
#  if defined(_MSC_VER) && (_MSC_VER >= 1500)
#    define VECTOR3(typ,alloc,mem) rm::vector<typ,alloc<typ,mem > >
#    define MAP3(typidx,typval,cmp,alloc,mem) rm::map<typidx,typval,cmp,alloc<pair<const typidx,typval >,mem > >
#  else
#    define VECTOR3(typ,alloc,mem) vector<typ,alloc<typ,mem > >
#    define MAP3(typidx,typval,cmp,alloc,mem) \
    map<typidx,typval,cmp,alloc<typval,mem > >
#  endif
#elif defined(NO_DEF_ALLOCATOR)
#  define LIST(typ) list<typ,allocator<typ > >
#  define LIST3(typ,alloc,mem) list<typ,alloc<typ,mem > >
#  define LIST4(typ,rel,alloc,mem) list<relative_ptr<typ,rel >,alloc<relative_ptr<typ,rel >,mem > >
#  define VECTOR(typ) vector<typ,allocator<typ > >
#  define VECTOR2(typ,alloc) vector<typ,alloc<typ > >
#  define VECTOR3(typ,alloc,mem) vector<typ,alloc<typ,mem > >
#  define SET(typ,cmp) set<typ,cmp,allocator<typ > >
#  define MAP(typidx,typval,cmp) \
    map<typidx,typval,cmp,allocator<pair<const typidx,typval > > >
#  define MAP2(typidx,typval,cmp,alloc) \
    map<typidx,typval,cmp,alloc<pair<const typidx,typval > > >
#  define MAP3(typidx,typval,cmp,alloc,mem) \
    map<typidx,typval,cmp,alloc<pair<const typidx,typval >,mem > >
#elif (defined(__INTEL_COMPILER) && __INTEL_COMPILER >= 1000) || (defined(__GNUC__) && !defined(__INTEL_COMPILER) && __GNUC__ >= 3)
#  define LIST(typ) list<typ >
#  define LIST3(typ,alloc,mem) list<typ,alloc<typ,mem > >
#  define LIST4(typ,rel,alloc,mem) list<relative_ptr<typ,rel >,alloc<relative_ptr<typ,rel >,mem > >
#  define VECTOR(typ) vector<typ >
#  define VECTOR2(typ,alloc) vector<typ,alloc<typ > >
#  define VECTOR3(typ,alloc,mem) \
    rm::vector<typ,alloc<typ,mem > >
#  define SET(typ,cmp) set<typ,cmp >
#  define MAP(typidx,typval,cmp) map<typidx,typval,cmp >
#  define MAP2(typidx,typval,cmp,alloc) \
    map<typidx,typval,cmp,alloc<pair<const typidx,typval > > >
#  define MAP3(typidx,typval,cmp,alloc,mem) \
    rm::map<typidx,typval,cmp,alloc<pair<const typidx,typval >,mem > >
#else
#  define LIST(typ) list<typ >
#  define LIST3(typ,alloc,mem) list<typ,alloc<typ,mem > >
#  define LIST4(typ,rel,alloc,mem) list<relative_ptr<typ,rel >,alloc<relative_ptr<typ,rel >,mem > >
#  define VECTOR(typ) vector<typ >
#  define VECTOR2(typ,alloc) vector<typ,alloc<typ > >
#  define VECTOR3(typ,alloc,mem) vector<typ,alloc<typ,mem > >
#  define SET(typ,cmp) set<typ,cmp >
#  define MAP(typidx,typval,cmp) map<typidx,typval,cmp >
#  define MAP2(typidx,typval,cmp,alloc) \
    map<typidx,typval,cmp,alloc<pair<const typidx,typval > > >
#  define MAP3(typidx,typval,cmp,alloc,mem) \
    map<typidx,typval,cmp,alloc<pair<const typidx,typval >,mem > >
#endif

#endif  /* _STLCAST_H_ */

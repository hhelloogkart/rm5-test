/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _MPT_SHOW_H_
#define _MPT_SHOW_H_

#include <rmglobal.h>

class mpt_var;
struct mpt_autoshow_private;

//! The abstract base class for observer classes of mpt_var (the observable) Subclasses of this will be called during mpt_baseshow::refresh
class RM_EXPORT mpt_baseshow
{
public:
    mpt_baseshow();
    virtual ~mpt_baseshow();
    virtual void setDirty(mpt_var*);
    virtual void notDirty(mpt_var*);
    virtual void operator()();
    static void refresh();

};

//! Standard observer class that will trigger when the observable changes state.
class RM_EXPORT mpt_show : public mpt_baseshow
{
public:
    mpt_show();
    virtual void setDirty(mpt_var*);

protected:
    bool dirty;

};

//! Enhanced observer class with methods to connect to the observable with automatic disconnection during destruction
class RM_EXPORT mpt_autoshow : public mpt_show
{
public:
    mpt_autoshow();
    mpt_autoshow(const mpt_autoshow& other);
    mpt_autoshow& operator=(const mpt_autoshow& other);
    virtual ~mpt_autoshow();
    void addCallback(mpt_var*);
    void removeCallback(mpt_var*);

protected:
    mpt_autoshow_private* prvt;

};
#endif /* _MPT_SHOW_H_ */

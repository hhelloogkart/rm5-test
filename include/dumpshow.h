/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _DUMPSHOW_H_
#define _DUMPSHOW_H_

#include <mpt_show.h>

class mpt_var;
struct dump_show_private;

class RM_EXPORT dump_show : public mpt_show
{
public:
    dump_show(mpt_var* _data, const char* _msg = 0);
    virtual ~dump_show();
    virtual void operator()();
protected:
    mpt_var* data;
    dump_show_private* prvt;

};

#endif /* _DUMPSHOW_H_ */

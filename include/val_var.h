/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _VAL_VAR_H_
#define _VAL_VAR_H_

#include <rm_var.h>

struct valid_var_private;

class RM_EXPORT valid_var : public rm_var
{
public:
    RUNTIME_TYPE(valid_var)
    valid_var(const char* nm, bool mode);
    virtual ~valid_var();
    virtual int extract(int len, const unsigned char* buf);
    virtual int size() const;
    virtual void output(outbuf& strm);
    virtual bool isValid() const;
    virtual bool setInvalid();
    virtual mpt_var* getNext();
    virtual const mpt_var& operator=(const mpt_var& right);
    virtual bool operator==(const mpt_var& right) const;
    virtual istream& operator>>(istream& s);
    virtual ostream& operator<<(ostream& s) const;

    const valid_var& operator=(const valid_var& right);
    bool operator==(const valid_var& right) const;
    void setElement(int obj);
    void clrElement(int obj);
    int addElement();
    bool chkElement(int obj) const;

private:
    valid_var();
    valid_var_private* prvt;

};

#endif

/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _COMPRESS_VAR_H_
#define _COMPRESS_VAR_H_

#include <cplx_var.h>
#include <outbuf.h>

/**
 \class   compress_var
 \brief   Base class for compression algorithms that compress the serialized byte
          stream of its children.
 \details This class provides common functions for all compression algoritms,
          such as the provision of the raw_buffer and compressed buffer for output,
          and the decompressed buffer for input, and overloaded extract() and output()
          that will shunt the data through the decompress() and compress() functions.
 */
class RM_EXPORT compress_var : public complex_var
{
public:
    /**
     \fn    compress_var::compress_var();
     \brief Default constructor
     */
    compress_var();
    /**
     \fn    compress_var::compress_var(const compress_var& right);
     \brief Copy constructor
     \param     right   The object where the compression level will be copied.
     */
    compress_var(const compress_var& right);
    /**
     \fn    compress_var::~compress_var();
     \brief Destructor
     */
    ~compress_var();
    /**
     \fn    void compress_var::init();
     \brief Initializes this object, called by the constructor of derived class
            macro so that virtual functions arre resolved correctly.
     */
    void init();
    /**
     \fn    virtual int compress_var::extract(int len, const unsigned char* buf);
     \brief Decompresses the data in the buffer, beefore passing the decompressed
            data to its children for extraction.
     \param     len The length.
     \param     buf The buffer.
     \returns   Number of bytes remaining that was not extracted.
     */
    virtual int extract(int len, const unsigned char* buf);
    /**
     \fn    virtual void compress_var::output(outbuf& strm);
     \brief Calls each child too output into a raw buffer provided by this class
            before compressing it into a comprressed buffer which is
            copied into the outbuf provided. The compressed buffer is only
            regenerated each time this object is dirty to reduce CPU overhead.
     \param [out]   strm    The byte stream where bytes are pushed into.
     */
    virtual void output(outbuf& strm);
    /**
     \fn    virtual int compress_var::size() const;
     \brief Gets the size in bytes for this object to write itself into the
            byte stream. As compressed data is of variable length, and depends
            on the contents of its children, this will return the size of the
            internal cached compressed buffer. If this object is dirty, the
            compressed buffer will be regenerated.
     \returns   Size in bytes.
     */
    virtual int size() const;
    /**
     \fn    virtual void compress_var::setRMDirty();
     \brief Sets remote dirty flag for top-level message objects to output data
            at the next outgoing(). This is overridden for setting the dirty flag.
     */
    virtual void setRMDirty();
    /**
     \fn    virtual bool compress_var::setInvalid();
     \brief Resets all children to invalid state, returning true if at least one
            child succeeded in being reset. This is overridden for setting the
            dirty flag.
     \returns   True if it succeeds, false if it fails.
     */
    virtual bool setInvalid();
    /**
     \fn    unsigned int compress_var::compressionLevel();
     \brief Compression level. See setCompressionLevel().
     \returns   The compression level.
     */
    unsigned int compressionLevel();
    /**
     \fn    void compress_var::setCompressionLevel(unsigned int l);
     \brief Sets compression level. This is typically a value from 0 to 9, where
            0 means no compression and 9 is the highest level compression. It is
            algorithm dependent.
     \param     l   The compression level to set.
     */
    void setCompressionLevel(unsigned int l);
    /**
     \fn    const compress_var& compress_var::operator=(const compress_var& right);
     \brief Assignment operator
     \param     right   The right object to copy from.
     \returns   A reference of this object.
     */
    const compress_var& operator=(const compress_var& right);

protected:
    /**
     \fn    virtual int compress_var::compressionOverhead() const = 0;
     \brief Returns the compression overhead so that it can be added to the worst
            case compressed buffer size. Mostly this depends on the dictionary size
            of the compression algorithm.
     \returns   Size in bytes.
     */
    virtual int compressionOverhead() const = 0;
    /**
     \fn    virtual int compress_var::compress(outbuf& buf, outbuf& rbuf) = 0;
     \brief Compress data from the raw buffer. This is an abstract function to be overlaoded
            to implement the specific compression algorithm.
     \param [out]   buf     The buffer to store compressed data.
     \param [in]    rbuf    The raw data buffer.
     \returns   Size of the compress buffer that was used.
     */
    virtual int compress(outbuf& buf, outbuf& rbuf) = 0;
    /**
     \fn    virtual int compress_var::decompress(outbuf& dbuf, int len, const unsigned char* sbuf) = 0;
     \brief Decompresses the buffer passed in and writes to decompressed data buffer. This
            is an abstract function to be overloaded to implement the specific compression
            algorithm.
     \param [out]    dbuf    The decompressed data buffer to write to.
     \param          len     The length of the buffer passed in.
     \param          sbuf    The buffer, contains compressed data.
     \returns   The remaining bytes of the buffer that was left unused. If this equals
                len, it means that the decompression failed completely.
     */
    virtual int decompress(outbuf& dbuf, int len, const unsigned char* sbuf) = 0;

protected:
    bool dirty; /*!< Data in enc_buf is out of date as changes to children has taken place */
    unsigned int compression_level; /*!< The compression level */

    //compression buffer
    outbuf raw_buf; /*!< Buffer for raw data, ouput from children, before compression */
    outbuf enc_buf; /*!< Compressed data buffer, passed to parent to output */
    outbuf dec_buf; /*!< Decompressed data buffer, passed to children to extract */
};

#endif /* _COMPRESS_VAR_H_ */

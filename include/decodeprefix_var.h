/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _DECODEPREFIX_VAR_H_
#define _DECODEPREFIX_VAR_H_

#include <gen_var.h>

//! A decode variable that uses prefix code to reduce its size
/*!
    This decode data will produce between 1 to 9 bytes. It uses the
    prefix code to create a variable length byte representation. Smaller
    numbers will require less number of bytes.
 */
class RM_EXPORT vdecode_prefix_var : public generic_var<double>
{
public:
    vdecode_prefix_var(double _scale, double _offset);

    virtual int extract(int buflen, const unsigned char* buf);
    virtual void output(outbuf& strm);
    virtual int size() const;
    virtual mpt_var* getNext();

protected:
    /* double value must be offsetted, scaled, rounded and checked for limits */
    virtual int encodeValue(double, char*) const = 0;
    virtual int decodeValue(double&, const char*, int) const = 0;

private:
    explicit vdecode_prefix_var();

    double scaling;
    double offset;

};

class RM_EXPORT vdecode_prefix_unsigned_var : public vdecode_prefix_var
{
public:
    RUNTIME_TYPE(vdecode_prefix_unsigned_var)

    vdecode_prefix_unsigned_var(double _scale, double _offset = 0.0);
    virtual const mpt_var& operator=(const mpt_var& right);
    double operator=(double right);
    const vdecode_prefix_unsigned_var& operator=(const vdecode_prefix_unsigned_var& right);

protected:
    virtual int encodeValue(double, char*) const;
    virtual int decodeValue(double&, const char*, int) const;

};

class RM_EXPORT vdecode_prefix_signed_var : public vdecode_prefix_var
{
public:
    RUNTIME_TYPE(vdecode_prefix_signed_var)

    vdecode_prefix_signed_var(double _scale, double _offset = 0.0);
    const mpt_var& operator=(const mpt_var& right);
    double operator=(double right);
    const vdecode_prefix_signed_var& operator=(const vdecode_prefix_signed_var& right);

protected:
    virtual int encodeValue(double, char*) const;
    virtual int decodeValue(double&, const char*, int) const;

};

template<typename C>
class templ_decode_prefix_unsigned_var : public vdecode_prefix_unsigned_var
{
public:
    templ_decode_prefix_unsigned_var() : vdecode_prefix_unsigned_var(C::SCALE_FACTOR, C::OFFSET)
    {
    }
    const mpt_var& operator=(const mpt_var& right)
    {
        return vdecode_prefix_unsigned_var::operator=(right);
    }
    double operator=(double right)
    {
        return vdecode_prefix_unsigned_var::operator=(right);
    }
    const templ_decode_prefix_unsigned_var<C>& operator=(const templ_decode_prefix_unsigned_var<C>& right)
    {
        vdecode_prefix_unsigned_var::operator=(right);
        return *this;
    }
};

template<typename C>
class templ_decode_prefix_signed_var : public vdecode_prefix_signed_var
{
public:
    templ_decode_prefix_signed_var() : vdecode_prefix_signed_var(C::SCALE_FACTOR, C::OFFSET)
    {
    }
    const mpt_var& operator=(const mpt_var& right)
    {
        return vdecode_prefix_signed_var::operator=(right);
    }
    double operator=(double right)
    {
        return vdecode_prefix_signed_var::operator=(right);
    }
    const templ_decode_prefix_signed_var<C>& operator=(const templ_decode_prefix_signed_var<C>& right)
    {
        vdecode_prefix_signed_var::operator=(right);
        return *this;
    }
};

#define DEFINE_DECODE_PREFIX_UNSIGNED_VAR(clsnm, scale, offset) \
    class clsnm : public vdecode_prefix_unsigned_var \
    { \
public: \
        clsnm() : vdecode_prefix_unsigned_var(scale, offset) {} \
        mpt_var* getNext() { return this + 1; } \
        const mpt_var& operator=(const mpt_var& right) { \
            return vdecode_prefix_unsigned_var::operator=(right); \
        } \
        double operator=(double right) { \
            return vdecode_prefix_unsigned_var::operator=(right); \
        } \
        const clsnm& operator=(const clsnm& right) { \
            vdecode_prefix_unsigned_var::operator=(right); \
            return *this; \
        } \
    };

#define DEFINE_DECODE_PREFIX_SIGNED_VAR(clsnm, scale, offset) \
    class clsnm : public vdecode_prefix_signed_var \
    { \
public: \
        clsnm() : vdecode_prefix_signed_var(scale, offset) {} \
        mpt_var* getNext() { return this + 1; } \
        const mpt_var& operator=(const mpt_var& right) { \
            return vdecode_prefix_signed_var::operator=(right); \
        } \
        double operator=(double right) { \
            return vdecode_prefix_signed_var::operator=(right); \
        } \
        const clsnm& operator=(const clsnm& right) { \
            vdecode_prefix_signed_var::operator=(right); \
            return *this; \
        } \
    };

#endif /* _DECODEPREFIX_VAR_H_ */

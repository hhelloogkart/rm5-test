/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _RM_VAR_H_
#define _RM_VAR_H_

#include <cplx_var.h>

extern "C" RM_EXPORT void rm_minisprintf(char* buf, const char* fmt, ...);

class rm_var;
class RMBaseSocket;
class rm_interface;
class cache_typ;
class defaults;

//! Interface to the RM. This class represents a RM message, and its contained objects describe its data structure.
class RM_EXPORT rm_var : public complex_var
{
public:
    friend class rm_interface;

    rm_var(const char nm[], bool _regflag = false);

    // Naming Commands
    void        setName(const char _name[]);
    const char* getName(void) const;
    bool        chkName (const char _name[]) const;

    // ID Commands
    unsigned long getID() const;
    void setID(unsigned long _id);

    // Register Flag Commands (Note: use registerRM, and unregisterRM to set)
    bool getRegister() const;
    void setRegister(bool _regflag);

    // RMSocket Commands
    bool queryRM();
    bool registerRM();
    bool unregisterRM();
    void refreshRM();
    RMBaseSocket& Socket() const;

    // Overloaded Virtual Functions
    virtual void setRMDirty();
    virtual bool isRMDirty() const;
    virtual istream& operator>>(istream& s);
    virtual ostream& operator<<(ostream& s) const;
    virtual bool write(outbuf& buf);
    virtual cache_typ* writebuffer(unsigned int id, size_t len);

    // Static calls
    static void init(defaults*);
    static void outgoing();
    static void incoming();
    static void close();
    static void deinit();
    static bool check_connect();
    static rm_var* findVariable(const char _name[]);

private:
    rm_var();

protected:
    bool regflag;
    bool dirty;
    int id;
    char rmmsgname[RMNAME_LEN];
public:
    static void (* connectCB)();
};

// ID Commands
inline unsigned long rm_var::getID() const
{
    return id;
}

inline void
rm_var::setID(unsigned long _id)
{
    id = _id;
}

// Name Commands
inline const char*
rm_var::getName(void) const
{
    return rmmsgname;
}

inline bool
rm_var::getRegister() const
{
    return regflag;
}

inline void rm_var::setRegister(bool _regflag)
{
    regflag = _regflag;
}

#define STD_RMVAR_METHODS(t) \
const t& operator=(const t& right) \
{ \
    complex_var::operator=(right); \
    return *this; \
} \
const mpt_var& operator=(const mpt_var& right) \
{ \
    complex_var::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)

#define RMCONST_1(t,mode) t(const char* nm) : rm_var(nm, mode) \
{ \
    pop(); \
} \
STD_RMVAR_METHODS(t)

#define RMCONST(t,nm,mode) t() : rm_var(nm, mode) \
{ \
    pop(); \
} \
t(mpt_var &link) : rm_var(nm, mode) \
{ \
    pop(); \
    *((*this)[0]) = link; \
} \
STD_RMVAR_METHODS(t)

#define RMCONST_ARRAY(t,nm,mode)  t() : rm_var("", mode) \
{ \
    pop(); \
    rm_minisprintf(rmmsgname, nm, classIndex()+1); \
} \
t(mpt_var &link) : rm_var("", mode) \
{ \
    pop(); \
    rm_minisprintf(rmmsgname, nm, classIndex()+1); \
    *((*this)[0]) = link; \
} \
STD_RMVAR_METHODS(t)

#define RMCONST_ARRAY2(t,nm,mode,div) t() : rm_var("", mode) \
{ \
    pop(); \
    int cli = classIndex(); \
    rm_minisprintf(rmmsgname, nm, (cli / div) + 1, (cli % div) + 1); \
} \
t(mpt_var &link) : rm_var("", mode) \
{ \
    pop(); \
    int cli = classIndex(); \
    rm_minisprintf(rmmsgname, nm, (cli / div) + 1, (cli % div) + 1); \
    *((*this)[0]) = link; \
} \
STD_RMVAR_METHODS(t)

#define CONSTRUCTN(t,nm,mode) t() : rm_var(nm, mode) \
{ \
    pop(); \
} \
STD_RMVAR_METHODS(t)

#define CONSTRUCTNA(t,nm,mode)  t() : rm_var("", mode) \
{ \
    pop(); \
    idx = classIndex(); \
    rm_minisprintf(rmmsgname, nm, idx+1); \
} \
STD_RMVAR_METHODS(t)

#define IA(type,name,rmname,regflag,num) inline type* Instance_ ## type() { char nm[32]; rm_minisprintf(nm, #rmname, 1); rm_var* var = rm_var::findVariable(nm), * var1 = var; if (var && regflag) for (int cnt=0; cnt<num; ++cnt,var1=static_cast<rm_var*>(var1->getNext())) var1->setRegister(true); return (var != NULL) ? static_cast<type*>(var) : new type[num]; } \
type* name = Instance_ ## type();

#define I(type,name,rmname,regflag) inline type& Instance_ ## type() { rm_var* var = rm_var::findVariable(#rmname); if (var && regflag) var->setRegister(true); return (var != NULL) ? static_cast<type&>(*var) : *(new type); } \
type& name = Instance_ ## type();

#endif /* _RM_VAR_H_ */

/******************************************************************/
/* Copyright DSO National Laboratories 2008. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _MUT_VAR_H_
#define _MUT_VAR_H_

#include <cplx_var.h>
#ifndef PROTOBUF_USE_DLLS
#define PROTOBUF_USE_DLLS
#endif

namespace google
{
namespace protobuf
{
class Message;
}
}

//! A Composite class that is able to work even when sender and receiver have different children
class RM_EXPORT mutable_var : public complex_var
{
public:
    mutable_var(google::protobuf::Message*);
    ~mutable_var();
    virtual int extract(int len, const unsigned char* buf);
    virtual int size() const;
    virtual void output(outbuf& strm);
    virtual void setRMDirty();
    virtual const mpt_var& operator=(const mpt_var& right);
    virtual bool operator==(const mpt_var& right) const;
    const complex_var& operator=(const mutable_var& right);
    bool operator==(const mutable_var& right) const;
    void to_message(google::protobuf::Message& mesg) const;
    void from_message(const google::protobuf::Message& mesg);

protected:
    google::protobuf::Message* message;
    bool in_extract;

private:
    mutable_var();
};

#define MUTABLECONST(t) t(const t& arg) \
    : mutable_var(new t ## _proto) \
{ \
    child_copy_constructor(arg); \
} \
t() \
    : mutable_var(new t ## _proto) \
{ \
    pop(); \
} \
const t& operator=(const t& right) \
{ \
    mutable_var::operator=(right); \
    return *this; \
} \
const mpt_var& operator=(const mpt_var& right) \
{ \
    mutable_var::operator=(right); \
    return *this; \
} \
virtual mpt_var* getNext() \
{ \
    return this + 1; \
} \
RUNTIME_TYPE(t)

#endif /* _MUT_VAR_H_ */

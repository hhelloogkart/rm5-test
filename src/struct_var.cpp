/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include <struct_var.h>
#include <string.h>
#include <iomanip>

struct_var::struct_var(void* ptr) : my_struct(ptr)
{
}

bool struct_var::isValid() const
{
    const char* my_buf = static_cast<const char*>(my_struct);
    int sz = size();
    for (int cnt=0; cnt < sz; ++cnt)
    {
        if (my_buf[cnt] != '\xFF')
        {
            return true;
        }
    }
    return false;
}

bool struct_var::setInvalid()
{
    const char neg = ~0;
    const int sz = size();
    char* const buf = static_cast<char*>(my_struct);
    int i;
    for (i = 0; i < sz; ++i)
    {
        if (buf[i] != neg)
        {
            goto fill;
        }
    }
    notDirty();
    return false;
    for (; i < sz; ++i)
    {
fill:
        buf[i] = neg;
    }
    setDirty();
    return true;
}

mpt_var* struct_var::getNext()
{
    return this + 1;
}

const struct_var& struct_var::operator=(const struct_var& right)
{
    memcpy(my_struct, right.my_struct, size());
    return *this;
}

bool struct_var::operator==(const mpt_var& right) const
{
    const struct_var* right1 = RECAST(const struct_var*, &right);
    return (right1) ? memcmp(my_struct, right1->my_struct, size()) == 0 : mpt_var::operator==(right);
}

istream& struct_var::operator>>(istream& s)
{
    char* my_buf = static_cast<char*>(my_struct);
    unsigned int buf;
    int sz = size();
    for (int cnt=0; cnt < sz; ++cnt)
    {
        s >> std::hex >> buf;
        my_buf[cnt] = buf;
    }
    setDirty();
    return s;
}

ostream& struct_var::operator<<(ostream& s) const
{
    const char* my_buf = static_cast<const char*>(my_struct);
    int sz = size();
    for (int cnt=0; cnt < sz; ++cnt)
    {
        if (cnt != 0)
        {
            s << ' ';
        }
        s << std::hex << std::setw(2) << (unsigned int)my_buf[cnt];
    }
    return s;
}

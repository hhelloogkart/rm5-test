#include "chksum_var.h"
#include <outbuf.h>
#include <assert.h>
#include <stdio.h>

static void make_chksum8(int len, const unsigned char* buf, outbuf& strm)
{
    unsigned char cs = 0;
    for (; len > 0; --len, ++buf)
    {
        cs += *buf;
    }
    strm += cs;
}

template<class T, bool _BE>
inline void make_chksum_x(int len, const unsigned char* buf, outbuf& strm)
{
    int sz = sizeof(T);
    T cs = 0;
    unsigned char* p = reinterpret_cast<unsigned char*>(&cs);

    for (; len > 0; --len, ++buf)
    {
        cs += *buf;
    }
    cs = _BE ? rm_BE(cs) : rm_LE(cs);
    for (; sz > 0; ++p, --sz)
    {
        strm += *p;
    }
}

static void make_chksum8_xsens(int len, const unsigned char* buf, outbuf& strm)
{
    unsigned char cs = 0;
    for (; len > 0; --len, ++buf)
    {
        cs -= *buf;
    }
    strm += cs;
}

static void make_chksum8_xor(int len, const unsigned char* buf, outbuf& strm)
{
    unsigned char cs = 0;
    for (; len > 0; --len, ++buf)
    {
        cs ^= *buf;
    }
    strm += cs;
}

static void make_chksum8_xor_nmea(int len, const unsigned char* buf, outbuf& strm)
{
    unsigned char cs = 0;
    char chk[3];
    for (; (len > 0) && (*buf != '*'); --len, ++buf)
    {
        cs ^= *buf;
    }
    rm_sprintf(chk, 3, "%02X", cs);
    strm += '*';
    strm += chk[0];
    strm += chk[1];
}

static void make_chksum8_two_comp(int len, const unsigned char* buf, outbuf& strm)
{
    unsigned char cs = 0;

    for (; len > 0; --len, ++buf)
    {
        cs += *buf;
    }

    strm += (~cs + 1);
}

static void make_chksum8_comp(int len, const unsigned char* buf, outbuf& strm)
{
    unsigned char cs = 0;

    for (; len > 0; --len, ++buf)
    {
        cs += *buf;
    }

    strm += (~cs);
}

static void make_chksum8_sum_int(int len, const unsigned char* buf, outbuf& strm)
{
    union
    {
        unsigned char c[4];
        unsigned int ui;
    } cs;

    cs.ui = 0;
    for (; len > 0; --len, ++buf)
    {
        cs.ui += *buf;
    }
    cs.ui = (cs.c[0] + cs.c[1] + cs.c[2] + cs.c[3]);
    strm += static_cast<unsigned char>(cs.ui);
}

static void make_chksum16_ig500(int len, const unsigned char* buf, outbuf& strm)
{
    const unsigned short poly = 0x8408;
    unsigned short crc = 0;
    unsigned char carry = 0;
    unsigned char i_bits = 0;
    unsigned short j = 0;

    for (j = 0; j<len; j++)
    {
        crc = crc^buf[j];
        for (i_bits = 0; i_bits<8; i_bits++)
        {
            carry = crc & 1;
            crc = crc / 2;
            if (carry)
            {
                crc = crc^poly;
            }
        }
    }
    strm += static_cast<char>(crc >> 8);
    strm += static_cast<char>(crc & 0xFF);
}

static void make_chksum16_1553b(int len, const unsigned char* buf, outbuf& strm)
{
    unsigned int cs = 0;
    unsigned short result = 0;

    while (len >  0)
    {
        cs += *(buf++);
        if (len != 1)
        {
            cs += *(buf++) << 8;
        }
        len -= 2;
    }

    result += cs;
    result += cs >> 16;
    strm += static_cast<char>(result & 0xFF);
    strm += static_cast<char>(result >> 8);
}

static void make_chksum16_fas(int len, const unsigned char* rbuf, outbuf& strm)
{
    unsigned char value1 = 0;
    unsigned char value2 = 0;

    for (int i = 0; i < len; ++i, ++rbuf)
    {
        value1 += *rbuf;
        if ((i & 0x01) == 0) //even bytes
        {
            value2 += *rbuf;
        }
    }

    value1 = static_cast<unsigned char>(-value1);
    value2 = static_cast<unsigned char>(-value2);
    strm += value1;
    strm += value2;
}

static void make_chksum16_xor_fog(int len, const unsigned char* rbuf, outbuf& strm)
{
    unsigned char value1 = 0;
    unsigned char value2 = 0;
    len /= 2;
    const unsigned short* buf = reinterpret_cast<const unsigned short*>(rbuf);

    while (len)
    {
        value1 = value1 ^ (*buf & 0xFF);
        value2 = value2 ^ value1;
        value1 = value1 ^ (((*buf) & 0xFF00) >> 8);
        value2 = value2 ^ value1;

        len--;
        buf++;    //move to next unsigned short int
    }//end while

    strm += value1;
    strm += value2;
}

static void make_chksum16b_xor_fog(int len, const unsigned char* rbuf, outbuf& strm)
{
    unsigned char value1 = 0;
    unsigned char value2 = 0;
    len /= 2;
    const unsigned short* buf = reinterpret_cast<const unsigned short*>(rbuf);

    while (len)
    {
        value1 = value1 ^ (*buf & 0xFF);
        value2 = value2 ^ value1;
        value1 = value1 ^ (((*buf) & 0xFF00) >> 8);
        value2 = value2 ^ value1;

        len--;
        buf++;     //move to next unsigned short int
    }//end while

    strm += value2;
    strm += value1;
}

static void make_chksum16_ublox(int len, const unsigned char* buf, outbuf& strm)
{
    unsigned char csa = 0;
    unsigned char csb = 0;

    for (; len > 0; --len, ++buf)
    {
        csa += *buf;
        csb += csa;
    }
    strm += csa;
    strm += csb;
}

template<unsigned short seed>
static void make_chksum16_ccitt(int len, const unsigned char* buf, outbuf& strm)
{
    static unsigned short ccitt_table[256];
    static bool ccitt_table_init = false;

    if (!ccitt_table_init)
    {
        int i, j;
        unsigned short crc, c;
        for (i = 0; i < 256; ++i)
        {
            crc = 0;
            c = static_cast<unsigned short>(i << 8);

            for (j = 0; j < 8; ++j)
            {
                if ((crc ^c) & 0x8000)
                {
                    crc = (crc << 1) ^ 0x1021;
                }
                else
                {
                    crc = crc << 1;
                }
                c = c << 1;
            }

            ccitt_table[i] = crc;
        }
        ccitt_table_init = true;
    }

    unsigned short temp, short_c;
    unsigned short crc = seed;

    for (int i = 0; i < len; ++i)
    {
        short_c = static_cast<unsigned short>(0x00ff & (*(buf + i)));
        temp = (crc >> 8) ^ short_c;
        crc = (crc << 8) ^ ccitt_table[temp];
    }

    strm += static_cast<char>(crc & 0xFF);
    strm += static_cast<char>(crc >> 8);
}

typedef void (* mkchksum_func)(int, const unsigned char*, outbuf&);
struct mkchksum_item
{
    mkchksum_func f;
    int size;
};

static mkchksum_item funclist[] = {
    { &make_chksum8, 1 },
    { &make_chksum_x<unsigned short,false>, 2 },
    { &make_chksum_x<unsigned short,true>, 2 },
    { &make_chksum_x<unsigned int,false>, 4 },
    { &make_chksum_x<unsigned int,true>, 4 },
    { &make_chksum8_xsens, 1 },
    { &make_chksum8_xor, 1 },
    { &make_chksum8_xor_nmea, 3 },
    { &make_chksum8_two_comp, 1 },
    { &make_chksum8_comp, 1 },
    { &make_chksum8_sum_int, 1 },
    { &make_chksum16_ig500, 2 },
    { &make_chksum16_1553b, 2 },
    { &make_chksum16_fas, 2 },
    { &make_chksum16_xor_fog, 2 },
    { &make_chksum16b_xor_fog, 2 },
    { &make_chksum16_ublox, 2 },
    { &make_chksum16_ccitt<0>, 2 },
    { &make_chksum16_ccitt<0xFFFF>, 2 }
};

chksum_var::chksum_var(int _hash, unsigned char chk_loc, unsigned char front_off, unsigned char back_off) : chk_var(chk_loc, front_off, back_off), hashtype(_hash)
{
    assert(_hash < (sizeof(funclist) / sizeof(mkchksum_item)));
}

chksum_var::chksum_var(const chksum_var& right) : chk_var(right), hashtype(right.hashtype)
{
}

const chksum_var& chksum_var::operator=(const chksum_var& right)
{
    chk_var::operator=(static_cast<const chk_var&>(right));
    return *this;
}

const mpt_var& chksum_var::operator=(const mpt_var& right)
{
    return chk_var::operator=(right);
}

int chksum_var::checksum_size() const
{
    return funclist[hashtype].size;
}

void chksum_var::make_chksum(int len, const unsigned char* buf, outbuf& strm)
{
    funclist[hashtype].f(len, buf, strm);
}

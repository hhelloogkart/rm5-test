/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "rmstl.h"
#include "mpt_show.h"
#include "mpt_var.h"

typedef LIST(mpt_baseshow*) showlist_typ;
typedef LIST(mpt_var*) showlist_var_typ;

struct mpt_autoshow_private
{
    showlist_var_typ varlist;
};

static showlist_typ& showlist()
{
    static showlist_typ shl;
    return shl;
}

static bool reentrant = false;
static showlist_typ holding_add;
static showlist_typ holding_del;

mpt_baseshow::mpt_baseshow()
{
    if (!reentrant)
    {
        showlist().push_back(this);
    }
    else
    {
        holding_add.push_back(this);
    }
}

mpt_baseshow::~mpt_baseshow()
{
    if (!reentrant)
    {
        showlist().remove(this);
    }
    else
    {
        holding_del.push_back(this);
    }
}

void mpt_baseshow::setDirty(mpt_var*)
{
}

void mpt_baseshow::notDirty(mpt_var*)
{
}

void mpt_baseshow::operator()()
{
}

void mpt_baseshow::refresh()
{
    if (!reentrant)
    {
        reentrant = true;
        showlist_typ::iterator p;
        for (p = showlist().begin(); p != showlist().end(); ++p)
        {
            (*(*p))();
        }
        reentrant = false;
        for (p = holding_del.begin(); p != holding_del.end(); ++p)
        {
            showlist().remove(*p);
        }
        showlist().splice(showlist().end(), holding_add);
    }
}

mpt_show::mpt_show() : dirty(false)
{
}

void mpt_show::setDirty(mpt_var*)
{
    dirty = true;
}

mpt_autoshow::mpt_autoshow() :
    prvt(new mpt_autoshow_private)
{
}

mpt_autoshow::mpt_autoshow(const mpt_autoshow& other) :
    mpt_show(), prvt(new mpt_autoshow_private)
{
    operator=( other );
}

mpt_autoshow& mpt_autoshow::operator=(const mpt_autoshow& other)
{
    showlist_var_typ::iterator iter;

    // Detach from old
    for (iter = prvt->varlist.begin(); iter != prvt->varlist.end(); ++iter)
    {
        (*iter)->removeCallback(this);
    }

    prvt->varlist = other.prvt->varlist;

    // Attach to new
    for (iter = prvt->varlist.begin(); iter != prvt->varlist.end(); ++iter)
    {
        (*iter)->addCallback(this);
    }

    return *this;
}

mpt_autoshow::~mpt_autoshow()
{
    showlist_var_typ::iterator iter;

    for (iter = prvt->varlist.begin(); iter != prvt->varlist.end(); ++iter)
    {
        (*iter)->removeCallback(this);
    }
    delete prvt;
}

void mpt_autoshow::addCallback(mpt_var* _var)
{
    _var->addCallback(this);
    prvt->varlist.push_back(_var);
}

void mpt_autoshow::removeCallback(mpt_var* _var)
{
    _var->removeCallback(this);
    prvt->varlist.remove(_var);
}

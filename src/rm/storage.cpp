/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "storage.h"
#include <assert.h>
#include <mm/event.h>

rmglobal_typ::rmglobal_typ(const char* name)
{
    static char const defaultname[] = "RESMGR4";
    unsigned int cnt;
    cache = var = NULL;
    for (cnt = 0; cnt < MAX_CLIENT; ++cnt)
    {
        queue[cnt] = NULL;
        queue_event[cnt] = NULL;
    }

    global_name = (name == NULL) ? defaultname : name;
    ptr = (rmglobal_private_typ*)mm_core_create(sizeof(rmglobal_private_typ), global_name);
    if (ptr && (ptr->magic != MAGICNUM))
    {
        // preinitialisation
        ptr->magic = MAGICNUM;
        new (&(ptr->store.cache)) rmname_typ;
        new (&(ptr->store.var)) rmname_typ;
        for (cnt = 0; cnt < MAX_CLIENT; ++cnt)
        {
            new (&(ptr->store.queue[cnt])) rmname_typ;
        }
    }
}

rmglobal_typ::~rmglobal_typ()
{
    for (int cnt = 0; cnt < (int)MAX_CLIENT; ++cnt)
    {
        delete queue_event[cnt];
    }
}

void rmglobal_typ::detach()
{
    if (ptr == NULL)
    {
        return;
    }
    lock();
    if (cache)
    {
        mm_core_delete(cache);
        cache = NULL;
    }
    unsigned int cnt;
    for (cnt = 0; cnt < MAX_CLIENT; ++cnt)
    {
        if (queue[cnt])
        {
            mm_core_delete(queue[cnt]);
            queue[cnt] = NULL;
        }
    }
    if (var)
    {
        mm_core_delete(var);
        var = NULL;
    }
    unlock();
    mm_core_delete(ptr);
}

void rmglobal_typ::set_name(rmname_typ& nm, char ch)
{
    char str[2];
    str[0] = ch;
    str[1] = '\0';
    lock();
    nm = global_name;
    nm += str;
    unlock();
}

void rmglobal_typ::set_name(rmname_typ& nm, char ch, int num)
{
    char str[4];
    str[0] = ch;
    str[1] = static_cast<char>(num / 10) + '0';
    str[2] = static_cast<char>(num % 10) + '0';
    str[3] = '\0';
    lock();
    nm = global_name;
    nm += str;
    unlock();
}

MM* rmglobal_typ::attach_cache(unsigned int size)
{
    // check if global SM mounted
    if (!ptr)
    {
        return NULL;
    }

    // check if area attached
    if (cache != NULL)
    {
        return cache;
    }

    // check if area allocated
    lock();
    if (ptr->store.cache.name[0] != '\0')
    {
        cache = (MM*) mm_core_create(size ? size : mm_maxsize(), ptr->store.cache);
        unlock();
        cache_sharedmemory_segment::Ptr = cache;
        return cache;
    }
    unlock();

    // allocate new area
    set_name(ptr->store.cache, 'C');
    lock();
    cache = (MM*) mm_create(size ? size : mm_maxsize(), ptr->store.cache);
    unlock();
    cache_sharedmemory_segment::Ptr = cache;

    return cache;
}

void rmglobal_typ::attach_queue(unsigned int size)
{
    // check if global SM mounted
    if (!ptr)
    {
        return;
    }

    unsigned int cnt;
    for (cnt = 0; cnt < MAX_CLIENT; ++cnt)
    {
        // check if area attached
        if (queue[cnt] != NULL)
        {
            continue;
        }

        // check if area allocated
        lock();
        if (ptr->store.queue[cnt].name[0] != '\0')
        {
            queue[cnt] = (MM*) mm_core_create(size ? size : mm_maxsize(), ptr->store.queue[cnt]);
            unlock();
        }
        else
        {
            unlock();

            // allocate new area
            set_name(ptr->store.queue[cnt], 'Q', cnt);
            lock();
            queue[cnt] = (MM*) mm_core_create(size ? size : mm_maxsize(), ptr->store.queue[cnt]);
            memset(queue[cnt], 0, mm_core_size(queue[cnt]));
            unlock();
        }

        // create event
        rmname_typ evtname;
        set_name(evtname, 'E', cnt);
        queue_event[cnt] = new osevent(evtname.name);
        queue_event[cnt]->create(true);
    }
}

MM* rmglobal_typ::attach_var(unsigned int size)
{
    // check if global SM mounted
    if (!ptr)
    {
        return NULL;
    }

    // check if area attached
    if (var != NULL)
    {
        return var;
    }

    // check if area allocated
    lock();
    if (ptr->store.var.name[0] != '\0')
    {
        var = (MM*) mm_core_create(size ? size : mm_maxsize(), ptr->store.var);
        unlock();
        var_sharedmemory_segment::Ptr = var;
        return var;
    }
    unlock();

    // allocate new area
    set_name(ptr->store.var, 'V');
    lock();
    var = (MM*) mm_create(size ? size : mm_maxsize(), ptr->store.var);
    unlock();
    var_sharedmemory_segment::Ptr = var;
    new (&(ptr->store.directory)) directory_typ;
    new (&(ptr->store.namedir)) namedir_typ;
    return var;
}

void rmglobal_typ::rdlock()
{
    for (int cnt = 0; cnt < 1000; ++cnt)
    {
        if ((mm_lock(ptr, MM_LOCK_RD) != 0) || (ptr == 0))
        {
            return;
        }
    }
    assert(0); // forces program to exit (dead-lock condition)
}

void rmglobal_typ::lock()
{
    for (int cnt = 0; cnt < 1000; ++cnt)
    {
        if ((mm_lock(ptr, MM_LOCK_RW) != 0) || (ptr == 0))
        {
            return;
        }
    }
    assert(0); // forces program to exit (dead-lock condition)
}

void rmglobal_typ::unlock()
{
    mm_unlock(ptr);
}

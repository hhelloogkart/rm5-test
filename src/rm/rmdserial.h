/******************************************************************/
/* Copyright DSO National Laboratories 2005. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _RMDSERIAL_H_
#define _RMDSERIAL_H_

#include "rmsocket.h"

struct rmdserial_socket_private;

//! Class for Dual Serial connection with RM
class RM_EXPORT RMDSerialSocket : public RMBaseSocket
{
public:

    RMDSerialSocket();
    virtual ~RMDSerialSocket();
    //////////////////////////////////////////////////////
    // Connection and status
    //////////////////////////////////////////////////////
    virtual bool Connect(const char* host, int port, const struct timeval* stm = 0);
    virtual bool Connected();
    virtual bool Close(); //this function closes 2 connections
    virtual int  Select(const struct timeval* stm = 0, bool defercb = false);
    //////////////////////////////////////////////////////
    // RM Protocol Calls for Clients
    //////////////////////////////////////////////////////
    virtual const cache_typ* GetListStart(unsigned int id);
    virtual void GetListEnd(unsigned int id, const cache_typ* pbuf); //1 is enough
    virtual cache_typ* SetListStart(unsigned int id, size_t len);   //1 set list is enough
    virtual cache_typ* MessageStart(unsigned int id, size_t len);
    virtual bool SetListEnd(unsigned int id, cache_typ* pbuf);
    virtual bool BroadcastEnd(unsigned int id, cache_typ* pbuf);
    virtual bool MessageEnd(unsigned int id, cache_typ* pbuf, void* addr=0);
    //////////////////////////////////////////////////////
    virtual bool FlowControl(unsigned int interval);
    virtual void KeepAlive(); //only 1 to keep alive

    //////////////////////////////////////////////////////
    //get socket handlers of connections - necessary to separate them out
    //////////////////////////////////////////////////////
    virtual int  GetHandle() const; //returns the 1st valid handle
    virtual bool CheckLink(); //returns false if all links fail and true if otherwise
    //////////////////////////////////////////////////////
    // Depreciated and Inefficient RM Protocol Calls
    //////////////////////////////////////////////////////
    virtual bool Broadcast(unsigned int id, size_t len, const unsigned int* buf);
    virtual bool SetList(unsigned int id, size_t len, const unsigned int* buf);
    virtual bool Message(unsigned int id, size_t len, const unsigned int* buf);
    virtual size_t GetList(unsigned int id, size_t len, unsigned int* buf);
    //////////////////////////////////////////////////////
    bool SendMessageT(unsigned int id, int cmd);
    //////////////////////////////////////////////////////
    virtual bool RegisterIDX(unsigned int);
    virtual bool UnregisterIDX(unsigned int);
    virtual bool RegisterID(const char*);
    virtual bool QueryID(const char*);
    virtual bool QueryID(const char*[], int);
    virtual bool ClientID(const char*);
    virtual bool WaitForHandshakeCompletion(const struct timeval*);
    virtual bool HandshakeAck(unsigned int cmd, unsigned int id);
    virtual bool Flush();

protected:
    void ReceivePacket(int sock_id, bool defercb);
    void EnterResetState(bool burstout);
    void CheckThroughput(bool flag);
    void setResetInterval(long sec, long microsec);
    void setResendInterval(long sec, long microsec);
    void setClearOutputBufferInterval(long sec, long microsec);
    rmdserial_socket_private* prvt;
};
#endif

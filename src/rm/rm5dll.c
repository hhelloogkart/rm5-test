//////////////////////////////////////////////////////////////////
//
// rm5.c - C file
//
// This file was generated using the RTX Application Wizard
// for .NET.
//////////////////////////////////////////////////////////////////

#include <winsock2.h>
#include <ws2tcpip.h>
#include <rtapi.h>
#include <stdio.h>
#include <rm_ver.h>

//
// MAIN - just suspend the main thread for this process until its
// use as a DLL is finished and the process is terminated.
//
int main(int argc, char** argv)
{
    printf("RM Lib %s (%s) - Compiled %s %s by %s\n", COMPILE_VER, COMPILE_OSNAME, COMPILE_DATE, COMPILE_TIME, COMPILE_BY);
    SuspendThread( GetCurrentThread());
    return 0;
}

/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "rmtcp.h"
#include "roamcli.h"
#include <limits.h>
#include <string.h>
#include <comm/tcpcore.h>
#include <time/timecore.h>

#define NO_THREAD

#ifndef QUEUE_TYP_DEFINED
#define QUEUE_TYP_DEFINED
struct RM_EXPORT queue_typ
{
    cache_typ* ptr;
    queue_typ* next;
};
#endif

#ifdef VXWORKS
#include <sys/socket.h>
#endif

#ifdef NET_OS
#include <threadx/tx_api.h>
#include <threadx/tx_tim.h>
#include <bsp_api.h>
#endif

#ifndef NO_THREAD
#include <task/threadt.h>
#include <util/proccore.h>

bool rcv_function(void* cl)
{
    return ((RMTCPSocket*)cl)->recv_th_function();
}
#endif

//copy and null terminate up to RMNAME_LEN
static void rmnamecpy(char* dst, const char* src);

struct RMTCPSocket_private
{
    RMTCPSocket_private()
    {
    }
    RMTCPSocket_private(const tcp_core&);
    tcp_core sock;
    unsigned int getid;
    unsigned int queryid;
    unsigned int rcvqueryid;
    unsigned int errflag;
    unsigned int valid;
    unsigned int in_seq;
    unsigned int out_seq;
    unsigned int intv;
    cache_typ* query_buf;
    cache_typ* out_buf;
    cache_typ* small_buf;
    unsigned int* in_buf;
    queue_typ* queue_start;
    queue_typ* queue_end;
    timeElapsed recvtime;
    timeElapsed sendtime;
    unsigned int raw_inbuf[READ_BUF+HEADER_OFF+1];
    bool readflag;
    bool clientok;
    RMRoamClient* roam;
#ifndef NO_THREAD
    bool deferred_close;
    thread_typ* thd;
    bool enable_recv_th;
    long receive_mutex;
#endif
};

RMTCPSocket_private::RMTCPSocket_private(const tcp_core& c) : sock(c)
{
}

RMTCPSocket::RMTCPSocket() : prvt(new RMTCPSocket_private)
{
    prvt->getid=UINT_MAX;
    prvt->queryid=UINT_MAX;
    prvt->rcvqueryid=UINT_MAX;
    prvt->errflag=0;
    prvt->valid=0;
    prvt->in_seq=0;
    prvt->out_seq=0;
    prvt->intv=0;
    prvt->query_buf=0;
    prvt->out_buf=cache_typ::h_allocate(WRITE_BUF);
    prvt->small_buf=cache_typ::h_allocate(RMNAME_LEN / sizeof(unsigned));
    prvt->in_buf=prvt->raw_inbuf + HEADER_OFF;
    prvt->queue_start=0;
    prvt->queue_end=0;
    prvt->readflag=false;
    prvt->clientok=false;
    prvt->roam=0;
#ifndef NO_THREAD
    prvt->deferred_close=false;
    prvt->thd=0;
    prvt->enable_recv_th=false;
    prvt->receive_mutex=0;
#endif
}

RMTCPSocket::RMTCPSocket(const tcp_core& asock) : prvt(new RMTCPSocket_private(asock))
{
    prvt->getid=UINT_MAX;
    prvt->queryid=UINT_MAX;
    prvt->rcvqueryid=UINT_MAX;
    prvt->errflag=0;
    prvt->valid=0;
    prvt->in_seq=0;
    prvt->out_seq=0;
    prvt->intv=0;
    prvt->query_buf=0;
    prvt->out_buf=cache_typ::h_allocate(WRITE_BUF);
    prvt->small_buf=cache_typ::h_allocate(RMNAME_LEN / sizeof(unsigned));
    prvt->in_buf=prvt->raw_inbuf + HEADER_OFF;
    prvt->queue_start=0;
    prvt->queue_end=0;
    prvt->readflag=false;
    prvt->clientok=false;
    prvt->roam=0;
#ifndef NO_THREAD
    prvt->deferred_close=false;
    prvt->thd=0;
#endif

}

RMTCPSocket::~RMTCPSocket()
{
    prvt->out_buf->h_unreserve();
    prvt->small_buf->h_unreserve();
    prvt->sock.close();
    delete prvt->roam;
#ifndef NO_THREAD
    delete prvt->thd;
#endif
    delete prvt;
}

bool RMTCPSocket::Connect(const char* host, int port, const struct timeval* stm)
{
    int timeout = CONN_WAIT;
    struct timeval def = {
        10, 0
    };
    if(stm)
    {
        def = *stm;
        timeout = def.tv_sec;
        if(def.tv_usec >= 50000)
        {
            ++timeout;
        }
        if(!timeout)
        {
            ++timeout;
        }
    }
    //host = Auto:mask mask OR Auto
    if(!strncmp("Auto", host, 4))
    {
        if(!prvt->roam)
        {
            prvt->roam = new RMRoamClient(port, (host[4] == ':') ? &host[5] : (const char*)0);
        }
        if(prvt->roam->Get_Port() != port)
        {
            prvt->roam->Set_Port(port);
        }

        if(!prvt->roam->Select(&def))
        {
            return false;
        }
        prvt->roam->ExportAddr(&prvt->sock);
    }
    else
    {
        prvt->sock.sethost(host, port);
    }
    if (prvt->sock.connect(timeout) == 1)
    {
        prvt->recvtime();
        return true;
    }
    return false;
}

bool RMTCPSocket::Connected()
{
    return prvt->sock.connected();
}

bool RMTCPSocket::Close()
{
#ifndef NO_THREAD
    if (thd != 0)
    {
        deferred_close = true;
        return false;
    }
#endif
    prvt->getid      = UINT_MAX;
    prvt->queryid    = UINT_MAX;
    prvt->rcvqueryid = UINT_MAX;
    prvt->errflag    = 0;
    prvt->valid      = 0;
    prvt->in_seq     = 0;
    prvt->out_seq    = 0;
    prvt->query_buf  = 0;
    prvt->recvtime.clear();
    prvt->intv       = 0;
    prvt->sendtime.clear();
    prvt->readflag   = false;
    prvt->clientok   = false;
    if(prvt->roam)
    {
        prvt->roam->Close();
    }
    return prvt->sock.close();
}

int RMTCPSocket::Select(const struct timeval* stm, bool defercb)
{
#ifndef NO_THREAD
    if (deferred_close)
    {
        deferred_close = false;
        return -1; /* this causes a close attempt */
    }
#endif
    const cache_typ* p;
    unsigned int cmd;

    KeepAlive();
    if (!defercb)
    {
        while ((p = Top()) != 0)
        {
            cmd = p->get_cmd();
            if (cmd < ENDCMD)
            {
                (*(callback[cmd]))(callback_arg[cmd], p->get_id(), p);
            }
            Pop();
            const_cast<cache_typ*>(p)->h_unreserve();
        }
    }

#ifndef NO_THREAD
    while (atomic_tas(&receive_mutex) != 1)
    {
        context_yield();
    }
#endif

    int ret = (stm == 0) ? 1 : prvt->sock.select(stm);
    if (ret > 0)
    {
        ret = 1;
        // do some unpacking of packets
        ReceivePacket(defercb);
#ifndef NO_THREAD
        atomic_untas(&receive_mutex);
#endif
        if (prvt->errflag > ERR_TOLERANCE)
        {
            return -1;
        }
    }
    else if(ret < 0)
    {
#ifndef NO_THREAD
        atomic_untas(&receive_mutex);
#endif
        return -1;
    }
    else
    {
#ifndef NO_THREAD
        atomic_untas(&receive_mutex);
#endif
        if (!CheckLink())
        {
            return -1;
        }
    }
    KeepAlive();

    return ret;
}

const cache_typ* RMTCPSocket::GetListStart(unsigned int id)
{
    int cnt = 0, ret;
    SendMessageT(id & 0xFFFFFF, 0, 0, GETLIST);
    prvt->getid = id;
    do
    {
        if (cnt != 0)
        {
            Sleep(WAIT_TIME);
        }
        struct timeval tm = {
            0, WAIT_TIME*1000
        };
        ret = Select(&tm, true);
        ++cnt;
    }
    while ((prvt->query_buf == 0) && (cnt < GETLIST_RETRY) && (ret >= 0));
    cache_typ* ptr = prvt->query_buf;
    prvt->getid = UINT_MAX;
    prvt->query_buf = 0;
    return ptr;
}

void RMTCPSocket::GetListEnd(unsigned int, const cache_typ* pbuf)
{
    const_cast<cache_typ*>(pbuf)->h_unreserve();
}

cache_typ* RMTCPSocket::MessageStart(unsigned int id, size_t len)
{
    return SetListStart(id, len);
}

cache_typ* RMTCPSocket::SetListStart(unsigned int, size_t len)
{
    if (len <= WRITE_BUF && prvt->out_buf->get_refcount() <= 1)
    {
        prvt->out_buf->set_len(static_cast<unsigned int>(len));
        prvt->out_buf->h_reserve();
        return prvt->out_buf;
    }
    return cache_typ::h_allocate(len);
}

bool RMTCPSocket::SetListEnd(unsigned int id, cache_typ* pbuf)
{
#ifndef NO_THREAD
    if (deferred_close)
    {
        return false;
    }
    else
    {
        enable_recv_th = true; /* receiving is enable on first output until flush */
        if (thd == 0)
        {
            thd = new thread_typ(&rcv_function, this, true);
        }
    }
#endif
    bool ret;
    pbuf->set_id(id);
    pbuf->set_cmd(SETLIST);
    ret = SendMessageC(pbuf);
    pbuf->h_unreserve();
    return ret;
}

bool RMTCPSocket::BroadcastEnd(unsigned int id, cache_typ* pbuf)
{
#ifndef NO_THREAD
    if (deferred_close)
    {
        return false;
    }
    else
    {
        enable_recv_th = true; /* receiving is enable on first output until flush */
        if (thd == 0)
        {
            thd = new thread_typ(&rcv_function, this, true);
        }
    }
#endif
    bool ret;
    pbuf->set_id(id);
    pbuf->set_cmd(BROADCAST);
    ret = SendMessageC(pbuf);
    pbuf->h_unreserve();
    return ret;
}

bool RMTCPSocket::RegisterIDX(unsigned int id)
{
    prvt->small_buf->set_len(0);
    prvt->small_buf->set_id(id);
    prvt->small_buf->set_cmd(REGISTERIDX);
    if (SendMessageC(prvt->small_buf))
    {
        return true;
    }
    return false;
}

bool RMTCPSocket::UnregisterIDX(unsigned int id)
{
    prvt->small_buf->set_len(0);
    prvt->small_buf->set_id(id);
    prvt->small_buf->set_cmd(UNREGISTERIDX);
    if (SendMessageC(prvt->small_buf))
    {
        return true;
    }
    return false;
}

bool RMTCPSocket::RegisterID(const char* str)
{
    prvt->small_buf->set_len(RMNAME_LEN / sizeof(unsigned int));
    prvt->small_buf->set_cmd(REGISTERID);
    rmnamecpy((char*)prvt->small_buf->buf(), str);
    if (SendMessageC(prvt->small_buf))
    {
        ++(prvt->queryid);
        return true;
    }
    return false;
}

bool RMTCPSocket::QueryID(const char* str)
{
    prvt->small_buf->set_len(RMNAME_LEN / sizeof(unsigned int));
    prvt->small_buf->set_cmd(QUERYID);
    rmnamecpy((char*)prvt->small_buf->buf(), str);
    if (SendMessageC(prvt->small_buf))
    {
        ++(prvt->queryid);
        return true;
    }
    return false;
}

bool RMTCPSocket::QueryID(const char* str[], int num)
{
    bool ret = true;
    for (int cnt = 0; cnt < num; ++cnt)
    {
        ret &= QueryID(str[cnt]);
    }
    return ret;
}

bool RMTCPSocket::ClientID(const char* client)
{
    prvt->clientok = false;
    prvt->small_buf->set_len(RMNAME_LEN / sizeof(unsigned int));
    prvt->small_buf->set_cmd(CLIENTID);
    rmnamecpy((char*)prvt->small_buf->buf(), client);
    return SendMessageC(prvt->small_buf);
}

bool RMTCPSocket::FlowControl(unsigned int interval)
{
    prvt->intv = interval;
    prvt->small_buf->set_len(0);
    prvt->small_buf->set_id(interval);
    prvt->small_buf->set_cmd(FLOWCONTROL);
    return SendMessageC(prvt->small_buf);
}

bool RMTCPSocket::Broadcast(unsigned int id, size_t len, const unsigned int* buf)
{
    return SendMessageT(id, len, buf, BROADCAST);
}

bool RMTCPSocket::SetList(unsigned int id, size_t len, const unsigned int* buf)
{
    return SendMessageT(id, len, buf, SETLIST);
}

bool RMTCPSocket::Message(unsigned int id, size_t len, const unsigned int* buf)
{
    return SendMessageT(id, len, buf, SETLIST);
}

size_t RMTCPSocket::GetList(unsigned int id, size_t len, unsigned int* buf)
{
    const cache_typ* ptr = GetListStart(id);
    if(ptr)
    {
        size_t sz = ptr->output(len, buf);
        GetListEnd(id, ptr);
        return sz;
    }
    return 0;
}

bool RMTCPSocket::SendMessageT(unsigned int id, size_t len, const unsigned int* buf, int cmd)
{
#ifndef NO_THREAD
    if (deferred_close)
    {
        return false;
    }
#endif
    unsigned int* packet = new unsigned int[HEADER_SIZE + len];
    const size_t size = (HEADER_SIZE + len) * sizeof(unsigned int);
    const char* const cpacket = reinterpret_cast<const char*>(packet);

    packet[0] = BE(MAGICNUM);
    packet[1] = BE((id & 0xFFFFFF) | (cmd << 24));
    packet[2] = BE((unsigned int)((prvt->out_seq << 16) | len));
    if (len)
    {
        memcpy(packet+HEADER_SIZE, buf, len * sizeof(unsigned int));
    }
    if (prvt->sock.send(cpacket, size) != size)
    {
        delete[] packet;
        return false;
    }
    prvt->out_seq = ++(prvt->out_seq) & 0xFFFF;
    delete[] packet;
    prvt->sendtime();

    return true;
}

bool RMTCPSocket::SendMessageC(const cache_typ* ptr)
{
#ifndef NO_THREAD
    if (deferred_close)
    {
        return false;
    }
#endif
    size_t size = (ptr->get_len() + HEADER_SIZE) * sizeof(unsigned int);
    const_cast<cache_typ*>(ptr)->set_seq(prvt->out_seq);
    if (prvt->sock.send((const char*)(ptr->packet()), size) != (int)size)
    {
        return false;
    }
    prvt->out_seq = ++(prvt->out_seq) & 0xFFFF;
    prvt->sendtime();

    return true;
}

void RMTCPSocket::ReceivePacket(bool defercb)
{
    unsigned int length = 0;       // Length of good packet
    unsigned int begin  = 0;       // Location of unprocessed
    unsigned int more   = 1;       // Indicator of pending packets
    unsigned int id     = 0;       // ID of good packet
    unsigned int cmd    = 0;       // Command of good packet
    unsigned int cnt1;             // Misc counters
    int result;                    // Read result

    // state = 0 read in up to (READ_BUF * 4) + 1 bytes (more, begin, valid flag)
    //       = 1 find next packet (begin, length, id, cmd)
    //		 = 2 move incomplete packet to start location (begin)

    if (prvt->readflag)
    {
        return;
    }
    prvt->readflag = true;

    while (more && (prvt->valid < (READ_BUF * sizeof(unsigned int) + 1)))
    {
        // state 0
        result = prvt->sock.recv(((char*)prvt->in_buf) + prvt->valid, (READ_BUF * sizeof(unsigned int) + 1) - prvt->valid);
        if (result > 0)
        {
            prvt->valid += result;
            if (prvt->valid != (READ_BUF * sizeof(unsigned int) + 1))
            {
                more = 0;
            }
        }
        else
        {
            ++(prvt->errflag);
            more = 0;
            break;
        }
        // state 1
        while (prvt->valid >= (HEADER_SIZE * sizeof(unsigned int)))
        {
            if (prvt->in_buf[begin] == BE(MAGICNUM))
            {
                // Magic Number OK
                cnt1   = BE(prvt->in_buf[begin+2]) >> 16;
                if (cnt1 == prvt->in_seq)
                {
                    // Frame Count OK
                    length = BE(prvt->in_buf[begin+2]) & 0xFFFF;
                    if (prvt->valid >= (length+HEADER_SIZE) * sizeof(unsigned int))
                    {
                        // Length Recieved OK
                        cmd = BE(prvt->in_buf[begin + 1]) >> 24;
                        id  = BE(prvt->in_buf[begin + 1]) & 0xFFFFFF;
                        prvt->recvtime();
                        prvt->in_seq = ++(prvt->in_seq) & 0xFFFF;
                        if (cmd < ENDCMD)
                        {
                            if ((cmd == QUERYID) || (cmd == REGISTERID))
                            {
                                if (id < 0xFFFFFF)
                                {
                                    prvt->rcvqueryid = id;
                                }
                            }
                            else if (cmd == CLIENTID)
                            {
                                if (id < 0xFFFFFF)
                                {
                                    prvt->clientok = true;
                                }
                            }

                            if ((cmd == SETLIST) && (id == prvt->getid))
                            {
                                prvt->query_buf = cache_typ::h_duplicate(id, length, &(prvt->in_buf[begin+HEADER_SIZE]));
                            }
                            else if (defercb)
                            {
                                cache_typ* p = cache_typ::h_duplicate(id, length, &(prvt->in_buf[begin+HEADER_SIZE]));
                                p->set_cmd(cmd);
                                Push(p);
                            }
                            else
                            {
                                cache_typ* p = cache_typ::ConvertToCache(&prvt->in_buf[begin]);
                                (*(callback[cmd]))(callback_arg[cmd], id, p);
                            }
                        }
                        begin += length + HEADER_SIZE;
                        prvt->valid -= (length + HEADER_SIZE) * sizeof(unsigned int);
                        prvt->errflag = 0;
                        continue;
                    }
                    else
                    {
                        // Check if exceed buffer size
                        if ((length+HEADER_SIZE > READ_BUF) &&
                            (more != 0))
                        {
                            cache_typ* p = cache_typ::h_allocate(length);
                            p->copy_header(&(prvt->in_buf[begin]));
                            memcpy(p->buf(), &(prvt->in_buf[begin+HEADER_SIZE]), prvt->valid);
                            begin = 0;
                            more = 0;
                            prvt->sock.recv(((char*)(p->buf())) + prvt->valid, length*sizeof(unsigned int) - prvt->valid);
                            prvt->recvtime();
                            Push(p);
                        }
                        // Not full packet recieved; Defer processing
                        else if (begin)
                        {
                            memmove(prvt->in_buf, &(prvt->in_buf[begin]), prvt->valid);
                            begin = 0;
                        }
                        break;
                    }
                }
            }
            ++(prvt->errflag);
            if (prvt->errflag > ERR_TOLERANCE)
            {
                more = 0;
                break;
            }
            begin++;
            prvt->valid -= 4;
        }
        // state 2
        if (begin && prvt->valid)
        {
            memmove(prvt->in_buf, &(prvt->in_buf[begin]), prvt->valid);
            begin = 0;
        }
    }
    prvt->readflag = false;
}

void RMTCPSocket::Push(cache_typ* ptr)
{
    queue_typ* p = new queue_typ;
    p->ptr = ptr;
    p->next = 0;
    if (prvt->queue_end == 0)
    {
        prvt->queue_start = p;
    }
    else
    {
        prvt->queue_end->next = p;
    }
    prvt->queue_end = p;
}

void RMTCPSocket::Pop()
{
    queue_typ* p;
    if ((p = prvt->queue_start) != 0)
    {
        prvt->queue_start = p->next;
        delete p;
        if (prvt->queue_start == 0)
        {
            prvt->queue_end = 0;
        }
    }
}

const cache_typ* RMTCPSocket::Top()
{
    if (prvt->queue_start)
    {
        return prvt->queue_start->ptr;
    }
    return 0;
}

bool RMTCPSocket::WaitForHandshakeCompletion(const struct timeval* stm)
{
    const timeExpire expire(stm);
    do
    {
        if (prvt->clientok && (prvt->rcvqueryid == (prvt->queryid+1)))
        {
            return true;
        }
    }
    while ((Select(stm, true) != -1) && !expire() && prvt->sock.connected());
    return false;
}

void RMTCPSocket::KeepAlive()
{
    if (prvt->intv && (prvt->sendtime.elapsed() >= prvt->intv*1000U))
    {
        SendMessageT(0xFFFFFF, 0, 0, ALIVE);
    }
}

static void rmnamecpy(char* dst, const char* src)
{
    rm_strncpy(dst, RMNAME_LEN, src, RMNAME_LEN-1);
    dst[RMNAME_LEN-1] = '\0';
}

bool RMTCPSocket::MessageEnd(unsigned int id, cache_typ* pbuf, void*)
{
    return SetListEnd(id, pbuf);
}

bool RMTCPSocket::Flush()
{
#ifndef NO_THREAD
    enable_recv_th = false;
    if (deferred_close && (thd != 0))
    {
        delete thd; /* Flush is a good place to delete thread because
                       it is at the end of outgoing which should be called
                       by the main thread only
                     */
        thd = 0;
    }
#endif
    return true;
}

int RMTCPSocket::GetHandle() const
{
    return -1;
}

bool RMTCPSocket::CheckLink()
{
    return !(prvt->intv && (prvt->recvtime.elapsed() > prvt->intv * 2000U));
}

bool RMTCPSocket::HandshakeAck(unsigned int cmd, unsigned int id)
{
    return SendMessageT(id, 0, 0, (int)cmd);
}

#ifndef NO_THREAD
bool RMTCPSocket::recv_th_function()
{
    struct timeval stm = {
        0, 0
    };
    if (enable_recv_th && atomic_tas(&receive_mutex) == 1)
    {
        int result = sock.select(&stm);
        if (result > 0)
        {
            ReceivePacket(true);
            atomic_untas(&receive_mutex);
            if (errflag > ERR_TOLERANCE)
            {
                deferred_close = true;
            }
        }
        else if (result < 0)
        {
            atomic_untas(&receive_mutex);
            deferred_close = true;
        }
        else
        {
            atomic_untas(&receive_mutex);
            context_yield();
        }
        return deferred_close;
    }
    else
    {
        context_yield();
    }
    return false;
}
#endif

/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "storage.h"
#include <util/dbgout.h>
#include <string.h>

void set_name(char* tgt, char ch)
{
    char str[128];
    rm_sprintf(str, RMNAME_LEN, "%c", ch);
    rm_strcat(tgt, RMNAME_LEN, str);
}

void set_name(char* tgt, char ch, int idx)
{
    char str[128];
    str[0] = ch;
    rm_sprintf(str+1, RMNAME_LEN-1, "%02d", idx);
    rm_strcat(tgt, RMNAME_LEN, str);
}

#ifdef _WIN32
# define DLL_IMPORT __declspec(dllimport)
#else
# define DLL_IMPORT
#endif

extern "C" DLL_IMPORT void mm_core_create_delete(size_t usersize, const char* file);

#if !defined(_WIN32) && !defined(VXWORKS)
extern "C" int unlink(const char*);
#endif

int main(int argc, char** argv)
{
    char defstorage[] = "RESMGR4";
    char* base = (argc > 1) ? argv[1] : defstorage;
    char shmname[RMNAME_LEN];
    dout.useconsole(true);

    rm_strcpy(shmname, RMNAME_LEN, base);
    set_name(shmname, 'C');
    mm_core_create_delete(CACHE_SIZE, shmname);

    unsigned int i;
    for (i = 0; i < MAX_CLIENT; ++i)
    {
        rm_strcpy(shmname, RMNAME_LEN, base);
        set_name(shmname, 'Q', i);
        mm_core_create_delete(QUEUE_SIZE, shmname);
    }

    rm_strcpy(shmname, RMNAME_LEN, base);
    set_name(shmname, 'V');
    mm_core_create_delete(VAR_SIZE, shmname);

    mm_core_create_delete(sizeof(rmglobal_private_typ), base);

#if !defined(_WIN32) && !defined(VXWORKS)
    char semname[RMNAME_LEN];
    for(i = 0; i < MAX_CLIENT; ++i)
    {
        sprintf(semname, "/tmp/%s-%u", base, i);
        unlink(semname);
    }
#endif
    return 0;
}

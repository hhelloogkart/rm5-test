#ifndef _LOCKFREEQUEUE_H_
#define _LOCKFREEQUEUE_H_

extern "C" OSCORE_EXPORT int atomic_cas(volatile long* addr, long new_val, long cmp_val);

template<class C>
int lockfree_atomic_cas(volatile long* addr, long new_val, long cmp_val)
{
    return atomic_cas(addr, new_val, cmp_val);
}

template<class C>
class LockFreeQueue
{
public:

    LockFreeQueue()
    {
    }
    ~LockFreeQueue()
    {
        volatile long* addr;
        while( first != NULL ) // release the list
        {
            cache_typ* tmp = first;
            addr = reinterpret_cast<long*>(&first);
            lockfree_atomic_cas<C>(addr, (long)first->dummy, first.offset);
            cache_typ::ssm_deallocate(tmp);
        }
    }
    void Dummy(void)
    {
        first = smcache_typ::ssm_allocate(0);
        divider = first;
        last = first;
    }

    void Produce(cache_typ* ptr)
    {
        const ptrdiff_t offset = reinterpret_cast<char*>(ptr) - reinterpret_cast<char*>(cache_staticmemory_segment::Ptr);
        last->dummy = (void*)(offset);
        volatile long* addr = reinterpret_cast<long*>(&last);
        lockfree_atomic_cas<C>(addr, offset, last.offset);
    }

    bool Consume(cache_typ*& result)
    {
        if( divider != last ) // if queue is nonempty
        {
            result = (cache_typ*)((ptrdiff_t)cache_staticmemory_segment::Ptr + (ptrdiff_t)divider->dummy);
            volatile long* addr = reinterpret_cast<long*>(&divider);
            lockfree_atomic_cas<C>(addr, (long)divider->dummy, divider.offset);
            return true;            // and report success
        }
        return false;               // else report empty
    }

    void Trim(void)
    {
        //Trim unused nodes
        while(first != divider)
        {
            cache_typ* tmp = first;
            volatile long* addr = reinterpret_cast<long*>(&first);
            lockfree_atomic_cas<C>(addr, (long)first->dummy, first.offset);
            smcache_typ::ssm_deallocate(tmp);
        }
    }
    bool Setup(unsigned int size, unsigned int num)
    {
        cache_typ* tmp = first;
        if(tmp->get_len() == 0)
        {
            smcache_typ::ssm_deallocate(tmp);

            cache_typ* prev = (cache_typ*)smcache_typ::ssm_allocate(size);
            cache_typ* curr;
            first = prev;
            for(unsigned int i = 0; i < num; i++)
            {
                curr = (cache_typ*)smcache_typ::ssm_allocate(size);
                prev->dummy = (void*)((unsigned long)curr - (unsigned long)cache_staticmemory_segment::Ptr);
                prev = curr;
            }
            curr->dummy = (void*)(first.offset);
            last = curr;
            return true;
        }
        return false;
    }

    bool Request(cache_typ*& ptr)
    {
        if((unsigned long)last->dummy == first.offset || first == last)
        {
            return false;
        }
        ptr = (cache_typ*)((ptrdiff_t)cache_staticmemory_segment::Ptr + (ptrdiff_t)last->dummy);
        volatile long* addr = reinterpret_cast<long*>(&last);
        lockfree_atomic_cas<C>(addr, (long)last->dummy, last.offset);
        return true;
    }

    cache_typ* getCache()
    {
        if((unsigned long)first->dummy == last.offset || first == last)
        {
            return NULL;
        }
        return (cache_typ*)((unsigned long)cache_staticmemory_segment::Ptr + first.offset);
    }

    void fillCache()
    {
        volatile long* addr = reinterpret_cast<long*>(&first);
        lockfree_atomic_cas<C>(addr, (long)first->dummy, first.offset);
    }
private:
    relative_ptr<cache_typ, cache_staticmemory_segment> first, divider, last;
};
#endif /* _LOCKFREEQUEUE_H_ */

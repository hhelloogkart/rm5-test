/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "varitem.h"

var_item_typ::var_item_typ()
{
}

var_item_typ::var_item_typ(const var_item_typ& other) : ptr(other.ptr)
{
    notify = other.notify;
}

const var_item_typ& var_item_typ::operator=(const var_item_typ& other)
{
    ptr = other.ptr;
    notify = other.notify;
    return *this;
}

/******************************************************************/
/* Copyright DSO National Laboratories 2006. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "../rmstl.h"
#include <rminterf.h>
#include <rmmsg_var.h>
#include <arry_var.h>
#include <gen_var.h>
#include <cache.h>
#include <defaults.h>
#include <defutil.h>
#include <rm_ver.h>
#include <util/dbgout.h>
#include <util/utilcore.h>
#include "rmsocket.h"
#include "rm_var.h"
#include <time/timecore.h>
#include <assert.h>
#ifndef NO_THREAD
#include <task/threadt.h>
#endif

extern RM_EXPORT int core_select(int* socks, int socklen, const struct timeval* stm);

typedef VECTOR(rm_interface*) rmintefacelist_typ;
typedef VECTOR(int) fdlist_typ;

static protocol_description& description()
{
    static protocol_description d = {
        0,
        &rm_interface::init,
        &rm_interface::incoming,
        &rm_interface::outgoing,
        0
    };
    return d;
}

static rmintefacelist_typ& rminterface_list()
{
    static rmintefacelist_typ v;
    return v;
}

static void _doSetList(void* interf, unsigned int id, const cache_typ* buf)
{
    reinterpret_cast<rm_interface*>(interf)->doSetList(id, buf);
}

static void _doGetList(void* interf, unsigned int id, const cache_typ*)
{
    reinterpret_cast<rm_interface*>(interf)->doGetList(id);
}

static void _doClientID(void* interf, unsigned int id, const cache_typ* buf)
{
    reinterpret_cast<rm_interface*>(interf)->doClientID(id, buf);
}

static void _doQueryID(void* interf, unsigned int id, const cache_typ* buf)
{
    reinterpret_cast<rm_interface*>(interf)->doQueryID(id, buf);
}

static void _doRegisterID(void* interf, unsigned int id, const cache_typ* buf)
{
    reinterpret_cast<rm_interface*>(interf)->doRegisterID(id, buf);
}

static struct timeval select_delay = {
    0, 100000
};

#ifndef NO_EXITSHOW
class RM_EXPORT var_exit : public rm_message_var
{
public:
    RMMSGCONST(var_exit, "", true);
    generic_var<int> value;
};
#endif

#ifndef NO_CLIENT_VER
class RM_EXPORT var_client_name : public rm_var
{
public:
    RMCONST(var_client_name, "", false)
    ext_array_str_var<0, 0, 0> value;
};
#endif

struct rm_interface_private
{
    RMBaseSocket* sock;
    char hostname[256];
    char clientname[RMNAME_LEN];
    int hostport;
    unsigned int flow_interval;
    void (* connectCB)();
    int queryid;
#ifndef NO_THREAD
    thread_typ* conn_thread;
    bool thread_working;
#endif
#ifndef NO_CLIENT_VER
    var_client_name* v_cl;
#endif
#ifndef NO_EXITSHOW
    var_exit* v_ex;
#endif

};

rm_interface::rm_interface() : prvt(new rm_interface_private)
{
    rmclient_init::install_protocol(description(), true);
    rminterface_list().push_back(this);
    prvt->sock = NULL;
    prvt->hostname[0] = '\0';
    prvt->clientname[0] = '\0';
    prvt->hostport = 0;
    prvt->flow_interval = FLOW_TIMEOUT;
    prvt->connectCB = 0;
    prvt->queryid = 1;
#ifndef NO_THREAD
    prvt->conn_thread = 0;
    prvt->thread_working = false;
#endif
#ifndef NO_CLIENT_VER
    prvt->v_cl = 0;
#endif
#ifndef NO_EXITSHOW
    prvt->v_ex = 0;
#endif
}

rm_interface::~rm_interface()
{
#ifndef NO_THREAD
    delete prvt->conn_thread;
#endif
#ifndef NO_CLIENT_VER
    delete prvt->v_cl;
#endif
#ifndef NO_EXITSHOW
    delete prvt->v_ex;
#endif
    delete prvt->sock;
    delete prvt;
}

void rm_interface::enable()
{
    for (rmintefacelist_typ::iterator iter = rminterface_list().begin(); iter != rminterface_list().end(); ++iter)
    {
        if ((*iter) == this)
        {
            return;
        }
    }
    rminterface_list().push_back(this);
}

void rm_interface::disable()
{
    for (rmintefacelist_typ::iterator iter = rminterface_list().begin(); iter != rminterface_list().end(); ++iter)
    {
        if ((*iter) == this)
        {
            rminterface_list().erase(iter);
            break;
        }
    }
#ifndef NO_THREAD
    if (prvt->conn_thread != NULL)
    {
        while (!prvt->conn_thread->finished())
        {
            millisleep(500);
        }
        delete prvt->conn_thread;
        prvt->conn_thread = 0;
    }
    else
    {
        if (prvt->sock)
        {
            prvt->sock->Close();
        }
    }
#else
    if (prvt->sock)
    {
        prvt->sock->Close();
    }
#endif
}

RMBaseSocket& rm_interface::Socket() const
{
    return *(prvt->sock);
}

void rm_interface::doSetList(unsigned int id, const cache_typ* buf)
{
    if ((id <= (unsigned int)cardinal()) && (id == static_cast<rm_var*>((*this)[id-1])->getID()))
    {
        (*this)[id-1]->extract(buf->get_len()*sizeof(unsigned int), (unsigned char*)(buf->buf()));
    }
    else
    {
        unsigned int i;
        for(i = 0; i < (unsigned)cardinal(); i++)
        {
            if(id == static_cast<rm_var*>((*this)[i])->getID())
            {
                (*this)[i]->extract(buf->get_len()*sizeof(unsigned int), (unsigned char*)(buf->buf()));
                return;
            }
        }
        dout << "Error rx id " << id << " > " << cardinal() << endl << flush;
    }
}

void rm_interface::doGetList(unsigned int id)
{
    if (id <= (unsigned int)cardinal())
    {
        (*this)[id-1]->setRMDirty();
    }
    else
    {
        dout << "Error rx id " << id << " > " << cardinal() << endl << flush;
    }
}

void rm_interface::doClientID(unsigned int, const cache_typ* buf)
{
    if (buf->get_len() != 0)
    {
        prvt->sock->HandshakeAck(CLIENTID, 0);
        prvt->queryid = 1;
    }
}

void rm_interface::doQueryID(unsigned int, const cache_typ* buf)
{
    if (buf->get_len() != 0)
    {
        if (prvt->queryid >= cardinal())
        {
            dout << "Error rx queryid " << prvt->queryid << " > list size\n";
        }
        else if (strcmp((char*)buf->buf(), static_cast<rm_var*>((*this)[prvt->queryid])->getName()) != 0)
        {
            dout << "Error rx queryid " << (char*)buf->buf() << " != " << static_cast<rm_var*>((*this)[prvt->queryid])->getName() << '\n';
        }
        prvt->sock->HandshakeAck(QUERYID, prvt->queryid);
        ++prvt->queryid;
    }
}

void rm_interface::doRegisterID(unsigned int, const cache_typ* buf)
{
    if (buf->get_len() != 0)
    {
        if (prvt->queryid >= cardinal())
        {
            dout << "Error rx queryid " << prvt->queryid << " > list size\n";
        }
        else if (strcmp((char*)buf->buf(), static_cast<rm_var*>((*this)[prvt->queryid])->getName()) != 0)
        {
            dout << "Error rx queryid " << (char*)buf->buf() << " != " << static_cast<rm_var*>((*this)[prvt->queryid])->getName() << '\n';
        }
        prvt->sock->HandshakeAck(REGISTERID, prvt->queryid);
        ++prvt->queryid;
    }
}

void rm_interface::initialize(const char* client, int port, const char* host, RMBaseSocket* clientsock, unsigned int flow)
{
    rm_strcpy(prvt->clientname, RMNAME_LEN, client);
    prvt->hostport = port;
    if (host)
    {
        rm_strcpy(prvt->hostname, 256, host);
    }
    prvt->flow_interval = flow;
    if (clientsock != NULL)
    {
        prvt->sock = clientsock;
        prvt->sock->AddCallback(SETLIST, this, _doSetList);
        prvt->sock->AddCallback(BROADCAST, this, _doSetList);
        prvt->sock->AddCallback(MESSAGE, this, _doSetList);
        prvt->sock->AddCallback(GETLIST, this, _doGetList);
        prvt->sock->AddCallback(CLIENTID, NULL, &_doClientID);
        prvt->sock->AddCallback(QUERYID, NULL, &_doQueryID);
        prvt->sock->AddCallback(REGISTERID, NULL, &_doRegisterID);
    }
    else
    {
        dout << "Initialisation of rm_socket failed\n";
    }
    for (int cnt = 0; cnt < cardinal(); ++cnt)
    {
        rm_var* var = RECAST(rm_var*,(*this)[cnt]);
        if (var == 0)
        {
            dout << "Error: children of rm_interface must be rm_vars\n";
            continue;
        }
        var->setID(cnt+1);
    }
}

void rm_interface::setHost(const char* host)
{
    rm_strcpy(prvt->hostname, 256, host);
}

bool rm_interface::try_connect()
{
    struct timeval conntimeout = {
        select_delay.tv_sec / static_cast<long>(rminterface_list().size()),
        select_delay.tv_usec / static_cast<long>(rminterface_list().size())
    };

    if (prvt->thread_working && prvt->sock->Connect(prvt->hostname, prvt->hostport, &conntimeout))
    {
        struct timeval stm = {
            5 + (cardinal() / 2), 0
        };

        prvt->sock->FlowControl(prvt->flow_interval);
        prvt->sock->ClientID(prvt->clientname);
        for (int i = 0; i < (size_t)cardinal(); ++i)
        {
            bool wait_for_reply;
            rm_var* var = RECAST(rm_var*,(*this)[i]);
            if (var->regflag)
            {
                wait_for_reply = prvt->sock->RegisterID(var->getName());
            }
            else
            {
                wait_for_reply = prvt->sock->QueryID(var->getName());
            }
            if (wait_for_reply && ((i % 5) == 0))
            {
                if(prvt->sock->Select(&select_delay, true) < 0)
                {
                    prvt->sock->Close();
                    return false;
                }
            }
        }
        if (!prvt->sock->WaitForHandshakeCompletion(&stm))
        {
            dout << "No reply from RM\n";
            prvt->sock->Close();
            return false;
        }
        if (prvt->connectCB)
        {
            (*(prvt->connectCB))();
        }
        prvt->queryid = 1;
        prvt->thread_working = false;
    }
    else
    {
#ifndef NO_THREAD
        millisleep(500);
#endif
    }
    return false;
}

bool try_connect(void* arg)
{
    return ((rm_interface*)arg)->try_connect();
}

bool rm_interface::check_connect()
{
#ifndef NO_THREAD
    static bool nomorethreads = false;
#endif

    if (prvt->sock == NULL)
    {
        return false;
    }
#ifndef NO_THREAD
    if (prvt->conn_thread != 0)
    {
        if (!prvt->thread_working)
        {
            const bool conn = prvt->sock->Connected();
            prvt->thread_working = !conn;
            return conn;
        }
        else
        {
            return false;
        }
    }
    else
    {
        if (!nomorethreads)
        {
            prvt->thread_working = true;
            prvt->conn_thread = new thread_typ(&::try_connect, this, true);
            if (!prvt->conn_thread->started())
            {
                delete prvt->conn_thread;
                prvt->conn_thread = 0;
                nomorethreads = true;
            }
        }
        return false;
    }
#else
    if (!prvt->sock->Connected())
    {
        return try_connect();
    }
#endif
    return true;
}

int rm_interface::interface_count()
{
    return static_cast<int>(rminterface_list().size());
}

void rm_interface::init(defaults* def)
{
    char clientstr[RMNAME_LEN], hoststr[256], accessstr[RMNAME_LEN];
    int port, flow;

    def->getint("timeout_sec", reinterpret_cast<int*>(&select_delay.tv_sec));
    def->getint("timeout_usec", reinterpret_cast<int*>(&select_delay.tv_usec));

#ifndef NO_CLIENT_VER
    char verstr[256];
    minisprintf(verstr, "RM Lib %s (%s) - Compiled %s %s by %s\n", COMPILE_VER, COMPILE_OSNAME, COMPILE_DATE, COMPILE_TIME, COMPILE_BY);
    dout << verstr;
#endif

    assert(def->getstring("client", clientstr) != 0);
    if (def->getstring("access", accessstr) == NULL)
    {
        rm_strcpy(accessstr, RMNAME_LEN, "TCP");
    }
    if (def->getstring("host", hoststr) == NULL)
    {
        if (strcmp(accessstr, "TCP") == 0)
        {
            rm_strcpy(hoststr, 256, "Auto");
        }
        else if (strcmp(accessstr, "SM") == 0)
        {
            rm_strcpy(hoststr, 256, "RESMGR4");
        }
        else if (strcmp(accessstr, "RM3") == 0)
        {
            rm_strcpy(hoststr, 256, "Auto");
        }
    }
#if defined(WIN32) && !defined(NDEBUG)
    if (strcmp(accessstr, "SM") == 0)
    {
        const size_t len = strlen(hoststr);
        if (len + 1 < RMNAME_LEN)
        {
            hoststr[len] = 'D';
            hoststr[len + 1] = '\0';
        }
    }
#endif
    if (def->getint("port", &port) == NULL)
    {
        if (strcmp(accessstr, "SM") == 0)
        {
            port = 0;
        }
        else
        {
            port = 2006;
        }
    }
    def->getint("flow", &flow);
    for (int cnt = 1; cnt <= rm_interface::interface_count(); ++cnt)
    {
        char __clientstr[RMNAME_LEN], __hoststr[256], __accessstr[RMNAME_LEN], __rmnamestr[RMNAME_LEN], tmpstr[256];
        const char* p_client, * p_hoststr, *p_accessstr;
        int __port, __flow;
        RMBaseSocket* sock;

        minisprintf(tmpstr, "access@@%d", cnt);
        if (def->getstring(tmpstr, __accessstr) == NULL)
        {
            p_accessstr = accessstr;
        }
        else
        {
            p_accessstr = __accessstr;
        }
        sock = RMBaseSocket::Construct(p_accessstr);
        assert(sock != 0);

        minisprintf(tmpstr, "host@@%d", cnt);
        if (def->getstring(tmpstr, __hoststr) == NULL)
        {
            p_hoststr = hoststr;
        }
        else
        {
            p_hoststr = __hoststr;
#if defined(WIN32) && !defined(NDEBUG)
            if (strcmp(p_accessstr, "SM") == 0)
            {
                const size_t len = strlen(__hoststr);
                if (len + 1 < RMNAME_LEN)
                {
                    __hoststr[len] = 'D';
                    __hoststr[len + 1] = '\0';
                }
            }
#endif
        }

        minisprintf(tmpstr, "port@@%d", cnt);
        if (def->getint(tmpstr, &__port) == NULL)
        {
            __port = port;
        }

        minisprintf(tmpstr, "flow@@%d", cnt);
        if (def->getint(tmpstr, &__flow) == NULL)
        {
            __flow = flow;
        }

        minisprintf(tmpstr, "client@@%d", cnt);
        if (def->getstring(tmpstr, __clientstr) == NULL)
        {
            p_client = clientstr;
        }
        else
        {
            p_client = __clientstr;
        }

        rm_interface* rmi = rminterface_list()[cnt - 1];
        rmi->push();

#if !defined(NO_CLIENT_VER) || !defined(NO_EXITSHOW)
        rm_strcpy(__rmnamestr + 1, RMNAME_LEN, p_client);
#endif
#ifndef NO_CLIENT_VER
        rmi->prvt->v_cl = new var_client_name;
        __rmnamestr[0] = '@';
        rmi->prvt->v_cl->setName(__rmnamestr);
        rmi->prvt->v_cl->value = verstr;
#endif
#ifndef NO_EXITSHOW
        rmi->prvt->v_ex = new var_exit;
        __rmnamestr[0] = '~';
        rmi->prvt->v_ex->setName(__rmnamestr);
        rmclient_init::install_exit_trigger(rmi->prvt->v_ex);
#endif
        rmi->pop();

        rmi->initialize(p_client, __port, p_hoststr, sock, (unsigned int)__flow);
    }
}

void rm_interface::set_select_timeout(struct timeval* wait)
{
    if (wait)
    {
        select_delay = *wait;
    }
}

void rm_interface::incoming()
{
    static bool reentrant = false;
    if (!reentrant)
    {
        reentrant = true;

        int cnt;
        rmintefacelist_typ noselectlist;
        rmintefacelist_typ selectlist;
        fdlist_typ fdlist;

        selectlist.reserve(rminterface_list().size());
        noselectlist.reserve(rminterface_list().size());
        for (cnt = 0; cnt < (int)rminterface_list().size(); ++cnt)
        {
            if (rminterface_list()[cnt]->check_connect())
            {
                int fd = rminterface_list()[cnt]->prvt->sock->GetHandle();
                if (fd != -1)
                {
                    selectlist.push_back(rminterface_list()[cnt]);
                    fdlist.push_back(fd);
                }
                else
                {
                    noselectlist.push_back(rminterface_list()[cnt]);
                }
            }
        }
        struct timeval def;
#if !defined(NO_RM3) || !defined(NO_RMTCP) || !defined(NO_RMDUDP) || !defined(NO_RMUDP)
        if(selectlist.size() > 0)
        {
            def.tv_sec = static_cast<long>(select_delay.tv_sec  * selectlist.size() / rminterface_list().size());
            def.tv_usec = static_cast<long>(select_delay.tv_usec * selectlist.size() / rminterface_list().size());
            int result = core_select(&fdlist[0], static_cast<int>(fdlist.size()), &def);
            if (result > 0)
            {
                for (cnt = 0; cnt < (int)fdlist.size(); ++cnt)
                {
                    if (fdlist[cnt] >= 0)
                    {
                        selectlist[cnt]->prvt->sock->Select();
                    }
                    else if (fdlist[cnt] == -1)
                    {
                        selectlist[cnt]->prvt->sock->Close();
                    }
                    else
                    {
                        selectlist[cnt]->prvt->sock->KeepAlive();
                        if (!selectlist[cnt]->prvt->sock->CheckLink())
                        {
                            selectlist[cnt]->prvt->sock->Close();
                        }
                    }
                }
            }
        }
#endif
        if(noselectlist.size() > 0)
        {
            def.tv_sec = select_delay.tv_sec / static_cast<long>(rminterface_list().size());
            def.tv_usec = select_delay.tv_usec / static_cast<long>(rminterface_list().size());
            for (cnt = 0; cnt < (int)noselectlist.size(); ++cnt)
            {
                if(noselectlist[cnt]->prvt->sock->Select(&def) < 0)
                {
                    noselectlist[cnt]->prvt->sock->Close();
                }
            }
        }
        reentrant = false;
    }
}

void rm_interface::outgoing()
{
    static bool reentrant = false;
    if (!reentrant)
    {
        reentrant = true;

        for (int cnt = 0; cnt < (int)rminterface_list().size(); ++cnt)
        {
            if (rminterface_list()[cnt]->check_connect())
            {
                for (int cnt1 = 0; cnt1 < rminterface_list()[cnt]->cardinal(); ++cnt1)
                {
                    mpt_var* mpv = (*rminterface_list()[cnt])[cnt1];
#ifdef NO_RTTI
                    reinterpret_cast<rm_var*>(mpv)->refreshRM();
#else
                    rm_var* rmv = dynamic_cast<rm_var*>(mpv);
                    if (rmv)
                    {
                        rmv->refreshRM();
                    }
#endif
                }
                rminterface_list()[cnt]->prvt->sock->KeepAlive();
                rminterface_list()[cnt]->prvt->sock->Flush();
            }
        }

        reentrant = false;
    }
}

void rm_interface::close()
{
    for (int cnt = 0; cnt < (int)rminterface_list().size(); ++cnt)
    {
        rminterface_list()[cnt]->prvt->sock->Close();
    }
}

/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "../rmstl.h"
#include <val_var.h>
#include <outbuf.h>

typedef SET(int, less<int> ) validset_typ;

struct valid_var_private
{
    validset_typ validset;
};

valid_var::valid_var(const char* nm, bool mode) :
    rm_var(nm, mode), prvt(new valid_var_private)
{
    pop();
}

valid_var::~valid_var()
{
    delete prvt;
}

int valid_var::extract(int len, const unsigned char* buf)
{
    int cnt1, len1 = len / sizeof(unsigned int);
    unsigned int* buf1 = (unsigned int*)buf;
    prvt->validset.clear();
    for (cnt1 = 0; cnt1 < len1; ++cnt1)
    {
        prvt->validset.insert(flipint(buf1[cnt1]));
    }
    setDirty();
    return 0;
}

int valid_var::size() const
{
    return static_cast<int>(prvt->validset.size() * sizeof(unsigned int));
}

void valid_var::output(outbuf& strm)
{
    validset_typ::iterator iter1;
    union
    {
        int i;
        char c[4];
    } cnt1;

    for (iter1 = prvt->validset.begin(); iter1 != prvt->validset.end(); ++iter1)
    {
        cnt1.i = flipint(*iter1);
        strm += cnt1.c[0];
        strm += cnt1.c[1];
        strm += cnt1.c[2];
        strm += cnt1.c[3];
    }
}

bool valid_var::isValid() const
{
    return size() != 0;
}

bool valid_var::setInvalid()
{
    if (!prvt->validset.empty())
    {
        prvt->validset.clear();
        setDirty();
        return true;
    }
    notDirty();
    return false;
}

const mpt_var& valid_var::operator=(const mpt_var& right)
{
    const valid_var* p = RECAST(const valid_var*,&right);
    if (p)
    {
        *this = *p;
    }
    return *this;
}

bool valid_var::operator==(const mpt_var& right) const
{
    const valid_var* p = RECAST(const valid_var*,&right);
    if (p)
    {
        return *this == *p;
    }
    return complex_var::operator==(right);
}

const valid_var& valid_var::operator=(const valid_var& right)
{
    prvt->validset = right.prvt->validset;
    setDirty();
    setRMDirty();
    return *this;
}

bool valid_var::operator==(const valid_var& right) const
{
    return prvt->validset == right.prvt->validset;
}

istream& valid_var::operator>>(istream& s)
{
    int max1, cnt1, tmp;
    s >> rmmsgname;
    s >> regflag;
    s >> max1;
    prvt->validset.clear();
    for (cnt1 = 0; !s.fail() && cnt1 < max1; ++cnt1)
    {
        s >> tmp;
        if (!s.fail()) setElement(tmp);
    }
    return s;
}

ostream& valid_var::operator<<(ostream& s) const
{
    validset_typ::const_iterator p;
    s << rmmsgname << ' ';
    s << regflag << ' ';
    s << prvt->validset.size();
    for (p = prvt->validset.begin(); p != prvt->validset.end(); ++p)
    {
        s << ' ';
        s << *p;
    }
    return s;
}

void valid_var::setElement(int obj)
{
    prvt->validset.insert(obj);
    setDirty();
    setRMDirty();
}
void valid_var::clrElement(int obj)
{
    prvt->validset.erase(obj);
    setDirty();
    setRMDirty();
}

bool valid_var::chkElement(int obj) const
{
    return prvt->validset.count(obj) > 0;
}

int valid_var::addElement()
{
    int cnt1 = 0;

    while (chkElement(cnt1))
    {
        ++cnt1;
    }
    setElement(cnt1);
    return cnt1;
}

mpt_var* valid_var::getNext()
{
    return this + 1;
}

#include "rmconn.h"
#include "../include/cache.h"
#include "../rmstl.h"
#include <comm/msgcore.h>
#include <time/timecore.h>
#include <limits.h>

typedef LIST(cache_typ*) rm_defer_list;

struct rmconn_socket_private
{
    msg_core sock;
    rm_defer_list defercb;
    unsigned int getid;
    cache_typ* query_buf;
};

RMIConnSocket::RMIConnSocket() : prvt(new rmconn_socket_private)
{
    prvt->getid = UINT_MAX;
    prvt->query_buf = 0;
}
RMIConnSocket::~RMIConnSocket()
{
    for (rm_defer_list::iterator iter = prvt->defercb.begin(); iter != prvt->defercb.end(); ++iter)
    {
        (*iter)->h_unreserve();
    }
    delete prvt;
}
bool RMIConnSocket::Connect(const char* host, int port, const struct timeval*)
{
    prvt->sock.sethost(host, port);
    return true;
}
bool RMIConnSocket::Connected()
{
    return prvt->sock.connected();
}
bool RMIConnSocket::Close()
{
    return prvt->sock.close();
}
int RMIConnSocket::Select(const struct timeval* stm, bool defercb)
{
    if (!defercb)
    {
        while (!prvt->defercb.empty())
        {
            const cache_typ* p = prvt->defercb.front();
            unsigned int cmd = p->get_cmd();
            if (cmd < ENDCMD)
            {
                (*(callback[cmd]))(callback_arg[cmd], p->get_id(), p);
            }
            prvt->defercb.pop_front();
            const_cast<cache_typ*>(p)->h_unreserve();
        }
    }
    else
    {
        return 0;
    }

    timeExpire* te = 0;
    bool redo = true;
    size_t len;
    char* buf;
    int ret = 0;

redo_recv:
    while (prvt->sock.recv(&buf, len))
    {
        redo = false;
        ret = 1;
        cache_typ* p = cache_typ::ConvertToCache((unsigned int*)buf, false);
        if (p->get_cmd() == GETLIST)
        {
            (*(callback[GETLIST]))(callback_arg[GETLIST], p->get_id(), p);
        }
        else if (p->get_id() == prvt->getid)
        {
            prvt->query_buf = cache_typ::h_duplicate(p->get_id(), p->get_len(), p->buf());
        }
        else
        {
            if (defercb)
            {
                cache_typ* dfp = cache_typ::h_duplicate(p->get_id(), p->get_len(), p->buf());
                prvt->defercb.push_back(dfp);
            }
            else
            {
                (*(callback[p->get_cmd()]))(callback_arg[p->get_cmd()], p->get_id(), p);
            }
        }
    }
    if (redo && (stm != 0) && ((stm->tv_sec != 0) || (stm->tv_usec != 0)))
    {
        unsigned int delay = stm->tv_sec * 1000 + stm->tv_usec / 1000;
        if (delay == 0)
        {
            redo = false;
            goto redo_recv;
        }
        else if (delay <= 10)
        {
            redo = false;
            millisleep(delay);
            goto redo_recv;
        }
        else if (te == 0)
        {
            te = new timeExpire(stm);
            millisleep(10);
            goto redo_recv;
        }

        unsigned int left = te->left();
        if (left <= 10)
        {
            redo = false;
            millisleep(left);
            goto redo_recv;
        }
        else if (left != 0)
        {
            millisleep(10);
            goto redo_recv;
        }
    }
    delete te;
    return ret;
}
const cache_typ* RMIConnSocket::GetListStart(unsigned int id)
{
    int cnt = 0, ret;
    char* buf = prvt->sock.send_buffer(sizeof(cache_typ));
    if (buf == 0)
    {
        return 0;
    }
    cache_typ* c = cache_typ::CastToCache(buf, true);
    c->set_cmd(GETLIST);
    c->set_id(id);
    c->set_len(0);
    prvt->sock.send((const char*)c->packet(), sizeof(header_typ));
    prvt->sock.flush();
    do
    {
        if (cnt != 0)
        {
            Sleep(WAIT_TIME);
        }
        struct timeval tm = {
            0, WAIT_TIME*1000
        };
        ret = Select(&tm, true);
        ++cnt;
    }
    while ((prvt->query_buf == NULL) && (cnt < GETLIST_RETRY) && (ret >= 0));
    cache_typ* ptr = prvt->query_buf;
    prvt->getid = UINT_MAX;
    prvt->query_buf = NULL;
    return ptr;
}
void RMIConnSocket::GetListEnd(unsigned int, const cache_typ* pbuf)
{
    if (pbuf->check_header())
    {
        const_cast<cache_typ*>(pbuf)->h_unreserve();
    }
}
cache_typ* RMIConnSocket::SetListStart(unsigned int, size_t len)
{
    char* buf = prvt->sock.send_buffer(sizeof(cache_typ)+len*sizeof(unsigned int));
    if (buf == 0)
    {
        return 0;
    }
    cache_typ* p = cache_typ::CastToCache((unsigned int*)buf, true);
    p->set_len(len);
    return p;
}
cache_typ* RMIConnSocket::MessageStart(unsigned int id, size_t len)
{
    return SetListStart(id, len);
}
bool RMIConnSocket::SetListEnd(unsigned int id, cache_typ* pbuf)
{
    pbuf->set_id(id);
    pbuf->set_cmd(SETLIST);
    prvt->sock.send((const char*)pbuf->packet(), pbuf->get_len()*sizeof(unsigned int)+sizeof(header_typ));
    return true;
}
bool RMIConnSocket::BroadcastEnd(unsigned int id, cache_typ* pbuf)
{
    pbuf->set_id(id);
    pbuf->set_cmd(BROADCAST);
    prvt->sock.send((const char*)pbuf->packet(), pbuf->get_len()*sizeof(unsigned int)+sizeof(header_typ));
    return true;
}
bool RMIConnSocket::MessageEnd(unsigned int id, cache_typ* pbuf, void*)
{
    pbuf->set_id(id);
    pbuf->set_cmd(MESSAGE);
    prvt->sock.send((const char*)pbuf->packet(), pbuf->get_len()*sizeof(unsigned int)+sizeof(header_typ));
    return true;
}
bool RMIConnSocket::RegisterID(const char*)
{
    return false;
}
bool RMIConnSocket::QueryID(const char*)
{
    return false;
}
bool RMIConnSocket::QueryID(const char*[], int)
{
    return false;
}
bool RMIConnSocket::ClientID(const char*)
{
    return false;
}
bool RMIConnSocket::RegisterIDX(unsigned int)
{
    return true;
}
bool RMIConnSocket::UnregisterIDX(unsigned int)
{
    return true;
}
bool RMIConnSocket::FlowControl(unsigned int)
{
    return true;
}
bool RMIConnSocket::WaitForHandshakeCompletion(const struct timeval*)
{
    return true;
}
bool RMIConnSocket::HandshakeAck(unsigned int, unsigned int)
{
    return true;
}
void RMIConnSocket::KeepAlive()
{
}
bool RMIConnSocket::Flush()
{
    prvt->sock.flush();
    return true;
}
int RMIConnSocket::GetHandle() const
{
    return prvt->sock.get_sock();
}
bool RMIConnSocket::CheckLink()
{
    return prvt->sock.connected();
}

bool RMIConnSocket::Broadcast(unsigned int id, size_t len, const unsigned int* buf)
{
    cache_typ* pbuf = SetListStart(id, len);
    const unsigned int* bufend = buf + len;
    unsigned int* dest = pbuf->buf();
    while (buf != bufend)
    {
        *dest = *buf;
        ++buf;
        ++dest;
    }
    return BroadcastEnd(id, pbuf);
}
bool RMIConnSocket::SetList(unsigned int id, size_t len, const unsigned int* buf)
{
    cache_typ* pbuf = SetListStart(id, len);
    const unsigned int* bufend = buf + len;
    unsigned int* dest = pbuf->buf();
    while (buf != bufend)
    {
        *dest = *buf;
        ++buf;
        ++dest;
    }
    return SetListEnd(id, pbuf);
}
bool RMIConnSocket::Message(unsigned int id, size_t len, const unsigned int* buf)
{
    cache_typ* pbuf = MessageStart(id, len);
    const unsigned int* bufend = buf + len;
    unsigned int* dest = pbuf->buf();
    while (buf != bufend)
    {
        *dest = *buf;
        ++buf;
        ++dest;
    }
    return MessageEnd(id, pbuf);
}
size_t RMIConnSocket::GetList(unsigned int id, size_t len, unsigned int* buf)
{
    const cache_typ* ptr = GetListStart(id);
    if (ptr)
    {
        size_t sz = cache_typ::output(len, buf, ptr);
        GetListEnd(id, ptr);
        return sz;
    }
    return 0;
}

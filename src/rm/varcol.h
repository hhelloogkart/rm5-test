/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _VARCOL_H_
#define _VARCOL_H_

#include "varitem.h"

class rmglobal_typ;
class cache_typ;
struct storage_typ;

class RM_EXPORT var_collection
{
public:
    var_collection(rmglobal_typ& glob, unsigned int sz = 0);
    unsigned int queryid(const char* name);
    unsigned int registerid(const char* name, unsigned int client);
    bool registeridx(unsigned int id, unsigned int client, bool reg);
    notify_typ setlist(unsigned int id, cache_typ* buf);
    cache_typ* getlist(unsigned int id);
    notify_typ getnotify(unsigned int id);
    void clear(unsigned int client);

    // used by TCP server only
    // void linkback(const char *name, unsigned int id, unsigned int client);
    void setonceonly(unsigned int id, unsigned int client);
    bool lastid(unsigned int id);
protected:
    rmglobal_typ& global_sm;
    storage_typ& store;
private:
    static const var_item_typ& dummy();
};

#endif /* _VARCOL_H_ */

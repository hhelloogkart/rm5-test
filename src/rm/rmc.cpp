/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/* HEADER FILES TO BE INCLUDED	*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#include "rmc.h"
#include "rmcdata.h"
#include "rmsocket.h"
#include "rmglobal.h"
#include "cache.h"
#include <util/dbgout.h>
#include <time/timecore.h>
#include <algorithm>

using std::endl;

#define KIV_ISSUE   (0)

/*~~~~~~~~~~~~~~*/
/* DECLARATIONS */
/*~~~~~~~~~~~~~~*/
static virtual_table* vtbl;
static RMBaseSocket* clientsock     = NULL;
static struct timeval select_delay  = {
    0, 100000
};

int find_index(void* var)
{   // Index here refers to the id associated with each RM list.
    // This function is only applicable when SENDING data TO RM.
    // The client would receive both the data and the id from RM.
    int start = 1;
    int end = vtbl[0].regflag - 1;  // Size of virtual table but ignoring the 0-th element

    // Insert error handling to process the scenario when the input argument
    // is larger than the range of the virtual table.
    if (((unsigned long)var >= (unsigned long)((char*)vtbl[end].dest + vtbl[end].size(vtbl[end].dest))) &&
        ((unsigned long)var < (unsigned long)(vtbl[1].dest)))
    {
        return -1;
    }

    while (start != end)
    {
        if (end - start == 1)
        {
            if ((unsigned long)var >= (unsigned long)vtbl[end].dest)
            {
                start = end;
            }
            break;
        }
        else
        {
            int mid = (end + start) / 2;
            if ((unsigned long)vtbl[mid].dest > (unsigned long)var)
            {
                end = mid;
            }
            else if ((unsigned long)var > (unsigned long)vtbl[mid].dest)
            {
                start = mid;
            }
            else
            {
                start = mid;
                break;
            }
        }
    }
    return start;
}

int rm_message_verify(int typ)
{
    // Differeniates the different possible message types:
    // RM_var, RM_message_var, RM_data_var
    // Assumption: Only the 4 least significant bits are defined
    //		+---+---+---+---+---+---+---+---+
    //	bit	| X | X	| X	| X	| 3	| 2 | 1	| 0	|
    //		+---+---+---+---+---+---+---+---+
    // bit 0: Register flag			bit 1: Read-only flag
    // bit 2: RM message flag		bit 3: RM data flag

    if (typ & MSG_FLG_MASK)
    {
        return typ_rm_message_var;
    }
    else if (typ & DATA_FLG_MASK)
    {
        return typ_rm_data_var;
    }
    else
    {
        return typ_rm_var;
    }
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/* FUNCTIONS DEFINITIONS GLOBAL TO ALL MESSAGES	*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

void rm_var_incoming(void)
{
    static bool reentrant = false;
    if (!reentrant)
    {
        reentrant = true;
        if (rm_var_check_connect())
        {
            if(clientsock->Select(&select_delay) < 0)
            {
                clientsock->Close();
            }
        }
        reentrant = false;
    }
}

void rm_var_outgoing(void)
{
    static bool reentrant = false;
    if (!reentrant)
    {
        reentrant = true;
        if (rm_var_check_connect())
        {
            for (int i = 1; i < vtbl[0].regflag; i++)
            {   // Refresh the connection for each element in the virtual table.
                rm_var_refreshRM(vtbl[i].dest);
            }
            clientsock->KeepAlive();
            clientsock->Flush();
        }
        reentrant = false;
    }
}

int rm_var_check_connect(void)
{
    if (clientsock == NULL)
    {
        return false;
    }
    if (!clientsock->Connected())
    {
        if (clientsock->Connect(((connection_info*)vtbl[0].dest)->host, ((connection_info*)vtbl[0].dest)->port))
        {   // Setup connection to RM
            size_t i = 0;
            struct timeval stm = {
                5 + (vtbl[0].regflag / 2), 0
            };

            /*------------------------------*/
            /* Handshaking protocol with RM	*/
            /*------------------------------*/
            clientsock->ClientID(vtbl[0].rmname);                               // Identifies client to RM
            clientsock->FlowControl(((connection_info*)vtbl[0].dest)->flow);    // Sets time to expect messages going to and coming from RM
            for (i = 1; (int)i < vtbl[0].regflag; ++i)
            {
                bool wait_for_reply;
                if (vtbl[i].regflag & REG_FLG_MASK)
                {   // Sends a listname to RM to instruct RM to send updates to this
                    // list whenever there is a change in data
                    wait_for_reply = clientsock->RegisterID(vtbl[i].rmname);
                }
                else
                {   // Sends a listname to RM (No automatic update from RM)
                    wait_for_reply = clientsock->QueryID(vtbl[i].rmname);
                }
                if (wait_for_reply && ((i % 5) == 0))
                {   // Waits for data from RM.
                    // Why is it only executed in multiples of 5?
                    if(clientsock->Select(&select_delay, true) < 0)
                    {
                        clientsock->Close();
                        return false;
                    }
                }
            }

            if (!clientsock->WaitForHandshakeCompletion(&stm))
            {
                dout << "No reply from RM\n" << endl;
                clientsock->Close();
                return false;
            }

        #if (KIV_ISSUE)
            if (connectCB)
            {   // rm_var completes both tasks of connecting to the RM and the handshaking protocol
                (*connectCB)();
            }
        #endif
            return true;
        }
    }
    return true;
}

void rm_var_set_host(const char* host)
{
    if (host)
    {
        rm_strcpy(((connection_info*)vtbl[0].dest)->host, 64, host);
    }
}

void rm_var_set_port(unsigned short port)
{
    ((connection_info*)vtbl[0].dest)->port = port;
}

void rm_var_set_client(const char* client)
{
    if (client)
    {
        rm_strcpy(vtbl[0].rmname, RMNAME_LEN, client);
    }
}

void rm_var_set_flow(unsigned short flow)
{
    ((connection_info*)vtbl[0].dest)->flow = flow;
}

void rm_var_set_wait(unsigned int sec, unsigned int usec)
{
    select_delay.tv_sec = sec;
    select_delay.tv_usec = usec;
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/* FUNCTIONS DEFINITIONS SPECIFIC TO A MESSAGE	*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void rm_var_setRMDirty(void* var)
{
    vtbl[find_index(var)].rmdirty = 1;
}

void rm_var_refreshRM(void* var)
{
    // Ported over from rm_var.cpp, rm_var::refreshRM()
    int vtbl_idx = find_index(var);
    if (vtbl[vtbl_idx].rmdirty)
    {
        int bufsz = (*(vtbl[vtbl_idx].size))(var);
        if (bufsz % sizeof(unsigned int))
        {   // If bufsz = 3 bytes, therefore 3 + 4 - 1 = 6
            // When SetListStart is called later, (bufsz / 4) will truncate 2 bytes at the end
            // and eventually the last data will also be a 4-bytes word.
            bufsz += sizeof(unsigned int) - 1;
        }

        /*----------------------*/
        /* Writing data into RM	*/
        /*----------------------*/
        cache_typ* buf = clientsock->SetListStart(vtbl_idx, bufsz / 4 /* Because RM lists consists of 4-bytes word */); // Request a buffer from RM
        (*(vtbl[vtbl_idx].pack))(var, 1, (unsigned char*)buf->buf() /* Returns a constant pointer to the start of the variable length data */, &bufsz);

        int message_typ = rm_message_verify(vtbl[vtbl_idx].regflag);
        switch (message_typ)
        {
        case typ_rm_var:                // Sets the list into RM
            vtbl[vtbl_idx].rmdirty = !clientsock->SetListEnd(vtbl_idx, buf);
            break;
        case typ_rm_message_var:        // Broadcast the list to RM clients
            vtbl[vtbl_idx].rmdirty = !clientsock->BroadcastEnd(vtbl_idx, buf);
            break;
        case typ_rm_data_var:           // Sets the list into RM, similar to SetListEnd but delivery not guranteed on non-guranteed medium
            vtbl[vtbl_idx].rmdirty = !clientsock->MessageEnd(vtbl_idx, buf);
            break;
        default:
            break;
        }
    }
}

void rm_var_clearDirty(void* var)
{
    vtbl[find_index(var)].dirty = 0;
}

void rm_var_registerIDX(void* var)
{
    int idx = find_index(var);
    clientsock->RegisterIDX(idx);
    vtbl[idx].regflag |= REG_FLG_MASK;
}

void rm_var_unregisterIDX(void* var)
{
    int idx = find_index(var);
    clientsock->UnregisterIDX(idx);
    vtbl[idx].regflag &= ~REG_FLG_MASK;
}

int rm_var_isDirty(void* var)
{   // check whether there is a need to update the data from rm.
    return vtbl[find_index(var)].dirty;
}

int rm_var_getregister(void* var)
{
    return (vtbl[find_index(var)].regflag & REG_FLG_MASK);
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/* EQUIVALENT CALLBACK FUNCTIONS	*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

static void rm_var_doSetList(void*, unsigned int id, const cache_typ* buf)
{
    if ((int)id < vtbl[0].regflag)  // Determine if the id is smaller than the virtual table size
    {
        // 'cache_typ' is the class for the data packet used by RM in order to communicate with its clients.
        // buf therefore contains the RM list which is to be extracted.
        if (buf->get_len() != 0)
        {
            int len = (buf->get_len()) * 4; // Because get_len() returns the number of words (4-bytes), instead of number of bytes
            (*(vtbl[id].extract))(vtbl[id].dest, 1, (unsigned char*)buf->buf() /* Returns a pointer to the start of the variable length data */, &len);
        }
        else
        {
        #if (KIV_ISSUE)
            rmlist()[id-1]->setInvalid();   // No equivalence in virtual table definition
        #endif
        }
        vtbl[id].dirty = 1;
    }
    else
    {
        dout << "Error rx id " << id << " > " << vtbl[0].regflag << endl;
    }
}

static void rm_var_doGetList(void*, unsigned int id, const cache_typ*)
{
    if ((int)id <= vtbl[0].regflag)
    {
        vtbl[id].rmdirty = 1;
    }
    else
    {
        dout << "Error rx id " << id << " > " << vtbl[0].regflag << endl;
    }
}

static int queryid = 1;

static void rm_var_doClientID(void*, unsigned int, const cache_typ* buf)
{
    if(buf->get_len()!=0)
    {
        clientsock->HandshakeAck(CLIENTID, 0);
        queryid = 1;
    }
}

static void rm_var_doQueryID(void*, unsigned int, const cache_typ* buf)
{
    if(buf->get_len()!=0)
    {
        if (queryid >= vtbl[0].regflag)
        {
            dout << "Error rx queryid " << queryid << " > list size\n";
        }
        else if (strcmp((char*)buf->buf(), vtbl[queryid].rmname) != 0)
        {
            dout << "Error rx queryid " << (char*)buf->buf() << " != " << vtbl[queryid].rmname << '\n';
        }
        clientsock->HandshakeAck(QUERYID, queryid);
        ++queryid;
    }
}

static void rm_var_doRegisterID(void*, unsigned int, const cache_typ* buf)
{
    if(buf->get_len()!=0)
    {
        if (queryid >= vtbl[0].regflag)
        {
            dout << "Error rx registerid " << queryid << " > list size\n";
        }
        else if (strcmp((char*)buf->buf(), vtbl[queryid].rmname) != 0)
        {
            dout << "Error rx registerid " << (char*)buf->buf() << " != " << vtbl[queryid].rmname << '\n';
        }
        clientsock->HandshakeAck(REGISTERID, queryid);
        ++queryid;
    }
}

static bool rm_var_lesser(const virtual_table& elem1, const virtual_table& elem2)
{
    return (unsigned long)elem1.dest < (unsigned long)elem2.dest;
}


void rm_var_init(virtual_table* _vtbl)
{
    vtbl = _vtbl;

    // Determine if the RMBaseSocket object has been constructed.
    if (clientsock == NULL)
    {   // Construction of the RMBaseSocket object.
        // Original constructor function: static RMBaseSocket *Construct(const char *)
        // Returns a RMBaseSocket pointer

        clientsock = RMBaseSocket::Construct(((connection_info*)vtbl[0].dest)->access);

        // Refer to data_init(input arguments) in data.c
        // All the information from the input arguments are written into a
        // 'connection_info' struct which is in turn stored in the virtual
        // table, vtbl[0].dest
        // This is why when a RMBaseSocket object needs to be constructed,
        // vtbl[0].dest is passed into the constructor.
        // 'access' will determine the type of connection (ie. TCP, UDP...etc)
    }

    if (clientsock != NULL)
    {
        clientsock->AddCallback(SETLIST,    NULL, &rm_var_doSetList);
        clientsock->AddCallback(BROADCAST,  NULL, &rm_var_doSetList);
        clientsock->AddCallback(MESSAGE,    NULL, &rm_var_doSetList);
        clientsock->AddCallback(GETLIST,    NULL, &rm_var_doGetList);
        clientsock->AddCallback(CLIENTID,   NULL, &rm_var_doClientID);
        clientsock->AddCallback(QUERYID,    NULL, &rm_var_doRegisterID);
        clientsock->AddCallback(REGISTERID, NULL, &rm_var_doQueryID);
    }
    else
    {
        dout << "Initialisation of rm_socket failed\n";
    }

    std::sort(&vtbl[1], &vtbl[vtbl[0].regflag], rm_var_lesser);
}



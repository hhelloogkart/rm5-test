/******************************************************************/
/* Copyright DSO National Laboratories 2004. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "../rmstl.h"
#include <util/dbgout.h>
#include "rmudp.h"
#include <time/timecore.h>
#include <cache.h>
#include "comm/udpcore.h"

#ifndef NO_PCRE
#include <pcre/pcre.h>
#endif
#include <limits.h>
#include <string.h>

#ifdef VXWORKS
#include <sys/socket.h>
#endif

#ifdef NET_OS
#include <threadx/tx_api.h>
#include <threadx/tx_tim.h>
#include <bsp_api.h>
#endif

#define HEADER_SIZE (sizeof(header_typ) / sizeof(unsigned int))
#define HEADER_OFF  ((sizeof(cache_typ) / sizeof(unsigned int)) - HEADER_SIZE)
#define MSG_ID BE((unsigned)0xFF00)
#define CONT_ID BE((unsigned)0x09022005)
#define MSG_NUM 0xFFFF
#define INVALID_SEQUENCE 0x8000
#define WINDOW_LIMIT 8U
#define PACKET_RATE 4.4f

typedef LIST(unsigned int*) BufList_Typ;
typedef LIST(cache_typ*) CBList_Typ;

extern int core_select(int* socks, int socklen, const struct timeval* stm);

struct rmudp_socket_private
{
    udp_core sock;
    unsigned int getid;
    unsigned int valid;
    cache_typ* data_pending;
    unsigned int errflag;
    unsigned int cont_innum;
    unsigned int major_innum;
    unsigned short minor_innum;
    unsigned int major_outnum;
    unsigned int minor_outnum;
    unsigned int flow_interval;
    unsigned int receiver_resend_counter;
    unsigned int receiver_last_major;
    unsigned int sender_resend_counter;
    unsigned int sender_last_major;
    unsigned int notfound_last_major;
    unsigned int notfound_last_counter;
    bool init_flag; //indicate instantiation of application in the beginning
    bool readflag;
    bool store_flag;
    int sync;
    unsigned int packet_window;
    float packet_rate; /* Number of packets per millisec */
    unsigned int last_zero_major_innum_timestamp;
#ifdef NO_THREAD
    timeElapsed in_connect;
#else
    volatile long buffer_lock;
    volatile long queue_list_mutex;
#endif
    timeElapsed recvtime;
    timeElapsed sendtime;
    timeElapsed closetime;
    cache_typ* query_buf;
    BufList_Typ buf_list;
    BufList_Typ queue_list;
    unsigned int* outbuf;
    unsigned int outbuf_sz;
    bool broadcast;
    udp_core* sock2;
    CBList_Typ cb_list;
};

#ifndef NO_THREAD
#include <util/proccore.h>
struct udp_locker
{
    udp_locker(volatile long* lck) : lock(lck)
    {
        while (!atomic_tas(lck))
        {
            context_yield();
        }
    }
    ~udp_locker()
    {
        atomic_untas(lock);
    }
    volatile long* lock;
};
#endif

static bool check_broadcast(const char* host)
{
#ifdef NO_PCRE
    int len = strlen(host);
    int state = 0;
    while(len--)
    {
        if(state == 0 && host[len] == '5')
        {
            state = 1;
        }
        else if(state == 0 && !isspace(host[len]))
        {
            return false;
        }
        else if(state == 1 && host[len] == '5')
        {
            state = 2;
        }
        else if(state == 2 && host[len] == '2')
        {
            state = 3;
        }
        else if(state == 3 && host[len] == '.')
        {
            state = 4;
        }
        else if(state == 4 && isdigit(host[len]))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    return false;
#else
    const char* errptr;
    int erroffset;
    int ovector[3];
    pcre* re = pcre_compile("\\d+\\.\\d+\\.\\d+\\.255$", 0, &errptr, &erroffset, 0);
    if (re == 0)
    {
        dout << "Error compiling RE " << errptr << " at offset " << erroffset << '\n';
        return false;
    }
    const bool result = (pcre_exec(re, 0, host, static_cast<int>(strlen(host)), 0, 0, ovector, 3) >= 0);
    if (pcre_free)
    {
        (*pcre_free)(re);
    }
    else
    {
        free(re);
    }
    return result;
#endif
}

inline bool check_close_interval(const timeElapsed& closetime, const timeElapsed& nowtime)
{
    return closetime.elapsed(nowtime) > 3000;
}

inline bool check_bandwidth_limit(const timeElapsed& sendtime, unsigned int& senddata, const timeElapsed& nowtime, float datalimit)
{
    if (senddata == 0)
    {
        return true;
    }
    const unsigned int interval = sendtime.elapsed(nowtime);
    const unsigned int bw = static_cast<unsigned int>((float)interval /*msec*/ * datalimit /*packets/msec*/);  /*packets*/
    if (senddata > bw)
    {
        return senddata-bw < WINDOW_LIMIT; // allow a 8 frame buffer to build up
    }
    else
    {
        /* all data has been sent, clear datacounter */
        senddata = 0;
        return true;
    }
}

inline void enforce_bandwidth_limit(timeElapsed& sendtime, unsigned int& senddata, const timeElapsed& nowtime, float datalimit)
{
    if (senddata == 0)
    {
        senddata = 1;
    }
    else
    {
        const unsigned int interval = sendtime.elapsed(nowtime);
        const unsigned int bw = static_cast<unsigned int>((float)interval /*msec*/ * datalimit /*packets/msec*/);  /*packets*/
        if (senddata > bw)
        {
            senddata = senddata - bw + 1;
        }
        else
        {
            senddata = 1;
        }
    }
    sendtime = nowtime;
}

inline unsigned int incr_major_num(unsigned int num)
{
    num = (num+1) & 0x7FFF;         // Max num is 0x7FFF
    return (num == 0) ? 1 : num;    // Num is zero only during reset
}

inline unsigned int decr_major_num(unsigned int num)
{
    // No need to mask with 0x7FFF coz smallest value is 0
    return (num == 0) ? 0x7FFF : num-1;  // Num is zero only during reset
}

RMUDPSocket::RMUDPSocket() :
    prvt(new rmudp_socket_private)
{
    prvt->readflag = false;
    cache_typ* pbuf = cache_typ::h_allocate(UDP_BUF);
    prvt->outbuf = pbuf->buf();
    prvt->outbuf[0] = MSG_ID;
    prvt->data_pending = NULL;
    prvt->query_buf = NULL;
    prvt->flow_interval = 0;
    prvt->init_flag = true;
    prvt->store_flag = false;
    prvt->broadcast = false;
    prvt->packet_rate = PACKET_RATE;
    prvt->packet_window = 0;
    prvt->sock2 = NULL;
#ifndef NO_THREAD
    prvt->buffer_lock = 0;
    prvt->queue_list_mutex = 0;
#endif
    ResetData();
    prvt->closetime.clear();
}

RMUDPSocket::RMUDPSocket(const udp_core& asock) :
    prvt(new rmudp_socket_private)
{
    prvt->sock = asock;
    prvt->readflag = false;
    cache_typ* pbuf = cache_typ::h_allocate(UDP_BUF);
    prvt->outbuf = pbuf->buf();
    prvt->outbuf[0] = MSG_ID;
    prvt->data_pending = NULL;
    prvt->query_buf = NULL;
    prvt->flow_interval = 0;
    prvt->init_flag = true;
    prvt->store_flag = false;
    prvt->packet_rate = PACKET_RATE;
    prvt->packet_window = 0;
    prvt->sock2 = NULL;
#ifndef NO_THREAD
    prvt->buffer_lock = 0;
#endif
    ResetData();
    prvt->closetime.clear();
}

RMUDPSocket::~RMUDPSocket()
{
    prvt->sock.close();
    if (prvt->data_pending != NULL)
    {
        prvt->data_pending->h_unreserve();
    }
    if (prvt->query_buf != NULL)
    {
        prvt->query_buf->h_unreserve();
    }

    {
        cache_typ* pbuf = (cache_typ*)prvt->outbuf - 1;
        pbuf->h_unreserve();
    }

    for (BufList_Typ::iterator iter = prvt->buf_list.begin(); iter != prvt->buf_list.end(); ++iter)
    {
        cache_typ* pbuf = (cache_typ*)(*iter) - 1;
        pbuf->h_unreserve();
    }
    for (BufList_Typ::iterator iter = prvt->queue_list.begin(); iter != prvt->queue_list.end(); ++iter)
    {
        cache_typ* pbuf = (cache_typ*)(*iter) - 1;
        pbuf->h_unreserve();
    }
    for (CBList_Typ::iterator iter1 = prvt->cb_list.begin(); iter1 != prvt->cb_list.end(); ++iter1)
    {
        (*iter1)->h_unreserve();
    }
    delete prvt;
}

bool RMUDPSocket::Connect(const char* host, int port, const struct timeval* deftv)
{
    prvt->broadcast = check_broadcast(host);
    if ((prvt->sync == 0) || !prvt->sock.connected())
    {
        if (prvt->broadcast)
        {
            prvt->sock.sethost(host, port);
            prvt->sock.bind();
            prvt->sync = 1;
            prvt->errflag = 0;
            prvt->recvtime();
            KeepAlive();
            return true;
        }
        else
        {
            int idxsemi0 = -1;
            int idxsemi1 = -1;

            for (int cnt = 0; host[cnt] != '\0'; ++cnt)
            {
                if (idxsemi0 == -1)
                {
                    if (*(host + cnt) == ';')
                    {
                        idxsemi0 = cnt;
                    }
                }
                else if (*(host + cnt) == ';')
                {
                    idxsemi1 = cnt;
                    break;
                }
            }

            if (idxsemi0 == -1)
            {
                prvt->sock.sethost(host, port);
                prvt->sock.bind();
            }
            else if (idxsemi1 == -1)
            {
                char* stop = 0;
                const int sz = static_cast<int>(strlen(host));
                const char* temp = host + idxsemi0 + 1;
                const float i = strtof(temp, &stop);

                while (stop != host + sz)
                {
                    if ((*stop == ' ') || (*stop == '\t'))
                    {
                        stop++;
                    }
                    else
                    {
                        break;
                    }
                }

                char* hostptr = new char[idxsemi0 + 1]; //add 1 for null
                rm_strncpy(hostptr, idxsemi0 + 1, host, idxsemi0);
                hostptr[idxsemi0] = '\0';
                prvt->sock.sethost(hostptr, port);
                prvt->sock.bind();
                delete[] hostptr;

                if (stop != host + sz)
                {
                    if (prvt->sock2 == NULL)
                    {
                        prvt->sock2 = new udp_core;
                    }
                    prvt->sock2->sethost(host + idxsemi0 + 1, port); //add 1 for 1 position after the semi-colon
                    prvt->sock2->bind();
                }
                else
                {
                    prvt->packet_rate = (i > 0) ? i : PACKET_RATE;
                }
            }
            else
            {
                const int slot1sz = idxsemi1 - idxsemi0 - 1;
                const int cnt = idxsemi0 > slot1sz ? idxsemi0 : slot1sz;
                char* hostptr = new char[cnt + 1]; //add 1 for null
                rm_strncpy(hostptr, cnt + 1, host, idxsemi0);
                hostptr[idxsemi0] = '\0';
                prvt->sock.sethost(hostptr, port);
                prvt->sock.bind();
                rm_strncpy(hostptr, cnt + 1, host + idxsemi0 + 1, slot1sz);
                hostptr[slot1sz] = '\0';
                if (prvt->sock2 == NULL)
                {
                    prvt->sock2 = new udp_core;
                }
                prvt->sock2->sethost(hostptr, port); //add 1 for 1 position after the semi-colon
                prvt->sock2->bind();
                char* stop = 0;
                const float i = strtof(host + idxsemi1 + 1, &stop);
                prvt->packet_rate = (i > 0) ? i : PACKET_RATE;
                delete[] hostptr;
            }
            prvt->errflag = 0;
            InConnectSendReset();
            if (prvt->flow_interval == 0)
            {
                prvt->sync = 1;
            }
#ifdef NO_THREAD
            prvt->in_connect();
#endif
        }
    }
    else if (prvt->sync == 2)
    {
        InConnectSendReset();
#ifdef NO_THREAD
        prvt->in_connect();
#endif
    }

#ifdef NO_THREAD
    unsigned flow_interval = (prvt->flow_interval) ? prvt->flow_interval : 15;
    struct timeval tv = {
        0, 10000
    };
#else
    struct timeval tv = {
        5, 0
    };
#endif
    if (deftv)
    {
        tv = *deftv;
    }
    InConnectSelect(&tv);
    if (prvt->sync == 1)
    {
        return true;
    }
#ifdef NO_THREAD
    else if ((prvt->sync == -1) &&
             (prvt->in_connect.elapsed() > flow_interval*500U)) // sec before resend reset = flow / 2
    {
        prvt->sync = prvt->sock.connected() ? 2 : 0;
    }
#else
    else if (prvt->sync==-1)
    {
        prvt->sync = (prvt->errflag >= ERR_TOLERANCE) ? 0 : 2;
#if DEBUG
        dout << "RMUDP: End of connect. Sync set to " << prvt->sync << ", errorflag=" << prvt->errflag << '\n';
#endif
    }
#endif
    return false;
}

bool RMUDPSocket::Connected()
{
    if (!prvt->sock.connected())
    {
        return false;
    }
    if (prvt->sync == 1)
    {
        return true;
    }
    return false;
}

bool RMUDPSocket::Close()
{
#if DEBUG
    dout << "RMUDP: Socket closed\n";
#endif
    prvt->closetime(); // Inhibit Ack from clearing the buffer or sending data for x secs
    ResetData();
    if (prvt->sock2)
    {
        return prvt->sock.close() && prvt->sock2->close();
    }
    return prvt->sock.close();
}

void RMUDPSocket::ResetData()
{
#ifdef NO_THREAD
    prvt->in_connect.clear();
#endif
    prvt->sync = 0;
    prvt->getid = UINT_MAX;
    prvt->errflag = 0;
    prvt->valid = 0;
    prvt->sender_last_major = INVALID_SEQUENCE;
    prvt->sender_resend_counter = INVALID_SEQUENCE;
    prvt->receiver_last_major = INVALID_SEQUENCE;
    prvt->receiver_resend_counter = INVALID_SEQUENCE;
    prvt->notfound_last_major = 0;
    prvt->notfound_last_counter = 0;

    if (prvt->query_buf != NULL)
    {
        prvt->query_buf->h_unreserve();
        prvt->query_buf = NULL;
    }
    if(prvt->store_flag)
    {
        cache_typ* pbuf = (cache_typ*)prvt->outbuf - 1;
        //miniprintf("SendPacket::Block %d, length %d\n", prvt->major_outnum, prvt->outbuf_sz);
        pbuf->set_len(prvt->outbuf_sz);
        pbuf->h_reserve();
        prvt->buf_list.push_back(prvt->outbuf);
        pbuf = cache_typ::h_allocate(UDP_BUF);
        prvt->outbuf = pbuf->buf();
        prvt->outbuf[0] = MSG_ID;
    }
    prvt->store_flag = false;
    prvt->recvtime.clear();
    prvt->sendtime.clear();
    unsigned int cnt = 0;
    {
#ifndef NO_THREAD
        udp_locker lock(&prvt->queue_list_mutex);
#endif
        for (BufList_Typ::iterator iter = prvt->queue_list.begin(); iter != prvt->queue_list.end(); ++iter)
        {
            cache_typ* pbuf = (cache_typ*)(*iter) - 1;
            pbuf->h_unreserve();
        }
        prvt->queue_list.clear();
        for (BufList_Typ::iterator iter = prvt->buf_list.begin(); iter != prvt->buf_list.end(); ++iter)
        {
            *(*iter) = BE(cnt);
            cache_typ* pbuf = (cache_typ*)(*iter) - 1;
            pbuf->h_reserve();
            prvt->queue_list.push_back(*iter);
            cnt = incr_major_num(cnt);
        }
    }
    prvt->major_innum = 0;
    prvt->minor_innum = 0;
    prvt->major_outnum = cnt;
    prvt->minor_outnum = 0;
    for (CBList_Typ::iterator iter1 = prvt->cb_list.begin(); iter1 != prvt->cb_list.end(); ++iter1)
    {
        (*iter1)->h_unreserve();
    }
    prvt->cb_list.clear();
    prvt->outbuf_sz = 1;
    if (prvt->data_pending != NULL)
    {
        prvt->data_pending->h_unreserve();
        prvt->data_pending = NULL;
    }
    prvt->packet_window = 0;
    prvt->last_zero_major_innum_timestamp = 0;
}

bool RMUDPSocket::FlowControl(unsigned int interval)
{
    prvt->flow_interval = interval;
    cache_typ* pbuf = SetListStart(0, 0);
    pbuf->set_id(interval);
    pbuf->set_seq(MSG_NUM);
    pbuf->set_cmd(FLOWCONTROL);
    return PackMessage(pbuf);
}
int RMUDPSocket::Select(const struct timeval* stm, bool defercb)
{
    unsigned int cmd;
    CBList_Typ::iterator it;

    if (prvt->sync == 1)
    {
        const timeElapsed nowtime(true);
        bool keepalive = false;
        {
#ifndef NO_THREAD
            udp_locker lock(&prvt->queue_list_mutex);
#endif
            if (!prvt->queue_list.empty() && check_bandwidth_limit(prvt->sendtime, prvt->packet_window, nowtime, prvt->packet_rate) && check_close_interval(prvt->closetime, nowtime))
            {
                unsigned int* obuf = prvt->queue_list.front();
                cache_typ* pbuf1 = (cache_typ*)obuf - 1;
                int plen = pbuf1->get_len() * sizeof(unsigned int);
                if (prvt->sock.send((char*)obuf, plen) == plen)
                {
                    pbuf1->h_unreserve();
                    prvt->queue_list.pop_front();
                    enforce_bandwidth_limit(prvt->sendtime, prvt->packet_window, nowtime, prvt->packet_rate);
                }
            }
            else
            {
                keepalive = true;
            }
        }
        if (keepalive)
        {
            KeepAlive();
        }
    }
    if (!defercb)
    {
        while (prvt->cb_list.size())
        {
            it = prvt->cb_list.begin();
            cmd = (*it)->get_cmd();
            if (cmd < ENDCMD)
            {
                (*(callback[cmd]))(callback_arg[cmd], (*it)->get_id(), *it);
            }
            const_cast<cache_typ*>(*it)->h_unreserve();
            prvt->cb_list.pop_front();
        }
    }
    int ret = (stm == NULL) ? 1 : prvt->sock.select(stm);
    if (ret > 0)
    {
        ret = 1;
        ReceivePacket(defercb);
        while (prvt->sock.chk_pending() && (prvt->errflag < ERR_TOLERANCE))
        {
            ReceivePacket(defercb);
        }
        if (prvt->errflag >= ERR_TOLERANCE)
        {
            dout << "RMDUP: Error tolerance exceeded; select returns -1\n";
            return -1;
        }
    }
    else if(ret < 0)
    {
        dout << "RMUDP: Socket returns error, select returns -1\n";
        return -1;
    }
    else if(!CheckLink())
    {
        dout << "RMUDP: Link timeout exceeded, select returns -1\n";
        return -1;
    }
    if (prvt->sync == 1)
    {
        const timeElapsed nowtime(true);
        if( !prvt->queue_list.empty() )
        {
#ifndef NO_THREAD
            udp_locker lock(&prvt->queue_list_mutex);
#endif
            for (int cnt = 0; (cnt < 3) && !prvt->queue_list.empty() && check_bandwidth_limit(prvt->sendtime, prvt->packet_window, nowtime, prvt->packet_rate) && check_close_interval(prvt->closetime, nowtime); ++cnt)
            {
                unsigned int* obuf = prvt->queue_list.front();
                cache_typ* pbuf1 = (cache_typ*)obuf - 1;
                int plen = pbuf1->get_len() * sizeof(unsigned int);
                if (prvt->sock.send((char*)obuf, plen) == plen)
                {
                    pbuf1->h_unreserve();
                    prvt->queue_list.pop_front();
                    enforce_bandwidth_limit(prvt->sendtime, prvt->packet_window, nowtime, prvt->packet_rate);
                }
            }
        }
        else if ( !prvt->buf_list.empty() && check_bandwidth_limit(prvt->sendtime, prvt->packet_window, nowtime, prvt->packet_rate) && check_close_interval(prvt->closetime, nowtime))
        {
            unsigned int* obuf = prvt->buf_list.back();
            cache_typ* pbuf1 = (cache_typ*)obuf - 1;
            int plen = pbuf1->get_len() * sizeof(unsigned int);
            if (prvt->sock.send((char*)obuf, plen) == plen)
            {
                enforce_bandwidth_limit(prvt->sendtime, prvt->packet_window, nowtime, prvt->packet_rate);
            }
        }
        else
        {
            KeepAlive();
        }
    }
    return ret;
}

size_t RMUDPSocket::GetList(unsigned int id, size_t len, unsigned int* buf)
{
    const cache_typ* ptr = GetListStart(id);
    if(ptr)
    {
        size_t sz = ptr->output(len, buf);
        GetListEnd(id, ptr);
        return sz;
    }
    return 0;
}

const cache_typ* RMUDPSocket::GetListStart(unsigned int id)
{
    int cnt = 0, ret;
    if (prvt->sync != 1)
    {
        return NULL;
    }

    cache_typ* pbuf = SetListStart(0, 0);
    pbuf->set_id(id);
    pbuf->set_seq(MSG_NUM);
    pbuf->set_cmd(GETLIST);
    PackMessage(pbuf);
    Flush();
    prvt->getid = id;
    do
    {
        if (cnt != 0)
        {
            Sleep(WAIT_TIME);
        }
        struct timeval tm = {
            0, WAIT_TIME*1000
        };
        ret = Select(&tm, true);
        ++cnt;
    }
    while ((prvt->query_buf == NULL) && (cnt < GETLIST_RETRY) && (ret != -1));
    cache_typ* ptr = prvt->query_buf;
    prvt->getid = UINT_MAX;
    prvt->query_buf = NULL;
    return ptr;
}

void RMUDPSocket::GetListEnd(unsigned int, const cache_typ* pbuf)
{
    // need to check whether the pbuf is real or fake
    if ((pbuf->packet() - prvt->outbuf) < 0 ||
        (pbuf->packet() - prvt->outbuf >= UDP_BUF * sizeof(unsigned int)))
    {
        const_cast<cache_typ*>(pbuf)->h_unreserve();
    }
}

cache_typ* RMUDPSocket::MessageStart(unsigned int id, size_t len)
{
    return SetListStart(id, len);
}

cache_typ* RMUDPSocket::SetListStart(unsigned int, size_t len)
{
    if ((int)len <= UDP_BUF - 3 - (int)prvt->outbuf_sz)
    {
        prvt->outbuf[prvt->outbuf_sz] = BE(MAGICNUM);
        cache_typ* pbuf = (cache_typ*)((char*)&prvt->outbuf[prvt->outbuf_sz] - sizeof(cache_typ) + sizeof(header_typ));
        pbuf->set_len(static_cast<unsigned int>(len));
        return pbuf;
    }
    else if (len <= UDP_BUF - 4)
    {
        Flush();
        prvt->outbuf[prvt->outbuf_sz] = BE(MAGICNUM);
        cache_typ* pbuf = (cache_typ*)((char*)&prvt->outbuf[prvt->outbuf_sz] - sizeof(cache_typ) + sizeof(header_typ));
        pbuf->set_len(static_cast<unsigned int>(len));
        return pbuf;
    }
    return cache_typ::h_allocate(len);
}

bool RMUDPSocket::SetListEnd(unsigned int id, cache_typ* pbuf)
{
    bool ret;
    pbuf->set_id(id);
    pbuf->set_cmd(SETLIST);
    pbuf->set_seq(prvt->minor_outnum);
    prvt->store_flag = true;

    ret = PackMessage(pbuf);
    if (ret)
    {
        ++(prvt->minor_outnum);
        prvt->minor_outnum &= 0x7FFF;
    }
    return ret;
}

bool RMUDPSocket::SetList(unsigned int id, size_t len, const unsigned int* buf)
{
    cache_typ* pbuf = SetListStart(id, len);
    const unsigned int* bufend = buf + len;
    unsigned int* dest = pbuf->buf();
    while (buf != bufend)
    {
        *dest = *buf;
        ++buf;
        ++dest;
    }
    return SetListEnd(id, pbuf);
}

bool RMUDPSocket::MessageEnd(unsigned int id, cache_typ* pbuf, void* addr)
{
    if (addr != NULL)
    {
        // check addr, and flush if different
        if (!prvt->sock.compare_addr(*reinterpret_cast<struct sockaddr_in*>(addr), 1))
        {
            Flush();
            // set new address
            prvt->sock.sethost(*reinterpret_cast<struct sockaddr_in*>(addr), 1);
        }
    }

    pbuf->set_id(id);
    pbuf->set_cmd(MESSAGE);
    pbuf->set_seq(MSG_NUM);
    return PackMessage(pbuf);
}

bool RMUDPSocket::Message(unsigned int id, size_t len, const unsigned int* buf)
{
    cache_typ* pbuf = MessageStart(id, len);
    const unsigned int* bufend = buf + len;
    unsigned int* dest = pbuf->buf();
    while (buf != bufend)
    {
        *dest = *buf;
        ++buf;
        ++dest;
    }
    return MessageEnd(id, pbuf);
}

bool RMUDPSocket::BroadcastEnd(unsigned int id, cache_typ* pbuf)
{
    bool ret;
    pbuf->set_id(id);
    pbuf->set_cmd(BROADCAST);
    pbuf->set_seq(prvt->minor_outnum);
    prvt->store_flag = true;

    ret = PackMessage(pbuf);
    if (ret)
    {
        ++(prvt->minor_outnum);
        prvt->minor_outnum &= 0x7FFF;
    }
    return ret;
}

bool RMUDPSocket::Broadcast(unsigned int id, size_t len, const unsigned int* buf)
{
    cache_typ* pbuf = SetListStart(id, len);
    const unsigned int* bufend = buf + len;
    unsigned int* dest = pbuf->buf();
    while (buf != bufend)
    {
        *dest = *buf;
        ++buf;
        ++dest;
    }
    return BroadcastEnd(id, pbuf);
}

bool RMUDPSocket::Acknowledge(unsigned int seq)
{
#if DEBUG
    miniprintf("RMUDP: Acknowledge::Block %d\n", seq);
#endif
    cache_typ* pbuf = SetListStart(0, 0);
    pbuf->set_id(0);
    pbuf->set_seq(seq);
    pbuf->set_cmd(ACKNOWLEDGE);
    return PackMessage(pbuf);
}

bool RMUDPSocket::Resend(unsigned short seq, unsigned short minor_seq)
{
    cache_typ* pbuf = SetListStart(0, 0);
    pbuf->set_id(minor_seq);
    pbuf->set_seq(seq);
    pbuf->set_cmd(RESEND);
    return PackMessage(pbuf);
}

void RMUDPSocket::KeepAlive( )
{
    if ((prvt->sync != 1) || (prvt->flow_interval==0) ||
        (prvt->sendtime.elapsed() < prvt->flow_interval * 500U))
    {
        return;
    }
    cache_typ* pbuf = SetListStart(0, 0);
    pbuf->set_id(0);
    pbuf->set_seq(MSG_NUM);
    pbuf->set_cmd(ALIVE);
    PackMessage(pbuf);
    Flush();
}

bool RMUDPSocket::RegisterIDX(unsigned int)
{
    return true;
}

bool RMUDPSocket::UnregisterIDX(unsigned int)
{
    return true;
}

bool RMUDPSocket::RegisterID(const char*)
{
    return false;
}

bool RMUDPSocket::QueryID(const char*)
{
    return false;
}

bool RMUDPSocket::QueryID(const char*[], int)
{
    return false;
}

bool RMUDPSocket::ClientID(const char* client)
{
    if(prvt->init_flag) //Send CLIENTID message if this is 1st time this udpsocket is instantiated
    {
        prvt->init_flag = false;
        cache_typ* pbuf = SetListStart(0, (RMNAME_LEN/sizeof(unsigned)));
        pbuf->set_id(0);
        pbuf->set_seq(MSG_NUM);
        pbuf->set_cmd(CLIENTID);
        char* data = (char*)pbuf->buf();
        rm_strncpy(data, RMNAME_LEN, client, RMNAME_LEN-1);
        data[RMNAME_LEN-1] = '\0';
        PackMessage(pbuf);
    } //if init_flag
    return false;
}

bool RMUDPSocket::PackMessage(cache_typ* pbuf)
{
#ifndef NO_THREAD
    udp_locker lock(&prvt->buffer_lock);
#endif
    if( ((prvt->sync == 1) && (prvt->buf_list.size() < 1000)) || (pbuf->get_cmd() == RESET) )
    {
        // check to see if the pbuf is actually outbuf
        if ((pbuf->packet() - prvt->outbuf >= 0) &&
            (pbuf->packet() - prvt->outbuf < UDP_BUF * sizeof(unsigned int)))
        {
            if (pbuf->packet() == &prvt->outbuf[prvt->outbuf_sz])
            {
                // let's just increase outbuf_sz
                prvt->outbuf_sz += HEADER_SIZE + pbuf->get_len();
            }
            else
            {
                // case where a flush was called in between Start and End
                // check if there is enough to pack it in
                if ((pbuf->get_len() + HEADER_SIZE + prvt->outbuf_sz) > UDP_BUF)
                {
                    Flush();
                }

                unsigned int* pbuf_start = pbuf->packet();
                unsigned int* pbuf_end = pbuf_start + pbuf->get_len() + HEADER_SIZE;
                while (pbuf_start != pbuf_end)
                {
                    prvt->outbuf[prvt->outbuf_sz] = *pbuf_start;
                    ++prvt->outbuf_sz;
                    ++pbuf_start;
                }
            }
        }
        // pbuf is not outbuf
        else
        {
            // the length must exceed outbuf size so let's try to squeeze whatever we can
            unsigned int leftover = pbuf->get_len() + prvt->outbuf_sz + HEADER_SIZE - UDP_BUF;
            prvt->store_flag = true; // long messages MUST have a block ID
            unsigned int* pbuf_start;
            for (pbuf_start = pbuf->packet(); prvt->outbuf_sz < UDP_BUF; ++pbuf_start, ++prvt->outbuf_sz)
            {
                prvt->outbuf[prvt->outbuf_sz] = *pbuf_start;
            }
            Flush();
            // when we pack the rest into packets
            while (leftover >= UDP_BUF-2)
            {
                prvt->store_flag = true;
                prvt->outbuf[1] = CONT_ID;
                unsigned int* ob = prvt->outbuf + 2;
                unsigned int* ob_end = prvt->outbuf + UDP_BUF;
                for (; ob != ob_end; ++ob, ++pbuf_start)
                {
                    *ob = *pbuf_start;
                }
                prvt->outbuf_sz = UDP_BUF;
                leftover -= UDP_BUF-2;
                Flush();
            }
            // finally, we have the last, unfilled packet
            prvt->store_flag = true;
            prvt->outbuf_sz += leftover + 1;
            prvt->outbuf[1] = CONT_ID;
            for (unsigned int* ob1 = prvt->outbuf + 2; leftover > 0; ++pbuf_start, ++ob1, --leftover)
            {
                *ob1 = *pbuf_start;
            }
        }

        // there isn't enough left for even one more packet
        if (prvt->outbuf_sz + HEADER_SIZE > UDP_BUF)
        {
            Flush();
        }
        return true;
    }
    else
    {
        // reinstate and copy
        GetListEnd(0, pbuf);
        return false;
    }
}

void RMUDPSocket::ReceivePacket(bool defercb)
{
    unsigned int length;             // Length of good packet
    unsigned int begin;              // Location of unprocessed
    unsigned int id;                 // ID of good packet
    unsigned int cmd;                // Command of good packet
    unsigned int cnt1;               // Misc counters
    int result;                      // Read result
    unsigned int big_buf[UDP_BUF+HEADER_OFF];    // Temporary input buffer
    unsigned int* in_buf = &big_buf[HEADER_OFF]; // Offset to allow convert to Cache to work
    bool outofsync;

    if (prvt->readflag)
    {
        return;
    }
    prvt->readflag = true;

    // 1. receive one packet
    result = prvt->sock.recv((char*)in_buf, UDP_BUF*sizeof(unsigned int));
    if (result >= 4)
    {
        if(prvt->sync != 1)
        {
            prvt->sync = 1;
            cache_typ* pbuf = SetListStart(0, 0);
            pbuf->set_id(0);
            pbuf->set_seq(MSG_NUM);
            pbuf->set_cmd(ALIVE);
            PackMessage(pbuf);
            Flush();
        }
    }
    else
    {
        goto receive_error;
    }

    // 2. check big packet header
    if (in_buf[0] != MSG_ID) // guaranteed block
    {
        if (in_buf[0] != BE(prvt->major_innum)) // big packet number mismatched
        {
            outofsync = true;

            // check for possibility of a reset
            if (in_buf[0] == 0)
            {
                unsigned int ntimestamp = now_sec();
                if ((prvt->last_zero_major_innum_timestamp == 0) ||
                    (prvt->major_innum != 1) ||
                    (ntimestamp > prvt->last_zero_major_innum_timestamp+1))
                {
                    Acknowledge(0);
                    prvt->major_innum = 1;
                    prvt->minor_innum = 0;
                    prvt->receiver_last_major = INVALID_SEQUENCE;
                    outofsync = false;
                    prvt->last_zero_major_innum_timestamp = ntimestamp;
                    goto receive_step3;
                }
                else
                {
                    Acknowledge(0);
                    prvt->last_zero_major_innum_timestamp = ntimestamp;
                    goto receive_exit;
                }
            }
            else
            {
                prvt->last_zero_major_innum_timestamp = 0;
                // check to see if we are receiving historical packets
                unsigned int decr = decr_major_num(prvt->major_innum);
                for (cnt1 = 0; cnt1 < 1024; ++cnt1, decr = decr_major_num(decr))
                {
                    if (in_buf[0] == BE(decr))
                    {
                        Acknowledge(decr_major_num(prvt->major_innum));
                        if (in_buf[1] != CONT_ID)
                        {
                            goto receive_step3;
                        }
                        else
                        {
                            goto receive_exit;
                        }
                    }
                }
            }

            if( prvt->receiver_last_major == INVALID_SEQUENCE)
            {
                miniprintf("ReceivePacket::RESEND CMD is SENT for packet [%d], received [%d]\n", prvt->major_innum, BE(in_buf[0]));
                prvt->receiver_resend_counter = 0;
            }
            else if(BE(in_buf[0]) <= prvt->receiver_last_major)
            {
                miniprintf("ReceivePacket::RESEND CMD is SENT for packet [%d], received [%d]\n", prvt->major_innum, BE(in_buf[0]));
                prvt->receiver_resend_counter = ++(prvt->receiver_resend_counter)&0x7FFF;
            }
            Resend(prvt->major_innum, prvt->receiver_resend_counter);
            prvt->receiver_last_major = BE(in_buf[0]);
            if (in_buf[1] == CONT_ID)
            {
                goto receive_exit;
            }
        }
        else
        {
            //miniprintf("ReceivePacket::Block %d, length %d\n", prvt->major_innum, result / 4);
            Acknowledge(prvt->major_innum);
            prvt->major_innum = incr_major_num(prvt->major_innum);
            prvt->receiver_last_major = INVALID_SEQUENCE;
            outofsync = false;
            prvt->last_zero_major_innum_timestamp = (in_buf[0] == 0) ? now_sec() : 0;
        }
    }
    else
    {
        //miniprintf("ReceivePacket::Non-guaranteed block, length %d\n", result / 4);
        outofsync = false;
    }
receive_step3:
    prvt->recvtime();

    // 3. check for special reset packet
    if ((in_buf[0] == MSG_ID) &&
        (BE(in_buf[2]) >> 24 == RESET))
    {
#if DEBUG
        dout << "RMUDP: Recv RESET\n";
#endif
#ifndef NO_THREAD
        udp_locker lock(&prvt->buffer_lock);
#endif
        prvt->getid = UINT_MAX;
        prvt->valid = 0;
        if (prvt->query_buf != NULL)
        {
            prvt->query_buf->h_unreserve();
            prvt->query_buf = NULL;
        }
        if(prvt->store_flag)
        {
            cache_typ* pbuf = (cache_typ*)prvt->outbuf - 1;
            // miniprintf("SendPacket::Block %d, length %d\n", prvt->major_outnum, prvt->outbuf_sz);
            pbuf->set_len(prvt->outbuf_sz);
            pbuf->h_reserve();
            prvt->buf_list.push_back(prvt->outbuf);
            pbuf = cache_typ::h_allocate(UDP_BUF);
            prvt->outbuf = pbuf->buf();
            prvt->outbuf[0] = MSG_ID;
        }
        prvt->outbuf_sz = 1;
        prvt->store_flag = false;
        unsigned int cnt = 0;
        {
#ifndef NO_THREAD
            udp_locker lock1(&prvt->queue_list_mutex);
#endif
            for (BufList_Typ::iterator iter = prvt->queue_list.begin(); iter != prvt->queue_list.end(); ++iter)
            {
                cache_typ* pbuf = (cache_typ*)(*iter) - 1;
                pbuf->h_unreserve();
            }
            prvt->queue_list.clear();
            for (BufList_Typ::iterator iter = prvt->buf_list.begin(); iter != prvt->buf_list.end(); ++iter)
            {
                *(*iter) = BE(cnt);
                cache_typ* pbuf = (cache_typ*)(*iter) - 1;
                pbuf->h_reserve();
                prvt->queue_list.push_back(*iter);
                cnt = incr_major_num(cnt);
            }
        }
        prvt->major_innum = 0;
        prvt->minor_innum = 0;
        prvt->major_outnum = cnt;
        prvt->minor_outnum = 0;
        for (CBList_Typ::iterator iter1 = prvt->cb_list.begin(); iter1 != prvt->cb_list.end(); ++iter1)
        {
            (*iter1)->h_unreserve();
        }
        prvt->cb_list.clear();
        if (prvt->data_pending != NULL)
        {
            prvt->data_pending->h_unreserve();
            prvt->data_pending = NULL;
        }
        prvt->readflag = false;
        prvt->receiver_last_major = INVALID_SEQUENCE;
        prvt->receiver_resend_counter = INVALID_SEQUENCE;
        prvt->sender_last_major = INVALID_SEQUENCE;
        prvt->sender_resend_counter = INVALID_SEQUENCE;
        prvt->last_zero_major_innum_timestamp = 0;
        prvt->closetime();
        prvt->errflag += 2; /* Receiving resets many times in a row is a bad thing */
        return;
    }

    // 4. check if we need to join to previous packet
    if ((prvt->data_pending != NULL) && !outofsync && (in_buf[0] == BE(prvt->cont_innum)) && (in_buf[1] == CONT_ID))
    {
        int cpylen = prvt->data_pending->get_len()*sizeof(unsigned int) - prvt->valid;
        int pktsz = static_cast<int>(result - 2*sizeof(unsigned int));
        if (cpylen > pktsz)
        {
            // still not long enough
            memcpy(((char*)prvt->data_pending->buf())+prvt->valid, &in_buf[2], pktsz);
            prvt->valid += pktsz;
            prvt->cont_innum = incr_major_num(prvt->cont_innum);
            prvt->readflag = false;
            return;
        }
        else
        {
            memcpy(((char*)prvt->data_pending->buf())+prvt->valid, &in_buf[2], cpylen);
            if (prvt->data_pending->get_id() == prvt->getid)
            {
                prvt->query_buf = prvt->data_pending;
            }
            else if (defercb)
            {
                prvt->cb_list.push_back(prvt->data_pending);
            }
            else
            {
                (*(callback[prvt->data_pending->get_cmd()]))(callback_arg[prvt->data_pending->get_cmd()], prvt->data_pending->get_id(), prvt->data_pending);
                prvt->data_pending->h_unreserve();
            }
            prvt->data_pending = NULL;
            prvt->valid = pktsz - cpylen;
            begin = 2 + (cpylen / sizeof(unsigned int));
        }
    }
    else
    {
        begin = 1;
    }
    result /= sizeof(unsigned int);

    // 5. normal processing of packets, begin=array location in in_buf to start, result=array location after the end
    while ((unsigned int)result >= HEADER_SIZE + begin)
    {
        if (in_buf[begin] == BE(MAGICNUM))
        {
            // Magic Number OK
            cnt1 = BE(in_buf[begin+2]) >> 16;
            cmd = BE(in_buf[begin + 1]) >> 24;
            id  = BE(in_buf[begin + 1]) & 0xFFFFFF;

            if (cmd >= ENDCMD)
            {
                ++begin;
                continue;
            }
            if ((((cmd == SETLIST) || (cmd == BROADCAST)) && ((cnt1 == prvt->minor_innum) || (prvt->minor_innum == 0) || (cnt1 == 0))) ||
                ((cmd == MESSAGE) && (cnt1 == MSG_NUM)) || ((cmd == CLIENTID) && (cnt1 == MSG_NUM)))
            {
                if (!outofsync && ((cmd == SETLIST) || (cmd == BROADCAST)))
                {
                    if (cnt1 == prvt->minor_innum)
                    {
                        // Frame Count OK
                        prvt->minor_innum = ++(prvt->minor_innum) & 0x7FFF;
                        prvt->errflag = 0;
                    }
                    else if ((prvt->minor_innum == 0) || (cnt1 == 0))
                    {
                        prvt->minor_innum = ++(cnt1) & 0x7FFF;
                        prvt->errflag = 0;
                    }
                }
                else if (cmd == MESSAGE)
                {
                    prvt->errflag = 0;
                }
                length = BE(in_buf[begin+2]) & 0xFFFF;
                if ((result - begin) >= (length+HEADER_SIZE))
                {
                    if (!outofsync || (cmd == MESSAGE) || (cmd == CLIENTID))
                    {
                        if (id == prvt->getid)
                        {
                            prvt->query_buf = cache_typ::h_duplicate(id, length, in_buf+begin+HEADER_SIZE);
                        }
                        else if (defercb)
                        {
                            cache_typ* p = cache_typ::h_duplicate(id, length, in_buf+begin+HEADER_SIZE);
                            p->set_cmd(cmd);
                            prvt->cb_list.push_back(p);
                        }
                        else
                        {
                            cache_typ* p = cache_typ::ConvertToCache(&in_buf[begin]);
                            (*(callback[cmd]))(callback_arg[cmd], id, p);
                        }
                    }
                    begin += HEADER_SIZE + length;
                }
                else
                {
                    if (!outofsync)
                    {
                        prvt->data_pending = cache_typ::h_allocate(length);
                        prvt->data_pending->copy_header(&(in_buf[begin]));
                        prvt->valid = (result - begin - HEADER_SIZE) * sizeof(unsigned int);
                        //miniprintf("Valid = %d, Pending length = %d\n", prvt->valid, length*4);
                        memcpy(prvt->data_pending->buf(), &(in_buf[begin+HEADER_SIZE]), prvt->valid);
                        prvt->cont_innum = prvt->major_innum;
                    }
                    prvt->readflag = false;
                    return;
                }
            }
            else if (cmd == ACKNOWLEDGE)
            {
                if (check_close_interval(prvt->closetime, prvt->recvtime))
                {
#ifndef NO_THREAD
                    udp_locker lock(&prvt->buffer_lock);
#endif
                    if (id != 0)
                    {
                        ++begin;
                        continue;
                    }
#if DEBUG
                    dout << "RMUDP: Ack " << cnt1 << " Clearing ";
#endif
                    bool more = false;
                    for(BufList_Typ::iterator iter = prvt->buf_list.begin(); (iter != prvt->buf_list.end()); ++iter)
                    {
                        if(*(*iter) == BE(cnt1))
                        {
                            more = true;
                            break;
                        }
                    }
                    for(BufList_Typ::iterator iter = prvt->buf_list.begin(); (iter != prvt->buf_list.end()) && more; iter = prvt->buf_list.erase(iter))
                    {
                        if(*(*iter) == BE(cnt1))
                        {
                            more = false;
                        }
#if DEBUG
                        dout << BE(*(*iter)) << ' ';
#endif
                        cache_typ* cache = (cache_typ*)(*iter) - 1;
                        cache->h_unreserve();
                    }
                    begin += HEADER_SIZE;
#if DEBUG
                    dout << "\n";
#endif
                }
                else
                {
                    if (id != 0)
                    {
                        ++begin;
                        continue;
                    }
                    begin += HEADER_SIZE;
#if DEBUG
                    dout << "RMUDP: Ack ignored\n";
#endif
                }
            }
            else if (cmd == RESEND)
            {
#ifndef NO_THREAD
                udp_locker lock(&prvt->buffer_lock);
#endif
                //check resend big pkt number
                if((cnt1 == prvt->sender_last_major) && (id == prvt->sender_resend_counter)) //duplicate resends
                {
                    //do nothing
                    begin += HEADER_SIZE;
                    continue;
                }

#if DEBUG
                dout << "RMUDP: Initiate resend and compaction from " << cnt1 << '\n';
#endif

                //proceed to compaction then resend
                prvt->sender_resend_counter = id;
                prvt->sender_last_major = cnt1;

                //clear the queue_list
                BufList_Typ::iterator iter;
                {
#ifndef NO_THREAD
                    udp_locker lock2(&prvt->queue_list_mutex);
#endif
                    for (iter = prvt->queue_list.begin(); iter != prvt->queue_list.end(); ++iter)
                    {
                        cache_typ* pbuf = (cache_typ*)(*iter) - 1;
                        pbuf->h_unreserve();
                    }
                    prvt->queue_list.clear();
                }

                for(iter = prvt->buf_list.begin(); iter != prvt->buf_list.end(); ++iter)
                {
                    if(*(*iter) == BE(cnt1))
                    {
                        break;
                    }
                }

                //compact the packets so that more can be sent out in one burst
                if ((iter == prvt->buf_list.end()) && (cnt1 != prvt->major_outnum))
                {
                    miniprintf("Unable to locate outnum %d for resend. Some data may be lost.\n", cnt1);
                    iter = prvt->buf_list.end();
                    if (prvt->notfound_last_major != cnt1)
                    {
                        prvt->notfound_last_major = cnt1;
                        prvt->notfound_last_counter = 1;
                    }
                    else if ((++prvt->notfound_last_counter) > 10)
                    {
                        prvt->errflag = ERR_TOLERANCE;
                    }
                }

                if (iter != prvt->buf_list.end())
                {
                    // Remove useless minor packets
                    int leftover = 0;
                    bool keepleftover;
                    BufList_Typ::iterator p_end;
                    // Eliminate the first batch of continued packets
                    for (p_end = iter; p_end != prvt->buf_list.end(); ++p_end)
                    {
                        if (*(*p_end+1) != CONT_ID)
                        {
                            break;
                        }
                    }
                    // Start compacting here
                    for (; p_end != prvt->buf_list.end(); ++p_end)
                    {
                        unsigned int src, dst;
                        if (leftover != 0)
                        {
                            if (*(*p_end+1) == CONT_ID)
                            {
                                if (leftover >= UDP_BUF - 2)
                                {
                                    if (!keepleftover)
                                    {
                                        ((cache_typ*)(*p_end) - 1)->set_len(0);
                                    }
                                    leftover -= UDP_BUF - 2;
                                    continue;
                                }
                                if (keepleftover)
                                {
                                    src = leftover + 2;
                                    dst = leftover + 2;
                                }
                                else
                                {
                                    src = leftover + 2;
                                    dst = 1; // this will override the CONT_ID
                                }
                                leftover = 0;
                            }
                            else
                            {
                                dout << "Compactor cannot find spanning packet\n";
                                leftover = 0;
                                unsigned int* buf = *p_end;
                                unsigned int end = ((cache_typ*)(*p_end) - 1)->get_len();
                                for (src = 1; src < end; ++src)
                                {
                                    if (buf[src] == BE(MAGICNUM))
                                    {
                                        break;
                                    }
                                }
                                if (src == end)
                                {
                                    ((cache_typ*)(*p_end) - 1)->set_len(0);
                                    continue;
                                }
                                dst = 1;
                            }
                        }
                        else if (*(*p_end+1) == CONT_ID)
                        {
                            dout << "Compactor detected invalid spanning packet\n";
                            continue;
                        }

                        if (*(*p_end+1) == BE(MAGICNUM))
                        {
                            src = 1;
                            dst = 1;
                        }
                        else if (*(*p_end+1) != CONT_ID)
                        {
                            dout << "Compactor detected invalid packet\n";
                            continue;
                        }

                        unsigned int end = ((cache_typ*)(*p_end) - 1)->get_len();
                        unsigned int* buf = *p_end;
                        while (src < end)
                        {
                            // first look for magic number
                            if (buf[src] != BE(MAGICNUM))
                            {
                                dout << "Compactor detected space between packets\n";
                                ++src;
                                continue;
                            }
                            unsigned int cmd1 = BE(buf[src + 1]) >> 24;
                            if (cmd1 >= ENDCMD)
                            {
                                dout << "Compactor detected invalid cmd\n";
                                ++src;
                                continue;
                            }
                            unsigned int length1 = BE(buf[src+2]) & 0xFFFF;
                            if (length1 + src + HEADER_SIZE > end)
                            {
                                if ((cmd1 == SETLIST) || (cmd1 == BROADCAST))
                                {
                                    if (src != dst)
                                    {
                                        memmove(buf+dst, buf+src, sizeof(unsigned int)*(UDP_BUF - src));
                                    }
                                    dst += UDP_BUF - src;
                                    leftover = length1 + HEADER_SIZE + src - UDP_BUF;
                                    keepleftover = true;
                                    src = end;
                                }
                                else if (cmd1 == MESSAGE)
                                {
                                    leftover = length1 + HEADER_SIZE + src - UDP_BUF;
                                    keepleftover = false;
                                    src = end;
                                }
                                else
                                {
                                    ++src;
                                    dout << "Compactor detected invalid length\n";
                                }
                                continue;
                            }
                            if ((cmd1 == SETLIST) ||
                                (cmd1 == BROADCAST) ||
                                (cmd1 == ACKNOWLEDGE) ||
                                (cmd1 == RESEND) ||
                                (cmd1 == REGISTERIDX) ||
                                (cmd1 == UNREGISTERIDX) ||
                                (cmd1 == QUERYID) ||
                                (cmd1 == CLIENTID) ||
                                (cmd1 == REGISTERID) ||
                                (cmd1 == FLOWCONTROL))
                            {
                                // dout << '*' << (BE(buf[src+2]) >> 16) << ' ';
                                if (src != dst)
                                {
                                    memmove(buf+dst, buf+src, sizeof(unsigned int)*(length1 + HEADER_SIZE) );
                                }
                                dst += HEADER_SIZE + length1;
                            }
                            src += HEADER_SIZE + length1;
                        }
                        ((cache_typ*)(*p_end) - 1)->set_len(dst);
                    }

                    // Recombine the left over data into major packets
                    BufList_Typ compact_buf;
                    for (BufList_Typ::iterator p_start = iter; p_start != prvt->buf_list.end(); p_start = p_end)
                    {
                        cache_typ* cache = (cache_typ*)(*p_start) - 1;
                        p_end = p_start;
                        ++p_end;
                        for (; p_end != prvt->buf_list.end(); ++p_end)
                        {
                            cache_typ* src = (cache_typ*)(*p_end) - 1;
                            unsigned int len = src->get_len();
                            if (len <= 1)
                            {
                                continue;
                            }
                            else if (cache->get_len() + len <= UDP_BUF)
                            {
                                memcpy(*p_start + cache->get_len(), *p_end + 1, (len-1)*sizeof(unsigned int));
                                src->h_unreserve();
                                cache->set_len(cache->get_len() + len - 1);
                            }
                            else
                            {
                                break;
                            }
                        }
                        *(*p_start) = BE(cnt1); // renumber the sequence number
                        cnt1 = incr_major_num(cnt1);
                        compact_buf.push_back(*p_start);
                        // at the same time we push the packet into the queue
                        cache->h_reserve();
#ifndef NO_THREAD
                        udp_locker lock3(&prvt->queue_list_mutex);
#endif
                        prvt->queue_list.push_back(*p_start);
                    }
                    prvt->buf_list.swap(compact_buf);
                    prvt->major_outnum = cnt1;
                }
                begin += HEADER_SIZE;
            }
            else if (cmd == GETLIST)
            {
                if (cnt1 != MSG_NUM)
                {
                    ++begin;
                    continue;
                }
                if (defercb)
                {
                    cache_typ* p = cache_typ::h_duplicate(id, 0, in_buf+begin+HEADER_SIZE);
                    p->set_cmd(cmd);
                    prvt->cb_list.push_back(p);
                }
                else
                {
                    cache_typ* p = cache_typ::ConvertToCache(&in_buf[begin]);
                    (*(callback[cmd]))(callback_arg[cmd], id, p);
                }
                begin += HEADER_SIZE;
            }
            else if (cmd == ALIVE)
            {
                if (cnt1 != MSG_NUM)
                {
                    ++begin;
                    continue;
                }
                begin += HEADER_SIZE;
            }
            else if (cmd == FLOWCONTROL)
            {
                if (cnt1 != MSG_NUM)
                {
                    ++begin;
                    continue;
                }
                prvt->flow_interval = id;
                begin += HEADER_SIZE;
            }
            else if ((cmd == SETLIST) || (cmd == BROADCAST))
            {
                // case where the seq number was not matched
                length = BE(in_buf[begin+2]) & 0xFFFF;
                begin += HEADER_SIZE + length;
                if (!outofsync)
                {
                    int decr = prvt->minor_innum, incr = prvt->minor_innum;
                    int cnt2;
                    for (cnt2 = 0; cnt2 < 100; ++cnt2)
                    {
                        if ((decr == (int)cnt1) || (incr == (int)cnt1))
                        {
                            break;
                        }
                        decr = (decr - 1) & 0x7FFF;
                        incr = (incr + 1) & 0x7FFF;
                    }
                    if ((cnt2 == 100) && (++prvt->errflag > ERR_TOLERANCE))
                    {
                        miniprintf("Resetting minor sequence number, data lost\n");
                        prvt->minor_innum = 0;
                        prvt->errflag = 0;
                    }
                    else
                    {
                        miniprintf("Sequence number error, difference %d\n", cnt2);
                    }
                }
            }
            else
            {
                ++begin;
            }
        }
        else
        {
            ++begin;
        }
    }
    prvt->readflag = false;
    return;
    // Error condition
receive_error:
    if (prvt->sock.hasCriticalError())
    {
        prvt->errflag = ERR_TOLERANCE;
    }
    else
    {
        ++prvt->errflag;
    }
    miniprintf("Receive ERROR\n");
receive_exit:
    prvt->readflag = false;
}

bool RMUDPSocket::Flush()
{
    if (prvt->sync < 0)
    {
        return false;
    }
    if (prvt->outbuf_sz == 1)
    {
        if (prvt->sync == 1)
        {
            // check to see if there is anything in the transmit queue
            if( !prvt->queue_list.empty() )
            {
                const timeElapsed nowtime(true);
#ifndef NO_THREAD
                udp_locker lock(&prvt->queue_list_mutex);
#endif
                for (int cnt = 0; (cnt < 5) && !prvt->queue_list.empty() && check_bandwidth_limit(prvt->sendtime, prvt->packet_window, nowtime, prvt->packet_rate) && check_close_interval(prvt->closetime, nowtime); ++cnt)
                {
                    unsigned int* obuf = prvt->queue_list.front();
                    cache_typ* pbuf1 = (cache_typ*)obuf - 1;
                    int plen = pbuf1->get_len() * sizeof(unsigned int);
                    if (prvt->sock.send((char*)obuf, plen) == plen)
                    {
                        pbuf1->h_unreserve();
                        prvt->queue_list.pop_front();
                        enforce_bandwidth_limit(prvt->sendtime, prvt->packet_window, nowtime, prvt->packet_rate);
                    }
                }
            }
        } //else if buf list not empty
        return true;
    }

    cache_typ* pbuf = (cache_typ*)prvt->outbuf - 1;
    if(prvt->store_flag)
    {
        // miniprintf("SendPacket::Block %d, length %d\n", prvt->major_outnum, prvt->outbuf_sz);
        prvt->outbuf[0] = BE(prvt->major_outnum);
        prvt->major_outnum = incr_major_num(prvt->major_outnum);
        pbuf->set_len(prvt->outbuf_sz);
        pbuf->h_reserve();
        prvt->buf_list.push_back(prvt->outbuf);
    }
    //    else
    //    {
    //        miniprintf("SendPacket::Non-guranteed block, length %d\n", prvt->outbuf_sz);
    //    }

    const timeElapsed nowtime(true);

    // check to see if there is anything in the transmit queue
    if( !prvt->queue_list.empty() )
    {
#ifndef NO_THREAD
        udp_locker lock(&prvt->queue_list_mutex);
#endif
        for (int cnt = 0; (cnt < 5) && !prvt->queue_list.empty() && check_bandwidth_limit(prvt->sendtime, prvt->packet_window, nowtime, prvt->packet_rate) && check_close_interval(prvt->closetime, nowtime); ++cnt)
        {
            unsigned int* obuf = prvt->queue_list.front();
            cache_typ* pbuf1 = (cache_typ*)obuf - 1;
            int plen = pbuf1->get_len() * sizeof(unsigned int);
            if (prvt->sock.send((char*)obuf, plen) == plen)
            {
                pbuf1->h_unreserve();
                prvt->queue_list.pop_front();
                enforce_bandwidth_limit(prvt->sendtime, prvt->packet_window, nowtime, prvt->packet_rate);
            }
        }
    }
    else
    {
        goto send_direct;
    }
    // check again
    if (prvt->queue_list.empty())
    {
send_direct:
        if (check_bandwidth_limit(prvt->sendtime, prvt->packet_window, nowtime, prvt->packet_rate) && check_close_interval(prvt->closetime, nowtime) && prvt->sock.send((char*)prvt->outbuf, prvt->outbuf_sz * sizeof(unsigned int)) == (int)(prvt->outbuf_sz * sizeof(unsigned int)))
        {
            if (prvt->store_flag)
            {
                pbuf->h_unreserve();
                cache_typ* cache = cache_typ::h_allocate(UDP_BUF);
                prvt->outbuf = cache->buf();
                prvt->outbuf[0] = MSG_ID;
            }
            enforce_bandwidth_limit(prvt->sendtime, prvt->packet_window, nowtime, prvt->packet_rate);
        }
        else
        {
            goto send_indirect;
        }
    }
    else
    {
send_indirect:
        pbuf->set_len(prvt->outbuf_sz);
        {
#ifndef NO_THREAD
            udp_locker lock(&prvt->queue_list_mutex);
#endif
            prvt->queue_list.push_back(prvt->outbuf);
        }
        cache_typ* cache = cache_typ::h_allocate(UDP_BUF);
        prvt->outbuf = cache->buf();
        prvt->outbuf[0] = MSG_ID;
    }
    prvt->outbuf_sz = 1;
    prvt->store_flag = false;
    return true;
}

bool RMUDPSocket::WaitForHandshakeCompletion(const struct timeval*)
{
    return true;
}

int RMUDPSocket::GetHandle() const
{
    return (int)prvt->sock.get_sock();
}

bool RMUDPSocket::CheckLink()
{
    return (prvt->flow_interval == 0) || (prvt->broadcast) || ((prvt->sync==1) && (prvt->recvtime.elapsed() <= prvt->flow_interval * 1000U));
}

bool RMUDPSocket::HandshakeAck(unsigned int, unsigned int)
{
    // UDP does not handshake nor handshake acknowledge
    return true;
}
int RMUDPSocket::InConnectSelect(const struct timeval* stm)
{
    if (prvt->sock2 == NULL)
    {
        return Select(stm, true);
    }
    int socks[2];
    socks[0] = prvt->sock.get_sock();
    socks[1] = prvt->sock2->get_sock();
    const int ret = core_select(socks, 2, stm);
    if (ret > 0)
    {
        if (socks[0] > 0)
        {
            prvt->sock2->close();
            delete prvt->sock2;
            prvt->sock2 = NULL;
        }
        else if (socks[1] > 0)
        {
            prvt->sock.close();
            prvt->sock = *(prvt->sock2);
            struct sockaddr_in& dummy = (struct sockaddr_in&)socks[0];
            prvt->sock2->sethost(dummy, 10);
            free(prvt->sock2); // destruction of sock2 without closing socket
            prvt->sock2 = NULL;
        }
        ReceivePacket(true);
        if (prvt->errflag >= ERR_TOLERANCE)
        {
            return -1;
        }
    }
    else if(ret < 0)
    {
        return -1;
    }
    else
    {
        prvt->errflag += 2;
    }
    return ret;
}

bool RMUDPSocket::InConnectSendReset()
{
    bool ok = true;
    cache_typ* pbuf = cache_typ::h_allocate(0);
    unsigned int* mag = pbuf->packet() - 1;
    *mag = MSG_ID;
    pbuf->set_id(0);
    pbuf->set_seq(MSG_NUM);
    pbuf->set_cmd(RESET);

    if (prvt->sock.send((const char*)mag, (HEADER_SIZE+1)*sizeof(unsigned int)) != (HEADER_SIZE+1)*sizeof(unsigned int))
    {
        ok = false;
    }
    if ((prvt->sock2 != NULL) && (prvt->sock2->send((const char*)mag, (HEADER_SIZE+1)*sizeof(unsigned int)) != (HEADER_SIZE+1)*sizeof(unsigned int)))
    {
        ok = false;
    }

    cache_typ::h_deallocate(pbuf);
    prvt->sync = -1;
    return ok;
}

/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _ROAMCLI_H_DEF_
#define _ROAMCLI_H_DEF_

struct RMRoamClient_private;
struct sockaddr_in;
class tcp_core;

class RMRoamClient
{
public:
    RMRoamClient(short _roam_port, const char* _host = 0);
    ~RMRoamClient();

    void Close();
    void Reset();
    bool Select(const struct timeval* stm = 0);
    bool OnDataAvail();

    short Get_Port() const;
    void Set_Port(short port);
    void ExportAddr(tcp_core*);
    void ExportAddr(struct sockaddr_in* addr);

protected:
    short rand_port();

    RMRoamClient_private* prvt;

private:
    RMRoamClient(const RMRoamClient&);
    RMRoamClient& operator = (const RMRoamClient&);

};

#endif /* _ROAMCLI_H_DEF_ */


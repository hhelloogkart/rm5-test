/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _STORAGE_H_
#define _STORAGE_H_

#include "directory.h"

struct queue_typ;
class queue_factory;
class var_collection;
class RMSMemSocket;
class osevent;

struct RM_EXPORT storage_typ
{
    rmname_typ cache;
    rmname_typ queue[MAX_CLIENT];
    rmname_typ var;
    directory_typ directory;
    namedir_typ namedir;
};

struct RM_EXPORT rmglobal_private_typ
{
    unsigned int magic;
    storage_typ store;
};

class RM_EXPORT rmglobal_typ
{
    friend class queue_factory;
    friend class var_collection;
    friend class RMSMemSocket;
public:
    explicit rmglobal_typ(const char* name = (const char*)NULL);
    ~rmglobal_typ();
    void detach();
    void rdlock();
    void lock();
    void unlock();
    MM* attach_cache(unsigned int size = CACHE_SIZE);
    void attach_queue(unsigned int size = QUEUE_SIZE);
    MM* attach_var(unsigned int size = VAR_SIZE);
protected:
    void set_name(rmname_typ& nm, char ch);
    void set_name(rmname_typ& nm, char ch, int num);

    rmname_typ global_name;
    rmglobal_private_typ* ptr;
    MM* cache;
    MM* queue[MAX_CLIENT];
    MM* var;
    osevent* queue_event[MAX_CLIENT];
};


#endif /* _STORAGE_H_ */

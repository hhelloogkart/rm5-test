/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _RMOLDTCP_H_
#define _RMOLDTCP_H_

#include "rmtcp.h"

struct RM3TCPSocket_Private;

// Class for TCP/IP connection with RM 3
// Not to be used in server code, the RM 3 protocol is not symmetrical

class RM_EXPORT RM3TCPSocket : public RMTCPSocket
{
public:
    enum
    {
        RM3_SET_LIST = 3, RM3_GET_LIST = 7, RM3_BROADCAST_MESSAGE = 12,
        RM3_REGISTER_ID = 16, RM3_UNREGISTER_ID = 17, RM3_QUERY_ID = 18, RM3_CLIENT_ID = 19,
        RM3_PING = 21, RM3_FLOW_CONTROL = 22, RM3_MAX_FUNC
    };

    RM3TCPSocket();
    RM3TCPSocket(const tcp_core& asock);
    virtual ~RM3TCPSocket();

    // Connection and status
    virtual int  Select(const struct timeval* stm = 0, bool defercb = false);

    // RM Protocol Calls for Clients
    virtual const cache_typ* GetListStart(unsigned int id);
    virtual bool SetListEnd(unsigned int id, cache_typ* pbuf);
    virtual bool BroadcastEnd(unsigned int id, cache_typ* pbuf);
    virtual bool RegisterID(const char* str);
    virtual bool QueryID(const char* str);
    virtual bool QueryID(const char* str[], int num);
    virtual bool ClientID(const char* client);
    virtual bool RegisterIDX(unsigned int id);
    virtual bool UnregisterIDX(unsigned int id);
    virtual bool FlowControl(unsigned int interval);
    virtual bool WaitForHandshakeCompletion(const struct timeval* stm = 0);
    virtual void KeepAlive();

    // Depreciated and Inefficient RM Protocol Calls
    virtual bool Broadcast(unsigned int id, size_t len, const unsigned int* buf);
    virtual bool SetList(unsigned int id, size_t len, const unsigned int* buf);

protected:
    void ReceivePacket(bool defercb);
    int IDX2ID(int _idx);
    int ID2IDX(int _id);

    RM3TCPSocket_Private* prvt2;
};

#endif /* _RMOLDTCP_H_ */

/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef REL_PTR_H_
#define REL_PTR_H_

#include <time/timecore.h>
#include <stddef.h>

#if !defined(__ADSPBLACKFIN__)
#include <sys/types.h>  // required for size_t
#endif
#include <iterator>

#if (defined(__INTEL_COMPILER) && __INTEL_COMPILER >= 1000) || (defined(__GNUC__) && !defined(__INTEL_COMPILER) && __GNUC__ == 3) || (defined(_MSC_VER) && (_MSC_VER >= 1500))
#define RM_DK
#include <dinkum_stl/xutility>
#endif

class var_sharedmemory_segment
{
public:
    static THREAD_PRIVATE void* Ptr;
};

class cache_sharedmemory_segment
{
public:
    static THREAD_PRIVATE void* Ptr;
    static THREAD_PRIVATE void* Ptr_end;
};

class cache_staticmemory_segment
{
public:
    static THREAD_PRIVATE void* Ptr;
    static THREAD_PRIVATE void* Ptr_end;
};

#if !defined(_MSC_VER) || (_MSC_VER != 1600)
namespace std
{
template<typename _T1, typename _T2>
inline void
_Construct(_T1 __p, const _T2& __value2)
{
    ::new(static_cast<void*>(__p)) _T2(__value2);
}
}
#endif

template<class T, class M>
class const_relative_ptr;

template<class T, class M>
class RM_EXPORT relative_ptr
{
public:
#ifdef RM_DK
    typedef rm::random_access_iterator_tag iterator_category;
#else
    typedef std::random_access_iterator_tag iterator_category;
#endif
    typedef T value_type;
    typedef size_t size_type;
    typedef ptrdiff_t difference_type;
    typedef relative_ptr<T,M> pointer;
    typedef const_relative_ptr<T,M> const_pointer;
    typedef T& reference;
#if defined(__LP64__) || defined(_WIN64)
    typedef unsigned long long offset_type;
#else
    typedef unsigned long offset_type;
#endif // __LP64__) || defined(_WIN64)
    offset_type offset;
private:
    relative_ptr<T,M>(offset_type _offset, int) : offset(_offset)
    {
    }
public:
    relative_ptr() : offset(0xFFFFFFFF)
    {
    }
    relative_ptr(const relative_ptr<T,M>& _Right) : offset(_Right.offset)
    {
    }
    relative_ptr(void* _Ptr) :
        offset((_Ptr != 0) ? (offset_type)_Ptr - (offset_type)M::Ptr : 0xFFFFFFFF)
    {
    }
    template<class O>
    relative_ptr<T,M>& operator=(const relative_ptr<O,M>& _Right)
    {
        this->offset = _Right.offset;
        return *this;
    }
    relative_ptr<T,M>& operator=(const relative_ptr<T,M>& _Right)
    {
        this->offset = _Right.offset;
        return *this;
    }
    relative_ptr<T,M>& operator=(void* _Ptr)
    {
        if (_Ptr)
        {
            this->offset = (offset_type)_Ptr - (offset_type)M::Ptr;
        }
        else
        {
            this->offset = 0xFFFFFFFF;
        }
        return *this;
    }
    bool operator==(const relative_ptr<T,M>& _Right) const
    {
        return _Right.offset == this->offset;
    }
    bool operator==(void* _Ptr) const
    {
        return ((_Ptr == 0) && (offset == 0xFFFFFFFF)) ||
               (((offset_type)_Ptr - (offset_type)M::Ptr) == this->offset);
    }
    bool operator!=(const relative_ptr<T,M>& _Right) const
    {
        return !operator==(_Right);
    }
    bool operator<=(const relative_ptr<T,M>& _Right) const
    {
        return this->offset <= _Right.offset;
    }
    bool operator>=(const relative_ptr<T,M>& _Right) const
    {
        return this->offset >= _Right.offset;
    }
    template<class U>
    bool operator==(const relative_ptr<U,M>& _Right) const
    {
        return _Right.offset == this->offset;
    }
    bool operator!=(void* _Ptr) const
    {
        return !operator==(_Ptr);
    }
    bool operator<(const relative_ptr<T,M>& _Right) const
    {
        return this->offset < _Right.offset;
    }
    T& operator*() const
    {
        return *((T*)((offset_type)M::Ptr + this->offset));
    }
    T* operator->( ) const
    {
        return (T*)((offset_type)M::Ptr + this->offset);
    }
    operator T* () const
    {
        if (this->offset == 0xFFFFFFFF)
        {
            return (T*)0;
        }
        return (T*)((offset_type)M::Ptr + this->offset);
    }
    operator void* () const
    {
        if (this->offset == 0xFFFFFFFF)
        {
            return (void*)0;
        }
        return (void*)((offset_type)M::Ptr + this->offset);
    }
    relative_ptr<T,M> operator+(difference_type _sz) const
    {
        return relative_ptr<T,M>(this->offset + _sz*sizeof(T),0);
    }
    relative_ptr<T,M> operator+(size_type _sz) const
    {
        return relative_ptr<T,M>(this->offset + _sz*sizeof(T),0);
    }
#ifdef _WIN64
    relative_ptr<T, M> operator+(int _sz) const
    {
        return relative_ptr<T, M>(this->offset + _sz * sizeof(T), 0);
    }
#endif // _WIN64
    relative_ptr<T,M> operator-(difference_type _sz) const
    {
        return relative_ptr<T,M>(this->offset - _sz*sizeof(T),0);
    }
    relative_ptr<T,M> operator-(size_type _sz) const
    {
        return relative_ptr<T,M>(this->offset - _sz*sizeof(T),0);
    }
    size_t operator-(const relative_ptr<T,M>& Right) const
    {
        return (size_t)((this->offset - Right.offset)/sizeof(T));
    }
#ifdef _WIN64
    relative_ptr<T, M> operator-(int _sz) const
    {
        return relative_ptr<T, M>(this->offset - _sz * sizeof(T), 0);
    }
#endif
    relative_ptr<T,M>& operator+=(size_t _sz)
    {
        this->offset += _sz*sizeof(T);
        return *this;
    }
    relative_ptr<T,M>& operator-=(size_t _sz)
    {
        this->offset -= _sz*sizeof(T);
        return *this;
    }
    relative_ptr<T,M>& operator++()
    {
        this->offset += sizeof(T);
        return *this;
    }
    relative_ptr<T,M> operator++(int)
    {
        relative_ptr<T,M> ret(*this);
        this->offset += sizeof(T);
        return ret;
    }
    relative_ptr<T,M>& operator--()
    {
        this->offset -= sizeof(T);
        return *this;
    }
};

template<class M>
class RM_EXPORT relative_ptr<void, M>
{
public:
#ifdef RM_DK
    typedef rm::random_access_iterator_tag iterator_category;
#else
    typedef std::random_access_iterator_tag iterator_category;
#endif
    typedef void value_type;
    typedef size_t size_type;
    typedef ptrdiff_t difference_type;
    typedef relative_ptr<void,M> pointer;
    typedef const_relative_ptr<void,M> const_pointer;
#if defined(__LP64__) || defined(_WIN64)
    typedef unsigned long long offset_type;
#else
    typedef unsigned long offset_type;
#endif // __LP64__) || defined(_WIN64)
    offset_type offset;
private:
    relative_ptr<void,M>(offset_type _offset, int) : offset(_offset)
    {
    }
public:
    relative_ptr() : offset(0xFFFFFFFF)
    {
    }
    relative_ptr(const relative_ptr<void,M>& _Right) :
        offset(_Right.offset)
    {
    }
    template<class O>
    relative_ptr(const relative_ptr<O,M>& _Right) :
        offset(_Right.offset)
    {
    }
    relative_ptr(void* _Ptr) :
        offset((_Ptr != 0) ? (offset_type)_Ptr - (offset_type)M::Ptr : 0xFFFFFFFF)
    {
    }
    template<class O>
    relative_ptr<void,M>& operator=(const relative_ptr<O,M>& _Right)
    {
        this->offset = _Right.offset;
        return *this;
    }
    relative_ptr<void,M>& operator=(const relative_ptr<void,M>& _Right)
    {
        this->offset = _Right.offset;
        return *this;
    }
    relative_ptr<void,M>& operator=(void* _Ptr)
    {
        if (_Ptr)
        {
            this->offset = (offset_type)_Ptr - (offset_type)M::Ptr;
        }
        else
        {
            this->offset = 0xFFFFFFFF;
        }
        return *this;
    }
    bool operator==(const relative_ptr<void,M>& _Right) const
    {
        return _Right.offset == this->offset;
    }
    bool operator==(void* _Ptr) const
    {
        return ((_Ptr == 0) && (offset == 0xFFFFFFFF)) ||
               (((offset_type)_Ptr - (offset_type)M::Ptr) == this->offset);
    }
    bool operator!=(const relative_ptr<void,M>& _Right) const
    {
        return !operator==(_Right);
    }
    template<class U>
    bool operator==(const relative_ptr<U,M>& _Right) const
    {
        return _Right.offset == this->offset;
    }
    bool operator!=(void* _Ptr) const
    {
        return !operator==(_Ptr);
    }
    bool operator<(const relative_ptr<void,M>& _Right) const
    {
        return this->offset < _Right.offset;
    }
    void* operator->( ) const
    {
        return (void*)((offset_type)M::Ptr + this->offset);
    }
    operator void* () const
    {
        if (this->offset == 0xFFFFFFFF)
        {
            return (void*)0;
        }
        return (void*)((offset_type)M::Ptr + this->offset);
    }
};

template<class T, class M>
class RM_EXPORT const_relative_ptr
{
    typedef std::random_access_iterator_tag iterator_category;
    typedef void value_type;
    typedef size_t size_type;
    typedef ptrdiff_t difference_type;
    typedef const_relative_ptr<void, M> pointer;
    typedef T& reference;
#if defined(__LP64__) || defined(_WIN64)
    typedef unsigned long long offset_type;
#else
    typedef unsigned long offset_type;
#endif // __LP64__) || defined(_WIN64)
protected:
    offset_type offset;
    const_relative_ptr<T,M>(offset_type _offset, int) : offset(_offset)
    {
    }
public:
    const_relative_ptr() : offset(0xFFFFFFFF)
    {
    }
    const_relative_ptr(const const_relative_ptr<T,M>& _Right) :
        offset(_Right.offset)
    {
    }
    const_relative_ptr(const void* _Ptr) :
        offset((_Ptr != 0) ? (offset_type)_Ptr - (offset_type)M::Ptr : 0xFFFFFFFF)
    {
    }
    template<class O>
    const_relative_ptr<T,M>& operator=(const const_relative_ptr<O,M>& _Right)
    {
        this->offset = _Right.offset;
        return *this;
    }
    const_relative_ptr<T,M>& operator=(const const_relative_ptr<T,M>& _Right)
    {
        this->offset = _Right.offset;
        return *this;
    }
    relative_ptr<T,M>& operator=(const void* _Ptr)
    {
        if (_Ptr)
        {
            this->offset = (offset_type)_Ptr - (offset_type)M::Ptr;
        }
        else
        {
            this->offset = 0xFFFFFFFF;
        }
        return *this;
    }
    bool operator==(const const_relative_ptr<T,M>& _Right) const
    {
        return _Right.offset == this->offset;
    }
    template<class U>
    bool operator==(const const_relative_ptr<U,M>& _Right) const
    {
        return _Right.offset == this->offset;
    }
    bool operator==(void* _Ptr) const
    {
        return ((_Ptr == 0) && (offset == 0xFFFFFFFF)) ||
               (((offset_type)_Ptr - (offset_type)M::Ptr) == this->offset);
    }
    bool operator!=(const const_relative_ptr<T,M>& _Right) const
    {
        return !operator==(_Right);
    }
    bool operator!=(void* _Ptr) const
    {
        return !operator==(_Ptr);
    }
    bool operator<(const const_relative_ptr<T,M>& _Right) const
    {
        return this->offset < _Right.offset;
    }
    const T& operator*() const
    {
        return *((T*)((offset_type)M::Ptr + this->offset));
    }
    const T* operator->( ) const
    {
        return (const T*)((offset_type)M::Ptr + this->offset);
    }
    operator const T* () const
    {
        if (this->offset == 0xFFFFFFFF)
        {
            return (T*)0;
        }
        return (const T*)((offset_type)M::Ptr + this->offset);
    }
    operator const void* () const
    {
        if (this->offset == 0xFFFFFFFF)
        {
            return (void*)0;
        }
        return (void*)((offset_type)M::Ptr + this->offset);
    }
    const_relative_ptr<T,M>& operator+(size_t _sz) const
    {
        return const_relative_ptr<T,M>(this->offset + _sz*sizeof(T),0);
    }
    const_relative_ptr<T,M>& operator-(size_t _sz) const
    {
        return const_relative_ptr<T,M>(this->offset - _sz*sizeof(T),0);
    }
    size_t operator-(const const_relative_ptr<T,M>& Right) const
    {
        return (size_t)((this->offset - Right.offset)/sizeof(T));
    }
    const_relative_ptr<T,M>& operator+=(size_t _sz)
    {
        this->offset += _sz*sizeof(T);
        return *this;
    }
    const_relative_ptr<T,M>& operator-=(size_t _sz)
    {
        this->offset -= _sz*sizeof(T);
        return *this;
    }
    const_relative_ptr<T,M>& operator++()
    {
        this->offset += sizeof(T);
        return *this;
    }
    const_relative_ptr<T,M> operator++(int)
    {
        const_relative_ptr<T,M> ret(*this);
        this->offset += sizeof(T);
        return ret;
    }
    const_relative_ptr<T,M>& operator--()
    {
        this->offset -= sizeof(T);
        return *this;
    }
};

#endif

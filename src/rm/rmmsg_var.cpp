/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "rmmsg_var.h"
#include "rmsocket.h"
#include "outbuf.h"

rm_message_var::rm_message_var(const char nm[], bool regflag) :
    rm_var(nm, regflag)
{
}

bool rm_message_var::write(outbuf& buf)
{
    return Socket().BroadcastEnd(id, buf.getcache());
}

int rm_message_var::extract(int len, const unsigned char* buf)
{
    len = complex_var::extract(len, buf);
    refresh();
    return len;
}

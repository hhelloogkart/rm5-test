/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _QUEUE_H_
#define _QUEUE_H_

#include "storage.h"

struct timeval;

class RM_EXPORT queue_factory
{
public:
    queue_factory(rmglobal_typ& glob, unsigned int sz);
    void push(cache_typ* ptr, unsigned int client);
    void pop(unsigned int client);
    cache_typ* top(unsigned int client) const;
    void clear(unsigned int client);
    void dump(unsigned int client);
    int select(unsigned int client, const struct timeval* stm = 0);
protected:
    rmglobal_typ& global_sm;
    storage_typ& store;
};

#endif /* _QUEUE_H_ */

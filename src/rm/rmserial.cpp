/******************************************************************/
/* Copyright DSO National Laboratories 2005. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "rmserial.h"
#include <util/dbgout.h>
#include <time/timecore.h>
#include "cache.h"
#ifndef SW_SERIAL
#include "comm/sercore.h"
#else
#include "comm/swsercore.h"
#endif
#include <limits.h>
#include <string.h>

#ifdef VXWORKS
#include <sys/socket.h>
#endif

#define HEADER_SIZE (sizeof(header_typ) / sizeof(unsigned int))
#define HEADER_OFF  ((sizeof(cache_typ) / sizeof(unsigned int)) - HEADER_SIZE)
#define MSG_SEQ 0xFF
#define BUF_NUM 96
#define BUF_SZ 128
#define OUTBUF (BUF_NUM*(HEADER_SIZE+HEADER_OFF+BUF_SZ))
#define RING_SIZE 63
#define SERIAL_MAGIC 0x72

// protocol tweaks
static const int NUMBER_OF_PACKETS_SENT_AT_KEEP_ALIVE = 2;
static const int NUMBER_OF_PACKETS_SENT_WHEN_RECV_IS_ALIVE = 1;

//#define SERIAL_DEBUG

#include "../mhash/src/libdefs.h"
extern "C"
{
#include "../mhash/src/mhash_crc32.h"
}

//timeElapsed serialtime(true);

struct queue_t
{
    unsigned short writer;
    unsigned short reader;
    cache_typ* queue[RING_SIZE+1];
};

inline unsigned int queue_next(unsigned int p)
{
    return (p + 1) & RING_SIZE;
}

inline bool queue_full(queue_t* q)
{
    return queue_next(q->writer) == q->reader;
}

inline bool queue_empty(queue_t* q)
{
    return q->reader == q->writer;
}

inline cache_typ*& queue_top(queue_t* q)
{
    return q->queue[q->reader];
}

inline cache_typ*& queue_bottom(queue_t* q)
{
    return q->queue[q->writer];
}

inline bool check_bandwidth_limit(const timeElapsed& sendtime, unsigned int& senddata, const timeElapsed& nowtime, unsigned int datalimit)
{
    if (senddata == 0)
    {
        return true;
    }
    const unsigned int interval = sendtime.elapsed(nowtime);
    const unsigned int bw = interval /*msec*/ * datalimit /*bits/sec*/ / 8000U;  /*bytes*/
    if (senddata > bw)
    {
#ifdef SERIAL_DEBUG
        dout << "Bandwidth check, requested " << senddata << " intv " << interval << " lim " << datalimit << " bw " << bw << '\n';
#endif
        return senddata-bw < 16; // allow a 16 byte buffer to build up
    }
    else
    {
        /* all data has been sent, clear datacounter */
        senddata = 0;
        return true;
    }
}

inline void enforce_bandwidth_limit(timeElapsed& sendtime, unsigned int& senddata, const timeElapsed& nowtime, unsigned int datalimit, unsigned int newdata)
{
    if (senddata == 0)
    {
        senddata = newdata;
    }
    else
    {
        const unsigned int interval = sendtime.elapsed(nowtime);
        const unsigned int bw = interval /*msec*/ * datalimit /*bits/sec*/ / 8000U;  /*bytes*/
        if (senddata > bw)
        {
            senddata = senddata - bw + newdata;
        }
        else
        {
            senddata = newdata;
        }
    }
    sendtime = nowtime;
#ifdef SERIAL_DEBUG
    dout << "Bandwidth enforcement, dat " << senddata << '\n';
#endif
}

/* MAGIC | ID | Length | Sequence or CMD */
struct serial_header
{
    unsigned char magic;
    unsigned char id;
    unsigned char len;
    unsigned char seq;
};

struct rmserial_socket_private
{
#ifndef SW_SERIAL
    serial_core sock;
#else
    sw_serial_core sock;
#endif
    unsigned char conn_state;  // 0 - initiate, 1 to 20 - wait for init, 21 - connected
    unsigned int getid;
    cache_typ* query_buf;
    timeElapsed recvtime;
    timeElapsed sendtime;
    unsigned int intv;
    bool init_flag;
    bool readflag;
    unsigned int valid;
    unsigned int errflag;
    unsigned char innum;   // from 0 to 127
    unsigned char outnum;  // from 0 to 127
    unsigned char last_resend_innum;
    unsigned int raw_inbuf[READ_BUF+HEADER_OFF+1];
    unsigned int raw_outbuf[OUTBUF];
    unsigned char* in_buf;
    queue_t outqueue;
    queue_t inqueue;
    queue_t cbqueue;
    bool reset_flag;
    unsigned int senddata;
    unsigned int datalimit;
};

RMSerialSocket::RMSerialSocket() : prvt(new rmserial_socket_private)
{
    prvt->init_flag = true;
    prvt->readflag = 0;
    prvt->outqueue.writer = 0;
    prvt->outqueue.reader = 0;
    prvt->inqueue.writer = 0;
    prvt->inqueue.reader = 0;
    prvt->cbqueue.writer = 0;
    prvt->cbqueue.reader = 0;
    prvt->query_buf = 0;
    prvt->reset_flag = false;
    prvt->senddata = 0;
    prvt->datalimit = 0;
    Close();
    prvt->intv = 0;
    prvt->in_buf = reinterpret_cast<unsigned char*>(prvt->raw_inbuf + HEADER_OFF);
    unsigned int* ptr = &prvt->raw_outbuf[0];
    for (int cnt = 0; cnt < BUF_NUM; ++cnt, ptr += HEADER_SIZE+HEADER_OFF+BUF_SZ)
    {
        reinterpret_cast<cache_typ*>(ptr)->construct(BUF_SZ);
        *(reinterpret_cast<cache_typ*>(ptr)->buf()-3) = MAGICNUM+1;
    }
}

RMSerialSocket::~RMSerialSocket()
{
    if (prvt->query_buf != NULL)
    {
        GetListEnd(0, prvt->query_buf);
    }
    prvt->sock.close();
    delete prvt;
}

bool RMSerialSocket::Connect(const char* host, int port, const struct timeval* stm)
{
    struct timeval tv = {
        2, 0
    };

    // check if serial_core is connected
    if (!prvt->sock.connected())
    {
        char hostname[64];
        prvt->datalimit = 0;
        for (int cnt=0; host[cnt]!='\0'; ++cnt)
        {
            if (host[cnt] == ',')
            {
                rm_strncpy(hostname, 64, host, cnt);
                hostname[cnt] = '\0';
                prvt->datalimit = atoi(host+cnt+1);
                break;
            }
        }
        if (prvt->datalimit == 0)
        {
            prvt->sock.sethost(host, port);
            prvt->datalimit = port;
        }
        else
        {
            prvt->sock.sethost(hostname, port);
        }
        prvt->sock.connect(0);   // serial connect is instantaneous
    }

    if (prvt->conn_state == '\21')
    {
        return true;
    }

    if ((prvt->conn_state == 0) &&
        (prvt->sendtime.elapsed() > 500))
    {
        SendMessageT(0xff, RESET);
    }

    if (Select(stm == 0 ? &tv : stm, true) == 0)
    {
        // still cannot get a reset
        ++(prvt->conn_state);
        if (prvt->conn_state == '\21')
        {
            prvt->conn_state = 0;
        }
        return false;
    }
    return prvt->conn_state == '\21';
}

bool RMSerialSocket::Connected()
{
    return prvt->conn_state == '\21';
}

bool RMSerialSocket::Close()
{
    prvt->conn_state = 0;
    prvt->getid = UINT_MAX;
    if (prvt->query_buf != NULL)
    {
        GetListEnd(0, prvt->query_buf);
        prvt->query_buf = NULL;
    }
    prvt->recvtime.clear();
    prvt->sendtime.clear();
    prvt->valid = 0;
    prvt->errflag = 0;
    prvt->innum = 0;   // from 0 to 127
    prvt->last_resend_innum = 128;
    for (int tag = prvt->inqueue.reader; tag != prvt->inqueue.writer; tag = queue_next(tag))
    {
        GetListEnd(0, prvt->inqueue.queue[tag]);
    }
    prvt->inqueue.writer = 0;
    prvt->inqueue.reader = 0;
    for (int tag = prvt->cbqueue.reader; tag != prvt->cbqueue.writer; tag = queue_next(tag))
    {
        GetListEnd(0, prvt->cbqueue.queue[tag]);
    }
    prvt->cbqueue.writer = 0;
    prvt->cbqueue.reader = 0;
    prvt->senddata = 0;
    unsigned char seq = 0;
    for (int tag = prvt->outqueue.reader; tag != prvt->outqueue.writer; tag = queue_next(tag), ++seq)
    {
        unsigned int* buf = prvt->outqueue.queue[tag]->buf() - 2;
        serial_header* head = reinterpret_cast<serial_header*>(buf+1);
        head->seq = seq;
        mhash_clear_crc32(reinterpret_cast<mutils_word32*>(buf));
        mhash_crc32(reinterpret_cast<mutils_word32*>(buf), buf+1, (head->len+1)*sizeof(unsigned int));    // the crc is packed at the beginning
        buf[0] = BE(buf[0]);
    }
    prvt->outnum = seq;
    return prvt->sock.close();
}

int RMSerialSocket::Select(const struct timeval* stm, bool defercb)
{
    unsigned int cmd;

    KeepAlive();
    if (!defercb)
    {
        while (!queue_empty(&prvt->cbqueue))
        {
            cache_typ* pcache = queue_top(&prvt->cbqueue);
            cmd = pcache->get_cmd();
            if (cmd < ENDCMD)
            {
                (*(callback[cmd]))(callback_arg[cmd], pcache->get_id(), pcache);
            }
            GetListEnd(0, pcache);
            prvt->cbqueue.reader = queue_next(prvt->cbqueue.reader);
        }
    }
    int ret = prvt->sock.select(stm);
    if (ret > 0)
    {
        ret = 1;
        ReceivePacket(defercb);
        if(prvt->errflag >= ERR_TOLERANCE)
        {
            return -1;
        }
    }
    else if(ret < 0)
    {
        return -1;
    }
    else if(!CheckLink())
    {
        return -1;
    }
    else
    {
        ret = 0;
    }
#ifdef SERIAL_DEBUG
    //   dout << "Keepalive from Select\n";
#endif
    KeepAlive();
    return ret;
}

const cache_typ* RMSerialSocket::GetListStart(unsigned int id)
{
    int cnt = 0, ret;
    SendMessageT(id & 0xFF, GETLIST);
    prvt->getid = id;
    do
    {
        if (cnt != 0)
        {
            Sleep(WAIT_TIME);
        }
        struct timeval tm = {
            0, WAIT_TIME*1000
        };
        ret =  Select(&tm, true);
        ++cnt;
    }
    while ((prvt->query_buf == NULL) && (cnt < GETLIST_RETRY) && (ret >= 0));
    cache_typ* ptr = prvt->query_buf;
    prvt->getid = UINT_MAX;
    prvt->query_buf = NULL;
    return ptr;
}

void RMSerialSocket::GetListEnd(unsigned int, const cache_typ* pbuf)
{
    *(const_cast<cache_typ*>(pbuf)->buf()-3) = MAGICNUM+1;
    const_cast<cache_typ*>(pbuf)->h_unreserve();
}

cache_typ* RMSerialSocket::MessageStart(unsigned int id, size_t len)
{
    return SetListStart(id, len);
}

cache_typ* RMSerialSocket::SetListStart(unsigned int, size_t len)
{
    if (len == 0)
    {
        dout << "RM Message does not allow zero length packets\n";
        return 0;
    }
    if (len <= BUF_SZ)
    {
        unsigned int* ptr = &prvt->raw_outbuf[0];
        for (int cnt  = 0; cnt < BUF_NUM; ++cnt, ptr += HEADER_SIZE+HEADER_OFF+BUF_SZ)
        {
            cache_typ* pcache = reinterpret_cast<cache_typ*>(ptr);
            if (*(pcache->buf()-3) == MAGICNUM+1)
            {
                // means it is free
                *(pcache->buf()-3) = MAGICNUM;
                pcache->set_cmd(SETLIST);
                pcache->set_len(static_cast<unsigned int>(len));
                pcache->h_reserve();
                return pcache;
            }
        }
    }
    else if (len > 255)
    {
        dout << "RM Message size exceeded\n";
        return 0;   // sorry size exceeded
    }
    return cache_typ::h_allocate(len);
}

bool RMSerialSocket::SetListEnd(unsigned int id, cache_typ* pbuf)
{
    if (!Connected())
    {
        GetListEnd(id, pbuf);
        return false;
    }
    if (queue_full(&prvt->outqueue))
    {
        dout << "RM Output queue is full, possible data loss\n";
        GetListEnd(id, pbuf);
        return false;
    }

    unsigned int* buf = pbuf->buf() - 2;                             // this is where the compact packet starts
    serial_header* head = reinterpret_cast<serial_header*>(buf+1);   // header is 4 bytes away
    head->len = (unsigned char)pbuf->get_len();                      // need to get useful data out before overriding
    head->magic = SERIAL_MAGIC;
    head->id = (unsigned char)id;
    if (id > 255)
    {
        dout << "RM Message ID exceeded\n";
    }
    head->seq = prvt->outnum;
    mhash_clear_crc32(reinterpret_cast<mutils_word32*>(buf));
    mhash_crc32(reinterpret_cast<mutils_word32*>(buf), buf+1, (head->len+1)*sizeof(unsigned int));   // the crc is packed at the beginning
    buf[0] = BE(buf[0]);

    int sz = (head->len + 2) * sizeof(unsigned int);
    const timeElapsed nowtime(true);
    if (check_bandwidth_limit(prvt->sendtime, prvt->senddata, nowtime, prvt->datalimit) &&
        (prvt->sock.send((const char*)buf, sz) == sz))
    {
        enforce_bandwidth_limit(prvt->sendtime, prvt->senddata, nowtime, prvt->datalimit, sz);
    }
    ++(prvt->outnum) &= 0x7f;
    queue_bottom(&prvt->outqueue) = pbuf;
    prvt->outqueue.writer = queue_next(prvt->outqueue.writer);

    return true;
}

bool RMSerialSocket::BroadcastEnd(unsigned int id, cache_typ* pbuf)
{
    return SetListEnd(id, pbuf);
}

bool RMSerialSocket::MessageEnd(unsigned int id, cache_typ* pbuf, void*)
{
    if (!Connected())
    {
        GetListEnd(id, pbuf);
        return false;
    }

    unsigned int* buf = pbuf->buf() - 2;                             // this is where the compact packet starts
    serial_header* head = reinterpret_cast<serial_header*>(buf+1);   // header is 4 bytes away
    head->len = (unsigned char)pbuf->get_len();                      // need to get useful data out before overriding
    head->magic = SERIAL_MAGIC;
    head->id = (unsigned char)id;
    if (id > 255)
    {
        dout << "RM Message ID exceeded\n";
    }
    head->seq = 0x80;
    mhash_clear_crc32(reinterpret_cast<mutils_word32*>(buf));
    mhash_crc32(reinterpret_cast<mutils_word32*>(buf), buf+1, (head->len+1)*sizeof(unsigned int));   // the crc is packed at the beginning
    buf[0] = BE(buf[0]);

    int sz = (head->len + 2) * sizeof(unsigned int);
    const timeElapsed nowtime(true);
    if (check_bandwidth_limit(prvt->sendtime, prvt->senddata, nowtime, prvt->datalimit) &&
        (prvt->sock.send((const char*)buf, sz) != sz))
    {
        GetListEnd(id, pbuf);
        return false;
    }
    enforce_bandwidth_limit(prvt->sendtime, prvt->senddata, nowtime, prvt->datalimit, sz);
    GetListEnd(id, pbuf);
    return true;
}

bool RMSerialSocket::RegisterIDX(unsigned int)
{
    return true;
}

bool RMSerialSocket::UnregisterIDX(unsigned int)
{
    return true;
}

bool RMSerialSocket::RegisterID(const char*)
{
    return false;
}

bool RMSerialSocket::QueryID(const char*)
{
    return false;
}

bool RMSerialSocket::QueryID(const char*[], int)
{
    return false;
}

bool RMSerialSocket::ClientID(const char*)
{
    if(prvt->init_flag)
    {
        if (!Connected())
        {
            return false;
        }
        prvt->init_flag = false;
        return SendMessageT(0, CLIENTID);
    }
    return false;
}

bool RMSerialSocket::FlowControl(unsigned int interval)
{
    prvt->intv = interval;
    return SendMessageT(interval, FLOWCONTROL);
}

bool RMSerialSocket::Broadcast(unsigned int id, size_t len, const unsigned int* buf)
{
    cache_typ* pbuf = SetListStart(id, len);
    const unsigned int* bufend = buf + len;
    unsigned int* dest = pbuf->buf();
    while (buf != bufend)
    {
        *dest = *buf;
        ++buf;
        ++dest;
    }
    return BroadcastEnd(id, pbuf);
}

bool RMSerialSocket::SetList(unsigned int id, size_t len, const unsigned int* buf)
{
    cache_typ* pbuf = SetListStart(id, len);
    const unsigned int* bufend = buf + len;
    unsigned int* dest = pbuf->buf();
    while (buf != bufend)
    {
        *dest = *buf;
        ++buf;
        ++dest;
    }
    return SetListEnd(id, pbuf);
}

bool RMSerialSocket::Message(unsigned int id, size_t len, const unsigned int* buf)
{
    cache_typ* pbuf = MessageStart(id, len);
    const unsigned int* bufend = buf + len;
    unsigned int* dest = pbuf->buf();
    while (buf != bufend)
    {
        *dest = *buf;
        ++buf;
        ++dest;
    }
    return MessageEnd(id, pbuf);
}

size_t RMSerialSocket::GetList(unsigned int id, size_t len, unsigned int* buf)
{
    const cache_typ* ptr = GetListStart(id);
    if (ptr)
    {
        size_t sz = ptr->output(len, buf);
        GetListEnd(id, ptr);
        return sz;
    }
    return 0;
}

bool RMSerialSocket::SendMessageT(unsigned int id, int cmd)
{
    // no CRC-32 for command packets
    serial_header sh = {
        SERIAL_MAGIC, static_cast<unsigned char>(id), '\0', static_cast<unsigned char>(cmd)
    };
    if (prvt->sock.send(reinterpret_cast<char*>(&sh), sizeof(serial_header)) != sizeof(serial_header))
    {
        return false;
    }
    enforce_bandwidth_limit(prvt->sendtime, prvt->senddata, timeElapsed(true), prvt->datalimit, sizeof(serial_header));
    return true;
}

void RMSerialSocket::ReceivePacket(bool defercb)
{
    unsigned int begin  = 0;          // Location of unprocessed
    unsigned int more   = 1;          // Indicator of pending packets
    unsigned char* p_crc;             // Temp storage of CRC-32
    unsigned int byte_crc;            // Length of valid CRC-32
    int result;                       // Read result
    bool burstout = false;

    // state = 0 read in up to (READ_BUF * 4) + 1 bytes (more, begin, valid flag)
    //       = 1 find next packet (begin, length, id, cmd)
    //       = 2 move incomplete packet to start location (begin)

    if (prvt->readflag)
    {
        return;
    }
    prvt->readflag = true;

#ifdef SERIAL_DEBUG
    if (prvt->valid >= (READ_BUF * sizeof(unsigned int) + 1))
    {
        dout << "Unable to receive data, buffer is full\n";
    }
#endif
    while (more && (prvt->valid < (READ_BUF * sizeof(unsigned int) + 1)))
    {
        // state 0
        result = prvt->sock.recv(reinterpret_cast<char*>(prvt->in_buf) + prvt->valid, (READ_BUF * sizeof(unsigned int) + 1) - prvt->valid);
        if (result > 0)
        {
#ifdef SERIAL_DEBUG
            dout << "New data\n";
#endif
            prvt->valid += result;
            if (prvt->valid != (READ_BUF * sizeof(unsigned int) + 1))
            {
                more = 0;
            }
            if (prvt->conn_state != '\21')
            {
                prvt->conn_state = '\21';
                burstout = true;
                int counter = 0;
                if (!prvt->reset_flag)
                {
                    for (int tag = prvt->outqueue.reader; (tag != prvt->outqueue.writer) && (counter < 1); tag = queue_next(tag), ++counter)
                    {
                        unsigned int* buf = prvt->outqueue.queue[tag]->buf()- 2;            // this is where the compact packet starts
                        serial_header* head = reinterpret_cast<serial_header*>(buf+1);      // header is 4 bytes away
                        int sz = (head->len + 2) * sizeof(unsigned int);
                        if (prvt->sock.send((char*)buf, sz) == sz)
                        {
                            enforce_bandwidth_limit(prvt->sendtime, prvt->senddata, timeElapsed(true), prvt->datalimit, sz);
                        }
#ifdef SERIAL_DEBUG
                        dout << "Resend output queue " << (int)head->seq << '\n';
#endif
                        prvt->reset_flag = true;
                    }
                }
            }
        }
        else
        {
#ifdef SERIAL_DEBUG
            dout << "No data\n";
#endif
            ++(prvt->errflag);
            more = 0;
            break;
        }
        p_crc = 0;   //reset the CRC-32
        byte_crc = 0;
        // state 1
        while (prvt->valid >= sizeof(unsigned int))
        {
            if (prvt->in_buf[begin] == SERIAL_MAGIC)
            {
                // Magic Number OK
                if (prvt->in_buf[begin+2] == 0)
                {
                    // this is a admin command
                    if (prvt->in_buf[begin+3] == ACKNOWLEDGE)
                    {
                        prvt->reset_flag = false;
#ifdef SERIAL_DEBUG
                        dout << "Ack " << (int)prvt->in_buf[begin+1] << '\n';
#endif

                        // check whether the seq exists, in case of duplicate acks
                        bool found = false;
                        for (int tag = prvt->outqueue.reader; tag != prvt->outqueue.writer; tag = queue_next(tag))
                        {
                            unsigned int* buf = prvt->outqueue.queue[tag]->buf() - 2;
                            if (reinterpret_cast<serial_header*>(buf+1)->seq == prvt->in_buf[begin+1])
                            {
                                found = true;
                                break;
                            }
                        }
                        if (found)
                        {
                            for (int tag = prvt->outqueue.reader; tag != prvt->outqueue.writer; tag = queue_next(tag))
                            {
                                unsigned int* buf = prvt->outqueue.queue[tag]->buf() - 2;
                                unsigned char seq = reinterpret_cast<serial_header*>(buf+1)->seq;
                                GetListEnd(0, prvt->outqueue.queue[tag]);
                                prvt->outqueue.reader = queue_next(tag);
                                if (seq == prvt->in_buf[begin+1])
                                {
                                    break;
                                }
                            }
                        }
                        prvt->valid -= sizeof(unsigned int);
                        begin += sizeof(unsigned int);
                        prvt->recvtime();
                        if (prvt->errflag <= ERR_TOLERANCE)
                        {
                            prvt->errflag = 0;
                        }
                        continue;
                    }
                    else if (prvt->in_buf[begin+3] == GETLIST)
                    {
                        prvt->reset_flag = false;
                        if (defercb)
                        {
                            cache_typ* pcache;
                            if (!queue_full(&prvt->cbqueue) && ((pcache = cache_typ::h_allocate(0)) != 0))
                            {
                                pcache->set_id(prvt->in_buf[begin+1]);
                                pcache->set_cmd(GETLIST);
                                queue_bottom(&prvt->cbqueue) = pcache;
                                prvt->cbqueue.writer = queue_next(prvt->cbqueue.writer);
                            }
                            else
                            {
                                dout << "Callback queue overflow\n";
                            }
                        }
                        else
                        {
                            unsigned int tbuf[HEADER_SIZE+HEADER_OFF];
                            cache_typ* pcache = reinterpret_cast<cache_typ*>(&tbuf[0]);
                            pcache->construct(0);
                            pcache->set_id(prvt->in_buf[begin+1]);
                            pcache->set_cmd(GETLIST);
                            (*(callback[GETLIST]))(callback_arg[GETLIST], prvt->in_buf[begin+1], pcache);
                        }
                        prvt->valid -= sizeof(unsigned int);
                        begin += sizeof(unsigned int);
                        prvt->recvtime();
                        if (prvt->errflag <= ERR_TOLERANCE)
                        {
                            prvt->errflag = 0;
                        }
                        continue;
                    }
                    else if (prvt->in_buf[begin+3] == RESEND)
                    {
                        prvt->reset_flag = false;
                        bool done;
                        if (prvt->in_buf[begin+1] == prvt->outnum)
                        {
                            // clear the output queue so that we don't send anymore old packets
                            for (int tag = prvt->outqueue.reader; tag != prvt->outqueue.writer; tag = queue_next(tag))
                            {
                                GetListEnd(0, prvt->outqueue.queue[tag]);
                            }
                            prvt->outqueue.reader = prvt->outqueue.writer;
                            done = true;
                        }
                        else
                        {
                            done = false;
                            for (int tag = prvt->outqueue.reader; tag != prvt->outqueue.writer; tag = queue_next(tag))
                            {
                                unsigned int* buf = prvt->outqueue.queue[tag]->buf() - 2;
                                unsigned char seq = reinterpret_cast<serial_header*>(buf+1)->seq;
                                if (seq == prvt->in_buf[begin+1])
                                {
                                    done = true;
                                    int sz = (reinterpret_cast<serial_header*>(buf+1)->len + 2) * sizeof(unsigned int);
                                    if (prvt->sock.send((char*)buf, sz) == sz)
                                    {
                                        enforce_bandwidth_limit(prvt->sendtime, prvt->senddata, timeElapsed(true), prvt->datalimit, sz);
                                    }
#ifdef SERIAL_DEBUG
                                    dout << "Resend request packet " << (int)seq << '\n';
#endif
                                    break;
                                }
                                else
                                {
#ifdef SERIAL_DEBUG
                                    dout << "Resend request " << (int)(prvt->in_buf[begin+1]) << " discarded " << (int)seq << '\n';
#endif
                                    GetListEnd(0, prvt->outqueue.queue[tag]);
                                    prvt->outqueue.reader = queue_next(tag);
                                }
                            }
                        }
                        if (!done)
                        {
                            prvt->errflag = ERR_TOLERANCE + 1;
                        }
                        prvt->valid -= sizeof(unsigned int);
                        begin += sizeof(unsigned int);
                        prvt->recvtime();
                        continue;
                    }
                    else if ((prvt->in_buf[begin+3] == RESET) && (prvt->in_buf[begin+1] == 0xff))
                    {
                        prvt->sendtime.clear();
                        prvt->errflag = 0;
                        prvt->innum = 0;        // from 0 to 127
                        prvt->last_resend_innum = 128;
                        for (int tag = prvt->inqueue.reader; tag != prvt->inqueue.writer; tag = queue_next(tag))
                        {
                            GetListEnd(0, prvt->inqueue.queue[tag]);
                        }
                        prvt->inqueue.writer = 0;
                        prvt->inqueue.reader = 0;
                        for (int tag = prvt->cbqueue.reader; tag != prvt->cbqueue.writer; tag = queue_next(tag))
                        {
                            GetListEnd(0, prvt->cbqueue.queue[tag]);
                        }
                        prvt->cbqueue.writer = 0;
                        prvt->cbqueue.reader = 0;
                        prvt->senddata = 0;

                        if (!burstout)
                        {
                            unsigned int datacount = 0;
                            unsigned char seq = 0;
                            if (!prvt->reset_flag)
                            {
                                int counter = 0;
                                int tag;
                                for (tag = prvt->outqueue.reader; (tag != prvt->outqueue.writer) && (counter < 1); tag = queue_next(tag), ++seq, ++counter)
                                {
                                    unsigned int* buf = prvt->outqueue.queue[tag]->buf() - 2;
                                    serial_header* head = reinterpret_cast<serial_header*>(buf+1);
                                    head->seq = seq;
                                    mhash_clear_crc32(reinterpret_cast<mutils_word32*>(buf));
                                    mhash_crc32(reinterpret_cast<mutils_word32*>(buf), buf+1, (head->len+1)*sizeof(unsigned int));         // the crc is packed at the beginning
                                    buf[0] = BE(buf[0]);
                                    int sz = (head->len + 2) * sizeof(unsigned int);
                                    if (prvt->sock.send((char*)buf, sz) != sz)
                                    {
                                        ++seq;
                                        tag = queue_next(tag);
                                        break;
                                    }
                                    datacount += sz;
#ifdef SERIAL_DEBUG
                                    dout << "Resend output queue " << (int)head->seq << '\n';
#endif
                                    prvt->reset_flag = true;
                                }
                                for (; tag != prvt->outqueue.writer; tag = queue_next(tag), ++seq)
                                {
                                    unsigned int* buf = prvt->outqueue.queue[tag]->buf() - 2;
                                    serial_header* head = reinterpret_cast<serial_header*>(buf+1);
                                    head->seq = seq;
                                    mhash_clear_crc32(reinterpret_cast<mutils_word32*>(buf));
                                    mhash_crc32(reinterpret_cast<mutils_word32*>(buf), buf+1, (head->len+1)*sizeof(unsigned int));         // the crc is packed at the beginning
                                    buf[0] = BE(buf[0]);
                                }
                            }
                            if (datacount != 0)
                            {
                                enforce_bandwidth_limit(prvt->sendtime, prvt->senddata, timeElapsed(true), prvt->datalimit, datacount);
                            }
                            prvt->outnum = seq;
                        }
                        if (prvt->outnum == 0)
                        {
                            // we haven't sent out anything yet, time for a alive signal
                            SendMessageT(0xFF, ALIVE);
                        }
                        prvt->valid -= sizeof(unsigned int);
                        begin += sizeof(unsigned int);
                        prvt->recvtime();
                        continue;
                    }
                    else if ((prvt->in_buf[begin+3] == ALIVE) && (prvt->in_buf[begin+1] == 0xff))
                    {
                        prvt->reset_flag = false;
                        prvt->valid -= sizeof(unsigned int);
                        begin += sizeof(unsigned int);
                        prvt->recvtime();
                        if (prvt->errflag <= ERR_TOLERANCE)
                        {
                            prvt->errflag = 0;
                        }
                        continue;
                    }
                    else if (prvt->in_buf[begin+3] == FLOWCONTROL)
                    {
                        prvt->reset_flag = false;
                        prvt->intv = static_cast<unsigned int>(prvt->in_buf[begin+1]);
                        prvt->valid -= sizeof(unsigned int);
                        begin += sizeof(unsigned int);
                        prvt->recvtime();
                        if (prvt->errflag <= ERR_TOLERANCE)
                        {
                            prvt->errflag = 0;
                        }
                        continue;
                    }
                    else if (prvt->in_buf[begin+3] == CLIENTID)
                    {
                        prvt->reset_flag = false;
                        if (defercb)
                        {
                            cache_typ* pcache;
                            if (!queue_full(&prvt->cbqueue) && ((pcache = cache_typ::h_allocate(prvt->in_buf[begin+2])) != 0))
                            {
                                pcache->set_id(prvt->in_buf[begin+1]);
                                pcache->set_cmd(CLIENTID);
                                queue_bottom(&prvt->cbqueue) = pcache;
                                prvt->cbqueue.writer = queue_next(prvt->cbqueue.writer);
                            }
                            else
                            {
                                dout << "Callback queue overflow\n";
                            }
                        }
                        else
                        {
                            unsigned int tbuf[HEADER_SIZE+HEADER_OFF];
                            cache_typ* pcache = reinterpret_cast<cache_typ*>(&tbuf[0]);
                            pcache->construct(0);
                            pcache->set_id(prvt->in_buf[begin+1]);
                            pcache->set_cmd(CLIENTID);
                            (*(callback[CLIENTID]))(callback_arg[CLIENTID], prvt->in_buf[begin+1], pcache);
                        }
                        prvt->valid -= sizeof(unsigned int);
                        begin += sizeof(unsigned int);
                        prvt->recvtime();
                        if (prvt->errflag <= ERR_TOLERANCE)
                        {
                            prvt->errflag = 0;
                        }
                        continue;
                    }     //else if
                }
                else if (byte_crc == sizeof(unsigned int))
                {
                    prvt->reset_flag = false;
                    // this is a message
                    if ((prvt->in_buf[begin+2]+1) * sizeof(unsigned int) > prvt->valid)
                    {
                        // packet is not complete for evaluation
                        prvt->recvtime();
                        break;
                    }
                    mutils_word32 crc;
                    mhash_clear_crc32(&crc);
                    mhash_crc32(&crc, &(prvt->in_buf[begin]), (prvt->in_buf[begin+2]+1)*sizeof(unsigned int));
                    crc = BE((unsigned int)crc);
                    if (memcmp(&crc, p_crc, sizeof(unsigned int)) == 0)
                    {
                        // crc ok
                        if (prvt->in_buf[begin+3] == 0x80)
                        {
                            // Message
                            if (defercb)
                            {
                                cache_typ* pcache;
                                if ((prvt->in_buf[begin+1] == prvt->getid) && ((pcache = SetListStart(0, prvt->in_buf[begin+2])) != 0))
                                {
                                    pcache->set_cmd(MESSAGE);
                                    pcache->set_id(prvt->in_buf[begin+1]);
                                    pcache->set_len(prvt->in_buf[begin+2]);
                                    memcpy(pcache->buf(), &prvt->in_buf[begin+4], prvt->in_buf[begin+2]*sizeof(unsigned int));
                                    prvt->query_buf = pcache;
                                }
                                else if (!queue_full(&prvt->cbqueue) && ((pcache = SetListStart(0, prvt->in_buf[begin+2])) != 0))
                                {
                                    pcache->set_cmd(MESSAGE);
                                    pcache->set_id(prvt->in_buf[begin+1]);
                                    pcache->set_len(prvt->in_buf[begin+2]);
                                    memcpy(pcache->buf(), &prvt->in_buf[begin+4], prvt->in_buf[begin+2]*sizeof(unsigned int));
                                    queue_bottom(&prvt->cbqueue) = pcache;
                                    prvt->cbqueue.writer = queue_next(prvt->cbqueue.writer);
                                }
                                else
                                {
                                    dout << "Callback queue overflow\n";
                                }
                            }
                            else
                            {
                                unsigned int id = prvt->in_buf[begin+1];
                                unsigned int len = prvt->in_buf[begin+2];
                                cache_typ* pcache = reinterpret_cast<cache_typ*>(&prvt->in_buf[begin+4]) - 1;
                                pcache->set_cmd(MESSAGE);
                                pcache->set_len(len);
                                (*(callback[MESSAGE]))(callback_arg[MESSAGE], id, pcache);
                                prvt->in_buf[begin+2] = len;         // restore the length, needed later
                            }
                        }
                        else if (prvt->errflag > ERR_TOLERANCE)
                        {
                        }
                        else if ((prvt->in_buf[begin+3] == prvt->innum) || (prvt->in_buf[begin+3] == 0))
                        {
                            SendMessageT(prvt->in_buf[begin+3], ACKNOWLEDGE);
                            if ((prvt->in_buf[begin+3] == 0) && (prvt->innum != 0))
                            {
                                // Assume a reset condition
                                prvt->innum = 1;
                                prvt->last_resend_innum = 128;
                                for (int tag = prvt->inqueue.reader; tag != prvt->inqueue.writer; tag = queue_next(tag))
                                {
                                    GetListEnd(0, prvt->inqueue.queue[tag]);
                                }
                                prvt->inqueue.writer = 0;
                                prvt->inqueue.reader = 0;
                                for (int tag = prvt->cbqueue.reader; tag != prvt->cbqueue.writer; tag = queue_next(tag))
                                {
                                    GetListEnd(0, prvt->cbqueue.queue[tag]);
                                }
                                prvt->cbqueue.writer = 0;
                                prvt->cbqueue.reader = 0;

                                if (!burstout)
                                {
                                    unsigned char seq = 0;
                                    unsigned int datacount = 0;
                                    int counter = 0;
                                    int tag;
                                    for (tag = prvt->outqueue.reader; (tag != prvt->outqueue.writer) && (counter < 1); tag = queue_next(tag), ++seq, ++counter)
                                    {
                                        unsigned int* buf = prvt->outqueue.queue[tag]->buf() - 2;
                                        serial_header* head = reinterpret_cast<serial_header*>(buf+1);
                                        head->seq = seq;
                                        mhash_clear_crc32(reinterpret_cast<mutils_word32*>(buf));
                                        mhash_crc32(reinterpret_cast<mutils_word32*>(buf), buf+1, (head->len+1)*sizeof(unsigned int));         // the crc is packed at the beginning
                                        buf[0] = BE(buf[0]);
                                        int sz = (head->len + 2) * sizeof(unsigned int);
                                        if (prvt->sock.send((char*)buf, sz) != sz)
                                        {
                                            ++seq;
                                            tag = queue_next(tag);
                                            break;
                                        }
                                        datacount += sz;
                                    }
                                    for (; tag != prvt->outqueue.writer; tag = queue_next(tag), ++seq)
                                    {
                                        unsigned int* buf = prvt->outqueue.queue[tag]->buf() - 2;
                                        serial_header* head = reinterpret_cast<serial_header*>(buf+1);
                                        head->seq = seq;
                                        mhash_clear_crc32(reinterpret_cast<mutils_word32*>(buf));
                                        mhash_crc32(reinterpret_cast<mutils_word32*>(buf), buf+1, (head->len+1)*sizeof(unsigned int));         // the crc is packed at the beginning
                                        buf[0] = BE(buf[0]);
                                    }
                                    if (datacount != 0)
                                    {
                                        enforce_bandwidth_limit(prvt->sendtime, prvt->senddata, timeElapsed(true), prvt->datalimit, datacount);
                                    }
                                    prvt->outnum = seq;
                                }
                            }
                            else
                            {
                                ++(prvt->innum) &= 0x7F;
                            }

                            if (defercb)
                            {
                                cache_typ* pcache;
                                if ((prvt->in_buf[begin+1] == prvt->getid) && ((pcache = SetListStart(0, prvt->in_buf[begin+2])) != 0))
                                {
                                    pcache->set_id(prvt->in_buf[begin+1]);
                                    pcache->set_len(prvt->in_buf[begin+2]);
                                    memcpy(pcache->buf(), &prvt->in_buf[begin+4], prvt->in_buf[begin+2]*sizeof(unsigned int));
                                    prvt->query_buf = pcache;
                                }
                                else if (!queue_full(&prvt->cbqueue) && ((pcache = SetListStart(0, prvt->in_buf[begin+2])) != 0))
                                {
                                    pcache->set_id(prvt->in_buf[begin+1]);
                                    pcache->set_len(prvt->in_buf[begin+2]);
                                    memcpy(pcache->buf(), &prvt->in_buf[begin+4], prvt->in_buf[begin+2]*sizeof(unsigned int));
                                    queue_bottom(&prvt->cbqueue) = pcache;
                                    prvt->cbqueue.writer = queue_next(prvt->cbqueue.writer);
                                }
                                else
                                {
                                    dout << "Callback queue overflow\n";
                                }
                            }
                            else
                            {
                                unsigned int id = prvt->in_buf[begin+1];
                                unsigned int len = prvt->in_buf[begin+2];
                                cache_typ* pcache = reinterpret_cast<cache_typ*>(&prvt->in_buf[begin+4]) - 1;
                                pcache->set_cmd(SETLIST);
                                pcache->set_len(len);
                                (*(callback[SETLIST]))(callback_arg[SETLIST], id, pcache);
                                prvt->in_buf[begin+2] = len;         // restore the length, needed later
                            }

                            // check to see if inqueue has anything of value
                            while (!queue_empty(&prvt->inqueue))
                            {
                                if (queue_top(&prvt->inqueue)->get_seq() == prvt->innum)
                                {
                                    if (defercb)
                                    {
                                        if (!queue_full(&prvt->cbqueue))
                                        {
                                            queue_bottom(&prvt->cbqueue) = queue_top(&prvt->inqueue);
                                            prvt->cbqueue.writer = queue_next(prvt->cbqueue.writer);
                                            prvt->inqueue.reader = queue_next(prvt->inqueue.reader);
                                        }
                                        else
                                        {
                                            dout << "Callback queue overflow\n";
                                        }
                                    }
                                    else
                                    {
                                        cache_typ* pcache = queue_top(&prvt->inqueue);
                                        (*(callback[SETLIST]))(callback_arg[SETLIST], pcache->get_id(), pcache);
                                        GetListEnd(0, pcache);
                                        prvt->inqueue.reader = queue_next(prvt->inqueue.reader);
                                    }
                                    SendMessageT(prvt->innum, ACKNOWLEDGE);
                                    ++(prvt->innum) &= 0x7f;
                                }
                                else
                                {
                                    // check if the next sequence is really far away
                                    unsigned short seq = prvt->innum, seq1 = queue_top(&prvt->inqueue)->get_seq();
                                    int cnt;
                                    for (cnt = 0; cnt < 8; ++cnt, ++seq &= 0x7f)
                                    {
                                        if (seq == seq1)
                                        {
                                            break;
                                        }
                                    }
                                    if (cnt == 8)
                                    {
                                        // trash the queue
                                        for (int tag = prvt->inqueue.reader; tag != prvt->inqueue.writer; tag = queue_next(tag))
                                        {
                                            GetListEnd(0, prvt->inqueue.queue[tag]);
                                        }
                                        prvt->inqueue.writer = 0;
                                        prvt->inqueue.reader = 0;
                                    }
                                    break;
                                }
                            }
                        }
                        else if (((prvt->in_buf[begin+3] + 1) & 0x7f) == prvt->innum)
                        {
                            dout << "Duplicate sequence " << (int)prvt->in_buf[begin+3] << " received. Packet ignored\n";
                            unsigned char lastinnum = (prvt->innum - 1) & 0x7f;
                            SendMessageT(lastinnum, ACKNOWLEDGE);
                        }
                        else if (((prvt->in_buf[begin+3] + 2) & 0x7f) == prvt->innum)
                        {
                            dout << "Old sequence " << (int)prvt->in_buf[begin+3] << " received. Packet ignored\n";
                            unsigned char lastinnum = (prvt->innum - 1) & 0x7f;
                            SendMessageT(lastinnum, ACKNOWLEDGE);
                        }
                        else if (((prvt->in_buf[begin+3] + 3) & 0x7f) == prvt->innum)
                        {
                            dout << "Old sequence " << (int)prvt->in_buf[begin+3] << " received. Packet ignored\n";
                            unsigned char lastinnum = (prvt->innum - 1) & 0x7f;
                            SendMessageT(lastinnum, ACKNOWLEDGE);
                        }
                        else if (((prvt->in_buf[begin+3] + 4) & 0x7f) == prvt->innum)
                        {
                            dout << "Old sequence " << (int)prvt->in_buf[begin+3] << " received. Packet ignored\n";
                            unsigned char lastinnum = (prvt->innum - 1) & 0x7f;
                            SendMessageT(lastinnum, ACKNOWLEDGE);
                        }
                        else if (((prvt->in_buf[begin+3] + 5) & 0x7f) == prvt->innum)
                        {
                            dout << "Old sequence " << (int)prvt->in_buf[begin+3] << " received. Packet ignored\n";
                            unsigned char lastinnum = (prvt->innum - 1) & 0x7f;
                            SendMessageT(lastinnum, ACKNOWLEDGE);
                        }
                        else if (((prvt->in_buf[begin+3] + 6) & 0x7f) == prvt->innum)
                        {
                            dout << "Old sequence " << (int)prvt->in_buf[begin+3] << " received. Packet ignored\n";
                            unsigned char lastinnum = (prvt->innum - 1) & 0x7f;
                            SendMessageT(lastinnum, ACKNOWLEDGE);
                        }
                        else if (((prvt->in_buf[begin+3] + 7) & 0x7f) == prvt->innum)
                        {
                            dout << "Old sequence " << (int)prvt->in_buf[begin+3] << " received. Packet ignored\n";
                            unsigned char lastinnum = (prvt->innum - 1) & 0x7f;
                            SendMessageT(lastinnum, ACKNOWLEDGE);
                        }
                        else if (((prvt->in_buf[begin+3] + 8) & 0x7f) == prvt->innum)
                        {
                            dout << "Old sequence " << (int)prvt->in_buf[begin+3] << " received. Packet ignored\n";
                            unsigned char lastinnum = (prvt->innum - 1) & 0x7f;
                            SendMessageT(lastinnum, ACKNOWLEDGE);
                        }
                        else
                        {
                            if (prvt->last_resend_innum != 128)
                            {
                                for (int cnt1 = 0; cnt1 < 8; ++cnt1)
                                {
                                    ++(prvt->last_resend_innum) &= 0x7f;
                                    if (prvt->in_buf[begin+3] == prvt->last_resend_innum)
                                    {
                                        goto receive_label1;
                                    }
                                }
                            }
#ifdef SERIAL_DEBUG
                            dout << "RM Message resend for packet " << (int)prvt->innum << ", received "
                                 << (int)prvt->in_buf[begin+3] << '\n';
#endif
                            SendMessageT(prvt->innum, RESEND);
                            prvt->last_resend_innum = (prvt->in_buf[begin+3] + 1) & 0x7f;
receive_label1:
                            cache_typ *pcache;
                            if (!queue_full(&prvt->inqueue) && ((pcache = SetListStart(0, prvt->in_buf[begin+2])) != 0))
                            {
                                pcache->set_id(prvt->in_buf[begin+1]);
                                pcache->set_len(prvt->in_buf[begin+2]);
                                pcache->set_seq(prvt->in_buf[begin+3]);
                                memcpy(pcache->buf(), &prvt->in_buf[begin+4], prvt->in_buf[begin+2]*sizeof(unsigned int));
                                queue_bottom(&prvt->inqueue) = pcache;
                                prvt->inqueue.writer = queue_next(prvt->inqueue.writer);
                            }
                            else
                            {
                                dout << "Unable to buffer any more incoming packets\n";
                            }
                        }
                        prvt->valid -= (prvt->in_buf[begin+2]+1) * sizeof(unsigned int);
                        begin += (prvt->in_buf[begin+2]+1) * sizeof(unsigned int);
                        byte_crc = 0;
                        prvt->recvtime();
                        if (prvt->errflag <= ERR_TOLERANCE)
                        {
                            prvt->errflag = 0;
                        }
                        continue;
                    }
                    else
                    {
                        mutils_word32 rcv_crc;
                        memcpy(&rcv_crc, p_crc, sizeof(mutils_word32));
                        // crc check failed
                        dout << "RM Message CRC failed: computed:" << std::hex << crc << " received:" << rcv_crc << '\n';
                        int sz = (prvt->in_buf[begin+2]+1)*sizeof(unsigned int);
                        unsigned char* dumpstr = &(prvt->in_buf[begin]);
                        for (int cnt=0; cnt<sz; ++cnt, ++dumpstr)
                        {
                            dout << (int)*dumpstr << ' ';
                        }
                        dout << '\n';
                    }
                }
            }
            // neither admin nor message
            if (byte_crc < sizeof(unsigned int))
            {
                if (byte_crc == 0)
                {
                    p_crc = &prvt->in_buf[begin];
                }
                ++byte_crc;
            }
            else
            {
                ++p_crc;
//                   if (++(prvt->errflag) > ERR_TOLERANCE)
//                   {
//                       more = 0;
//                       break;
//                   }
//                   dout << "Junk data; increasing errflag\n";
            }
            begin++;
            --(prvt->valid);
        }
        // state 2
        if (byte_crc > 0)
        {
            if (p_crc != prvt->in_buf)
            {
                memmove(prvt->in_buf, p_crc, prvt->valid+byte_crc);
            }
            prvt->valid += byte_crc;
            begin = 0;
        }
        else if ((begin > 0) && (prvt->valid > 0))
        {
            memmove(prvt->in_buf, &prvt->in_buf[begin], prvt->valid);
            begin = 0;
        }
    }
    prvt->readflag = false;
}

bool RMSerialSocket::WaitForHandshakeCompletion(const struct timeval*)
{
    return true;
}

void RMSerialSocket::KeepAlive()
{
    if (Connected())
    {
        const timeElapsed currtime(true);
        if (!queue_empty(&prvt->outqueue) &&
            check_bandwidth_limit(prvt->sendtime, prvt->senddata, currtime, prvt->datalimit))
        {
            int intv = prvt->intv * 1000 / 4;
            if (intv == 0)
            {
                intv = 2000;
            }

            if (prvt->sendtime.elapsed(currtime) >= (unsigned int)(intv))
            {
                // send up to 2 unacknowledged packets at a quarter interval time or
                // in the situation that nothing is heard by a third interval
                int counter = 0;
                for (int tag = prvt->outqueue.reader; (tag != prvt->outqueue.writer) && (counter < NUMBER_OF_PACKETS_SENT_AT_KEEP_ALIVE); tag = queue_next(tag))
                {
                    unsigned int* buf = prvt->outqueue.queue[tag]->buf()- 2;        // this is where the compact packet starts
                    serial_header* head = reinterpret_cast<serial_header*>(buf+1);  // header is 4 bytes away
                    if (head->seq != 0)
                    {
                        // we avoid sending seq 0 because, this seq has the ability to reset the receiver
                        // seq 0 must only be resent upon request, or if there is only one unack packet
                        int sz = (head->len + 2) * sizeof(unsigned int);
                        if (prvt->sock.send((char*)buf, sz) == sz)
                        {
                            enforce_bandwidth_limit(prvt->sendtime, prvt->senddata, currtime, prvt->datalimit, sz);
                        }
#ifdef SERIAL_DEBUG
                        dout << "Keep alive resend type I " << (int)head->seq << " size=" << sz << '\n';
#endif
                        ++counter;
                    }
                }
            }
            else if ((prvt->recvtime.elapsed(currtime) <= 1000U) &&
                     (prvt->sendtime.elapsed(currtime) >= 100U))
            {
                // send up to 1 unacknowledged packets when data is healthly received but not immediately
                // in the situation for half duplex links
                int counter = 0;
                for (int tag = prvt->outqueue.reader; (tag != prvt->outqueue.writer) && (counter < NUMBER_OF_PACKETS_SENT_WHEN_RECV_IS_ALIVE); tag = queue_next(tag))
                {
                    unsigned int* buf = prvt->outqueue.queue[tag]->buf()- 2;        // this is where the compact packet starts
                    serial_header* head = reinterpret_cast<serial_header*>(buf+1);  // header is 4 bytes away
                    if (head->seq != 0)
                    {
                        // we avoid sending seq 0 because, this seq has the ability to reset the receiver
                        // seq 0 must only be resent upon request, or if there is only one unack packet
                        int sz = (head->len + 2) * sizeof(unsigned int);
                        if (prvt->sock.send((char*)buf, sz) == sz)
                        {
                            enforce_bandwidth_limit(prvt->sendtime, prvt->senddata, currtime, prvt->datalimit, sz);
                        }
                        else
                        {
                            dout << "KA II failed\n";
                        }
#ifdef SERIAL_DEBUG
                        dout << "Keep alive resend type II " << (int)head->seq << " size=" << sz << '\n';
#endif
                        ++counter;
                    }
                }
            }
            else if ((prvt->sendtime.elapsed(currtime) >= 1000U) &&
                     (prvt->recvtime.elapsed(currtime) >= 2000U) &&
                     (queue_next(prvt->outqueue.reader) == prvt->outqueue.writer))
            {
                // special consideration if there is only one packet in the queue
                // interval of sending is 1 sec
                // send when nothing is received, may imply that there is no other data from other party
                unsigned int* buf = prvt->outqueue.queue[prvt->outqueue.reader]->buf()- 2; // this is where the compact packet starts
                serial_header* head = reinterpret_cast<serial_header*>(buf+1);  // header is 4 bytes away
                int sz = (head->len + 2) * sizeof(unsigned int);
                if (prvt->sock.send((char*)buf, sz) == sz)
                {
                    enforce_bandwidth_limit(prvt->sendtime, prvt->senddata, currtime, prvt->datalimit, sz);
                }
#ifdef SERIAL_DEBUG
                dout << "Keep alive resend type III " << (int)head->seq << '\n';
#endif
            }
        }

        if (prvt->intv && (prvt->sendtime.elapsed() >= (unsigned int)(prvt->intv * 500)))
        {
            SendMessageT(0xFF, ALIVE);
#ifdef SERIAL_DEBUG
            dout << "Sending alive\n";
#endif
        }
    }
}

bool RMSerialSocket::Flush()
{
    return true;
}

int RMSerialSocket::GetHandle() const
{
    return (int)prvt->sock.get_sock();
}

bool RMSerialSocket::CheckLink()
{
    const bool result1 = (prvt->conn_state == '\21');
    const bool result2 = !(prvt->intv && (prvt->recvtime.elapsed() > (unsigned int)(prvt->intv * 2000)));
    const bool result = result1 && result2;
    return result;
}

bool RMSerialSocket::HandshakeAck(unsigned int, unsigned int)
{
    // Serial does not handshake nor handshake acknowledge
    return true;
}

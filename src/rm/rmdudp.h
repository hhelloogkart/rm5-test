/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _RMDUALUDP_H_
#define _RMDUALUDP_H_

#include "rmsocket.h"

class udp_core;

// Class for UDP/IP connection with RM
struct rmdualudp_socket_private;

class RM_EXPORT RMDUALUDPSocket : public RMBaseSocket
{
public:
    RMDUALUDPSocket();
    RMDUALUDPSocket(const udp_core& asock);
    virtual ~RMDUALUDPSocket();

    // Connection and status
    virtual bool Connect(const char* host, int port, const struct timeval* stm = 0);
    virtual bool Connected();
    virtual bool Close();
    virtual bool Close2();
    virtual int  Select(const struct timeval* stm = 0, bool defercb = false);

    // RM Protocol Calls for Clients
    virtual const cache_typ* GetListStart(unsigned int id);
    virtual void GetListEnd(unsigned int id, const cache_typ* pbuf);
    virtual cache_typ* SetListStart(unsigned int id, size_t len);
    virtual cache_typ* MessageStart(unsigned int id, size_t len);
    virtual bool SetListEnd(unsigned int id, cache_typ* pbuf);
    virtual bool BroadcastEnd(unsigned int id, cache_typ* pbuf);
    virtual bool MessageEnd(unsigned int id, cache_typ* pbuf, void* addr=0);
    virtual bool RegisterID(const char* str);
    virtual bool QueryID(const char* str);
    virtual bool QueryID(const char* str[], int num);
    virtual bool ClientID(const char* client);
    virtual bool RegisterIDX(unsigned int id);
    virtual bool UnregisterIDX(unsigned int id);
    virtual bool FlowControl(unsigned int interval);
    virtual bool WaitForHandshakeCompletion(const struct timeval* stm = 0);
    virtual bool HandshakeAck(unsigned int cmd, unsigned int id);
    virtual void KeepAlive();
    virtual void KeepAlive2();
    virtual bool Flush();
    virtual int  GetHandle() const;
    virtual bool CheckLink();

    // Depreciated and Inefficient RM Protocol Calls
    virtual bool Broadcast(unsigned int id, size_t len, const unsigned int* buf);
    virtual bool SetList(unsigned int id, size_t len, const unsigned int* buf);
    virtual bool Message(unsigned int id, size_t len, const unsigned int* buf);
    virtual size_t GetList(unsigned int id, size_t len, unsigned int* buf);
    bool Resend(unsigned short seq, unsigned short minor_seq);
    bool Acknowledge(unsigned int seq);


    void StartThread1(void);
    void StartThread2(void);
    void KillThread1(void);
    void KillThread2(void);
    int DualSend(char* buf, int txSize);
    int DualRecv(char* pin_buf);
    int DualSelect(const struct timeval* stm, bool defercb = false);

protected:
    void ResetData();
    void ReceivePacket(bool defercb);
    bool PackMessage(cache_typ* pbuf);

private:
    rmdualudp_socket_private* prvt;
};

#endif /* _RMUDP_H_ */

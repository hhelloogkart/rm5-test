/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "rmdata_var.h"
#include "rmsocket.h"
#include "outbuf.h"

rm_data_var::rm_data_var(const char nm[], bool regflag) :
    rm_var(nm, regflag)
{
}

cache_typ* rm_data_var::writebuffer(unsigned int id, size_t len)
{
    return Socket().MessageStart(id, len);
}

bool rm_data_var::write(outbuf& buf)
{
    return Socket().MessageEnd(id, buf.getcache());
}

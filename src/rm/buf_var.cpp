/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include <buf_var.h>
#include <outbuf.h>
#define QUERYBUF 8192

buf_var::buf_var(const char nm[], bool _regflag) :
    rm_var(nm, _regflag),
    data(0), sz(0)
{
    pop();
}

buf_var::~buf_var()
{
    delete[] data;
}

bool buf_var::readData(int len, const unsigned char* buf, bool setrmdirty)
{
    const int wlen = ((len - 1) / sizeof(unsigned int)) + 1;
    const int blen = wlen * sizeof(unsigned int);
    int cnt1 = 0;
    if (wlen != sz)
    {
        delete[] data;
        if (len > 0)
        {
            data = new unsigned int[wlen];
            sz = wlen;
        }
        else
        {
            data = 0;
            sz = 0;
        }
    }
    else
    {
        const unsigned char* const bdata = reinterpret_cast<unsigned char*>(data);
        // same length, test for equivalence
        for (; cnt1 < len; ++cnt1)
        {
            if (bdata[cnt1] != buf[cnt1])
            {
                break;
            }
        }
        for (; cnt1 < blen; ++cnt1)
        {
            if (bdata[cnt1] != '\0')
            {
                break;
            }
        }
        if (cnt1 == blen)
        {
            notDirty();
            return false;
        }
    }
    {
        unsigned char* const bdata = reinterpret_cast<unsigned char*>(data);
        for (; cnt1 < len; ++cnt1)
        {
            bdata[cnt1] = buf[cnt1];
        }
        for (; cnt1 < blen; ++cnt1)
        {
            bdata[cnt1] = '\0';
        }
    }
    setDirty();
    if (setrmdirty)
    {
        setRMDirty();
    }
    return true;
}

int buf_var::extract(int len, const unsigned char* buf)
{
    readData(len, buf, false);
    return 0;
}

int buf_var::size() const
{
    return sz * sizeof(unsigned int);
}
void buf_var::output(outbuf& strm)
{
    char* p = reinterpret_cast< char* >(data);
    char* q = p + size();

    if (p)
    {
        while (p != q)
        {
            strm += *p;
            ++p;
        }
    }
}
bool buf_var::isValid() const
{
    return data != 0;
}
bool buf_var::setInvalid()
{
    return readData(0, 0, false);
}
const mpt_var& buf_var::operator=(const mpt_var& right)
{
    const buf_var* gen = RECAST(const buf_var*,&right);
    if (gen && (gen != this))
    {
        readData(gen->size(), reinterpret_cast<unsigned char*>(gen->data), false);
    }
    else
    {
        notDirty();
    }
    return *this;
}
const buf_var& buf_var::operator=(const buf_var& right)
{
    if (&right != this)
    {
        readData(right.size(), reinterpret_cast<unsigned char*>(right.data), false);
    }
    else
    {
        notDirty();
    }
    return *this;
}
const buf_var& buf_var::operator=(const char* right)
{
    setBuffer(right);
    return *this;
}
const buf_var& buf_var::operator+=(const char* right)
{
    int _sz2 = 0;
    for (; right[_sz2] != '\0'; ++_sz2)
    {
        ;
    }

    if (_sz2 == 0)
    {
        notDirty();
    }
    else if (sz == 0)
    {
        readData(_sz2, reinterpret_cast<const unsigned char*>(right), true);
    }
    else
    {
        // check backwards for nulls
        int _sz1 = sz;
        for (; (_sz1 > 0) && (data[_sz1 - 1] == '\0'); --_sz1)
        {
            ;
        }
        if (_sz1 == 0)
        {
            readData(_sz2, reinterpret_cast<const unsigned char*>(right), true);
        }
        else
        {
            set(_sz1 + _sz2);
            char* buf = reinterpret_cast<char*>(data) + _sz1;
            for (int idx = 0; idx < _sz2; ++idx)
            {
                buf[idx] = right[idx];
            }
            setDirty();
            setRMDirty();
        }
    }
    return *this;
}
bool buf_var::operator==(const mpt_var& right) const
{
    const buf_var* gen = RECAST(const buf_var*,&right);
    if (gen && isValid() && gen->isValid())
    {
        for (int cnt1 = 0; cnt1 < sz; ++cnt1)
        {
            if (data[cnt1] != (*gen).data[cnt1])
            {
                return false;
            }
        }
        return true;
    }
    return false;
}

istream& buf_var::operator>>(istream& s)
{
    int cnt1 = 0;
    setInvalid();

    s >> rmmsgname;
    s >> cnt1;
    regflag = (cnt1 != 0);
    s >> sz;
    if (sz > 0)
    {
        data = new unsigned int [sz];
        for (cnt1 = 0; cnt1 < sz; ++cnt1)
        {
            s >> data[cnt1];
            if (s.fail()) break;
        }
        if (cnt1 == 0)
        {
            setInvalid();
        }
        else
        {
            sz = cnt1;
        }
    }
    setDirty();
    setRMDirty();
    return s;
}

ostream& buf_var::operator<<(ostream& s) const
{
    int cnt1 = 0;

    s << rmmsgname << ' ';
    s << regflag << ' ';
    s << sz;
    for (cnt1 = 0; cnt1 < sz; ++cnt1)
    {
        s << ' ';
        s << data[cnt1];
    }
    return s;
}

void buf_var::setBuffer(const void* p, int _sz)
{
    if (_sz <= 0)
    {
        const char* q = reinterpret_cast<const char*>(p);
        for (_sz = 0; _sz < sz; ++_sz)
        {
            if (q[_sz] == '\0')
            {
                break;
            }
        }
    }
    readData(_sz, static_cast<const unsigned char*>(p), true);
}

unsigned int& buf_var::operator[](int idx)
{
    static unsigned int b = 0xFFFFFFFF;
    return (idx >= sz) ? b : data[idx];
}

void buf_var::set(int _sz)
{
    if (_sz != sz)
    {
        unsigned int* data1 = new unsigned int[_sz];
        const int sz1 = (_sz > sz) ? sz : _sz;
        for (int idx = 0; idx < sz1; ++idx)
        {
            data1[idx] = data[idx];
        }
        delete[] data;
        data = data1;
        sz = _sz;
        setDirty();
        setRMDirty();
    }
    else
    {
        notDirty();
    }
}

mpt_var* buf_var::getNext()
{
    return this + 1;
}

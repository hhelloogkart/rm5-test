/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

//#include "rmutil.h"
#include "rmsocket.h"
#include <time/timecore.h>
#ifndef NO_RMDUDP
#include "rmdudp.h"
#endif
#ifndef NO_RMUDP
#include "rmudp.h"
#endif
#ifndef NO_RMTCP
#include "rmtcp.h"
#endif
#ifndef NO_RM3
#include "rmoldtcp.h"
#endif
#ifndef NO_RMSERIAL
#include "rmserial.h"
#endif
#ifndef NO_SM
#include "rmsm.h"
#endif
#ifndef NO_RMDSERIAL
#include "rmdserial.h"
#endif
#ifndef NO_SPREAD
#include "rmspread.h"
#endif
#ifndef NO_SSM
#include "rmstaticsm.h"
#endif
#ifndef NO_SCRAMNET
#include "rmscram.h"
#endif
#ifndef NO_RMICONN
#include "rmconn.h"
#endif

#include <string.h>

static void doNothing(void*, unsigned int, const cache_typ*)
{
}


RMBaseSocket::RMBaseSocket()
{
    for (int cnt = 0; cnt < ENDCMD; ++cnt)
    {
        callback[cnt]     = &doNothing;
        callback_arg[cnt] = NULL;
    }
}

RMBaseSocket::~RMBaseSocket()
{
}

bool RMBaseSocket::Close()
{
    return true;
}

void RMBaseSocket::AddCallback(unsigned int cmd, void* arg, RMCallback func)
{
    if ( cmd < ENDCMD )
    {
        callback[cmd]     = (func == NULL) ? &doNothing : func;
        callback_arg[cmd] = arg;
    }
}

void RMBaseSocket::Sleep(int msec)
{
    millisleep(msec);
}

RMBaseSocket* RMBaseSocket::Construct(const char* accessstr)
{
#ifndef NO_RMDSERIAL
    if (strcmp(accessstr, "RMDSERIAL") == 0)
    {
        return new RMDSerialSocket;
    }
#endif
#ifndef NO_SM
    if (strcmp(accessstr, "SM") == 0)
    {
        return new RMSMemSocket;
    }
#endif
#ifndef NO_RMTCP
    if (strcmp(accessstr, "TCP") == 0)
    {
        return new RMTCPSocket;
    }
#endif
#ifndef NO_RM3
    if (strcmp(accessstr, "RM3") == 0)
    {
        return new RM3TCPSocket;
    }
#endif
#ifndef NO_RMUDP
    if (strcmp(accessstr, "UDP") == 0)
    {
        return new RMUDPSocket;
    }
#endif
#ifndef NO_RMDUDP
    if (strcmp(accessstr, "DUALUDP") == 0)
    {
        return new RMDUALUDPSocket;
    }
#endif
#ifndef NO_RMSERIAL
    if (strcmp(accessstr, "SERIAL") == 0)
    {
        return new RMSerialSocket;
    }
#endif
#ifndef NO_SPREAD
    if (strcmp(accessstr, "SPREAD") == 0)
    {
        return new RMSpreadSocket;
    }
#endif
#ifndef NO_SSM
    if (strcmp(accessstr, "SSM") == 0)
    {
        return new RMStaticSM;
    }
#endif
#ifndef NO_SCRAMNET
    if (strcmp(accessstr, "SCRAM") == 0)
    {
        return new RMScram;
    }
#endif
#ifndef NO_RMICONN
    if (strcmp(accessstr, "IConn") == 0)
    {
        return new RMIConnSocket;
    }
#endif
    return NULL;

}

int RMBaseSocket::GetHandle() const
{
    return -1;
}

/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _RMSOCKET_H_
#define _RMSOCKET_H_
#include <rmglobal.h>
#include <stddef.h>

class cache_typ;

struct timeval;

// Base class for communication with RM

class RM_EXPORT RMBaseSocket
{
public:
    typedef void (* RMCallback)(void*, unsigned int, const cache_typ*);
    RMBaseSocket();
    virtual ~RMBaseSocket();

    // Connection and status
    virtual bool Connect(const char* host, int port, const struct timeval* stm = 0) = 0;
    virtual bool Connected() = 0;
    virtual bool Close();
    virtual int  Select(const struct timeval* stm = 0, bool defercb = false) = 0;

    // RM Protocol Calls for Clients
    virtual const cache_typ* GetListStart(unsigned int id) = 0;
    virtual void GetListEnd(unsigned int id, const cache_typ* pbuf) = 0;
    virtual cache_typ* SetListStart(unsigned int id, size_t len) = 0;
    virtual cache_typ* MessageStart(unsigned int id, size_t len) = 0;
    virtual bool SetListEnd(unsigned int id, cache_typ* pbuf) = 0;
    virtual bool BroadcastEnd(unsigned int id, cache_typ* pbuf) = 0;
    virtual bool MessageEnd(unsigned int id, cache_typ* pbuf, void* addr=0) = 0;
    virtual bool RegisterID(const char* str) = 0;
    virtual bool QueryID(const char* str) = 0;
    virtual bool QueryID(const char* str[], int num) = 0;
    virtual bool ClientID(const char* client) = 0;
    virtual bool RegisterIDX(unsigned int id) = 0;
    virtual bool UnregisterIDX(unsigned int id) = 0;
    virtual bool FlowControl(unsigned int interval) = 0;
    virtual bool WaitForHandshakeCompletion(const struct timeval* stm = 0) = 0;
    virtual bool HandshakeAck(unsigned int cmd, unsigned int id) = 0;
    virtual void KeepAlive() = 0;
    virtual bool Flush() = 0;
    virtual int  GetHandle() const;
    virtual bool CheckLink() = 0;

    // Depreciated and Inefficient RM Protocol Calls
    virtual bool Broadcast(unsigned int id, size_t len, const unsigned int* buf) = 0;
    virtual bool SetList(unsigned int id, size_t len, const unsigned int* buf) = 0;
    virtual bool Message(unsigned int id, size_t len, const unsigned int* buf) = 0;
    virtual size_t GetList(unsigned int id, size_t len, unsigned int* buf) = 0;

    // Callbacks
    void AddCallback(unsigned int cmd, void* arg, RMCallback func);

    // Utility Functions
    static void Sleep(int msec);
    static RMBaseSocket* Construct(const char*);

protected:
    RMCallback callback[ENDCMD];
    void* callback_arg[ENDCMD];
};

#endif /* _RMSOCKET_H_ */

/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _RMSH_H_
#define _RMSH_H_

#ifdef _MSC_VER
#pragma warning(disable: 4786) // disable warning C4786: symbol greater than 255 character
#endif

#define SOCK_PRIVATE
#include "rmsocket.h"

class var_collection;
class cache_factory;
class queue_factory;
class rmglobal_typ;
struct RMSMemSocket_Private;

// Class for shared memory communication with RM

class RM_EXPORT RMSMemSocket : public RMBaseSocket
{
public:
    RMSMemSocket();
    virtual ~RMSMemSocket();

    // Connection and status
    virtual bool Connect(const char* host, int port, const struct timeval* stm = 0);
    virtual bool Connected();
    virtual bool Close();
    virtual int  Select(const struct timeval* stm = 0, bool defercb = false);

    // RM Protocol Calls for Clients
    virtual const cache_typ* GetListStart(unsigned int id);
    virtual void GetListEnd(unsigned int id, const cache_typ* pbuf);
    virtual cache_typ* SetListStart(unsigned int id, size_t len);
    virtual cache_typ* MessageStart(unsigned int id, size_t len);
    virtual bool SetListEnd(unsigned int id, cache_typ* pbuf);
    virtual bool BroadcastEnd(unsigned int id, cache_typ* pbuf);
    virtual bool MessageEnd(unsigned int id, cache_typ* pbuf, void* addr=0);
    virtual bool RegisterID(const char* str);
    virtual bool QueryID(const char* str);
    virtual bool QueryID(const char* str[], int num);
    virtual bool ClientID(const char* client);
    virtual bool RegisterIDX(unsigned int id);
    virtual bool UnregisterIDX(unsigned int id);
    virtual bool FlowControl(unsigned int interval);
    virtual bool WaitForHandshakeCompletion(const struct timeval* stm = 0);
    virtual bool HandshakeAck(unsigned int cmd, unsigned int id);
    virtual void KeepAlive();
    virtual bool Flush();
    virtual bool CheckLink();

    // Depreciated and Inefficient RM Protocol Calls
    virtual bool Broadcast(unsigned int id, size_t len, const unsigned int* buf);
    virtual bool SetList(unsigned int id, size_t len, const unsigned int* buf);
    virtual bool Message(unsigned int id, size_t len, const unsigned int* buf);
    virtual size_t GetList(unsigned int id, size_t len, unsigned int* buf);

    // Special call for dumping shared memory
    void dump();

protected:
    bool UnclientID();
    unsigned FindFreeClientIDX(const char* host);
    rmglobal_typ& GetGlobalStorage(const char*);

protected:
    var_collection* vc;
    queue_factory* qf;
    unsigned int clientidx;
    RMSMemSocket_Private* prvt;
};

#endif /* _RMSH_H_ */

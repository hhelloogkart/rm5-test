/******************************************************************/
/* Copyright DSO National Laboratories 2004. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "../rmstl.h"
#include <util/dbgout.h>
#include "rmdudp.h"
#include <time/timecore.h>
#include <cache.h>
#include <comm/udpcore.h>
#include <task/threadt.h>

#ifndef NO_PCRE
#include <pcre/pcre.h>
#endif
#include <limits.h>
#include <string.h>

#ifdef VXWORKS
#include <sys/socket.h>
#endif

#ifdef NET_OS
#include <threadx/tx_api.h>
#include <threadx/tx_tim.h>
#include <bsp_api.h>
#endif

#define HEADER_SIZE (sizeof(header_typ) / sizeof(unsigned int))
#define HEADER_OFF  ((sizeof(cache_typ) / sizeof(unsigned int)) - HEADER_SIZE)
#define MSG_ID BE((unsigned)0xFF00)
#define CONT_ID BE((unsigned)0x09022005)
#define MSG_NUM 0xFFFF
#define MAX_SIZE 0xFFFF+HEADER_SIZE
#define INVALID_SEQUENCE 0x8000
#define MAXCACHE    40000
#define MAX_RETRY   0
#define  SOCKALIVE  MAXCACHE +4444
#define  ALIVEMAXTRY    3

unsigned int seqNumber = 0;
char tempBuffer[2048];
unsigned int big_buf[UDP_BUF+HEADER_OFF+sizeof(seqNumber)];    // Temporary input buffer
unsigned int* in_buf = &big_buf[HEADER_OFF]; // Offset to allow convert to Cache to work
int byteRx = 0;

unsigned int cacheBuf[MAXCACHE][1500];
unsigned int cacheCount = 0;
unsigned int cacheCatch = 0;
int retry = MAX_RETRY;

static thread_typ* rx_thread1 = 0;
static thread_typ* rx_thread2 = 0;
static bool thread1Stop = false;
static bool thread2Stop = false;
static int alive1 = 0;
static int alive2 = 0;


typedef LIST(unsigned int*) BufList_Typ;
typedef LIST(cache_typ*) CBList_Typ;

struct rmdualudp_socket_private
{
    udp_core sock1;
    udp_core sock2;
    unsigned int getid;
    unsigned int valid;
    cache_typ* data_pending;
    unsigned int errflag;
    unsigned int cont_innum;
    unsigned int major_innum;
    unsigned short minor_innum;
    unsigned int major_outnum;
    unsigned int minor_outnum;
    unsigned int flow_interval;
    unsigned int receiver_resend_counter;
    unsigned int receiver_last_major;
    unsigned int sender_resend_counter;
    unsigned int sender_last_major;
    bool readflag;
    bool store_flag;
    int sync;
#ifdef NO_THREAD
    timeElapsed in_connect;
#endif
    timeElapsed recvtime;
    timeElapsed sendtime;
    cache_typ* query_buf;
    BufList_Typ buf_list;
    BufList_Typ queue_list;
    unsigned int* outbuf;
    unsigned int outbuf_sz;
    bool broadcast;
    CBList_Typ cb_list;
};

bool recv2(void* arg)
{
    unsigned int big_buf2[UDP_BUF+HEADER_OFF+sizeof(seqNumber)];     // Temporary input buffer
    unsigned int* in_buf2 = &big_buf2[HEADER_OFF];  // Offset to allow convert to Cache to work
    int ret2;
    struct timeval stm = {
        0, 1
    };
    rmdualudp_socket_private* prvt = (rmdualudp_socket_private*)arg;
    if (!thread2Stop)
    {
        ret2 = prvt->sock2.select(&stm);
        if (ret2 > 0)
        {
            byteRx = prvt->sock2.recv((char*)in_buf2, UDP_BUF*sizeof(unsigned int)+sizeof(seqNumber)) -sizeof(seqNumber);
            if (byteRx>0)
            {
                if ( in_buf2[0] == SOCKALIVE )
                {
                    alive2 = 0;
                }
                else
                {
                    cacheBuf[in_buf2[0]][0] = byteRx;
                    memcpy(&cacheBuf[in_buf2[0]][1], &in_buf2[1], byteRx);
                    //miniprintf("nic2 recv %d\n", byteRx);
                    if ( cacheCount < in_buf2[0] +1)
                    {
                        cacheCount = in_buf2[0] +1;
                    }
                    cacheCount %= MAXCACHE;
                }
            }
        }
        return false;
    }
    else
    {
        return true;   // stop thread
    }
}

bool recv1(void* arg)
{
    unsigned int big_buf2[UDP_BUF+HEADER_OFF+sizeof(seqNumber)];     // Temporary input buffer
    unsigned int* in_buf2 = &big_buf2[HEADER_OFF];  // Offset to allow convert to Cache to work
    struct timeval stm = {
        0, 1
    };
    rmdualudp_socket_private* prvt = (rmdualudp_socket_private*)arg;
    if (!thread1Stop)
    {
        const int ret = prvt->sock1.select(&stm);
        if (ret > 0)
        {
            byteRx = prvt->sock1.recv((char*)in_buf2, UDP_BUF*sizeof(unsigned int)+sizeof(seqNumber)) -sizeof(seqNumber);
            if (byteRx>0)
            {
                if ( in_buf2[0] == SOCKALIVE )
                {
                    alive1 = 0;
                }
                else
                {
                    cacheBuf[in_buf2[0]][0] = byteRx;
                    memcpy(&cacheBuf[in_buf2[0]][1], &in_buf2[1], byteRx);
                    //miniprintf("nic1 recv %d\n", byteRx);
                    if ( cacheCount < in_buf2[0] +1)
                    {
                        cacheCount = in_buf2[0] +1;
                    }
                    cacheCount %= MAXCACHE;
                }
            }
        }
        return false;
    }
    else
    {
        return true;   // stop thread
    }
}


void RMDUALUDPSocket::StartThread2(void)
{
    //miniprintf("Starting Thread2\n");
    KillThread2();
    thread2Stop = false;
    if (rx_thread2 == NULL)  //thread t null
    {
        alive2 = 0;
        rx_thread2 = new thread_typ(&::recv2, (void*)prvt, true);
        //miniprintf("Thread2 started %d\n", rx_thread2);
    }
}

void RMDUALUDPSocket::StartThread1(void)
{
    //miniprintf("Starting Thread1\n");
    KillThread1();
    thread1Stop = false;
    if (rx_thread1 == 0)  //thread t null
    {
        alive1 = 0;
        rx_thread1 = new thread_typ(&::recv1, (void*)prvt, true);
        //miniprintf("Thread1 Started %d\n", rx_thread1);
    }
    cacheCatch = cacheCount = 0;
    for (int i=0; i<MAXCACHE; i++)
    {
        cacheBuf[i][0] = 0;
    }
}


void RMDUALUDPSocket::KillThread2(void)
{
    thread2Stop = true;
    if (rx_thread2 != NULL)  //thread not null
    {
        while (!rx_thread2->finished())
        {
            //miniprintf("Waiting for Thread2 to stop\n");
        }
        delete rx_thread2;
        rx_thread2 = NULL;
        //miniprintf("Thread2 Killed\n");
    }
}

void RMDUALUDPSocket::KillThread1(void)
{
    thread1Stop = true;
    if (rx_thread1 != NULL)  //thread not null
    {
        while (!rx_thread1->finished())
        {
            //miniprintf("Waiting for Thread1 to stop\n)";
        }
        delete rx_thread1;
        rx_thread1 = NULL;
        //miniprintf("Thread1 Killed\n");
    }
}

int RMDUALUDPSocket::DualSend(char* buf, int txSize)
{
    int result, result2;
    memcpy(&tempBuffer[0], &seqNumber, sizeof(seqNumber));
    memcpy(&tempBuffer[sizeof(seqNumber)], buf, txSize);

    result2 = prvt->sock2.send(tempBuffer, txSize+sizeof(seqNumber)) -sizeof(seqNumber);
    //miniprintf("nic2 send %d [%d]\n", result2, seqNumber);

    result = prvt->sock1.send(tempBuffer, txSize+sizeof(seqNumber)) -sizeof(seqNumber);
    //miniprintf("nic1 send  %d [%d]\n", result, seqNumber);
    //miniprintf("nic send %d [%d]\n", result, seqNumber);

    ++seqNumber %= MAXCACHE;
    return (result >0) ? result : (result2 >0) ? result2 : result;
}

int RMDUALUDPSocket::DualSelect(const struct timeval*, bool)
{
    if (cacheCatch == cacheCount)
    {
        return 0;
    }
    return 1;
};

int RMDUALUDPSocket::DualRecv(char* pin_buf)
{
    int result = cacheBuf[cacheCatch][0];// Read result
    if (result == 0)
    {
        retry--;
        if (retry<0)
        {
            retry = MAX_RETRY;
            miniprintf("<<<<<<<<<<< miss frame %d retry %d>>>>>>>>>>>>>>>>>>>>\n", cacheCatch, retry);
        }
        return 0;
    }
    memcpy(pin_buf, &cacheBuf[cacheCatch][1], result);
    //miniprintf("nic recv %d <<%d>>\n", result, cacheCatch);
    cacheBuf[cacheCatch][0] = 0;
    return result;
}

static bool check_broadcast(const char* host)
{
#ifdef NO_PCRE
    int len = strlen(host);
    int state = 0;
    while(len--)
    {
        if(state == 0 && host[len] == '5')
        {
            state = 1;
        }
        else if(state == 0 && !isspace(host[len]))
        {
            return false;
        }
        else if(state == 1 && host[len] == '5')
        {
            state = 2;
        }
        else if(state == 2 && host[len] == '2')
        {
            state = 3;
        }
        else if(state == 3 && host[len] == '.')
        {
            state = 4;
        }
        else if(state == 4 && isdigit(host[len]))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    return false;
#else
    const char* errptr;
    int erroffset;
    int ovector[3];
    pcre* re = pcre_compile("\\d+\\.\\d+\\.\\d+\\.255$", 0, &errptr, &erroffset, 0);
    if (re == 0)
    {
        dout << "Error compiling RE " << errptr << " at offset " << erroffset << '\n';
        return false;
    }
    return pcre_exec(re, 0, host, strlen(host), 0, 0, ovector, 3) >= 0;
#endif
}

RMDUALUDPSocket::RMDUALUDPSocket() : prvt(new rmdualudp_socket_private)
{
    prvt->readflag = false;
    cache_typ* pbuf = cache_typ::h_allocate(UDP_BUF);
    prvt->outbuf = pbuf->buf();
    prvt->outbuf[0] = MSG_ID;
    prvt->data_pending = NULL;
    prvt->query_buf = NULL;
    prvt->flow_interval = 0;
    prvt->store_flag = false;
    prvt->broadcast = false;
    ResetData();
}

RMDUALUDPSocket::RMDUALUDPSocket(const udp_core& asock) :
    prvt(new rmdualudp_socket_private)
{
    prvt->sock1 = asock;
    prvt->readflag = false;
    cache_typ* pbuf = cache_typ::h_allocate(UDP_BUF);
    prvt->outbuf = pbuf->buf();
    prvt->outbuf[0] = MSG_ID;
    prvt->data_pending = NULL;
    prvt->query_buf = NULL;
    prvt->flow_interval = 0;
    prvt->store_flag = false;
    ResetData();
}

RMDUALUDPSocket::~RMDUALUDPSocket()
{
    prvt->sock1.close();
    if (prvt->data_pending != NULL)
    {
        prvt->data_pending->h_unreserve();
    }
    if (prvt->query_buf != NULL)
    {
        prvt->query_buf->h_unreserve();
    }

    {
        cache_typ* pbuf = (cache_typ*)prvt->outbuf - 1;
        pbuf->h_unreserve();
    }

    for (BufList_Typ::iterator iter = prvt->buf_list.begin(); iter != prvt->buf_list.end(); ++iter)
    {
        cache_typ* pbuf = (cache_typ*)(*iter) - 1;
        pbuf->h_unreserve();
    }
    for (BufList_Typ::iterator iter = prvt->queue_list.begin(); iter != prvt->queue_list.end(); ++iter)
    {
        cache_typ* pbuf = (cache_typ*)(*iter) - 1;
        pbuf->h_unreserve();
    }
    for (CBList_Typ::iterator iter1 = prvt->cb_list.begin(); iter1 != prvt->cb_list.end(); ++iter1)
    {
        (*iter1)->h_unreserve();
    }
    delete prvt;
}

bool RMDUALUDPSocket::Connect(const char* host, int port, const struct timeval* deftv)
{
    char* pstrIndex = const_cast<char*>(strstr(host, ";"));
    *pstrIndex++ = '\0';
    prvt->broadcast = check_broadcast(host);
    if (!prvt->sock1.connected() || !prvt->sock2.connected())
    {
        miniprintf("nic1: host: [%s] port: [%d]\n", host, port);
        miniprintf("nic2: host: [%s] port: [%d]\n", pstrIndex, port+1);
        prvt->sock2.sethost(pstrIndex, port +1);
        prvt->sock2.bind();

        StartThread2();
        prvt->sock1.sethost(host, port);
        prvt->sock1.bind();

        StartThread1();
        if (prvt->broadcast)
        {
            prvt->sync = 1;
            prvt->errflag = 0;
            return true;
        }
        Flush();
        cache_typ* pbuf = SetListStart(0, 0);
        pbuf->set_id(0);
        pbuf->set_seq(MSG_NUM);
        pbuf->set_cmd(RESET);
        if (PackMessage(pbuf))
        {
            Flush();
            prvt->sync = -1;
        }
#ifdef NO_THREAD
        prvt->in_connect();
#endif
    }
    else if (prvt->sync == 2)
    {
        Flush();
        cache_typ* pbuf = SetListStart(0, 0);
        pbuf->set_id(0);
        pbuf->set_seq(MSG_NUM);
        pbuf->set_cmd(RESET);
        if (PackMessage(pbuf))
        {
            Flush();
            prvt->sync = -1;
        }
#ifdef NO_THREAD
        prvt->in_connect();
#endif
    }

    *(--pstrIndex) = ';';
#ifdef NO_THREAD
    unsigned flow_interval = (prvt->flow_interval? prvt->flow_interval : 15);
    struct timeval tv = {
        0, 10000
    };
    if (deftv)
    {
        tv = *deftv;
    }
    Select(&tv, true);
    if (prvt->sync == 1)
    {
        return true;
    }
    else if ((prvt->sync == -1) &&
             (prvt->in_connect.elapsed() > flow_interval*1000U)) //sec before resend reset
    {
        prvt->sync = 2;
    }
#else
    struct timeval tv = {
        5, 0
    };
    if (deftv)
    {
        tv = *deftv;
    }
    Select(&tv, true);
    if (prvt->sync == 1)
    {
        return true;
    }
    else if ((prvt->sync==-1) && ((++prvt->errflag) == 3))
    {
        prvt->errflag = 0;
        prvt->sync = 2;
    }
#endif
    return false;
}

bool RMDUALUDPSocket::Connected()
{
    if (!prvt->sock1.connected() && !prvt->sock2.connected())
    {
        return false;
    }
    if (prvt->sync == 1)
    {
        return true;
    }
    return false;
}

bool RMDUALUDPSocket::Close()
{
    Close2();
    ResetData();
    return prvt->sock1.close();
}

bool RMDUALUDPSocket::Close2()
{
    //miniprintf("nic 2 Socket closed\n");
    return prvt->sock2.close();
}

void RMDUALUDPSocket::ResetData()
{
#ifdef NO_THREAD
    prvt->in_connect = 0;
#endif
    prvt->sync = 0;
    prvt->getid = UINT_MAX;
    prvt->errflag = 0;
    prvt->valid = 0;
    prvt->sender_last_major = INVALID_SEQUENCE;
    prvt->sender_resend_counter = INVALID_SEQUENCE;
    prvt->receiver_last_major = INVALID_SEQUENCE;
    prvt->receiver_resend_counter = INVALID_SEQUENCE;

    if (prvt->query_buf != NULL)
    {
        prvt->query_buf->h_unreserve();
        prvt->query_buf = NULL;
    }
    if(prvt->store_flag)
    {
        cache_typ* pbuf = (cache_typ*)prvt->outbuf - 1;
        //miniprintf("SendPacket::Block %d, length %d\n", prvt->major_outnum, prvt->outbuf_sz);
        pbuf->set_len(prvt->outbuf_sz);
        pbuf->h_reserve();
        prvt->buf_list.push_back(prvt->outbuf);
        pbuf = cache_typ::h_allocate(UDP_BUF);
        prvt->outbuf = pbuf->buf();
        prvt->outbuf[0] = MSG_ID;
    }
    prvt->store_flag = false;
    prvt->recvtime.clear();
    prvt->sendtime.clear();
    unsigned int cnt = 0;
    for (BufList_Typ::iterator iter = prvt->queue_list.begin(); iter != prvt->queue_list.end(); ++iter)
    {
        cache_typ* pbuf = (cache_typ*)(*iter) - 1;
        pbuf->h_unreserve();
    }
    prvt->queue_list.clear();
    for (BufList_Typ::iterator iter = prvt->buf_list.begin(); iter != prvt->buf_list.end(); ++iter, ++cnt)
    {
        *(*iter) = BE(cnt);
        cache_typ* pbuf = (cache_typ*)(*iter) - 1;
        pbuf->h_reserve();
        prvt->queue_list.push_back(*iter);
    }
    prvt->major_innum = 0;
    prvt->minor_innum = 0;
    prvt->major_outnum = cnt;
    prvt->minor_outnum = 0;
    for (CBList_Typ::iterator iter1 = prvt->cb_list.begin(); iter1 != prvt->cb_list.end(); ++iter1)
    {
        (*iter1)->h_unreserve();
    }
    prvt->cb_list.clear();
    prvt->outbuf_sz = 1;
    if (prvt->data_pending != NULL)
    {
        prvt->data_pending->h_unreserve();
        prvt->data_pending = NULL;
    }
}

bool RMDUALUDPSocket::FlowControl(unsigned int interval)
{
    prvt->flow_interval = interval;
    cache_typ* pbuf = SetListStart(0, 0);
    pbuf->set_id(interval);
    pbuf->set_seq(MSG_NUM);
    pbuf->set_cmd(FLOWCONTROL);
    return PackMessage(pbuf);
}

int RMDUALUDPSocket::Select(const struct timeval* stm, bool defercb)
{
    unsigned int cmd;
    CBList_Typ::iterator it;

    if (prvt->sync == 1)
    {
        if ( !prvt->queue_list.empty() )
        {
            unsigned int* obuf = prvt->queue_list.front();
            cache_typ* pbuf1 = (cache_typ*)obuf - 1;
            int plen = pbuf1->get_len() * sizeof(unsigned int);
            if (DualSend((char*)obuf, plen) == plen)
            {
                pbuf1->h_unreserve();
                prvt->queue_list.pop_front();
                prvt->sendtime();
            }
        }
        else
        {
            KeepAlive();
        }
    }
    if (!defercb)
    {
        while (prvt->cb_list.size())
        {
            it = prvt->cb_list.begin();
            cmd = (*it)->get_cmd();
            if (cmd < ENDCMD)
            {
                (*(callback[cmd]))(callback_arg[cmd], (*it)->get_id(), *it);
            }
            const_cast<cache_typ*>(*it)->h_unreserve();
            prvt->cb_list.pop_front();
        }
    }
    int ret = (stm == NULL) ? 1 : DualSelect(stm);
    if (ret > 0)
    {
        ret = 1;
        if (cacheCatch != cacheCount)
        {
            while(cacheCatch != cacheCount)
            {
                ReceivePacket(defercb);
                cacheCatch++;
                cacheCatch %= MAXCACHE;
                //miniprintf("Catch:%d   Count:%d \n", cacheCatch, cacheCount);
            }
        }
    }
    else if(ret < 0)
    {
        return -1;
    }
    else if(!CheckLink())
    {
        return -1;
    }
    if (prvt->sync == 1)
    {
        if( !prvt->queue_list.empty() )
        {
            for (int cnt = 0; (cnt < 3) && !prvt->queue_list.empty(); ++cnt)
            {
                unsigned int* obuf = prvt->queue_list.front();
                cache_typ* pbuf1 = (cache_typ*)obuf - 1;
                int plen = pbuf1->get_len() * sizeof(unsigned int);
                if (DualSend((char*)obuf, plen) == plen)
                {
                    pbuf1->h_unreserve();
                    prvt->queue_list.pop_front();
                    prvt->sendtime();
                }
            }
        }
        else if ( !prvt->buf_list.empty() )
        {
            BufList_Typ::iterator iter = prvt->buf_list.begin();
            for (int cnt = 0; (cnt < 3) && (iter !=prvt->buf_list.end()); ++cnt, ++iter)
            {
                unsigned int* obuf = *iter;
                cache_typ* pbuf1 = (cache_typ*)obuf - 1;
                int plen = pbuf1->get_len() * sizeof(unsigned int);
                if (DualSend((char*)obuf, plen) == plen)
                {
                    prvt->sendtime();
                }
                else
                {
                    break;
                }
            }
        }
        else
        {
            KeepAlive();
        }
    }
    return ret;
}

size_t RMDUALUDPSocket::GetList(unsigned int id, size_t len, unsigned int* buf)
{
    const cache_typ* ptr = GetListStart(id);
    if(ptr)
    {
        size_t sz = ptr->output(len, buf);
        GetListEnd(id, ptr);
        return sz;
    }
    return 0;
}

const cache_typ* RMDUALUDPSocket::GetListStart(unsigned int id)
{
    int cnt = 0, ret;
    if (prvt->sync != 1)
    {
        return NULL;
    }

    cache_typ* pbuf = SetListStart(0, 0);
    pbuf->set_id(id);
    pbuf->set_seq(MSG_NUM);
    pbuf->set_cmd(GETLIST);
    PackMessage(pbuf);
    Flush();
    prvt->getid = id;
    do
    {
        if (cnt != 0)
        {
            Sleep(WAIT_TIME);
        }
        struct timeval tm = {
            0, WAIT_TIME*1000
        };
        ret = Select(&tm, true);
        ++cnt;
    }
    while ((prvt->query_buf == NULL) && (cnt < GETLIST_RETRY) && (ret != -1));
    cache_typ* ptr = prvt->query_buf;
    prvt->getid = UINT_MAX;
    prvt->query_buf = NULL;
    return ptr;
}

void RMDUALUDPSocket::GetListEnd(unsigned int, const cache_typ* pbuf)
{
    // need to check whether the pbuf is real or fake
    if (((unsigned long)pbuf->packet() < (unsigned long)prvt->outbuf) ||
        ((unsigned long)pbuf->packet() >= (unsigned long)prvt->outbuf + UDP_BUF * sizeof(unsigned int)))
    {
        const_cast<cache_typ*>(pbuf)->h_unreserve();
    }
}
cache_typ* RMDUALUDPSocket::MessageStart(unsigned int id, size_t len)
{
    return SetListStart(id, len);
}

cache_typ* RMDUALUDPSocket::SetListStart(unsigned int, size_t len)
{
    if ((int)len <= UDP_BUF - 3 - (int)prvt->outbuf_sz)
    {
        prvt->outbuf[prvt->outbuf_sz] = BE(MAGICNUM);
        cache_typ* pbuf = (cache_typ*)((char*)&prvt->outbuf[prvt->outbuf_sz] - sizeof(cache_typ) + sizeof(header_typ));
        pbuf->set_len(len);
        return pbuf;
    }
    else if (len <= UDP_BUF - 4)
    {
        Flush();
        prvt->outbuf[prvt->outbuf_sz] = BE(MAGICNUM);
        cache_typ* pbuf = (cache_typ*)((char*)&prvt->outbuf[prvt->outbuf_sz] - sizeof(cache_typ) + sizeof(header_typ));
        pbuf->set_len(len);
        return pbuf;
    }
    return cache_typ::h_allocate(len);
}

bool RMDUALUDPSocket::SetListEnd(unsigned int id, cache_typ* pbuf)
{
    bool ret;
    pbuf->set_id(id);
    pbuf->set_cmd(SETLIST);
    pbuf->set_seq(prvt->minor_outnum);
    prvt->store_flag = true;

    ret = PackMessage(pbuf);
    if (ret)
    {
        ++(prvt->minor_outnum);
        prvt->minor_outnum &= 0x7FFF;
    }
    return ret;
}

bool RMDUALUDPSocket::SetList(unsigned int id, size_t len, const unsigned int* buf)
{
    cache_typ* pbuf = SetListStart(id, len);
    const unsigned int* bufend = buf + len;
    unsigned int* dest = pbuf->buf();
    while (buf != bufend)
    {
        *dest = *buf;
        ++buf;
        ++dest;
    }
    return SetListEnd(id, pbuf);
}

bool RMDUALUDPSocket::MessageEnd(unsigned int id, cache_typ* pbuf, void* addr)
{
    if (addr != NULL)
    {
        // check addr, and flush if different
        if (!prvt->sock1.compare_addr(*reinterpret_cast<struct sockaddr_in*>(addr), 1))
        {
            Flush();
            // set new address
            prvt->sock1.sethost(*reinterpret_cast<struct sockaddr_in*>(addr), 1);
        }
    }

    pbuf->set_id(id);
    pbuf->set_cmd(MESSAGE);
    pbuf->set_seq(MSG_NUM);
    return PackMessage(pbuf);
}

bool RMDUALUDPSocket::Message(unsigned int id, size_t len, const unsigned int* buf)
{
    cache_typ* pbuf = SetListStart(id, len);
    const unsigned int* bufend = buf + len;
    unsigned int* dest = pbuf->buf();
    while (buf != bufend)
    {
        *dest = *buf;
        ++buf;
        ++dest;
    }
    return MessageEnd(id, pbuf);
}

bool RMDUALUDPSocket::BroadcastEnd(unsigned int id, cache_typ* pbuf)
{
    bool ret;
    pbuf->set_id(id);
    pbuf->set_cmd(BROADCAST);
    pbuf->set_seq(prvt->minor_outnum);
    prvt->store_flag = true;

    ret = PackMessage(pbuf);
    if (ret)
    {
        ++(prvt->minor_outnum);
        prvt->minor_outnum &= 0x7FFF;
    }
    return ret;
}

bool RMDUALUDPSocket::Broadcast(unsigned int id, size_t len, const unsigned int* buf)
{
    cache_typ* pbuf = SetListStart(id, len);
    const unsigned int* bufend = buf + len;
    unsigned int* dest = pbuf->buf();
    while (buf != bufend)
    {
        *dest = *buf;
        ++buf;
        ++dest;
    }
    return BroadcastEnd(id, pbuf);
}

bool RMDUALUDPSocket::Acknowledge(unsigned int seq)
{
    cache_typ* pbuf = SetListStart(0, 0);
    pbuf->set_id(0);
    pbuf->set_seq(seq);
    pbuf->set_cmd(ACKNOWLEDGE);
    return PackMessage(pbuf);
}

bool RMDUALUDPSocket::Resend(unsigned short seq, unsigned short minor_seq)
{
    cache_typ* pbuf = SetListStart(0, 0);
    pbuf->set_id(minor_seq);
    pbuf->set_seq(seq);
    pbuf->set_cmd(RESEND);
    return PackMessage(pbuf);
}

void RMDUALUDPSocket::KeepAlive2()
{
    //if (++alive2 >= ALIVEMAXTRY) miniprintf("nic2 connection NOT OK\n");
    const unsigned tempSeqNumber = seqNumber;
    memcpy(&tempBuffer[1], &seqNumber, sizeof(seqNumber));
    seqNumber = SOCKALIVE;
    memcpy(&tempBuffer[0], &seqNumber, sizeof(seqNumber));
    prvt->sock2.send(tempBuffer, sizeof(seqNumber)*2);
    miniprintf("nic2 send ALIVE signal [[%d]]\n", seqNumber);
    seqNumber = tempSeqNumber;
}

void RMDUALUDPSocket::KeepAlive( )
{
    if ((prvt->sync != 1) || (prvt->flow_interval==0) || prvt->broadcast ||
        (prvt->sendtime.elapsed() < prvt->flow_interval*500U))
    {
        return;
    }

    KeepAlive2();
    //if (++alive1 >= ALIVEMAXTRY) miniprintf("nic1 connection NOT OK\n");

    const unsigned tempSeqNumber = seqNumber;
    memcpy(&tempBuffer[1], &seqNumber, sizeof(seqNumber));
    seqNumber = SOCKALIVE;
    memcpy(&tempBuffer[0], &seqNumber, sizeof(seqNumber));
    prvt->sock1.send(tempBuffer, sizeof(seqNumber)*2);
    miniprintf("nic1 send ALIVE signal [[%d]]\n", seqNumber);
    seqNumber = tempSeqNumber;

    cache_typ* pbuf = SetListStart(0, 0);
    pbuf->set_id(0);
    pbuf->set_seq(MSG_NUM);
    pbuf->set_cmd(ALIVE);
    PackMessage(pbuf);
    Flush();
}

bool RMDUALUDPSocket::RegisterIDX(unsigned int)
{
    return true;
}

bool RMDUALUDPSocket::UnregisterIDX(unsigned int)
{
    return true;
}

bool RMDUALUDPSocket::RegisterID(const char*)
{
    return false;
}

bool RMDUALUDPSocket::QueryID(const char*)
{
    return false;
}

bool RMDUALUDPSocket::QueryID(const char*[], int)
{
    return false;
}

bool RMDUALUDPSocket::ClientID(const char*)
{
    return false;
}

bool RMDUALUDPSocket::PackMessage(cache_typ* pbuf)
{
    if( (prvt->sync == 1) || (pbuf->get_cmd() == RESET) )
    {
        // check to see if the pbuf is actually outbuf
        if (((unsigned long)pbuf->packet() >= (unsigned long)prvt->outbuf) &&
            ((unsigned long)pbuf->packet() < (unsigned long)prvt->outbuf + UDP_BUF * sizeof(unsigned int)))
        {
            if (pbuf->packet() == &prvt->outbuf[prvt->outbuf_sz])
            {
                // let's just increase outbuf_sz
                prvt->outbuf_sz += HEADER_SIZE + pbuf->get_len();
            }
            else
            {
                // case where a flush was called in between Start and End
                // check if there is enough to pack it in
                if ((pbuf->get_len() + HEADER_SIZE + prvt->outbuf_sz) > UDP_BUF)
                {
                    Flush();
                }

                unsigned int* pbuf_start = pbuf->packet();
                unsigned int* pbuf_end = pbuf_start + pbuf->get_len() + HEADER_SIZE;
                while (pbuf_start != pbuf_end)
                {
                    prvt->outbuf[prvt->outbuf_sz] = *pbuf_start;
                    ++prvt->outbuf_sz;
                    ++pbuf_start;
                }
            }
        }
        // pbuf is not outbuf
        else
        {
            // the length must exceed outbuf size so let's try to squeeze whatever we can
            unsigned int leftover = pbuf->get_len() + prvt->outbuf_sz + HEADER_SIZE - UDP_BUF;
            prvt->store_flag = true; // long messages MUST have a block ID
            unsigned int* pbuf_start;
            for (pbuf_start = pbuf->packet(); prvt->outbuf_sz < UDP_BUF; ++pbuf_start, ++prvt->outbuf_sz)
            {
                prvt->outbuf[prvt->outbuf_sz] = *pbuf_start;
            }
            Flush();
            // when we pack the rest into packets
            while (leftover >= UDP_BUF-2)
            {
                prvt->store_flag = true;
                prvt->outbuf[1] = CONT_ID;
                unsigned int* ob = prvt->outbuf + 2;
                unsigned int* ob_end = prvt->outbuf + UDP_BUF;
                for (; ob != ob_end; ++ob, ++pbuf_start)
                {
                    *ob = *pbuf_start;
                }
                prvt->outbuf_sz = UDP_BUF;
                leftover -= UDP_BUF-2;
                Flush();
            }
            // finally, we have the last, unfilled packet
            prvt->store_flag = true;
            prvt->outbuf_sz += leftover + 1;
            prvt->outbuf[1] = CONT_ID;
            for (unsigned int* ob1 = prvt->outbuf + 2; leftover > 0; ++pbuf_start, ++ob1, --leftover)
            {
                *ob1 = *pbuf_start;
            }
        }

        // there isn't enough left for even one more packet
        if (prvt->outbuf_sz + HEADER_SIZE > UDP_BUF)
        {
            Flush();
        }
        return true;
    }
    else
    {
        // reinstate and copy
        GetListEnd(0, pbuf);
        return false;
    }
}

void RMDUALUDPSocket::ReceivePacket(bool defercb)
{
    unsigned int length;             // Length of good packet
    unsigned int begin;              // Location of unprocessed
    unsigned int id;                 // ID of good packet
    unsigned int cmd;                // Command of good packet
    unsigned int cnt1;               // Misc counters
    int result;                      // Read result
    unsigned int big_buf[UDP_BUF+HEADER_OFF];    // Temporary input buffer
    unsigned int* in_buf = &big_buf[HEADER_OFF]; // Offset to allow convert to Cache to work
    bool outofsync;

    if (prvt->readflag)
    {
        return;
    }
    prvt->readflag = true;

    // 1. receive one packet
    result = DualRecv((char*)in_buf);
    if (result >= 4)
    {
        if(prvt->sync != 1)
        {
            KeepAlive2();
            prvt->sync = 1;
            cache_typ* pbuf = SetListStart(0, 0);
            pbuf->set_id(0);
            pbuf->set_seq(MSG_NUM);
            pbuf->set_cmd(ALIVE);
            PackMessage(pbuf);
            Flush();
        }
    }
    else
    {
        //miniprintf("Receive result %d\n", result);
        goto receive_error;
    }

    // 2. check big packet header
    if (in_buf[0] != MSG_ID) // guaranteed block
    {
        if (in_buf[0] != BE(prvt->major_innum)) // big packet number mismatched
        {
            outofsync = true;
            // check to see if we are receiving historical packets
            {
                unsigned int decr = prvt->major_innum - 1;
                for (cnt1 = 0; cnt1 < 16; ++cnt1, --decr)
                {
                    decr &= 0x7FFF;
                    if (in_buf[0] == BE(decr))
                    {
                        Acknowledge((prvt->major_innum - 1) & 0x7FFF);
                        if (in_buf[1] != CONT_ID)
                        {
                            goto receive_step3;
                        }
                        else
                        {
                            goto receive_exit;
                        }
                    }
                }
            }
            // check for possibility of a reset
            if ((in_buf[0] == 0) && (prvt->major_innum < 0x7F0))
            {
                Acknowledge(0);
                prvt->major_innum = 1;
                prvt->minor_innum = 0;
                prvt->receiver_last_major = INVALID_SEQUENCE;
                outofsync = false;
                goto receive_step3;
            }
            if( prvt->receiver_last_major == INVALID_SEQUENCE)
            {
                miniprintf("ReceivePacket::RESEND CMD is SENT for packet [%d], received [%d]\n", prvt->major_innum, BE(in_buf[0]));
                prvt->receiver_resend_counter = 0;
            }
            else if(BE(in_buf[0]) <= prvt->receiver_last_major)
            {
                miniprintf("ReceivePacket::RESEND CMD is SENT for packet [%d], received [%d]\n", prvt->major_innum, BE(in_buf[0]));
                prvt->receiver_resend_counter = ++(prvt->receiver_resend_counter)&0x7FFF;
            }
            Resend(prvt->major_innum, prvt->receiver_resend_counter);
            prvt->receiver_last_major = BE(in_buf[0]);
            if (in_buf[1] == CONT_ID)
            {
                goto receive_exit;
            }
        }
        else
        {
            //miniprintf("ReceivePacket::Block %d, length %d\n", prvt->major_innum, result / 4);
            Acknowledge(prvt->major_innum);
            prvt->major_innum = ++(prvt->major_innum) & 0x7FFF;
            prvt->receiver_last_major = INVALID_SEQUENCE;
            outofsync = false;
        }
    }
    else
    {
        //miniprintf("ReceivePacket::Non-guaranteed block, length %d\n", result / 4);
        outofsync = false;
    }
receive_step3:
    prvt->recvtime();

    // 3. check for special reset packet
    if ((in_buf[0] == MSG_ID) &&
        (BE(in_buf[2]) >> 24 == RESET))
    {
        prvt->getid = UINT_MAX;
        prvt->valid = 0;
        if (prvt->query_buf != NULL)
        {
            prvt->query_buf->h_unreserve();
            prvt->query_buf = NULL;
        }
        if(prvt->store_flag)
        {
            cache_typ* pbuf = (cache_typ*)prvt->outbuf - 1;
            // miniprintf("SendPacket::Block %d, length %d\n", prvt->major_outnum, prvt->outbuf_sz);
            pbuf->set_len(prvt->outbuf_sz);
            pbuf->h_reserve();
            prvt->buf_list.push_back(prvt->outbuf);
            pbuf = cache_typ::h_allocate(UDP_BUF);
            prvt->outbuf = pbuf->buf();
            prvt->outbuf[0] = MSG_ID;
        }
        prvt->outbuf_sz = 1;
        prvt->store_flag = false;
        unsigned int cnt = 0;
        for (BufList_Typ::iterator iter = prvt->queue_list.begin(); iter != prvt->queue_list.end(); ++iter)
        {
            cache_typ* pbuf = (cache_typ*)(*iter) - 1;
            pbuf->h_unreserve();
        }
        prvt->queue_list.clear();
        for (BufList_Typ::iterator iter = prvt->buf_list.begin(); iter != prvt->buf_list.end(); ++iter, ++cnt)
        {
            *(*iter) = BE(cnt);
            cache_typ* pbuf = (cache_typ*)(*iter) - 1;
            pbuf->h_reserve();
            prvt->queue_list.push_back(*iter);
        }
        prvt->major_innum = 0;
        prvt->minor_innum = 0;
        prvt->major_outnum = cnt;
        prvt->minor_outnum = 0;
        for (CBList_Typ::iterator iter1 = prvt->cb_list.begin(); iter1 != prvt->cb_list.end(); ++iter1)
        {
            (*iter1)->h_unreserve();
        }
        prvt->cb_list.clear();
        if (prvt->data_pending != NULL)
        {
            prvt->data_pending->h_unreserve();
            prvt->data_pending = NULL;
        }
        prvt->readflag = false;
        prvt->receiver_last_major = INVALID_SEQUENCE;
        prvt->receiver_resend_counter = INVALID_SEQUENCE;
        prvt->sender_last_major = INVALID_SEQUENCE;
        prvt->sender_resend_counter = INVALID_SEQUENCE;
        return;
    }

    // 4. check if we need to join to previous packet
    if ((prvt->data_pending != NULL) && !outofsync && (in_buf[0] == BE(prvt->cont_innum)) && (in_buf[1] == CONT_ID))
    {
        int cpylen = prvt->data_pending->get_len()*sizeof(unsigned int) - prvt->valid;
        int pktsz = static_cast<int>(result - 2*sizeof(unsigned int));
        if (cpylen > pktsz)
        {
            // still not long enough
            memcpy(((char*)prvt->data_pending->buf())+prvt->valid, &in_buf[2], pktsz);
            prvt->valid += pktsz;
            ++prvt->cont_innum;
            prvt->readflag = false;
            return;
        }
        else
        {
            memcpy(((char*)prvt->data_pending->buf())+prvt->valid, &in_buf[2], cpylen);
            if (prvt->data_pending->get_id() == prvt->getid)
            {
                prvt->query_buf = prvt->data_pending;
            }
            else if (defercb)
            {
                prvt->cb_list.push_back(prvt->data_pending);
            }
            else
            {
                (*(callback[prvt->data_pending->get_cmd()]))(callback_arg[prvt->data_pending->get_cmd()], prvt->data_pending->get_id(), prvt->data_pending);
                prvt->data_pending->h_unreserve();
            }
            prvt->data_pending = NULL;
            prvt->valid = 0;
            begin = 2 + (cpylen / sizeof(unsigned int));
        }
    }
    else
    {
        begin = 1;
    }
    result /= sizeof(unsigned int);

    // 5. normal processing of packets, begin=array location in in_buf to start, result=array location after the end
    while ((unsigned int)result >= HEADER_SIZE + begin)
    {
        if (in_buf[begin] == BE(MAGICNUM))
        {
            // Magic Number OK
            cnt1 = BE(in_buf[begin+2]) >> 16;
            cmd = BE(in_buf[begin + 1]) >> 24;
            id  = BE(in_buf[begin + 1]) & 0xFFFFFF;

            if (cmd >= ENDCMD)
            {
                ++begin;
                continue;
            }
            if ((((cmd == SETLIST) || (cmd == BROADCAST)) && ((cnt1 == prvt->minor_innum) || (prvt->minor_innum == 0) || (cnt1 == 0))) ||
                ((cmd == MESSAGE) && (cnt1 == MSG_NUM)))
            {
                if (!outofsync && ((cmd == SETLIST) || (cmd == BROADCAST)))
                {
                    if (cnt1 == prvt->minor_innum)
                    {
                        // Frame Count OK
                        prvt->minor_innum = ++(prvt->minor_innum) & 0xFFFF;
                        prvt->errflag = 0;
                    }
                    else if ((prvt->minor_innum == 0) || (cnt1 == 0))
                    {
                        prvt->minor_innum = ++(cnt1) & 0xFFFF;
                        prvt->errflag = 0;
                    }
                }
                length = BE(in_buf[begin+2]) & 0xFFFF;
                if ((result - begin) >= (length+HEADER_SIZE))
                {
                    if (!outofsync || (cmd == MESSAGE))
                    {
                        if (id == prvt->getid)
                        {
                            prvt->query_buf = cache_typ::h_duplicate(id, length, in_buf+begin+HEADER_SIZE);
                        }
                        else if (defercb)
                        {
                            cache_typ* p = cache_typ::h_duplicate(id, length, in_buf+begin+HEADER_SIZE);
                            p->set_cmd(cmd);
                            prvt->cb_list.push_back(p);
                        }
                        else
                        {
                            cache_typ* p = cache_typ::ConvertToCache(&in_buf[begin]);
                            (*(callback[cmd]))(callback_arg[cmd], id, p);
                        }
                    }
                    begin += HEADER_SIZE + length;
                }
                else
                {
                    if (!outofsync)
                    {
                        prvt->data_pending = cache_typ::h_allocate(length);
                        prvt->data_pending->copy_header(&(in_buf[begin]));
                        prvt->valid = (result - begin - HEADER_SIZE) * sizeof(unsigned int);
                        //miniprintf("Valid = %d, Pending length = %d\n", prvt->valid, length*4);
                        memcpy(prvt->data_pending->buf(), &(in_buf[begin+HEADER_SIZE]), prvt->valid);
                        prvt->cont_innum = prvt->major_innum;
                    }
                    prvt->readflag = false;
                    return;
                }
            }
            else if (cmd == ACKNOWLEDGE)
            {
                if (id != 0)
                {
                    ++begin;
                    continue;
                }
                // dout << "Ack " << cnt1 << " Clearing ";
                bool more = false;
                for(BufList_Typ::iterator iter = prvt->buf_list.begin(); (iter != prvt->buf_list.end()); ++iter)
                {
                    if(*(*iter) == BE(cnt1))
                    {
                        more = true;
                        break;
                    }
                }
                for(BufList_Typ::iterator iter = prvt->buf_list.begin(); (iter != prvt->buf_list.end()) && more; iter = prvt->buf_list.erase(iter))
                {
                    if(*(*iter) == BE(cnt1))
                    {
                        more = false;
                    }
                    // dout << BE(*(*iter)) << ' ';
                    cache_typ* cache = (cache_typ*)(*iter) - 1;
                    cache->h_unreserve();
                }
                begin += HEADER_SIZE;
                // dout << "\n";
            }
            else if (cmd == RESEND)
            {
                //check resend big pkt number
                if((cnt1 == prvt->sender_last_major) && (id == prvt->sender_resend_counter))//duplicate resends
                {
                    //do nothing
                    begin += HEADER_SIZE;
                    continue;
                }
                //proceed to compaction then resend
                prvt->sender_resend_counter = id;
                prvt->sender_last_major = cnt1;

                //clear the queue_list
                BufList_Typ::iterator iter;
                for (iter = prvt->queue_list.begin(); iter != prvt->queue_list.end(); ++iter)
                {
                    cache_typ* pbuf = (cache_typ*)(*iter) - 1;
                    pbuf->h_unreserve();
                }
                prvt->queue_list.clear();

                for(iter = prvt->buf_list.begin(); iter != prvt->buf_list.end(); ++iter)
                {
                    if(*(*iter) == BE(cnt1))
                    {
                        break;
                    }
                }

                //compact the packets so that more can be sent out in one burst
                if ((iter == prvt->buf_list.end()) && (cnt1 != prvt->major_outnum))
                {
                    if (++prvt->errflag > ERR_TOLERANCE)
                    {
                        miniprintf("Unable to locate outnum %d for resend. Resetting.\n", cnt1);
                        ResetData();
                        prvt->sync = 1;
                        cache_typ* pbuf = SetListStart(0, 0);
                        pbuf->set_id(0);
                        pbuf->set_seq(MSG_NUM);
                        pbuf->set_cmd(RESET);
                        PackMessage(pbuf);
                    }
                    else
                    {
                        miniprintf("Unable to locate outnum %d for resend. Some data may be lost.\n", cnt1);
                        iter = prvt->buf_list.begin();
                    }
                }

                if (iter != prvt->buf_list.end())
                {
                    // Remove useless minor packets
                    int leftover = 0;
                    bool keepleftover;
                    BufList_Typ::iterator p_end;
                    // Eliminate the first batch of continued packets
                    for (p_end = iter; p_end != prvt->buf_list.end(); ++p_end)
                    {
                        if (*(*p_end+1) != CONT_ID)
                        {
                            break;
                        }
                    }
                    // Start compacting here
                    for (; p_end != prvt->buf_list.end(); ++p_end)
                    {
                        unsigned int src, dst;
                        if (leftover != 0)
                        {
                            if (*(*p_end+1) == CONT_ID)
                            {
                                if (leftover >= UDP_BUF - 2)
                                {
                                    if (!keepleftover)
                                    {
                                        ((cache_typ*)(*p_end) - 1)->set_len(0);
                                    }
                                    leftover -= UDP_BUF - 2;
                                    continue;
                                }
                                if (keepleftover)
                                {
                                    src = leftover + 2;
                                    dst = leftover + 2;
                                }
                                else
                                {
                                    src = leftover + 2;
                                    dst = 1; // this will override the CONT_ID
                                }
                                leftover = 0;
                            }
                            else
                            {
                                dout << "Compactor cannot find spanning packet\n";
                                leftover = 0;
                                unsigned int* buf = *p_end;
                                unsigned int end = ((cache_typ*)(*p_end) - 1)->get_len();
                                for (src = 1; src < end; ++src)
                                {
                                    if (buf[src] == BE(MAGICNUM))
                                    {
                                        break;
                                    }
                                }
                                if (src == end)
                                {
                                    ((cache_typ*)(*p_end) - 1)->set_len(0);
                                    continue;
                                }
                                dst = 1;
                            }
                        }
                        else if (*(*p_end+1) == CONT_ID)
                        {
                            dout << "Compactor detected invalid spanning packet\n";
                            continue;
                        }

                        if (*(*p_end+1) == BE(MAGICNUM))
                        {
                            src = 1;
                            dst = 1;
                        }
                        else if (*(*p_end+1) != CONT_ID)
                        {
                            dout << "Compactor detected invalid packet\n";
                            continue;
                        }

                        unsigned int end = ((cache_typ*)(*p_end) - 1)->get_len();
                        unsigned int* buf = *p_end;
                        while (src < end)
                        {
                            // first look for magic number
                            if (buf[src] != BE(MAGICNUM))
                            {
                                dout << "Compactor detected space between packets\n";
                                ++src;
                                continue;
                            }
                            unsigned int cmd1 = BE(buf[src + 1]) >> 24;
                            if (cmd1 >= ENDCMD)
                            {
                                dout << "Compactor detected invalid cmd\n";
                                ++src;
                                continue;
                            }
                            unsigned int length1 = BE(buf[src+2]) & 0xFFFF;
                            if (length1 + src + HEADER_SIZE > end)
                            {
                                if ((cmd1 == SETLIST) || (cmd1 == BROADCAST))
                                {
                                    if (src != dst)
                                    {
                                        memmove(buf+dst, buf+src, sizeof(unsigned int)*(UDP_BUF - src));
                                    }
                                    dst += UDP_BUF - src;
                                    leftover = length1 + HEADER_SIZE + src - UDP_BUF;
                                    keepleftover = true;
                                    src = end;
                                }
                                else if (cmd1 == MESSAGE)
                                {
                                    leftover = length1 + HEADER_SIZE + src - UDP_BUF;
                                    keepleftover = false;
                                    src = end;
                                }
                                else
                                {
                                    ++src;
                                    dout << "Compactor detected invalid length\n";
                                }
                                continue;
                            }
                            if ((cmd1 == SETLIST) ||
                                (cmd1 == BROADCAST) ||
                                (cmd1 == ACKNOWLEDGE) ||
                                (cmd1 == RESEND) ||
                                (cmd1 == REGISTERIDX) ||
                                (cmd1 == UNREGISTERIDX) ||
                                (cmd1 == QUERYID) ||
                                (cmd1 == CLIENTID) ||
                                (cmd1 == REGISTERID) ||
                                (cmd1 == FLOWCONTROL))
                            {
                                // dout << '*' << (BE(buf[src+2]) >> 16) << ' ';
                                if (src != dst)
                                {
                                    memmove(buf+dst, buf+src, sizeof(unsigned int)*(length1 + HEADER_SIZE) );
                                }
                                dst += HEADER_SIZE + length1;
                            }
                            src += HEADER_SIZE + length1;
                        }
                        ((cache_typ*)(*p_end) - 1)->set_len(dst);
                    }

                    // Recombine the left over data into major packets
                    BufList_Typ compact_buf;
                    for (BufList_Typ::iterator p_start = iter; p_start != prvt->buf_list.end(); p_start = p_end)
                    {
                        cache_typ* cache = (cache_typ*)(*p_start) - 1;
                        p_end = p_start;
                        ++p_end;
                        for (; p_end != prvt->buf_list.end(); ++p_end)
                        {
                            cache_typ* src = (cache_typ*)(*p_end) - 1;
                            unsigned int len = src->get_len();
                            if (len <= 1)
                            {
                                continue;
                            }
                            else if (cache->get_len() + len <= UDP_BUF)
                            {
                                memcpy(*p_start + cache->get_len(), *p_end + 1, (len-1)*sizeof(unsigned int));
                                src->h_unreserve();
                                cache->set_len(cache->get_len() + len - 1);
                            }
                            else
                            {
                                break;
                            }
                        }
                        *(*p_start) = BE(cnt1++); // renumber the sequence number
                        compact_buf.push_back(*p_start);
                        // at the same time we push the packet into the queue
                        cache->h_reserve();
                        prvt->queue_list.push_back(*p_start);
                    }
                    prvt->buf_list.swap(compact_buf);
                    prvt->major_outnum = cnt1;
                }
                begin += HEADER_SIZE;
            }
            else if (cmd == GETLIST)
            {
                if (cnt1 != MSG_NUM)
                {
                    ++begin;
                    continue;
                }
                if (defercb)
                {
                    cache_typ* p = cache_typ::h_duplicate(id, 0, in_buf+begin+HEADER_SIZE);
                    p->set_cmd(cmd);
                    prvt->cb_list.push_back(p);
                }
                else
                {
                    cache_typ* p = cache_typ::ConvertToCache(&in_buf[begin]);
                    (*(callback[cmd]))(callback_arg[cmd], id, p);
                }
                begin += HEADER_SIZE;
            }
            else if (cmd == ALIVE)
            {
                if (cnt1 != MSG_NUM)
                {
                    ++begin;
                    continue;
                }
                begin += HEADER_SIZE;
            }
            else if (cmd == FLOWCONTROL)
            {
                if (cnt1 != MSG_NUM)
                {
                    ++begin;
                    continue;
                }
                prvt->flow_interval = id;
                begin += HEADER_SIZE;
            }
            else if ((cmd == SETLIST) || (cmd == BROADCAST))
            {
                // case where the seq number was not matched
                length = BE(in_buf[begin+2]) & 0xFFFF;
                begin += HEADER_SIZE + length;
                if (!outofsync)
                {
                    int decr = prvt->minor_innum, incr = prvt->minor_innum;
                    int cnt2;
                    for (cnt2 = 0; cnt2 < 100; ++cnt2)
                    {
                        if ((decr == (int)cnt1) || (incr == (int)cnt1))
                        {
                            break;
                        }
                        decr = (decr - 1) & 0xFFFF;
                        incr = (incr + 1) & 0xFFFF;
                    }
                    if ((cnt2 == 100) && (++prvt->errflag > ERR_TOLERANCE))
                    {
                        miniprintf("Resetting minor sequence number, data lost\n");
                        prvt->minor_innum = 0;
                        prvt->errflag = 0;
                    }
                    else
                    {
                        miniprintf("Sequence number error, difference %d\n", cnt2);
                    }
                }
            }
            else
            {
                ++begin;
            }
        }
        else
        {
            ++begin;
        }
    }
    prvt->readflag = false;
    return;
    // Error condition
receive_error:
    if (prvt->sock1.hasCriticalError())
    {
        prvt->errflag = ERR_TOLERANCE;
    }
    else
    {
        ++prvt->errflag;
    }
    miniprintf("Receive ERROR\n");
receive_exit:
    prvt->readflag = false;
}

bool RMDUALUDPSocket::Flush()
{
    if (prvt->sync < 0)
    {
        return false;
    }
    if (prvt->outbuf_sz == 1)
    {
        if (prvt->sync == 1)
        {
            // check to see if there is anything in the transmit queue
            if( !prvt->queue_list.empty() )
            {
                for (int cnt = 0; (cnt < 5) && !prvt->queue_list.empty(); ++cnt)
                {
                    unsigned int* obuf = prvt->queue_list.front();
                    cache_typ* pbuf1 = (cache_typ*)obuf - 1;
                    int plen = pbuf1->get_len() * sizeof(unsigned int);
                    if (DualSend((char*)obuf, plen) == plen)   ///dwx
                    {
                        pbuf1->h_unreserve();
                        prvt->queue_list.pop_front();
                        prvt->sendtime();
                    }
                }
            }
        }//else if buf list not empty
        return true;
    }

    cache_typ* pbuf = (cache_typ*)prvt->outbuf - 1;
    if(prvt->store_flag)
    {
        // miniprintf("SendPacket::Block %d, length %d\n", prvt->major_outnum, prvt->outbuf_sz);
        prvt->outbuf[0] = BE(prvt->major_outnum);
        (++(prvt->major_outnum)) &= 0x7FFF;
        pbuf->set_len(prvt->outbuf_sz);
        pbuf->h_reserve();
        prvt->buf_list.push_back(prvt->outbuf);
    }
    // check to see if there is anything in the transmit queue
    if( !prvt->queue_list.empty() )
    {
        for (int cnt = 0; (cnt < 5) && !prvt->queue_list.empty(); ++cnt)
        {
            unsigned int* obuf = prvt->queue_list.front();
            cache_typ* pbuf1 = (cache_typ*)obuf - 1;
            int plen = pbuf1->get_len() * sizeof(unsigned int);
            if (DualSend((char*)obuf, plen) == plen)
            {
                pbuf1->h_unreserve();
                prvt->queue_list.pop_front();
                prvt->sendtime();
            }
        }
    }
    else
    {
        goto send_direct;
    }
    // check again
    if (prvt->queue_list.empty())
    {
send_direct:
        if (DualSend((char*)prvt->outbuf, prvt->outbuf_sz * sizeof(unsigned int)) == (int)(prvt->outbuf_sz * sizeof(unsigned int)))  ///dwx
        {
            if (prvt->store_flag)
            {
                pbuf->h_unreserve();
                cache_typ* cache = cache_typ::h_allocate(UDP_BUF);
                prvt->outbuf = cache->buf();
                prvt->outbuf[0] = MSG_ID;
            }
            prvt->sendtime();
        }
        else
        {
            goto send_indirect;
        }
    }
    else
    {
send_indirect:
        pbuf->set_len(prvt->outbuf_sz);
        prvt->queue_list.push_back(prvt->outbuf);
        cache_typ* cache = cache_typ::h_allocate(UDP_BUF);
        prvt->outbuf = cache->buf();
        prvt->outbuf[0] = MSG_ID;
    }
    prvt->outbuf_sz = 1;
    prvt->store_flag = false;
    return true;
}

bool RMDUALUDPSocket::WaitForHandshakeCompletion(const struct timeval*)
{
    return true;
}

int RMDUALUDPSocket::GetHandle() const
{
    return (int)prvt->sock1.get_sock();
}

bool RMDUALUDPSocket::CheckLink()
{
    return (prvt->flow_interval == 0) || prvt->broadcast || ((prvt->sync==1) && (prvt->recvtime.elapsed() <= prvt->flow_interval*1000));
}

bool RMDUALUDPSocket::HandshakeAck(unsigned int, unsigned int)
{
    // UDP does not handshake nor handshake acknowledge
    return true;
}

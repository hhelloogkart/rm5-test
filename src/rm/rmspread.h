/******************************************************************/
/* Copyright DSO National Laboratories 2008. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _RMSPREAD_H_
#define _RMSPREAD_H_

#include "rmsocket.h"

struct rmspread_socket_private;

// Class for Spread connection with RM

class RM_EXPORT RMSpreadSocket : public RMBaseSocket
{
public:
    RMSpreadSocket();
    virtual ~RMSpreadSocket();
    // Connection and status
    virtual bool Connect(const char* host, int port, const struct timeval* stm = 0);
    virtual bool Connected();
    virtual bool Close();
    virtual int  Select(const struct timeval* stm = 0, bool defercb = false);

    // RM Protocol Calls for Clients
    virtual const cache_typ* GetListStart(unsigned int id);
    virtual void GetListEnd(unsigned int id, const cache_typ* pbuf);
    virtual cache_typ* SetListStart(unsigned int id, size_t len);
    virtual cache_typ* MessageStart(unsigned int id, size_t len);
    virtual bool SetListEnd(unsigned int id, cache_typ* pbuf);
    virtual bool BroadcastEnd(unsigned int id, cache_typ* pbuf);
    virtual bool MessageEnd(unsigned int id, cache_typ* pbuf, void* addr=0);
    virtual bool RegisterID(const char* str);
    virtual bool QueryID(const char* str);
    virtual bool QueryID(const char* str[], int num);
    virtual bool ClientID(const char* client);
    virtual bool RegisterIDX(unsigned int id);
    virtual bool UnregisterIDX(unsigned int id);
    virtual bool FlowControl(unsigned int interval);
    virtual bool WaitForHandshakeCompletion(const struct timeval* stm = 0);
    virtual bool HandshakeAck(unsigned int cmd, unsigned int id);
    virtual void KeepAlive();
    virtual bool Flush();
    virtual int  GetHandle() const;
    virtual bool CheckLink();

    // Depreciated and Inefficient RM Protocol Calls
    virtual bool Broadcast(unsigned int id, size_t len, const unsigned int* buf);
    virtual bool SetList(unsigned int id, size_t len, const unsigned int* buf);
    virtual bool Message(unsigned int id, size_t len, const unsigned int* buf);
    virtual size_t GetList(unsigned int id, size_t len, unsigned int* buf);

protected:
    void ReceivePacket(bool defercb);

    rmspread_socket_private* prvt;
};

#endif /* _RMSPREAD_H_ */

/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "../rmstl.h"
#include <rmmsg_var.h>
#include <gen_var.h>
#include <arry_var.h>
#include <outbuf.h>
#include <cache.h>
#include <defaults.h>
#include <defutil.h>
#include <rm_ver.h>
#include "rmsocket.h"
#include <util/dbgout.h>
#include <util/utilcore.h>
#include <time/timecore.h>
#include <stdarg.h>
#include <assert.h>

#ifndef NO_RMINTERF
#include "rminterf.h"
#endif

extern "C" RM_IMPORT void minivsprintf(char* buf, const char* fmt, va_list ap);

typedef VECTOR(rm_var*) rmlist_typ;

static char hostname[256] = "";
static char clientname[RMNAME_LEN] = "";
static int hostport = 0;
static struct timeval select_delay = {
    0, 100000
};
static unsigned int flow_interval = FLOW_TIMEOUT;
static RMBaseSocket* client_socket = (RMBaseSocket*)NULL;

void (*rm_var::connectCB)();

static rmlist_typ& rmlist()
{
    static rmlist_typ v;
    return v;

}

static protocol_description& description()
{
    static protocol_description d = {
        0,
        &rm_var::init,
        &rm_var::incoming,
        &rm_var::outgoing,
        &rm_var::deinit
    };
    return d;
}

static void _doSetList(void*, unsigned int id, const cache_typ* buf)
{
    if (id <= rmlist().size())
    {
        if (buf->get_len() != 0)
        {
            rmlist()[id-1]->extract(buf->get_len()*sizeof(unsigned int),
                                    (unsigned char*)(buf->buf()));
        }
        else
        {
            rmlist()[id-1]->setInvalid();
        }
    }
    else
    {
        dout << "Error rx id " << id << " > " << rmlist().size() << endl << flush;
    }
}

static void _doGetList(void*, unsigned int id, const cache_typ*)
{
    if (id <= rmlist().size())
    {
        rmlist()[id-1]->setRMDirty();
    }
    else
    {
        dout << "Error rx id " << id << " > " << rmlist().size() << endl << flush;
    }
}

static int queryid = 1;

static void _doClientID(void*, unsigned int, const cache_typ* buf)
{
    if(buf->get_len()!=0)
    {
        client_socket->HandshakeAck(CLIENTID, 0);
        queryid = 1;
    }
}

static void _doQueryID(void*, unsigned int, const cache_typ* buf)
{
    if(buf->get_len()!=0)
    {
        if (queryid >= static_cast<int>(rmlist().size()))
        {
            dout << "Error rx queryid " << queryid << " > list size\n";
        }
        else if (strcmp((char*)buf->buf(), rmlist()[queryid]->getName()) != 0)
        {
            dout << "Error rx queryid " << (char*)buf->buf() << " != " << rmlist()[queryid]->getName() << '\n';
        }
        client_socket->HandshakeAck(QUERYID, queryid);
        ++queryid;
    }
}

static void _doRegisterID(void*, unsigned int, const cache_typ* buf)
{
    if(buf->get_len()!=0)
    {
        if (queryid >= static_cast<int>(rmlist().size()))
        {
            dout << "Error rx registerid " << queryid << " > list size\n";
        }
        else if (strcmp((char*)buf->buf(), rmlist()[queryid]->getName()) != 0)
        {
            dout << "Error rx registerid " << (char*)buf->buf() << " != " << rmlist()[queryid]->getName() << '\n';
        }
        client_socket->HandshakeAck(REGISTERID, queryid);
        ++queryid;
    }
}

#ifndef NO_EXITSHOW
class RM_EXPORT var_exit : public rm_message_var
{
public:
    RMMSGCONST(var_exit, "", true);
    generic_var<int> value;
};
static var_exit* v_ex = 0;
#endif

#ifndef NO_CLIENT_VER
class RM_EXPORT var_client_name : public rm_var
{
public:
    RMCONST(var_client_name, "", false)
    ext_array_str_var<0, 0, 0> value;
};
static var_client_name* v_cl = 0;
#endif

rm_var::rm_var(const char nm[], bool _regflag) :
    regflag(_regflag), dirty(false)
{
    rmclient_init::install_protocol(description(), false);
    if (nm[0] != '\0')
    {
        rm_strcpy(rmmsgname, RMNAME_LEN, nm);
    }
    rmlist().push_back(this);
    setID(static_cast<unsigned long>(rmlist().size()));
}

void rm_var::setName(const char _name[])
{
    rm_strcpy(rmmsgname, RMNAME_LEN, _name);
}

bool rm_var::chkName(const char _name[]) const
{
    return strcmp(_name, rmmsgname) == 0;
}

bool rm_var::registerRM()
{
    if (Socket().RegisterIDX(id))
    {
        regflag = true;
        return true;
    }
    return false;
}

bool rm_var::unregisterRM()
{
    if (Socket().UnregisterIDX(id))
    {
        regflag = false;
        return true;
    }
    return false;
}

rm_var* rm_var::findVariable(const char _name[])
{
    rmlist_typ::iterator p;
    for (p = rmlist().begin(); p!= rmlist().end(); ++p)
    {
        if ((*p)->chkName(_name))
        {
            return (*p);
        }
    }
    return 0;
}

void rm_var::setRMDirty()
{
    dirty = true;
}

bool rm_var::isRMDirty() const
{
    return dirty;
}

bool rm_var::queryRM()
{
    const cache_typ* ptr = Socket().GetListStart(id);
    if (ptr)
    {
        extract(ptr->get_len()*sizeof(unsigned int), (unsigned char*)(ptr->buf()));
        Socket().GetListEnd(id, ptr);
        return true;
    }
    return false;
}

istream& rm_var::operator>>(istream& s)
{
    s >> rmmsgname;
    s >> regflag;
    return complex_var::operator>>(s);
}

ostream& rm_var::operator<<(ostream& s) const
{
    s << rmmsgname << ' ';
    s << regflag << ' ';
    return complex_var::operator<<(s);
}

void rm_var::refreshRM()
{
    if (dirty)
    {
        outbuf buf;
        int bufsz = size();
        if (bufsz % sizeof(unsigned int))
        {
            bufsz += sizeof(unsigned int) - 1;
        }
        bufsz /= 4;
        cache_typ* packet = writebuffer(id, bufsz);
        if (packet == NULL)
        {
            return;               // dirty flag is not cleared
        }
        buf.set(packet, bufsz);
        output(buf);
        dirty = !write(buf);
    }
}

cache_typ* rm_var::writebuffer(unsigned int id, size_t len)
{
    return Socket().SetListStart(id, len);
}

bool rm_var::write(outbuf& buf)
{
    return Socket().SetListEnd(id, buf.getcache());
}

void rm_var::outgoing()
{
    static bool reentrant = false;
    if (!reentrant)
    {
        reentrant = true;
        if (check_connect())
        {
            rmlist_typ::iterator q;
            for (q = rmlist().begin(); q != rmlist().end(); ++q)
            {
                (*q)->refreshRM();
            }
            client_socket->KeepAlive();
            client_socket->Flush();
        }
        reentrant = false;
    }
}

void rm_var::incoming()
{
    static bool reentrant = false;
    if (!reentrant)
    {
        reentrant = true;
        if (check_connect())
        {
            if(client_socket->Select(&select_delay) < 0)
            {
                client_socket->Close();
            }
        }
        reentrant = false;
    }
}

void rm_var::close()
{
    if (client_socket)
    {
        client_socket->Close();
    }
}

bool rm_var::check_connect()
{
    static bool reentrant = false;
    if (client_socket == NULL)
    {
        return false;
    }
    if (!reentrant)
    {
        reentrant = true;

        if (!client_socket->Connected())
        {
            if (client_socket->Connect(hostname, hostport))
            {
                size_t i = 0;
                struct timeval stm = {
                    5 + (static_cast<int>(rmlist().size()) / 2), 0
                };

                client_socket->FlowControl(flow_interval);
                client_socket->ClientID(clientname);
                for (i = 0; i < rmlist().size(); ++i)
                {
                    bool wait_for_reply;
                    if (rmlist()[i]->regflag)
                    {
                        wait_for_reply = client_socket->RegisterID(rmlist()[i]->getName());
                    }
                    else
                    {
                        wait_for_reply = client_socket->QueryID(rmlist()[i]->getName());
                    }
                    if (wait_for_reply && ((i % 5) == 0))
                    {
                        if(client_socket->Select(&select_delay, true)<0)
                        {
                            client_socket->Close();
                            reentrant = false;
                            return false;
                        }
                    }
                }
                if (!client_socket->WaitForHandshakeCompletion(&stm))
                {
                    dout << "No reply from RM\n";
                    client_socket->Close();
                    reentrant = false;
                    return false;
                }
                if (connectCB)
                {
                    (*connectCB)();
                }
                // succeed in connecting
                reentrant = false;
                queryid = 1;
                return true;
            }
            reentrant = false;
            return false;
        }
        else
        {
            // already connected
            reentrant = false;
            return true;
        }
    }
    else
    {
        return false;
    }
}

RMBaseSocket& rm_var::Socket() const
{
#ifndef NO_RMINTERF
    rm_interface* interf;
    if ((parent != NULL) && ((interf = RECAST(rm_interface*,parent)) != NULL))
    {
        return interf->Socket();
    }
#endif
    return *client_socket;
}

void rm_var::init(defaults* def)
{
    char accessstr[RMNAME_LEN];
    assert(client_socket == 0);
    assert(def->getstring("client", clientname) != 0);

#if !defined(NO_CLIENT_VER) || !defined(NO_EXITSHOW)
    char hoststr[256] = "@";
    rm_strcpy(&hoststr[1], RMNAME_LEN, clientname);
#endif

#ifndef NO_CLIENT_VER
    char verstr[256];
    assert(v_cl == 0);
    v_cl = new var_client_name;
    v_cl->setName(hoststr);
    minisprintf(verstr, "RM Lib %s (%s) - Compiled %s %s by %s\n", COMPILE_VER, COMPILE_OSNAME, COMPILE_DATE, COMPILE_TIME, COMPILE_BY);
    v_cl->value = verstr;
    dout << verstr;
#endif

#ifndef NO_EXITSHOW
    assert(v_ex == 0);
    hoststr[0] = '~';
    v_ex = new var_exit();
    v_ex->setName(hoststr);
    rmclient_init::install_exit_trigger(v_ex);
#endif

    if (def->getstring("access", accessstr) == NULL)
    {
#ifndef NO_SM
        rm_strcpy(accessstr, RMNAME_LEN, "SM");
#else
        assert(false);
#endif
    }
    if (def->getstring("host", hostname) == NULL)
    {
        if (strcmp(accessstr, "TCP") == 0)
        {
            rm_strcpy(hostname, 256, "Auto");
        }
        else if (strcmp(accessstr, "SM") == 0)
        {
            rm_strcpy(hostname, 256, "RESMGR4");
        }
        else if (strcmp(accessstr, "RM3") == 0)
        {
            rm_strcpy(hostname, 256, "Auto");
        }
    }
#if defined(WIN32) && !defined(NDEBUG)
    if (strcmp(accessstr, "SM") == 0)
    {
        const size_t len = strlen(hostname);
        if (len + 1 < RMNAME_LEN)
        {
            hostname[len] = 'D';
            hostname[len + 1] = '\0';
        }
    }
#endif
    if (def->getint("port", &hostport) == NULL)
    {
        if (strcmp(accessstr, "SM") == 0)
        {
            hostport = 0;
        }
        else
        {
            hostport = 2006;
        }
    }
    def->getint("flow", reinterpret_cast<int*>(&flow_interval));
    def->getint("timeout_sec", reinterpret_cast<int*>(&select_delay.tv_sec));
    def->getint("timeout_usec", reinterpret_cast<int*>(&select_delay.tv_usec));
    client_socket = RMBaseSocket::Construct(accessstr);
    assert(client_socket != 0);
    client_socket->AddCallback(SETLIST, NULL, &_doSetList);
    client_socket->AddCallback(BROADCAST, NULL, &_doSetList);
    client_socket->AddCallback(MESSAGE, NULL, &_doSetList);
    client_socket->AddCallback(GETLIST, NULL, &_doGetList);
    client_socket->AddCallback(CLIENTID, NULL, &_doClientID);
    client_socket->AddCallback(QUERYID, NULL, &_doQueryID);
    client_socket->AddCallback(REGISTERID, NULL, &_doRegisterID);
}

void rm_var::deinit()
{
#ifndef NO_CLIENT_VER
    for (rmlist_typ::iterator p = rmlist().begin(); p != rmlist().end(); ++p)
    {
        if (*p == v_cl)
        {
            rmlist().erase(p);
            break;
        }
    }
    delete v_cl;
#endif
#ifndef NO_EXITSHOW
    for (rmlist_typ::iterator p = rmlist().begin(); p != rmlist().end(); ++p)
    {
        if (*p == v_ex)
        {
            rmlist().erase(p);
            break;
        }
    }
    delete v_ex;
#endif
    delete client_socket;
    v_cl = 0;
    v_ex = 0;
    client_socket = 0;
}

extern "C"
void rm_minisprintf(char* buf, const char* fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    minivsprintf(buf, fmt, ap);
    va_end(ap);
}

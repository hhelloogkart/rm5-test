/******************************************************************/
/* Copyright DSO National Laboratories 2005. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/
#include "rmdserial.h"
#include <time/timecore.h>
#include "cache.h"
#include <util/dbgout.h>
#include <comm/sercore.h>
#include <map>
#include <list>
#include <limits.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

//#define PRINT_DEBUG
#define DEBUG_MAX_THR 0 //set to 1 to disable - to be erased

enum LINK_STATUS
{
    OFF=0, ON, JUST_CONNECTED
};

#ifdef INTEGRITY
#include <INTEGRITY.h>
#endif

#ifdef VXWORKS
#include <sys/socket.h>
#endif

#ifdef NET_OS
#include <threadx/tx_api.h>
#include <threadx/tx_tim.h>
#endif

#define HEADER_SIZE (sizeof(header_typ) / sizeof(unsigned int))
#define HEADER_OFF  ((sizeof(cache_typ) / sizeof(unsigned int)) - HEADER_SIZE)
#define MSG_SEQ 0xFF
#define BUF_NUM 24
#define BUF_SZ 32
#define OUTBUF (BUF_NUM*(HEADER_SIZE+HEADER_OFF+BUF_SZ))
#define RING_SIZE 15
#define SERIAL_MAGIC 0x72

#include "../mhash/src/libdefs.h"
extern "C"
{
#include "../mhash/src/mhash_crc32.h"
}

struct queue_t
{
    unsigned short writer;
    unsigned short reader;
    cache_typ* queue[RING_SIZE+1];
};

struct serial_header    // MAGIC | ID | Length | Sequence or CMD
{
    unsigned char magic;
    unsigned char id;
    unsigned char len;
    unsigned char seq;
};

struct temp_typ
{
    serial_header header;
    unsigned char temp_buf[READ_BUF+HEADER_OFF+1];
};

struct rmdserial_socket
{
    serial_core sock;
    int conn_state;                                     // 0 - initiate, 1 to 20 - wait for init, 21 - connected									// last send time for this socket
    timeval recvtime;                                   // last received time for this socket for clearing output buffer
    timeval sendtime;                                   // last send time for this socket

    unsigned int bytes_remaining;
    unsigned int errflag;                               //errflag for this socket

    unsigned int leftover;                              //number of left over bytes for this socket for receiving
    unsigned char lo_buf[READ_BUF+HEADER_OFF+1];        //queue for leftover data when receiving

    unsigned int leftover_output;                       //number of left over bytes for this socket for sending
    unsigned char lo_buf_output[READ_BUF+HEADER_OFF+1]; //queue for leftover data when sending

    unsigned int baudrate;
    unsigned int max_throughput;
};

struct rmdserial_socket_private
{
    rmdserial_socket* socket;
    int num_of_connections;

    timeval last_resend_recv_time;          //restrict receiver side from implementing too much resends
    timeval last_reset_recv_time;           //restrict receiver side from implementing too much resets
    timeval last_clear_buffer_time;

    unsigned int getid;
    cache_typ* query_buf;
    unsigned int intv;
    bool readflag;
    bool check_max_thrput;                  //to erase
    short close_connection;                 //-1 do nothing, >0 represents connection id, -2 close all
    unsigned int valid;                     //number of bytes still valid in the receive buffer(in_buf)

    unsigned char innum;                    // from 0 to 127
    unsigned char outnum;                   // from 0 to 127

    unsigned char last_resend_innum;
    unsigned int raw_inbuf[READ_BUF+HEADER_OFF+1];
    unsigned int raw_outbuf[OUTBUF];
    unsigned char* in_buf;

    queue_t outqueue;                       //enqueue only in SetListEnd
    queue_t inqueue;
    queue_t cbqueue;

    LINK_STATUS link_state;
    bool reestablish_link_flag;
    timeval clear_outbuf_interval;
    timeval reset_interval;
    timeval resend_interval;
};

inline unsigned int Dqueue_next(unsigned int p)
{
    return (p + 1) % RING_SIZE;
}

inline bool Dqueue_full(queue_t* q)
{
    return Dqueue_next(q->writer) == q->reader;
}

inline bool Dqueue_empty(queue_t* q)
{
    return q->reader == q->writer;
}

inline cache_typ*& Dqueue_top(queue_t* q)
{
    return q->queue[q->reader];
}

inline void Dqueue_bottom(queue_t* q, cache_typ* buf)
{
    q->queue[q->writer] = buf;
}

inline int sendPacket( rmdserial_socket_private* prvt, int sock_id, const char* buf, unsigned int sz ) //Sending of data via socket - flow control is implemented here
{
    unsigned int result = 0;
    if(sock_id<0)
    {
        return -1;
    }
    if(prvt->socket[sock_id].sendtime.tv_sec==0 && prvt->socket[sock_id].sendtime.tv_usec==0)
    {
        prvt->socket[sock_id].bytes_remaining = prvt->socket[sock_id].max_throughput/2;
    }
    if( (prvt->socket[sock_id].max_throughput <= prvt->socket[sock_id].baudrate) && !prvt->check_max_thrput )
    {
        if( prvt->socket[sock_id].sock.send(buf, sz) == (int)sz ) //send per normal
        {
            result = sz;
            now(&prvt->socket[sock_id].sendtime);
        } //if
    } //if
    else
    {
        //if not within .5 seconds reset timer and reset num of bytes allowed
        timeval current_time;
        now(&current_time);

        if( (current_time.tv_sec>prvt->socket[sock_id].sendtime.tv_sec) ||
            (current_time.tv_sec==prvt->socket[sock_id].sendtime.tv_sec &&
             (current_time.tv_usec-prvt->socket[sock_id].sendtime.tv_usec) >= 500000) )
        {
            prvt->socket[sock_id].bytes_remaining = prvt->socket[sock_id].max_throughput/2;
        }
        // check if there is buffered packets. If there is, attempt to send allowed size of buffered packets
        if( (prvt->socket[sock_id].leftover_output > 0) && (prvt->socket[sock_id].bytes_remaining > 0) )
        {
            if ( prvt->socket[sock_id].leftover_output <= prvt->socket[sock_id].bytes_remaining )
            {
                if( prvt->socket[sock_id].sock.send( (char*)prvt->socket[sock_id].lo_buf_output, prvt->socket[sock_id].leftover_output ) == (int)prvt->socket[sock_id].leftover_output )
                {
                    result = prvt->socket[sock_id].leftover_output;
                    now(&prvt->socket[sock_id].sendtime);
                    prvt->socket[sock_id].bytes_remaining -= prvt->socket[sock_id].leftover_output;
                    prvt->socket[sock_id].leftover_output = 0;
                } //if
            } //if
            else
            {
                //leftover > bytes remaining
                if( prvt->socket[sock_id].sock.send( (char*)prvt->socket[sock_id].lo_buf_output, prvt->socket[sock_id].bytes_remaining ) == (int)prvt->socket[sock_id].bytes_remaining )
                {
                    int diff = prvt->socket[sock_id].leftover_output - prvt->socket[sock_id].bytes_remaining;
                    memmove( &(prvt->socket[sock_id].lo_buf_output[0]), &(prvt->socket[sock_id].lo_buf_output[prvt->socket[sock_id].bytes_remaining]), diff );
                    result = prvt->socket[sock_id].bytes_remaining;
                    now(&prvt->socket[sock_id].sendtime);
                    prvt->socket[sock_id].leftover_output = diff;
                    prvt->socket[sock_id].bytes_remaining = 0;
                } //if
            } //else
        } //if

        //if num of bytes allowed is violated, buffer data and send allowed size
        //else send data
        if(prvt->socket[sock_id].bytes_remaining > 0)
        {
            if( sz > (prvt->socket[sock_id].bytes_remaining) )
            {
                if(prvt->socket[sock_id].bytes_remaining > 0 && (prvt->socket[sock_id].sock.send(buf, prvt->socket[sock_id].bytes_remaining) == (int)prvt->socket[sock_id].bytes_remaining) )
                {
                    result += prvt->socket[sock_id].bytes_remaining;
                    now(&prvt->socket[sock_id].sendtime);
                }
                //still need to buffer remaining packets
                memcpy( &(prvt->socket[sock_id].lo_buf_output[prvt->socket[sock_id].leftover_output]), (buf + prvt->socket[sock_id].bytes_remaining), (sz - prvt->socket[sock_id].bytes_remaining) );
                prvt->socket[sock_id].leftover_output += (sz - prvt->socket[sock_id].bytes_remaining);
                prvt->socket[sock_id].bytes_remaining = 0;
            } //sz > bytes_remaining
            else //just send data packet without buffering
            {
                if( prvt->socket[sock_id].sock.send(buf, sz) == (int)sz )
                {
                    result += sz;                               //result = result + size of data to be sent in bytes
                    now(&prvt->socket[sock_id].sendtime);
                }
                prvt->socket[sock_id].bytes_remaining -= sz;
            } //send data without buffering
        } //bytes remaining > 0
        else
        {
            if(prvt->socket[sock_id].leftover_output+sz >= (READ_BUF+HEADER_OFF+1))
            {
#ifdef PRINT_DEBUG
                dout<<"Size of leftover output buffer exceeded\n";  // Size of leftover buffer exceeded
#endif
                return 0;
            } //if socket leftover_output + sz >= READ_BUF + HEADER_OFF + 1
            else
            {
                memcpy( &(prvt->socket[sock_id].lo_buf_output[prvt->socket[sock_id].leftover_output]), buf, sz );
                prvt->socket[sock_id].leftover_output += sz;
            } //else
        } //else bytes remaining <= 0
    } //else there is a need to check flow control
    return result; //number of bytes sent
} //end of sendPacket function

void RMDSerialSocket::EnterResetState(bool burstout) // enter the reset state
{
    //resets the variables, this command is issued when all connections are down
    for(int i = 0; i < prvt->num_of_connections; i++)
    {
        prvt->socket[i].sendtime.tv_sec=0;
        prvt->socket[i].sendtime.tv_usec=0;
        now(&prvt->socket[i].recvtime);

        prvt->socket[i].sendtime.tv_sec=0;
        prvt->socket[i].sendtime.tv_usec=0;
        prvt->socket[i].errflag = 0;
        prvt->socket[i].leftover = 0;
        prvt->socket[i].leftover_output = 0;
        prvt->socket[i].bytes_remaining = prvt->socket[i].max_throughput/2;
    } //for
    prvt->innum = 0;  // from 0 to 127  - expecting incoming packet number
    prvt->last_resend_innum = 0x80; // - expecting last resend in coming packet

    //////////////////////////////////////////////////////////////////////////////////
    //clear inqueue, cbqueue
    //////////////////////////////////////////////////////////////////////////////////
    for (int tag = prvt->inqueue.reader; tag != prvt->inqueue.writer; tag = Dqueue_next(tag))
    {
        GetListEnd(0, prvt->inqueue.queue[tag]);
    }
    prvt->inqueue.writer = 0;
    prvt->inqueue.reader = 0;

    for (int tag = prvt->cbqueue.reader; tag != prvt->cbqueue.writer; tag = Dqueue_next(tag))
    {
        GetListEnd(0, prvt->cbqueue.queue[tag]);
    }
    prvt->cbqueue.writer = 0;
    prvt->cbqueue.reader = 0;
    //////////////////////////////////////////////////////////////////////////////////
    if (!burstout)
    {
        unsigned char seq = 0;
        for (int tag = prvt->outqueue.reader; tag != prvt->outqueue.writer; tag = Dqueue_next(tag), ++seq)
        {
            unsigned int* buf = prvt->outqueue.queue[tag]->buf() - 2;
            serial_header* head = reinterpret_cast<serial_header*>(buf+1);
            head->seq = seq;
            mhash_clear_crc32(reinterpret_cast<mutils_word32*>(buf));
            mhash_crc32(reinterpret_cast<mutils_word32*>(buf), buf+1, (head->len+1)*sizeof(unsigned int));  // the crc is packed at the beginning
            buf[0] = BE(buf[0]);

            timeval current_time;
            now(&current_time);
            if( ((current_time.tv_sec - prvt->last_clear_buffer_time.tv_sec)>prvt->clear_outbuf_interval.tv_sec)
                ||( ((current_time.tv_sec - prvt->last_clear_buffer_time.tv_sec)==prvt->clear_outbuf_interval.tv_sec)
                    &&((current_time.tv_usec - prvt->last_clear_buffer_time.tv_usec)>prvt->clear_outbuf_interval.tv_usec)))
            {
                now(&prvt->last_clear_buffer_time);
            }
            else
            {
                break;
            }

            for(int i = 0; i<prvt->num_of_connections; i++)
            {
                sendPacket( prvt, i, (char*)buf, (head->len + 2) * sizeof(unsigned int) );
            }
#ifdef PRINT_DEBUG
            dout << "[Resend output queue] Sequence num: " << (int)head->seq << '\n';
#endif
        } //for
        prvt->outnum = seq;
    }
    if (prvt->outnum == 0)
    {
        SendMessageT(0xFF, ALIVE); //SEND ALIVE MESSAGES
    }
    now(&prvt->last_reset_recv_time);
}

RMDSerialSocket::RMDSerialSocket() : prvt(new rmdserial_socket_private)
{
    prvt->socket = NULL;
    prvt->num_of_connections = 0;

#if DEBUG_MAX_THR
    prvt->check_max_thrput = false;
#else
    prvt->check_max_thrput = true;
#endif

    prvt->readflag = false;
    prvt->close_connection = -1;
    prvt->outqueue.writer = 0;
    prvt->outqueue.reader = 0;
    prvt->inqueue.writer = 0;
    prvt->inqueue.reader = 0;
    prvt->cbqueue.writer = 0;
    prvt->cbqueue.reader = 0;

    prvt->close_connection = -1; //indicates no need to take any action if Close() is called
    prvt->getid = UINT_MAX;
    prvt->valid = 0;
    prvt->innum = 0;
    prvt->outnum = 0;
    prvt->last_resend_innum = 0x80; //last_resend_innum is 128 - indicates MessageEnd
    prvt->query_buf = NULL;

    prvt->last_resend_recv_time.tv_sec = prvt->last_resend_recv_time.tv_usec = 0;
    prvt->last_reset_recv_time.tv_sec = prvt->last_reset_recv_time.tv_usec = 0;
    prvt->last_clear_buffer_time.tv_sec = prvt->last_clear_buffer_time.tv_usec = 0;

    prvt->intv = 0;

    prvt->in_buf = reinterpret_cast<unsigned char*>(prvt->raw_inbuf + HEADER_OFF);
    unsigned int* ptr = &prvt->raw_outbuf[0];
    for (int cnt = 0; cnt < BUF_NUM; ++cnt, ptr += HEADER_SIZE+HEADER_OFF+BUF_SZ)
    {
        reinterpret_cast<cache_typ*>(ptr)->construct(BUF_SZ);
        *(reinterpret_cast<cache_typ*>(ptr)->buf()-3) = MAGICNUM+1;
    } //for

    prvt->link_state=OFF;
    prvt->reestablish_link_flag=false;

    prvt->clear_outbuf_interval.tv_sec=0;
    prvt->clear_outbuf_interval.tv_usec=500000;

    prvt->reset_interval.tv_sec=0;
    prvt->reset_interval.tv_usec=500000;

    prvt->resend_interval.tv_sec=0;
    prvt->resend_interval.tv_usec=500000;
}

RMDSerialSocket::~RMDSerialSocket()
{
    if (prvt->query_buf != NULL)
    {
        GetListEnd(0, prvt->query_buf);
    }
    delete[]prvt->socket;
    delete prvt;
}

bool RMDSerialSocket::Connect(const char* host, int port, const struct timeval* stm) // Connect Function
{
    struct connection_struct
    {
        char hostname[200];
        unsigned int baudrate;
        unsigned int max_thrput;
    }* conn = NULL;
    struct timeval tv = {
        2, 0
    };
    int num_of_connections = 0, num_of_commas_per_set = 0, index = 0, counter = 0;
    const char* ptr = NULL;
    char tmp_host[200] = "\0";

    switch(port)
    {
    case 1:
    {
        ///////////////////////////////////////////////////////
        //find out number of connections first
        ///////////////////////////////////////////////////////
        num_of_commas_per_set = 2;

        rm_strcpy(&tmp_host[0], 200, host);
        while( (ptr = strchr(&tmp_host[index],','))!=NULL)
        {
            if(num_of_commas_per_set == 0)
            {
                num_of_commas_per_set = 2;
                num_of_connections++;
            }
            else
            {
                num_of_commas_per_set--;
            }
            index += ptr+1-&tmp_host[index];
        }        //while ptr is not null
        if(num_of_commas_per_set==0)
        {
            num_of_connections++;
        }
        ///////////////////////////////////////////////////////

        conn = new connection_struct[num_of_connections];
        ptr = NULL;
        index = 0;

        while( (ptr = strchr(host,','))!=NULL  )
        {
            //get host name
            rm_strncpy(conn[counter].hostname, 200, host,(ptr-host));
            conn[counter].hostname[ptr-host] = '\0';
            ptr++;
            host = ptr;

            //get baudrate
            if( (ptr = strchr(host,','))!=NULL)
            {
                char temp[200] = "\0";
                rm_strncpy(&temp[0], 200, host, (ptr-host));
                conn[counter].baudrate = atoi(&temp[0]);
                ptr++;
                host = ptr;
            }        //if
            else
            {
                dout << "Insufficient parameter exception [baudrate / max_throughput not found] \n";
                delete[] conn;
                return false;
            }

            //get max_throughput
            if( (ptr = strchr(host,','))!=NULL)
            {
                char temp[200] = "\0";
                rm_strncpy(&temp[0], 200, host, (ptr-host));
                conn[counter].max_thrput = atoi(&temp[0]);
                ptr++;
                host = ptr;
            }
            else
            {
                break;
            }
            counter++;
        }        //end-while

        if(host[0] != '\0')
        {
            char temp[200] = "\0";
            rm_strcpy(&temp[0], 200, host);
            conn[counter].max_thrput = atoi(&temp[0]);
        }
        else
        {
            dout << "Insufficient parameter exception [baudrate / max_throughput not found] \n";
            delete[] conn;
            return false;
        }
        break;
    }
    case 2:
    {
        /////////////////////////////////////////
        //find out number of connections first
        /////////////////////////////////////////
        num_of_commas_per_set = 1;
        rm_strcpy(&tmp_host[0], 200, host);
        while( (ptr = strchr(&tmp_host[index],','))!=NULL )
        {
            if(num_of_commas_per_set == 0)
            {
                num_of_commas_per_set = 1;
                num_of_connections++;
            }
            else
            {
                num_of_commas_per_set--;
            }
            index += ptr+1-&tmp_host[index];
        }        //while ptr is not null
        if(num_of_commas_per_set==0)
        {
            num_of_connections++;
        }
        /////////////////////////////////////////

        conn = new connection_struct[num_of_connections];
        ptr = NULL;

        while( (ptr = strchr(host,','))!=NULL  )
        {
            //get host name
            rm_strncpy(conn[counter].hostname, 200, host,(ptr-host));
            conn[counter].hostname[ptr-host] = '\0';
            ptr++;
            host = ptr;

            //get baudrate
            if( (ptr = strchr(host,','))!=NULL)
            {
                char temp[200] = "\0";
                rm_strncpy(&temp[0], 200, host, (ptr-host));
                conn[counter].baudrate = atoi(&temp[0]);
                ptr++;
                host = ptr;
            }        //if
            else
            {
                break;
            }

            conn[counter].max_thrput = conn[counter].baudrate;
            counter++;
        }        //end-while
        if(host[0] != '\0')
        {
            char temp[200] = "\0";
            rm_strcpy( &temp[0], 200, host);
            conn[counter].baudrate = atoi(&temp[0]);
            conn[counter].max_thrput = conn[counter].baudrate;
        }
        else
        {
            dout << "Insufficient parameter exception [baudrate / max_throughput not found] \n";
            delete[] conn;
            return false;
        }
        break;
    }
    default:
    {
        num_of_connections = 1;
        conn = new connection_struct[1];
        rm_strcpy( conn[0].hostname, 200, host);
        conn[0].baudrate = port;
        conn[0].max_thrput = port;
        break;
    }
    } //switch

    if(prvt->num_of_connections != num_of_connections)
    {
        prvt->num_of_connections = num_of_connections;
        if(prvt->socket!=NULL)
        {
            delete prvt->socket;
        }
        prvt->socket = new rmdserial_socket[num_of_connections];
        for(int i =0; i<prvt->num_of_connections; i++)
        {
            prvt->socket[i].conn_state = 0;
            prvt->socket[i].leftover_output = 0;
            prvt->socket[i].leftover = 0;
            prvt->socket[i].errflag = 0;
            prvt->socket[i].sendtime.tv_sec=0;
            prvt->socket[i].sendtime.tv_usec=0;
            now(&prvt->socket[i].recvtime);
            prvt->socket[i].bytes_remaining = 0;
        }
    }

    for(index = 0; index< prvt->num_of_connections; index++)
    {
        if(!prvt->socket[index].sock.connected())
        {
            prvt->socket[index].baudrate = conn[index].baudrate; //set baudrate
            prvt->socket[index].max_throughput = conn[index].max_thrput; //set max_throughput
            prvt->socket[index].sock.sethost(conn[index].hostname, conn[index].baudrate); //set host
            prvt->socket[index].sock.connect(0); // serial connect is instantaneous
        } //if sock is not connected
    } //for index < num of connections

    if(Connected())
    {
        delete[] conn;
        return true;
    }

    for(index=0; index<prvt->num_of_connections; index++)
    {
        if( prvt->socket[index].conn_state==21 )
        {
            break;
        }
    } //for

    if(index==prvt->num_of_connections)
    {
        SendMessageT(0xff, RESET); // send reset message
    } // send reset message

    if ( Select(stm == 0 ? &tv : stm, true ) == 0) // Select and hold
    {
        // still cannot get a reset
        for(index = 0; index<prvt->num_of_connections; index++)
        {
            ++(prvt->socket[index].conn_state);
            if (prvt->socket[index].conn_state == 21)
            {
                prvt->socket[index].conn_state = 0;
            }
        }
        delete[] conn;
        return false;
    }
    delete[] conn;
    return Connected();
} // Connect

bool RMDSerialSocket::Connected() // if any connection is state21
{
    if(prvt->socket == NULL)
    {
        return false;
    }
    for( int i = 0; i < prvt->num_of_connections; i++ )
    {
        if(prvt->socket[i].conn_state == 21)
        {
            return true;
        }
    } // for
    return false;
}

bool RMDSerialSocket::Close() // Close
{
    if(prvt->close_connection >=0)
    {
        prvt->socket[prvt->close_connection].conn_state = 0;
        now(&prvt->socket[prvt->close_connection].recvtime);
        prvt->socket[prvt->close_connection].sendtime.tv_sec = 0;
        prvt->socket[prvt->close_connection].sendtime.tv_usec = 0;
        prvt->socket[prvt->close_connection].errflag = 0;
        prvt->socket[prvt->close_connection].leftover = 0;
        prvt->socket[prvt->close_connection].leftover_output = 0;
    }
    else if(prvt->close_connection == -2)
    {
        for(int i = 0; i < prvt->num_of_connections; i++)
        {
            //reset variables for connection i
            prvt->socket[i].conn_state = 0;
            now(&prvt->socket[i].recvtime);
            prvt->socket[i].sendtime.tv_sec = 0;
            prvt->socket[i].sendtime.tv_usec = 0;
            prvt->socket[i].errflag = 0;
            prvt->socket[i].leftover = 0;
            prvt->socket[i].leftover_output = 0;
        } //for
        prvt->getid = UINT_MAX;
        prvt->valid = 0;
        prvt->innum = 0;
        prvt->outnum = 0;
        prvt->last_resend_innum = 0x80; //last_resend_innum is 128 - indicates MessageEnd
        if( prvt->query_buf != NULL)
        {
            GetListEnd(0, prvt->query_buf);
            prvt->query_buf = NULL;
        }

        //erase inqueue
        for (int tag = prvt->inqueue.reader; tag != prvt->inqueue.writer; tag = Dqueue_next(tag))
        {
            GetListEnd( 0, prvt->inqueue.queue[tag] );
        }
        prvt->inqueue.writer = 0;
        prvt->inqueue.reader = 0;

        //erase cbqueue
        for (int tag = prvt->cbqueue.reader; tag != prvt->cbqueue.writer; tag = Dqueue_next(tag))
        {
            GetListEnd( 0, prvt->cbqueue.queue[tag] );
        }
        prvt->cbqueue.writer = 0;
        prvt->cbqueue.reader = 0;

        unsigned char seq = 0;
        for (int tag = prvt->outqueue.reader; tag != prvt->outqueue.writer; tag = Dqueue_next(tag), ++seq)
        {
            unsigned int* buf = prvt->outqueue.queue[tag]->buf() - 2;
            serial_header* head = reinterpret_cast<serial_header*>(buf+1);
            head->seq = seq;
            mhash_clear_crc32(reinterpret_cast<mutils_word32*>(buf));
            mhash_crc32(reinterpret_cast<mutils_word32*>(buf), buf+1, (head->len+1)*sizeof(unsigned int));  // the crc is packed at the beginning
            buf[0] = BE(buf[0]);
        }
        prvt->outnum = seq;
    } //else if
    prvt->close_connection = -1; //unset close connection
    return true;
}

int RMDSerialSocket::Select(const struct timeval* stm, bool defercb) // Select
{
    int ret=0, counter=0;
    bool one_conn_without_error = false;
    unsigned int cmd;
    KeepAlive();
    ///////////////////////////////////////////////////////////////////////////////
    //defercb flag is false implies that library will call the call back function immediately
    //for each pending packet in the receive buffer
    ////////////////////////////////////////////////////////////////////////////////
    if (!defercb)
    {
        while (!Dqueue_empty(&prvt->cbqueue))
        {
            cache_typ* pcache = Dqueue_top(&prvt->cbqueue);
            cmd = pcache->get_cmd();
            if (cmd < ENDCMD)
            {
                (*(callback[cmd]))(callback_arg[cmd], pcache->get_id(), pcache);
            }
            GetListEnd(0, pcache);      //returns allocated buffer to RM after handling command packet
            prvt->cbqueue.reader = Dqueue_next(prvt->cbqueue.reader);
        } //while
    } //if
      //check whether there is data coming in from either connections
      //if there is, then receive packet else if both no data or error close connections
    for(counter =0; counter<prvt->num_of_connections; counter++)
    {
        int tmp_ret=0;
        if( (tmp_ret=prvt->socket[counter].sock.select(stm)) > 0 )
        {
            ret = 1;
            ReceivePacket(counter, defercb);
            one_conn_without_error = true;
        }
        else if(tmp_ret < 0)
        {
            prvt->close_connection = counter; //Close this error connection
            Close();
        }
        else
        {
            one_conn_without_error = true;
        }
    } //for
    if( !one_conn_without_error ) //-1 indicates there is error in ALL connections => close connections
    {
        return -1;
    }
    else if( !CheckLink() ) //if all links not ok (exceeds 2xflow control or does not hav state of 21)  then close all connections
    {
        for(counter=0; counter < prvt->num_of_connections; counter++)
        {
            if( prvt->socket[counter].recvtime.tv_sec!=0 || prvt->socket[counter].recvtime.tv_usec!=0 )
            {
                prvt->close_connection = -2; // close all my connections
                Close();
                break;
            }
        } //for counter < number of connections
        return false;
    } //else if check link fails
    KeepAlive();
    return ret;
}

const cache_typ* RMDSerialSocket::GetListStart(unsigned int id)
{
    int cnt = 0, ret;
    SendMessageT(id & 0xFF, GETLIST);
    prvt->getid = id;
    do
    {
        if (cnt != 0)
        {
            Sleep(WAIT_TIME);
        }
        struct timeval tm = {
            0, WAIT_TIME*1000
        };
        ret =  Select(&tm, true);
        ++cnt;
    }
    while ((prvt->query_buf == NULL) && (cnt < GETLIST_RETRY) && (ret >= 0));
    cache_typ* ptr = prvt->query_buf;
    prvt->getid = UINT_MAX;
    prvt->query_buf = NULL;
    return ptr;
}

void RMDSerialSocket::GetListEnd(unsigned int, const cache_typ* pbuf) // GetListEnd
{
    *(const_cast<cache_typ*>(pbuf)->buf()-3) = MAGICNUM+1;
    const_cast<cache_typ*>(pbuf)->h_unreserve();
}

cache_typ* RMDSerialSocket::MessageStart(unsigned int id, size_t len)
{
    return SetListStart(id, len);
}
cache_typ* RMDSerialSocket::SetListStart(unsigned int, size_t len)  // SetListStart
{
    if (len == 0)
    {
#ifdef PRINT_DEBUG
        dout << "RM Message does not allow zero length packets\n";
#endif
        return 0;
    } //if
    if (len <= BUF_SZ)  // len <=32
    {
        unsigned int* ptr = &prvt->raw_outbuf[0];
        for (int cnt  = 0; cnt < BUF_NUM; ++cnt, ptr += HEADER_SIZE+HEADER_OFF+BUF_SZ)
        {
            cache_typ* pcache = reinterpret_cast<cache_typ*>(ptr);
            if (*(pcache->buf()-3) == MAGICNUM+1)
            {
                *(pcache->buf()-3) = MAGICNUM; // means it is free
                pcache->set_cmd(SETLIST);
                pcache->set_len(static_cast<unsigned int>(len));
                pcache->h_reserve();
                return pcache;
            } //if
        } //for loop
    } //if len <= BUF_SZ
    else if (len > 255)
    {
#ifdef PRINT_DEBUG
        dout<< "RM Message size exceeded\n";
#endif
        return 0;  // sorry size exceeded
    } //if len > 255
    return cache_typ::h_allocate(len);
}

bool RMDSerialSocket::SetListEnd(unsigned int id, cache_typ* pbuf) // SetListEnd
{
#ifdef PRINT_DEBUG
    for(int i=prvt->outqueue.reader; i!=prvt->outqueue.writer; i=Dqueue_next(prvt->outqueue.reader))
    {
        dout<<"Queue: "<<pbuf->packet()<<"\n";
    }
#endif
    if ( prvt->reestablish_link_flag )
    {
        GetListEnd(id, pbuf); //remove the recently written-to buffer from RM since outqueue is already full
        return false;
    }
    if (!Connected())
    {
        return false;                   //if both connections are down then return false
    }
    if (Dqueue_full(&prvt->outqueue))   //if outqueue full
    {
#ifdef PRINT_DEBUG
        dout<<"Output Buffer full\n";
#endif
        GetListEnd(id, pbuf); //remove the recently written-to buffer from RM since outqueue is already full
        return false;
    } //outqueue full

    bool sock_flag = false;
    bool ret = true;
    ////////////////////////////////////////////////////////////////////
    //create the header and data packet
    ////////////////////////////////////////////////////////////////////
    unsigned int* buf = pbuf->buf() - 2;                            // this is where the compact packet starts
    serial_header* head = reinterpret_cast<serial_header*>(buf+1);  // header is 4 bytes away
    head->len = (unsigned char)pbuf->get_len();                     // need to get useful data out before overriding
    head->magic = SERIAL_MAGIC;
    head->id = (unsigned char)id;
    if (id > 255 )
    {
#ifdef PRINT_DEBUG
        dout << "RM Message ID exceeded\n";
#endif
    }
    head->seq = prvt->outnum;
    mhash_clear_crc32(reinterpret_cast<mutils_word32*>(buf));
    mhash_crc32(reinterpret_cast<mutils_word32*>(buf), buf+1, (head->len+1)*sizeof(unsigned int));  // the crc is packed at the beginning
    buf[0] = BE(buf[0]);
    const int sz = (head->len + 2) * sizeof(unsigned int);

    for(int i = 0; i < prvt->num_of_connections; i++)
    {
        if ( sendPacket( prvt, i, (const char*)buf, sz ) >= sz )
        {
            sock_flag = true;
        }
    } //for
    ++(prvt->outnum) &= 0x7f;
    ret = sock_flag;

    Dqueue_bottom(&prvt->outqueue, pbuf);   // queue bottom
    prvt->outqueue.writer = Dqueue_next(prvt->outqueue.writer);
    return ret;
}

bool RMDSerialSocket::BroadcastEnd(unsigned int id, cache_typ* pbuf) // BroadcastEnd
{
    return SetListEnd(id, pbuf);
}

//to specify end of message where sequence number is given a value of 128
bool RMDSerialSocket::MessageEnd(unsigned int id, cache_typ* pbuf, void*)  // MessageEnd
{
    if (!Connected())
    {
        return false;
    }
    unsigned int* buf = pbuf->buf() - 2;                            // this is where the compact packet starts
    serial_header* head = reinterpret_cast<serial_header*>(buf+1);  // header is 4 bytes away
    head->len = (unsigned char)pbuf->get_len();                     // need to get useful data out before overriding
    head->magic = SERIAL_MAGIC;
    head->id = (unsigned char)id;

    if (id > 255)
    {
#ifdef PRINT_DEBUG
        dout << "RM Message ID exceeded\n";
#endif
    } //if
    head->seq = 0x80;
    mhash_clear_crc32(reinterpret_cast<mutils_word32*>(buf));
    mhash_crc32(reinterpret_cast<mutils_word32*>(buf), buf+1, (head->len+1)*sizeof(unsigned int));  // the crc is packed at the beginning
    buf[0] = BE(buf[0]);
    const int sz = (head->len + 2) * sizeof(unsigned int);

    bool send_flag = false;                                                             //send_flag = false, send_flag = true
    for(int i = 0; i < prvt->num_of_connections; i++)                                   //send packet out for every connection
    {
        if(sendPacket( prvt, i, (const char*)buf, sz ) >= sz  )
        {
            send_flag = true;                                                           //sendPacket
        }
    } //for
    GetListEnd(id,pbuf);
    return send_flag;
}

bool RMDSerialSocket::RegisterIDX(unsigned int)
{
    return true;
}

bool RMDSerialSocket::UnregisterIDX(unsigned int)
{
    return true;
}

bool RMDSerialSocket::RegisterID(const char*)
{
    return false;
}

bool RMDSerialSocket::QueryID(const char*)
{
    return false;
}

bool RMDSerialSocket::QueryID(const char*[], int)
{
    return false;
}

bool RMDSerialSocket::ClientID(const char*)
{
    return false;
}

bool RMDSerialSocket::FlowControl(unsigned int interval)
{
    prvt->intv = interval;
    return SendMessageT(interval, FLOWCONTROL);
}

bool RMDSerialSocket::Broadcast(unsigned int id, size_t len, const unsigned int* buf)
{
    cache_typ* pbuf = SetListStart(id, len);
    const unsigned int* bufend = buf + len;
    unsigned int* dest = pbuf->buf();
    while (buf != bufend)
    {
        *dest = *buf;
        ++buf;
        ++dest;
    }
    return BroadcastEnd(id, pbuf);
} //BroadcastEnd

bool RMDSerialSocket::SetList(unsigned int id, size_t len, const unsigned int* buf)
{
    cache_typ* pbuf = SetListStart(id, len);
    const unsigned int* bufend = buf + len;
    unsigned int* dest = pbuf->buf();
    while (buf != bufend)
    {
        *dest = *buf;
        ++buf;
        ++dest;
    }
    return SetListEnd(id, pbuf);
} //SetList

bool RMDSerialSocket::Message(unsigned int id, size_t len, const unsigned int* buf)
{
    cache_typ* pbuf = SetListStart(id, len);
    const unsigned int* bufend = buf + len;
    unsigned int* dest = pbuf->buf();
    while (buf != bufend)
    {
        *dest = *buf;
        ++buf;
        ++dest;
    }
    return MessageEnd(id, pbuf);
}

size_t RMDSerialSocket::GetList(unsigned int id, size_t len, unsigned int* buf)
{
    const cache_typ* ptr = GetListStart(id);
    if (ptr)
    {
        size_t sz = ptr->output(len, buf);
        GetListEnd(id, ptr);
        return sz;
    } //if
    return 0;
} //GetList

bool RMDSerialSocket::SendMessageT(unsigned int id, int cmd)
{
    bool send_flag = false;
    serial_header sh = {
        SERIAL_MAGIC, static_cast<unsigned char>(id), '\0', static_cast<unsigned char>(cmd)
    };                                                                                                          //create the data packet for message
    switch(cmd)
    {
    case ALIVE:
    case ACKNOWLEDGE:
    case GETLIST:
    case RESEND:
    case RESET:
    case FLOWCONTROL: //if ADMIN packets just send out
        for(int i = 0; i < prvt->num_of_connections; i++ )
        {
            if( prvt->socket[i].sock.send( reinterpret_cast<char*>(&sh), sizeof(serial_header) ) == sizeof(serial_header) )
            {
                now(&prvt->socket[i].sendtime);
            }
            send_flag = true;
        } //for loop to iterate through all the ports
        break;
    default:
        for(int i = 0; i < prvt->num_of_connections; i++ )
        {
            if (prvt->socket[i].sock.connected() && (sendPacket( prvt, i, reinterpret_cast<char*>(&sh), sizeof(serial_header)) >= (int)sizeof(serial_header)))
            {
                send_flag = true;
            } //socket is connected and sending is ok
        } //for loop to iterate through all the ports
        break;
    } //switch
    return send_flag;
}

void RMDSerialSocket::ReceivePacket(int sock_id, bool defercb)
{
    unsigned int begin  = 0;            // Location of unprocessed
    unsigned int more   = 1;            // Indicator of pending packets
    unsigned char* p_crc;               // Temp storage of CRC-32
    unsigned int byte_crc;              // Length of valid CRC-32
    int result;                         // Read result
    bool burstout = false;              // Burstout is set to true when !Connected

    // state = 0 read in up to (READ_BUF * 4) + 1 bytes (more, begin, valid flag)
    //       = 1 find next packet (begin, length, id, cmd)
    //       = 2 move incomplete packet to start location (begin)
    if (prvt->readflag)
    {
        return;
    }
    else
    {
        prvt->readflag = true;
    }

    if(prvt->socket[sock_id].leftover>0)
    {
        memmove(prvt->in_buf, &(prvt->socket[sock_id].lo_buf[0]), prvt->socket[sock_id].leftover);
        prvt->valid = prvt->socket[sock_id].leftover;
    }

    while (more && (prvt->valid < (READ_BUF * sizeof(unsigned int) + 1))) //valid data < (8192*4)+1 bytes = 32769 bytes
    {
        result = prvt->socket[sock_id].sock.recv(reinterpret_cast<char*>(prvt->in_buf) + prvt->valid, (READ_BUF * sizeof(unsigned int) + 1) - prvt->valid);     // state 0

        if (result > 0) //amount of received bytes is > 0
        {
            prvt->valid += result;
            prvt->socket[sock_id].conn_state = 21;

            if ( prvt->valid != (READ_BUF * sizeof(unsigned int) + 1) )
            {
                more = 0;
            }
            if ( !Connected() ) // socket is not connected
            {
                burstout = true;
                for (int tag = prvt->outqueue.reader; tag != prvt->outqueue.writer; tag = Dqueue_next(tag)) //-original code
                {
                    unsigned int* buf = prvt->outqueue.queue[tag]->buf()- 2;        // this is where the compact packet starts
                    serial_header* head = reinterpret_cast<serial_header*>(buf+1);  // header is 4 bytes away
                    timeval current_time;
                    now(&current_time);
                    if( ((current_time.tv_sec - prvt->last_clear_buffer_time.tv_sec)>prvt->clear_outbuf_interval.tv_sec)
                        ||( ((current_time.tv_sec - prvt->last_clear_buffer_time.tv_sec)==prvt->clear_outbuf_interval.tv_sec)
                            &&((current_time.tv_usec - prvt->last_clear_buffer_time.tv_usec)>prvt->clear_outbuf_interval.tv_usec)))
                    {
                        now(&prvt->last_clear_buffer_time);
                    }
                    else
                    {
                        break;
                    }

                    for(int k = 0; k < prvt->num_of_connections; k++) //send outqueue packets via all the connections
                    {
                        sendPacket( prvt, k, (char*)buf, (head->len + 2) * sizeof(unsigned int) );
                    } //for
#ifdef PRINT_DEBUG
                    dout << "[Resend output queue] Sequence num: " << (int)head->seq << '\n';
#endif
                } //if
            } //if
        } //if
        else
        {
            ++(prvt->socket[sock_id].errflag);
            more = 0;
            break;
        } //else

        p_crc = 0; //reset the CRC
        byte_crc = 0;
        // state 1 - when size of data in inqueue is >= 4
        while (prvt->valid >= sizeof(unsigned int))
        {
            if (prvt->in_buf[begin] == SERIAL_MAGIC) //Magic number is OK
            {
                if (prvt->in_buf[begin+2] == 0) //Len is 0
                {
                    if (prvt->in_buf[begin+3] == ACKNOWLEDGE) //ACK packet
                    {
                        int position = prvt->outqueue.reader;
                        for (; position != prvt->outqueue.writer; position = Dqueue_next(position))
                        {
                            unsigned int* buf = prvt->outqueue.queue[position]->buf() - 2;
                            unsigned char seq = reinterpret_cast<serial_header*>(buf+1)->seq;
                            if (seq == prvt->in_buf[begin+1])
                            {
                                break;                              //return the position of the outqueue item
                            }
                        } //for
                        if(position != prvt->outqueue.writer)
                        {
                            //Removing all the packets pending for ack up till the matching ACK
                            for(int tag = prvt->outqueue.reader; tag != position; tag = Dqueue_next(tag))
                            {
                                GetListEnd(0, prvt->outqueue.queue[tag]); //delete the pending for ack packets
                                prvt->outqueue.reader = Dqueue_next(prvt->outqueue.reader);
                            } //for
                            if (prvt->socket[sock_id].errflag <= ERR_TOLERANCE)
                            {
                                prvt->socket[sock_id].errflag = 0;
                            }
                        } //if packet found
                        prvt->valid -= sizeof(unsigned int);
                        begin += sizeof(unsigned int);
                        now(&prvt->socket[sock_id].recvtime);
                        continue;
                    } //if ACK packet
                    else if (prvt->in_buf[begin+3] == GETLIST)
                    {
                        int position = prvt->cbqueue.reader;
                        for (; position != prvt->cbqueue.writer; position = Dqueue_next(position))
                        {
                            unsigned int* buf = prvt->cbqueue.queue[position]->buf() - 2;
                            unsigned char seq = reinterpret_cast<serial_header*>(buf+1)->seq;
                            if (seq == prvt->in_buf[begin+1])
                            {
                                break;                              //return the position of the outqueue item
                            }
                        } //for
                        if(position != prvt->cbqueue.writer)
                        {
                            if (defercb) //defer sending to application layer
                            {
                                cache_typ* pcache;
                                if (!Dqueue_full(&prvt->cbqueue) && ((pcache = cache_typ::h_allocate(0)) != 0))
                                {
                                    pcache->set_id(prvt->in_buf[begin+1]);
                                    pcache->set_cmd(GETLIST);
                                    Dqueue_bottom(&prvt->cbqueue,pcache);

                                    prvt->cbqueue.writer = Dqueue_next(prvt->cbqueue.writer);
                                } //if
                                else
                                {
#ifdef PRINT_DEBUG
                                    dout << "Callback queue overflow\n";
#endif
                                } //else
                            } //if
                            else //send to application layer immediately
                            {
                                unsigned int tbuf[HEADER_SIZE+HEADER_OFF];
                                cache_typ* pcache = reinterpret_cast<cache_typ*>(&tbuf[0]);
                                pcache->construct(0);
                                pcache->set_id(prvt->in_buf[begin+1]);
                                pcache->set_cmd(GETLIST);
                                (*(callback[GETLIST]))(callback_arg[GETLIST], prvt->in_buf[begin+1], pcache);
                            } //else
                            if (prvt->socket[sock_id].errflag <= ERR_TOLERANCE)
                            {
                                prvt->socket[sock_id].errflag = 0;
                            }
                        } //if position >=0
                        prvt->valid -= sizeof(unsigned int);
                        begin += sizeof(unsigned int);
                        now(&prvt->socket[sock_id].recvtime);
                        continue;
                    } //else if GETLIST
                    else if (prvt->in_buf[begin+3] == RESEND)
                    {
#ifdef PRINT_DEBUG
                        dout<<"received RESEND packet ID: "<< (int)(prvt->in_buf[begin+1])<<"\n";
#endif
                        timeval current_time;
                        now(&current_time);
                        if( ((current_time.tv_sec - prvt->last_resend_recv_time.tv_sec)>prvt->resend_interval.tv_sec)
                            || (((current_time.tv_sec - prvt->last_resend_recv_time.tv_sec)==prvt->resend_interval.tv_sec)&&
                                ((current_time.tv_usec - prvt->last_resend_recv_time.tv_usec)>prvt->resend_interval.tv_usec)) )
                        {
                            ///////////////////////////////////////////////////////////////////////////////
                            //see if can find the packet first
                            ///////////////////////////////////////////////////////////////////////////////
                            int position = prvt->outqueue.reader;
                            for (; position != prvt->outqueue.writer; position = Dqueue_next(position))
                            {
                                unsigned int* buf = prvt->outqueue.queue[position]->buf() - 2;
                                unsigned char seq = reinterpret_cast<serial_header*>(buf+1)->seq;
                                if (seq == prvt->in_buf[begin+1])
                                {
                                    break;                              //return the position of the outqueue item
                                }
                            } //for
                            if( position != prvt->outqueue.writer)
                            {
                                //////////////////////////////////////////////////////////////////////////
                                //	iterate through the entire outqueue and for those pending for ack packets and their seq num
                                //	matches the ID of the RESEND packet, send out the packet and break
                                //////////////////////////////////////////////////////////////////////////
                                unsigned int* buf = prvt->outqueue.queue[position]->buf() - 2;
                                for(int i =0; i<prvt->num_of_connections; i++) //iterate thr' all the sockets and send out packet from all connections
                                {
                                    sendPacket( prvt, i, (char*)buf, (reinterpret_cast<serial_header*>(buf+1)->len + 2) * sizeof(unsigned int));  //send packet
#ifdef PRINT_DEBUG
                                    dout<< "RE-send packet: "<<(int)((reinterpret_cast<serial_header*>(buf+1)->seq))<<"\n";
#endif
                                } //for
                                  //Erase all the packet that before resend position
                                for (int tag = prvt->outqueue.reader; tag<position && tag != prvt->outqueue.writer; tag = Dqueue_next(tag))
                                {
                                    GetListEnd(0, prvt->outqueue.queue[tag]); //if resend not for this packet then erase from out queue list
                                    prvt->outqueue.reader = Dqueue_next(prvt->outqueue.reader);
                                } //for
                            } //if position >= 0
                            else
                            {
                                prvt->socket[sock_id].errflag = ERR_TOLERANCE + 1; //means packet corresponding to resend ID not found
                            }
                            now(&prvt->last_resend_recv_time);
                        } //if resend interval is over - can still send resends again
                        now(&prvt->socket[sock_id].recvtime);
                        prvt->valid -= sizeof(unsigned int);
                        begin += sizeof(unsigned int);
                        continue;
                    } //if reset command
                    else if ((prvt->in_buf[begin+3] == RESET) && (prvt->in_buf[begin+1] == 0xff))
                    {
                        timeval current_time;
                        now(&current_time);
                        if( ((current_time.tv_sec - prvt->last_reset_recv_time.tv_sec)>prvt->reset_interval.tv_sec)
                            || (((current_time.tv_sec - prvt->last_reset_recv_time.tv_sec)==prvt->reset_interval.tv_sec)&&
                                ((current_time.tv_usec - prvt->last_reset_recv_time.tv_usec)>prvt->reset_interval.tv_usec)) )
                        {
                            EnterResetState(burstout);
                        }
                        prvt->valid -= sizeof(unsigned int);
                        begin += sizeof(unsigned int);
                        now(&prvt->socket[sock_id].recvtime);
                        continue;
                    } //if RESET
                    else if ((prvt->in_buf[begin+3] == ALIVE) && (prvt->in_buf[begin+1] == 0xff)) //ALIVE messages
                    {
#ifdef PRINT_DEBUG
                        dout<<"Receive keepalive\n"; // keep alive messages
#endif
                        prvt->valid -= sizeof(unsigned int);
                        begin += sizeof(unsigned int);
                        now(&prvt->socket[sock_id].recvtime);
                        if (prvt->socket[sock_id].errflag <= ERR_TOLERANCE)
                        {
                            prvt->socket[sock_id].errflag = 0;
                        }
                        continue;
                    }
                    else if (prvt->in_buf[begin+3] == FLOWCONTROL)
                    {
                        prvt->intv = static_cast<unsigned int>(prvt->in_buf[begin+1]);
                        prvt->valid -= sizeof(unsigned int);
                        begin += sizeof(unsigned int);
                        now(&prvt->socket[sock_id].recvtime);
                        if (prvt->socket[sock_id].errflag <= ERR_TOLERANCE)
                        {
                            prvt->socket[sock_id].errflag = 0;
                        }
                        continue;
                    }
                } //if length field is 0
                else if (byte_crc == sizeof(unsigned int)) //non admin packets
                {
                    if ( (prvt->in_buf[begin+2]+1) * sizeof(unsigned int) > prvt->valid) //if packet size > buffer size
                    {
                        break;
                    }
                    mutils_word32 crc;                                          //crc data type
                    mhash_clear_crc32(&crc);                                    //clear_crc
                    mhash_crc32(&crc, &(prvt->in_buf[begin]), (prvt->in_buf[begin+2]+1)*sizeof(unsigned int));
                    crc = BE((unsigned int)crc);                        //crc
                    if (memcmp(&crc, p_crc, sizeof(unsigned int)) == 0) //crc ok
                    {
                        if (prvt->in_buf[begin+3] == 0x80) //if sequence number is 128 implies end of messageEnd
                        {
                            if (defercb) // Message
                            {
                                cache_typ* pcache;
                                if ((prvt->in_buf[begin+1] == prvt->getid) && ((pcache = SetListStart(0, prvt->in_buf[begin+2])) != 0))
                                {
                                    pcache->set_cmd(MESSAGE);
                                    pcache->set_id(prvt->in_buf[begin+1]);
                                    pcache->set_len(prvt->in_buf[begin+2]);
                                    memcpy(pcache->buf(), &prvt->in_buf[begin+4], prvt->in_buf[begin+2]*sizeof(unsigned int));
                                    prvt->query_buf = pcache;
                                }
                                else if (!Dqueue_full(&prvt->cbqueue) && ((pcache = SetListStart(0, prvt->in_buf[begin+2])) != 0))
                                {
                                    pcache->set_cmd(MESSAGE);
                                    pcache->set_id(prvt->in_buf[begin+1]);
                                    pcache->set_len(prvt->in_buf[begin+2]);
                                    memcpy(pcache->buf(), &prvt->in_buf[begin+4], prvt->in_buf[begin+2]*sizeof(unsigned int));
                                    Dqueue_bottom(&prvt->cbqueue,pcache);
                                    prvt->cbqueue.writer = Dqueue_next(prvt->cbqueue.writer);
                                }
                                else
                                {
#ifdef PRINT_DEBUG
                                    dout << "Callback queue overflow\n";
#endif
                                }
                            }
                            else
                            {
                                unsigned int id = prvt->in_buf[begin+1];
                                unsigned int len = prvt->in_buf[begin+2];
                                cache_typ* pcache = reinterpret_cast<cache_typ*>(&prvt->in_buf[begin+4]) - 1;
                                pcache->set_cmd(MESSAGE);
                                pcache->set_len(len);
                                (*(callback[MESSAGE]))(callback_arg[MESSAGE], id, pcache);
                                prvt->in_buf[begin+2] = len; // restore the length, needed later
                            }
                        } //if sequence number is 128
                        else if ( (prvt->socket[sock_id].errflag > ERR_TOLERANCE) )
                        {
                        }
                        else if ( (prvt->in_buf[begin+3] == prvt->innum) || (prvt->in_buf[begin+3] == 0))
                        {
                            SendMessageT( prvt->in_buf[begin+3], ACKNOWLEDGE );
                            if (prvt->in_buf[begin+3] == 0) // SEQ NUM is 0
                            {
                                // Assume a reset condition
                                prvt->innum = 1;
                                prvt->last_resend_innum = 0x80;

                                //clear inqueue and cbqueue
                                for (int tag = prvt->inqueue.reader; tag != prvt->inqueue.writer; tag = Dqueue_next(tag))
                                {
                                    GetListEnd(0, prvt->inqueue.queue[tag]);
                                }
                                prvt->inqueue.writer = 0;
                                prvt->inqueue.reader = 0;

                                for (int tag = prvt->cbqueue.reader; tag != prvt->cbqueue.writer; tag = Dqueue_next(tag))
                                {
                                    GetListEnd(0, prvt->cbqueue.queue[tag]);
                                }
                                prvt->cbqueue.writer = 0;
                                prvt->cbqueue.reader = 0;

                                if (!burstout) //either connection 1 or 2 was previously connected, so blast out all the pending ACK packets
                                {
                                    unsigned char seq = 0; //sequence number of the outgoing packet
                                    for (int tag = prvt->outqueue.reader; tag != prvt->outqueue.writer; tag = Dqueue_next(tag), ++seq)
                                    {
                                        timeval current_time;
                                        now(&current_time);
                                        if( ((current_time.tv_sec - prvt->last_clear_buffer_time.tv_sec)>prvt->clear_outbuf_interval.tv_sec)
                                            ||( ((current_time.tv_sec - prvt->last_clear_buffer_time.tv_sec)==prvt->clear_outbuf_interval.tv_sec)
                                                &&((current_time.tv_usec - prvt->last_clear_buffer_time.tv_usec)>prvt->clear_outbuf_interval.tv_usec)))
                                        {
                                            now(&prvt->last_clear_buffer_time);
                                        }
                                        else
                                        {
                                            break;
                                        }
                                        unsigned int* buf = prvt->outqueue.queue[tag]->buf() - 2;
                                        serial_header* head = reinterpret_cast<serial_header*>(buf+1);
                                        head->seq = seq;
                                        mhash_clear_crc32(reinterpret_cast<mutils_word32*>(buf));
                                        mhash_crc32(reinterpret_cast<mutils_word32*>(buf), buf+1, (head->len+1)*sizeof(unsigned int));  // the crc is packed at the beginning
                                        buf[0] = BE(buf[0]);
                                        for(int i = 0; i<prvt->num_of_connections; i++) //send out data from both ports
                                        {
                                            sendPacket( prvt, i, (char*)buf, (head->len + 2) * sizeof(unsigned int) );
                                        } //for
                                    } //for there is items in outqueue
                                    prvt->outnum = seq; //set outnum to seq number which is incremented with every outgoing packet
                                } //if !burstout
                            } //if
                            else
                            {
                                ++(prvt->innum) &= 0x7F;  //if innum is not 0 increment innum

                            }
                            if (defercb)
                            {
                                cache_typ* pcache;
                                if ((prvt->in_buf[begin+1] == prvt->getid) && ((pcache = SetListStart(0, prvt->in_buf[begin+2])) != 0))
                                {
                                    pcache->set_id(prvt->in_buf[begin+1]);
                                    pcache->set_len(prvt->in_buf[begin+2]);
                                    memcpy(pcache->buf(), &prvt->in_buf[begin+4], prvt->in_buf[begin+2]*sizeof(unsigned int));
                                    prvt->query_buf = pcache;
                                }
                                else if (!Dqueue_full(&prvt->cbqueue) && ((pcache = SetListStart(0, prvt->in_buf[begin+2])) != 0))
                                {
                                    pcache->set_id(prvt->in_buf[begin+1]);
                                    pcache->set_len(prvt->in_buf[begin+2]);
                                    memcpy(pcache->buf(), &prvt->in_buf[begin+4], prvt->in_buf[begin+2]*sizeof(unsigned int));

                                    Dqueue_bottom(&prvt->cbqueue,pcache);
                                    prvt->cbqueue.writer = Dqueue_next(prvt->cbqueue.writer);
                                }
                                else
                                {
#ifdef PRINT_DEBUG
                                    dout << "Callback queue overflow\n";
#endif
                                }
                            } //defercb is true
                            else
                            {
                                unsigned int id = prvt->in_buf[begin+1];
                                unsigned int len = prvt->in_buf[begin+2];
                                cache_typ* pcache = reinterpret_cast<cache_typ*>(&prvt->in_buf[begin+4]) - 1;
                                pcache->set_cmd(SETLIST);
                                pcache->set_len(len);
                                (*(callback[SETLIST]))(callback_arg[SETLIST], id, pcache);
                                prvt->in_buf[begin+2] = len; // restore the length, needed later
                            } //defercb is false

                            // check to see if inqueue has anything of value
                            while (!Dqueue_empty(&prvt->inqueue))
                            {
                                if (Dqueue_top(&prvt->inqueue)->get_seq() == prvt->innum)
                                {
                                    if (defercb)
                                    {
                                        if (!Dqueue_full(&prvt->cbqueue))
                                        {
                                            Dqueue_bottom(&prvt->cbqueue,Dqueue_top(&prvt->inqueue));
                                            prvt->cbqueue.writer = Dqueue_next(prvt->cbqueue.writer);
                                            prvt->inqueue.reader = Dqueue_next(prvt->inqueue.reader);
                                        }
                                        else
                                        {
#ifdef PRINT_DEBUG
                                            dout << "Callback queue overflow\n";
#endif
                                        }
                                    }
                                    else
                                    {
                                        cache_typ* pcache = Dqueue_top(&prvt->inqueue);
                                        (*(callback[SETLIST]))(callback_arg[SETLIST], pcache->get_id(), pcache);
                                        GetListEnd(0, pcache);
                                        prvt->inqueue.reader = Dqueue_next(prvt->inqueue.reader);
                                    }
                                    ++(prvt->innum) &= 0x7f;
                                    SendMessageT(prvt->innum, ACKNOWLEDGE);
                                } //if
                                else //queue_top(&prvt->inqueue)->get_seq() != prvt->innum
                                {
                                    // check if the next sequence is really far away
                                    unsigned short seq = prvt->innum, seq1 = Dqueue_top(&prvt->inqueue)->get_seq();
                                    int cnt;
                                    for (cnt = 0; cnt < 8; ++cnt, ++seq &= 0x7f)
                                    {
                                        if (seq == seq1)
                                        {
                                            break;
                                        }
                                    }
                                    if (cnt == 8) // trash the queue
                                    {
                                        for (int tag = prvt->inqueue.reader; tag != prvt->inqueue.writer; tag = Dqueue_next(tag))
                                        {
                                            GetListEnd(0, prvt->inqueue.queue[tag]);
                                        }
                                        prvt->inqueue.writer = 0;
                                        prvt->inqueue.reader = 0;
                                    }
                                    break;
                                } //else
                            } //while
                        } //else if
                        else if (((prvt->in_buf[begin+3] + 1) & 0x7f) == prvt->innum)
                        {
#ifdef PRINT_DEBUG
                            dout << "Old sequence received. Packet ignored\n";
#endif
                        } //else if
                        else if (((prvt->in_buf[begin+3] + 2) & 0x7f) == prvt->innum)
                        {
#ifdef PRINT_DEBUG
                            dout << "Old sequence received. Packet ignored\n";
#endif
                        } //else if
                        else if (((prvt->in_buf[begin+3] + 3) & 0x7f) == prvt->innum)
                        {
#ifdef PRINT_DEBUG
                            dout << "Old sequence received. Packet ignored\n";
#endif
                        } //else if
                        else if (((prvt->in_buf[begin+3] + 4) & 0x7f) == prvt->innum)
                        {
#ifdef PRINT_DEBUG
                            dout << "Old sequence received. Packet ignored\n";
#endif
                        } //else if
                        else if (((prvt->in_buf[begin+3] + 5) & 0x7f) == prvt->innum)
                        {
#ifdef PRINT_DEBUG
                            dout << "Old sequence received. Packet ignored\n";
#endif
                        } //else if
                        else if (((prvt->in_buf[begin+3] + 6) & 0x7f) == prvt->innum)
                        {
#ifdef PRINT_DEBUG
                            dout << "Old sequence received. Packet ignored\n";
#endif
                        } //else if
                        else if (((prvt->in_buf[begin+3] + 7) & 0x7f) == prvt->innum)
                        {
#ifdef PRINT_DEBUG
                            dout << "Old sequence received. Packet ignored\n";
#endif
                        } //else if
                        else if (((prvt->in_buf[begin+3] + 8) & 0x7f) == prvt->innum)
                        {
#ifdef PRINT_DEBUG
                            dout << "Old sequence received. Packet ignored\n";
#endif
                        }
                        else
                        {
                            if (prvt->last_resend_innum != 0x80) //last_resend innum is last received packet seq num + 1
                            {
                                //if received packet is not within 8 numbers from the last received packet
                                //then ask for resend else do not ask for resend to minimise network
                                //congestion
                                for (int cnt1 = 0; cnt1 < 8; ++cnt1)
                                {
                                    ++(prvt->last_resend_innum) &= 0x7f;
                                    if (prvt->in_buf[begin+3] == prvt->last_resend_innum)
                                    {
                                        goto receive_label1;
                                    }
                                } //for loop
                            }
#ifdef PRINT_DEBUG
                            dout << "RM Message resend for packet " << (int)prvt->innum << ", received " << (int)prvt->in_buf[begin+3] <<'\n';
#endif
                            SendMessageT(prvt->innum, RESEND);
                            prvt->last_resend_innum = (prvt->in_buf[begin+3] + 1) & 0x7f;
receive_label1:
                            cache_typ *pcache;
                            if (!Dqueue_full(&prvt->inqueue) && ((pcache = SetListStart(0, prvt->in_buf[begin+2])) != 0))
                            {
                                pcache->set_id(prvt->in_buf[begin+1]);
                                pcache->set_len(prvt->in_buf[begin+2]);
                                pcache->set_seq(prvt->in_buf[begin+3]);
                                memcpy(pcache->buf(), &prvt->in_buf[begin+4], prvt->in_buf[begin+2]*sizeof(unsigned int));
                                Dqueue_bottom(&prvt->inqueue,pcache);
                                prvt->inqueue.writer = Dqueue_next(prvt->inqueue.writer);
                            }
                            else
                            {
#ifdef PRINT_DEBUG
                                dout << "Unable to buffer any more incoming packets, inqueue will be cleared\n";
#endif
                                //input buffer full so clear inqueue
                                for (int tag = prvt->inqueue.reader; tag != prvt->inqueue.writer; tag = Dqueue_next(tag))
                                {
                                    GetListEnd(0, prvt->inqueue.queue[tag]);
                                }
                                prvt->inqueue.writer = 0;
                                prvt->inqueue.reader = 0;
                            } //else
                        } //else
                        prvt->valid -= (prvt->in_buf[begin+2]+1) * sizeof(unsigned int);
                        begin += (prvt->in_buf[begin+2]+1) * sizeof(unsigned int);
                        byte_crc = 0;
                        now(&prvt->socket[sock_id].recvtime);
                        if (prvt->socket[sock_id].errflag <= ERR_TOLERANCE)
                        {
                            prvt->socket[sock_id].errflag = 0;
                        }
                        continue;
                    } //CRC passed
                    else //CRC failed
                    {
#ifdef PRINT_DEBUG
                        dout <<"Valid data remain::"<< (int)prvt->valid<<", RM Message CRC failed... \n";
#endif
                    } //
                } //non admin packets
            } //if prvt->in_buf is serial magic
              // neither admin nor message
            if (byte_crc < sizeof(unsigned int))
            {
                if (byte_crc == 0)
                {
                    p_crc = &prvt->in_buf[begin];
                }
                ++byte_crc;
            } //if byte_crc < sizeof(unsigned int)
            else
            {
                ++p_crc;
            } //else
            begin++;
            --(prvt->valid);
        } //while prvt is valid
        if ( byte_crc > 0 ) //state 2 - at this state, size of data in inqueue is <4
        {
            if ( p_crc != prvt->in_buf )
            {
                memmove(prvt->in_buf, p_crc, prvt->valid+byte_crc);
            }
            prvt->valid += byte_crc;
            begin = 0;
        } //byte_crc > 0
        else if ((begin > 0) && (prvt->valid > 0))
        {
            memmove(prvt->in_buf, &prvt->in_buf[begin], prvt->valid);
            begin = 0;
        } //begin > 0 and valid > 0
    } //while more && ...
    if (prvt->socket[sock_id].errflag > ERR_TOLERANCE)
    {
        prvt->close_connection = sock_id; //close the single connection
        Close();
    }
    prvt->readflag = false;
    if(prvt->valid > 0)
    {
        memcpy(prvt->socket[sock_id].lo_buf,&prvt->in_buf[begin], prvt->valid);
    }
    prvt->socket[sock_id].leftover = prvt->valid;
    prvt->valid = 0;
} //ReceivePacket

bool RMDSerialSocket::WaitForHandshakeCompletion(const struct timeval*)
{
    return true;
}

void RMDSerialSocket::KeepAlive() //KeepAlive messages is sent through all connections
{
    timeval current_time;
    now(&current_time);

    bool pkt_sent = false;
    int result=0;

    if (Connected())
    {
        //find out whether any of the connections has time out for keeping alive
        //if so then send keep alive messages else do nothing
        bool alive_timeout = true;
        int intv = (prvt->intv) ? prvt->intv : 4;   //set flow control in config file to default value of 4 is it is 0
        for(int index=0; alive_timeout && index<prvt->num_of_connections; index++)
        {
            if( current_time.tv_sec - prvt->socket[index].sendtime.tv_sec < ((time_t)intv))
            {
                alive_timeout = false;
            }
        }
        if(alive_timeout) //alive timeout
        {
            if (!Dqueue_empty(&prvt->outqueue)) //outqueue is not empty
            {
                //iterate thr all the packets in outqueue
                for (int tag = prvt->outqueue.reader; tag != prvt->outqueue.writer; tag = Dqueue_next(tag)) //iterate thr the outqueue
                {
                    unsigned int* buf = prvt->outqueue.queue[tag]->buf()- 2;        // this is where the compact packet starts, right after magic number
                    serial_header* head = reinterpret_cast<serial_header*>(buf+1);  // header is 4 bytes away
                    result=0;
                    timeval current_time;
                    now(&current_time);
                    if( ((current_time.tv_sec - prvt->last_clear_buffer_time.tv_sec)>prvt->clear_outbuf_interval.tv_sec)
                        ||( ((current_time.tv_sec - prvt->last_clear_buffer_time.tv_sec)==prvt->clear_outbuf_interval.tv_sec)
                            &&((current_time.tv_usec - prvt->last_clear_buffer_time.tv_usec)>prvt->clear_outbuf_interval.tv_usec)))
                    {
                        now(&prvt->last_clear_buffer_time);
                    }
                    else
                    {
                        break;
                    }



                    for( int i=0; i < prvt->num_of_connections; i++ ) //send thr all the connections
                    {
                        int t_result=sendPacket( prvt, i, (char*)buf, (head->len + 2) * sizeof(unsigned int) );
                        result=(t_result==0) ? result : t_result;
#ifdef PRINT_DEBUG
                        if(result!=0)
                        {
                            dout << "[Keep alive resend for socket]: "<<i<<" seq num: "<< (int)head->seq << '\n';
                        }
#endif
                    } //iterate through all the ports
                    if(result==0&&!pkt_sent)
                    {
                        pkt_sent=true;
                    }
                } //for
            } //if outqueue is not empty

            if (Dqueue_empty(&prvt->outqueue) && prvt->reestablish_link_flag )
            {
                prvt->reestablish_link_flag=false;
            }
            if(!pkt_sent)
            {
#ifdef PRINT_DEBUG
                dout<<"Keeping alive\n";
#endif
                SendMessageT(0xFF, ALIVE); //sending alive message
            } //else
        } //if alive time out is true
    } //if connected
} //KeepAlive

bool RMDSerialSocket::Flush()
{
    return true;
}

int RMDSerialSocket::GetHandle() const
{
    int id = -1;
    for(int i = 0; id < 0 && i < prvt->num_of_connections; i++)
    {
        id = (int)prvt->socket[i].sock.get_sock();
    }
    return id;
}

bool RMDSerialSocket::CheckLink()
{
    timeval current_time;
    now(&current_time);
    for(int i = 0; i<prvt->num_of_connections; i++ )
    {
        if( (prvt->socket[i].conn_state == 21) &&
            !(prvt->intv && ((current_time.tv_sec - prvt->socket[i].recvtime.tv_sec) > (time_t)(prvt->intv * 2))))
        {
            if(prvt->link_state == OFF)
            {
                prvt->link_state = ON;
                prvt->reestablish_link_flag = true;
                prvt->last_clear_buffer_time.tv_sec=0;
                prvt->last_clear_buffer_time.tv_usec=0;
            } //if
            return true;
        } //if
    } //for

    if( prvt->link_state==ON && !Dqueue_empty(&prvt->outqueue) )
    {
        typedef std::map<unsigned char, temp_typ > std_map; //do sorting algo here
        std_map my_map;
        unsigned char seq_num = 0;

        int index=0;
        if(!Dqueue_empty(&prvt->outqueue))
        {
            unsigned int* buf = prvt->outqueue.queue[prvt->outqueue.reader]->buf()-2;
            serial_header* head = reinterpret_cast<serial_header*>(buf+1);
            seq_num=head->seq;
        } //if

        for(index = prvt->outqueue.writer; index != prvt->outqueue.reader; )
        {
            if(prvt->outqueue.queue[index]!=NULL)
            {
                unsigned int* buf = prvt->outqueue.queue[index]->buf()-2;
                serial_header* head = reinterpret_cast<serial_header*>(buf+1);
                unsigned char tmp_id=head->id;
                if( my_map.find(tmp_id)==my_map.end() )
                {
                    my_map[tmp_id].header.len=head->len;
                    my_map[tmp_id].header.id=head->id;
                    my_map[tmp_id].header.seq=head->seq;
                    my_map[tmp_id].header.magic=head->magic;
                    memcpy( &(my_map[tmp_id].temp_buf[0]), head+1, (( (unsigned int)(my_map[tmp_id].header.len))*sizeof(unsigned int)) );
                }
                GetListEnd(tmp_id, prvt->outqueue.queue[index]);
                prvt->outqueue.queue[index] = NULL;
            } //if
            if(index ==0)
            {
                index = RING_SIZE-1;
            }
            else
            {
                --index;
            }
        } //for

        prvt->outqueue.reader = 0;
        prvt->outqueue.writer = 0;

        for(std_map::const_iterator iter = my_map.begin(); iter!= my_map.end(); ++iter, ++seq_num, prvt->outqueue.writer=Dqueue_next(prvt->outqueue.writer))
        {
            cache_typ* pcache=cache_typ::h_allocate((int)iter->second.header.len);
            memcpy(pcache->buf(),&iter->second.temp_buf[0],(iter->second.header.len)*sizeof(int));
            prvt->outqueue.queue[prvt->outqueue.writer]=pcache;
            unsigned int* buf = prvt->outqueue.queue[prvt->outqueue.writer]->buf() - 2;
            serial_header* head = reinterpret_cast<serial_header*>(buf+1);
            head->id=iter->second.header.id;
            head->len=iter->second.header.len;
            head->seq=seq_num;
            head->magic=iter->second.header.magic;
            mhash_clear_crc32(reinterpret_cast<mutils_word32*>(buf));
            mhash_crc32(reinterpret_cast<mutils_word32*>(buf), buf+1, (head->len+1)*sizeof(unsigned int));  // the crc is packed at the beginning
            buf[0] = BE(buf[0]);
        } //for
    } //if
    prvt->link_state=OFF;
    return false;
}

void RMDSerialSocket::CheckThroughput(bool flag)
{
    prvt->check_max_thrput=flag;
}

void RMDSerialSocket::setResetInterval(long sec, long microsec)
{
    prvt->reset_interval.tv_sec=sec;
    prvt->reset_interval.tv_usec=microsec;
}

void RMDSerialSocket::setResendInterval(long sec, long microsec)
{
    prvt->resend_interval.tv_sec=sec;
    prvt->resend_interval.tv_usec=microsec;
}

void RMDSerialSocket::setClearOutputBufferInterval(long sec, long microsec)
{
    prvt->clear_outbuf_interval.tv_sec=sec;
    prvt->clear_outbuf_interval.tv_usec=microsec;
}

bool RMDSerialSocket::HandshakeAck(unsigned int, unsigned int)
{
    // DSerial does not handshake nor handshake acknowledge
    return true;
}

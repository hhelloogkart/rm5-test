/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "queue.h"
#include <cache.h>
#include "smcache.h"
#include <util/dbgout.h>
#include <time/timecore.h>
#include <mm/event.h>
#include <assert.h>

static unsigned int ring_size = 0;
static const int element_size = sizeof(relative_ptr<cache_typ, cache_sharedmemory_segment>);

/*
   Queue shared memory is partitioned into the queue_data at the front, followed by a ring buffer of ring_size bytes.
   In every increment (queue_advance), the reader and writer index is incremented by sizeof relative pointer (element_size).
 */
struct queue_data
{
    unsigned int writer;
    unsigned int reader;
};

inline unsigned int queue_advance(unsigned int p)
{
    return (p + element_size) % ring_size;
}

inline bool queue_full(queue_data* q)
{
    return queue_advance(q->writer) == q->reader;
}

inline bool queue_empty(queue_data* q)
{
    return q->reader == q->writer;
}

inline relative_ptr<cache_typ,cache_sharedmemory_segment>& queue_element(queue_data* q, unsigned int idx)
{
    return *reinterpret_cast<relative_ptr<cache_typ,cache_sharedmemory_segment>*>(
               reinterpret_cast<char*>(q) + sizeof(queue_data) + idx);
}

inline relative_ptr<cache_typ,cache_sharedmemory_segment>& queue_top(queue_data* q)
{
    return queue_element(q, q->reader);
}

inline relative_ptr<cache_typ,cache_sharedmemory_segment>& queue_bottom(queue_data* q)
{
    return queue_element(q, q->writer);
}

static void lock(MM* mm_ptr)
{
    while ((mm_lock(mm_ptr, MM_LOCK_RW) == 0) && mm_ptr)
    {
        millisleep(1);
    }
}

static void rdlock(MM* mm_ptr)
{
    while ((mm_lock(mm_ptr, MM_LOCK_RD) == 0) && mm_ptr)
    {
        millisleep(1);
    }
}

static void unlock(MM* mm_ptr)
{
    mm_unlock(mm_ptr);
}

queue_factory::queue_factory(rmglobal_typ& glob, unsigned int sz)
    : global_sm(glob), store(glob.ptr->store)
{
    glob.attach_queue(sz);
}

void queue_factory::push(cache_typ* ptr, unsigned int client)
{
    queue_data* qd = reinterpret_cast<queue_data*>(global_sm.queue[client]);

    rdlock(global_sm.queue[client]);
    bool full = queue_full(qd);
    unlock(global_sm.queue[client]);
    if (full)
    {
        return;
    }

    smcache_typ::sm_reserve(ptr);

    lock(global_sm.queue[client]);
    bool empty = queue_empty(qd);
    queue_bottom(qd) = ptr;
    qd->writer = queue_advance(qd->writer);
    unlock(global_sm.queue[client]);

    if (empty)
    {
        global_sm.queue_event[client]->signal();
    }
}

void queue_factory::pop(unsigned int client)
{
    cache_typ* ptr = top(client);
    if (ptr == NULL)
    {
        return;
    }

    queue_data* qd = reinterpret_cast<queue_data*>(global_sm.queue[client]);
    lock(global_sm.queue[client]);
    qd->reader = queue_advance(qd->reader);
    unlock(global_sm.queue[client]);

    smcache_typ::sm_unreserve(ptr);
}

cache_typ* queue_factory::top(unsigned int client) const
{
    queue_data* qd = reinterpret_cast<queue_data*>(global_sm.queue[client]);
    assert((qd->reader <= ring_size) && (qd->writer <= ring_size));
    rdlock(global_sm.queue[client]);
    if (!queue_empty(qd))
    {
        cache_typ* ptr = queue_top(qd);
        unlock(global_sm.queue[client]);
        return ptr;
    }
    unlock(global_sm.queue[client]);
    return NULL;
}

void queue_factory::clear(unsigned int client)
{
    if (ring_size == 0)
    {
        // figure out how large is the memory
        const unsigned int sz = static_cast<unsigned int>(mm_core_size(global_sm.queue[0]) - sizeof(queue_data));
        ring_size = (sz / element_size) * element_size;
    }
    queue_data* qd = reinterpret_cast<queue_data*>(global_sm.queue[client]);
    lock(global_sm.queue[client]);
    if ((qd->reader <= ring_size) && (qd->writer <= ring_size))
    {
        while (!queue_empty(qd))
        {
            cache_typ* ptr = queue_top(qd);
            unlock(global_sm.queue[client]);    // to prevent deadlock
            smcache_typ::sm_unreserve(ptr);    // this locks the cache
            lock(global_sm.queue[client]);    // lock it back again
            qd->reader = queue_advance(qd->reader);
        }
    }
    qd->reader = 0;
    qd->writer = 0;
    unlock(global_sm.queue[client]);
}

void queue_factory::dump(unsigned int client)
{
    queue_data* qd = reinterpret_cast<queue_data*>(global_sm.queue[client]);
    assert((qd->reader <= ring_size) && (qd->writer <= ring_size));
    rdlock(global_sm.queue[client]);
    for (unsigned int idx=qd->reader; idx!=qd->writer; idx=queue_advance(idx))
    {
        cache_typ* ptr = queue_element(qd, idx);
        if (ptr != NULL)
        {
            dout << idx << ':' << "  " << ptr->get_id() << '(' << ptr->get_len() << ") " << ">> ";
            for (unsigned int icnt = 0; icnt < ptr->get_len(); ++icnt)
            {
                dout<< *(ptr->buf() + icnt) << "  ";
            }
            dout << "\n";
        }
    }
    unlock(global_sm.queue[client]);
}

int queue_factory::select(unsigned int client, const struct timeval* stm)
{
    return (int)global_sm.queue_event[client]->trywait(stm);
}

/******************************************************************/
/* Copyright DSO National Laboratories 2020. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef SMCACHE_H
#define SMCACHE_H
class cache_typ;
class rmglobal_typ;

namespace smcache_typ
{
cache_typ* sm_allocate(size_t sz);
cache_typ* sm_duplicate(unsigned int id, size_t len, const unsigned int* src);
void sm_deallocate(cache_typ* ptr);
void sm_reserve(cache_typ* ptr);
void sm_unreserve(cache_typ* ptr);
void lock();
void rdlock();
void unlock();
bool init(rmglobal_typ& glob, unsigned int sz);

#if !defined NO_SSM || !defined NO_SCRAMNET
cache_typ* ssm_allocate(size_t sz);
void ssm_deallocate(cache_typ* ptr);
cache_typ* ssm_duplicate(cache_typ* cache, size_t len);
#endif
}
#endif // SMCACHE_H

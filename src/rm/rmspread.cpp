#include "rmspread.h"
#include <time/timecore.h>
#include <util/utilcore.h>
#include <cache.h>
#include <limits.h>
#include "../spread/sp.h"
#include "rmname.h"
#include "../rmstl.h"

#define CACHETYP_SIZE (sizeof(cache_typ) / sizeof(unsigned int))

struct rm_msg
{
    rmname_typ name;
    bool write_out;
    bool reg_flag;
};

typedef VECTOR(rm_msg) rm_msg_list;
typedef LIST(cache_typ*) rm_defer_list;

struct rmspread_socket_private
{
    mailbox mbox;
    char private_group[MAX_GROUP_NAME];
    rm_msg_list msglist;
    cache_typ* query_buf;
    cache_typ* out_buf;
    cache_typ* small_buf;
    unsigned int getid;
    unsigned int raw_inbuf[READ_BUF+CACHETYP_SIZE];
    char* in_buf;
    rm_defer_list defercb;
};

RMSpreadSocket::RMSpreadSocket() : prvt(new rmspread_socket_private)
{
    prvt->mbox = 0;
    prvt->private_group[0] = '\0';
    prvt->out_buf = cache_typ::h_allocate(WRITE_BUF);
    prvt->small_buf = cache_typ::h_allocate(0);
    prvt->query_buf = 0;
    prvt->getid = UINT_MAX;
    prvt->in_buf = reinterpret_cast<char*>(prvt->raw_inbuf + CACHETYP_SIZE);
    rm_msg msg;
    msg.name = "GETLIST";
    msg.reg_flag = false;
    msg.write_out = false;
    prvt->msglist.push_back(msg);
}

RMSpreadSocket::~RMSpreadSocket()
{
    Close();
    prvt->out_buf->h_unreserve();
    prvt->small_buf->h_unreserve();
    for (rm_defer_list::iterator iter = prvt->defercb.begin(); iter != prvt->defercb.end(); ++iter)
    {
        (*iter)->h_unreserve();
    }
    delete prvt;
}

bool RMSpreadSocket::Connect(const char* host, int port, const struct timeval* stm)
{
    char hostname[64];
    minisprintf(hostname, "%d@%s", port, host);
    sp_time timeout = { 2, 0 };
    if (stm)
    {
        timeout.sec = stm->tv_sec;
        timeout.usec = stm->tv_usec;
    }
    if (SP_connect_timeout(hostname, 0, 0, 0, &prvt->mbox, prvt->private_group, timeout) == ACCEPT_SESSION)
    {
        if (SP_join(prvt->mbox, "GETLIST") == 0)
        {
            return true;
        }
        else
        {
            Close();
        }
    }
    return false;
}

bool RMSpreadSocket::Connected()
{
    return prvt->mbox != 0;
}

bool RMSpreadSocket::Close()
{
    SP_disconnect(prvt->mbox);
    prvt->mbox = 0;
    prvt->msglist.clear();
    return true;
}

int RMSpreadSocket::Select(const struct timeval* stm, bool defercb)
{
    if (!defercb)
    {
        while (!prvt->defercb.empty())
        {
            const cache_typ* p = prvt->defercb.front();
            unsigned int cmd = p->get_cmd();
            if (cmd < ENDCMD)
            {
                (*(callback[cmd]))(callback_arg[cmd], p->get_id(), p);
            }
            prvt->defercb.pop_front();
            const_cast<cache_typ*>(p)->h_unreserve();
        }
    }
    int result = SP_poll(prvt->mbox);
    if (result > 0)
    {
        ReceivePacket(defercb);
        return 1;
    }
    else if (result < 0)
    {
        return -1;
    }

    timeExpire te(stm);
    do
    {
        Sleep(1);
        result = SP_poll(prvt->mbox);
    }
    while ((result == 0) && !te());
    if (result > 0)
    {
        ReceivePacket(defercb);
        return 1;
    }
    return result;
}

const cache_typ* RMSpreadSocket::GetListStart(unsigned int id)
{
    int cnt = 0, ret;
    if (id >= prvt->msglist.size())
    {
        return 0;
    }

    if (SP_multicast(prvt->mbox, RELIABLE_MESS, "GETLIST", 0, RMNAME_LEN, prvt->msglist[id].name) < 0)
    {
        return 0;
    }
    prvt->getid = id;
    do
    {
        if (cnt != 0)
        {
            Sleep(WAIT_TIME);
        }
        struct timeval tm = {
            0, WAIT_TIME*1000
        };
        ret =  Select(&tm, true);
        ++cnt;
    }
    while ((prvt->query_buf == NULL) && (cnt < GETLIST_RETRY) && (ret >= 0));
    cache_typ* ptr = prvt->query_buf;
    prvt->getid = UINT_MAX;
    prvt->query_buf = NULL;
    return ptr;
}

void RMSpreadSocket::GetListEnd(unsigned int, const cache_typ* pbuf)
{
    const_cast<cache_typ*>(pbuf)->h_unreserve();
}

cache_typ* RMSpreadSocket::MessageStart(unsigned int id, size_t len)
{
    return SetListStart(id, len);
}

cache_typ* RMSpreadSocket::SetListStart(unsigned int, size_t len)
{
    if (len <= WRITE_BUF && prvt->out_buf->get_refcount() <= 1)
    {
        prvt->out_buf->set_len(static_cast<unsigned int>(len));
        prvt->out_buf->h_reserve();
        return prvt->out_buf;
    }
    return cache_typ::h_allocate(len);
}

bool RMSpreadSocket::SetListEnd(unsigned int id, cache_typ* pbuf)
{
    if (id >= prvt->msglist.size())
    {
        GetListEnd(id, pbuf);
        return false;
    }
    int result = SP_multicast(prvt->mbox, FIFO_MESS, prvt->msglist[id].name, 0, pbuf->get_len()*sizeof(unsigned int), (const char*)pbuf->buf());
    GetListEnd(id, pbuf);
    prvt->msglist[id].write_out = true;
    return result >= 0;
}

bool RMSpreadSocket::BroadcastEnd(unsigned int id, cache_typ* pbuf)
{
    if (id >= prvt->msglist.size())
    {
        GetListEnd(id, pbuf);
        return false;
    }
    int result = SP_multicast(prvt->mbox, FIFO_MESS, prvt->msglist[id].name, (short)1, pbuf->get_len()*sizeof(unsigned int), (const char*)pbuf->buf());
    GetListEnd(id, pbuf);
    prvt->msglist[id].write_out = false;
    return result >= 0;
}

bool RMSpreadSocket::MessageEnd(unsigned int id, cache_typ* pbuf, void*)
{
    if (id >= prvt->msglist.size())
    {
        GetListEnd(id, pbuf);
        return false;
    }
    int result = SP_multicast(prvt->mbox, UNRELIABLE_MESS, prvt->msglist[id].name, 0, pbuf->get_len()*sizeof(unsigned int), (const char*)pbuf->buf());
    GetListEnd(id, pbuf);
    prvt->msglist[id].write_out = true;
    return result >= 0;
}

bool RMSpreadSocket::RegisterID(const char* str)
{
    rm_msg msg;
    msg.name = str;
    msg.reg_flag = true;
    msg.write_out = false;
    prvt->msglist.push_back(msg);
    SP_join(prvt->mbox, str);
    SP_multicast(prvt->mbox, RELIABLE_MESS, "GETLIST", 0, RMNAME_LEN, msg.name);
    return false;
}

bool RMSpreadSocket::QueryID(const char* str)
{
    rm_msg msg;
    msg.name = str;
    msg.reg_flag = false;
    msg.write_out = false;
    prvt->msglist.push_back(msg);
    SP_join(prvt->mbox, str);
    return false;
}

bool RMSpreadSocket::QueryID(const char* str[], int num)
{
    for (int cnt=0; cnt<num; ++cnt)
    {
        QueryID(str[cnt]);
    }
    return false;
}

bool RMSpreadSocket::ClientID(const char*)
{
    for (rm_msg_list::iterator iter= ++(prvt->msglist.begin()); iter!=prvt->msglist.end(); ++iter)
    {
        SP_leave(prvt->mbox, (*iter).name);
    }
    prvt->msglist.resize(1);
    return false;
}

bool RMSpreadSocket::RegisterIDX(unsigned int id)
{
    if (id >= prvt->msglist.size())
    {
        return false;
    }
    prvt->msglist[id].reg_flag = true;
    return true;
}

bool RMSpreadSocket::UnregisterIDX(unsigned int id)
{
    if (id >= prvt->msglist.size())
    {
        return false;
    }
    prvt->msglist[id].reg_flag = false;
    return true;
}

bool RMSpreadSocket::FlowControl(unsigned int)
{
    return true;
}

bool RMSpreadSocket::WaitForHandshakeCompletion(const struct timeval*)
{
    return true;
}

bool RMSpreadSocket::HandshakeAck(unsigned int, unsigned int)
{
    return true;
}

void RMSpreadSocket::KeepAlive()
{
}

bool RMSpreadSocket::Flush()
{
    return true;
}

int RMSpreadSocket::GetHandle() const
{
    return 0;
}

bool RMSpreadSocket::CheckLink()
{
    return true;
}

bool RMSpreadSocket::Broadcast(unsigned int id, size_t len, const unsigned int* buf)
{
    if (id >= prvt->msglist.size())
    {
        return false;
    }
    int result = SP_multicast(prvt->mbox, FIFO_MESS, prvt->msglist[id].name, (short)1, static_cast<int>(len*sizeof(unsigned int)), (const char*)buf);
    prvt->msglist[id].write_out = false;
    return result >= 0;
}

bool RMSpreadSocket::SetList(unsigned int id, size_t len, const unsigned int* buf)
{
    if (id >= prvt->msglist.size())
    {
        return false;
    }
    int result = SP_multicast(prvt->mbox, FIFO_MESS, prvt->msglist[id].name, 0, static_cast<int>(len*sizeof(unsigned int)), (const char*)buf);
    prvt->msglist[id].write_out = true;
    return result >= 0;
}

bool RMSpreadSocket::Message(unsigned int id, size_t len, const unsigned int* buf)
{
    if (id >= prvt->msglist.size())
    {
        return false;
    }
    int result = SP_multicast(prvt->mbox, UNRELIABLE_MESS, prvt->msglist[id].name, 0, static_cast<int>(len*sizeof(unsigned int)), (const char*)buf);
    prvt->msglist[id].write_out = true;
    return result >= 0;
}

size_t RMSpreadSocket::GetList(unsigned int id, size_t len, unsigned int* buf)
{
    const cache_typ* glbuf = GetListStart(id);
    if (glbuf == 0)
    {
        return 0;
    }
    size_t sz = glbuf->get_len();
    if (sz > (int)len)
    {
        sz = len;
    }
    memcpy(buf, glbuf->buf(), sz*sizeof(unsigned int));
    GetListEnd(id, glbuf);
    return sz;
}

void RMSpreadSocket::ReceivePacket(bool defercb)
{
    service srv;
    char sender[MAX_GROUP_NAME];
    char group[1][MAX_GROUP_NAME];
    int num_groups;
    short mess_type;
    int endian_mismatch;
    int result = SP_receive(prvt->mbox, &srv, sender, 1, &num_groups, group, &mess_type, &endian_mismatch, READ_BUF*sizeof(unsigned int), prvt->in_buf);
    if (result > 0)
    {
        // see if its a getlist response
        if ((prvt->getid != UINT_MAX) && (prvt->msglist[prvt->getid].name == group[0]))
        {
            prvt->query_buf = cache_typ::h_duplicate(prvt->getid, result/sizeof(unsigned int), reinterpret_cast<unsigned int*>(prvt->in_buf));
        }
        // see if its a getlist request
        else if (prvt->msglist[0].name == group[0])
        {
            for (int cnt=1; cnt<(int)prvt->msglist.size(); ++cnt)
            {
                if (prvt->msglist[cnt].name == prvt->in_buf)
                {
                    if (prvt->msglist[cnt].write_out)
                    {
                        (*(callback[GETLIST]))(callback_arg[GETLIST], cnt, prvt->small_buf);
                    }
                    break;
                }
            }
        }
        // check for deferred callback
        else
        {
            for (int cnt=1; cnt<(int)prvt->msglist.size(); ++cnt)
            {
                if (prvt->msglist[cnt].name == group[0])
                {
                    //if you don't register you are not supposed to get it
                    if (prvt->msglist[cnt].reg_flag)
                    {
                        if (defercb)
                        {
                            cache_typ* p = cache_typ::h_duplicate(cnt, result/sizeof(unsigned int), reinterpret_cast<unsigned int*>(prvt->in_buf));
                            p->set_cmd((mess_type==0)?SETLIST:BROADCAST);
                            prvt->defercb.push_back(p);
                        }
                        else
                        {
                            unsigned int cmd =(mess_type==0)?SETLIST:BROADCAST;
                            cache_typ* p = cache_typ::ConvertToCache(prvt->in_buf, result/sizeof(unsigned int), cmd);
                            (*(callback[cmd]))(callback_arg[cmd], cnt, p);
                        }
                    }
                    break;
                }
            }
        }
    }
}

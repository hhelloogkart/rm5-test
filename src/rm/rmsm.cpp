/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "../rmstl.h"

#include "rmsm.h"
#include "storage.h"
#include "queue.h"
#include <cache.h>
#include "smcache.h"
#include "varcol.h"
#include <time/timecore.h>
#include <mm/mutex.h>
#include <util/dbgout.h>
#include <util/utilcore.h>

typedef MAP(unsigned int, unsigned int, less<unsigned int> ) id2idx_typ;
typedef VECTOR(unsigned int) idx2id_typ;

struct RMSMemSocket_Private
{
    id2idx_typ id2idx;
    idx2id_typ idx2id;
    osmutex sem;
    rmglobal_typ* glob;
    bool own_data;
};

RMSMemSocket::RMSMemSocket() :
    vc((var_collection*)NULL),
    qf((queue_factory*)NULL),
    clientidx(MAX_CLIENT),
    prvt(new RMSMemSocket_Private)
{
    prvt->idx2id.push_back(0xFFFFFFFF); // for client ID
    prvt->glob = 0;
    prvt->own_data = false;
}

RMSMemSocket::~RMSMemSocket()
{
    Close();

    delete qf;
    delete vc;
    vc = (var_collection*)NULL;
#ifdef WIN32
    if (prvt->glob)
    {
        prvt->glob->detach();
    }
#endif // WIN32
    delete prvt->glob;
    delete prvt;
}

bool RMSMemSocket::Connect(const char* host, int port, const struct timeval*)
{
    if(!Connected())
    {
        rmglobal_typ& glob = GetGlobalStorage(host);

        clientidx = FindFreeClientIDX(host);
        if(clientidx >= MAX_CLIENT)
        {
            return false;
        }

        vc = new var_collection(glob, VAR_SIZE);
        if (vc == NULL)
        {
            prvt->sem.signal();
            return false;
        }
        vc->clear(clientidx);

        /* Port is either 0 or specified in KB */
        port = (port < 4096) ? CACHE_SIZE : port*1024;
        if (!smcache_typ::init(glob, port))
        {
            delete vc;
            vc = (var_collection*)NULL;
            prvt->sem.signal();
            return false;
        }

        queue_factory* tqf = new queue_factory(glob, QUEUE_SIZE);
        if (tqf == NULL)
        {
            delete vc;
            vc = (var_collection*)NULL;
            prvt->sem.signal();
            qf = NULL;
            return false;
        }
        tqf->clear(clientidx);
        qf = tqf;
    }
    return true;
}

bool RMSMemSocket::Connected()
{
    return qf != NULL;
}

bool RMSMemSocket::Close()
{
    if (!Connected())
    {
        return false;
    }
    UnclientID();
    if (vc)
    {
        vc->clear(clientidx);
    }
    if (qf)
    {
        qf->clear(clientidx);
    }
    prvt->sem.signal();
    return true;
}

int RMSMemSocket::Select(const struct timeval* stm, bool defercb)
{
    int sel = 0;
    cache_typ* data;
    id2idx_typ::const_iterator iter;

    if(!Connected())
    {
        return -1;
    }
    if(defercb)
    {
        return 0;
    }

    if (prvt->own_data || qf->select(clientidx, stm))
    {
        prvt->own_data = false;
        while ((data = qf->top(clientidx)) != NULL)
        {
            iter = prvt->id2idx.find(data->get_id());
            if (iter != prvt->id2idx.end())
            {
                (*(callback[data->get_cmd()]))(callback_arg[data->get_cmd()], (*iter).second, data);
            }
            qf->pop(clientidx);
            sel = 1;
        }
    }
    return sel;
}

const cache_typ* RMSMemSocket::GetListStart(unsigned int id)
{
    if (!Connected() || (id >= prvt->idx2id.size()))
    {
        return (cache_typ*)NULL;
    }

    cache_typ* ptr = vc->getlist(prvt->idx2id[id]);
    if (ptr != NULL)
    {
        if (ptr->check_header())
        {
            smcache_typ::sm_reserve(ptr);
            prvt->own_data = true;
        }
        else
        {
            dout << "Warning: Directory contains entry with invalid MAGIC number\n";
            return NULL;
        }
    }
    return ptr;
}

void RMSMemSocket::GetListEnd(unsigned int, const cache_typ* ptr)
{
    smcache_typ::sm_unreserve(const_cast<cache_typ*>(ptr));
}

cache_typ* RMSMemSocket::MessageStart(unsigned int id, size_t len)
{
    return SetListStart(id, len);
}

cache_typ* RMSMemSocket::SetListStart(unsigned int, size_t len)
{
    return smcache_typ::sm_allocate(len);
}

bool RMSMemSocket::SetListEnd(unsigned int id, cache_typ* pbuf)
{
    if (!Connected() || (pbuf == NULL))
    {
        return false;
    }
    if (id >= prvt->idx2id.size())
    {
        smcache_typ::sm_unreserve(pbuf);
        return false;
    }

    const unsigned int id1 = prvt->idx2id[id];

    pbuf->set_id(id1);
    notify_typ notify = vc->setlist(id1, pbuf);
    notify.set(clientidx, false);

    if (notify.not_empty())
    {
        for (unsigned int cnt = 0; cnt < MAX_CLIENT; ++cnt)
        {
            if (notify[cnt])
            {
                qf->push(pbuf, cnt);
            }
        }
    }
    smcache_typ::sm_unreserve(pbuf);
    return true;
}

bool RMSMemSocket::BroadcastEnd(unsigned int id, cache_typ* pbuf)
{
    if (!Connected() || (pbuf == NULL))
    {
        return false;
    }
    if (id >= prvt->idx2id.size())
    {
        smcache_typ::sm_unreserve(pbuf);
        return false;
    }

    const unsigned int id1 = prvt->idx2id[id];

    pbuf->set_id(id1);
    pbuf->set_cmd(BROADCAST);
    notify_typ notify = vc->getnotify(id1);
    notify.set(clientidx, false);

    if (notify.not_empty())
    {
        for (unsigned int cnt = 0; cnt < MAX_CLIENT; ++cnt)
        {
            if (notify[cnt])
            {
                qf->push(pbuf, cnt);
            }
        }
    }
    smcache_typ::sm_unreserve(pbuf);

    return true;
}

bool RMSMemSocket::RegisterIDX(unsigned int id)
{
    if (!Connected())
    {
        return false;
    }
    if (id >= prvt->idx2id.size())
    {
        return false;
    }

    const unsigned int id1 = prvt->idx2id[id];
    if (vc->registeridx(id1, clientidx, true))
    {
        cache_typ* ptr = vc->getlist(id1);
        if (ptr != NULL)
        {
            if (ptr->check_header())
            {
                qf->push(ptr, clientidx);
                prvt->own_data = true;
            }
            else
            {
                dout << "Warning: Directory contains entry with invalid MAGIC number\n";
            }
        }
        return true;
    }
    return false;
}

bool RMSMemSocket::UnregisterIDX(unsigned int id)
{
    if (!Connected())
    {
        return false;
    }
    if (id >= prvt->idx2id.size())
    {
        return false;
    }

    const unsigned int id1 = prvt->idx2id[id];
    return vc->registeridx(id1, clientidx, false);
}

bool RMSMemSocket::RegisterID(const char* str)
{
    if (!Connected())
    {
        return false;
    }
    unsigned int id = vc->registerid(str, clientidx);
    prvt->id2idx[id] = static_cast<unsigned int>(prvt->idx2id.size());
    prvt->idx2id.push_back(id);
    cache_typ* ptr = vc->getlist(id);
    if (ptr != NULL)
    {
        if (ptr->check_header())
        {
            qf->push(ptr, clientidx);
            prvt->own_data = true;
        }
        else
        {
            dout << "Warning: Directory contains entry with invalid MAGIC number\n";
        }
    }
    return false;
}

bool RMSMemSocket::QueryID(const char* str)
{
    if (!Connected())
    {
        return false;
    }
    unsigned int id = vc->queryid(str);
    prvt->id2idx[id] = static_cast<unsigned int>(prvt->idx2id.size());
    prvt->idx2id.push_back(id);
    return false;
}

bool RMSMemSocket::QueryID(const char* str[], int num)
{
    if (!Connected())
    {
        return false;
    }
    for (int cnt = 0; cnt < num; ++cnt)
    {
        QueryID(str[cnt]);
    }
    return false;
}

bool RMSMemSocket::ClientID(const char* str)
{
    // Full access for shared memory
    // No need to check
    if (!Connected())
    {
        return false;
    }

#ifdef _WIN32
    // needed when connect is run in a separate thread, causing mutex to unlock when thread exits
    prvt->sem.wait();
#endif

    //close old connection if any
    UnclientID();

    cache_typ* ptr;
    unsigned int id = vc->queryid(str);
    prvt->id2idx[id] = 0;
    prvt->idx2id[0] = id;

    if ((ptr = const_cast<cache_typ*>(GetListStart(0))) != NULL)
    {
        smcache_typ::lock();
        (ptr->buf())[0] = BE(BE((ptr->buf())[0]) + 1);
        smcache_typ::unlock();
        SetListEnd(0, ptr);
    }
    else
    {
        ptr = SetListStart(0, 1);
        if (ptr)
        {
            (ptr->buf())[0] = BE((unsigned int)1);
            SetListEnd(0, ptr);
        }
        else
        {
            return false;
        }
    }

    return false;
}

bool RMSMemSocket::FlowControl(unsigned int)
{
    // No flow control for shared memory
    return true;
}

bool RMSMemSocket::Broadcast(unsigned int id, size_t len, const unsigned int* buf)
{
    if (!Connected())
    {
        return false;
    }
    if (id >= prvt->idx2id.size())
    {
        return false;
    }

    const int id1 = prvt->idx2id[id];
    notify_typ notify = vc->getnotify(id1);
    notify.set(clientidx, false);

    if (notify.not_empty())
    {
        cache_typ* c = smcache_typ::sm_duplicate(id1, len, buf);
        if (c)
        {
            c->set_cmd(BROADCAST);
            for (unsigned int cnt = 0; cnt < MAX_CLIENT; ++cnt)
            {
                if (notify[cnt])
                {
                    qf->push(c, cnt);
                }
            }
            smcache_typ::sm_unreserve(c);
        }
    }
    return true;
}

bool RMSMemSocket::SetList(unsigned int id, size_t len, const unsigned int* buf)
{
    if (!Connected())
    {
        return false;
    }
    if (id >= prvt->idx2id.size())
    {
        return false;
    }

    const int id1 = prvt->idx2id[id];
    cache_typ* c = smcache_typ::sm_duplicate(id1, len, buf);
    if (c)
    {
        notify_typ notify = vc->setlist(id1, c);
        notify.set(clientidx, false);

        if (notify.not_empty())
        {
            for (unsigned int cnt = 0; cnt < MAX_CLIENT; ++cnt)
            {
                if (notify[cnt])
                {
                    qf->push(c, cnt);
                }
            }
        }
        smcache_typ::sm_unreserve(c);
    }
    return true;
}

bool RMSMemSocket::Message(unsigned int id, size_t len, const unsigned int* buf)
{
    return SetList(id, len, buf);
}

size_t RMSMemSocket::GetList(unsigned int id, size_t len, unsigned int* buf)
{
    const cache_typ* ptr = GetListStart(id);
    if (ptr)
    {
        size_t sz = ptr->output(len, buf);
        GetListEnd(id, ptr);
        return sz;
    }
    return 0;
}

rmglobal_typ& RMSMemSocket::GetGlobalStorage(const char* name)
{
    if (prvt->glob == 0)
    {
        prvt->glob = new rmglobal_typ(name);
    }
    return *(prvt->glob);
}

bool RMSMemSocket::WaitForHandshakeCompletion(const struct timeval*)
{
    return true;
}

bool RMSMemSocket::UnclientID()
{
    if (vc)
    {
        vc->clear(clientidx);
    }
    if (qf)
    {
        qf->clear(clientidx);
    }

    cache_typ* ptr;
    if ((ptr = const_cast<cache_typ*>(GetListStart(0))) != NULL)
    {
        smcache_typ::lock();
        if ((ptr->buf())[0])
        {
            (ptr->buf())[0] = BE(BE((ptr->buf())[0]) - 1);
            smcache_typ::unlock();
            SetListEnd(0, ptr); //write the data back
            prvt->id2idx.clear();
            prvt->idx2id.clear();
            prvt->idx2id.push_back(0xFFFFFFFF); // for client ID
            return true;
        }
        smcache_typ::unlock();
    }
    return false;
}

unsigned RMSMemSocket::FindFreeClientIDX(const char* host)
{
    char name[40];
    unsigned int cnt;
    for(cnt = 0; cnt < MAX_CLIENT; ++cnt)
    {
        minisprintf(name, "%s-%u", host, cnt);
        prvt->sem.create(name, true);
        if(prvt->sem.trywait())
        {
            dout << "Found Free client IDX " << cnt << endl << flush;
            return cnt;
        }
    }
    prvt->sem.destroy();
    return cnt;
}

void RMSMemSocket::KeepAlive()
{
}

void RMSMemSocket::dump()
{
    if(Connected())
    {
        rmglobal_typ& glob = GetGlobalStorage("");
        if (glob.ptr == NULL)
        {
            return;
        }
        dout << "Shared Memory Dump for " << static_cast<const char*>(glob.global_name) << "\n\n";
        dout << "Name Directory\n";
        dout << "==============\n";
        for (namedir_typ::iterator i1 = glob.ptr->store.namedir.begin(); i1 != glob.ptr->store.namedir.end(); ++i1)
        {
            dout << "  " << static_cast<const char*>((*i1).first) << '\t' << (*i1).second << '\n';
        }
        dout << "\nList Directory\n";
        dout << "==============\n";
        int cnt = 0;
        dout << "     01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31\n";
        dout << "     --------------------------------------------------------------------------------------------\n";
        directory_typ::iterator i2;
        for (i2 = glob.ptr->store.directory.begin(); i2 != glob.ptr->store.directory.end(); ++i2, ++cnt)
        {
            dout << "  " << cnt << ">> ";
            for (int icnt = 0; icnt < 32; ++icnt)
            {
                dout<< (*i2).notify[icnt] << "  ";
            }
            dout << "\n";
        }
        dout << "\nList Data\n";
        dout << "=========\n";
        cnt = 0;
        for (i2 = glob.ptr->store.directory.begin(); i2 != glob.ptr->store.directory.end(); ++i2, ++cnt)
        {
            if ((*i2).ptr != NULL)
            {
                dout << "  [" << (*i2).ptr->get_refcount() << ']' << cnt << '(' << (*i2).ptr->get_len() << ") " << ">> ";
                for (unsigned int icnt = 0; icnt < (*i2).ptr->get_len(); ++icnt)
                {
                    dout<< *((*i2).ptr->buf() + icnt) << "  ";
                }
                dout << "\n";
            }
        }
        dout << "\nClient Queue Data\n";
        dout << "=================\n";
        for (cnt=0; cnt<(int)MAX_CLIENT; ++cnt)
        {
            dout << "Client " << cnt << '\n';
            qf->dump(cnt);
        }
        dout << "\nCache heap\n";
        dout << "==========\n";
        mm_display_info(glob.cache);
        dout << "\nVar heap\n";
        dout << "==========\n";
        mm_display_info(glob.var);

        file_buffer cache_fb("cache.dump");
        long dummy_off = 0;
        cache_fb.writefile((const char*)glob.cache, mm_core_size(glob.cache), dummy_off, true);
    }
}

bool RMSMemSocket::MessageEnd(unsigned int id, cache_typ* pbuf, void*)
{
    return SetListEnd(id, pbuf);
}

bool RMSMemSocket::Flush()
{
    return true;
}

bool RMSMemSocket::CheckLink()
{
    return true;
}

bool RMSMemSocket::HandshakeAck(unsigned int, unsigned int)
{
    // SM does not wait for handshake completion
    return true;
}

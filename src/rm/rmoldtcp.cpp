/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "../rmstl.h"
#include "rmoldtcp.h"
#include <limits.h>
#include <string.h>
#include <comm/tcpcore.h>
#include <time/timecore.h>

#ifdef NET_OS
#include <threadx/tx_api.h>
#include <threadx/tx_tim.h>
#include <bsp_api.h>
#endif

typedef MAP(int, int, less<int>) rm3_id2idx_typ;
typedef VECTOR(int) rm3_idx2id_typ;

struct queue_typ;
class RMRoamClient;

struct RMTCPSocket_private
{
    RMTCPSocket_private()
    {
    }
    RMTCPSocket_private(const tcp_core&);
    tcp_core sock;
    unsigned int getid;
    unsigned int queryid;
    unsigned int rcvqueryid;
    unsigned int errflag;
    unsigned int valid;
    unsigned int in_seq;
    unsigned int out_seq;
    unsigned int intv;
    cache_typ* query_buf;
    cache_typ* out_buf;
    cache_typ* small_buf;
    unsigned int* in_buf;
    queue_typ* queue_start;
    queue_typ* queue_end;
    timeElapsed recvtime;
    timeElapsed sendtime;
    unsigned int raw_inbuf[READ_BUF+HEADER_OFF+1];
    bool readflag;
    bool clientok;
    RMRoamClient* roam;
};

struct RM3TCPSocket_Private
{
    rm3_idx2id_typ idx2id;
    rm3_id2idx_typ id2idx;
};

#define RM3_NAME_LEN 20
//copy and null terminate up to RM3_NAME_LEN
static void rmnamecpy(char* dst, const char* src);

RM3TCPSocket::RM3TCPSocket() :
    prvt2(new RM3TCPSocket_Private)
{
    prvt->rcvqueryid = 0;
}

RM3TCPSocket::RM3TCPSocket(const tcp_core& asock) :
    RMTCPSocket(asock), prvt2(new RM3TCPSocket_Private)
{
}

RM3TCPSocket::~RM3TCPSocket()
{
    delete prvt2;
}

int RM3TCPSocket::Select(const struct timeval* stm, bool defercb)
{
    const cache_typ* p;

    KeepAlive();
    if (!defercb)
    {
        while ((p = Top()) != NULL)
        {

            (*(callback[SETLIST]))(callback_arg[SETLIST], p->get_id(), p);
            Pop();
            const_cast<cache_typ*>(p)->h_unreserve();
        }
    }
    int ret = (stm == NULL) ? 1 : prvt->sock.select(stm);
    if (ret > 0)
    {
        ret = 1;
        // do some unpacking of packets
        ReceivePacket(defercb);
        if (prvt->errflag > ERR_TOLERANCE)
        {
            return -1;
        }
    }
    else if (ret < 0)
    {
        return -1;
    }
    else
    {
        if (prvt->intv && (prvt->recvtime.elapsed() > prvt->intv * 2000U))
        {
            return -1;
        }
    }
    KeepAlive();

    return ret;
}

const cache_typ* RM3TCPSocket::GetListStart(unsigned int id)
{
    id = IDX2ID(id);
    if (id > 0)
    {
        int cnt = 0, ret;
        SendMessageT(id & 0xFFFFFF, 0, NULL, RM3_GET_LIST);
        prvt->getid = id;
        do
        {
            ret = prvt->sock.select(NULL);
            if (ret > 0)
            {
                ReceivePacket(true);
            }
            else if (ret < 0)
            {
                break;
            }
            Sleep(WAIT_TIME);
            ++cnt;
        }
        while ((prvt->query_buf == NULL) && (cnt < GETLIST_RETRY));
        cache_typ* ptr = prvt->query_buf;
        prvt->getid = UINT_MAX;
        prvt->query_buf = NULL;
        return ptr;
    }
    return NULL;
}

bool RM3TCPSocket::SetListEnd(unsigned int id, cache_typ* pbuf)
{
    id = IDX2ID(id);
    if (id > 0)
    {
        bool ret;
        pbuf->set_id(id);
        pbuf->set_cmd(RM3_SET_LIST);
        ret = SendMessageC(pbuf);
        pbuf->h_unreserve();
        return ret;
    }
    return false;
}

bool RM3TCPSocket::BroadcastEnd(unsigned int id, cache_typ* pbuf)
{
    id = IDX2ID(id);
    if (id > 0)
    {
        bool ret;
        pbuf->set_id(id);
        pbuf->set_cmd(RM3_BROADCAST_MESSAGE);
        ret = SendMessageC(pbuf);
        pbuf->h_unreserve();
        return ret;
    }
    return false;
}

bool RM3TCPSocket::RegisterIDX(unsigned int id)
{
    id = IDX2ID(id);
    if (id > 0)
    {
        prvt->small_buf->set_len(0);
        prvt->small_buf->set_id(id);
        prvt->small_buf->set_cmd(RM3_REGISTER_ID);
        return SendMessageC(prvt->small_buf);
    }
    return false;
}

bool RM3TCPSocket::UnregisterIDX(unsigned int id)
{
    id = IDX2ID(id);
    if (id > 0)
    {
        prvt->small_buf->set_len(0);
        prvt->small_buf->set_id(id);
        prvt->small_buf->set_cmd(RM3_UNREGISTER_ID);
        return SendMessageC(prvt->small_buf);
    }
    return false;
}

bool RM3TCPSocket::RegisterID(const char* str)
{
    QueryID(str);
    prvt2->idx2id.back() = -2;
    return true;
}

bool RM3TCPSocket::QueryID(const char* str)
{
    prvt->small_buf->set_len(RM3_NAME_LEN / sizeof(unsigned int));
    prvt->small_buf->set_cmd(RM3_QUERY_ID);
    rmnamecpy((char*)prvt->small_buf->buf(), str);
    if (SendMessageC(prvt->small_buf))
    {
        prvt2->idx2id.push_back(-4);
        return true;
    }
    return false;
}

bool RM3TCPSocket::QueryID(const char* str[], int num)
{
    return RMTCPSocket::QueryID(str, num);
}

bool RM3TCPSocket::ClientID(const char* client)
{
    prvt->clientok = false;
    prvt2->idx2id.clear();
    prvt2->id2idx.clear();
    prvt->rcvqueryid = 0;
    QueryID(client);
    prvt2->idx2id.back() = -3;
    return true;
}

bool RM3TCPSocket::FlowControl(unsigned int interval)
{
    prvt->intv = interval;
    prvt->small_buf->set_len(0);
    prvt->small_buf->set_id(interval == 0 ? 0 : 1);
    prvt->small_buf->set_cmd(RM3_FLOW_CONTROL);
    return SendMessageC(prvt->small_buf);
}

bool RM3TCPSocket::Broadcast(unsigned int id, size_t len, const unsigned int* buf)
{
    id = IDX2ID(id);
    if (id > 0)
    {
        return SendMessageT(id, len, buf, RM3_BROADCAST_MESSAGE);
    }
    return false;
}

bool RM3TCPSocket::SetList(unsigned int id, size_t len, const unsigned int* buf)
{
    id = IDX2ID(id);
    if (id > 0)
    {
        return SendMessageT(id, len, buf, RM3_SET_LIST);
    }
    return false;
}

void RM3TCPSocket::ReceivePacket(bool defercb)
{
    unsigned int length = 0;       // Length of good packet
    unsigned int begin  = 0;       // Location of unprocessed
    unsigned int more   = 1;       // Indicator of pending packets
    unsigned int id     = 0;       // ID of good packet
    unsigned int cnt1;             // Misc counters
    int result;                    // Read result

    // state = 0 read in up to (READ_BUF * 4) + 1 bytes (more, begin, valid flag)
    //       = 1 find next packet (begin, length, id, cmd)
    //		 = 2 move incomplete packet to start location (begin)

    if (prvt->readflag)
    {
        return;
    }
    prvt->readflag = true;

    while (more && (prvt->valid < (READ_BUF * sizeof(unsigned int) + 1)))
    {
        // state 0
        result = prvt->sock.recv(((char*)prvt->in_buf) + prvt->valid, (READ_BUF * sizeof(unsigned int) + 1) - prvt->valid);
        if (result > 0)
        {
            prvt->valid += result;
            if (prvt->valid != (READ_BUF * sizeof(unsigned int) + 1))
            {
                more = 0;
            }
        }
        else
        {
            ++(prvt->errflag);
            more = 0;
            break;
        }
        // state 1
        while (prvt->valid >= (HEADER_SIZE * sizeof(unsigned int)))
        {
            if (prvt->in_buf[begin] == BE(MAGICNUM))
            {
                // Magic Number OK
                cnt1   = BE(prvt->in_buf[begin+2]) >> 16;
                if (cnt1 == prvt->in_seq)
                {
                    // Frame Count OK
                    length = BE(prvt->in_buf[begin+2]) & 0xFFFF;
                    if (prvt->valid >= (length+HEADER_SIZE) * sizeof(unsigned int))
                    {
                        // Length Recieved OK
                        const int cmd = BE(prvt->in_buf[begin + 1]) >> 24; // Command of good packet
                        id  = BE(prvt->in_buf[begin + 1]) & 0xFFFFFF;
                        prvt->recvtime();
                        prvt->in_seq = ++(prvt->in_seq) & 0xFFFF;
                        if (cmd < RM3_MAX_FUNC)
                        {
                            if (cmd == RM3_QUERY_ID)
                            {
                                if (prvt->rcvqueryid < prvt2->idx2id.size())
                                {
                                    if (id < 0xFFFFFF)
                                    {
                                        prvt2->id2idx[id] = prvt->rcvqueryid;
                                        if (prvt2->idx2id[prvt->rcvqueryid] == -2)
                                        {
                                            prvt->small_buf->set_len(0);
                                            prvt->small_buf->set_id(id);
                                            prvt->small_buf->set_cmd(RM3_REGISTER_ID);
                                            SendMessageC(prvt->small_buf);
                                        }
                                        else if (prvt2->idx2id[prvt->rcvqueryid] == -3)
                                        {
                                            prvt->small_buf->set_len(0);
                                            prvt->small_buf->set_id(id);
                                            prvt->small_buf->set_cmd(RM3_CLIENT_ID);
                                            SendMessageC(prvt->small_buf);
                                        }
                                        prvt2->idx2id[prvt->rcvqueryid] = id;
                                    }
                                    ++(prvt->rcvqueryid);
                                }
                            }
                            else if (cmd == RM3_CLIENT_ID)
                            {
                                if (id < 0xFFFFFF)
                                {
                                    prvt->clientok = true;
                                }
                            }

                            if ((cmd == RM3_SET_LIST) && (id == prvt->getid))
                            {
                                id = ID2IDX(id);
                                if (id > 0)
                                {
                                    prvt->query_buf = cache_typ::h_duplicate(id, length, &(prvt->in_buf[begin+HEADER_SIZE]));
                                }
                            }
                            else if (defercb && (cmd == RM3_SET_LIST))
                            {
                                id = ID2IDX(id);
                                if (id > 0)
                                {
                                    cache_typ* p = cache_typ::h_duplicate(id, length, &(prvt->in_buf[begin+HEADER_SIZE]));
                                    p->set_cmd(SETLIST);
                                    Push(p);
                                }
                            }
                            else if (cmd == RM3_SET_LIST)
                            {
                                cache_typ* p = cache_typ::ConvertToCache(&prvt->in_buf[begin]);
                                id = ID2IDX(id);
                                p->set_cmd(SETLIST);
                                if (id > 0)
                                {
                                    (*(callback[SETLIST]))(callback_arg[SETLIST], id, p);
                                }
                            }
                            else if (cmd == RM3_PING)
                            {
                                prvt->small_buf->set_len(0);
                                prvt->small_buf->set_id(0);
                                prvt->small_buf->set_cmd(RM3_PING);
                                SendMessageC(prvt->small_buf);
                            }
                        }
                        begin += length + HEADER_SIZE;
                        prvt->valid -= (length + HEADER_SIZE) * sizeof(unsigned int);
                        prvt->errflag = 0;
                        continue;
                    }
                    else
                    {
                        // Check if exceed buffer size
                        if ((length+HEADER_SIZE > READ_BUF) &&
                            (more != 0))
                        {
                            cache_typ* p = cache_typ::h_allocate(length);
                            p->copy_header(&(prvt->in_buf[begin]));
                            memcpy(p->buf(), &(prvt->in_buf[begin+HEADER_SIZE]), prvt->valid);
                            begin = 0;
                            more = 0;
                            prvt->sock.recv(((char*)(p->buf())) + prvt->valid, length*sizeof(unsigned int) - prvt->valid);
                            prvt->recvtime();
                            if (p->get_cmd() == RM3_SET_LIST)
                            {
                                int _idx = ID2IDX(p->get_id());
                                if (_idx > 0)
                                {
                                    p->set_id(_idx);
                                    p->set_cmd(SETLIST);
                                    Push(p);
                                }
                                else
                                {
                                    p->h_unreserve();
                                }
                            }
                            else
                            {
                                p->h_unreserve();
                            }
                        }
                        // Not full packet recieved; Defer processing
                        else if (begin)
                        {
                            memmove(prvt->in_buf, &(prvt->in_buf[begin]), prvt->valid);
                            begin = 0;
                        }
                        break;
                    }
                }
            }
            ++(prvt->errflag);
            if (prvt->errflag > ERR_TOLERANCE)
            {
                prvt->sock.close();
                more = 0;
                break;
            }
            begin++;
            prvt->valid -= 4;
        }
        // state 2
        if (begin && prvt->valid)
        {
            memmove(prvt->in_buf, &(prvt->in_buf[begin]), prvt->valid);
            begin = 0;
        }
    }
    prvt->readflag = false;
}

bool RM3TCPSocket::WaitForHandshakeCompletion(const struct timeval* stm)
{
    const timeExpire expire(stm);
    do
    {
        if (prvt->clientok && (prvt->rcvqueryid == prvt2->idx2id.size()))
        {
            return true;
        }
    }
    while ((Select(stm) != -1) && !expire() && prvt->sock.connected());
    return false;
}

void RM3TCPSocket::KeepAlive()
{
    if (prvt->intv && (prvt->sendtime.elapsed() >= prvt->intv * 1000U))
    {
        SendMessageT(0xFFFFFF, 0, NULL, RM3_PING);
    }
}

int RM3TCPSocket::IDX2ID(int _idx)
{
    int result;
    if (_idx < (int)prvt2->idx2id.size())
    {
        result = prvt2->idx2id[_idx];
        if (result < 0)
        {
            struct timeval tm = {
                1, 0
            };
            WaitForHandshakeCompletion(&tm);
            return prvt2->idx2id[_idx];
        }
        else
        {
            return result;
        }
    }
    return -1;
}

int RM3TCPSocket::ID2IDX(int _id)
{
    rm3_id2idx_typ::iterator iter = prvt2->id2idx.find(_id);
    return (iter != prvt2->id2idx.end()) ? (*iter).second : -1;
}

static void rmnamecpy(char* dst, const char* src)
{
    rm_strncpy(dst, RM3_NAME_LEN, src, RM3_NAME_LEN-1);
    dst[RM3_NAME_LEN-1] = '\0';
}

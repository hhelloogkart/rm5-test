/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _RMNAME_H_
#define _RMNAME_H_

#include <rmglobal.h>

struct RM_EXPORT rmname_typ
{
    explicit rmname_typ();
    rmname_typ(const rmname_typ& other);
    explicit rmname_typ(const char* other);
    const rmname_typ& operator=(const rmname_typ& other);
    const rmname_typ& operator=(const char* other);
    unsigned int size() const;
    void operator+=(const char* other);
    bool operator==(const char* other) const;
    bool operator<(const rmname_typ& other) const;
    operator const char* () const
    {
        return name;
    }
    char name[RMNAME_LEN];
};

#endif /* _RMNAME_H_ */

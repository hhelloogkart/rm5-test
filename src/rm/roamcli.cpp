/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "roamcli.h"
#include <rmglobal.h>
#include <time/timecore.h>
#include <stdlib.h>
#include <string.h>
#include <comm/udpcore.h>
#include <comm/tcpcore.h>

#if defined(_WIN32)
#include <winsock2.h>
#elif defined(NET_OS)
extern "C"
{
#include <sockapi.h>
#include <tcpip/in.h>
}
#else
#include <netinet/in.h>
#include <sys/socket.h>
#endif

struct RMRoamClient_private
{
    udp_core sock;
    short roam_port, local_port;
    char* host;
    struct in_addr hostaddr;
};

RMRoamClient::RMRoamClient(short _roam_port, const char* _host) :
    prvt(new RMRoamClient_private)
{
    prvt->roam_port = _roam_port;
    if(_host)
    {
        size_t len = strlen(_host) + 1;
        prvt->host = new char[len];
        if(prvt->host)
        {
            rm_strcpy(prvt->host, len, _host);
        }
    }
    else
    {
        prvt->host = NULL;
    }
}

RMRoamClient::~RMRoamClient()
{
    Close();
    delete[] prvt->host;
    delete prvt;
}

void RMRoamClient::Close()
{
    prvt->sock.close();
}

void RMRoamClient::Reset()
{
    prvt->hostaddr.s_addr = 0;
    do
    {
        prvt->local_port = rand_port();
    }
    while(!prvt->sock.bind("0.0.0.0", prvt->local_port));
    prvt->local_port = htons(prvt->local_port);
}

bool RMRoamClient::Select(const struct timeval* stm)
{
    struct timeval def = {
        10, 0
    };
    int ret;
    if(stm)
    {
        def = *stm;
    }
    if(!prvt->sock.isvalid())
    {
        Reset();
    }
    if(!(prvt->host))
    {
        ret = prvt->sock.send((char*)&(prvt->local_port), sizeof(prvt->local_port), NULL, prvt->roam_port);
        if(ret > 0 && prvt->sock.select(&def) > 0)
        {
            return OnDataAvail();
        }
        else if(ret < 0)
        {
            Reset();
        }
    }
    else
    {
        char* h = prvt->host, * beg, old;
        do
        {
            while(*h == ' ')
            {
                ++h;
            }
            beg = h;
            while(*h && *h != ' ')
            {
                ++h;
            }
            if(h == beg)
            {
                break;
            }

            old = *h;
            *h = '\0';
            ret = prvt->sock.send((char*)&(prvt->local_port), sizeof(prvt->local_port), beg, prvt->roam_port);
            *h = old;
            if(ret > 0 && prvt->sock.select(&def) > 0)
            {
                if(OnDataAvail())
                {
                    return true;
                }
            }
            else if(ret < 0)
            {
                Reset();
            }
        }
        while(*h);
    }
    return false;
}

bool RMRoamClient::OnDataAvail()
{
    short recvport;
    struct sockaddr_in clientaddr;

    if(prvt->sock.recv((char*)&recvport, sizeof(short), &clientaddr)
       == sizeof(short))
    {
        recvport = ntohs(recvport);
        if(recvport == prvt->roam_port)
        {
            prvt->hostaddr = clientaddr.sin_addr;
            return true;
        }
    }
    return false;
}

short RMRoamClient::Get_Port() const
{
    return prvt->roam_port;
}

void RMRoamClient::Set_Port(short port)
{
    prvt->roam_port = port;
    Reset();
}

void RMRoamClient::ExportAddr(tcp_core* tc)
{
    struct sockaddr_in addr;
    memset((char*)&addr, 0, sizeof(struct sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_port   = htons(prvt->roam_port);
    addr.sin_addr   = prvt->hostaddr;
    tc->sethost(addr);
}

void RMRoamClient::ExportAddr(struct sockaddr_in* addr)
{
    memset((char*)addr, 0, sizeof(struct sockaddr_in));
    addr->sin_family = AF_INET;
    addr->sin_port   = htons(prvt->roam_port);
    addr->sin_addr   = prvt->hostaddr;
}

short RMRoamClient::rand_port()
{
    struct timeval stm;
    now(&stm);
    unsigned short ran = static_cast<unsigned short>(stm.tv_usec / 1000 + stm.tv_sec * 1000);
    ran = (ran % 16384) + 1024;
    return ran;
}

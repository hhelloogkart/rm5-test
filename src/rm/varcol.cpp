/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "varcol.h"
#include "storage.h"
#include <cache.h>
#include "smcache.h"

const var_item_typ& var_collection::dummy()
{
    static var_item_typ obj;
    return obj;
}

var_collection::var_collection(rmglobal_typ& glob, unsigned int sz) :
    global_sm(glob), store(glob.ptr->store)
{
    glob.attach_var(sz);
}

unsigned int var_collection::queryid(const char* name)
{
    rmname_typ qname(name);
    unsigned int id;

    global_sm.lock();
    namedir_typ::iterator iter = store.namedir.find(qname);
    if (iter == store.namedir.end())
    {
        id = static_cast<unsigned int>(store.directory.size());
        store.namedir[qname] = id;
        store.directory.push_back(dummy());
    }
    else
    {
        id = (*iter).second;
    }
    global_sm.unlock();
    return id;
}

unsigned int var_collection::registerid(const char* name, unsigned int client)
{
    unsigned int id = queryid(name);
    global_sm.lock();
    store.directory[id].notify.set(client, true);
    global_sm.unlock();
    return id;
}

bool var_collection::registeridx(unsigned int id, unsigned int client, bool reg)
{
    bool ok;
    global_sm.rdlock();
    ok = id >= store.directory.size();
    global_sm.unlock();
    if (ok)
    {
        return false;
    }
    global_sm.lock();
    store.directory[id].notify.set(client, reg);
    global_sm.unlock();
    return true;
}
/*
   void var_collection::linkback(const char *name, unsigned int id, unsigned int client)
   {
   rmname_typ qname(name);
   global_sm.lock();
   namedir_typ::iterator iter = store.namedir.find(qname);
   if (iter != store.namedir.end())
   {
     store.directory[id].ptr = reinterpret_cast<cache_typ *>(&((*iter).second));
     store.directory[id].notify.set(client, true);
   }
   global_sm.unlock();
   }
 */
notify_typ var_collection::setlist(unsigned int id, cache_typ* buf)
{
    global_sm.rdlock();
    if (id < store.directory.size())
    {
        notify_typ noty(store.directory[id].notify);
        global_sm.unlock();
        if (buf->get_len() > 0)
        {
            smcache_typ::sm_reserve(buf);
        }
        else
        {
            buf = (cache_typ*)NULL;
        }
        global_sm.lock();
        cache_typ* oldbuf = store.directory[id].ptr;
        store.directory[id].ptr = buf;
        global_sm.unlock();
        if (oldbuf)
        {
            smcache_typ::sm_unreserve(oldbuf);
        }
        return noty;
    }
    else
    {
        global_sm.unlock();
    }
    return dummy().notify;
}

cache_typ* var_collection::getlist(unsigned int id)
{
    cache_typ* ptr;
    global_sm.rdlock();
    if (id < store.directory.size())
    {
        ptr = store.directory[id].ptr;
    }
    else
    {
        ptr = NULL;
    }
    global_sm.unlock();
    return ptr;
}

void var_collection::setonceonly(unsigned int id, unsigned int client)
{
    bool ok;
    global_sm.rdlock();
    ok = id < store.directory.size();
    global_sm.unlock();
    if (ok)
    {
        global_sm.lock();
        store.directory[id].notify.setonce(client);
        global_sm.unlock();
    }
}

notify_typ var_collection::getnotify(unsigned int id)
{
    notify_typ notify;
    global_sm.rdlock();
    if (id < store.directory.size())
    {
        notify = store.directory[id].notify;
    }
    else
    {
        notify = dummy().notify;
    }
    global_sm.unlock();
    return notify;
}

void var_collection::clear(unsigned int client)
{
    directory_typ::iterator iter;
    global_sm.lock();
    for (iter = store.directory.begin(); iter != store.directory.end(); ++iter)
    {
        (*iter).notify.set(client, false);
    }
    global_sm.unlock();
}

bool var_collection::lastid(unsigned int id)
{
    unsigned int dsz;
    global_sm.rdlock();
    dsz = static_cast<unsigned int>(store.directory.size());
    global_sm.unlock();
    return id == (dsz - 1);
}

/******************************************************************/
/* Copyright DSO National Laboratories 2020. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "smcache.h"
#include <cache.h>
#include <util/dbgout.h>
#include <assert.h>

#ifndef NO_SM
#include "storage.h"
#elif defined(NO_SSM) && defined(NO_SCRAMNET)
typedef void MM;
#else
#include <mm/mm.h>
#endif

#if (!defined(NO_SSM) || !defined(NO_SCRAMNET) || !defined(NO_SM))
#include "rel_ptr.h"
#endif

#if defined(_WIN64) || (defined(__SIZEOF_POINTER__) && __SIZEOF_POINTER__== 8)
typedef unsigned long long ptr_int_t;
#else
typedef unsigned long ptr_int_t;
#endif

#if (!defined(NO_SM))
THREAD_PRIVATE MM* var_sharedmemory_segment::Ptr = 0;
THREAD_PRIVATE MM* cache_sharedmemory_segment::Ptr = 0;
THREAD_PRIVATE MM* cache_sharedmemory_segment::Ptr_end = 0;
#endif

#if (!defined(NO_SSM) || !defined(NO_SCRAMNET))
THREAD_PRIVATE MM* cache_staticmemory_segment::Ptr = 0;
THREAD_PRIVATE MM* cache_staticmemory_segment::Ptr_end = 0;
#endif

#if (!defined(NO_SM))
cache_typ* smcache_typ::sm_allocate(size_t sz)
{
    lock();
    cache_typ* ptr = (cache_typ*)mm_malloc(cache_sharedmemory_segment::Ptr, (sz * sizeof(unsigned int)) + sizeof(cache_typ));
    unlock();
    if (ptr)
    {
        ptr->construct(sz);
    }
    else
    {
        dout << "Warning: Unable to allocate cache buffer\n";
    }
    return ptr;
}

cache_typ* smcache_typ::sm_duplicate(unsigned int id, size_t len, const unsigned int* src)
{
    cache_typ* dst = sm_allocate(len);
    if (dst)
    {
        dst->set_id(id);
        unsigned int* dstbuf = dst->buf();

        for (unsigned int cnt = 0; cnt < len; ++cnt)
        {
            dstbuf[cnt] = src[cnt];
        }
    }
    return dst;
}
#else
cache_typ* smcache_typ::sm_allocate(size_t)
{
    return 0;
}
cache_typ* smcache_typ::sm_duplicate(unsigned int, size_t, const unsigned int*)
{
    return 0;
}
#endif

#if (!defined(NO_SSM) || !defined(NO_SCRAMNET))
cache_typ* smcache_typ::ssm_allocate(size_t sz)
{
    cache_typ* ptr = (cache_typ*)mm_malloc(cache_staticmemory_segment::Ptr, (sz * sizeof(unsigned int)) + sizeof(cache_typ));
    if (ptr)
    {
        ptr->construct(sz);
    }
    else
    {
        dout << "Warning: Unable to allocate cache buffer\n";
    }
    return ptr;
}

cache_typ* smcache_typ::ssm_duplicate(cache_typ* cache, size_t sz)
{
    cache_typ* ptr = (cache_typ*)mm_malloc(cache_staticmemory_segment::Ptr, (sz * sizeof(unsigned int)) + sizeof(cache_typ));
    if (ptr)
    {
        memcpy(ptr, cache, (sz * sizeof(unsigned int)) + sizeof(cache_typ));
    }
    else
    {
        dout << "Warning: Unable to allocate duplicate cache buffer\n";
    }
    return ptr;
}

void smcache_typ::ssm_deallocate(cache_typ* ptr)
{
    // perform a pointer check first
    if (((ptr_int_t)ptr >= (ptr_int_t)cache_staticmemory_segment::Ptr) && ((ptr_int_t)ptr < (ptr_int_t)cache_staticmemory_segment::Ptr_end))
    {
        if ((ptr->get_len() * sizeof(unsigned int) + sizeof(cache_typ)) == mm_sizeof(cache_staticmemory_segment::Ptr, ptr))
        {
            if (ptr->check_header())
            {
                memset(ptr, 255, (ptr->get_len() * sizeof(unsigned int)) + sizeof(cache_typ));
            }
            else
            {
                dout << "Warning: Cache MAGIC number is corrupted\n";
            }
        }
        else
        {
            dout << "Warning: Cache pointer size mismatch\n";
        }
        mm_free(cache_staticmemory_segment::Ptr, ptr);
    }
    else
    {
        dout << "Warning: Ptr address " << (ptr_int_t)ptr << " seems invalid, MM = " << (ptr_int_t)cache_staticmemory_segment::Ptr << " MM_end = " << (ptr_int_t)cache_staticmemory_segment::Ptr_end << '\n';
    }
}
#endif

void smcache_typ::sm_deallocate(cache_typ* ptr)
{
#ifndef NO_SM
    // perform a pointer check first
    if (((ptr_int_t)ptr >= (ptr_int_t)cache_sharedmemory_segment::Ptr) && ((ptr_int_t)ptr < (ptr_int_t)cache_sharedmemory_segment::Ptr_end))
    {
        if ((ptr->get_len() * sizeof(unsigned int) + sizeof(cache_typ)) == mm_sizeof(cache_sharedmemory_segment::Ptr, ptr))
        {
            if (ptr->check_header())
            {
                memset(ptr, 255, (ptr->get_len() * sizeof(unsigned int)) + sizeof(cache_typ));
            }
            else
            {
                dout << "Warning: Cache MAGIC number is corrupted\n";
            }
        }
        else
        {
            dout << "Warning: Cache pointer size mismatch\n";
        }
        lock();
        mm_free(cache_sharedmemory_segment::Ptr, ptr);
        unlock();
    }
    else
    {
        dout << "Warning: Ptr address " << (ptr_int_t)ptr << " seems invalid, MM = " << (ptr_int_t)cache_sharedmemory_segment::Ptr << " MM_end = " << (ptr_int_t)cache_sharedmemory_segment::Ptr_end << '\n';
    }
#endif
}

void smcache_typ::lock()
{
#ifndef NO_SM
    for (int cnt = 0; cnt < 1000; ++cnt)
    {
        if ((mm_lock(cache_sharedmemory_segment::Ptr, MM_LOCK_RW) != 0) || (cache_sharedmemory_segment::Ptr == 0))
        {
            return;
        }
    }
    assert(0); // forces program to exit (dead-lock condition)
#endif
}

void smcache_typ::rdlock()
{
#ifndef NO_SM
    for (int cnt = 0; cnt < 1000; ++cnt)
    {
        if ((mm_lock(cache_sharedmemory_segment::Ptr, MM_LOCK_RD) != 0) || (cache_sharedmemory_segment::Ptr == 0))
        {
            return;
        }
    }
    assert(0); // forces program to exit (dead-lock condition)
#endif
}

void smcache_typ::unlock()
{
#ifndef NO_SM
    mm_unlock(cache_sharedmemory_segment::Ptr);
#endif
}

#ifndef NO_SM
bool smcache_typ::init(rmglobal_typ& glob, unsigned int sz)
{
    cache_sharedmemory_segment::Ptr = glob.attach_cache(sz);
    if (cache_sharedmemory_segment::Ptr)
    {
        cache_sharedmemory_segment::Ptr_end = (char*)cache_sharedmemory_segment::Ptr + mm_core_align2page(sz);
    }
    else
    {
        cache_sharedmemory_segment::Ptr_end = NULL;
    }
    return cache_sharedmemory_segment::Ptr != NULL;

}
#else
bool smcache_typ::init(rmglobal_typ&, unsigned int)
{
    return true;
}
#endif

void smcache_typ::sm_reserve(cache_typ* ptr)
{
    lock();
    ptr->h_reserve();
    unlock();
}

void smcache_typ::sm_unreserve(cache_typ* ptr)
{
    lock();
    const unsigned int _ref = ptr->unreserve();
    unlock();
    if (_ref == 0)
    {
        sm_deallocate(ptr);
    }
}

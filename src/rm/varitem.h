/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/
/* Note: This file is internal to the library                     */
/******************************************************************/

#ifndef _VARITEM_H_
#define _VARITEM_H_

#include "rmglobal.h"
class cache_typ;

#if MAX_CLIENT <= 64
#if MAX_CLIENT <= 32
typedef unsigned int flag_typ;
#else
typedef unsigned long long flag_typ;
#endif
struct RM_EXPORT notify_typ
{
    notify_typ()
    {
        notify = 0;
        onceonly = 0;
    }
    notify_typ(const notify_typ& other) :
        notify(other.notify), onceonly(other.onceonly)
    {
    }
    bool operator[](unsigned int idx)
    {
        bool result = (((notify >> idx) % 2) != 0) || (((onceonly >> idx) % 2) != 0);
        onceonly &= ~((flag_typ)1 << idx);
        return result;
    }
    void set(unsigned int idx, bool flag)
    {
        if (flag)
        {
            notify |= ((flag_typ)1 << idx);
        }
        else
        {
            notify &= ~((flag_typ)1 << idx);
        }
    }
    void setonce(unsigned int idx)
    {
        onceonly |= ((flag_typ)1 << idx);
    }
    const notify_typ& operator=(const notify_typ& other)
    {
        notify = other.notify;
        onceonly = other.onceonly;
        return *this;
    }
    bool not_empty()
    {
        return (notify | onceonly) != 0;
    }
    flag_typ notify, onceonly;
};
#else
struct RM_EXPORT notify_typ
{
    notify_typ()
    {
        for (int cnt = 0; cnt < MAX_CLIENT; ++cnt)
        {
            notify[cnt] = onceonly[cnt] = false;
        }
    }
    notify_typ(const notify_typ& other)
    {
        for (int cnt = 0; cnt < MAX_CLIENT; ++cnt)
        {
            notify[cnt] = other.notify[cnt];
            onceonly[cnt] = other.onceonly[cnt];
        }
    }
    bool operator[](unsigned int idx)
    {
        bool result = notify[idx] | onceonly[idx];
        onceonly[idx] = false;
        return result;
    }
    void set(unsigned int idx, bool flag)
    {
        notify[idx] = flag;
    }
    void setonce(unsigned int idx)
    {
        onceonly[idx] = true;
    }
    const notify_typ& operator=(const notify_typ& other)
    {
        for (int cnt = 0; cnt < MAX_CLIENT; ++cnt)
        {
            notify[cnt] = other.notify[cnt];
            onceonly[cnt] = other.onceonly[cnt];
        }
        return *this;
    }
    bool not_empty()
    {
        for (int cnt = 0; cnt < MAX_CLIENT; ++cnt)
        {
            if (notify[cnt] || onceonly[cnt])
            {
                return true;
            }
        }
        return false;
    }
    bool notify[MAX_CLIENT];
    bool onceonly[MAX_CLIENT];
};
#endif

#include "rel_ptr.h"
#include "cache.h"

struct RM_EXPORT var_item_typ
{
    explicit var_item_typ();
    var_item_typ(const var_item_typ& other);
    const var_item_typ& operator=(const var_item_typ& other);
    relative_ptr<cache_typ,cache_sharedmemory_segment> ptr;
    notify_typ notify;
};

#endif /* _VARITEM_H_ */

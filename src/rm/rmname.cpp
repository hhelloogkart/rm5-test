/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "rmname.h"

rmname_typ::rmname_typ()
{
    name[0] = '\0';
}

rmname_typ::rmname_typ(const rmname_typ& other)
{
    operator=(other);
}

rmname_typ::rmname_typ(const char* other)
{
    unsigned int cnt;
    for (cnt = 0; (cnt < RMNAME_LEN-1) && (other[cnt] != '\0'); ++cnt)
    {
        name[cnt] = other[cnt];
    }
    name[cnt] = '\0';
}

unsigned int rmname_typ::size() const
{
    unsigned int cnt;
    for (cnt = 0; (name[cnt] != '\0') && (cnt < RMNAME_LEN); ++cnt)
    {
        ;
    }
    return cnt;
}

void rmname_typ::operator+=(const char* other)
{
    unsigned int cnt1, cnt2;
    for (cnt1 = size(), cnt2 = 0; (cnt1 < RMNAME_LEN-1) && (other[cnt2] != '\0'); ++cnt1, ++cnt2)
    {
        name[cnt1] = other[cnt2];
    }
    name[cnt1] = '\0';
}

const rmname_typ& rmname_typ::operator=(const rmname_typ& other)
{
    if (&other != this)
    {
        unsigned int cnt;
        for (cnt = 0; (other.name[cnt] != '\0') && (cnt < RMNAME_LEN-1); ++cnt)
        {
            name[cnt] = other.name[cnt];
        }
        name[cnt] = '\0';
    }
    return *this;
}

const rmname_typ& rmname_typ::operator=(const char* other)
{
    if (other != name)
    {
        unsigned int cnt;
        for (cnt = 0; (other[cnt] != '\0') && (cnt < RMNAME_LEN-1); ++cnt)
        {
            name[cnt] = other[cnt];
        }
        name[cnt] = '\0';
    }
    return *this;
}

bool rmname_typ::operator==(const char* other) const
{
    for (unsigned int cnt = 0; cnt < RMNAME_LEN; ++cnt)
    {
        if (name[cnt] != other[cnt])
        {
            return false;
        }
        if (name[cnt] == '\0')
        {
            return true;
        }
    }
    return true;
}

bool rmname_typ::operator<(const rmname_typ& other) const
{
    if (this == &other)
    {
        return false;
    }
    for (unsigned int cnt = 0; cnt < RMNAME_LEN; ++cnt)
    {
        if (name[cnt] < other.name[cnt])
        {
            return true;
        }
        if (name[cnt] > other.name[cnt])
        {
            return false;
        }
        if (name[cnt] == '\0')
        {
            return false;
        }
    }
    return false;
}


/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/
#define MM_PRIVATE
#include "rmscram.h"
#include "../rmstl.h"
#include "directory.h"
#include "lockfreequeue.h"
#include "smcache.h"
#include <util/dbgout.h>
#include <time/timecore.h>
#include "../gt200/scgtapi.h"

typedef VECTOR(unsigned int) idx2id_typ;
typedef MAP(unsigned int, unsigned int, less<unsigned int>) id2idx_typ;

struct info_typ
{
    int id; //1st byte is queue type, next 3 bytes global ID of variable
    var_item_typ var_item;  //32 bit field to indicate which client to send to
};
struct RMScram_private
{
    unsigned int* memPtr;   //Pointer to the start of shared memory
    info_typ* infoArea;     //Pointer to info shared memory
    LockFreeQueue<RMScram>* queueSArea; //Pointer to the queueS shared memory
    LockFreeQueue<RMScram>* recvMArea;  //Pointer to the receiverM shared memory;
    unsigned int* queueMArea;           //Pointer to the queueM shared memory
    unsigned int* recvSArea; //Pointer to the receiverS shared memory;
    unsigned int numVar;    //Number of variables
    unsigned int numQueueS; //Number of queueS
    unsigned int numQueueM; //Number of queueM
    unsigned int numRecvS;  //Number of receiverS
    unsigned int numRecvM;  //Number of receiverM
    unsigned int clientId;  //Unique ID to identify the client
    unsigned int msgSize;   //Max buffer size that receiver allocates
    MM* cacheS;             //Pointer to own cache
    MM* cacheR;             //Pointer to own cache
    id2idx_typ id2idx;      //Map to store the id to idx mapping
    LockFreeQueue<RMScram>* queue;  //A temporary queue
    unsigned int notyIndex; //A temporary index
    notify_typ noty;        //A temporary notify
    unsigned int cache_size;    //Size of cache
    unsigned int* mapAreaS; //Maps idxx to clientId for queueS
    unsigned int* mapAreaR; //Maps idxx to clientId for recvM
    bool isConnected;
    scgtHandle handle;
};

template<>
int lockfree_atomic_cas<RMScram>(volatile long* addr, long new_val, long)
{
    /*--------- API method -----------*/
    /*unsigned int bytesTransferred = 0;
       int *buf;
     * buf = new_val;
       scgtWrite(&handle, (addr-(int*)beginPtr), buf, 4, SCGT_RW_PIO, &bytesTransferred, NULL);
       return (bytesTransferred == 4);*/
    /*--------------------------------*/

    addr[0] = new_val;  //PIO write
    return 1;
}

RMScram::RMScram() : prvt(new RMScram_private)
{
    prvt->memPtr = NULL;    //Initialise to NULL
    prvt->isConnected = false;
    scgtOpen(0, &prvt->handle); /* open GT, unit number 0*/
}

RMScram::~RMScram()
{
    scgtClose(&prvt->handle);
    Close();
    delete prvt;
}

bool RMScram::Connect(const char* host, int port, const struct timeval*)
{
    char hostname[64];
    id2idx_typ::iterator iter;

    if(!Connected())
    {
        for (unsigned int cnt = 0; host[cnt]!='\0'; ++cnt)
        {
            if (host[cnt] == ',')
            {
                rm_strncpy(hostname, 64, host, cnt);
                hostname[cnt] = '\0';
                prvt->clientId = atoi(host+cnt+1);
                break;
            }
        }

        if(prvt->clientId > MAX_CLIENT)
        {
            dout << "Scram: client id not specified\n";
            return false;
        }

        if(port == 0)
        {
            dout << "Scram: port is 0\n";
            return false;
        }
        prvt->msgSize = port;

        prvt->memPtr = (unsigned int*)scgtMapMem(&prvt->handle);

        if(prvt->memPtr == NULL)
        {
            dout << "Scram: mem ptr is null\n";
            return false;
        }

        //Read the magic number
        if(prvt->memPtr[0] != MAGICNUM)
        {
            //Memory is not set up
            dout << "Scram: memory not set up\n";
            prvt->memPtr = NULL;
            return false;
        }

        //Read the cache size
        prvt->cache_size = prvt->memPtr[1];

        //Read the client offset
        void* pointer = prvt->memPtr + prvt->memPtr[prvt->clientId + 2];

        //Populate the vectors and maps
        prvt->numVar = *((unsigned int*)pointer);
        pointer = ((unsigned int*)pointer)+1;
        prvt->infoArea = (info_typ*)pointer;
        for(unsigned int i = 0; i<prvt->numVar; i++)
        {
            const info_typ info = prvt->infoArea[i];
            unsigned int id = info.id & 0xFFFFFF;
            prvt->id2idx[id] = i+1;
        }

        pointer = ((info_typ*)pointer) + prvt->numVar;
        prvt->numQueueS = *((unsigned int*)pointer);
        pointer = ((unsigned int*)pointer)+1;

        if(prvt->numQueueS != 0)
        {
            prvt->queueSArea = (LockFreeQueue<RMScram>*)pointer;
            pointer = ((LockFreeQueue<RMScram>*)pointer) + prvt->numQueueS;
        }

        prvt->mapAreaS = (unsigned int*) pointer;
        pointer = ((unsigned int*)pointer)+prvt->numQueueS;

        prvt->numRecvM = *((unsigned int*)pointer);
        pointer = ((unsigned int*)pointer)+1;

        if(prvt->numRecvM != 0)
        {
            prvt->recvMArea = (LockFreeQueue<RMScram>*)pointer;
            pointer = ((LockFreeQueue<RMScram>*)pointer) + prvt->numRecvM;
        }

        prvt->mapAreaR = (unsigned int*) pointer;
        pointer = ((unsigned int*)pointer)+prvt->numRecvM;

        prvt->numRecvS = *((unsigned int*)pointer);
        pointer = ((unsigned int*)pointer)+1;

        if(prvt->numRecvS != 0)
        {
            prvt->recvSArea = (unsigned int*)pointer;
            pointer = ((unsigned int*)pointer) + (2*prvt->numRecvS);
        }

        prvt->numQueueM = *((unsigned int*)pointer);
        pointer = ((unsigned int*)pointer)+1;

        if(prvt->numQueueM != 0)
        {
            prvt->queueMArea = (unsigned int*)pointer;
            pointer = ((unsigned int*)pointer) + (2*prvt->numQueueM);
        }

        prvt->cacheS = (MM*)pointer;
        pointer = (unsigned int*)pointer + (prvt->cache_size/sizeof(unsigned int));
        prvt->cacheR = (MM*)pointer;

        if(prvt->numRecvM > 0)
        {
            const size_t nodesize = mm_core_align2word(prvt->msgSize * sizeof(unsigned int) + sizeof(cache_typ) + SIZEOF_mem_chunk);
            const size_t nodenum = (prvt->cache_size/prvt->numRecvM/nodesize) - 1;

            if(nodenum < 2)
            {
                dout << "SSM: Number of nodes is " << nodenum << "\n";
                prvt->memPtr = NULL;
                return false;
            }

            cache_staticmemory_segment::Ptr = prvt->cacheR;
            cache_staticmemory_segment::Ptr_end = (char*)cache_staticmemory_segment::Ptr + mm_core_align2page(prvt->cache_size);

            for(unsigned int cnt = 0; cnt < prvt->numRecvM; cnt++)
            {
                prvt->recvMArea[cnt].Setup(prvt->msgSize, static_cast<unsigned int>(nodenum));
            }
        }
        prvt->isConnected = true;
    }
    return true;
}

bool RMScram::Connected()
{
    return prvt->isConnected;
}

bool RMScram::Close()
{
    if (!Connected())
    {
        return false;
    }
    prvt->memPtr = NULL;
    prvt->isConnected = false;
    return true;
}

int RMScram::Select(const struct timeval* stm, bool defercb)
{
    cache_typ* data;
    id2idx_typ::const_iterator iter;
    LockFreeQueue<RMScram>* queue;
    const timeExpire expire(stm);
    bool max_packet_summary; /* true when at least one queue has 100 packets waiting */
    int sel = 0; /* true when at least one message is received */
    bool max_packet[64] = {
        true, true, true, true, true, true, true, true,
        true, true, true, true, true, true, true, true,
        true, true, true, true, true, true, true, true,
        true, true, true, true, true, true, true, true,
        true, true, true, true, true, true, true, true,
        true, true, true, true, true, true, true, true,
        true, true, true, true, true, true, true, true,
        true, true, true, true, true, true, true, true
    };
    /* true when the queue has 100 packets waiting, initially true */

    if(!Connected())
    {
        return -1;
    }
    if(defercb)
    {
        return 0;
    }

    do
    {
        max_packet_summary = false;

        cache_staticmemory_segment::Ptr = prvt->cacheR;
        cache_staticmemory_segment::Ptr_end = (char*)cache_staticmemory_segment::Ptr + mm_core_align2page(prvt->cache_size);

        for(unsigned int cnt = 0; cnt < prvt->numRecvM; cnt++)
        {
            if(max_packet[cnt+32])
            {
                int readcnt;
                for(readcnt = 0; (readcnt < 100) && (prvt->recvMArea[cnt].Request(data) == true); ++readcnt, context_yield())
                {
                    iter = prvt->id2idx.find(data->get_id());
                    if (iter != prvt->id2idx.end())
                    {
                        (*(callback[data->get_cmd()]))(callback_arg[data->get_cmd()], (*iter).second, data);
                    }
                    sel = 1;
                }
                max_packet[cnt+32] = (readcnt == 100);
                max_packet_summary |= max_packet[cnt+32];
            }
        }

        for(unsigned int cnt = 0; cnt < prvt->numRecvS; cnt++)
        {
            if (max_packet[cnt])
            {
                cache_staticmemory_segment::Ptr = prvt->memPtr + prvt->recvSArea[cnt];
                queue = (LockFreeQueue<RMScram>*)(prvt->memPtr + prvt->recvSArea[cnt + prvt->numRecvS]);

                int readcnt;
                for (readcnt = 0; (readcnt < 100) && (queue->Consume(data) == true); ++readcnt, context_yield())
                {
                    iter = prvt->id2idx.find(data->get_id());
                    if (iter != prvt->id2idx.end())
                    {
                        (*(callback[data->get_cmd()]))(callback_arg[data->get_cmd()], (*iter).second, data);
                    }
                    sel = 1;
                }
                max_packet[cnt] = (readcnt == 100);
                max_packet_summary |= max_packet[cnt];
            }
        }

    }
    while(max_packet_summary);

    while(sel == 0  &&  !expire())
    {
        cache_staticmemory_segment::Ptr = prvt->cacheR;
        cache_staticmemory_segment::Ptr_end = (char*)cache_staticmemory_segment::Ptr + mm_core_align2page(prvt->cache_size);

        for(unsigned int cnt = 0; cnt < prvt->numRecvM; cnt++)
        {
            if(prvt->recvMArea[cnt].Request(data) == true)
            {
                iter = prvt->id2idx.find(data->get_id());
                if (iter != prvt->id2idx.end())
                {
                    (*(callback[data->get_cmd()]))(callback_arg[data->get_cmd()], (*iter).second, data);
                }
                sel = 1;
            }
            context_yield();
        }

        for(unsigned int cnt = 0; cnt < prvt->numRecvS; cnt++)
        {
            cache_staticmemory_segment::Ptr = prvt->memPtr + prvt->recvSArea[cnt];
            queue = (LockFreeQueue<RMScram>*)(prvt->memPtr + prvt->recvSArea[cnt + prvt->numRecvS]);

            if(queue->Consume(data) == true)
            {
                iter = prvt->id2idx.find(data->get_id());
                if (iter != prvt->id2idx.end())
                {
                    (*(callback[data->get_cmd()]))(callback_arg[data->get_cmd()], (*iter).second, data);
                }
                sel = 1;
            }
            context_yield();
        }
    }
    return sel;
}

const cache_typ* RMScram::GetListStart(unsigned int)
{
    return NULL;
}

void RMScram::GetListEnd(unsigned int, const cache_typ*)
{
    //Do nothing
}

cache_typ* RMScram::SetListStart(unsigned int idx, size_t len)
{
    if(idx > prvt->numVar)
    {
        return NULL;
    }

    cache_staticmemory_segment::Ptr = prvt->cacheS;
    cache_staticmemory_segment::Ptr_end = (char*)cache_staticmemory_segment::Ptr + mm_core_align2page(prvt->cache_size);

    for(unsigned int i=0; i<prvt->numQueueS; i++)
    {
        prvt->queueSArea[i].Trim();
    }

    return smcache_typ::ssm_allocate(len);
}

cache_typ* RMScram::MessageStart(unsigned int idx, size_t len)
{
    cache_typ* cache = NULL;
    if(!Connected() || (idx > prvt->numVar) || (len > prvt->msgSize))
    {
        dout << "MessageStart: idx " << idx << "is more than numVar " << prvt->numVar << "\n";
        return 0;
    }

    const info_typ info = prvt->infoArea[idx - 1];
    prvt->noty = info.var_item.notify;
    for(prvt->notyIndex=0; prvt->notyIndex<prvt->numQueueM; prvt->notyIndex++)
    {
        if(prvt->noty[prvt->notyIndex])
        {
            cache_staticmemory_segment::Ptr = prvt->memPtr + prvt->queueMArea[prvt->notyIndex];
            prvt->queue = ((LockFreeQueue<RMScram>*)(prvt->memPtr + prvt->queueMArea[prvt->notyIndex+prvt->numQueueM]));
            cache = prvt->queue->getCache();
            if(cache)
            {
                cache->set_len(static_cast<unsigned int>(len));
                cache->set_cmd(MESSAGE);
                cache->set_id(info.id & 0xFFFFFF);
                break;
            }
        }
    }
    return cache;
}

bool RMScram::SetListEnd(unsigned int idx, cache_typ* pbuf)
{
    if (!Connected() || pbuf == NULL)
    {
        return false;
    }

    if(idx > prvt->numVar)
    {
        dout << "SetlistEnd: idx " << idx << "is more than numVar " << prvt->numVar << "\n";
        smcache_typ::ssm_deallocate(pbuf);
        return false;
    }

    const info_typ info = prvt->infoArea[idx-1];
    const unsigned int id = info.id & 0xFFFFFF;
    pbuf->set_cmd(SETLIST);
    pbuf->set_id(id);

    bool found = false;
    for(unsigned int i=0; i<prvt->numQueueS; i++)
    {
        notify_typ noty(info.var_item.notify);
        if(noty[i])
        {
            if(found)
            {
                cache_typ* cache = smcache_typ::ssm_duplicate(pbuf, pbuf->get_len());
                if (cache != NULL)
                {
                    prvt->queueSArea[i].Produce(cache);
                }
            }
            else
            {
                prvt->queueSArea[i].Produce(pbuf);
                found = true;
            }
        }
    }
    if(!found)
    {
        smcache_typ::ssm_deallocate(pbuf);
        return false;
    }
    return true;
}

bool RMScram::BroadcastEnd(unsigned int id, cache_typ* pbuf)
{
    return SetListEnd(id, pbuf);
}

bool RMScram::RegisterIDX(unsigned int)
{
    return true;
}

bool RMScram::UnregisterIDX(unsigned int)
{
    return true;
}

bool RMScram::RegisterID(const char*)
{
    return false;
}

bool RMScram::QueryID(const char*)
{
    return false;
}

bool RMScram::QueryID(const char*[], int)
{
    return false;
}

bool RMScram::ClientID(const char*)
{
    return false;
}

bool RMScram::FlowControl(unsigned int)
{
    // No flow control for shared memory
    return true;
}

bool RMScram::Broadcast(unsigned int id, size_t len, const unsigned int* buf)
{
    return SetList(id, len, buf);
}

bool RMScram::SetList(unsigned int id, size_t len, const unsigned int* buf)
{
    cache_typ* packet = SetListStart(id, len);
    packet->set_id(id);
    packet->set_cmd(SETLIST);
    unsigned int* dstbuf = packet->buf();

    for (unsigned int cnt = 0; cnt < len; ++cnt)
    {
        dstbuf[cnt] = buf[cnt];
    }

    return SetListEnd(id, packet);
}

bool RMScram::Message(unsigned int id, size_t len, const unsigned int* buf)
{
    cache_typ* packet = MessageStart(id, len);
    packet->set_id(id);
    packet->set_cmd(MESSAGE);
    unsigned int* dstbuf = packet->buf();

    for (unsigned int cnt = 0; cnt < len; ++cnt)
    {
        dstbuf[cnt] = buf[cnt];
    }

    return MessageEnd(id, packet);
}

size_t RMScram::GetList(unsigned int id, size_t len, unsigned int* buf)
{
    const cache_typ* ptr = GetListStart(id);
    if (ptr)
    {
        size_t sz = ptr->output(len, buf);
        GetListEnd(id, ptr);
        return sz;
    }
    return 0;
}

bool RMScram::WaitForHandshakeCompletion(const struct timeval*)
{
    return true;
}

bool RMScram::MessageEnd(unsigned int idx, cache_typ* pbuf, void*)
{
    if(!Connected() || pbuf == NULL || (idx > prvt->numVar))
    {
        dout << "MessageEnd: idx " << idx << "is more than numVar " << prvt->numVar << "\n";
        return false;
    }

    prvt->queue->fillCache();   //queue stored from messageStart

    for(unsigned int i = prvt->notyIndex + 1; i<prvt->numQueueM; i++)
    {
        if(prvt->noty[i])
        {
            cache_staticmemory_segment::Ptr = prvt->memPtr + prvt->queueMArea[i];
            prvt->queue = ((LockFreeQueue<RMScram>*)(prvt->memPtr + prvt->queueMArea[i+prvt->numQueueM]));
            cache_typ* cache = prvt->queue->getCache();
            if(cache != NULL)
            {
                memcpy(cache, pbuf, sizeof(cache_typ)+(pbuf->get_len()*sizeof(unsigned int)));
                prvt->queue->fillCache();
            }
        }
    }
    return true;
}

bool RMScram::Flush()
{
    return true;
}

bool RMScram::CheckLink()
{
    return true;
}

bool RMScram::HandshakeAck(unsigned int, unsigned int)
{
    // SM does not wait for handshake completion
    return true;
}

void RMScram::KeepAlive()
{
}

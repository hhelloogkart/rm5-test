/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _DIRECTORY_H_
#define _DIRECTORY_H_

#ifdef _MSC_VER
#pragma warning(disable: 4786) // disable warning C4786: symbol greater than 255 character
#endif

#include "../rmstl.h"
#include <mm/mm.h>
#include "rmname.h"
#include "varitem.h"
#include "rel_ptr.h"

#if (defined(__INTEL_COMPILER) && __INTEL_COMPILER >= 1000) || (defined(__GNUC__) && !defined(__INTEL_COMPILER) && __GNUC__ >= 3) || (defined(_MSC_VER) && (_MSC_VER >= 1500))
#include <dinkum_stl/vector>
#include <dinkum_stl/map>
#endif

template<class T, class M>
class RM_EXPORT var_allocator
{
public:
    typedef T value_type;
    typedef size_t size_type;
    typedef ptrdiff_t difference_type;
    typedef relative_ptr<T,M> pointer;
    typedef const_relative_ptr<T,M> const_pointer;
    typedef T& reference;
    typedef const T& const_reference;

    var_allocator()
    {
    }
    var_allocator(const var_allocator<T,M>&)
    {
    }
    template<class U>
    var_allocator(const var_allocator<U,M>&)
    {
    }
    pointer address(reference ref) const
    {
        return &ref;
    }
    const_pointer address(const_reference ref) const
    {
        return &ref;
    }
    void* raw_allocate( size_type size, size_type n, void* )
    {
        return mm_malloc(M::Ptr, size * n);
    }
    void raw_deallocate( void* ptr )
    {
        mm_free(M::Ptr, ptr);
    }
    pointer allocate( size_type n, const void* )
    {
        return (pointer)raw_allocate( sizeof(T), n, NULL);
    }
    pointer allocate( size_type n )
    {
        return (pointer)raw_allocate( sizeof(T), n, NULL);
    }
    void* _Charalloc(size_type size)
    {
        return raw_allocate(size, 1, NULL);
    }
    void deallocate( pointer ptr, size_type )
    {
        raw_deallocate(ptr);
    }
    void deallocate( pointer ptr )
    {
        raw_deallocate(ptr);
    }
    void deallocate( void* ptr, size_type )
    {
        raw_deallocate(ptr);
    }
    void deallocate( void* ptr )
    {
        raw_deallocate(ptr);
    }
    void construct(const relative_ptr<T,M>& ptr, const T& val)
    {
        new ((void*)ptr) T (val);
    }
    template <class U> void construct(U* ptr, const U& val)
    {
        new (ptr) U (val);
    }
    template <class U> void construct(U* ptr, int)
    {
        new (ptr) U;
    }
    template <class U> void construct(relative_ptr<U,M>& ptr, const U& val)
    {
        new ((void*)ptr) U (val);
    }
    template <class U> void construct(relative_ptr<U,M>& ptr, int)
    {
        new ((void*)ptr) U;
    }
#ifdef _WIN32
#pragma warning(disable: 4100)
#endif
    template <class U> void destroy(U* ptr)
    {
        (*ptr).~U();
    }
#ifdef _WIN32
#pragma warning(default: 4100)
#endif
    template <class U> void destroy(relative_ptr<U,M>& ptr)
    {
        ptr->~U();
    }
    size_type max_size() const
    {
        return mm_core_size(M::Ptr) / sizeof(T);
    }
    template <class U>
    struct RM_EXPORT rebind
    {
        typedef var_allocator<U,M> other;
    };
};

template<class M>
class RM_EXPORT var_allocator<void,M>
{
    typedef relative_ptr<void,M> pointer;
};

typedef VECTOR3(var_item_typ,var_allocator,var_sharedmemory_segment) directory_typ;
typedef MAP3(rmname_typ,unsigned int,less<rmname_typ>,var_allocator,var_sharedmemory_segment) namedir_typ;

template<class M> inline
void _Destroy_range(var_item_typ*, var_item_typ*, var_allocator<var_item_typ,M>&)
{   // destroy [_First, _Last), scalar type (do nothing)
}

#endif /* _DIRECTORY_H_ */

#include <block_var.h>
#include <decodeprefix_var.h>
#include <avl_var.h>
#include <rm_var.h>
#include <cyclshow.h>
#include <task/putil.h>
#include <list>
#include <map>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>

#if defined(__BYTE_ORDER) && __BYTE_ORDER == __BIG_ENDIAN || \
    defined(__BIG_ENDIAN__) || \
    defined(__ARMEB__) || \
    defined(__THUMBEB__) || \
    defined(__AARCH64EB__) || \
    defined(_MIBSEB) || defined(__MIBSEB) || defined(__MIBSEB__) || \
    defined(_M_PPC) || \
    defined(FLIP)
#define DECODE_BIG_ENDIAN
#include <rgen_var.h>
#endif

/*
 *** Inline helper functions
 */
inline const char* extract_string(mpt_var* var)
{
    value_var* vv = dynamic_cast<value_var*>(var);
    if (vv)
    {
        return vv->to_const_char_ptr();
    }

    array_value_var* avv = dynamic_cast<array_value_var*>(var);
    if (avv)
    {
        return avv->to_const_char_ptr(0);
    }

    return 0;
}

inline int block_offset(int blk)
{
    if (blk < 128)
    {
        return 1;
    }
    if (blk < 16384)
    {
        return 2;
    }
    if (blk < 2097152)
    {
        return 3;
    }
    if (blk < 268435456)
    {
        return 4;
    }
    return 5;
}

inline int guessed_num_block(long long len, int block_sz)
{
    const int capacity_at_1_byte = (block_sz - 1) * 128;
    const int capacity_at_2_byte = (block_sz - 2) * (16384 - 128) + capacity_at_1_byte;
    const long long capacity_at_3_byte = (block_sz - 3) * (2097152LL - 16384LL - 128LL) + capacity_at_2_byte + capacity_at_1_byte;
    const long long capacity_at_4_byte = (block_sz - 4) * (268435456LL - 2097152LL - 16384LL - 128LL) + capacity_at_3_byte + capacity_at_2_byte + capacity_at_1_byte;

    if (len > capacity_at_4_byte)
    {
        const long long left = len - capacity_at_4_byte;
        const long long rem = left % (block_sz - 5);
        return static_cast<int>((left / (block_sz - 5)) + (rem != 0) + 268435456);
    }
    else if (len > capacity_at_3_byte)
    {
        const long long left = len - capacity_at_3_byte;
        const long long rem = left % (block_sz - 4);
        return static_cast<int>((left / (block_sz - 4)) + (rem != 0) + 2097152);
    }
    else if (len > capacity_at_2_byte)
    {
        const int left = static_cast<int>(len) - capacity_at_2_byte;
        const int rem = left % (block_sz - 3);
        return (left / (block_sz - 3)) + (rem != 0) + 16384;
    }
    else if (len > capacity_at_1_byte)
    {
        const int left = static_cast<int>(len) - capacity_at_1_byte;
        const int rem = left % (block_sz - 2);
        return (left / (block_sz - 2)) + (rem != 0) + 128;
    }
    else
    {
        const int left = static_cast<int>(len);
        const int rem = left % (block_sz - 1);
        return (left / (block_sz - 1)) + (rem != 0);
    }
}

inline int derive_max_total_data_size(int first_packet_data_len, int block_sz, int num_blocks)
{
    int sz = first_packet_data_len;
    for (int idx = 1; idx < num_blocks; ++idx)
    {
        sz += block_sz - block_offset(idx);
    }
    return sz;
}

inline std::string wildcard_eval(const std::string &recv, const std::string &key, const std::string &val)
{
    if ((key.at(key.size() - 1) == '*') && (val.at(val.size() - 1) == '*'))
    {
        std::string mrg(val.data(), val.size() - 1);
        mrg.append(recv.data() + key.size() - 1);
        return mrg;
    }
    return val;
}

#if defined(_MSC_VER) && _MSC_VER > 1400
FILE* blk_var_fopen(const char* filename, const char* mode)
{
    FILE* handle;
    return (fopen_s(&handle, filename, mode) == 0) ? handle : 0;
}
#define __strdup _strdup
#define __fileno _fileno
#elif defined __ppc__
char* __strdup(const char* str)
{
    unsigned int len = strlen(str);
    char* new_str = new char[len];
    memcpy(new_str, str, len);
    return new_str;
}
#define __fileno fileno
FILE* blk_var_fopen(const char* filename, const char* mode)
{
    return fopen(filename, mode);
}
#else
FILE* blk_var_fopen(const char* filename, const char* mode)
{
    return fopen(filename, mode);
}
#define __strdup strdup
#define __fileno fileno
#endif

/*
 *** Queue for acknowledgement IDs, implemented as a ring buffer
 */
inline unsigned int block_var_queue_advance(unsigned int p)
{
    return (p + block_var_ack_queue::ELEMENT_SIZE) % block_var_ack_queue::RING_SIZE;
}

inline int& queue_element(block_var_ack_queue* tis, unsigned int idx)
{
    return reinterpret_cast<int&>(tis->ring_buffer[idx]);
}

inline const int& queue_element(const block_var_ack_queue* tis, unsigned int idx)
{
    return reinterpret_cast<const int&>(tis->ring_buffer[idx]);
}

void block_var_ack_queue::push(int value)
{
    if (isFull())
    {
        return;
    }
    queue_element(this, writer) = value;
    writer = block_var_queue_advance(writer);
}

int block_var_ack_queue::pop()
{
    const int value = queue_element(this, reader);
    reader = block_var_queue_advance(reader);
    return value;
}

bool block_var_ack_queue::isFull() const
{
    return block_var_queue_advance(writer) == reader;
}

bool block_var_ack_queue::isEmpty() const
{
    return reader == writer;
}

void block_var_ack_queue::clear()
{
    reader = 0;
    writer = 0;
}

/*
 *** Periodic show functions to manage block_var
 */
class every_beat_show : public mpt_baseshow
{
public:
    every_beat_show(block_var* v);
    void operator()();

private:
    block_var* var;
};

every_beat_show::every_beat_show(block_var* v)
    : var(v)
{
}

void every_beat_show::operator()()
{
    var->processing();
}

class skip_beat_show : public mpt_baseshow
{
public:
    skip_beat_show(block_var* v, int skip);
    void operator()();

private:
    block_var* var;
    int skip;
    int count;

};

skip_beat_show::skip_beat_show(block_var* v, int sk)
    : var(v), skip(sk), count(0)
{
}

void skip_beat_show::operator()()
{
    if ((++count % skip) == 0)
    {
        var->processing();
    }
}

class interval_show : public cycle_show
{
public:
    interval_show(block_var* v, unsigned int sec, unsigned int usec);
    void operator()();

private:
    block_var* var;

};

interval_show::interval_show(block_var* v, unsigned int sec, unsigned int usec)
    : cycle_show(sec, usec), var(v)
{
}

void interval_show::operator()()
{
    if (getDirty())
    {
        var->processing();
    }
}

/*
 *** Show functions that will automatically trigger the block transfer process
 */
struct trigger_list
{
    std::list<mpt_autoshow*> m;
};

class trigger_on_name_show : public mpt_autoshow
{
public:
    trigger_on_name_show(block_var* blk, mpt_var* name, mpt_var* var, bool (block_var::* f)(const char*, mpt_var*));
    void notDirty(mpt_var*);
    void operator()();

private:
    bool (block_var::* activate)(const char*, mpt_var*);
    mpt_var* activate_arg;
    mpt_var* trigger_var;
    block_var* block_obj;
};

trigger_on_name_show::trigger_on_name_show(block_var* blk, mpt_var* name, mpt_var* var, bool(block_var::* f)(const char*, mpt_var*)) :
    activate(f), activate_arg(var), trigger_var(name), block_obj(blk)
{
    addCallback(trigger_var);
}

void trigger_on_name_show::notDirty(mpt_var*)
{
    dirty = true;
}

void trigger_on_name_show::operator()()
{
    if (dirty)
    {
        dirty = false;
        const char* nm = extract_string(trigger_var);
        if (nm)
        {
            (block_obj->*activate)(nm, activate_arg);
        }
    }
}

class trigger_on_var_show : public mpt_autoshow
{
public:
    trigger_on_var_show(block_var* blk, const char* name, mpt_var* var, bool (block_var::* f)(const char*, mpt_var*));
    void notDirty(mpt_var*);
    void operator()();

private:
    bool (block_var::* activate)(const char*, mpt_var*);
    std::string activate_arg;
    mpt_var* trigger_var;
    block_var* block_obj;
};

trigger_on_var_show::trigger_on_var_show(block_var* blk, const char* name, mpt_var* var, bool (block_var::* f)(const char*, mpt_var*))
    : activate(f), activate_arg(name), trigger_var(var), block_obj(blk)
{
}

void trigger_on_var_show::notDirty(mpt_var*)
{
    dirty = true;
}

void trigger_on_var_show::operator()()
{
    if (dirty)
    {
        dirty = false;
        (block_obj->*activate)(activate_arg.c_str(), trigger_var);
    }
}

/*
 *** Rules to process when a new block transfer is received
 */
struct map_entry
{
    map_entry();
    map_entry(const map_entry&);
    ~map_entry();

    void set_var(mpt_var*, bool);
    void set_filename(const char*, bool);
    void set_callback(block_var_callback, bool);

    bool onceonly;
    union
    {
        void* arg;
        block_var_callback func;
    };
    int isvar; /* 0 - string (needs deleting), 1 - var, 2 - callback function */
};

map_entry::map_entry() : onceonly(true), arg(0), isvar(1)
{
}

map_entry::map_entry(const map_entry& ent) : onceonly(ent.onceonly), arg(ent.arg), isvar(ent.isvar)
{
}

map_entry::~map_entry()
{
    if (!isvar)
    {
        free(arg);
    }
}

void map_entry::set_var(mpt_var* v, bool o)
{
    onceonly = o;
    arg = v;
    isvar = 1;
}

void map_entry::set_filename(const char* c, bool o)
{
    onceonly = o;
    arg = __strdup(c);
    isvar = 0;
}

void map_entry::set_callback(block_var_callback f, bool o)
{
    onceonly = o;
    func = f;
    isvar = 2;
}

struct data_mapping
{
    std::map<std::string, map_entry> m;
    std::map<std::string, map_entry>::iterator find(const std::string &srch);
};

std::map<std::string, map_entry>::iterator data_mapping::find(const std::string &srch)
{
    std::map<std::string, map_entry>::iterator it = m.begin();
    for (; it != m.end(); ++it)
    {
        const std::string &entry = (*it).first;
        if (entry.at(entry.size() - 1) == '*')
        {
            const size_t cmpsz = entry.size() - 1;
            if ((srch.size() >= cmpsz) && (memcmp(srch.data(), entry.data(), cmpsz) == 0))
                return it;
        }
        else if (entry == srch) return it;
    }
    return m.end();
}

/*
 *** Receivers
 */
class receiver_base
{
public:
    virtual ~receiver_base();
    virtual void set_data(int blk, int len, const unsigned char* buf) = 0;
    virtual bool is_done() const = 0;
    virtual unsigned char percent_done() const = 0;
    time_t last_data() const;
    void stamp_data();

private:
    time_t data_timestamp;

};

receiver_base::~receiver_base()
{
}

time_t receiver_base::last_data() const
{
    return data_timestamp;
}

void receiver_base::stamp_data()
{
    data_timestamp = time(0);
}

class file_receiver : public receiver_base
{
public:
    file_receiver(const char* filename, int num_blks, int blk_size);
    ~file_receiver();
    void set_data(int blk, int len, const unsigned char* buf);
    bool is_done() const;
    unsigned char percent_done() const;

protected:
    int num_blocks;
    int block_size;
    int next_block;
    FILE* fd;
    std::map<unsigned int, long> missed_blocks;

};

file_receiver::file_receiver(const char* filename, int num_blks, int blk_size)
    : num_blocks(num_blks), block_size(blk_size), next_block(0), fd(blk_var_fopen(filename, "wb"))
{
}

file_receiver::~file_receiver()
{
    if (fd)
    {
        fclose(fd);
    }
}

void file_receiver::set_data(int blk, int len, const unsigned char* buf)
{
    if (fd)
    {
        if (blk < next_block)
        {
            std::map<unsigned int, long>::iterator it = missed_blocks.find(blk);
            if (it != missed_blocks.end())
            {
                fseek(fd, (*it).second, SEEK_SET);
                fwrite(buf, 1, len, fd);
                fseek(fd, 0, SEEK_END);
                missed_blocks.erase(it);
            }
        }
        else
        {
            if (blk > next_block)
            {
                char* zbuf = new char[block_size - 1];
                memset(zbuf, 0, block_size - 1);
                for (; blk > next_block; ++next_block)
                {
                    const long pos = ftell(fd);
                    missed_blocks[next_block] = pos;
                    fwrite(zbuf, 1, block_size - block_offset(next_block), fd);
                }
                delete[] zbuf;
            }
            fwrite(buf, 1, len, fd);
            ++next_block;
        }
    }
    else
    {
        if (blk >= next_block) next_block = blk + 1;
    }
    stamp_data();
}

bool file_receiver::is_done() const
{
    return missed_blocks.empty() && (num_blocks == next_block);
}

unsigned char file_receiver::percent_done() const
{
    int num_blocks_recv = next_block - static_cast<int>(missed_blocks.size());
    return static_cast<unsigned char>((100 * num_blocks_recv) / num_blocks);
}

class mpt_var_receiver : public receiver_base
{
public:
    mpt_var_receiver(mpt_var* var, int num_blks);
    ~mpt_var_receiver();
    void set_data(int blk, int len, const unsigned char* buf);
    bool is_done() const;
    unsigned char percent_done() const;

protected:
    int num_blocks;
    mpt_var* output_var;
    std::map<unsigned int, std::string> blocks;

};

mpt_var_receiver::mpt_var_receiver(mpt_var* var, int num_blks)
    : num_blocks(num_blks), output_var(var)
{
}

mpt_var_receiver::~mpt_var_receiver()
{
    if (is_done())
    {
        int bufsz = 0;
        for (std::map<unsigned int, std::string>::iterator it = blocks.begin(); it != blocks.end(); ++it)
        {
            bufsz += static_cast<int>((*it).second.size());
        }
        unsigned char* buf = new unsigned char[bufsz];
        unsigned char* p = buf;
        for (std::map<unsigned int, std::string>::iterator it = blocks.begin(); it != blocks.end(); ++it)
        {
            const int sz = static_cast<int>((*it).second.size());
            memcpy(p, (*it).second.data(), sz);
            p += sz;
        }
        output_var->extract(bufsz, buf);
        delete[] buf;
    }
}

void mpt_var_receiver::set_data(int blk, int len, const unsigned char* buf)
{
    blocks[blk] = std::string(reinterpret_cast<const char*>(buf), len);
    stamp_data();
}

bool mpt_var_receiver::is_done() const
{
    return blocks.size() == num_blocks;
}

unsigned char mpt_var_receiver::percent_done() const
{
    return static_cast<unsigned char>((100 * blocks.size()) / num_blocks);
}

class data_receiver : public receiver_base
{
public:
    data_receiver(char* buf, int num_blks, int blk_size);
    void set_data(int blk, int len, const unsigned char* buf);
    bool is_done() const;
    unsigned char percent_done() const;

protected:
    int num_blocks;
    int block_size;
    int next_block;
    char* write_location;
    std::map<unsigned int, char*> missed_blocks;

};

data_receiver::data_receiver(char* buf, int num_blks, int blk_size)
    : num_blocks(num_blks), block_size(blk_size), next_block(0), write_location(buf)
{
}

void data_receiver::set_data(int blk, int len, const unsigned char* buf)
{
    if (blk < next_block)
    {
        std::map<unsigned int, char*>::iterator it = missed_blocks.find(blk);
        if (it != missed_blocks.end())
        {
            memcpy((*it).second, buf, len);
            missed_blocks.erase(it);
        }
    }
    else
    {
        for (; blk > next_block; ++next_block)
        {
            missed_blocks[next_block] = write_location;
            write_location += block_size - block_offset(next_block);
        }
        memcpy(write_location, buf, len);
        write_location += len;
        ++next_block;
    }
    stamp_data();
}

bool data_receiver::is_done() const
{
    return missed_blocks.empty() && (num_blocks == next_block);
}

unsigned char data_receiver::percent_done() const
{
    int num_blocks_recv = next_block - static_cast<int>(missed_blocks.size());
    return static_cast<unsigned char>((100 * num_blocks_recv) / num_blocks);
}

class dummy_reciever : public receiver_base
{
public:
    dummy_reciever(int num_blks);
    void set_data(int blk, int len, const unsigned char* buf);
    bool is_done() const;
    unsigned char percent_done() const;

protected:
    int num_blocks;
    int next_block;

};

dummy_reciever::dummy_reciever(int num_blks)
    : num_blocks(num_blks), next_block(0)
{
}

void dummy_reciever::set_data(int blk, int, const unsigned char*)
{
    if (blk >= next_block) next_block = blk + 1;
    stamp_data();
}

bool dummy_reciever::is_done() const
{
    return num_blocks == next_block;
}

unsigned char dummy_reciever::percent_done() const
{
    return static_cast<unsigned char>((100 * next_block) / num_blocks);
}

/*
 *** Senders
 */
class sender_base
{
public:
    sender_base();
    virtual ~sender_base();
    virtual void ack(int);
    virtual bool nack() = 0;
    virtual bool have_data() const = 0;
    virtual int size() const = 0;  // Excludes type field, ack block num.
    virtual void output(outbuf&) = 0;
    virtual unsigned char percent_done() const = 0;
    virtual bool ready() const;
    time_t last_ack() const;

private:
    time_t ack_timestamp;

};

sender_base::sender_base()
{
    sender_base::ack(0);
}

sender_base::~sender_base()
{
}

bool sender_base::ready() const
{
    return true;
}

void sender_base::ack(int)
{
    ack_timestamp = time(0);
}

time_t sender_base::last_ack() const
{
    return ack_timestamp;
}

class file_sender : public sender_base
{
public:
    file_sender(const char* name, const char* filepath, int blk_size);
    ~file_sender();
    void ack(int);
    bool nack();
    bool have_data() const;
    int size() const;
    void output(outbuf&);
    unsigned char percent_done() const;

private:
    int add_block_overhead(int sz) const;

protected:
    FILE* fd;
    std::map<int,std::pair<int,int> > blocks;
    std::string name;
    int num_blocks;
    mutable int next_block;
};

file_sender::file_sender(const char* nm, const char* filepath, int blk_size) :
    fd(blk_var_fopen(filepath, "rb")), name(nm), next_block(0)
{
    if (fd)
    {
        struct stat filestats;
        filestats.st_size = 0;
        fstat(__fileno(fd), &filestats);
        int left = filestats.st_size;
        int offset = 0;
        int num_blocks_bytes = 1;
        for (;;)
        {
            num_blocks = guessed_num_block(left + name.size() + 2 + num_blocks_bytes, blk_size); // 1 for null, 1 for block idx 0
            const int new_num_blocks_bytes = block_offset(num_blocks);
            if (new_num_blocks_bytes == num_blocks_bytes)
            {
                break;                                           // guessed correctly
            }
            else
            {
                num_blocks_bytes = new_num_blocks_bytes;  // guess again
            }
        }
        if (left)
        {
            int sz = blk_size - static_cast<int>(name.size()) - 2 - num_blocks_bytes; // 1 for null, 1 for block idx 0
            if (left < sz)
            {
                sz = left;
            }
            blocks[0] = std::pair<int, int>(0, sz);
            left -= sz;
            offset += sz;
        }
        for (int blkno = 1; left; ++blkno)
        {
            int sz = blk_size - block_offset(blkno);
            if (left < sz)
            {
                sz = left;
            }
            blocks[blkno] = std::pair<int, int>(offset, sz);
            left -= sz;
            offset += sz;
        }
    }
    num_blocks = static_cast<int>(blocks.size());
}

file_sender::~file_sender()
{
    if (fd)
    {
        fclose(fd);
    }
}

void file_sender::ack(int blk)
{
    std::map<int,std::pair<int, int> >::iterator it = blocks.find(blk);
    if (it != blocks.end())
    {
        blocks.erase(it);
    }
    sender_base::ack(blk);
}

bool file_sender::nack()
{
    std::map<int, std::pair<int, int> >::iterator it = blocks.find(0);
    const bool success = it != blocks.end();
    if (success)
    {
        next_block = 0;
    }
    else
    {
        blocks.clear();
    }
    return success;
}

bool file_sender::have_data() const
{
    return !blocks.empty();
}

int file_sender::add_block_overhead(int sz) const
{
    if (next_block == 0)
    {
        return sz + static_cast<int>(name.size()) + block_offset(num_blocks) + block_offset(next_block) + 1; /* for the null */
    }
    else
    {
        return sz + block_offset(next_block);
    }
}

int file_sender::size() const
{
    std::map<int,std::pair<int, int> >::const_iterator it = blocks.find(next_block);
    if (it != blocks.end())
    {
        return add_block_overhead((*it).second.second);
    }
    if (!blocks.empty())
    {
        it = blocks.begin();
        next_block = (*it).first;
        return add_block_overhead((*it).second.second);
    }
    return 0;
}

void file_sender::output(outbuf& ob)
{
    std::map<int, std::pair<int, int> >::const_iterator it = blocks.find(next_block);
    if (it != blocks.end())
    {
        vdecode_prefix_unsigned_var vpv(1.0, 0.0);
        vpv = next_block;
        vpv.output(ob);

        if (next_block == 0)
        {
            vpv = num_blocks;
            vpv.output(ob);
            for (size_t idx = 0; idx <= name.size(); ++idx)
            {
                ob += name[idx];
            }
        }

        char buf[4096];
        fseek(fd, (*it).second.first, SEEK_SET);
        const int len = static_cast<int>(fread(buf, 1, (*it).second.second, fd));
        for (int idx = 0; idx < len; ++idx)
        {
            ob += buf[idx];
        }
        ++it;
        if (it == blocks.end())
        {
            it = blocks.begin();
        }
        next_block = (*it).first;
    }
}

unsigned char file_sender::percent_done() const
{
    return static_cast<unsigned char>(100 * (num_blocks-blocks.size()) / num_blocks);
}

class data_sender : public sender_base
{
public:
    data_sender(const char* name, const char* buf, size_t len, int blk_size);
    ~data_sender();
    void ack(int);
    bool nack();
    bool have_data() const;
    int size() const;
    void output(outbuf&);
    unsigned char percent_done() const;

protected:
    std::map<int,std::string> blocks;
    int num_blocks;
    mutable int next_block;
};

data_sender::data_sender(const char* name, const char* buf, size_t len, int blk_size)
    : next_block(0)
{
    const int namelen = static_cast<int>(strlen(name)) + 1; // include null
    int num_blocks_bytes = 1;
    for (;;)
    {
        num_blocks = guessed_num_block(len + namelen + 1 + num_blocks_bytes, blk_size); // 1 for block idx 0
        const int new_num_blocks_bytes = block_offset(num_blocks);
        if (new_num_blocks_bytes == num_blocks_bytes)
        {
            break;                                           // guessed correctly
        }
        else
        {
            num_blocks_bytes = new_num_blocks_bytes;  // guess again
        }
    }
    char block_meta_buf[6];
    outbuf ob;
    ob.set(reinterpret_cast<unsigned char*>(block_meta_buf), sizeof(block_meta_buf));
    vdecode_prefix_unsigned_var vpv(1.0, 0.0);
    vpv = 0;
    vpv.output(ob);
    vpv = num_blocks;
    vpv.output(ob);

    size_t data_sz = blk_size - num_blocks_bytes - namelen - 1;
    if (len < data_sz)
    {
        data_sz = len;
        blk_size -= static_cast<int>(data_sz - len);
    }
    std::map<int,std::string>::iterator it = blocks.insert(blocks.end(), std::pair<int, std::string>(0, std::string()));
    (*it).second.resize(blk_size);
    std::string::iterator str_it = (*it).second.begin();
    for (int idx = 0; idx < static_cast<int>(ob.size()); ++idx, ++str_it)
    {
        (*str_it) = block_meta_buf[idx];
    }
    for (int idx = 0; idx < namelen; ++idx, ++str_it)
    {
        (*str_it) = name[idx];
    }
    for (size_t idx = 0; idx < data_sz; ++idx, ++str_it)
    {
        (*str_it) = *(buf + idx);
    }
    len -= data_sz;
    buf += data_sz;
    for (int blknum=1; len > 0; ++blknum)
    {
        ob.set(reinterpret_cast<unsigned char*>(block_meta_buf), sizeof(block_meta_buf));
        vpv = blknum;
        vpv.output(ob);

        data_sz = blk_size - block_offset(blknum);
        if (len < data_sz)
        {
            data_sz = len;
            blk_size = static_cast<int>(data_sz) + block_offset(blknum);
        }
        it = blocks.insert(blocks.end(), std::pair<int,std::string>(blknum, std::string()));
        (*it).second.resize(blk_size);
        str_it = (*it).second.begin();
        for (int idx = 0; idx < static_cast<int>(ob.size()); ++idx, ++str_it)
        {
            (*str_it) = block_meta_buf[idx];
        }
        for (size_t idx = 0; idx < data_sz; ++idx, ++str_it)
        {
            (*str_it) = *(buf + idx);
        }
        len -= data_sz;
        buf += data_sz;
    }
}

data_sender::~data_sender()
{
}

void data_sender::ack(int blk)
{
    std::map<int,std::string>::iterator it = blocks.find(blk);
    if (it != blocks.end())
    {
        blocks.erase(it);
    }
    sender_base::ack(blk);
}

bool data_sender::nack()
{
    std::map<int, std::string>::iterator it = blocks.find(0);
    const bool success = it != blocks.end();
    if (success)
    {
        next_block = 0;
    }
    else
    {
        blocks.clear();
    }
    return success;
}

bool data_sender::have_data() const
{
    return !blocks.empty();
}

int data_sender::size() const
{
    std::map<int,std::string>::const_iterator it = blocks.find(next_block);
    if (it != blocks.end())
    {
        return static_cast<int>((*it).second.size());
    }
    if (!blocks.empty())
    {
        it = blocks.begin();
        next_block = (*it).first;
        return static_cast<int>((*it).second.size());
    }
    return 0;
}

void data_sender::output(outbuf& ob)
{
    std::map<int,std::string>::const_iterator it = blocks.find(next_block);
    if (it != blocks.end())
    {
        for (size_t idx = 0; idx < (*it).second.size(); ++idx)
        {
            ob += (*it).second[idx];
        }
        ++it;
        if (it == blocks.end())
        {
            it = blocks.begin();
        }
        next_block = (*it).first;
    }
}

unsigned char data_sender::percent_done() const
{
    return static_cast<unsigned char>(100 * (num_blocks - blocks.size()) / num_blocks);
}

class error_sender : public sender_base
{
public:
    error_sender(const char * name);
    void ack(int);
    bool nack();
    bool have_data() const;
    int size() const;
    void output(outbuf&);
    unsigned char percent_done() const;

private:
    std::string nm;

};

error_sender::error_sender(const char * name)
    : nm(name)
{
}

void error_sender::ack(int)
{
    nm.clear();
}

bool error_sender::nack()
{
    return !nm.empty();
}

bool error_sender::have_data() const
{
    return !nm.empty();
}

int error_sender::size() const
{
    if (!nm.empty())
    {
        return static_cast<int>(nm.size()) + 3; // block_no, num blocks, name, null
    }
    return 0;
}

void error_sender::output(outbuf &ob)
{
    vdecode_prefix_unsigned_var vpv(1.0, 0.0);
    vpv = 0;
    vpv.output(ob);
    vpv = 1;
    vpv.output(ob);
    for (size_t idx = 0; idx <= nm.size(); ++idx)
    {
        ob += nm[idx];
    }
}

unsigned char error_sender::percent_done() const
{
    return (nm.empty()) ? 100 : 0;
}

#ifndef NO_EXEC
class wait_sender : public sender_base
{
public:
    wait_sender(block_var* parent, const char* name, int blk_size);
    ~wait_sender();
    bool nack();
    bool have_data() const;
    int size() const;
    void output(outbuf&);
    unsigned char percent_done() const;
    bool ready() const;

protected:
    block_var* var;
    char* nm;
    int blk_sz;
    mutable Pipe pipe;
    Program* pgm;

};

wait_sender::wait_sender(block_var* parent, const char* name, int blk_size) :
    var(parent), nm(__strdup(name)), blk_sz(blk_size)
{
    const char* nl = strchr(name, '\n');
    if (nl)
    {
        const std::string filename(name, nl - name - 1);
        pgm = new Program(filename.c_str(), nl + 1, pipe.sendHandle());
    }
    else
    {
        pgm = new Program(name, 0, pipe.sendHandle());
    }
}

wait_sender::~wait_sender()
{
    free(nm);
    delete pgm;
}

bool wait_sender::nack()
{
    return true;
}

bool wait_sender::have_data() const
{
    return true;
}

int wait_sender::size() const
{
    return 0;
}

void wait_sender::output(outbuf&)
{
}

unsigned char wait_sender::percent_done() const
{
    return 0;
}

bool wait_sender::ready() const
{
    pipe.select();
    if (pgm->finished())
    {
        pipe.select();
        const char* const pipeout = pipe.getBuffer();
        const size_t pipeoutlen = strlen(pipeout);
        const bool result = (pipeoutlen > 0);
        var->active_sender = (result) ? new data_sender(nm, pipeout, pipeoutlen, blk_sz) : 0;
        delete this;
        return result;
    }
    return false;
}
#endif
/*
 *** Factories build data_senders
 */
#ifdef _WIN32
#include <io.h>

data_sender* build_directory_data(const char* name, int blk_size)
{
    std::string wildcard(name);
    std::string buf;
    wildcard.append("\\*.*");
    struct _finddata_t fileinfo;
    intptr_t hdl = _findfirst(wildcard.c_str(), &fileinfo);
    if (hdl != -1)
    {
        buf.append(fileinfo.name);
        buf.append(1, '\n');
        while (_findnext(hdl, &fileinfo) != -1)
        {
            buf.append(1, (fileinfo.attrib && _A_SUBDIR) ? '/' : ' ');
            buf.append(fileinfo.name);
            buf.append(1, '\n');
        }
        _findclose(hdl);
    }
    else
    {
        buf = "No files\n";
    }
    return new data_sender(name, buf.data(), buf.size(), blk_size);
}
#else
#include <sys/types.h>
#include <dirent.h>

data_sender* build_directory_data(const char* name, int blk_size)
{
    std::string buf;
    DIR* directory = opendir(name);
    if (directory)
    {
        struct dirent* entry;
#if defined(__QNX__)
        struct dirent_extra* dex;
        struct dirent_extra_stat* dex_stat = NULL;

        while ((entry = readdir(directory)))
        {
            for (dex = _DEXTRA_FIRST(entry); _DEXTRA_VALID(dex, entry); dex = _DEXTRA_NEXT(dex))
            {
                if ((dex->d_type == _DTYPE_STAT) || (dex->d_type == _DTYPE_LSTAT))
                {
                    dex_stat = (struct dirent_extra_stat*) dex;
                    break;
                }
            }
            if(dex_stat!=NULL)
            {
                switch (dex_stat->d_stat.st_mode)
                {
                case S_IFDIR:
                    buf.append(1, '/');
                    buf.append(entry->d_name);
                    buf.append(1, '\n');
                    break;
                case S_IFREG:
                    buf.append(1, ' ');
                    buf.append(entry->d_name);
                    buf.append(1, '\n');
                    break;
                case S_IFLNK:
                    buf.append(1, '>');
                    buf.append(entry->d_name);
                    buf.append(1, '\n');
                    break;
                }
            }
        }
#else //non-QNX and non-Windows
        while ((entry = readdir(directory)))
        {
            switch (entry->d_type)
            {
            case DT_DIR:
                buf.append(1, '/');
                buf.append(entry->d_name);
                buf.append(1, '\n');
                break;
            case DT_REG:
                buf.append(1, ' ');
                buf.append(entry->d_name);
                buf.append(1, '\n');
                break;
            case DT_LNK:
                buf.append(1, '>');
                buf.append(entry->d_name);
                buf.append(1, '\n');
                break;
            }
        }
#endif
        closedir(directory);
    }
    else
    {
        buf = "No files\n";
    }
    return new data_sender(name, buf.data(), buf.size(), blk_size);
}
#endif

/*
 *** Block var implementation
 */
block_var::block_var() :
    block_size(90),
    send_session_id(-1),
    recv_session_id(-1),
    active_sender(0),
    active_receiver(0),
    nack(-1),
    req_block(0),
    req_block_sz(0),
    pause(false),
    triggers(0),
    recv_mapper(0),
    send_mapper(0),
    show(new every_beat_show(this)),
    block_unmapped_sends(false),
    block_unmapped_recvs(false),
    block_dir_pulls(false),
    block_cmd_pulls(false),
    timeout(20)
{
    pop();
}

block_var::~block_var()
{
    if (triggers)
    {
        for (std::list<mpt_autoshow*>::iterator it = triggers->m.begin(); it != triggers->m.end(); ++it)
        {
            delete *it;
        }
        delete triggers;
    }
    delete recv_mapper;
    delete send_mapper;
    delete active_receiver;
    delete active_sender;
    delete show;
}

int block_var::extract(int buflen, const unsigned char* buf)
{
    // sanity check
    if (buflen < 3)
    {
        return 0;
    }

    union
    {
        unsigned short mylen;
        unsigned char buf[2];
    } u;
#ifdef DECODE_BIG_ENDIAN
    u.buf[0] = buf[1];
    u.buf[1] = buf[0];
#else
    u.buf[0] = buf[0];
    u.buf[1] = buf[1];
#endif
    if (u.mylen > (buflen - 2))
    {
        return 0;
    }

    const int hasdata = buf[2] & 0x80;
    const int hasack = buf[2] & 0x40;
    int offset = 3;
    u.mylen -= 1;

    const int xxx = (buf[2] >> 3) & 0x7;

    if (hasack)
    {
        vdecode_prefix_unsigned_var vpv(1.0, 0.0);
        int retlen = vpv.extract(u.mylen, buf + offset);
        offset += u.mylen - retlen;
        u.mylen = retlen;

        const int yyy = buf[2] & 0x7;
        if (active_sender && (yyy == (send_session_id & 0x7)))
        {
            active_sender->ack(vpv.to_int());
            while (!hasdata && (xxx == 0) && (u.mylen > 0))
            {
                retlen = vpv.extract(u.mylen, buf + offset);
                offset += u.mylen - retlen;
                u.mylen = retlen;
                active_sender->ack(vpv.to_int());
            }
            // for the case when sending is complete so need to inhibit
            // dirty from propagating
            mpt_var * const prt = parent;
            parent = 0; // prevent dirty from propagating
            send_percent = active_sender->percent_done();
            parent = prt;
        }
    }

    if (hasdata)
    {
        vdecode_prefix_unsigned_var vpv(1.0, 0.0);
        const int retlen = vpv.extract(u.mylen, buf + offset);
        offset += u.mylen - retlen;
        u.mylen = retlen;

        if ((active_receiver != 0) && (xxx == recv_session_id))
        {
            const int block = vpv.to_int();
            active_receiver->set_data(block, u.mylen, buf + offset);
            ack.push(block);
            recv_percent = active_receiver->percent_done();
            if (active_receiver->is_done())
            {
                delete active_receiver;
                active_receiver = 0;
            }
        }
        else if (vpv.to_int() != 0)
        {
            if (xxx != recv_session_id)
                nack = xxx;
            // if xxx == recv_session_id, we do nothing because
            // when block 0 was received we already rejected it
            // and made a note of the recv_session_id
        }
        else
        {
            delete active_receiver;
            recv_session_id = xxx;
            const int retlen2 = vpv.extract(u.mylen, buf + offset);
            offset += u.mylen - retlen2;
            u.mylen = retlen2;

            const char *nm = reinterpret_cast<const char*>(buf + offset);
            const int dlen = static_cast<int>(strlen(nm)) + 1;
            offset += dlen;
            u.mylen -= dlen;

            if (u.mylen > 0)
            {
                // make an active receiver
                std::map<std::string, map_entry>::iterator it;
                if (recv_mapper != 0)
                {
                    it = recv_mapper->find(std::string(nm, dlen - 1));
                }

                if ((recv_mapper != 0) && (it != recv_mapper->m.end()))
                {
                    const map_entry& entry = (*it).second;
                    if (entry.isvar == 1)
                    {
                        active_receiver = new mpt_var_receiver(static_cast<mpt_var*>(entry.arg), vpv.to_int());
                    }
                    else if (entry.isvar == 2)
                    {
                        int max_total = derive_max_total_data_size(u.mylen, block_size, vpv.to_int());
                        char* rbuf = (*(entry.func))(max_total);
                        if (rbuf)
                        {
                            // for apps that benefit from unused buffer being set to zero
                            if (max_total >= block_size)
                            {
                                memset(rbuf + max_total - block_size + 1, 0, block_size - 1);
                            }
                            active_receiver = new data_receiver(rbuf, vpv.to_int(), block_size);
                        }
                        else active_receiver = new dummy_reciever(vpv.to_int());
                    }
                    else
                    {
                        const std::string filename = wildcard_eval(std::string(nm, dlen - 1), (*it).first, std::string(reinterpret_cast<const char*>(entry.arg)));
                        active_receiver = new file_receiver(filename.c_str(), vpv.to_int(), block_size);
                    }
                    if (entry.onceonly)
                    {
                        recv_mapper->m.erase(it);
                    }
                }
                else if (!block_unmapped_recvs)
                {
                    rm_var* rvar = rm_var::findVariable(nm);
                    if (rvar)
                    {
                        active_receiver = new mpt_var_receiver(static_cast<mpt_var*>(rvar), vpv.to_int());
                    }
                    else
                    {
                        active_receiver = new file_receiver(nm, vpv.to_int(), block_size);
                    }
                }
                if (active_receiver)
                {
                    active_receiver->set_data(0, u.mylen, buf + offset);
                    ack.push(0);
                    recv_name = nm;
                    recv_percent = active_receiver->percent_done();
                    recv_percent.setDirty(); // in case only have 1 block
                    if (active_receiver->is_done())
                    {
                        delete active_receiver;
                        active_receiver = 0;
                    }
                }
            }
            else
            {
                ack.push(0);
                recv_name = nm;
                recv_percent = '\xFE';
                recv_percent.setDirty(); // in case previous request also ended in error
            }
        }
    }
    else if (!active_sender || !active_sender->have_data())
    {
        delete active_sender;
        switch (xxx)
        {
        case 1: /* PULL */
        {
            active_sender = 0;
            // make an active sender
            const char* nm = reinterpret_cast<const char*>(buf) + offset;
            std::map<std::string, map_entry>::iterator it;
            if (send_mapper && ((it = send_mapper->find(std::string(nm))) != send_mapper->m.end()))
            {
                const map_entry& entry = (*it).second;
                if (entry.isvar == 1)
                {
                    push_data(nm, reinterpret_cast<mpt_var*>((*it).second.arg));
                }
                else if (entry.isvar == 2)
                {
                    int len = 0;
                    char* buf1 = (*(entry.func))(len);
                    if (len > 0)
                    {
                        push_data(nm, buf1, len);
                    }
                }
                else
                {
                    const std::string filename = wildcard_eval(nm, (*it).first, std::string(reinterpret_cast<const char*>((*it).second.arg)));
                    push_data(nm, filename.c_str());
                }
                if (entry.onceonly)
                {
                    send_mapper->m.erase(it);
                }
            }
            else if (!block_unmapped_sends)
            {
                rm_var* rvar = rm_var::findVariable(nm);
                if (rvar)
                {
                    push_data(nm, rvar);
                }
                else
                {
                    push_data(nm, nm);
                }
            }
            break;
        }
        case 2: /* DIR */
        {
            if (!block_dir_pulls)
            {
                const char* nm = reinterpret_cast<const char*>(buf) + offset;
                active_sender = build_directory_data(nm, block_size);
                ++send_session_id;
                send_name = nm;
                send_percent = 0;
            }
            break;
        }
        case 3: /* CMD */
        {
            if (!block_cmd_pulls)
            {
#ifndef NO_EXEC
                const char* nm = reinterpret_cast<const char*>(buf) + offset;
                active_sender = new wait_sender(this, nm, block_size);
                ++send_session_id;
                send_name = nm;
                send_percent = 0;
#endif
            }
            break;
        }
        case 5: /* SHA1 */
            /* TODO */
            active_sender = 0;
            break;
        default:
            active_sender = 0;
            break;
        }
        if ((xxx != 0) && (active_sender == 0))
        {
            // creation of a new sender failed
            const char* nm = reinterpret_cast<const char*>(buf) + offset;
            active_sender = new error_sender(nm);
            ++send_session_id;
        }
    }
    else if (xxx == 4)
    {
        const int yyy = buf[2] & 0x7;
        if (yyy == (send_session_id & 0x7))
        {
            const bool ok = active_sender->nack();
            if (ok)
            {
                send_percent = 0;
            }
            else
            {
                delete active_sender;
                active_sender = 0;
                mpt_var * const prt = parent;
                parent = 0; // prevent dirty from propagating
                send_percent = '\xFE'; // indicate error
                parent = prt;
            }
        }
    }
    return 0;
}

void block_var::output(outbuf& strm)
{
#ifdef DECODE_BIG_ENDIAN
    r_generic_var<unsigned short> len;
#else
    generic_var<unsigned short> len;
#endif
    len = static_cast<unsigned short>(size() - 2);
    len.output(strm);

    if (nack != -1)
    {
        // NAK has highest priority
        const unsigned char uch = 0x20 + nack;
        strm += uch;
        // Clear NAK
        nack = -1;
        return;
    }
    int sz_ack_block_no = 0;
    unsigned long long buffer;
    const bool have_ack = !ack.isEmpty();
    if (have_ack)
    {
        vdecode_prefix_unsigned_var vpv(1.0, 0.0);
        vpv = static_cast<double>(ack.pop());
        outbuf ob;
        ob.set(reinterpret_cast<unsigned char*>(&buffer), sizeof(buffer));
        vpv.output(ob);
        sz_ack_block_no = ob.size();
    }
    if (req_block)
    {
        // Request have 2nd highest priority
        unsigned char uch = static_cast<unsigned char>(req_block[0]);
        if (have_ack)
        {
            // insert ack session, flag and block
            uch += recv_session_id & 0x7;
            uch |= 0x40;
            strm += uch;
            const unsigned char* st = reinterpret_cast<unsigned char*>(&buffer);
            const unsigned char* const ed = st + sz_ack_block_no;
            for (; st != ed; ++st)
            {
                strm += *st;
            }
        }
        else
        {
            strm += uch;
        }
        for (int cnt = 1; cnt < req_block_sz; ++cnt)
        {
            strm += static_cast<unsigned char>(req_block[cnt]);
        }
        // Clear request
        delete[] req_block;
        req_block = 0;
        req_block_sz = 0;
        return;
    }
    if (active_sender && active_sender->ready())
    {
        // Data have 3rd highest priority
        unsigned char uch = ((send_session_id & 0x7) << 3) | 0x80;
        if (have_ack)
        {
            // insert ack session, flag and block
            uch += recv_session_id & 0x7;
            uch |= 0x40;
            strm += uch;
            const unsigned char* st = reinterpret_cast<unsigned char*>(&buffer);
            const unsigned char* const ed = st + sz_ack_block_no;
            for (; st != ed; ++st)
            {
                strm += *st;
            }
        }
        else
        {
            strm += uch;
        }
        active_sender->output(strm);
        return;
    }
    if (have_ack)
    {
        // Only ack
        // insert ack session, flag and block
        unsigned char uch = recv_session_id & 0x7;
        uch |= 0x40;
        strm += uch;
        const unsigned char* st = reinterpret_cast<unsigned char*>(&buffer);
        const unsigned char* const ed = st + sz_ack_block_no;
        for (; st != ed; ++st)
        {
            strm += *st;
        }
        // pack anymore outstanding acks
        for (int ack_sz = sz_ack_block_no; !ack.isEmpty() && ((ack_sz + sz_ack_block_no) < block_size); ack_sz += sz_ack_block_no)
        {
            vdecode_prefix_unsigned_var vpv(1.0, 0.0);
            vpv = static_cast<double>(ack.pop());
            outbuf ob;
            ob.set(reinterpret_cast<unsigned char*>(&buffer), sizeof(buffer));
            vpv.output(ob);
            sz_ack_block_no = ob.size();
            const unsigned char* st1 = reinterpret_cast<unsigned char*>(&buffer);
            const unsigned char* const ed1 = st1 + sz_ack_block_no;
            for (; st1 != ed1; ++st1)
            {
                strm += *st1;
            }
        }
        return;
    }
}

int block_var::size() const
{
    if (nack != -1)
    {
        // NAK has highest priority
        return 1 + 2;
    }
    int sz_ack_block_no = 0;
    const bool have_ack = !ack.isEmpty();
    if (have_ack)
    {
        vdecode_prefix_unsigned_var vpv(1.0, 0.0);
        vpv = static_cast<double>(queue_element(&ack, ack.reader));
        unsigned long long buffer;
        outbuf ob;
        ob.set(reinterpret_cast<unsigned char*>(&buffer), sizeof(buffer));
        vpv.output(ob);
        sz_ack_block_no = ob.size();
    }
    if (req_block)
    {
        // Request have 2nd highest priority
        return req_block_sz + sz_ack_block_no + 2;
    }
    if (active_sender && active_sender->ready())
    {
        // Data have 3rd highest priority
        return active_sender->size() + 1 /* flag */ + sz_ack_block_no + 2 /* array_var size */;
    }
    if (have_ack)
    {
        // Only ack
        int ack_sz = sz_ack_block_no;
        // pack anymore outstanding acks
        int peek_reader = block_var_queue_advance(ack.reader);
        for (; (peek_reader != ack.writer) && ((ack_sz + sz_ack_block_no) < block_size); ack_sz += sz_ack_block_no)
        {
            vdecode_prefix_unsigned_var vpv(1.0, 0.0);
            vpv = static_cast<double>(queue_element(&ack, peek_reader));
            unsigned long long buffer;
            outbuf ob;
            ob.set(reinterpret_cast<unsigned char*>(&buffer), sizeof(buffer));
            vpv.output(ob);
            sz_ack_block_no = ob.size();
            peek_reader = block_var_queue_advance(peek_reader);
        }

        return 1 + ack_sz  + 2;
    }

    // Completely nothing to send, shouldn't even get here
    return 2;
}

void block_var::setRMDirty()
{
}

bool block_var::isValid() const
{
    return active_receiver || active_sender;
}

bool block_var::setInvalid()
{
    const bool ret = active_receiver || active_sender;
    send_percent.setInvalid();
    recv_percent.setInvalid();
    send_name.setInvalid();
    recv_name.setInvalid();
    send_session_id = -1;
    recv_session_id = -1;
    delete active_sender;
    active_sender = 0;
    delete active_receiver;
    active_receiver = 0;
    nack = -1;
    delete[] req_block;
    req_block = 0;
    req_block_sz = 0;
    ack.clear();
    pause = false;
    return ret;
}

void block_var::pause_sending(bool p)
{
    pause = p;
}

void block_var::set_block_size(int b)
{
    block_size = b;
}

void block_var::regulate_sending_by_count(int sk)
{
    delete show;
    show = new skip_beat_show(this, sk);
}

void block_var::regulate_sending_by_time(unsigned int sec, unsigned int usec)
{
    delete show;
    show = new interval_show(this, sec, usec);
}

void block_var::set_block_unmapped_sends(bool blk)
{
    block_unmapped_sends = blk;
}

void block_var::set_block_unmapped_recvs(bool blk)
{
    block_unmapped_recvs = blk;
}

void block_var::set_block_dir_pulls(bool blk)
{
    block_dir_pulls = blk;
}

void block_var::set_block_cmd_pulls(bool blk)
{
    block_cmd_pulls = blk;
}

void block_var::set_timeout(int to)
{
    timeout = to;
}

bool block_var::push_data(const char* name, const char* buf, size_t len)
{
    if (active_sender && active_sender->have_data())
    {
        return false;
    }
    delete active_sender;
    active_sender = new data_sender(name, buf, len, block_size);
    ++send_session_id;
    send_name = name;
    send_percent = 0;
    return true;
}

bool block_var::push_data(const char* name, mpt_var* var)
{
    const int sz = var->size();
    char* buf = new char[sz];
    outbuf ob;
    ob.set(reinterpret_cast<unsigned char*>(buf), sz);
    var->output(ob);
    const bool ret = push_data(name, buf, sz);
    delete[] buf;
    return ret;
}

bool block_var::push_data(const char* name, const char* filepath)
{
    if (active_sender && active_sender->have_data())
    {
        return false;
    }
    delete active_sender;
    active_sender = new file_sender(name, filepath, block_size);
    if (active_sender->have_data())
    {
        ++send_session_id;
        send_name = name;
        send_percent = 0;
        return true;
    }
    else
    {
        delete active_sender; /* file could not be loaded */
        active_sender = 0;
        return false;
    }
}

void block_var::push_data_on_trigger(const char* name, mpt_var* var)
{
    if (!triggers)
    {
        triggers = new trigger_list;
    }
    triggers->m.push_back(new trigger_on_var_show(this, name, var, &block_var::push_data));
}

void block_var::push_data_on_pull(const char* name, const char* filepath, bool onceonly)
{
    if (!send_mapper)
    {
        send_mapper = new data_mapping;
    }
    send_mapper->m[std::string(name)].set_filename(filepath, onceonly);
}

void block_var::push_data_on_pull(const char* name, mpt_var* var, bool onceonly)
{
    if (!send_mapper)
    {
        send_mapper = new data_mapping;
    }
    send_mapper->m[std::string(name)].set_var(var, onceonly);
}

void block_var::push_data_on_pull(const char* name, block_var_callback cb, bool onceonly)
{
    if (!send_mapper)
    {
        send_mapper = new data_mapping;
    }
    send_mapper->m[std::string(name)].set_callback(cb, onceonly);
}

bool block_var::pull_data(const char* name, const char* filepath)
{
    if (req_block)
    {
        return false;
    }

    const size_t sz = strlen(name) + 1; // include the null
    req_block_sz = static_cast<int>(sz) + 1;
    req_block = new char[req_block_sz];
    memcpy(req_block + 1, name, sz);
    req_block[0] = '\x8';
    recv_data(name, filepath, true);
    return true;
}

bool block_var::pull_data(const char* name, mpt_var* var)
{
    if (req_block)
    {
        return false;
    }

    const size_t sz = strlen(name) + 1; // include the null
    req_block_sz = static_cast<int>(sz) + 1;
    req_block = new char[req_block_sz];
    memcpy(req_block + 1, name, sz);
    req_block[0] = '\x8';
    recv_data(name, var, true);
    return true;
}

bool block_var::pull_data(const char* name, block_var_callback cb)
{
    if (req_block)
    {
        return false;
    }

    const size_t sz = strlen(name) + 1; // include the null
    req_block_sz = static_cast<int>(sz) + 1;
    req_block = new char[req_block_sz];
    memcpy(req_block + 1, name, sz);
    req_block[0] = '\x8';
    recv_data(name, cb, true);
    return true;
}

bool block_var::get_dir(const char* name, mpt_var* var)
{
    if (req_block)
    {
        return false;
    }

    const size_t sz = strlen(name) + 1; // include the null
    req_block_sz = static_cast<int>(sz) + 1;
    req_block = new char[req_block_sz];
    memcpy(req_block + 1, name, sz);
    req_block[0] = '\x10';
    recv_data(name, var, true);
    return true;
}

bool block_var::exec_cmd(const char* name, mpt_var* var)
{
    if (req_block)
    {
        return false;
    }

    const size_t sz = strlen(name) + 1; // include the null
    req_block_sz = static_cast<int>(sz) + 1;
    req_block = new char[req_block_sz];
    memcpy(req_block + 1, name, sz);
    req_block[0] = '\x18';
    recv_data(name, var, true);
    return true;
}

bool block_var::get_sha1(const char* name, mpt_var* var)
{
    if (req_block)
    {
        return false;
    }

    const size_t sz = strlen(name) + 1; // include the null
    req_block_sz = static_cast<int>(sz) + 1;
    req_block = new char[req_block_sz];
    memcpy(req_block + 1, name, sz);
    req_block[0] = '\x28';
    recv_data(name, var, true);
    return true;
}

void block_var::recv_data(const char* name, const char* filepath, bool onceonly)
{
    if (!recv_mapper)
    {
        recv_mapper = new data_mapping;
    }
    recv_mapper->m[std::string(name)].set_filename(filepath, onceonly);
}

void block_var::recv_data(const char* name, mpt_var* var, bool onceonly)
{
    if (!recv_mapper)
    {
        recv_mapper = new data_mapping;
    }
    recv_mapper->m[std::string(name)].set_var(var, onceonly);
}

void block_var::recv_data(const char* name, block_var_callback cb, bool onceonly)
{
    if (!recv_mapper)
    {
        recv_mapper = new data_mapping;
    }
    recv_mapper->m[std::string(name)].set_callback(cb, onceonly);
}

void block_var::pull_data_on_trigger(mpt_var* name, mpt_var* var)
{
    if (!triggers)
    {
        triggers = new trigger_list;
    }
    triggers->m.push_back(new trigger_on_name_show(this, name, var, &block_var::pull_data));
}

void block_var::get_dir_on_trigger(mpt_var* name, mpt_var* var)
{
    if (!triggers)
    {
        triggers = new trigger_list;
    }
    triggers->m.push_back(new trigger_on_name_show(this, name, var, &block_var::get_dir));
}

void block_var::exec_cmd_on_trigger(mpt_var* name, mpt_var* var)
{
    if (!triggers)
    {
        triggers = new trigger_list;
    }
    triggers->m.push_back(new trigger_on_name_show(this, name, var, &block_var::exec_cmd));
}

void block_var::get_sha1_on_trigger(mpt_var* names, mpt_var* var)
{
    if (!triggers)
    {
        triggers = new trigger_list;
    }
    triggers->m.push_back(new trigger_on_name_show(this, names, var, &block_var::get_sha1));
}

void block_var::processing()
{
    if ((nack != -1) || (!ack.isEmpty()) || (req_block))
    {
        if (parent)
        {
            setDirty();
            parent->setRMDirty();
        }
    }
    else if (active_sender)
    {
        if (active_sender->have_data())
        {
            if ((time(0) - active_sender->last_ack()) > timeout)
            {
                delete active_sender;
                active_sender = 0;
                mpt_var * const prt = parent;
                parent = 0; // prevent dirty from propagating
                send_percent = '\xFE'; // indicate error
                parent = prt;
            }
            else if (!pause && parent)
            {
                setDirty();
                parent->setRMDirty();
            }
        }
        else
        {
            delete active_sender;
            active_sender = 0;
        }
    }
    if (active_receiver &&
        (time(0) - active_receiver->last_data() > timeout))
    {
        delete active_receiver;
        active_receiver = 0;
        mpt_var * const prt = parent;
        parent = 0; // prevent dirty from propagating
        recv_percent = '\xFE'; // indicate error
        parent = prt;
    }
}

mpt_var* block_var::getNext()
{
    return this + 1;
}

const mpt_var& block_var::operator=(const mpt_var& right)
{
    setInvalid();

    const block_var* gen = RECAST(const block_var*, &right);
    if (gen)
    {
        block_size = gen->block_size;
    }
    return *this;
}

bool block_var::operator==(const mpt_var&) const
{
    return false;
}

istream& block_var::operator>> (istream& s)
{
    setInvalid();
    s >> block_size;
    return s;
}

ostream& block_var::operator<<(ostream& s) const
{
    s << block_size;
    return s;
}

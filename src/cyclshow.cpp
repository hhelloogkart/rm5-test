/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "cyclshow.h"

cycle_show::cycle_show(unsigned int _sec, unsigned int _usec) :
    delay_show(_sec, _usec)
{
    unsigned int nsec = 0, nusec = 0;
    getTime(nsec, nusec);
    trigger_sec = nsec + _sec;
    trigger_usec = nusec + _usec;
    if (trigger_usec >= 1000000)
    {
        ++(trigger_sec);
        trigger_usec -= 1000000;
    }
}

void cycle_show::setDirty(mpt_var*)
{
    getTime(trigger_sec, trigger_usec);
}

bool cycle_show::getDirty()
{
    unsigned int _sec = 0, _usec = 0;
    getTime(_sec, _usec);
    bool dirty = false;
    while ((_sec > trigger_sec) ||
           ((_sec == trigger_sec) && (_usec > trigger_usec)))
    {
        trigger_sec += sec;
        trigger_usec += usec;
        if (trigger_usec >= 1000000)
        {
            ++(trigger_sec);
            trigger_usec -= 1000000;
        }
        dirty = true;
    }
    return dirty;
}

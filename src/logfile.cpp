#include "logfile.h"
#include "outbuf.h"
#include <stdio.h>
#include <string.h>
#include <time/timecore.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#ifdef WIN32
#include <io.h>
#include <share.h>
#else
#include <stddef.h>
#endif

static const char hexstr[] = "0123456789ABCDEF";
static VarLogFile* p_logfile = 0;

inline unsigned long long getCurrentTime()
{
    struct timeval tv;
    now(&tv);
    return static_cast<unsigned long long>(tv.tv_sec) * 1000 + tv.tv_usec / 1000;
}

void VarLogFile::enable(const char* filename)
{
    p_logfile = new VarLogFile(filename);
}
void VarLogFile::disable()
{
    delete p_logfile;
    p_logfile = 0;
}

VarLogFile::VarLogFile(const char* filename) : counter(0), lastflush(0)
{
    if (strstr(filename, "%d")==0)
    {
#ifdef WIN32
        fopen_s((FILE**)&log, filename, "w");
#else
        log = fopen(filename, "w");
#endif
    }
    else
    {
        log = 0;
        for (int idx=0; log == 0; ++idx)
        {
            char nfilename[256];
            int fh;
#ifdef WIN32
            sprintf_s(nfilename, sizeof(nfilename), filename, idx);
            const errno_t err = _sopen_s(&fh, nfilename, _O_CREAT|_O_EXCL|_O_WRONLY|_O_SEQUENTIAL|_O_TEXT, SH_DENYWR, _S_IREAD|_S_IWRITE);
            if (err==EEXIST || err==EACCES)
            {
                continue;
            }
            if (err==0)
            {
                log = _fdopen(fh, "w");
            }
            break;
#else
            sprintf(nfilename, filename, idx);
            fh = open(nfilename, O_CREAT | O_EXCL | O_WRONLY, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
            if ((fh == -1) && (errno==EEXIST || errno==EACCES))
            {
                continue;
            }
            if (fh != -1)
            {
                log = fdopen(fh, "w");
            }
            break;
#endif
        }
    }
    if (log)
    {
        const char firstline[] = "# DSO Datamon Log File v1.0\n";
        timestamp = getCurrentTime();
        fwrite(firstline, 1, sizeof(firstline)-1, (FILE*)log);
        fprintf((FILE*)log, "T %llu\n", timestamp);
    }
}

VarLogFile::~VarLogFile()
{
    if (log)
    {
        const char lastline[] = "E\n";
        fwrite(lastline, 1, sizeof(lastline)-1, (FILE*)log);
        fclose((FILE*)log);
    }
}

void VarLogFile::logData(int len, const unsigned char* buf, const char* name, unsigned int rtti)
{
    const unsigned long long offset = getCurrentTime() - timestamp;
    fprintf((FILE*)log, "%llu %llu %s %u ", counter, offset, name, rtti);
    ++counter;
    const int dlen = len*2+1;
    char* data = new char[dlen], * pc = data;
    for (int idx=0; idx<len; ++idx)
    {
        const unsigned char uc = buf[idx];
        *pc = hexstr[uc >> 4];
        ++pc;
        *pc = hexstr[uc & 0xF];
        ++pc;
    }
    *pc = '\n';
    fwrite(data, 1, dlen, (FILE*)log);
    delete[] data;

    if (offset - lastflush > 1000)
    {
        fflush((FILE*)log);
        lastflush = offset;
    }
}

RM_EXPORT void vld1(int len, const unsigned char* buf, const char* nm, unsigned int rtti)
{
    if (p_logfile)
    {
        p_logfile->logData(len, buf, nm, rtti);
    }
}

class RM_EXPORT vld2
{
public:
    vld2(outbuf& ob, const char* nm, unsigned int rtti);
    ~vld2();

private:
    unsigned char* p;
    const char* nm;
    unsigned int rtti;
    outbuf& ob;
};

vld2::vld2(outbuf& ob_, const char* nm_, unsigned int rtti_)
    : p(ob_.getcur()), nm(nm_), rtti(rtti_), ob(ob_)
{
}

vld2::~vld2()
{
    if (p_logfile)
    {
        const ptrdiff_t len = ob.getcur() - p;
        p_logfile->logData(static_cast<int>(len), p, nm, rtti);
    }
}

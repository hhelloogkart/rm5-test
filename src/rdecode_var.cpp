/******************************************************************/
/* Copyright DSO National Laboratories 2016. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "rdecode_var.h"

inline static int byte_size_to_code_typ(int bsz)
{
    if (bsz < 0)
    {
        return 0x500 | (-bsz*8 & 0xff);
    }
    else
    {
        return 0x200 | (bsz*8 & 0xff);
    }
}

rdecode_var::rdecode_var(int _byte_size, double _range, double _offset, int _scale_offset) :
    vdecode_var(byte_size_to_code_typ(_byte_size), _range, _offset, _scale_offset)
{
}

const mpt_var& rdecode_var::operator=(const mpt_var& right)
{
    return vdecode_var::operator=(right);
}

double rdecode_var::operator=(double right)
{
    return vdecode_var::operator=(right);
}

const rdecode_var& rdecode_var::operator=(const rdecode_var& right)
{
    vdecode_var::operator=(right);
    return *this;
}
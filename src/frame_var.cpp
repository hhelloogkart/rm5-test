/******************************************************************/
/* Copyright DSO National Laboratories 2008. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "frame_var.h"
#include "mpt_show.h"
#include <outbuf.h>
#include <assert.h>
#include <set>
#include <string.h>

using std::set;

class frame_var_private_show : public mpt_baseshow
{
public:
    frame_var_private_show(frame_var* var);
    virtual ~frame_var_private_show();
    virtual void setDirty(mpt_var* var);
    virtual void operator()();
    void setEnabled(bool enable);
    int extract(int len, const unsigned char* buf);
    void output(outbuf& strm);

protected:
    frame_var* obj;
    unsigned int frame_sz;
    unsigned int data_sz;
    unsigned char num_sub_frames;
    unsigned char sub_frame_no;
    unsigned char frame_no;
    unsigned char last_rx_frame_no;
    bool enb;
    bool dirty;
    bool latch_dirty;
    unsigned char* obuf;
    unsigned char* ibuf;
    set<unsigned char> iset;
};

frame_var_private_show::frame_var_private_show(frame_var* var) :
    obj(var), frame_sz(var->size()), data_sz(var->data_size()),
    num_sub_frames(0), sub_frame_no(num_sub_frames), frame_no(0), last_rx_frame_no(0),
    enb(true), dirty(false), latch_dirty(false), obuf(0), ibuf(0)
{
    unsigned sz = frame_sz - 2;
    assert(((data_sz + sz - 1) / sz) < 256);
    num_sub_frames = (data_sz + sz - 1) / sz;
    obuf = new unsigned char[data_sz];
    ibuf = new unsigned char[num_sub_frames * sz];
    var->addCallback(this);
}

frame_var_private_show::~frame_var_private_show()
{
    delete[] obuf;
    delete[] ibuf;
}

void frame_var_private_show::setDirty(mpt_var*)
{
    if (enb)
    {
        latch_dirty = dirty = true;
    }
}

void frame_var_private_show::operator()()
{
    if (!obj->isRMDirty())
    {
        if (sub_frame_no >= num_sub_frames)
        {
            latch_dirty = dirty;
            dirty = false;
        }
        if (latch_dirty)
        {
            obj->setRMDirty();
        }
    }
}

void frame_var_private_show::setEnabled(bool enable)
{
    enb = enable;
}

int frame_var_private_show::extract(int len, const unsigned char* buf)
{
    if (len >= (int)frame_sz)
    {
        unsigned char fr_num = buf[0];
        unsigned char sub_fr_num = buf[1];
        unsigned int sz = frame_sz - 2;

        if(fr_num != last_rx_frame_no)
        {
            last_rx_frame_no = fr_num;
            iset.clear();
        }
        if (sub_fr_num < num_sub_frames)
        {
            memcpy(ibuf + sub_fr_num * sz,  buf + 2, len < (int)sz ? (size_t)len : sz);
            iset.insert(sub_fr_num);
        }
        if ((sub_fr_num == num_sub_frames - 1) && (iset.size() == num_sub_frames))
        {
            setEnabled(false);
            ((complex_var*)obj)->complex_var::extract(data_sz, ibuf);
            setEnabled(true);
            iset.clear();
        }
        len -= frame_sz;
    }
    return len;
}

void frame_var_private_show::output(outbuf& strm)
{
    if (sub_frame_no >= num_sub_frames)
    {
        sub_frame_no = 0;
    }
    if (sub_frame_no == 0)
    {
        outbuf buf;
        buf.set(obuf, data_sz);
        ((complex_var*)obj)->complex_var::output(buf);
        ++frame_no;
    }
    strm += frame_no;
    strm += sub_frame_no;
    unsigned sz = frame_sz - 2;
    if (sub_frame_no  == num_sub_frames - 1)
    {
        unsigned left = data_sz - (sub_frame_no * sz);
        strm.append(obuf + sub_frame_no * sz, left);
        left = sz - left;
        while(left-- > 0)
        {
            strm += '\0';
        }
    }
    else
    {
        strm.append(obuf + sub_frame_no * sz, sz);
    }
    ++sub_frame_no;
}

frame_var::frame_var(unsigned int _frame_sz) : complex_var(),
    frame_sz(_frame_sz), shw(0)
{
    assert(frame_sz > 2);
}

frame_var::frame_var(const frame_var& right) : complex_var()
{
    (*this) = right;
}

frame_var::~frame_var()
{
    delete shw;
}

int frame_var::extract(int len, const unsigned char* buf)
{
    if (shw)
    {
        return shw->extract(len, buf);
    }
    return len;
}

void frame_var::output(outbuf& strm)
{
    shw->output(strm);
}

int frame_var::size() const
{
    return frame_sz;
}

unsigned int frame_var::data_size() const
{
    return (unsigned int)complex_var::size();
}

const mpt_var& frame_var::operator=(const mpt_var& right)
{
    const frame_var* gen = RECAST(const frame_var*,&right);
    if (gen)
    {
        frame_var::operator=(* gen);
    }
    return *this;
}

const frame_var& frame_var::operator=(const frame_var& right)
{
    complex_var::operator = ((const complex_var&)right);
    frame_sz = right.frame_sz;
    return *this;
}

bool frame_var::operator==(const mpt_var& right) const
{
    const frame_var* gen = RECAST(const frame_var*,&right);
    if (gen)
    {
        return (*this) == *gen;
    }
    return complex_var::operator==(right);
}

bool frame_var::operator==(const frame_var& right) const
{
    return complex_var::operator==((const complex_var&)right);
}

void frame_var::init_child_observers()
{
    delete shw;
    shw = new frame_var_private_show(this);
}

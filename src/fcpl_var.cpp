/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include <fcpl_var.h>
#include <outbuf.h>
#include <string.h>

flex_complex_var::flex_complex_var() : complex_var()
{
    pop();
}
flex_complex_var::~flex_complex_var()
{
    delete_children();
}
int flex_complex_var::extract(int buflen, const unsigned char* buf)
{
    int car, cnt1, tmpsz = buflen;

    // extract the cardinal by calling a function
    buflen = extract_cardinal(buflen, buf, car);
    buf += tmpsz - buflen;

    // check for cardinal change
    if (cardinal() != car)
    {
        setDirty();
    }
    else
    {
        notDirty();
    }

    // remove excess elements
    // this will set the flex_complex_var dirty for sure
    for (cnt1 = cardinal(); (cnt1 > 0) && (cnt1 > car); --cnt1)
    {
        complex_var::remove(cnt1-1);
        setDirty();
    }

    // extract into elements that already exist
    // this will set dirty or not dirty depending on its children
    for (cnt1 = 0; (buflen > 0) && (cnt1 < cardinal()); ++cnt1)
    {
        tmpsz = buflen;
        buflen = (*this)[cnt1]->extract(buflen, buf);
        buf += tmpsz - buflen;
    }

    // extract new elements
    // this will set dirty for sure
    if ((cnt1 < car) && (car > 0))
    {
        mpt_var* tmp;
        push();
        while ((cnt1 < car) && (buflen > 0))
        {
            tmp = create();
            tmpsz = buflen;
            buflen = tmp->extract(buflen, buf);
            buf += tmpsz - buflen;
            ++cnt1;
        }
        pop();
    }

    return buflen;
}
int flex_complex_var::extract_cardinal(int buflen, const unsigned char* buf, int& car)
{
    // this function is identical to the one in array_value_var
    if (buflen < size_cardinal())
    {
        car = 0;
        return 0;
    }

    union
    {
        unsigned long long vll;
        unsigned long vl;
        unsigned short vs;
        unsigned char vc;
        char c[8];
    } ucar;
    int wd = cardinal_width();
    if (wd>0)
    {
        memcpy(ucar.c, buf+cardinal_offset(), wd);
    }
    else if (wd<0)
    {
        buf += cardinal_offset();
        for (int cnt= -wd-1; cnt>=0; --cnt, ++buf)
        {
            ucar.c[cnt] = *buf;
        }
    }
    switch (wd)
    {
    case -8:
    case 8:
        car = static_cast<int>(ucar.vll);
        break;
    case -4:
    case 4:
        car = ucar.vl;
        break;
    case -2:
    case 2:
        car = ucar.vs;
        break;
    case -1:
    case 1:
        car = ucar.vc;
        break;
    default: // includes case 0
        car = buflen/width();
        break;
    }
    return buflen-size_cardinal();
}

int flex_complex_var::size() const
{
    return cardinal() ? size_cardinal() + complex_var::size() : size_cardinal();
}
void flex_complex_var::output_cardinal(outbuf& strm)
{
    // this function is identical to the one in array_value_var
    const int wd = cardinal_width();
    const int of = cardinal_offset();
    int ct = 0;
    const int sz = size_cardinal();
    union
    {
        unsigned long long vll;
        unsigned long vl;
        unsigned short vs;
        unsigned char vc;
        char c[8];
    } car;
    switch (wd)
    {
    case -8:
    case 8:
        car.vll = cardinal();
        break;
    case -4:
    case 4:
        car.vl = cardinal();
        break;
    case -2:
    case 2:
        car.vs = cardinal();
        break;
    case -1:
    case 1:
        car.vc = cardinal();
        break;
    default: /* includes case 0 */
        break;
    }

    // apply offset via padding
    for (int cnt=0; (cnt<of) && (ct<sz); ++cnt, ++ct)
    {
        strm += '\0';
    }

    // apply cardinal
    if (wd > 0)
    {
        for (int cnt=0; (cnt<wd) && (ct<sz); ++cnt, ++ct)
        {
            strm += car.c[cnt];
        }
    }
    else if (wd < 0)
    {
        for (int cnt=-wd-1; (cnt>=0) && (ct<sz); --cnt, ++ct)
        {
            strm += car.c[cnt];
        }
    }

    // apply padding
    for (; ct<sz; ++ct)
    {
        strm += '\0';
    }
}
void flex_complex_var::output(outbuf& strm)
{
    output_cardinal(strm);
    complex_var::output(strm);
}
bool flex_complex_var::isValid() const
{
    return cardinal() > 0;
}
bool flex_complex_var::setInvalid()
{
    if (isValid())
    {
        delete_children();
        setDirty();
        return true;
    }
    notDirty();
    return false;
}
mpt_var* flex_complex_var::getNext()
{
    return this + 1;
}
const mpt_var& flex_complex_var::operator=(const mpt_var& right)
{
    const flex_complex_var* _r = RECAST(const flex_complex_var*,&right);
    int cnt1, max1;

    if (_r)
    {
        max1 = _r->cardinal();
        cnt1 = cardinal();

        if (max1 != cnt1)
        {
            setDirty();
            setRMDirty();
        }
        else
        {
            notDirty();
        }

        // remove excess elements
        for (; (cnt1 > 0) && (cnt1 > max1); --cnt1)
        {
            remove(cnt1-1);
        }

        // assign existing elements
        for (cnt1 = 0; cnt1 < cardinal(); ++cnt1)
        {
            *(*this)[cnt1] = *(*_r)[cnt1];
        }

        // add new elements
        if (cnt1 < max1)
        {
            push();
            while (cnt1 < max1)
            {
                create((*_r)[cnt1]);
                ++cnt1;
            }
            pop();
        }
    }
    return *this;
}
const flex_complex_var& flex_complex_var::operator=(const flex_complex_var& right)
{
    const int max1 = right.cardinal();
    int cnt1 = cardinal();

    if (max1 != cnt1)
    {
        setDirty();
        setRMDirty();
    }
    else
    {
        notDirty();
    }

    // remove excess elements
    for (; (cnt1 > 0) && (cnt1 > max1); --cnt1)
    {
        remove(cnt1 - 1);
    }

    // assign existing elements
    for (cnt1 = 0; cnt1 < cardinal(); ++cnt1)
    {
        *(*this)[cnt1] = *right[cnt1];
    }

    // add new elements
    if (cnt1 < max1)
    {
        push();
        while (cnt1 < max1)
        {
            create(right[cnt1]);
            ++cnt1;
        }
        pop();
    }
    return *this;
}
bool flex_complex_var::operator==(const mpt_var& right) const
{
    const flex_complex_var* _r = RECAST(const flex_complex_var*,&right);
    int cnt1, max1 = cardinal();
    mpt_var* tmp, * tmp1;
    if (_r)
    {
        if ((int)_r->cardinal() != max1)
        {
            return false;
        }
        for (cnt1 = 0; cnt1 < max1; ++cnt1)
        {
            tmp  = (*_r)[cnt1];
            tmp1 = (*this)[cnt1];
            if (*tmp != *tmp1)
            {
                return false;
            }
        }
        return true;
    }
    return complex_var::operator==(right);
}
istream& flex_complex_var::operator>>(istream& s)
{
    int max1=0, cnt1 = cardinal();
    s >> max1;

    if (max1 != cnt1)
    {
        setDirty();
        setRMDirty();
    }
    else
    {
        notDirty();
    }

    // remove excess elements
    for (; (cnt1 > 0) && (cnt1 > max1); --cnt1)
    {
        remove(cnt1-1);
    }

    // insert into existing elements
    for (cnt1 = 0; cnt1 < cardinal(); ++cnt1)
    {
        (*this)[cnt1]->operator>>(s);
    }

    // add new elements
    if (cnt1 < max1)
    {
        push();
        while (cnt1 < max1)
        {
            mpt_var* tmp = create();
            tmp->operator>>(s);
            ++cnt1;
        }
        pop();
    }

    return s;
}
ostream& flex_complex_var::operator<<(ostream& s) const
{
    int cnt1, max1 = cardinal();
    s << max1;
    for (cnt1 = 0; cnt1 < max1; ++cnt1)
    {
        s << ' ';
        (*this)[cnt1]->operator<<(s);
    }
    return s;
}
void flex_complex_var::add(mpt_var* p)
{
    complex_var::add(p);
    setDirty();
    setRMDirty();
}
void flex_complex_var::insert(mpt_var* p, int idx)
{
    complex_var::insert(p, idx);
    setDirty();
    setRMDirty();
}
void flex_complex_var::remove(int idx)
{
    complex_var::remove(idx);
    setDirty();
    setRMDirty();
}
void flex_complex_var::remove(mpt_var* p)
{
    complex_var::remove(p);
    setDirty();
    setRMDirty();
}
void flex_complex_var::resize(int nsize)
{
    int osize = cardinal(), cnt1;
    if (nsize != osize)
    {
        for (cnt1 = osize-1; cnt1 >= nsize; --cnt1)
        {
            remove(cnt1);
        }
        push();
        for (cnt1 = osize; cnt1 < nsize; ++cnt1)
        {
            create();
        }
        pop();
        setDirty();
        setRMDirty();
    }
}

int flex_complex_var::size_cardinal() const
{
    const int wd = cardinal_width();
    return (wd > 0) ? wd:-wd;
}

int flex_complex_var::cardinal_offset() const
{
    return 0;
}

int flex_complex_var::cardinal_width() const
{
    return 4;
}

#include <decode_var.h>
#include <rgen_var.h>
#include <bit_var.h>
#include <cplx_var.h>
#include <assert.h>

#ifndef DBL_EPSILON
#define DBL_EPSILON 0.000001L
#endif

static unsigned int i2pow(int exp)
{
    return 0xffffffff >> (32-exp);
}

vdecode_var::vdecode_var(int _code_typ, double _range, double _offset, int _scale_offset) :
    scaling(1.0), offset(_offset), msg(0)
{
    const int bit_size = _code_typ & 0xff;
    assert(bit_size > 0);
    assert(bit_size <= 32);
    const int data_typ = (_code_typ >> 8) & 0xff;
    unsigned int int_range;

    switch (data_typ)
    {
    case 0:
        int_range = i2pow(bit_size);
        upper_limit = int_range;
        lower_limit = 0;
        convert_signed_to_unsigned = false;
        if (bit_size > 16)
        {
            msg = new generic_var<unsigned int>;
        }
        else if (bit_size > 8)
        {
            msg = new generic_var<unsigned short>;
        }
        else
        {
            msg = new generic_var<unsigned char>;
        }
        break;
    case 1:
    {
        int_range = i2pow(bit_size);
        upper_limit = int_range;
        lower_limit = 0;
        convert_signed_to_unsigned = false;
        const int bz = (_code_typ >> 16) & 0xff;
        msg = new vbit_var(bit_size, ((bz >= 1) && (bz <= 7)) ? bz : 8);
        break;
    }
    case 2:
        int_range = i2pow(bit_size);
        upper_limit = int_range;
        lower_limit = 0;
        convert_signed_to_unsigned = false;
        if (bit_size > 16)
        {
            msg = new r_generic_var<unsigned int>;
        }
        else if (bit_size > 8)
        {
            msg = new r_generic_var<unsigned short>;
        }
        else
        {
            msg = new generic_var<unsigned char>;
        }
        break;
    case 3:
    {
        int_range = i2pow(bit_size);
        upper_limit = int_range;
        lower_limit = 0;
        convert_signed_to_unsigned = false;
        const int bz = (_code_typ >> 16) & 0xff;
        msg = new rvbit_var(bit_size, ((bz >= 1) && (bz <= 7)) ? bz : 8);
        break;
    }
    case 4:
        int_range = i2pow(bit_size);
        upper_limit = int_range / 2;
        lower_limit = -static_cast<int>(int_range / 2) - 1;
        convert_signed_to_unsigned = false;
        if (bit_size > 16)
        {
            msg = new generic_var<int>;
        }
        else if (bit_size > 8)
        {
            msg = new generic_var<short>;
        }
        else
        {
            msg = new generic_var<char>;
        }
        break;
    case 5:
        int_range = i2pow(bit_size);
        upper_limit = int_range / 2;
        lower_limit = -static_cast<int>(int_range / 2) - 1;
        convert_signed_to_unsigned = false;
        if (bit_size > 16)
        {
            msg = new r_generic_var<int>;
        }
        else if (bit_size > 8)
        {
            msg = new r_generic_var<short>;
        }
        else
        {
            msg = new generic_var<char>;
        }
        break;
    case 6:
    {
        int_range = i2pow(bit_size);
        upper_limit = int_range / 2;
        lower_limit = -static_cast<int>(int_range / 2) - 1;
        convert_signed_to_unsigned = true;
        const int bz = (_code_typ >> 16) & 0xff;
        msg = new vbit_var(-bit_size, ((bz >= 1) && (bz <= 7)) ? bz : 8);
        break;
    }
    case 7:
    {
        int_range = i2pow(bit_size);
        upper_limit = int_range / 2;
        lower_limit = -static_cast<int>(int_range / 2) - 1;
        convert_signed_to_unsigned = true;
        const int bz = (_code_typ >> 16) & 0xff;
        msg = new rvbit_var(-bit_size, ((bz >= 1) && (bz <= 7)) ? bz : 8);
        break;
    }
    default:
        assert(false);
    }
    // Because int_range is 32-bit, it has problems representing 2^32
    // therefore, i2pow computes i2pow - 1.
    // The upper and lower limits are there to cut off when the data
    // expands beyond the size of the integer or bitfield representing
    // it.
    // The scale offset is used to reduce the scale use of the range, but
    // this results in decreased resolution when the offset is increased.
    // If scale_offset is 1 or more, then the upper and lower limit check
    // will never trigger as long as the double falls within the range.
    // If scale_offset is 0 or less, then the upper limit check will trigger
    // at the top end of the scale. For example is scale is 360, then the
    // upper value of 360 cannot be represented.
    if ((_range < (0.0 - DBL_EPSILON)) || (_range > (0.0 + DBL_EPSILON)))
    {
        scaling = _range / (static_cast<double>(int_range - _scale_offset) + 1);
    }

    if (parent)
    {
        complex_var* c = RECAST(complex_var*, parent);
        if (c)
        {
            c->disown(msg);
        }
        // causes msg to think that it is a child of the parent,
        // but the parent does not think it is its child
    }
}

vdecode_var::~vdecode_var()
{
    delete msg;
}

int vdecode_var::encodeValueSigned(double val)
{
    const double enc = (val - offset) / scaling;
    if (enc >= upper_limit)
    {
        return static_cast<unsigned int>(upper_limit);
    }
    if (enc <= lower_limit)
    {
        return static_cast<int>(lower_limit);
    }
    const int ienc = static_cast<int>(enc);
    if (ienc < 0)
    {
        return ((static_cast<double>(ienc) - enc) >= .5) ? (ienc - 1) : ienc;
    }
    else
    {
        return ((enc - static_cast<double>(ienc)) >= .5) ? (ienc + 1) : ienc;
    }
}

unsigned int vdecode_var::encodeValueUnsigned(double val)
{
    const double enc = (val - offset) / scaling;
    if (enc >= upper_limit)
    {
        return static_cast<unsigned int>(upper_limit);
    }
    if (enc <= lower_limit)
    {
        return static_cast<unsigned int>(lower_limit);
    }
    const unsigned int ienc = static_cast<unsigned int>(enc);
    return ((enc - static_cast<double>(ienc)) >= .5) ? (ienc + 1) : ienc;
}

double vdecode_var::decodeValueSigned(int enc)
{
    return (enc*scaling) + offset;
}

double vdecode_var::decodeValueUnsigned(unsigned int enc)
{
    return (enc*scaling) + offset;
}

double vdecode_var::getScaling() const
{
    return scaling;
}

double vdecode_var::getOffset() const
{
    return offset;
}

int vdecode_var::extract(int buflen, const unsigned char* buf)
{
    if (msg)
    {
        buflen = msg->extract(buflen, buf);
        double dbl = 0;
        if (lower_limit < 0.)
        {
            const int val = (convert_signed_to_unsigned) ? static_cast<int>(msg->to_unsigned_int()) : msg->to_int();
            dbl = decodeValueSigned(val);
        }
        else
        {
            dbl = decodeValueUnsigned(msg->to_unsigned_int());
        }
        generic_var<double>::extract(sizeof(double), reinterpret_cast<const unsigned char*>(&dbl));
    }
    return buflen;
}

bool vdecode_var::extract_zero(const unsigned char* buf)
{
    bool ret = false;
    if (msg)
    {
        ret = msg->extract_zero(buf);
        double dbl = 0;
        if (lower_limit < 0.)
        {
            const int val = (convert_signed_to_unsigned) ? static_cast<int>(msg->to_unsigned_int()) : msg->to_int();
            dbl = decodeValueSigned(val);
        }
        else
        {
            dbl = decodeValueUnsigned(msg->to_unsigned_int());
        }
        generic_var<double>::extract(sizeof(double), reinterpret_cast<const unsigned char*>(&dbl));
    }
    return ret;
}

int vdecode_var::size() const
{
    return (msg) ? msg->size() : 0;
}

void vdecode_var::output(outbuf& strm)
{
    if (msg)
    {
        if (lower_limit < 0.)
        {
            const int val = encodeValueSigned(data);
            if (convert_signed_to_unsigned)
            {
                msg->from_unsigned_long(static_cast<unsigned long>(val));
            }
            else
            {
                msg->from_long(val);
            }
        }
        else
        {
            msg->from_unsigned_long(encodeValueUnsigned(data));
        }
        msg->output(strm);
    }
}

mpt_var* vdecode_var::getNext()
{
    return this + 1;
}

unsigned int vdecode_var::rtti() const
{
    // when msg is null, this means that this function has been called
    // while the creation of msg is in progress, for example when
    // msg happens to be a vbit_var or rvbit_var, and it needs to detect
    // its sibling. At this point, its sibling is just this vdecode_var
    // because the msg has not been disowned from the parent yet. So
    // for the correct behaviour, it needs to in turn return the rtti
    // of its sibling, so that we "jump two places".
    if (msg)
    {
        return msg->rtti();
    }

    complex_var* prnt = static_cast<complex_var*>(parent);
    const int car = prnt->cardinal();
    if (car >= 3)
    {
        return (*prnt)[car - 3]->rtti();
    }

    return rm_hash_string("vdecode_var");
}

const mpt_var& vdecode_var::operator=(const mpt_var& right)
{
    return generic_var<double>::operator=(right);
}

double vdecode_var::operator=(double right)
{
    return generic_var<double>::operator=(right);
}

const vdecode_var& vdecode_var::operator=(const vdecode_var& right)
{
    return static_cast<const vdecode_var&>(generic_var<double>::operator=(right));
}

const value_var* vdecode_var::getMsg() const
{
    if (msg)
    {
        return msg;
    }

    complex_var* prnt = static_cast<complex_var*>(parent);
    const int car = prnt->cardinal();
    if (car >= 3)
    {
        vdecode_var* bro = dynamic_cast<vdecode_var*>((*prnt)[car - 3]);
        if (bro)
        {
            return bro->getMsg();
        }
        vbit_var* bit = dynamic_cast<vbit_var*>((*prnt)[car - 3]);
        if (bit)
        {
            return bit;
        }
    }
    return 0;
}

inline static int byte_size_to_code_typ(int bsz)
{
    if (bsz < 0)
    {
        return 0x400 | ((-bsz*8) & 0xff);
    }
    else
    {
        return (bsz*8) & 0xff;
    }
}

decode_var::decode_var(int _byte_size, double _range, double _offset, int _scale_offset) :
    vdecode_var(byte_size_to_code_typ(_byte_size), _range, _offset, _scale_offset)
{
}

const mpt_var& decode_var::operator=(const mpt_var& right)
{
    return vdecode_var::operator=(right);
}

double decode_var::operator=(double right)
{
    return vdecode_var::operator=(right);
}

const decode_var& decode_var::operator=(const decode_var& right)
{
    vdecode_var::operator=(right);
    return *this;
}

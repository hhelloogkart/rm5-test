/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "ttag_var.h"

static bool isBE()
{
    union
    {
        unsigned int u;
        char c[4];
    } src;

    src.u = 1;
    return src.c[3] == 1;
}

ttag_var::ttag_var()
{
}

ttag_var::ttag_var(const ttag_var& cpy) :
    generic_var<unsigned int>(reinterpret_cast<const generic_var<unsigned int>&>(cpy))
{
}

int ttag_var::extract(int buflen, const unsigned char* buf)
{
    unsigned int old = data;
    char* p = reinterpret_cast<char*>(&data);
    char* q = p + 3;
    const char* r = reinterpret_cast<const char*>(buf);
    if (static_cast<size_t>(buflen) < 3)
    {
        return 0;
    }
#ifndef FLIP
    while (p != q)
    {
        *p = *r;
        ++p;
        ++r;
    }
#else
    while (q != p)
    {
        *(q-1) = *r;
        --q;
        ++r;
    }
#endif
#if defined(__BIG_ENDIAN_) || defined(_HPUX_SOURCE)
    data >>= 8;
#elif defined(__SMALL_ENDIAN_) || defined(_WIN32) || defined(__osf__) || defined(__CYGWIN__)
    data &= 0xFFFFFF;
#else // self-detect
    static bool f = isBE();
    if (!f)
    {
        data &= 0xFFFFFF;
    }
    else
    {
        data >>= 8;
    }
#endif
    if (isValid())
    {
        if (!::isValid(old) || (data != old))
        {
            mpt_var::setDirty();
            return buflen - 3;
        }
    }
    // error message here
    data = old;
    mpt_var::notDirty();
    return buflen - 3;
}

int ttag_var::size() const
{
    return 3;
}

void ttag_var::output(outbuf& strm)
{
    unsigned int tdata = data;
#if defined(__BIG_ENDIAN_) || defined(_HPUX_SOURCE)
    tdata <<= 8;
#elif defined(__SMALL_ENDIAN_) || defined(_WIN32) || defined(__osf__) || defined(__CYGWIN__)
#else
    static bool f = isBE();
    if (f)
    {
        tdata <<= 8;
    }
#endif
    char* p = reinterpret_cast<char*>(&tdata);
    char* q = p + 3;
#ifndef FLIP
    while (p != q)
    {
        strm += *p;
        ++p;
    }
#else
    while (p != q)
    {
        strm += *(q-1);
        --q;
    }
#endif
}

mpt_var* ttag_var::getNext()
{
    return this + 1;
}

const ttag_var& ttag_var::operator=(const ttag_var& right)
{
    generic_var<unsigned int>::operator=(reinterpret_cast<const generic_var<unsigned int>&>(right));
    return *this;
}

bool ttag_var::operator==(const ttag_var& right) const
{
    return data == right.data;
}

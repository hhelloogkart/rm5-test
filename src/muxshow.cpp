/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "rmstl.h"
#include "muxshow.h"
#include "mpt_var.h"

typedef LIST(int) dirtylist_typ;

struct mux_show_private
{
    dirtylist_typ dirty;
};

inline unsigned long bytes_offset(const void* p2, const void* p1)
{
    const char* cp1 = reinterpret_cast<const char*>(p1);
    const char* cp2 = reinterpret_cast<const char*>(p2);
    return static_cast<unsigned long>(cp2 - cp1);
}

mux_show::mux_show(mpt_var* st, unsigned int cnt, mpt_var* off) :
    prvt(new mux_show_private), array_start(st), array_cnt(cnt), offset(0), elem_offset(0)
{
    if (st)
    {
        offset = bytes_offset(st->getNext(), st);
        if (off)
        {
            elem_offset = bytes_offset(off, st);
        }
        while (cnt > 0)
        {
            advance(st, elem_offset)->addCallback(this);
            st = st->getNext();
            --cnt;
        }
    }
}

mux_show::~mux_show()
{
    if (array_start)
    {
        while (array_cnt > 0)
        {
            advance(array_start, elem_offset)->removeCallback(this);
            array_start = array_start->getNext();
            --array_cnt;
        }
    }
    delete prvt;
}

void mux_show::setDirty(mpt_var* v)
{
    mpt_var* const p1 = array_start;
    mpt_var* const p2 = v;

    if (p2 >= p1)
    {
        const int index = static_cast<int>(bytes_offset(p2, p1) / offset);
        prvt->dirty.remove(index);
        prvt->dirty.push_back(index);
    }
}

int mux_show::getDirty()
{
    if (!prvt->dirty.empty())
    {
        int diff = prvt->dirty.front();
        prvt->dirty.pop_front();
        return diff;
    }
    return -1;
}

mpt_var* mux_show::ref(unsigned int index)
{
    return advance(array_start, index * offset + elem_offset);
}

mpt_var* mux_show::advance(mpt_var* ptr, unsigned int off)
{
    return reinterpret_cast<mpt_var*>((char*)ptr + off);
}

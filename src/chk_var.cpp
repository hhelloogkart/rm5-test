/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "chk_var.h"
#include "outbuf.h"
#include <util/utilcore.h>
#include <cstddef>
#include <string.h>

chk_var::chk_var(unsigned char chk_loc, unsigned char front_off, unsigned char back_off) :
    total_in_packets(0),
    total_failed_packets(0),
    front_offset(front_off),
    back_offset(back_off),
    chk_location(chk_loc)
{
}

chk_var::chk_var(const chk_var& var) :
    complex_var(var),
    total_in_packets(0),
    total_failed_packets(0),
    front_offset(var.front_offset),
    back_offset(var.back_offset),
    chk_location(var.chk_location)
{
}

int chk_var::extract(int len, const unsigned char* buf)
{
    unsigned char computed_checksum[128]; // Assume that largest hash is 1024 bits
    outbuf obuf;
    const int chklen = checksum_size();

    ++total_in_packets;
    if (chk_location == 1)
    {
        if (len < (chklen + back_offset))
        {
            return len;
        }

        const unsigned char* const bufptr = buf - front_offset;
        const int buflen = len + front_offset - back_offset - chklen;
        const unsigned char* const chkptr = buf + len - back_offset - chklen;
        obuf.set(computed_checksum, sizeof(computed_checksum));
        make_chksum(buflen, bufptr, obuf);
        if (check(computed_checksum, chkptr, chklen))
        {
            return complex_var::extract(len, buf) - chklen;
        }
    }
    else if (chk_location == 2)
    {
        if (len < (chklen + front_offset + back_offset))
        {
            return len;
        }

        const unsigned char* const bufptr = buf + chklen + front_offset;
        const int buflen = len - front_offset - chklen - back_offset;
        const unsigned char* const chkptr = buf;
        obuf.set(computed_checksum, sizeof(computed_checksum));
        make_chksum(buflen, bufptr, obuf);
        if (check(computed_checksum, chkptr, chklen))
        {
            return complex_var::extract(len - chklen, buf + chklen);
        }
    }
    else if (chk_location == 3)
    {
        if (len < back_offset)
        {
            return len;
        }

        const unsigned char* const bufptr = buf - front_offset;
        const int buflen = len + front_offset - back_offset;
        const unsigned char* const chkptr = buf - front_offset - chklen;
        obuf.set(computed_checksum, sizeof(computed_checksum));
        make_chksum(buflen, bufptr, obuf);
        if (check(computed_checksum, chkptr, chklen))
        {
            return complex_var::extract(len, buf);
        }
    }
    else if (chk_location == 4)
    {
        // backoffset is ignored because we use child vars to determine the size
        const int childlen = complex_var::size();
        if (len < (chklen + childlen))
        {
            return len;
        }

        const unsigned char* const bufptr = buf - front_offset;
        const int buflen = childlen + front_offset;
        const unsigned char* const chkptr = buf + childlen;

        obuf.set(computed_checksum, sizeof(computed_checksum));
        make_chksum(buflen, bufptr, obuf);
        if (check(computed_checksum, chkptr, chklen))
        {
            return complex_var::extract(len, buf) - chklen;
        }
    }
    else
    {
        --total_in_packets; /* subtract it back, but should not get here */
        return len;  /* return all data to allow parent to continue parsing for valid chksum. */
    }
    ++total_failed_packets;
    notDirty();
    return len; /* return all data to allow parent to continue parsing for valid chksum. */
}

int chk_var::size() const
{
    if ((chk_location == 1) || (chk_location == 2) || (chk_location == 4))
    {
        return complex_var::size() + checksum_size();
    }
    else
    {
        return complex_var::size();
    }
}

void chk_var::output(outbuf& strm)
{
    unsigned char* const buf = strm.getcur();
    const int chklen = checksum_size();
    if ((chk_location == 1) || (chk_location == 4))
    {
        complex_var::output(strm);

        const unsigned char* const bufptr = buf - front_offset;
        const std::ptrdiff_t len = strm.getcur() - buf;
        const int buflen = static_cast<int>(len) + front_offset - back_offset;
        make_chksum(buflen, bufptr, strm);
    }
    else if (chk_location == 2)
    {
        outbuf obuf;
        obuf.set(buf + chklen, strm.maxsize() - strm.size() - chklen);
        complex_var::output(obuf);

        const unsigned char* const bufptr = buf + chklen + front_offset;
        const std::ptrdiff_t len = obuf.getcur() - buf;
        const int buflen = static_cast<int>(len) - front_offset - chklen - back_offset;
        make_chksum(buflen, bufptr, strm);

        strm.setsize(static_cast<unsigned int>(obuf.getcur() - strm.get()));
    }
    else if (chk_location == 3)
    {
        complex_var::output(strm);

        const unsigned char* const bufptr = buf - front_offset;
        const std::ptrdiff_t len = strm.getcur() - buf;
        const int buflen = static_cast<int>(len) + front_offset - back_offset;

        outbuf obuf;
        obuf.set(buf - front_offset - chklen, chklen);
        make_chksum(buflen, bufptr, obuf);
    }
    else
    {
        complex_var::output(strm);
    }
}

const mpt_var& chk_var::operator=(const mpt_var& right)
{
    const chk_var* gen = RECAST(const chk_var*,&right);
    if (gen)
    {
        chk_var::operator=(* gen);
    }
    return *this;
}

const chk_var& chk_var::operator=(const chk_var& right)
{
    if (this != &right)
    {
        complex_var::operator=(right);
    }
    return *this;
}

int chk_var::received_packets() const
{
    return total_in_packets;
}

int chk_var::failed_packets() const
{
    return total_failed_packets;
}

bool chk_var::check(const unsigned char* left, const unsigned char* right, int len)
{
    return memcmp(left, right, len) == 0;
}

int rm_BE(int val)
{
    return BE(val);
}

unsigned int rm_BE(unsigned int val)
{
    return BE(val);
}

unsigned short rm_BE(unsigned short val)
{
    return BE(val);
}

short rm_BE(short val)
{
    return BE(val);
}

int rm_LE(int val)
{
    return LE(val);
}

unsigned int rm_LE(unsigned int val)
{
    return LE(val);
}

unsigned short rm_LE(unsigned short val)
{
    return LE(val);
}

short rm_LE(short val)
{
    return LE(val);
}

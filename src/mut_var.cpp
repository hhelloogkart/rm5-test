#include <mut_var.h>
#include <gen_var.h>
#include <str_var.h>
#include <util/utilcore.h>
#include <outbuf.h>

#include <google/protobuf/descriptor.h>
#include <google/protobuf/message.h>

#include <assert.h>

mutable_var::mutable_var(google::protobuf::Message* _mesg) : message(_mesg), in_extract(false)
{
}

mutable_var::~mutable_var()
{
    delete message;
}

int mutable_var::extract(int len, const unsigned char* buf)
{
    if (len > static_cast<int>(sizeof(unsigned int)))
    {
        unsigned int sz;
        memcpy(&sz, buf, sizeof(unsigned int));
        sz = BE(sz);
        if ((unsigned int)len >= sz+sizeof(unsigned int))
        {
            if (message->ParseFromArray(buf+sizeof(unsigned int), sz))
            {
                in_extract = true;
                from_message(*message);
                in_extract = false;
            }
            return len-sz;
        }
    }
    return 0;
}

int mutable_var::size() const
{
    to_message(*message);
    return message->ByteSize() + sizeof(unsigned int);
}

void mutable_var::output(outbuf& strm)
{
    std::string str;
    message->SerializePartialToString(&str);
    union
    {
        unsigned int ui;
        char ch[4];
    } car;
    car.ui = BE(static_cast<unsigned int>(str.length()));
    strm += car.ch[0];
    strm += car.ch[1];
    strm += car.ch[2];
    strm += car.ch[3];
    for (int cnt=0; cnt<(int)str.length(); ++cnt)
    {
        strm += str[cnt];
    }
}

const mpt_var& mutable_var::operator=(const mpt_var& right)
{
    const complex_var* gen = RECAST(const complex_var*,&right);
    if (gen)
    {
        return complex_var::operator=(*gen);
    }
    return *this;
}

bool mutable_var::operator==(const mpt_var& right) const
{
    const complex_var* gen = RECAST(const complex_var*,&right);
    return (gen) ? complex_var::operator==(*gen) : complex_var::operator==(right);
}

const complex_var& mutable_var::operator=(const mutable_var& right)
{
    return complex_var::operator=(right);
}

bool mutable_var::operator==(const mutable_var& right) const
{
    return complex_var::operator==(right);
}

void mutable_var::setRMDirty()
{
    if (!in_extract)
    {
        mpt_var::setRMDirty();
    }
}

void mutable_var::to_message(google::protobuf::Message& mesg) const
{
    const google::protobuf::Descriptor* des = mesg.GetDescriptor();

    assert(cardinal() == des->field_count());

    for (int cnt=0; cnt<cardinal(); ++cnt)
    {
        const google::protobuf::FieldDescriptor* fd = des->field(cnt);
        const google::protobuf::Reflection* ref = mesg.GetReflection();

        if (fd->is_repeated())
        {
            // get number of elements
            complex_var* ch = static_cast<complex_var*>((*this)[cnt]);
            int car = ch->cardinal();
            int mcar = ref->FieldSize(mesg, fd);
            if (mcar > car)
            {
                ref->ClearField(&mesg, fd);
                mcar = 0;
            }
            switch (fd->type())
            {
            case google::protobuf::FieldDescriptor::TYPE_DOUBLE:
            {
                int idx;
                for (idx=0; idx<mcar; ++idx)
                {
                    value_var* mu = static_cast<value_var*>((*ch)[idx]);
                    ref->SetRepeatedDouble(&mesg, fd, idx, mu->to_double());
                }
                for (; idx<car; ++idx)
                {
                    value_var* mu = static_cast<value_var*>((*ch)[idx]);
                    ref->AddDouble(&mesg, fd, mu->to_double());
                }
                break;
            }
            case google::protobuf::FieldDescriptor::TYPE_FLOAT:
            {
                int idx;
                for (idx=0; idx<mcar; ++idx)
                {
                    value_var* mu = static_cast<value_var*>((*ch)[idx]);
                    ref->SetRepeatedFloat(&mesg, fd, idx, mu->to_float());
                }
                for (; idx<car; ++idx)
                {
                    value_var* mu = static_cast<value_var*>((*ch)[idx]);
                    ref->AddFloat(&mesg, fd, mu->to_float());
                }
                break;
            }
            case google::protobuf::FieldDescriptor::TYPE_SFIXED64:
            case google::protobuf::FieldDescriptor::TYPE_SINT64:
            case google::protobuf::FieldDescriptor::TYPE_INT64:
            {
                int idx;
                for (idx=0; idx<mcar; ++idx)
                {
                    value_var* mu = static_cast<value_var*>((*ch)[idx]);
                    ref->SetRepeatedInt64(&mesg, fd, idx, mu->to_long());
                }
                for (; idx<car; ++idx)
                {
                    value_var* mu = static_cast<value_var*>((*ch)[idx]);
                    ref->AddInt64(&mesg, fd, mu->to_long());
                }
                break;
            }
            case google::protobuf::FieldDescriptor::TYPE_FIXED64:
            case google::protobuf::FieldDescriptor::TYPE_UINT64:
            {
                int idx;
                for (idx=0; idx<mcar; ++idx)
                {
                    value_var* mu = static_cast<value_var*>((*ch)[idx]);
                    ref->SetRepeatedUInt64(&mesg, fd, idx, mu->to_unsigned_long());
                }
                for (; idx<car; ++idx)
                {
                    value_var* mu = static_cast<value_var*>((*ch)[idx]);
                    ref->AddUInt64(&mesg, fd, mu->to_unsigned_long());
                }
                break;
            }
            case google::protobuf::FieldDescriptor::TYPE_SFIXED32:
            case google::protobuf::FieldDescriptor::TYPE_SINT32:
            case google::protobuf::FieldDescriptor::TYPE_INT32:
            case google::protobuf::FieldDescriptor::TYPE_ENUM:
            {
                int idx;
                for (idx=0; idx<mcar; ++idx)
                {
                    value_var* mu = static_cast<value_var*>((*ch)[idx]);
                    ref->SetRepeatedInt32(&mesg, fd, idx, mu->to_int());
                }
                for (; idx<car; ++idx)
                {
                    value_var* mu = static_cast<value_var*>((*ch)[idx]);
                    ref->AddInt32(&mesg, fd, mu->to_int());
                }
                break;
            }
            case google::protobuf::FieldDescriptor::TYPE_FIXED32:
            case google::protobuf::FieldDescriptor::TYPE_UINT32:
            {
                int idx;
                for (idx=0; idx<mcar; ++idx)
                {
                    value_var* mu = static_cast<value_var*>((*ch)[idx]);
                    ref->SetRepeatedUInt32(&mesg, fd, idx, mu->to_unsigned_int());
                }
                for (; idx<car; ++idx)
                {
                    value_var* mu = static_cast<value_var*>((*ch)[idx]);
                    ref->AddUInt32(&mesg, fd, mu->to_unsigned_int());
                }
                break;
            }
            case google::protobuf::FieldDescriptor::TYPE_MESSAGE:
            {
                int idx;
                for (idx=0; idx<mcar; ++idx)
                {
                    mutable_var* mu = static_cast<mutable_var*>((*ch)[idx]);
                    google::protobuf::Message* msg = ref->MutableRepeatedMessage(&mesg, fd, idx);
                    mu->to_message(*msg);
                }
                for (; idx<car; ++idx)
                {
                    mutable_var* mu = static_cast<mutable_var*>((*ch)[idx]);
                    google::protobuf::Message* msg = ref->AddMessage(&mesg, fd);
                    mu->to_message(*msg);
                }
                break;
            }

            case google::protobuf::FieldDescriptor::TYPE_BYTES:
            {
                int idx;
                for (idx=0; idx<mcar; ++idx)
                {
                    mpt_var* mu = (*ch)[idx];
                    unsigned char* buf = new unsigned char[mu->size()];
                    outbuf obuf;
                    obuf.set(buf, mu->size());
                    mu->output(obuf);
                    std::string scratch((char*)buf+sizeof(short), mu->size()-sizeof(short));
                    ref->SetRepeatedString(&mesg, fd, idx, scratch);
                    delete[] buf;
                }
                for (; idx<car; ++idx)
                {
                    mpt_var* mu = (*ch)[idx];
                    unsigned char* buf = new unsigned char[mu->size()];
                    outbuf obuf;
                    obuf.set(buf, mu->size());
                    mu->output(obuf);
                    std::string scratch((char*)buf+sizeof(short), mu->size()-sizeof(short));
                    ref->AddString(&mesg, fd, scratch);
                }
                break;
            }

            case google::protobuf::FieldDescriptor::TYPE_STRING:
            {
                int idx;
                for (idx=0; idx<mcar; ++idx)
                {
                    value_var* mu = static_cast<value_var*>((*ch)[idx]);
                    std::string scratch(mu->to_const_char_ptr(), mu->size());
                    ref->SetRepeatedString(&mesg, fd, idx, scratch);
                }
                for (; idx<car; ++idx)
                {
                    value_var* mu = static_cast<value_var*>((*ch)[idx]);
                    std::string scratch(mu->to_const_char_ptr(), mu->size());
                    ref->AddString(&mesg, fd, scratch);
                }
                break;
            }

            case google::protobuf::FieldDescriptor::TYPE_BOOL:
            {
                int idx;
                for (idx=0; idx<mcar; ++idx)
                {
                    value_var* mu = static_cast<value_var*>((*ch)[idx]);
                    ref->SetRepeatedBool(&mesg, fd, idx, (bool)mu->to_unsigned_int());
                }
                for (; idx<car; ++idx)
                {
                    value_var* mu = static_cast<value_var*>((*ch)[idx]);
                    ref->AddBool(&mesg, fd, (bool)mu->to_unsigned_int());
                }
                break;
            }
            default:
                break;
            }
        }
        else
        {
            switch (fd->type())
            {
            case google::protobuf::FieldDescriptor::TYPE_DOUBLE:
            {
                value_var* mu = static_cast<value_var*>((*this)[cnt]);
                ref->SetDouble(&mesg, fd, mu->to_double());
                break;
            }
            case google::protobuf::FieldDescriptor::TYPE_FLOAT:
            {
                value_var* mu = static_cast<value_var*>((*this)[cnt]);
                ref->SetFloat(&mesg, fd, mu->to_float());
                break;
            }
            case google::protobuf::FieldDescriptor::TYPE_SFIXED64:
            case google::protobuf::FieldDescriptor::TYPE_SINT64:
            case google::protobuf::FieldDescriptor::TYPE_INT64:
            {
                value_var* mu = static_cast<value_var*>((*this)[cnt]);
                ref->SetInt64(&mesg, fd, mu->to_long());
                break;
            }
            case google::protobuf::FieldDescriptor::TYPE_FIXED64:
            case google::protobuf::FieldDescriptor::TYPE_UINT64:
            {
                value_var* mu = static_cast<value_var*>((*this)[cnt]);
                ref->SetUInt64(&mesg, fd, mu->to_unsigned_long());
                break;
            }
            case google::protobuf::FieldDescriptor::TYPE_SFIXED32:
            case google::protobuf::FieldDescriptor::TYPE_SINT32:
            case google::protobuf::FieldDescriptor::TYPE_INT32:
            case google::protobuf::FieldDescriptor::TYPE_ENUM:
            {
                value_var* mu = static_cast<value_var*>((*this)[cnt]);
                ref->SetInt32(&mesg, fd, mu->to_int());
                break;
            }
            case google::protobuf::FieldDescriptor::TYPE_FIXED32:
            case google::protobuf::FieldDescriptor::TYPE_UINT32:
            {
                value_var* mu = static_cast<value_var*>((*this)[cnt]);
                ref->SetUInt32(&mesg, fd, mu->to_unsigned_int());
                break;
            }
            case google::protobuf::FieldDescriptor::TYPE_MESSAGE:
            {
                mutable_var* mu = static_cast<mutable_var*>((*this)[cnt]);
                google::protobuf::Message* msg = ref->MutableMessage(&mesg, fd);
                mu->to_message(*msg);
                break;
            }
            case google::protobuf::FieldDescriptor::TYPE_BYTES:
            {
                mpt_var* mu = (*this)[cnt];
                unsigned char* buf = new unsigned char[mu->size()];
                outbuf obuf;
                obuf.set(buf, mu->size());
                mu->output(obuf);
                std::string scratch((char*)buf+sizeof(short), mu->size()-sizeof(short));
                ref->SetString(&mesg, fd, scratch);
                delete[] buf;
                break;
            }
            case google::protobuf::FieldDescriptor::TYPE_STRING:
            {
                value_var* mu = static_cast<value_var*>((*this)[cnt]);
                std::string scratch(mu->to_const_char_ptr(), mu->size());
                ref->SetString(&mesg, fd, scratch);
                break;
            }
            case google::protobuf::FieldDescriptor::TYPE_BOOL:
            {
                value_var* mu = static_cast<value_var*>((*this)[cnt]);
                ref->SetBool(&mesg, fd, (bool)mu->to_unsigned_int());
                break;
            }
            default:
                break;
            }
        }
    }
}

void mutable_var::from_message(const google::protobuf::Message& mesg)
{
    const google::protobuf::Descriptor* des = mesg.GetDescriptor();

    assert(cardinal() == des->field_count());

    for (int cnt=0; cnt<cardinal(); ++cnt)
    {
        const google::protobuf::FieldDescriptor* fd = des->field(cnt);
        const google::protobuf::Reflection* ref = mesg.GetReflection();

        if (fd->is_repeated())
        {
            // get number of elements
            int car = ref->FieldSize(mesg, fd);
            // flex_var or r_flex_var
            complex_var* ch = static_cast<complex_var*>((*this)[cnt]);
            ch->resize(car);

            switch (fd->type())
            {
            case google::protobuf::FieldDescriptor::TYPE_DOUBLE:
                for (int idx=0; idx<car; ++idx)
                {
                    generic_var<double>* mu = static_cast<generic_var<double>*>((*ch)[idx]);
                    double mudata = ref->GetRepeatedDouble(mesg, fd, idx);
                    if (*mu != mudata)
                    {
                        *mu = mudata;
                    }
                    else
                    {
                        mu->notDirty();
                    }
                }
                break;

            case google::protobuf::FieldDescriptor::TYPE_FLOAT:
                for (int idx=0; idx<car; ++idx)
                {
                    generic_var<float>* mu = static_cast<generic_var<float>*>((*ch)[idx]);
                    float mudata = ref->GetRepeatedFloat(mesg, fd, idx);
                    if (*mu != mudata)
                    {
                        *mu = mudata;
                    }
                    else
                    {
                        mu->notDirty();
                    }
                }
                break;

            case google::protobuf::FieldDescriptor::TYPE_SFIXED64:
            case google::protobuf::FieldDescriptor::TYPE_SINT64:
            case google::protobuf::FieldDescriptor::TYPE_INT64:
                for (int idx=0; idx<car; ++idx)
                {
                    generic_var<long long>* mu = static_cast<generic_var<long long>*>((*ch)[idx]);
                    long long mudata = static_cast<long long>(ref->GetRepeatedInt64(mesg, fd, idx));
                    if (*mu != mudata)
                    {
                        *mu = mudata;
                    }
                    else
                    {
                        mu->notDirty();
                    }
                }
                break;

            case google::protobuf::FieldDescriptor::TYPE_FIXED64:
            case google::protobuf::FieldDescriptor::TYPE_UINT64:
                for (int idx=0; idx<car; ++idx)
                {
                    generic_var<unsigned long long>* mu = static_cast<generic_var<unsigned long long>*>((*ch)[idx]);
                    unsigned long long mudata = static_cast<unsigned long long>(ref->GetRepeatedUInt64(mesg, fd, idx));
                    if (*mu != mudata)
                    {
                        *mu = mudata;
                    }
                    else
                    {
                        mu->notDirty();
                    }
                }
                break;

            case google::protobuf::FieldDescriptor::TYPE_SFIXED32:
            case google::protobuf::FieldDescriptor::TYPE_SINT32:
            case google::protobuf::FieldDescriptor::TYPE_INT32:
            case google::protobuf::FieldDescriptor::TYPE_ENUM:
                if (car > 0)
                {
                    static const int sival = 0;
                    static const short ssval = 0;
                    static const char scval = 0;

                    mpt_var* cmpt = (*ch)[0];
                    if ((cmpt->rtti() == (hash_string("generic_var")   ^ var_rtti(sival))) ||
                        (cmpt->rtti() == (hash_string("r_generic_var") ^ var_rtti(sival))))
                    {
                        for (int idx=0; idx<car; ++idx)
                        {
                            int mudata = static_cast<int>(ref->GetRepeatedInt32(mesg, fd, idx));
                            generic_var<int>* mu = static_cast<generic_var<int>*>((*ch)[idx]);
                            if (*mu != mudata)
                            {
                                *mu = mudata;
                            }
                            else
                            {
                                mu->notDirty();
                            }
                        }
                    }
                    else if ((cmpt->rtti() == (hash_string("generic_var")   ^ var_rtti(ssval))) ||
                             (cmpt->rtti() == (hash_string("r_generic_var") ^ var_rtti(ssval))))
                    {
                        for (int idx=0; idx<car; ++idx)
                        {
                            short mudata = static_cast<short>(ref->GetRepeatedInt32(mesg, fd, idx));
                            generic_var<short>* mu = static_cast<generic_var<short>*>((*ch)[idx]);
                            if (*mu != mudata)
                            {
                                *mu = mudata;
                            }
                            else
                            {
                                mu->notDirty();
                            }
                        }
                    }
                    else if ((cmpt->rtti() == (hash_string("generic_var")   ^ var_rtti(scval))) ||
                             (cmpt->rtti() == (hash_string("r_generic_var") ^ var_rtti(scval))))
                    {
                        for (int idx=0; idx<car; ++idx)
                        {
                            char mudata = static_cast<char>(ref->GetRepeatedInt32(mesg, fd, idx));
                            generic_var<char>* mu = static_cast<generic_var<char>*>((*ch)[idx]);
                            if (*mu != mudata)
                            {
                                *mu = mudata;
                            }
                            else
                            {
                                mu->notDirty();
                            }
                        }
                    }
                }
                break;

            case google::protobuf::FieldDescriptor::TYPE_FIXED32:
            case google::protobuf::FieldDescriptor::TYPE_UINT32:
                if (car > 0)
                {
                    static const unsigned int uival = 0;
                    static const unsigned short usval = 0;
                    static const unsigned char ucval = 0;

                    mpt_var* cmpt = (*ch)[0];
                    if ((cmpt->rtti() == (hash_string("generic_var")   ^ var_rtti(uival))) ||
                        (cmpt->rtti() == (hash_string("r_generic_var") ^ var_rtti(uival))) ||
                        (cmpt->rtti() == hash_string("bit_var")) ||
                        (cmpt->rtti() == hash_string("rbit_var")) ||
                        (cmpt->rtti() == hash_string("ttag_var")))
                    {
                        for (int idx=0; idx<car; ++idx)
                        {
                            unsigned int mudata = static_cast<unsigned int>(ref->GetRepeatedUInt32(mesg, fd, idx));
                            generic_var<unsigned int>* mu = static_cast<generic_var<unsigned int>*>((*ch)[idx]);
                            if (*mu != mudata)
                            {
                                *mu = mudata;
                            }
                            else
                            {
                                mu->notDirty();
                            }
                        }
                    }
                    else if ((cmpt->rtti() == (hash_string("generic_var")   ^ var_rtti(usval))) ||
                             (cmpt->rtti() == (hash_string("r_generic_var") ^ var_rtti(usval))))
                    {
                        for (int idx=0; idx<car; ++idx)
                        {
                            unsigned short mudata = static_cast<unsigned short>(ref->GetRepeatedUInt32(mesg, fd, idx));
                            generic_var<unsigned short>* mu = static_cast<generic_var<unsigned short>*>((*ch)[idx]);
                            if (*mu != mudata)
                            {
                                *mu = mudata;
                            }
                            else
                            {
                                mu->notDirty();
                            }
                        }
                    }
                    else if ((cmpt->rtti() == (hash_string("generic_var")   ^ var_rtti(ucval))) ||
                             (cmpt->rtti() == (hash_string("r_generic_var") ^ var_rtti(ucval))))
                    {
                        for (int idx=0; idx<car; ++idx)
                        {
                            unsigned char mudata = static_cast<unsigned char>(ref->GetRepeatedUInt32(mesg, fd, idx));
                            generic_var<unsigned char>* mu = static_cast<generic_var<unsigned char>*>((*ch)[idx]);
                            if (*mu != mudata)
                            {
                                *mu = mudata;
                            }
                            else
                            {
                                mu->notDirty();
                            }
                        }
                    }
                }
                break;

            case google::protobuf::FieldDescriptor::TYPE_MESSAGE:
                for (int idx=0; idx<car; ++idx)
                {
                    mutable_var* mu = static_cast<mutable_var*>((*ch)[idx]);
                    mu->from_message(ref->GetRepeatedMessage(mesg, fd, idx));
                }
                break;

            case google::protobuf::FieldDescriptor::TYPE_BYTES:
                for (int idx=0; idx<car; ++idx)
                {
                    static const int sival = 0;
                    static const short ssval = 0;
                    static const int uival = 0;
                    static const short usval = 0;

                    mpt_var* mu = (*ch)[idx];
                    std::string scratch;
                    const std::string& str = ref->GetRepeatedStringReference(mesg, fd, idx, &scratch);
                    unsigned char* tbuf = new unsigned char[str.length()+sizeof(unsigned short)];
                    if ((mu->rtti() == (hash_string("array_var") ^ var_rtti(sival))) ||
                        (mu->rtti() == (hash_string("array_var") ^ var_rtti(uival))) ||
                        (mu->rtti() == (hash_string("r_array_var") ^ var_rtti(sival))) ||
                        (mu->rtti() == (hash_string("r_array_var") ^ var_rtti(uival))))
                    {
                        *((unsigned short*)tbuf) = static_cast<unsigned short>(str.length()/sizeof(sival));
                    }
                    else if ((mu->rtti() == (hash_string("array_var") ^ var_rtti(ssval))) ||
                             (mu->rtti() == (hash_string("array_var") ^ var_rtti(usval))) ||
                             (mu->rtti() == (hash_string("r_array_var") ^ var_rtti(ssval))) ||
                             (mu->rtti() == (hash_string("r_array_var") ^ var_rtti(usval))))
                    {
                        *((unsigned short*)tbuf) = static_cast<unsigned short>(str.length()/sizeof(ssval));
                    }
                    else
                    {
                        *((unsigned short*)tbuf) = static_cast<unsigned short>(str.length());
                    }
                    memcpy(tbuf+sizeof(unsigned short), str.data(), str.length());
                    mu->extract(static_cast<int>(str.length()+sizeof(unsigned short)), tbuf);
                    delete[] tbuf;
                }
                break;

            case google::protobuf::FieldDescriptor::TYPE_STRING:
                for (int idx=0; idx<car; ++idx)
                {
                    value_var* mu = static_cast<value_var*>((*ch)[idx]);
                    std::string scratch;
                    const std::string& str = ref->GetRepeatedStringReference(mesg, fd, idx, &scratch);
                    mu->extract(static_cast<int>(str.length()), reinterpret_cast<const unsigned char*>(str.data()));
                }
                break;

            case google::protobuf::FieldDescriptor::TYPE_BOOL:
                if (car > 0)
                {
                    mpt_var* cmpt = (*ch)[0];
                    if ((cmpt->rtti() == hash_string("bit_var")) ||
                        (cmpt->rtti() == hash_string("rbit_var")))
                    {
                        for (int idx=0; idx<car; ++idx)
                        {
                            unsigned int mudata = static_cast<unsigned int>(ref->GetRepeatedBool(mesg, fd, idx));
                            generic_var<unsigned int>* mu = static_cast<generic_var<unsigned int>*>((*ch)[idx]);
                            if (*mu != mudata)
                            {
                                *mu = mudata;
                            }
                            else
                            {
                                mu->notDirty();
                            }
                        }
                    }
                }
                break;
            default:
                break;
            }
        }
        else
        {
            switch (fd->type())
            {
            case google::protobuf::FieldDescriptor::TYPE_DOUBLE:
            {
                generic_var<double>* mu = static_cast<generic_var<double>*>((*this)[cnt]);
                double mudata = ref->GetDouble(mesg, fd);
                if (*mu != mudata)
                {
                    *mu = mudata;
                }
                else
                {
                    mu->notDirty();
                }
                break;
            }
            case google::protobuf::FieldDescriptor::TYPE_FLOAT:
            {
                generic_var<float>* mu = static_cast<generic_var<float>*>((*this)[cnt]);
                float mudata = ref->GetFloat(mesg, fd);
                if (*mu != mudata)
                {
                    *mu = mudata;
                }
                else
                {
                    mu->notDirty();
                }
                break;
            }
            case google::protobuf::FieldDescriptor::TYPE_SFIXED64:
            case google::protobuf::FieldDescriptor::TYPE_SINT64:
            case google::protobuf::FieldDescriptor::TYPE_INT64:
            {
                generic_var<long long>* mu = static_cast<generic_var<long long>*>((*this)[cnt]);
                long long mudata = ref->GetInt64(mesg, fd);
                if (*mu != mudata)
                {
                    *mu = mudata;
                }
                else
                {
                    mu->notDirty();
                }
                break;
            }

            case google::protobuf::FieldDescriptor::TYPE_FIXED64:
            case google::protobuf::FieldDescriptor::TYPE_UINT64:
            {
                generic_var<unsigned long long>* mu = static_cast<generic_var<unsigned long long>*>((*this)[cnt]);
                unsigned long long mudata = ref->GetUInt64(mesg, fd);
                if (*mu != mudata)
                {
                    *mu = mudata;
                }
                else
                {
                    mu->notDirty();
                }
                break;
            }

            case google::protobuf::FieldDescriptor::TYPE_SFIXED32:
            case google::protobuf::FieldDescriptor::TYPE_SINT32:
            case google::protobuf::FieldDescriptor::TYPE_INT32:
            case google::protobuf::FieldDescriptor::TYPE_ENUM:
            {
                static const int sival = 0;
                static const short ssval = 0;
                static const char scval = 0;

                mpt_var* cmpt = (*this)[cnt];
                if ((cmpt->rtti() == (hash_string("generic_var")   ^ var_rtti(sival))) ||
                    (cmpt->rtti() == (hash_string("r_generic_var") ^ var_rtti(sival))))
                {
                    int mudata = static_cast<int>(ref->GetInt32(mesg, fd));
                    generic_var<int>* mu = static_cast<generic_var<int>*>((*this)[cnt]);
                    if (*mu != mudata)
                    {
                        *mu = mudata;
                    }
                    else
                    {
                        mu->notDirty();
                    }
                }
                else if ((cmpt->rtti() == (hash_string("generic_var")   ^ var_rtti(ssval))) ||
                         (cmpt->rtti() == (hash_string("r_generic_var") ^ var_rtti(ssval))))
                {
                    short mudata = static_cast<short>(ref->GetInt32(mesg, fd));
                    generic_var<short>* mu = static_cast<generic_var<short>*>((*this)[cnt]);
                    if (*mu != mudata)
                    {
                        *mu = mudata;
                    }
                    else
                    {
                        mu->notDirty();
                    }
                }
                else if ((cmpt->rtti() == (hash_string("generic_var")   ^ var_rtti(scval))) ||
                         (cmpt->rtti() == (hash_string("r_generic_var") ^ var_rtti(scval))))
                {
                    char mudata = static_cast<char>(ref->GetInt32(mesg, fd));
                    generic_var<char>* mu = static_cast<generic_var<char>*>((*this)[cnt]);
                    if (*mu != mudata)
                    {
                        *mu = mudata;
                    }
                    else
                    {
                        mu->notDirty();
                    }
                }
                break;
            }

            case google::protobuf::FieldDescriptor::TYPE_FIXED32:
            case google::protobuf::FieldDescriptor::TYPE_UINT32:
            {
                static const unsigned int uival = 0;
                static const unsigned short usval = 0;
                static const unsigned char ucval = 0;

                mpt_var* cmpt = (*this)[cnt];
                if ((cmpt->rtti() == (hash_string("generic_var")   ^ var_rtti(uival))) ||
                    (cmpt->rtti() == (hash_string("r_generic_var") ^ var_rtti(uival))) ||
                    (cmpt->rtti() == hash_string("bit_var")) ||
                    (cmpt->rtti() == hash_string("rbit_var")) ||
                    (cmpt->rtti() == hash_string("ttag_var")))
                {
                    unsigned int mudata = static_cast<unsigned int>(ref->GetUInt32(mesg, fd));
                    generic_var<unsigned int>* mu = static_cast<generic_var<unsigned int>*>((*this)[cnt]);
                    if (*mu != mudata)
                    {
                        *mu = mudata;
                    }
                    else
                    {
                        mu->notDirty();
                    }
                }
                else if ((cmpt->rtti() == (hash_string("generic_var")   ^ var_rtti(usval))) ||
                         (cmpt->rtti() == (hash_string("r_generic_var") ^ var_rtti(usval))))
                {
                    unsigned short mudata = static_cast<unsigned short>(ref->GetUInt32(mesg, fd));
                    generic_var<unsigned short>* mu = static_cast<generic_var<unsigned short>*>((*this)[cnt]);
                    if (*mu != mudata)
                    {
                        *mu = mudata;
                    }
                    else
                    {
                        mu->notDirty();
                    }
                }
                else if ((cmpt->rtti() == (hash_string("generic_var")   ^ var_rtti(ucval))) ||
                         (cmpt->rtti() == (hash_string("r_generic_var") ^ var_rtti(ucval))))
                {
                    unsigned char mudata = static_cast<unsigned char>(ref->GetUInt32(mesg, fd));
                    generic_var<unsigned char>* mu = static_cast<generic_var<unsigned char>*>((*this)[cnt]);
                    if (*mu != mudata)
                    {
                        *mu = mudata;
                    }
                    else
                    {
                        mu->notDirty();
                    }
                }
                break;
            }

            case google::protobuf::FieldDescriptor::TYPE_MESSAGE:
            {
                mutable_var* mu = static_cast<mutable_var*>((*this)[cnt]);
                mu->from_message(ref->GetMessage(mesg, fd));
                break;
            }

            case google::protobuf::FieldDescriptor::TYPE_BYTES:
            {
                static const int sival = 0;
                static const short ssval = 0;
                static const int uival = 0;
                static const short usval = 0;

                mpt_var* mu = (*this)[cnt];
                std::string scratch;
                const std::string& str = ref->GetStringReference(mesg, fd, &scratch);
                unsigned char* tbuf = new unsigned char[str.length()+sizeof(unsigned short)];
                if ((mu->rtti() == (hash_string("array_var") ^ var_rtti(sival))) ||
                    (mu->rtti() == (hash_string("array_var") ^ var_rtti(uival))) ||
                    (mu->rtti() == (hash_string("r_array_var") ^ var_rtti(sival))) ||
                    (mu->rtti() == (hash_string("r_array_var") ^ var_rtti(uival))))
                {
                    *((unsigned short*)tbuf) = static_cast<unsigned short>(str.length()/sizeof(sival));
                }
                else if ((mu->rtti() == (hash_string("array_var") ^ var_rtti(ssval))) ||
                         (mu->rtti() == (hash_string("array_var") ^ var_rtti(usval))) ||
                         (mu->rtti() == (hash_string("r_array_var") ^ var_rtti(ssval))) ||
                         (mu->rtti() == (hash_string("r_array_var") ^ var_rtti(usval))))
                {
                    *((unsigned short*)tbuf) = static_cast<unsigned short>(str.length()/sizeof(ssval));
                }
                else
                {
                    *((unsigned short*)tbuf) = static_cast<unsigned short>(str.length());
                }
                memcpy(tbuf+sizeof(unsigned short), str.data(), str.length());
                mu->extract(static_cast<int>(str.length()+sizeof(unsigned short)), tbuf);
                delete[] tbuf;
                break;
            }

            case google::protobuf::FieldDescriptor::TYPE_STRING:
            {
                value_var* mu = static_cast<value_var*>((*this)[cnt]);
                std::string scratch;
                const std::string& str = ref->GetStringReference(mesg, fd, &scratch);
                mu->extract(static_cast<int>(str.length()), reinterpret_cast<const unsigned char*>(str.data()));
                break;
            }
            case google::protobuf::FieldDescriptor::TYPE_BOOL:
            {
                mpt_var* cmpt = (*this)[cnt];
                if ((cmpt->rtti() == hash_string("bit_var")) ||
                    (cmpt->rtti() == hash_string("rbit_var")))
                {
                    unsigned int mudata = static_cast<unsigned int>(ref->GetBool(mesg, fd));
                    generic_var<unsigned int>* mu = static_cast<generic_var<unsigned int>*>((*this)[cnt]);
                    if (*mu != mudata)
                    {
                        *mu = mudata;
                    }
                    else
                    {
                        mu->notDirty();
                    }
                }
                break;
            }
            default:
                break;
            }
        }
    }
}

/******************************************************************/
/* Copyright DSO National Laboratories 2013. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include <flun_show.h>
#include <flun_var.h>
#include <list>

typedef std::list<int> id_list_typ;

struct flex_union_show_private
{
    id_list_typ id_list;
};

flex_union_show::flex_union_show() : obj(0), prvt(new flex_union_show_private)
{
}

flex_union_show::~flex_union_show()
{
    delete prvt;
}

void flex_union_show::setDirty(mpt_var* var)
{
    if (var)
    {
        const int id = obj->addr_to_id(var);
        if (id != -1)
        {
            setIDDirty(id);
        }
    }
}

void flex_union_show::setIDDirty(int id)
{
    id_list_typ::reverse_iterator it=prvt->id_list.rbegin();
    if (it != prvt->id_list.rend())
    {
        if ((*it) == id)
        {
            return;
        }
        for (++it; it != prvt->id_list.rend(); ++it)
        {
            if ((*it) == id)
            {
                //itTemp = it;
                //prvt->id_list.erase((++itTemp).base());
                prvt->id_list.erase((++it).base()); //google stackoverflow
                //it--;
                break;
            }
        }
    }
    prvt->id_list.push_back(id); //for compute
}

void flex_union_show::operator()()
{
    //std::cout << "flex_union_show " << std::endl;
    if (!prvt->id_list.empty())
    {

        for (id_list_typ::iterator it=prvt->id_list.begin(); it!=prvt->id_list.end(); ++it)
        {
            compute(*it);
        }
        prvt->id_list.clear();
    }
}


void flex_union_show::addCallback(flex_union_var* var)
{
    obj = var;
    var->addChildCallback(this);
}

void flex_union_show::removeCallback(flex_union_var* var)
{
    obj = var;
    var->removeChildCallback(this);
}
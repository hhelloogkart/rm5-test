/******************************************************************/
/* Copyright DSO National Laboratories 2003. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "mhash_var.h"
#include "mhash/include/mhash.h"
#include "outbuf.h"

mhash_var::mhash_var(int _hash, unsigned char chk_loc, unsigned char front_off, unsigned char back_off) : chk_var(chk_loc, front_off, back_off), hashtype(_hash)
{
}

mhash_var::mhash_var(const mhash_var& right) : chk_var(right), hashtype(right.hashtype)
{
}

const mhash_var& mhash_var::operator=(const mhash_var& right)
{
    chk_var::operator=(static_cast<const chk_var&>(right));
    return *this;
}

const mpt_var& mhash_var::operator=(const mpt_var& right)
{
    return chk_var::operator=(right);
}

int mhash_var::checksum_size() const
{
    return (int)mhash_get_block_size((hashid)hashtype);
}

void mhash_var::make_chksum(int len, const unsigned char* buf, outbuf& strm)
{
    int sz = checksum_size();
    unsigned char p[64], * r = p;

    MHASH mh = mhash_init((hashid)hashtype);
    mhash(mh, buf, (size_t)len);
    mhash_deinit(mh, p);
    for (; sz > 0; ++r, --sz)
    {
        strm += *r;
    }
}

mhash_be_int_var::mhash_be_int_var(int _hash, unsigned char chk_loc, unsigned char front_off, unsigned char back_off) :
    mhash_var(_hash, chk_loc, front_off, back_off)
{
}

mhash_be_int_var::mhash_be_int_var(const mhash_be_int_var& right) : mhash_var(right)
{
}

const mhash_be_int_var& mhash_be_int_var::operator=(const mhash_be_int_var& right)
{
    mhash_var::operator=(static_cast<const mhash_var&>(right));
    return *this;
}

const mpt_var& mhash_be_int_var::operator=(const mpt_var& right)
{
    return mhash_var::operator=(right);
}

void mhash_be_int_var::make_chksum(int len, const unsigned char* buf, outbuf& strm)
{
    static const int ival = 0x01020304;
    static const bool be = *reinterpret_cast<const char*>(&ival) == '\1';
    if (be)
    {
        return mhash_var::make_chksum(len, buf, strm);
    }
    else
    {
        unsigned char p[64];
        outbuf ob;
        ob.set(p, sizeof(p));
        mhash_var::make_chksum(len, buf, ob);
        for (int i = ob.size() - 1; i >= 0; --i)
        {
            strm += p[i];
        }
    }
}

mhash_le_int_var::mhash_le_int_var(int _hash, unsigned char chk_loc, unsigned char front_off, unsigned char back_off) :
    mhash_var(_hash, chk_loc, front_off, back_off)
{
}

mhash_le_int_var::mhash_le_int_var(const mhash_le_int_var& right) : mhash_var(right)
{
}

const mhash_le_int_var& mhash_le_int_var::operator=(const mhash_le_int_var& right)
{
    mhash_var::operator=(static_cast<const mhash_var&>(right));
    return *this;
}

const mpt_var& mhash_le_int_var::operator=(const mpt_var& right)
{
    return mhash_var::operator=(right);
}

void mhash_le_int_var::make_chksum(int len, const unsigned char* buf, outbuf& strm)
{
    const static int ival = 0x04030201;
    const static bool le = *reinterpret_cast<const char*>(&ival) == '\1';
    if (le)
    {
        return mhash_var::make_chksum(len, buf, strm);
    }
    else
    {
        unsigned char p[64];
        outbuf ob;
        ob.set(p, sizeof(p));
        mhash_var::make_chksum(len, buf, ob);
        for (int i = ob.size() - 1; i >= 0; --i)
        {
            strm += p[i];
        }
    }
}
#include "counter_var.h"
#include "outbuf.h"
#include <vector>
#include <utility>
#include <assert.h>

typedef std::vector<std::pair<unsigned int, unsigned int> > counter_store;

counter_store& store()
{
    static counter_store s;
    return s;
}

counter_var::counter_var(unsigned int counter_id, unsigned int counter_sz, unsigned char chk_loc, unsigned char front_off, unsigned char back_off) :
    chk_var(chk_loc, front_off, back_off), id(counter_id), sz(counter_sz), in_extract(false), cached(false)
{
    counter_store& st = store();
    if (st.size() <= counter_id)
    {
        st.resize(counter_id + 1);
    }
    assert(counter_sz == 1 || counter_sz == 2 || counter_sz == 4);
}

counter_var::counter_var(const counter_var& right) :
    chk_var(right), id(right.id), sz(right.sz), in_extract(false), cached(false)
{
}

int counter_var::extract(int len, const unsigned char* buf)
{
    in_extract = true;
    const int ret = chk_var::extract(len, buf);
    in_extract = false;
    return ret;
}

bool counter_var::setInvalid()
{
    store()[id].first = 0;
    store()[id].second = 0;
    return complex_var::setInvalid();
}

void counter_var::setRMDirty()
{
    cached = false;
    chk_var::setRMDirty();
}

int counter_var::checksum_size() const
{
    return sz;
}

void counter_var::make_chksum(int, const unsigned char*, outbuf& strm)
{
    const static int ival = 0x04030201;
    const static bool le = *reinterpret_cast<const char*>(&ival) == '\1';
    unsigned int& counter = (!in_extract) ? (cached) ? cached_output_counter : store()[id].second : store()[id].first;
    if (le)
    {
        unsigned char* ptr = reinterpret_cast<unsigned char*>(&counter);
        for (unsigned int idx=0; idx < sz; ++idx, ++ptr)
        {
            strm += *ptr;
        }
    }
    else
    {
        unsigned char* ptr = reinterpret_cast<unsigned char*>(&counter) + sizeof(unsigned int) - 1;
        for (unsigned int idx = 0; idx < sz; ++idx, --ptr)
        {
            strm += *ptr;
        }
    }
    if (!in_extract && !cached)
    {
        cached_output_counter = counter;
        ++counter;
        cached = true;
    }
}

const counter_var& counter_var::operator=(const counter_var& right)
{
    chk_var::operator=(static_cast<const chk_var&>(right));
    return *this;
}

const mpt_var& counter_var::operator=(const mpt_var& right)
{
    return chk_var::operator=(right);
}

template<class C>
bool inner_check(const unsigned char* left, const unsigned char* right)
{
    const int reject_window = 100;
    C lval = 0;
    C rval = 0;
    memcpy(&lval, left, sizeof(C));
    memcpy(&rval, right, sizeof(C));
    if (lval == rval)
    {
        return true;
    }
    --lval;
    for (int idx = 0; idx < reject_window; ++idx, --lval)
    {
        if (lval == rval)
        {
            return false;
        }
    }
    return true;
}

bool counter_var::check(const unsigned char* left, const unsigned char* right, int len)
{
    const static int ival = 0x04030201;
    const static bool le = *reinterpret_cast<const char*>(&ival) == '\1';
    bool ok = false;
    switch (len)
    {
    case 1:
        ok = inner_check<unsigned char>(left, right);
        break;
    case 2:
        ok = inner_check<unsigned short>(left, right);
        break;
    case 4:
        ok = inner_check<unsigned int>(left, right);
        break;
    default:
        ok = false;
    }
    if (ok)
    {
        unsigned int& counter = store()[id].first;
        if (le)
        {
            unsigned char* dst = reinterpret_cast<unsigned char*>(&counter);
            for (unsigned int idx = 0; idx < sz; ++idx, ++right, ++dst)
            {
                *dst = *right;
            }
        }
        else
        {
            unsigned char* dst = reinterpret_cast<unsigned char*>(&counter) + sizeof(unsigned int) - 1;
            for (unsigned int idx = 0; idx < sz; ++idx, ++right, --dst)
            {
                *dst = *right;
            }
        }
        ++counter;
    }
    return ok;
}

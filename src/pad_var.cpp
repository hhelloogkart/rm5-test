#include <pad_var.h>
#include <outbuf.h>

vpad_var::vpad_var(int d) : desired_size(d)
{
}

int vpad_var::size() const
{
    const int derived = complex_var::size();
    return (derived > desired_size) ? derived : desired_size;
}

void vpad_var::output(outbuf& strm)
{
    const int oldsize = strm.size();
    complex_var::output(strm);
    for (int delta = strm.size() - oldsize; delta < desired_size; ++delta)
    {
        strm += '\0';
    }
}
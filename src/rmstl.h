/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _RMSTL_H_
#define _RMSTL_H_

#include <rmglobal.h>

#ifndef OSPACE
# if (defined(__GNUG__) && (__GNUG__ < 3)) || (defined(_HPUX_SOURCE) && !defined(__ia64))
#  include <iostream.h>
# else
#  if defined(__osf__) && !defined(__USE_STD_IOSTREAM)
#    define __USE_STD_IOSTREAM
#  endif
#  include <istream>
#  include <ostream>
# endif
# include <map>
# include <vector>
# include <list>
# include <set>
# include <string>
# ifndef NO_RTTI
#  include <typeinfo>
# endif
# ifndef RWSTD_NO_NAMESPACE
using std::map;
using std::vector;
using std::list;
using std::less;
using std::pair;
using std::allocator;
using std::set;
using std::string;
using std::endl;
using std::flush;
#  ifndef NO_RTTI
using std::type_info;
#  endif
# endif
#else
# include <ospace/std/list>
# include <ospace/std/map>
# include <ospace/std/vector>
# include <ospace/std/set>
# include <ospace/std/string>
using os_std::map;
using os_std::vector;
using os_std::list;
using os_std::less;
using os_std::set;
using os_std::pair;
using os_std::allocator;
using os_std::string;
using os_std::endl;
using os_std::flush;
#endif

#include <stlcast.h>

template<class C>
struct Node
{
    Node* next;
    C data;
};

template<class C>
class mpt_LinkedList_Iterator
{
    Node<C>* current;
    mpt_LinkedList_Iterator();
public:
    mpt_LinkedList_Iterator(const mpt_LinkedList_Iterator<C>& c) : current(c.current)
    {
    }
    explicit mpt_LinkedList_Iterator(Node<C>* c) : current(c)
    {
    }
    void operator++()
    {
        if (current)
        {
            current = current->next;
        }
    }
    bool end() const
    {
        return current == 0;
    }
    C operator*() const
    {
        return current->data;
    }
    C& operator*()
    {
        return current->data;
    }
    mpt_LinkedList_Iterator<C>& operator=(const mpt_LinkedList_Iterator<C>& c)
    {
        current = c.current;
        return *this;
    }
};

template<class C>
class mpt_LinkedList
{
    Node<C>* head;
public:
    typedef mpt_LinkedList_Iterator<C> iterator;
    mpt_LinkedList() : head(0)
    {
    }
    ~mpt_LinkedList()
    {
        clear();
    }
    void push_back(C d)
    {
        Node<C>* n = new Node<C>;
        n->data = d;
        n->next = 0;

        if (head == 0)
        {
            head = n;
        }
        else
        {
            Node<C>* m;
            for (m = head; m->next != 0; m = m->next)
            {
                ;
            }
            m->next = n;
        }
    }
    void remove(C d)
    {
        if (head->data == d)
        {
            Node<C>* m = head->next;
            delete head;
            head = m;
        }
        else
        {
            for (Node<C>* m = head; m->next != 0; m = m->next)
            {
                if (m->next->data == d)
                {
                    Node<C>* p = m->next->next;
                    delete m->next;
                    m->next = p;
                    break;
                }
            }
        }
    }
    bool empty() const
    {
        return head == 0;
    }
    void clear()
    {
        Node<C>* n = head;
        while (n != 0)
        {
            Node<C>* const m = n;
            n = n->next;
            delete m;
        }
        head = 0;
    }
    iterator begin()
    {
        return iterator(head);
    }
};

#endif  /* _RMSTL_H_ */


#ifndef _SYNCDEF_H_
#define _SYNCDEF_H_
#include <vector>
#include <map>
#include <list>
#include <string>
#include "../../rm/rmsocket.h"
#include <time/timecore.h>

using std::string;

struct varelem
{
    string first;
    int second;
    string third;
    varelem(int _s, const string& _f, const string& _f2=string()) : first(_f), second(_s), third(_f2.empty() ? _f : _f2)
    {
    }
};

typedef std::vector<varelem> varlist_typ;
typedef std::map<unsigned int, cache_typ*, std::less<unsigned int> > std_map;
typedef std::list<unsigned int> std_list;
class defaults;
class cache_typ;

class sync_defaults
{
public:
    sync_defaults();
    ~sync_defaults();
    void incomingData(unsigned int id, const cache_typ* buf);
    void incomingGetList(unsigned int id, const cache_typ* buf);
    void incomingClientID(unsigned int id, const cache_typ* buf);
    void outputQueuedPackets();
    bool waitForIncomingData();
    void maintainConnection();

    // Values from config file (fixed after init)
    struct timeval* select_tv;
    char host[256];
    char client[256];
    int port;
    int flow;
    int dispatch;
    int resettimeout;
    RMBaseSocket* sock;
    char myname;
    char othername;

    // Runtime status (shared by different threads)
    int sock_state; /*!< 0-Not connected, 1-Connected, 2-Ready */
    int external_close_requested; /*!< 0-Do nothing, 1-Close socket */
    int output_queue_has_data; /*!< 0-No data, 1 or more -Has data */
    bool reject_clientID; /*!< The first client ID after connection is ignored, otherwise output queue is cleared */
    std_list output_packet_id_queue; /*!< Queue of IDs pending transmission */
    long mutex; /*!< Guards access to sock AND output_packet_id_queue */
    struct timeval last_close_timestamp; /*!< Time when socket was closed */

    // Runtime status (thread-local)
    struct timeval last_connect_timestamp; /*!<Time when last connect was attempted */

    // Reference to other sync_defaults properties
    const int* other_sock_state;
    int* other_output_queue_has_data;
    std_list* other_output_packet_id_queue;
    RMBaseSocket* other_sock;
    long* other_mutex;
    struct timeval* other_last_close_timestamp;

    static varlist_typ varlist;
    static std_map id_data_pair;
    static bool cache;
};

/*!
    Initialize sync defaults from configuration file and command-line arguments
    sdef is an array of 2 sync_defaults objects
 */
bool init_sync_defaults(sync_defaults* sdef, int argc, char** argv);

#endif /* _SYNCDEF_H_ */

#ifndef _SENDER_SOCKET_H_
#define _SENDER_SOCKET_H_

#include <defaults.h>
class cache_typ;
struct SenderSocket_private;
struct timeval;

/*!
    Responsible for sending test messages, receiving messages
    and collecting test results. This is a generic test socket
    independent of any protocols.

    Sub-class need to overload the virtual functions and also
    route received packets to RecvData.
 */
class SenderSocket
{
public:
    SenderSocket(defaults*);
    virtual ~SenderSocket();

    /*! Call CheckConnected repeatedly until the connection is successful.
     */
    void WaitUntilConnected();

    /*! Wait up to a timeout value for data to arrive, calls Receive repeatedly.
     */
    void WaitForData(struct timeval*);

    /*! Send out one packet of test data
     */
    void SendData(int len);

    /*! Receive one packet of test data and store test results
     */
    void RecvData(unsigned char* buf, int len);

    /*! Output test results and reset test results

        Sub-classes that overload this method must call this baseclass method
        or else nothing will work!
     */
    virtual void OutputResults(char* filename);

    /*! Create a new data point, i.e. start a new row in the results
        Special configuration parameters may be passed for this data point

        Sub-classes that overload this method must call this baseclass method
        or else nothing will work!
     */
    virtual void NewDataPoint(defaults*, tree_node*);

    /*! Checks if already connected. If not, attempts to connect to the socket
        and returns true if successful.
     */
    virtual bool CheckConnected() = 0;

    /*! Retrieves a buffer to write to, to facilitate sending a packet
     */
    virtual cache_typ* SendStage1(int len) = 0;

    /*! Sends the packet. Returns true if successful.
     */
    virtual bool SendStage2(cache_typ* buf) = 0;

    /*! Waits up to the timeout value for a packet. Returns immediately when
        packet(s) are received.
     */
    virtual void Receive(struct timeval*) = 0;

#ifdef NITIMER
    void startNI(void);
    void stopNI(void);
#endif

protected:
    SenderSocket_private* prvt;

private:
    SenderSocket();
};
#endif /* _SENDER_SOCKET_H_ */


#include "sendsock.h"

#include <util/dbgout.h>
#include <util/utilcore.h>
#include <time/timecore.h>
#include <cache.h>

#include <list>
#include <string>
#include <stdlib.h>
#include "../../mhash/src/libdefs.h"
extern "C"
{
#include "../../mhash/src/mhash_crc32.h"
}

#ifdef NITIMER
#include <NIDAQmx.h>
#endif

#ifdef CONSOLE_OUT
struct report_typ
{
    void operator+=(char c);
    void operator+=(const char* str);
    report_typ(const char* str);
};
report_typ::report_typ(const char* str)
{
    dout << str;
}
void report_typ::operator+=(char c)
{
    dout << c;
}
void report_typ::operator+=(const char* str)
{
    dout << str;
}
#else
typedef std::string report_typ;
#endif

struct dataset
{
    unsigned int bytesSent;
    unsigned int bytesRecv;
    unsigned int maxLatency;
    double avgLatency;
    unsigned int error;
    unsigned int loss;
    unsigned int num_recv;
    unsigned int timetag_sec;
    unsigned int timetag_usec;
};

typedef std::list<dataset> datasetlist;

struct SenderSocket_private
{
    datasetlist dsl;
    int wnd;
    unsigned int out_seq;
    unsigned int in_seq;
    unsigned int starttime_sec;
    unsigned int starttime_usec;
#ifdef NITIMER
    TaskHandle outHandle;
    TaskHandle inHandle;
    bool useNI;
#endif
};

SenderSocket::SenderSocket(defaults* def) : prvt(new SenderSocket_private)
{
    prvt->wnd = 1;
    prvt->out_seq = 0;
    prvt->in_seq = 0;

#ifndef NO_DOUT
    char tstr[256];
    int ival;
// init dout with output options
    dout.useconsole(def->getnode("console") != NULL);
    if ((def->getstring("log", tstr) != NULL) && (tstr[0] != '\0'))
    {
        dout.openfile(tstr);
    }
    if ((def->getstring("udpaddr", tstr) != NULL) &&
        (def->getint("udport", &ival) != NULL))
    {
        dout.openudp(tstr, ival);
    }
#endif

    struct timeval tv;
    now(&tv);
    srand( (unsigned)tv.tv_sec );

#ifdef NITIMER
    prvt->outHandle = NULL;
    prvt->inHandle = NULL;
    prvt->useNI = false;
#endif
}

SenderSocket::~SenderSocket()
{
    delete prvt;
}

void SenderSocket::WaitUntilConnected()
{
    while (!CheckConnected())
    {
        ;
    }
}

void SenderSocket::WaitForData(struct timeval* tv)
{
    // time stamp
    timeExpire te(tv);
    do
    {
        // check connectivity
        bool conn;
        while (!(conn = CheckConnected()) && !te())
        {
            ;
        }
        // adjust time interval
        unsigned int left = te.left();
        struct timeval tvl = {
            left/1000, (left%1000)*1000
        };
        // wait for packets
        if (conn)
        {
            Receive(&tvl);
        }

    }
    while(!te());
    struct timeval now_tv;
    now(&now_tv);
    prvt->dsl.back().timetag_sec = now_tv.tv_sec;
    prvt->dsl.back().timetag_usec = now_tv.tv_usec;
}
#ifdef NITIMER
void SenderSocket::startNI(void)
{
    if(prvt->outHandle == NULL)
    {
        //creates a counter out task
        DAQmxCreateTask("",&prvt->outHandle);
        DAQmxCreateCOPulseChanFreq(prvt->outHandle,(char*)"Dev1/ctr0","",DAQmx_Val_Hz,DAQmx_Val_Low,0 /*initCount*/,10000.0,0.5); //1khz, pin 5
        DAQmxCfgImplicitTiming(prvt->outHandle,DAQmx_Val_ContSamps,1000);
    }

    if(prvt->inHandle == NULL)
    {
        //creates a counter in task
        DAQmxCreateTask("",&prvt->inHandle);
        DAQmxCreateCICountEdgesChan(prvt->inHandle,(const char*)"Dev1/ctr4","",DAQmx_Val_Falling,0 /*initCount*/,DAQmx_Val_CountUp); //pin 28
    }

    DAQmxStartTask(prvt->inHandle);
    DAQmxStartTask(prvt->outHandle);
    prvt->useNI = true;
}

void SenderSocket::stopNI(void)
{
    DAQmxStopTask(prvt->inHandle);
    DAQmxStopTask(prvt->outHandle);
    prvt->useNI = false;
}
#endif

void SenderSocket::SendData(int len)
{
    /* Correct the length if needed, >= 16 and multiple of 4 */
    if (len < 16)
    {
        len = 16;
    }
    int rem = len%sizeof(int);
    if (rem != 0)
    {
        len += sizeof(int)-rem;
    }

    /* Get buffer for sending */
    cache_typ* cbuf = SendStage1(len);
    if (cbuf == 0)
    {
        return;
    }

    /* Populate the buffer. CRC32, seq no, timetag(sec), timetag(usec), random number ... */
    unsigned int* buf = cbuf->buf();
    struct timeval now_tv;
    int wlen = len / sizeof(int);
    for (int idx=4; idx<wlen; ++idx)
    {
        buf[idx] = rand();
    }
    buf[1] = prvt->out_seq;

#ifdef NITIMER
    if(prvt->useNI)
    {
        unsigned int value;
        DAQmxReadCounterScalarU32(prvt->inHandle,10.0,(uInt32*)(&value),NULL);
        buf[2] = value;
        buf[3] = 0; //unused
    }
    else
#endif
    {
        now(&now_tv);
        buf[2] = now_tv.tv_sec;
        buf[3] = now_tv.tv_usec;
    }

    /* Compute CRC-32 */
    mutils_word32 crc;
    mhash_clear_crc32(&crc);
    mhash_crc32(&crc, buf+1, len-sizeof(unsigned int)); // the crc is packed at the beginning
    *(reinterpret_cast<mutils_word32*>(buf)) = crc;

    /* Send buffer */
    if (SendStage2(cbuf))
    {
        /* Buffer send successful */
        ++(prvt->out_seq);
        prvt->dsl.back().bytesSent += len;
    }
}

void SenderSocket::RecvData(unsigned char* buf, int len)
{
    /* Time stamp */
    struct timeval now_tv;
    now(&now_tv);

    /* Helper variables */
    unsigned int* pbuf = reinterpret_cast<unsigned int*>(buf);
    struct dataset& ds = prvt->dsl.back();

    /* Check CRC-32 */
    mutils_word32 crc;
    mhash_clear_crc32(&crc);
    mhash_crc32(&crc, pbuf+1, len-sizeof(unsigned int));

    if (crc != pbuf[0])
    {
        /* CRC fail */
        ds.error += 1;
        return;
    }
    unsigned int diff;
#ifdef NITIMER
    if(prvt->useNI)
    {
        unsigned int value;
        DAQmxReadCounterScalarU32(prvt->inHandle,10.0,(uInt32*)(&value),NULL);
        diff = value - pbuf[2];
    }
    else
#endif
    {
        /* Measure latency */
        diff = (now_tv.tv_sec - pbuf[2]) * 1000U;
        if (now_tv.tv_usec >= (long)pbuf[3])
        {
            diff += (now_tv.tv_usec - pbuf[3]) / 1000U;
        }
        else
        {
            diff -= (pbuf[3] - now_tv.tv_usec) / 1000U;
        }

        /* Check if the latency is > 20sec, indicate something is not right */
        if ((prvt->in_seq == 0) &&
            ((diff > 10000) || (pbuf[1] > 90)))
        {
            return;
        }
    }

    /* Measure lost */
    if (prvt->in_seq == pbuf[1])
    {
        ++(prvt->in_seq);
    }
    else if (pbuf[1] > prvt->in_seq)
    {
        /* loss */
        ds.loss += pbuf[1] - prvt->in_seq;
        prvt->in_seq = pbuf[1] + 1;
    }
    else
    {
        dout << "Suspicious packet of lower sequence received (seq=" << pbuf[1] << " expected=" << prvt->in_seq << ")\n";
        return;
    }

    /* Record bytes recieved */
    ds.bytesRecv += len;

    if (ds.num_recv == 0)
    {
        ds.avgLatency = ds.maxLatency = diff;
    }
    else
    {
        if (diff > ds.maxLatency)
        {
            ds.maxLatency = diff;
        }
        ds.avgLatency = ((ds.avgLatency * ds.num_recv) + diff) / static_cast<double>(ds.num_recv+1);
    }

    /* Increment received packet */
    ++(ds.num_recv);
}

#ifndef CONSOLE_OUT
void SenderSocket::OutputResults(char* filename)
#else
void SenderSocket::OutputResults(char*)
#endif
{
    static char header[] = "Cycle,Aggregate Receive Bandwidth,Windowed Receive Bandwidth,Aggregate Send Bandwidth,Windowed Send Bandwidth,Avg Latency,Max Latency,Error,Loss,Num Packets Recv,Elapsed Time\n";
    report_typ filestr = header;
    char text[256];
    unsigned int latency = 0;
    unsigned long long tot_send = 0;
    unsigned long long tot_recv = 0;
    unsigned int cycle = 0;
    unsigned int num_recv = 0;

    for (datasetlist::iterator iter = prvt->dsl.begin(); iter != prvt->dsl.end(); ++iter, ++cycle)
    {
        /* Cycle */
        rm_sprintf(text, 256, "%d", cycle);
        filestr += text;
        filestr += ',';

        /* Cycle Time in msec */
        unsigned int cyctime = ((*iter).timetag_sec - prvt->starttime_sec) * 1000U;
        if ((*iter).timetag_usec >= prvt->starttime_usec)
        {
            cyctime += ((*iter).timetag_usec - prvt->starttime_usec) / 1000U;
        }
        else
        {
            cyctime -= (prvt->starttime_usec - (*iter).timetag_usec) / 1000U;
        }

        /* Window Start */
        datasetlist::iterator wnd = iter;
        unsigned int wndtime;
        int cnt;
        for (cnt=0; (cnt < prvt->wnd) && (wnd != prvt->dsl.begin()); ++cnt, --wnd)
        {
            ;
        }
        if (cnt == prvt->wnd)
        {
            /* Enough elements in list */
            wndtime = ((*iter).timetag_sec - (*wnd).timetag_sec) * 1000U;
            if ((*iter).timetag_usec >= (*wnd).timetag_usec)
            {
                wndtime += ((*iter).timetag_usec - (*wnd).timetag_usec) / 1000U;
            }
            else
            {
                wndtime -= ((*wnd).timetag_usec - (*iter).timetag_usec) / 1000U;
            }
            ++wnd;
        }
        else
        {
            /* Short */
            wndtime = cyctime;
        }

        /* Aggregate Receive Bandwidth */
        tot_recv += (*iter).bytesRecv;
        rm_sprintf(text, 256, "%.4f", static_cast<double>(tot_recv * 1000U) / static_cast<double>(cyctime));
        filestr += text;
        filestr += ',';

        /* Windowed Receive Bandwidth */
        if (wndtime != 0)
        {
            unsigned int wndrecv = (*iter).bytesRecv;
            for (datasetlist::iterator it = wnd; it != iter; ++it)
            {
                wndrecv += (*it).bytesRecv;
            }
            rm_sprintf(text, 256, "%.4f", static_cast<double>(wndrecv * 1000U) / static_cast<double>(wndtime));
            filestr += text;
            filestr += ',';
        }
        else
        {
            filestr += "0,";
        }

        /* Aggregate Send Bandwidth */
        tot_send += (*iter).bytesSent;
        rm_sprintf(text, 256, "%.4f", static_cast<double>(tot_send * 1000U) / static_cast<double>(cyctime));
        filestr += text;
        filestr += ',';

        /* Windowed Send Bandwidth */
        if (wndtime != 0)
        {
            unsigned int wndsend = (*iter).bytesSent;
            for (datasetlist::iterator it = wnd; it != iter; ++it)
            {
                wndsend += (*it).bytesSent;
            }
            rm_sprintf(text, 256, "%.4f", static_cast<double>(wndsend * 1000U) / static_cast<double>(wndtime));
            filestr += text;
            filestr += ',';
        }
        else
        {
            filestr += "0,";
        }

        /* Average Latency */
        if ((*iter).num_recv > 0)
        {
            /* We divide by 2 for round-trip */
            double d_latency = (*iter).avgLatency / 2.0;
            unsigned int i_latency = static_cast<int>(d_latency);
            if (d_latency - static_cast<double>(i_latency) >= 0.5)
            {
                ++i_latency;
            }
            latency = i_latency;
        }
        rm_sprintf(text, 256, "%d", latency);
        filestr += text;
        filestr += ',';

        /* Max Latency */
        if ((*iter).num_recv > 0)
        {
            /* We divide by 2 for round-trip */
            if ((*iter).maxLatency % 2 != 0)
            {
                rm_sprintf(text, 256, "%d", ((*iter).maxLatency+1U)/2U);
            }
            else
            {
                rm_sprintf(text, 256, "%d", ((*iter).maxLatency)/2U);
            }
            filestr += text;
            filestr += ',';
        }
        else
        {
            filestr += "0,";
        }

        /* Error */
        rm_sprintf(text, 256, "%d", (*iter).error);
        filestr += text;
        filestr += ',';

        /* Loss */
        rm_sprintf(text, 256, "%d", (*iter).loss);
        filestr += text;
        filestr += ',';

        /* Total packets received */
        num_recv += (*iter).num_recv;
        rm_sprintf(text, 256, "%d", num_recv);
        filestr += text;
        filestr += ',';

        rm_sprintf(text, 256, "%u", cyctime);
        filestr += text;
        filestr += '\n';
    }

    prvt->dsl.clear();
    prvt->wnd = 1;
    prvt->out_seq = 0;
    prvt->in_seq = 0;

#ifndef CONSOLE_OUT
    file_buffer PrintFile(filename);
    PrintFile.writefile(filestr.data(), filestr.length());
#endif
}

void SenderSocket::NewDataPoint(defaults* def, tree_node* tn)
{
    if (prvt->wnd == 1)
    {
        if (def->getint("WindowSize", &prvt->wnd, tn) == 0) // Window Length
        {
            prvt->wnd = 10;
        }
        struct timeval now_tv;
        now(&now_tv);
        prvt->starttime_sec = now_tv.tv_sec;
        prvt->starttime_usec = now_tv.tv_usec;
    }
    dataset dp = {
        0, 0, 0, 0.0, 0, 0, 0
    };
    prvt->dsl.push_back(dp);
}

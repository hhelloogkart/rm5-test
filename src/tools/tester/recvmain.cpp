#include <defaults.h>
#include <util/dbgout.h>
#include <time/timecore.h>
#include <comm/sercore.h>
#include <cache.h>
#include "../../rm/rmsocket.h"
#include <list>

typedef std::list<cache_typ*> buflist_typ;
buflist_typ buflist;
#ifdef INTEGRITY
extern const char recv_cfg[];
#endif

static void dataInSL(void*, unsigned int id, const cache_typ* buf)
{
    // We should not send data directly from the callback, may cause problems
    cache_typ* cac = cache_typ::h_duplicate(id+1, buf->get_len(), buf->buf());
    cac->set_cmd(SETLIST);
    buflist.push_back(cac);
}

static void dataInBC(void*, unsigned int id, const cache_typ* buf)
{
    // We should not send data directly from the callback, may cause problems
    cache_typ* cac = cache_typ::h_duplicate(id+1, buf->get_len(), buf->buf());
    cac->set_cmd(BROADCAST);
    buflist.push_back(cac);
}

static void dataInMS(void*, unsigned int id, const cache_typ* buf)
{
    // We should not send data directly from the callback, may cause problems
    cache_typ* cac = cache_typ::h_duplicate(id+1, buf->get_len(), buf->buf());
    cac->set_cmd(MESSAGE);
    buflist.push_back(cac);
}

int main(int argc, char** argv)
{
    // 1. Load configuration
#ifdef INTEGRITY
    defaults* def = load_defaults(recv_cfg, argv[0]);
#else
    defaults* def = load_defaults("+recv.cfg", argv[0]);
#endif
    if (!def)
    {
        return 1;
    }
    def->parse_cmdline(argc, argv);

    // 2. Init the output options
#ifndef NO_DOUT
    char tstr[256];
    int ival;
    dout.useconsole(def->getnode("console") != NULL);
    if ((def->getstring("log", tstr) != NULL) && (tstr[0] != '\0'))
    {
        dout.openfile(tstr);
    }
    if ((def->getstring("udpaddr", tstr) != NULL) &&
        (def->getint("udport", &ival) != NULL))
    {
        dout.openudp(tstr, ival);
    }
#endif

    // 3. Create the socket
    char host[64];
    int port;
    char client[32];
    int flow;
    // get hostnames
    if (def->getstring("host", host) == NULL)
    {
        dout << "No hostname\n";
        return 1;
    }
    // get port numbers
    if (def->getint("port", &port) == NULL)
    {
        dout << "No port number\n";
        return 1;
    }
    // get client name
    if (def->getstring("client", client) == NULL)
    {
        dout << "No client name\n";
        return 1;
    }
    // get flow control
    if (def->getint("flow", &flow) == NULL)
    {
        dout << "No flow\n";
        return 1;
    }

    // Set up the socket
    RMBaseSocket* sock = RMBaseSocket::Construct(def->getString("access"));
    if (sock == 0)
    {
        dout << "Unable to create socket\n";
        return 1;
    }

    //socket add call backs - will crash if socket creation fails
    sock->AddCallback(SETLIST, 0, &dataInSL);
    sock->AddCallback(BROADCAST, 0, &dataInBC);
    sock->AddCallback(MESSAGE, 0, &dataInMS);

    for (;;)
    {
        while (!sock->Connected())
        {
            // Settle connection matters
            if (sock->Connect(host, port))
            {
                sock->FlowControl(flow);
                sock->ClientID(client);

                char sndname[] = "SEND000";
                char rcvname[] = "RECV000";
                for (int idx = 1; idx <= 127; ++idx)
                {
                    // Output
                    rm_sprintf(sndname+4, 4, "%03d", idx); // Odd
                    sock->RegisterID(sndname);
                    // Input
                    rm_sprintf(rcvname+4, 4, "%03d", idx); // Even
                    sock->QueryID(rcvname);
                    struct timeval tv = {
                        0, 0
                    };
                    if (sock->Select(&tv, true) == -1)
                    {
                        sock->Close();
                        continue;
                    }
                }
                struct timeval stm = {
                    68, 0
                };
                if (!sock->WaitForHandshakeCompletion(&stm))
                {
                    dout << "No reply from RM\n";
                    sock->Close();
                    continue;
                }
                break;
            }
        }

        struct timeval swait = {
            60, 0
        };
        int result = sock->Select(&swait);
        if (result == -1)
        {
            sock->Close();
            buflist.clear();
        }
        else if (result > 0)
        {
            buflist_typ::iterator iter;
            for (iter = buflist.begin(); iter != buflist.end(); ++iter)
            {
                cache_typ* cac;
                unsigned int cmd = (*iter)->get_cmd();
                if (cmd == MESSAGE)
                {
                    cac = sock->MessageStart((*iter)->get_id(), (*iter)->get_len());
                }
                else
                {
                    cac = sock->SetListStart((*iter)->get_id(), (*iter)->get_len());
                }
                if (cac == NULL)
                {
                    break;
                }
                memcpy(cac->buf(), (*iter)->buf(), (*iter)->get_len()*sizeof(int));

                if (cmd == SETLIST)
                {
                    if (!sock->SetListEnd((*iter)->get_id(), cac))
                    {
                        break;
                    }
                }
                else if (cmd == BROADCAST)
                {
                    if (!sock->BroadcastEnd((*iter)->get_id(), cac))
                    {
                        break;
                    }
                }
                else if (cmd == MESSAGE)
                {
                    if (!sock->MessageEnd((*iter)->get_id(), cac))
                    {
                        break;
                    }
                }
                else
                {
                    sock->GetListEnd((*iter)->get_id(), cac);
                }
                (*iter)->h_unreserve();
            }
            if (iter != buflist.begin())
            {
                buflist.erase(buflist.begin(), iter);
                sock->Flush();
            }
        }
    }

    delete def;
    delete sock;

    return 0;
}
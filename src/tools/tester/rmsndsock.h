#ifndef _RM_SENDER_SOCKET_
#define _RM_SENDER_SOCKET_

#include "sendsock.h"

class RMBaseSocket;

/*!
    This extends SenderSocket to faciliate testing over the RM
    protocol. This socket will use RM names SEND001 to SEND127
    and RECV001 to RECV127 to send and receive respectively. Odd
    IDs 1,3,5,7...253 are used for sending. The receiver program
    will return the messages in IDs 2,4,6,8...254.
 */
class RM_SenderSocket : public SenderSocket
{
public:
    RM_SenderSocket(defaults*);
    virtual ~RM_SenderSocket();
    virtual bool CheckConnected();
    virtual cache_typ* SendStage1(int len);
    virtual bool SendStage2(cache_typ* buf);
    virtual void Receive(struct timeval*);
    virtual void NewDataPoint(defaults*, tree_node*);
    virtual void OutputResults(char* filename);

protected:
    RMBaseSocket* sock;
    char msg_typ;
    char host[64];
    int port;
    char client[32];
    int flow;
    int testid;
    bool toflush;
    bool flush;
};

#endif /* _RM_SENDER_SOCKET_ */

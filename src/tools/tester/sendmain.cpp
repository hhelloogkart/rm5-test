#include <defaults.h>
#include <util/dbgout.h>
#include <time/timecore.h>
#include "rmsndsock.h"

#ifdef INTEGRITY
extern const char sender_cfg[];
#endif

int main(int argc, char** argv)
{
    // 1. Load configuration
#ifdef INTEGRITY
    defaults* def = load_defaults(sender_cfg,argv[0]);
#else
    defaults* def = load_defaults("+send.cfg", argv[0]);
#endif

    if (!def)
    {
        return 1;
    }
    def->parse_cmdline(argc, argv);

    // 2. Create the tester socket
    RM_SenderSocket* sock = new RM_SenderSocket(def);

    // 3. Execute each run
    tree_node* tn = 0, * ch;
    for (tn = def->getnode("run", tn); (tn != 0) && ((ch = def->getChild(tn)) != 0);
         tn = def->getnode("run", tn))
    {
        // 3.1 Read the run parameters
        char ResultFile[256] = "output.txt"; // The name of the file
        int nNumOfCycle = 1;                 // Number of cycle
        int nPauseLength = 20;               // The length of pause in ms
        int nStartLength = 16;               // The start length in bytes
        int nEndLength = 1024;               // The end length in bytes
        int nIncrement = 4;                  // Increment in bytes
        int nTestPeriod = 5000;              // Length of time for the test in ms

        def->getstring("Name", ResultFile, ch);         // The name of the file
        def->getint("TotalCycle", &nNumOfCycle, ch);    // Number of cycle
        def->getint("Pause", &nPauseLength, ch);        // The length of pause in ms
        def->getint("StartLen", &nStartLength, ch);     // The start length in bytes
        def->getint("EndLen", &nEndLength, ch);         // The end length in bytes
        def->getint("LenIncrement", &nIncrement, ch);   // Increment in bytes
        def->getint("TestTime", &nTestPeriod, ch);      // Length of time for the test in ms

        /* 3.2 We start each test run ensuring that the link is connected */
        sock->WaitUntilConnected();

#ifdef NITIMER
        char timerMethod[32] = "O"; //Default to OS method
        def->getstring("timer", timerMethod, ch);
        char timerType = timerMethod[0];
        if(timerType == 'N')
        {
            sock->startNI();
        }
#endif

        /* 3.3 Start the clock */
        struct timeval te = {
            nTestPeriod/1000, (nTestPeriod%1000)*1000
        };
        timeExpire expire(&te);

        int pktlen = nStartLength; // Init the packet length

        /* 3.4 Runs for the number of cycles */
        for (int cycle=0; (cycle < nNumOfCycle) && !expire(); ++cycle)
        {
            /* 3.4.1 Start a new row of recording - one for each cycle */
            sock->NewDataPoint(def, ch);
            /* 3.4.2 Send one packet of data */
            sock->SendData(pktlen);
            /* 3.4.3 Pause */
            unsigned int el = nTestPeriod - expire.left();
            unsigned int tt = (cycle+1)*nPauseLength;
            if (el >= tt)
            {
                /* No time left */
                struct timeval tv = {
                    0, 0
                };
                sock->WaitForData(&tv);
            }
            else
            {
                tt -= el;
                struct timeval tv = {
                    tt/1000, (tt%1000)*1000
                };
                sock->WaitForData(&tv);
            }
            /* 3.4.4 Packet Length Increase */
            pktlen+=nIncrement;
            if (pktlen > nEndLength)
            {
                pktlen = nStartLength;
            }
        }
        /* 3.5 Continue running until the time is up */
        while (!expire())
        {
            /* 3.5.1 Start a new row of recording - extra cycle */
            sock->NewDataPoint(def, ch);

            /* 3.5.2 Pause for a cycle time */
            struct timeval tv = {
                nPauseLength/1000, (nPauseLength%1000)*1000
            };
            sock->WaitForData(&tv);
        }

#ifdef NITIMER
        if(timerType == 'N')
        {
            sock->stopNI();
        }
#endif

        /* 3.6 Write the results to file */
        sock->OutputResults(ResultFile);
    }

    /* 4. Clean up */
    delete def;
    delete sock;
    return 0;
}

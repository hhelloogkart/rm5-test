#include "rmsndsock.h"
#include "../../rm/rmsocket.h"
#include <cache.h>
#include <util/dbgout.h>
#include <time/timecore.h>

static void dataIn(void* arg, unsigned int id, const cache_typ* buf)
{
    if (id > 254)
    {
        dout << "Invalid packet: ID more that 254\n";
        return;
    }
    if ((id % 2) == 1)
    {
        dout << "Invalid packet: ID is odd number\n";
        return;
    }
    reinterpret_cast<RM_SenderSocket*>(arg)->RecvData((unsigned char*)buf->buf(), buf->get_len() * sizeof(int));
}

RM_SenderSocket::RM_SenderSocket(defaults* def) : SenderSocket(def), sock(0), msg_typ('S'), testid(1), toflush(false)
{
    // get hostnames
    if (def->getstring("host", host) == NULL)
    {
        dout << "No hostname\n";
    }
    // get port numbers
    if (def->getint("port", &port) == NULL)
    {
        dout << "No port number\n";
    }
    // get client name
    if (def->getstring("client", client) == NULL)
    {
        dout << "No client name\n";
    }
    // get flow control
    if (def->getint("flow", &flow) == NULL)
    {
        dout << "No flow\n";
    }

    flush = (def->getnode("flush") != NULL);

    // Set up the socket
    sock = RMBaseSocket::Construct(def->getString("access"));
    if (sock == 0)
    {
        dout << "Unable to create socket\n";
    }

    //socket add call backs - will crash if socket creation fails
    sock->AddCallback(SETLIST, this, &dataIn);
    sock->AddCallback(BROADCAST, this, &dataIn);
    sock->AddCallback(MESSAGE, this, &dataIn);
}

RM_SenderSocket::~RM_SenderSocket()
{
    delete sock;
}

bool RM_SenderSocket::CheckConnected()
{
    if (!sock->Connected())
    {
        if (sock->Connect(host, port))
        {
            sock->FlowControl(flow);
            sock->ClientID(client);

            char sndname[] = "SEND000";
            char rcvname[] = "RECV000";
            for (int idx = 1; idx <= 127; ++idx)
            {
                // Output
                rm_sprintf(sndname+4, 4, "%03d", idx); // Odd
                sock->QueryID(sndname);
                // Input
                rm_sprintf(rcvname+4, 4, "%03d", idx); // Even
                sock->RegisterID(rcvname);
                struct timeval tv = {
                    0, 0
                };
                if (sock->Select(&tv, true) == -1)
                {
                    sock->Close();
                    return false;
                }
            }
            struct timeval stm = {
                68, 0
            };
            if (!sock->WaitForHandshakeCompletion(&stm))
            {
                dout << "No reply from RM\n";
                sock->Close();
                return false;
            }
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return true;
    }
}

cache_typ* RM_SenderSocket::SendStage1(int len)
{
    int wlen = len / sizeof(int);
    if ((len%sizeof(int)) != 0)
    {
        ++wlen;
    }
    toflush = false;
    if (msg_typ == 'M')
    {
        return sock->MessageStart(testid, wlen);
    }
    else
    {
        return sock->SetListStart(testid, wlen);
    }

}

bool RM_SenderSocket::SendStage2(cache_typ* buf)
{
    int oldtestid = testid;
    testid += 2;
    testid %= 128;
    bool result;
    if      (msg_typ == 'S')
    {
        result = sock->SetListEnd(oldtestid, buf);
    }
    else if (msg_typ == 'B')
    {
        result = sock->BroadcastEnd(oldtestid, buf);
    }
    else if (msg_typ == 'M')
    {
        result = sock->MessageEnd(oldtestid, buf);
    }
    else
    {
        RecvData((unsigned char*)buf->buf(), buf->get_len() * sizeof(int));
        sock->GetListEnd(oldtestid, buf);
        return true;
    }
    if (flush)
    {
        sock->Flush();
    }
    return result;
}

void RM_SenderSocket::Receive(struct timeval* tv)
{
    if (CheckConnected())
    {
        if (toflush)
        {
            sock->Flush();
            toflush = false;
        }
        if (sock->Select(tv) == -1)
        {
            sock->Close();
        }
    }
}

void RM_SenderSocket::NewDataPoint(defaults* def, tree_node* tn)
{
    char MessageType[32] = "S";    // The message type
    def->getstring("MessageType", MessageType, tn); // The message type
    msg_typ = MessageType[0];
    toflush = !flush;

    SenderSocket::NewDataPoint(def, tn);
}

void RM_SenderSocket::OutputResults(char* filename)
{
    testid = 1; // Reset the test ID
    SenderSocket::OutputResults(filename);
}

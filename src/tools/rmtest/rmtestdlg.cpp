/****************************************************************************
** Form implementation generated from reading ui file 'rmtest.ui'
**
** Created: Tue Jan 8 11:59:35 2002
**      by:  The User Interface Compiler (uic)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/
#include "rmtestdlg.h"

#include <qpushbutton.h>
#include <qlayout.h>
#include <qvariant.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>
#include <qapplication.h>
#include <qtimer.h>

#include "qconsole.h"
#include "rmtestmod.h"
#include "rmmod.h"    // To be taken out when DLLed
#include "filemod.h"  // To be taken out when DLLed
#include "dbgmod.h"   // To be taken out when DLLed

/*
 *  Constructs a RMTestDlg which is a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
RMTestDlg::RMTestDlg( defaults* _def, QWidget* parent, const char* name, WFlags fl )
    : QMainWindow( parent, fl ), def(_def), echo_flag(false)
{
    if ( !name )
    {
        setObjectName( "RMTestDlg" );
    }
    resize( 639, 507 );
    setWindowTitle( tr( "RM Test" ) );

    CmdFrame = new QConsole( this, "CmdFrame" );
    CmdFrame->setCursor( QCursor( Qt::IBeamCursor ) );
    CmdFrame->setFrameShape( QFrame::Box );
    CmdFrame->setFrameShadow( QFrame::Raised );
    CmdFrame->setFocus();
    CmdFrame->setPrompt(QString(">"));
    init_command_widget();
    setCentralWidget( CmdFrame );

    // initialize the CmdFrame from defaults
    connect( CmdFrame, SIGNAL(commandOut(QString,void*)), this, SLOT(processCmd(QString,void*)) );
    connect( this, SIGNAL(lineOut(QString)), CmdFrame, SLOT(writeLine(QString)) );

    RMTestModule* mod;

    // instantiate all modules
    mod = filemod_init(def, this);
    modlist.append(mod);
    mod = rmmod_init(def, this);
    modlist.append(mod);
    mod = dbgmod_init(def, this);
    modlist.append(mod);
    // -----------------------

    QList< RMTestModule* >::Iterator iter;
    for (iter = modlist.begin(); iter != modlist.end(); ++iter)
    {
        connect( *iter, SIGNAL(commandOut(QString,void*)), this, SLOT(processCmd(QString,void*)) );
        connect( *iter, SIGNAL(lineOut(QString)), CmdFrame, SLOT(writeLine(QString)) );
        connect( *iter, SIGNAL(output(const QString&)), CmdFrame, SLOT(output(const QString&)) );
        connect( *iter, SIGNAL(output(char)), CmdFrame, SLOT(output(char)) );
        connect( *iter, SIGNAL(output(uchar)), CmdFrame, SLOT(output(uchar)) );
        connect( *iter, SIGNAL(output(short)), CmdFrame, SLOT(output(short)) );
        connect( *iter, SIGNAL(output(ushort)), CmdFrame, SLOT(output(ushort)) );
        connect( *iter, SIGNAL(output(int)), CmdFrame, SLOT(output(int)) );
        connect( *iter, SIGNAL(output(uint)), CmdFrame, SLOT(output(uint)) );
        connect( *iter, SIGNAL(output(long)), CmdFrame, SLOT(output(long)) );
        connect( *iter, SIGNAL(output(ulong)), CmdFrame, SLOT(output(ulong)) );
        connect( *iter, SIGNAL(output(float)), CmdFrame, SLOT(output(float)) );
        connect( *iter, SIGNAL(output(double)), CmdFrame, SLOT(output(double)) );
        connect( *iter, SIGNAL(setPrompt(const QString&)), CmdFrame, SLOT(setPrompt(const QString&)) );
    }
    QTimer::singleShot( 800, this, SLOT(autoscript()) );
}

/*
 *  Destroys the object and frees any allocated resources
 */
RMTestDlg::~RMTestDlg()
{
    // no need to delete child widgets, Qt does it all for us
}

void RMTestDlg::processCmd(QString cmd, void* src)
{
    if (echo_flag)
    {
        lineOut(cmd);
    }
    if (((void*)this != src) && commandIn(cmd))
    {
        return;
    }
    QList< RMTestModule* >::Iterator iter;
    for (iter = modlist.begin(); iter != modlist.end(); ++iter)
    {
        if (((void*)(*iter) != src) &&
            ((*iter)->commandIn(cmd)))
        {
            break;
        }
    }
    if ((iter == modlist.end()) && (RMTestModule::getNext(cmd) != "help"))
    {
        lineOut("[Error RMTest: Unrecognised command]");
    }
}

bool RMTestDlg::commandIn(QString cmd)
{
    cmd = cmd.simplified();
    QString arg(RMTestModule::getNext(cmd));

    if (arg == "echo")
    {
        if (cmd.length() > 0)
        {
            QString tstr(RMTestModule::getNext(cmd));
            if (tstr == "on")
            {
                echo_flag = true;
            }
            else if (tstr == "off")
            {
                echo_flag = false;
            }
        }
        else
        {
            if (echo_flag)
            {
                lineOut("[Main] echo on");
            }
            else
            {
                lineOut("[Main] echo off");
            }
        }
        return true;
    }
    else if (arg == "clear")
    {
        CmdFrame->clearscr();
        return true;
    }
    else if (arg == "screendump")
    {
        if (cmd.length() > 0)
        {
            CmdFrame->screendump(RMTestModule::getNext(cmd));
        }
        else
        {
            lineOut("Error Main: Enter a filename as argument]");
        }
        return true;
    }
    else if (arg == "history")
    {
        CmdFrame->showhistory();
        return true;
    }
    else if (arg == "historydump")
    {
        if (cmd.length() > 0)
        {
            CmdFrame->historydump(RMTestModule::getNext(cmd));
        }
        else
        {
            lineOut("Error Main: Enter a filename as argument]");
        }
        return true;
    }
    else if (arg == "capture")
    {
        if (cmd.length() > 0)
        {
            CmdFrame->capture(RMTestModule::getNext(cmd));
        }
        else
        {
            lineOut("Error Main: Enter a filename as argument]");
        }
        return true;
    }
    else if (arg == "endcapture")
    {
        CmdFrame->capture("");
        return true;
    }
    else if (arg == "help")
    {
        lineOut("Main Module");
        lineOut("    echo [on|off]");
        lineOut("    clear");
        lineOut("    screendump filename");
        lineOut("    historydump filename");
        lineOut("    history");
        lineOut("    capture filename");
        lineOut("    endcapture");
        lineOut("    quit");
        return false;
    }
    else if (arg == "quit")
    {
        QApplication::exit(0);
    }
    return false;
}

void RMTestDlg::commandOut(QString cmd, void* src)
{
    processCmd(cmd, src);
}

void RMTestDlg::init_command_widget()
{
    char tstr[1024];
    int tint;
    QFont* font = NULL;

    if (def == NULL)
    {
        return;
    }
    if ((def->getint("scrollback", &tint) != NULL) &&
        (tint > 0))
    {
        CmdFrame->setBufferSize((unsigned int)tint);
    }
    if ((def->getint("history", &tint) != NULL) &&
        (tint > 0))
    {
        CmdFrame->setHistorySize((unsigned int)tint);
    }
    if (def->getstring("fontname", tstr) != NULL)
    {
        font = new QFont;
        font->setFamily(QString(tstr));
    }
    if (def->getstring("fontweight", tstr) != NULL)
    {
        QString tstr1(tstr);
        if (!font)
        {
            font = new QFont;
        }
        if (tstr1 == "Light")
        {
            font->setWeight(25);
        }
        else if (tstr1 == "DemiBold")
        {
            font->setWeight(63);
        }
        else if (tstr1 == "Bold")
        {
            font->setWeight(75);
        }
        else if (tstr1 == "Black")
        {
            font->setWeight(87);
        }
        else
        {
            font->setWeight(50);
        }
    }
    if ((def->getint("fontsize", &tint) != NULL) &&
        (tint > 0))
    {
        if (!font)
        {
            font = new QFont;
        }
        font->setPointSize(tint);
    }
    if (font)
    {
        CmdFrame->setFont(*font);
    }
    if ((def->getstring("foreground", tstr) != NULL) &&
        (tint > 0))
    {
        QPalette pal(palette());
        pal.setColor(QPalette::Foreground, QColor(tstr));
        setPalette(pal);
    }
    if ((def->getstring("background", tstr) != NULL) &&
        (tint > 0))
    {
        QPalette pal(CmdFrame->palette());
        pal.setColor(QPalette::Window, QColor(tstr));
        pal.setColor(QPalette::Base, QColor(tstr));
        CmdFrame->setPalette(pal);
    }
    if (def->getstring("echo", tstr) != NULL)
    {
        QString tstr1(tstr);
        if (tstr1 == "off")
        {
            echo_flag = false;
        }
        else if (tstr1 == "on")
        {
            echo_flag = true;
        }
    }
}

void RMTestDlg::autoscript()
{
    char tstr[1024];
    tree_node* node = NULL;
    if (def == NULL)
    {
        return;
    }
    while ((node = def->getstring("run", tstr, node)) != NULL)
    {
        QString cmd("run ");
        cmd += tstr;
        commandOut(cmd, this);
    }
}

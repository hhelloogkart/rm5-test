/****************************************************************************
** Form implementation generated from reading ui file 'rmtest.ui'
**
** Created: Tue Jan 8 11:59:35 2002
**      by:  The User Interface Compiler (uic)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/
#include "cmdline.h"
#include <util/dbgout.h>

#include "rmtestmod.h"
#include "rmmod.h"    // To be taken out when DLLed
#include "filemod.h"  // To be taken out when DLLed
#include "dbgmod.h"  // To be taken out when DLLed

/*
 *  Constructs a CmdLine which is a child of 'parent', with the
 *  name 'name'
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
CmdLine::CmdLine(defaults* _def, QObject* parent, const char* name)
    : QObject(parent), def(_def)
{
    setObjectName(name);
    init_command_widget();

    RMTestModule* mod;

    // instantiate all modules
    mod = filemod_init(def, this);
    modlist.append(mod);
    mod = rmmod_init(def, this);
    modlist.append(mod);
    mod = dbgmod_init(def, this);
    modlist.append(mod);
    // -----------------------

    QList< RMTestModule* >::Iterator iter;
    for (iter = modlist.begin(); iter != modlist.end(); ++iter)
    {
        connect(*iter, SIGNAL(commandOut(QString,void*)), this, SLOT(processCmd(QString,void*)));
        connect(*iter, SIGNAL(lineOut(QString)), this, SLOT(lineOut(QString)));
    }
}

/*
 *  Destroys the object and frees any allocated resources
 */
CmdLine::~CmdLine()
{
    // no need to delete child widgets, Qt does it all for us
    QList< RMTestModule* >::Iterator iter;
    for (iter = modlist.begin(); iter != modlist.end(); ++iter)
    {
        delete *iter;
    }
}

void CmdLine::processCmd(QString cmd, void* src)
{
    if (echo_flag)
    {
        lineOut(cmd);
    }
    if (((void*)this != src) && commandIn(cmd))
    {
        return;
    }
    QList< RMTestModule* >::Iterator iter;
    for (iter = modlist.begin(); iter != modlist.end(); ++iter)
    {
        if (((void*)(*iter) != src) &&
            ((*iter)->commandIn(cmd)))
        {
            break;
        }
    }
    if ((iter == modlist.end()) && (RMTestModule::getNext(cmd) != "help"))
    {
        lineOut("[Error RMTest: Unrecognised command]");
    }
}

bool CmdLine::commandIn(QString cmd)
{
    cmd = cmd.simplified();
    QString arg(RMTestModule::getNext(cmd));

    if (arg == "echo")
    {
        if (cmd.length() > 0)
        {
            QString tstr(RMTestModule::getNext(cmd));
            if (tstr == "on")
            {
                echo_flag = true;
            }
            else if (tstr == "off")
            {
                echo_flag = false;
            }
        }
        else
        {
            if (echo_flag)
            {
                lineOut("[Main] echo on");
            }
            else
            {
                lineOut("[Main] echo off");
            }
        }
        return true;
    }
    else if (arg == "capture")
    {
        if (cmd.length() > 0)
        {
            dout.openfile(RMTestModule::getNext(cmd).toLatin1());
        }
        else
        {
            lineOut("Error Main: Enter a filename as argument]");
        }
        return true;
    }
    else if (arg == "endcapture")
    {
        dout.closefile();
        return true;
    }
    else if (arg == "help")
    {
        lineOut("Main Module");
        lineOut("    echo [on|off]");
        lineOut("    capture filename");
        lineOut("    endcapture");
        lineOut("    quit");
        return false;
    }
    else if (arg == "quit")
    {
        exit(0);
    }
    return false;
}

void CmdLine::commandOut(QString cmd, void* src)
{
    processCmd(cmd, src);
}

void CmdLine::init_command_widget()
{
    char tstr[1024];

    if (def && def->getstring("echo", tstr) != NULL)
    {
        QString tstr1(tstr);
        if (tstr1 == "off")
        {
            echo_flag = false;
        }
        else if (tstr1 == "on")
        {
            echo_flag = true;
        }
    }
}

void CmdLine::autoscript()
{
    char tstr[1024];
    tree_node* node = NULL;
    while ((node = def->getstring("run", tstr, node)) != NULL)
    {
        QString cmd("run ");
        cmd += tstr;
        commandOut(cmd, this);
    }
}

void CmdLine::lineOut(QString cmd)
{
    dout << cmd.toLatin1().constData() << '\n';
}

void CmdLine::update()
{
    QList< RMTestModule* >::Iterator iter;
    for (iter = modlist.begin(); iter != modlist.end(); ++iter)
    {
        (*iter)->update();
    }
}

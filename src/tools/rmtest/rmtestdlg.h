/****************************************************************************
** Form interface generated from reading ui file 'rmtest.ui'
**
** Created: Tue Jan 8 11:59:02 2002
**      by:  The User Interface Compiler (uic)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef RMTESTDLG_H
#define RMTESTDLG_H

#include <qvariant.h>
#include <qmainwindow.h>
#define WFlags Qt::WindowFlags
#include <defaults.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QConsole;
class QString;
class RMTestModule;

class RMTestDlg : public QMainWindow
{
    Q_OBJECT

public:
    RMTestDlg( defaults* _def, QWidget* parent = 0, const char* name = 0, WFlags fl = 0 );
    ~RMTestDlg();
    bool commandIn(QString cmd);
    void commandOut(QString cmd, void* src);

    QConsole* CmdFrame;
    defaults* def;

public slots:
    void processCmd(QString cmd, void* src);
    void autoscript();

signals:
    void lineOut(QString cmd);

protected:
    void init_command_widget();

    QList< RMTestModule* > modlist;
    bool echo_flag;
};

#endif // RMTESTDLG_H

# Microsoft Developer Studio Project File - Name=" - Win32 Debug" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 5.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG= - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "rmtest.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "rmtest.mak" CFG=" - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE " - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE " - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName "rmtest"
# PROP Scc_LocalPath ".."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == " - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /O1 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /FD /c
# ADD CPP /nologo /W3 /GR /GX /O1 /I ".." /I "$(QTDIR)\include" /D "NDEBUG" /D "NO_DEBUG" /D "WIN32" /D "_WINDOWS" /D "STATIC" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib imm32.lib winmm.lib wsock32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib imm32.lib winmm.lib wsock32.lib imm32.lib wsock32.lib winmm.lib $(QTDIR)\lib\qt230.lib $(QTDIR)\lib\qtmain.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == " - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FD /c
# ADD CPP /nologo /W3 /Gm /GR /GX /Zi /Od /I "c:\progra~1\qt\include" /I "$(QTDIR)\include" /I ".." /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "STATIC" /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib imm32.lib winmm.lib wsock32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib imm32.lib winmm.lib wsock32.lib imm32.lib wsock32.lib winmm.lib $(QTDIR)\lib\qt230d.lib $(QTDIR)\lib\qtmaind.lib /nologo /subsystem:windows /debug /machine:I386 /nodefaultlib:"libc" /pdbtype:sept

!ENDIF 

# Begin Target

# Name " - Win32 Release"
# Name " - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\filemod.cpp
# End Source File
# Begin Source File

SOURCE=.\main.cpp
# End Source File
# Begin Source File

SOURCE=.\moc_filemod.cpp
# End Source File
# Begin Source File

SOURCE=.\moc_qconsole.cpp
# End Source File
# Begin Source File

SOURCE=.\moc_rmmod.cpp
# End Source File
# Begin Source File

SOURCE=.\moc_rmtestdlg.cpp
# End Source File
# Begin Source File

SOURCE=.\moc_rmtestmod.cpp
# End Source File
# Begin Source File

SOURCE=.\qconsole.cpp
# End Source File
# Begin Source File

SOURCE=.\rmmod.cpp
# End Source File
# Begin Source File

SOURCE=.\rmtestdlg.cpp
# End Source File
# Begin Source File

SOURCE=.\rmtestmod.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\filemod.h

!IF  "$(CFG)" == " - Win32 Release"

# Begin Custom Build - Moc'ing filemod.h...
InputPath=.\filemod.h

"moc_filemod.cpp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	%QTDIR%\bin\moc.exe filemod.h -o moc_filemod.cpp

# End Custom Build

!ELSEIF  "$(CFG)" == " - Win32 Debug"

# Begin Custom Build - Moc'ing filemod.h...
InputPath=.\filemod.h

"moc_filemod.cpp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	%QTDIR%\bin\moc.exe filemod.h -o moc_filemod.cpp

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\qconsole.h

!IF  "$(CFG)" == " - Win32 Release"

# Begin Custom Build - Moc'ing qconsole.h...
InputPath=.\qconsole.h

"moc_qconsole.cpp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	%QTDIR%\bin\moc.exe qconsole.h -o moc_qconsole.cpp

# End Custom Build

!ELSEIF  "$(CFG)" == " - Win32 Debug"

# Begin Custom Build - Moc'ing qconsole.h...
InputPath=.\qconsole.h

"moc_qconsole.cpp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	%QTDIR%\bin\moc.exe qconsole.h -o moc_qconsole.cpp

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\rmmod.h

!IF  "$(CFG)" == " - Win32 Release"

# Begin Custom Build - Moc'ing rmmod.h...
InputPath=.\rmmod.h

"moc_rmmod.cpp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	%QTDIR%\bin\moc.exe rmmod.h -o moc_rmmod.cpp

# End Custom Build

!ELSEIF  "$(CFG)" == " - Win32 Debug"

# Begin Custom Build - Moc'ing rmmod.h...
InputPath=.\rmmod.h

"moc_rmmod.cpp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	%QTDIR%\bin\moc.exe rmmod.h -o moc_rmmod.cpp

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\rmtestdlg.h

!IF  "$(CFG)" == " - Win32 Release"

# Begin Custom Build - Moc'ing rmtestdlg.h...
InputPath=.\rmtestdlg.h

"moc_rmtestdlg.cpp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	%QTDIR%\bin\moc.exe rmtestdlg.h -o moc_rmtestdlg.cpp

# End Custom Build

!ELSEIF  "$(CFG)" == " - Win32 Debug"

# Begin Custom Build - Moc'ing rmtestdlg.h...
InputPath=.\rmtestdlg.h

"moc_rmtestdlg.cpp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	%QTDIR%\bin\moc.exe rmtestdlg.h -o moc_rmtestdlg.cpp

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\rmtestmod.h

!IF  "$(CFG)" == " - Win32 Release"

# Begin Custom Build - Moc'ing rmtestmod.h...
InputPath=.\rmtestmod.h

"moc_rmtestmod.cpp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	%QTDIR%\bin\moc.exe rmtestmod.h -o moc_rmtestmod.cpp

# End Custom Build

!ELSEIF  "$(CFG)" == " - Win32 Debug"

# Begin Custom Build - Moc'ing rmtestmod.h...
InputPath=.\rmtestmod.h

"moc_rmtestmod.cpp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	%QTDIR%\bin\moc.exe rmtestmod.h -o moc_rmtestmod.cpp

# End Custom Build

!ENDIF 

# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# Begin Group "RM4 Library Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\cache.cpp
# End Source File
# Begin Source File

SOURCE=..\cache.h
# End Source File
# Begin Source File

SOURCE=..\dbgout.cpp
# End Source File
# Begin Source File

SOURCE=..\dbgout.h
# End Source File
# Begin Source File

SOURCE=..\defaults.cpp
# End Source File
# Begin Source File

SOURCE=..\defaults.h
# End Source File
# Begin Source File

SOURCE=..\hcache.cpp
# End Source File
# Begin Source File

SOURCE=..\hcache.h
# End Source File
# Begin Source File

SOURCE=..\mm\mm.h
# End Source File
# Begin Source File

SOURCE=..\mm\mm_alloc.c
# End Source File
# Begin Source File

SOURCE=..\mm\mm_conf.h
# End Source File
# Begin Source File

SOURCE=..\mm\mm_lib.c
# End Source File
# Begin Source File

SOURCE=..\mm\mm_vers.c
# End Source File
# Begin Source File

SOURCE=..\mm\mm_win_core.c
# End Source File
# Begin Source File

SOURCE=..\mptstl.h
# End Source File
# Begin Source File

SOURCE=..\outbuf.cpp
# End Source File
# Begin Source File

SOURCE=..\outbuf.h
# End Source File
# Begin Source File

SOURCE=..\queue.cpp
# End Source File
# Begin Source File

SOURCE=..\queue.h
# End Source File
# Begin Source File

SOURCE=..\rmglobal.h
# End Source File
# Begin Source File

SOURCE=..\rmname.cpp
# End Source File
# Begin Source File

SOURCE=..\rmname.h
# End Source File
# Begin Source File

SOURCE=..\rmsm.cpp
# End Source File
# Begin Source File

SOURCE=..\rmsm.h
# End Source File
# Begin Source File

SOURCE=..\rmsocket.cpp
# End Source File
# Begin Source File

SOURCE=..\rmsocket.h
# End Source File
# Begin Source File

SOURCE=..\rmstl.h
# End Source File
# Begin Source File

SOURCE=..\rmtcp.cpp
# End Source File
# Begin Source File

SOURCE=..\rmtcp.h
# End Source File
# Begin Source File

SOURCE=..\rmutil.cpp
# End Source File
# Begin Source File

SOURCE=..\rmutil.h
# End Source File
# Begin Source File

SOURCE=..\smcache.cpp
# End Source File
# Begin Source File

SOURCE=..\smcache.h
# End Source File
# Begin Source File

SOURCE=..\stlcast.h
# End Source File
# Begin Source File

SOURCE=..\storage.cpp
# End Source File
# Begin Source File

SOURCE=..\storage.h
# End Source File
# Begin Source File

SOURCE=..\tcp\tcpcore.cpp
# End Source File
# Begin Source File

SOURCE=..\tcp\tcpcore.h
# End Source File
# Begin Source File

SOURCE=..\udp\udpcore.cpp
# End Source File
# Begin Source File

SOURCE=..\udp\udpcore.h
# End Source File
# Begin Source File

SOURCE=..\varcol.cpp
# End Source File
# Begin Source File

SOURCE=..\varcol.h
# End Source File
# Begin Source File

SOURCE=..\varitem.cpp
# End Source File
# Begin Source File

SOURCE=..\varitem.h
# End Source File
# End Group
# End Target
# End Project

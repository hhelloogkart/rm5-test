/****************************************************************************
** $Id: qconsole.h
**
** Definition of QConsole class
**
**********************************************************************/
#ifndef QCONSOLE_H
#define QCONSOLE_H

#ifndef QT_H
#include "qframe.h"
#include "qstringlist.h"
#define WFlags Qt::WindowFlags
#endif // QT_H

class QFile;

class QConsole : public QFrame
{
    Q_OBJECT
    Q_PROPERTY( QString floatformat READ getFloatFormat WRITE setFloatFormat )
    Q_PROPERTY( int floatprecision READ getFloatPrecision WRITE setFloatPrecision )
    Q_PROPERTY( int intbase READ getIntBase WRITE setIntBase )
    Q_PROPERTY( uint historysize READ getHistorySize WRITE setHistorySize )
    Q_PROPERTY( uint buffersize READ getBufferSize WRITE setBufferSize )
    Q_PROPERTY( bool cmdlinevis READ getCmdLineVisible WRITE setCmdLineVisible )
    Q_PROPERTY( bool wrap READ getWrap WRITE setWrap )
    Q_PROPERTY( QString prompt READ getPrompt WRITE setPrompt )

public:
    QConsole(QWidget* parent=0, const char* name=0, WFlags f=0);
    ~QConsole();

    void setCompletion(const QStringList&);

    void setFloatFormat(const QString&);
    QString getFloatFormat() const;

    void setFloatPrecision(int);
    int  getFloatPrecision() const;

    void setIntBase(int);
    int  getIntBase() const;

    void setHistorySize(uint);
    uint getHistorySize() const;

    void setBufferSize(uint);
    uint getBufferSize() const;

    void setCmdLineVisible(bool);
    bool getCmdLineVisible() const;

    void setWrap(bool w = true);
    bool getWrap() const;

    QString getPrompt() const;

    void clearscr();
    void showhistory();
    void capture(const QString& filename);
    void screendump(const QString& filename);
    void historydump(const QString& filename);
    virtual void setFont (const QFont&);
public slots:
    void writeLine(QString);
    void newLine();
    void breakLine();
    void output(const QString&);
    void output(const char*);
    void output(char);
    void output(uchar);
    void output(short);
    void output(ushort);
    void output(int);
    void output(uint);
    void output(long);
    void output(ulong);
    void output(quint64);
    void output(float);
    void output(double);
    void setPrompt(const QString&);

signals:
    void commandOut(QString cmd, void* src);

protected:
    virtual void keyPressEvent(QKeyEvent*);
    virtual void wheelEvent(QWheelEvent*);
    virtual void paintEvent(QPaintEvent*);
    virtual void resizeEvent (QResizeEvent*);

private:
    void snap();

    QChar floatformat;
    int floatprecision;
    int intbase;
    uint historysize;
    uint buffersize;
    uint scrollback;
    uint histposition;
    uint curposition;
    uint wrap;
    bool cmdlinevis;
    bool insertmode;
    QString cmdline;
    QString prompt;
    QStringList completion;
    QStringList buffer;
    QStringList history;
    QFile* capfile;
    uint wrapcnt;
};

#endif // QCONSOLE

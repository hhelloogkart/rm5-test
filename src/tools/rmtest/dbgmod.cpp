/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "dbgmod.h"
#include <comm/udpcore.h>
#include <qtimer.h>

#ifndef _WIN32
#include <sys/time.h>
#endif

#define STRBUFFER_SIZE (16 * 1024)

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

DebugModule::DebugModule(QObject* parent, const char* name)
    : RMTestModule(parent, name), food(0)
{
    QTimer* timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(10);
}

DebugModule::~DebugModule()
{
    QList<udp_core*>::Iterator iter;
    for (iter = socklist.begin(); iter != socklist.end(); ++iter)
    {
        delete *iter;
    }
}

bool DebugModule::commandIn(QString cmd)
{
    cmd = cmd.simplified();
    QString arg(getNext(cmd));
    QString arg2;
    int cnt;

    if (arg == "debug")
    {
        if (!cmd.length())
        {
            QStringList::Iterator iter;
            lineOut("[Socket   Hostname          Port]");
            lineOut("[-------------------------------]");
            for (cnt = 1, iter = proplist.begin(); iter != proplist.end(); ++cnt, ++iter)
            {
                arg2.setNum(cnt);
                arg2.prepend('[');
                arg2 = arg2.leftJustified(9, ' ', true);
                arg2 += *iter;
                arg2 += ']';
                lineOut(arg2);
            }
            return true;
        }
        else
        {
            udp_core* udp = new udp_core;
            arg2 = getNext(cmd);
            int port = arg2.toInt();
            if (!cmd.length())
            {
                udp->bind(NULL, port);
                arg2 = "IN_ADDR_ANY       ";
            }
            else
            {
                arg2 = getNext(cmd);
                udp->bind(arg2.toLatin1(), port);
                arg2 = arg2.leftJustified(18, ' ', true);
            }
            arg2 += QString::number(port);
            socklist.append(udp);
            proplist.append(arg2);
            leftovers.append(QString());
        }
        return true;
    }
    else if (arg == "debugstop")
    {
        if (!cmd.length())
        {
            lineOut("[Error DbgMod: Please enter a socket number.]");
        }
        else
        {
            arg2 = getNext(cmd);
            int sock = arg2.toInt();
            QStringList::Iterator iter1, iter3;
            QList<udp_core*>::Iterator iter2;

            for (iter1 = proplist.begin(), iter2 = socklist.begin(), iter3 = leftovers.begin();
                 iter1 != proplist.end();
                 ++iter1, ++iter2, ++iter3, --sock)
            {
                if (sock == 1)
                {
                    proplist.erase(iter1);
                    delete *iter2;
                    socklist.erase(iter2);
                    leftovers.erase(iter3);
                    break;
                }
            }
        }
        return true;
    }
    else if (arg == "help")
    {
        lineOut("");
        lineOut("Debug Module");
        lineOut("    debug [port] [hostname]");
        lineOut("    debugstop socket");

        return false;
    }
    return false;
}

void DebugModule::update()
{
    QList<udp_core*>::Iterator iter;
    struct timeval def = {
        0, 10000
    };
    char str[STRBUFFER_SIZE];
    int rxsz, cnt1, offset, cnt2;
    QString outstr;

    str[STRBUFFER_SIZE - 2] = '\0';
    for (cnt2 = 0, iter = socklist.begin(); iter != socklist.end(); ++iter, ++cnt2)
    {
        bool datain = false;
        while ((*iter)->select(&def) && ((rxsz = (*iter)->recv(str, STRBUFFER_SIZE - 1)) > 0))
        {
            str[rxsz] = '\0';
            datain = true;
            offset = 0;
            for (cnt1 = 0; cnt1 < rxsz; ++cnt1)
            {
                if (str[cnt1] == '\n')
                {
                    str[cnt1] = '\0';
                    outstr += &(str[offset]);
                    outstr.prepend(leftovers[cnt2]);
                    leftovers[cnt2].truncate(0);
                    lineOut(outstr);
                    outstr.truncate(0);
                    offset = cnt1 + 1;
                }
            }
            outstr += &(str[offset]);
        }
        if (datain)
        {
            leftovers[cnt2] += outstr;
            outstr.truncate(0);
        }
    }

    if (food)
    {
        ++food;
        if (food == 10)
        {
            QStringList::iterator iter2;
            for (iter2 = leftovers.begin(); iter2 != leftovers.end(); ++iter2)
            {
                if ((*iter2).length() > 0)
                {
                    lineOut(*iter2);
                    (*iter2).truncate(0);
                }
            }
            food = 0;
        }
    }
}

RMTestModule* dbgmod_init(defaults*, QObject* parent)
{
    return new DebugModule(parent);
}

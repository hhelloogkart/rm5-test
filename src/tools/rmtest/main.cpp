#include <qapplication.h>
#include <defaults.h>
#include <util/dbgout.h>
#include "rmtestdlg.h"
#include "cmdline.h"

#ifdef _WIN32
#include <windows.h>
#endif

int main(int argc, char** argv)
{
    defaults* def = (argc > 0) ?
                    load_defaults(NULL, argv[0]) :
                    load_defaults("+rmtest.cfg", NULL);
    if (def)
    {
        char hoststr[64];
        int port;

        def->parse_cmdline(argc, argv);
        dout.useconsole(def->getnode("console") != NULL);
        if ((def->getstring("log", hoststr) != NULL) && (hoststr[0] != '\0'))
        {
            dout.openfile(hoststr);
        }
        if ((def->getstring("udpaddr", hoststr) != NULL) &&
            (def->getint("udport", &port) != NULL))
        {
            dout.openudp(hoststr, port);
        }
    }
    if (def && def->getnode("nogui"))
    {
        dout.useconsole(true);
        if (def->getnode("run"))
        {
#ifdef _WIN32
            QApplication app(argc, argv);
#endif
            CmdLine cmd(def);
            cmd.autoscript();
            while (1)
            {
                cmd.update();
            }
        }
        else
        {
            dout << "Running RM Test in non-interactive mode with no startup script does nothing.\n";
        }
    }
    else
    {
#ifdef _WIN32
        if (def && (def->getnode("console") == NULL))
        {
            FreeConsole();
        }
#endif
        QApplication app(argc, argv);
        RMTestDlg w(def);
        w.show();
        return app.exec();
    }
    return 0;
}

// dbgmod.h: interface for the DebugModule class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DBGMOD_H__18EAED1E_F541_4C86_B101_04240FFA60C4__INCLUDED_)
#define AFX_DBGMOD_H__18EAED1E_F541_4C86_B101_04240FFA60C4__INCLUDED_

#include "rmtestmod.h"
#include <qstringlist.h>

class defaults;
class udp_core;

class DebugModule : public RMTestModule
{
    Q_OBJECT

public:
    DebugModule(QObject* parent=0, const char* name=0);
    ~DebugModule();

    virtual bool commandIn(QString cmd);

public slots:
    void update();

protected:
    QList<udp_core*> socklist;
    QStringList proplist;
    QStringList leftovers;
    int food;
};

extern RMTestModule* dbgmod_init(defaults* def, QObject* parent);

#endif // !defined(AFX_DBGMOD_H__18EAED1E_F541_4C86_B101_04240FFA60C4__INCLUDED_)

// filemod.cpp: implementation of the FileModule class.
//
//////////////////////////////////////////////////////////////////////

#include "filemod.h"
#include <qfile.h>
#include <qregexp.h>
#include <time/timecore.h>
#include <qapplication.h>

static FileModule* __filemod = 0;

#ifdef WITH_C_SCRIPT
/* EiC stuff-- */
#define ARGMAX (100*1024)
#define BUFMAX (256*1024)
extern "C"
{
#include "eic.h"
}
extern "C" void stdClib(void);

struct callback_st
{
    char callback[256];
    char rmname[256];
    unsigned int len;
    struct
    {
        union
        {
            int i;
            unsigned int ui;
            short sh;
            unsigned short ush;
            char ch;
            unsigned char uch;
            float f;
            double d;
            char str[8];
        } data;
        char dtype;
    } buf[BUFMAX];
};
struct sleep_st
{
    char callback[256];
    unsigned int msec;
};
static char _arg[ARGMAX];
static sleep_st _scallback;
callback_st _callback;
extern "C" void rmtest_cmd(void)
{
    if (__filemod)
    {
        __filemod->scriptCall();
    }
}
void FileModule::scriptCall()
{
    commandOut(QString(_arg), NULL);
}
/* --------EiC */
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#ifdef WITH_C_SCRIPT
FileModule::FileModule(QObject* parent, const char* name)
    : RMTestModule(parent, name), loopinsub(0), offset_sec(0), stimer(NULL)
{
    _callback.callback[0] = '\0';
    _callback.rmname[0] = '\0';
    _callback.len = 0;
    _scallback.callback[0] = '\0';

    /* initiate EiC */
    EiC_init_EiC();
    /* add in the standard C library */
    stdClib();
    /* pass EiC the following command line switches */
    EiC_switches("-I/opt/EiC/include -IEiC");
    EiC_parseString("#define BUFMAX (256*1024)");
    EiC_parseString("struct { char cb_func[256]; char rmname[256]; unsigned int len; struct {  union { int i; unsigned int ui; short sh; unsigned short ush; char ch; unsigned char  uch; float f; double d; char str[8]; } data; char dtype; } buf[256*1024]; } callback @ %ld;", (long)&_callback);
    EiC_parseString("struct sleep_st { char cb_func[256]; unsigned int msec; } scallback @ %ld;", (long)&_scallback);
    EiC_parseString("char arg[%d] @ %ld;", ARGMAX, (long)_arg);
    EiC_parseString("void rmtest_cmd(void)  @ %ld;", (long)&rmtest_cmd);
    EiC_parseString("#include <rmtest.h>");
}
#else
FileModule::FileModule(QObject* parent, const char* name)
    : RMTestModule(parent, name), loopinsub(0), offset_sec(0)
{
}
#endif

FileModule::~FileModule()
{

}

bool FileModule::commandIn(QString cmd)
{
    cmd = cmd.simplified();
    QString cmdarg(getNext(cmd));

    if (cmdarg == "help")
    {
        lineOut("");
        lineOut("File Module");
        lineOut("    run filename [filename ...]");
        lineOut("    loop [count]");
        lineOut("    endloop");
        lineOut("    sub subroutine_name");
        lineOut("    endsub");
        lineOut("    call subroutine_name");
        lineOut("    listsub [subroutine_name]");
        lineOut("    sleep msec");
        return false;
    }
    else if (cmdarg == "loop")
    {
        if (subname.length() > 0)
        {
            ++loopinsub;
            cmdarg += ' ';
            cmd.insert(0, cmdarg);
            subroutines[subname].append(cmd);
            return true;
        }
        else if (cmd.length() > 0)
        {
            bool ok;
            uint cnt = getNext(cmd).toUInt(&ok);
            if (!ok || !cnt)
            {
                lineOut("[Error FileMod: Invalid loop count - Enter a positve integer]");
            }
            else
            {
                loopcnt.append(cnt);
                loops.append(QStringList());
            }
        }
        else
        {
            loopcnt.append(UINT_MAX);
        }
        return true;
    }
    else if (cmdarg == "endloop")
    {
        if (subname.length() > 0)
        {
            --loopinsub;
            cmdarg += ' ';
            cmd.insert(0, cmdarg);
            subroutines[subname].append(cmd);
        }
        else if (loopcnt.count() == 0)
        {
            lineOut("[Error FileMod: Not within a loop]");
        }
        else
        {
            QStringList::ConstIterator iter;
            uint cnt = loopcnt.takeLast();
            QStringList loopcmd = loops.takeLast();
            while (cnt > 0)
            {
                for (iter = loopcmd.begin(); iter != loopcmd.end(); ++iter)
                {
                    commandOut(*iter, NULL);
                }
                --cnt;
            }
        }
        return true;
    }
    else if (cmdarg == "sub")
    {
        if (loopcnt.count() > 0)
        {
            lineOut("[Error FileMod: Cannot define subroutine in loop]");
        }
        else
        {
            if (cmd.length() > 0)
            {
                subname = getNext(cmd);
                subroutines[subname].clear();
            }
            else
            {
                lineOut("[FileMod: Missing subroutine name]");
            }
        }
        return true;
    }
    else if (cmdarg == "endsub")
    {
        if (subname.length() > 0)
        {
            while (loopinsub > 0)
            {
                subroutines[subname].append(QString("endloop"));
                lineOut("[Warning FileMod: Loop not closed, endloop added]");
                --loopinsub;
            }
            subname = "";
        }
        else
        {
            lineOut("[Error FileMod: Not in subroutine definition]");
        }
        return true;
    }
    else if (subname.length() > 0)
    {
        cmdarg += ' ';
        cmd.insert(0, cmdarg);
        subroutines[subname].append(cmd);
        return true;
    }
    else if (loops.count() > 0)
    {
        cmdarg += ' ';
        cmd.insert(0, cmdarg);
        loops.last().append(cmd);
        return true;
    }
    else if (cmdarg == "sleep")
    {
        bool ok = false;
        int msec;
        if (cmd.length() > 0)
        {
            msec = getNext(cmd).toInt(&ok);
        }
        if (!ok || msec <= 0)
        {
            lineOut("[Error FileMod: Invalid sleep timeout - Enter a positve integer in msec]");
        }
        else
        {
            if (!QApplication::startingUp())
            {
                struct timeval tv = {
                    msec/1000, (msec%1000)*1000
                };
                timeExpire te(&tv);
                do
                {
                    qApp->processEvents(QEventLoop::AllEvents);
                }
                while (!te());
            }
            else
            {
                millisleep(msec);
            }
        }
        return true;
    }
    else if (cmdarg == "listsub")
    {
        if (cmd.length() > 0)
        {
            QString subname(getNext(cmd));
            QMap<QString,QStringList>::Iterator siter = subroutines.find(subname);
            if (siter != subroutines.end())
            {
                subname.insert(0, "sub ");
                lineOut(subname);
                QStringList::ConstIterator iter;
                for (iter = siter.value().begin(); iter != siter.value().end(); ++iter)
                {
                    lineOut(*iter);
                }
                lineOut("endsub");
            }
            else
            {
                lineOut("[Error FileMod: Subroutine not found]");
            }
        }
        else
        {
            lineOut("[FileMod: List of subroutines]");
            QString outstr;
            QMap<QString,QStringList>::ConstIterator iter;
            for (iter = subroutines.begin(); iter != subroutines.end(); ++iter)
            {
                outstr = "[         ";
                outstr += iter.key();
                outstr += "]";
                lineOut(outstr);
            }
        }
        return true;
    }
    else if (cmdarg == "call")
    {
        if (cmd.length() > 0)
        {
            QMap<QString,QStringList>::Iterator siter = subroutines.find(getNext(cmd));
            if (siter != subroutines.end())
            {
                QStringList::ConstIterator iter;
                for (iter = siter.value().begin(); iter != siter.value().end(); ++iter)
                {
                    commandOut(*iter, NULL);
                }
            }
            else
            {
                lineOut("[Error FileMod: Subroutine not found]");
            }
        }
        else
        {
            lineOut("[Error FileMod: Subroutine name required]");
        }
        return true;
    }
    else if (cmdarg == "run")
    {
        while (cmd.length() > 0)
        {
            fileRun(getNext(cmd));
        }
        return true;
    }
    return false;
}

void FileModule::fileRun(const QString& filename)
{
    QFile script(filename);
    QString line, cmd;
    if (script.open(QIODevice::ReadOnly))
    {
        while (!script.atEnd())
        {
            line = script.readLine();
            if (line.contains("/* C SCRIPT */"))
            {
                script.close();
#ifdef WITH_C_SCRIPT
                if (!cscript.isEmpty())
                {
                    _callback.callback[0] = '\0';
                    EiC_parseString(":clear %s", cscript.latin1());
                }
                cscript = filename;
                EiC_parseString("#include \"%s\"", filename.latin1());
                addTimer();
#else
                lineOut(QString("[Error Filemod: This version does not support C script]"));
#endif
                return;
            }
            cmd = extractCommand(line);
            cmd = cmd.simplified();
            if (cmd.left(3) == "run")
            {
                getNext(cmd);
                while (cmd.length() > 0)
                {
                    fileRun(getNext(cmd));
                }
            }
            else if (cmd.length() > 0)
            {
                commandOut(cmd, NULL);
            }
        }
    }
    else
    {
        QString outstr("[Error FileMod: Unable to open file ");
        outstr += filename;
        outstr += ']';
        lineOut(outstr);
    }
}

QString FileModule::extractCommand(const QString& line)
{
    static QRegExp re(QString("(\\d+)\\s+(\\d+)$"));
    uint idx, mode = 0;
    QString comments, cmd;
    QChar ch;

    if (line.at(0) != '#')
    {
        for (idx = 0; idx < (uint)line.length(); ++idx)
        {
            ch = line.at(idx);
            if (ch == '[')
            {
                ++mode;
            }
            else if ((ch == ']') && (mode > 0))
            {
                --mode;
            }
            else
            {
                if (mode == 0)
                {
                    cmd += ch;
                }
                else
                {
                    comments += ch;
                }
            }
        }
    }
    if (comments.length() > 0)
    {
        if (re.indexIn(comments) != -1)
        {
            delay(re.cap(1).toUInt(), re.cap(2).toUInt());
        }
        comments.insert(0, '[');
        comments += ']';
        lineOut(comments);
    }
    else
    {
        if (!QApplication::startingUp())
        {
            qApp->processEvents(QEventLoop::AllEvents);
        }
    }
    return cmd;
}

void FileModule::delay(uint sec, uint usec)
{
    struct timeval tv;
    uint sec1, usec1;
    now(&tv);
    if (offset_sec == 0)
    {
        offset_sec = (static_cast<uint>(tv.tv_sec) > sec) ? static_cast<int>(tv.tv_sec - sec) :
                     -static_cast<int>(sec - tv.tv_sec);
    }
    else
    {
        if (offset_sec > 0)
        {
            tv.tv_sec -= static_cast<uint>(offset_sec);
        }
        else
        {
            tv.tv_sec += static_cast<uint>(-offset_sec);
        }

        if (!QApplication::startingUp())
        {
            if (!((static_cast<uint>(tv.tv_sec) < sec) || ((static_cast<uint>(tv.tv_sec) == sec) && (static_cast<uint>(tv.tv_usec) < usec))))
            {
                qApp->processEvents(QEventLoop::AllEvents);
                return;
            }
            while ((static_cast<uint>(tv.tv_sec) < sec) || ((static_cast<uint>(tv.tv_sec) == sec) && (static_cast<uint>(tv.tv_usec) < usec)))
            {
                usec1 = usec / 1000; // Don't work with usec, work with milliseconds
                tv.tv_usec /= 1000;  // to prevent overflows
                if ((uint)tv.tv_usec > usec1)
                {
                    sec1  = sec - tv.tv_sec - 1;
                    usec1 = usec1 + 1000 - tv.tv_usec;
                }
                else
                {
                    sec1  = sec - tv.tv_sec;
                    usec1 -= tv.tv_usec;
                }
                qApp->processEvents(QEventLoop::AllEvents);
                now(&tv);
                if (offset_sec > 0)
                {
                    tv.tv_sec -= static_cast<uint>(offset_sec);
                }
                else
                {
                    tv.tv_sec += static_cast<uint>(-offset_sec);
                }

                if ((static_cast<uint>(tv.tv_sec) < sec) || ((static_cast<uint>(tv.tv_sec) == sec) && (static_cast<uint>(tv.tv_usec) < usec)))
                {
                    millisleep(10);
                    now(&tv);
                    if (offset_sec > 0)
                    {
                        tv.tv_sec -= static_cast<uint>(offset_sec);
                    }
                    else
                    {
                        tv.tv_sec += static_cast<uint>(-offset_sec);
                    }
                }
                else
                {
                    break;
                }
            }
        }
    }
}

#ifdef WITH_C_SCRIPT
void FileModule::sleepcallback()
{
    EiC_parseString("%s();", stimer_cb.latin1());
    stimer_cb = "";
    addTimer();
}

void FileModule::update()
{
    if (!stimer_cb.isEmpty() && (QTime::currentTime() > stime))
    {
        sleepcallback();
    }
    else
    {
        addTimer();
    }
}

void FileModule::addTimer()
{
    if ((_scallback.callback[0] != '\0') && (_scallback.msec > 0))
    {
        delete stimer;
        stimer = new QTimer(this);
        connect( stimer, SIGNAL(timeout()), this, SLOT(sleepcallback()) );
        stimer_cb = _scallback.callback;
        _scallback.callback[0] = '\0';
        stime = QTime::currentTime().addMSecs((int)_scallback.msec);
        stimer->start((int)_scallback.msec, TRUE);
    }
}
#else
void FileModule::sleepcallback()
{
}
void FileModule::update()
{
}
#endif

RMTestModule* filemod_init(defaults*, QObject* parent)
{
    __filemod = new FileModule(parent);
    return __filemod;
}

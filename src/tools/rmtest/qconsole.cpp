#include "qconsole.h"
#include <qpainter.h>
#include <qfile.h>
#include <qapplication.h>
#include <qclipboard.h>
#include <QKeyEvent>

#define LINEINDENT 2
#define WHEEL_DELTA 120

QConsole::QConsole(QWidget* parent, const char* name, WFlags f) :
    QFrame(parent, f),
    floatformat('g'),
    floatprecision(6),
    intbase(10),
    historysize(40),
    buffersize(1000),
    scrollback(0),
    histposition(0),
    curposition(0),
    wrap(Qt::AlignLeft | Qt::AlignBottom),
    cmdlinevis(true),
    insertmode(true),
    capfile(NULL),
    wrapcnt(0)
{
    setObjectName(name);
    newLine();
    setFocusPolicy(Qt::WheelFocus);
    setWrap();
}
QConsole::~QConsole()
{
    delete capfile;
}
void QConsole::newLine()
{
    if ((unsigned int)buffer.count() >= buffersize)
    {
        buffer.removeLast();
    }
    if (capfile)
    {
        while (wrapcnt > 0)
        {
            capfile->write(buffer[wrapcnt].toLatin1());
            --wrapcnt;
        }
        capfile->write(buffer.first().toLatin1());
        capfile->putChar('\n');
    }
    else
    {
        wrapcnt = 0;
    }
    buffer.prepend(QString());
    update();
}
void QConsole::breakLine()
{
    if ((unsigned int)buffer.count() >= buffersize)
    {
        buffer.removeLast();
    }
    buffer.prepend(QString());
    update();
    ++wrapcnt;
}
void QConsole::output(const QString& str)
{
    if (buffer.empty())
    {
        buffer.push_front(str);
    }
    buffer.first() += str;
}
void QConsole::output(const char* str)
{
    buffer.first() += str;
}
void QConsole::output(char ch)
{
    buffer.first() += ch;
}
void QConsole::output(uchar ch)
{
    buffer.last() += (char)ch;
}
void QConsole::output(short num)
{
    QString str;
    str.setNum(num, intbase);
    output(str);
}
void QConsole::output(ushort num)
{
    QString str;
    str.setNum(num, intbase);
    output(str);
}
void QConsole::output(int num)
{
    QString str;
    str.setNum(num, intbase);
    output(str);
}
void QConsole::output(uint num)
{
    QString str;
    str.setNum(num, intbase);
    output(str);
}
void QConsole::output(long num)
{
    QString str;
    str.setNum(num, intbase);
    output(str);
}
void QConsole::output(ulong num)
{
    QString str;
    str.setNum(num, intbase);
    output(str);
}
void QConsole::output(quint64 num)
{
    QString str;
    str.setNum(num, intbase);
    output(str);
}
void QConsole::output(float num)
{
    QString str;
    str.setNum(num, floatformat.toLatin1(), floatprecision);
    output(str);
}
void QConsole::output(double num)
{
    QString str;
    str.setNum(num, floatformat.toLatin1(), floatprecision);
    output(str);
}
void QConsole::setCompletion(const QStringList& cpt)
{
    completion = cpt;
}
void QConsole::setFloatFormat(const QString& ch)
{
    if (ch.length() > 0)
    {
        floatformat = ch.at(0);
    }
}
QString QConsole::getFloatFormat() const
{
    return QString(floatformat);
}
void QConsole::setFloatPrecision(int fp)
{
    floatprecision = fp;
}
int QConsole::getFloatPrecision() const
{
    return floatprecision;
}
void QConsole::setIntBase(int ib)
{
    intbase = ib;
}
int QConsole::getIntBase() const
{
    return intbase;
}
void QConsole::setHistorySize(uint hs)
{
    historysize = hs;
}
uint QConsole::getHistorySize() const
{
    return historysize;
}
void QConsole::setBufferSize(uint bs)
{
    buffersize = bs;
}
uint QConsole::getBufferSize() const
{
    return buffersize;
}
void QConsole::setCmdLineVisible(bool clv)
{
    cmdlinevis = clv;
}
bool QConsole::getCmdLineVisible() const
{
    return cmdlinevis;
}
void QConsole::keyPressEvent(QKeyEvent* e)
{
    if ((e->key() >= 0x20) && (e->key() <= 0x7e) && ((e->modifiers() & (Qt::AltModifier | Qt::ControlModifier)) == 0))
    {
        snap();
        if (curposition == cmdline.length())
        {
            cmdline += e->text();
            ++curposition;
        }
        else
        {
            if (insertmode)
            {
                cmdline.insert(curposition, e->text());
                ++curposition;
            }
            else
            {
                cmdline.replace(curposition, e->text().length(), e->text());
                ++curposition;
            }
        }
    }
    else
    {
        switch(e->key())
        {
        case Qt::Key_Enter:
        case Qt::Key_Return:
            if (cmdline.length())
            {
                snap();
                if ((history.count() == 0) || (history.first() != cmdline))
                {
                    if (history.count() >= (int)historysize)
                    {
                        history.removeLast();
                    }
                    history.prepend(cmdline);
                }
                commandOut(cmdline, this);
                cmdline.truncate(0);
                curposition = 0;
            }
            break;
        case Qt::Key_Escape:
            snap();
            break;
        case Qt::Key_Tab:
        {
            if (!cmdline.length() || completion.isEmpty())
            {
                return;
            }
            // search cmdline backwards until a space is encountered
            int stidx = cmdline.lastIndexOf(' ');
            if (stidx == -1)
            {
                stidx = -1;
            }
            QString shcmdline(cmdline.right(cmdline.length() - stidx - 1));

            // check if the string is null, if not perform a grep on the
            // stringlist
            if (shcmdline.length() == 0)
            {
                return;
            }
            shcmdline.insert(0, '^');
            QStringList shlist(completion.filter(shcmdline));

            // using the first entry, iterate through to extract additional
            // similar characters
            uint cnt1;
            QChar matchch;
            QStringList::ConstIterator iter;
            for (cnt1 = shcmdline.length() - 1;; ++cnt1)
            {
                if (shlist.first().length() <= (int)cnt1)
                {
                    goto end_of_tab;
                }
                else
                {
                    matchch = shlist.first().at(cnt1);
                }
                for (iter = shlist.begin(), ++iter; iter != shlist.end(); ++iter)
                {
                    if ((*iter).length() <= (int)cnt1)
                    {
                        goto end_of_tab;
                    }
                    if ((*iter).at(cnt1) != matchch)
                    {
                        goto end_of_tab;
                    }
                }
                cmdline += matchch;
            }
            // append the similar characters to cmdline
end_of_tab:
            break;
        }
        case Qt::Key_Backspace:
            if (curposition > 0)
            {
                --curposition;
                cmdline.remove(curposition, 1);
            }
            snap();
            break;
        case Qt::Key_Insert:
        {
            if (e->modifiers() == Qt::ShiftModifier)
            {
                QClipboard* cb = QApplication::clipboard();
                QString clipstr = cb->text();
                if (clipstr.isEmpty())
                {
                    break;
                }
                snap();
                if (curposition == cmdline.length())
                {
                    cmdline += clipstr;
                    curposition += clipstr.length();
                }
                else
                {
                    if (insertmode)
                    {
                        cmdline.insert(curposition, clipstr);
                        curposition += clipstr.length();
                    }
                    else
                    {
                        cmdline.replace(curposition, clipstr.length(), clipstr);
                        curposition += clipstr.length();
                    }
                }
            }
            else if (e->modifiers() == 0)
            {
                insertmode = !insertmode;
            }
        }
        break;
        case Qt::Key_Delete:
            if (curposition < (unsigned int)cmdline.length())
            {
                cmdline.remove(curposition, 1);
            }
            snap();
            break;
        case Qt::Key_Home:
            if (scrollback == 0)
            {
                curposition = 0;
                snap();
            }
            else
            {
                uint numline = height() / (fontMetrics().height() + 1);
                if (numline < (unsigned int)buffer.count())
                {
                    scrollback = buffer.count() - numline;
                    update();
                }
            }
            break;
        case Qt::Key_End:
            if (scrollback == 0)
            {
                curposition = cmdline.length();
                snap();
            }
            else
            {
                snap();
            }
            break;
        case Qt::Key_Left:
            if (curposition > 0)
            {
                --curposition;
            }
            snap();
            break;
        case Qt::Key_Up:
            if (scrollback == 0)
            {
                if (history.count() > 0)
                {
                    cmdline = history[histposition];
                    curposition = cmdline.length();
                    ++histposition;
                    if (histposition >= (unsigned int)history.count())
                    {
                        histposition = 0;
                    }
                }
            }
            else
            {
                uint numline = height() / (fontMetrics().height() + 1);
                if (numline < (unsigned int)buffer.count())
                {
                    ++scrollback;
                    numline = buffer.count() - numline;
                    if (scrollback > numline)
                    {
                        scrollback = numline;
                    }
                    update();
                }
            }
            break;
        case Qt::Key_Right:
            if (curposition < (unsigned int)cmdline.length())
            {
                ++curposition;
            }
            snap();
            break;
        case Qt::Key_Down:
            if (scrollback == 0)
            {
                if (history.count() > 0)
                {
                    if (histposition == 0)
                    {
                        histposition = history.count() - 1;
                    }
                    else
                    {
                        --histposition;
                    }
                    cmdline = history[histposition];
                    curposition = cmdline.length();
                }
            }
            else
            {
                uint numline = height() / (fontMetrics().height() + 1);
                if (numline < (unsigned int)buffer.count())
                {
                    if (scrollback > 1)
                    {
                        --scrollback;
                    }
                    update();
                }
            }
            break;
        case Qt::Key_PageUp:
        {
            uint numline = height() / (fontMetrics().height() + 1);
            if (numline < (unsigned int)buffer.count())
            {
                scrollback += numline;
                numline = buffer.count() - numline;
                if (scrollback > numline)
                {
                    scrollback = numline;
                }
                update();
            }
        }
            return;
        case Qt::Key_PageDown:
        {
            uint numline = height() / (fontMetrics().height() + 1);
            if (numline < (unsigned int)buffer.count())
            {
                if (scrollback < numline)
                {
                    scrollback = 0;
                }
                else
                {
                    scrollback -= numline;
                }
                update();
            }
        }
            return;
        case Qt::Key_V:
        {
            if (e->modifiers() == Qt::ControlModifier)
            {
                QClipboard* cb = QApplication::clipboard();
                QString clipstr = cb->text();
                if (clipstr.isEmpty())
                {
                    break;
                }
                snap();
                if (curposition == cmdline.length())
                {
                    cmdline += clipstr;
                    curposition += clipstr.length();
                }
                else
                {
                    if (insertmode)
                    {
                        cmdline.insert(curposition, clipstr);
                        curposition += clipstr.length();
                    }
                    else
                    {
                        cmdline.replace(curposition, clipstr.length(), clipstr);
                        curposition += clipstr.length();
                    }
                }
            }
        }
        }
    }
    if (cmdlinevis)
    {
        int lineht = fontMetrics().height() + 1;
        lineht += (height() % lineht);
        update(0, height() - lineht, width(), lineht);
    }
}
void QConsole::wheelEvent(QWheelEvent* e)
{
    int delta = e->delta() / WHEEL_DELTA;
    uint numline = height() / (fontMetrics().height() + 1);
    if (numline < (unsigned int)buffer.count())
    {
        if (delta > 0)
        {
            scrollback += delta;
            numline = buffer.count() - numline;
            if (scrollback > numline)
            {
                scrollback = numline;
            }
            update();
        }
        else if ((delta < 0) && (scrollback > 0))
        {
            if (scrollback < static_cast<uint>(-delta))
            {
                scrollback = 0;
            }
            else
            {
                scrollback += delta;
            }
            update();
        }
    }
}
void QConsole::paintEvent(QPaintEvent* event)
{
    QFrame::paintEvent(event);

    QPainter p( this );
    const QFontMetrics fm(fontMetrics());
    const int lineht = fm.height() + 1;
    int y = height() - fm.descent() - 1 - (height() % lineht);

    QStringList::ConstIterator iter = buffer.begin();
    if (scrollback == 0)
    {
        if (cmdlinevis)
        {
            int x, y1, y2;
            x = fm.width(cmdline.left(curposition)) + LINEINDENT + fm.width(prompt);
            y1 = y - fm.ascent();
            y2 = y + fm.descent();
            p.drawText(LINEINDENT, y, prompt + cmdline);
            p.drawLine(x, y1, x, y2);
            y -= lineht;
        }
    }
    else
    {
        for (uint cnt = 0;
             (cnt < scrollback) && (iter != buffer.end());
             ++cnt, ++iter)
        {
            ;
        }
    }

    QRect rect(LINEINDENT, 0, width()-LINEINDENT, y);
    for (; rect.isValid() && (iter != buffer.end()); ++iter)
    {
        QRect brect;
        p.drawText(rect, wrap, *iter, &brect);
        rect.setBottom(brect.top());
    }
}
void QConsole::snap()
{
    if (scrollback != 0)
    {
        scrollback = 0;
        update();
    }
    histposition = 0;
}

void QConsole::writeLine(QString ln)
{
    if (!buffer.empty() && buffer.first().length())
    {
        newLine();
    }
    else
    {
        update();
    }
    output(ln);
}

void QConsole::setPrompt(const QString& _prompt)
{
    prompt = _prompt;
    int lineht = fontMetrics().height() + 1;
    lineht += (height() % lineht);
    update(0, height() - lineht, width(), lineht);
}

QString QConsole::getPrompt() const
{
    return prompt;
}

void QConsole::clearscr()
{
    buffer.clear();
    buffer.push_back(QString());
    update();
}

void QConsole::capture(const QString& filename)
{
    if (capfile)
    {
        if ((buffer.count() > 0) && !buffer.first().isEmpty())
        {
            capfile->write(buffer.first().toLatin1());
        }
        capfile->flush();
        delete capfile;
        capfile = NULL;
    }
    if (filename.length() > 0)
    {
        capfile = new QFile(filename);
        capfile->open(QIODevice::WriteOnly | QIODevice::Append);
    }
}

void QConsole::screendump(const QString& filename)
{
    QFile dumpfile(filename);
    dumpfile.open(QIODevice::WriteOnly);

    QListIterator<QString> iter(buffer);
    iter.toBack();
    while (iter.hasPrevious())
    {
        QString str = iter.previous();
        dumpfile.write(str.toLatin1());
        dumpfile.putChar('\n');
    }
}

void QConsole::historydump(const QString& filename)
{
    QFile dumpfile(filename);
    dumpfile.open(QIODevice::WriteOnly);

    QListIterator<QString> iter(history);
    iter.toBack();
    while (iter.hasPrevious())
    {
        QString str = iter.previous();
        dumpfile.write(str.toLatin1());
        dumpfile.putChar('\n');
    }
}

void QConsole::showhistory()
{
    QListIterator<QString> iter(history);
    iter.toBack();
    while (iter.hasPrevious())
    {
        writeLine(iter.previous());
    }
}

void QConsole::setWrap(bool w)
{
    if (w)
    {
        wrap |= Qt::TextWordWrap;
    }
    else
    {
        wrap &= ~Qt::TextWordWrap;
    }
}

bool QConsole::getWrap() const
{
    return wrap & Qt::TextWordWrap;
}

void QConsole::resizeEvent (QResizeEvent* ev)
{
    if (getWrap())
    {
        setWrap();
    }
    QFrame::resizeEvent(ev);
}

void QConsole::setFont (const QFont& font)
{
    if (getWrap())
    {
        setWrap();
    }
    QWidget::setFont(font);
}

// filemod.h: interface for the FileModule class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FILEMOD_H__18EAED1E_F541_4C86_B101_04240FFA60C4__INCLUDED_)
#define AFX_FILEMOD_H__18EAED1E_F541_4C86_B101_04240FFA60C4__INCLUDED_

#include "rmtestmod.h"
#include <qstringlist.h>
#include <qmap.h>
#include <qtimer.h>
#include <qdatetime.h>

class defaults;

class FileModule : public RMTestModule
{
    Q_OBJECT

public:
    FileModule(QObject* parent=0, const char* name=0);
    ~FileModule();

    virtual bool commandIn(QString cmd);
    void fileRun(const QString& filename);
    QString extractCommand(const QString& line);
    void delay(uint sec, uint usec);
#ifdef WITH_C_SCRIPT
    void scriptCall();
    void addTimer();
#endif

public slots:
    void sleepcallback();
    void update();

protected:
    uint loopinsub;
    QList<uint> loopcnt;
    QString subname;
    QString cscript;
    QList<QStringList> loops;
    QMap<QString,QStringList> subroutines;
    int offset_sec;
#ifdef WITH_C_SCRIPT
    QTimer* stimer;
    QString stimer_cb;
    QTime stime;
#endif
};

extern RMTestModule* filemod_init(defaults* def, QObject* parent);

#endif // !defined(AFX_FILEMOD_H__18EAED1E_F541_4C86_B101_04240FFA60C4__INCLUDED_)

#ifndef CMDLINE_H
#define CMDLINE_H

#include <qobject.h>
#include <qlist.h>

#include <defaults.h>

class QString;
class RMTestModule;

class CmdLine : public QObject
{
    Q_OBJECT

public:
    CmdLine( defaults* _def, QObject* parent = 0, const char* name = 0 );
    ~CmdLine();
    bool commandIn(QString cmd);
    void commandOut(QString cmd, void* src);
    void autoscript();
    void update();

    defaults* def;

public slots:
    void processCmd(QString cmd, void* src);
    void lineOut(QString cmd);

protected:
    void init_command_widget();

    QList< RMTestModule* > modlist;
    bool echo_flag;

};

#endif // CMDLINE_H

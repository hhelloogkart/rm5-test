/****************************************************************************
** $Id: qrminterface.h
**
** Definition of QConsole class
**
**********************************************************************/
#ifndef RMTESTMODULE_H
#define RMTESTMODULE_H

#ifndef QT_H
#include <qobject.h>
#endif // QT_H

class RMTestModule : public QObject
{
    Q_OBJECT

public:
    RMTestModule(QObject* parent=0, const char* name=0);
    ~RMTestModule();

    static QString getNext(QString& str);

    virtual bool commandIn(QString cmd);

public slots:
    virtual void update();

signals:
    void lineOut(QString cmd);
    void commandOut(QString cmd, void* src);
    void newLine();
    void output(const QString& str);
    void output(char);
    void output(uchar);
    void output(short);
    void output(ushort);
    void output(int);
    void output(uint);
    void output(long);
    void output(ulong);
    void output(float);
    void output(double);
    void setPrompt(const QString&);

};

#endif // RMTESTMODULE_H

#include "rmmod.h"
#include "../../rm/rmtcp.h"
#include "../../rm/rmoldtcp.h"
#include "../../rm/rmsm.h"
#include <defaults.h>
#include <qtimer.h>
#include <qregexp.h>
#include <qapplication.h>
#include <task/threadt.h>
#include <time/timecore.h>

#include <qlist.h>

class DisplayEvent : public QEvent
{
public:
    DisplayEvent(const QString& dispstr);
    QString getDisplayString() const;

private:
    QString displaystring;

};

DisplayEvent::DisplayEvent(const QString& dispstr) : QEvent(QEvent::User), displaystring(dispstr)
{
}

QString DisplayEvent::getDisplayString() const
{
    return displaystring;
}

#ifdef WITH_C_SCRIPT
/* EiC stuff */
#define BUFMAX (256*1024)
extern "C"
{
#include "eic.h"
}
struct callback_st
{
    char callback[256];
    char rmname[256];
    unsigned int len;
    struct
    {
        union
        {
            int i;                  /* 1 */
            unsigned int ui;        /* 2 */
            short sh;               /* 3 */
            unsigned short ush;     /* 4 */
            char ch;                /* 5 */
            unsigned char uch;      /* 6 */
            float f;                /* 7 */
            double d;               /* 8 */
            char str[8];            /* 9 */
        } data;
        char dtype;
    } buf[BUFMAX];
};
extern callback_st _callback;
/* end of EiC stuff */
#endif

static defaults* _def = NULL;

void repeat_call(RMModule* mod, const QString& cmd, QRegExp& regexp)
{
    static QRegExp repl(QString("\\{(\\d+)\\.\\.\\.(\\d+)\\}"));
    bool bok, eok;
    uint begin = regexp.cap(3).toUInt(&bok);
    uint end   = regexp.cap(4).toUInt(&eok);

    if (!(bok && eok && (end >= begin)))
    {
        return;
    }
    for (; begin <= end; ++begin)
    {
        QString cmd1(cmd);
        mod->commandIn(cmd1.replace(repl, QString::number(begin)));
    }
}

void parseInt(QString& outstr, const QString& instr, bool hex, bool reverse)
{
    bool result;
    union
    {
        int i;
        char ch[4];
    } value;
    value.i = instr.toInt(&result, hex ? 16 : 10);
    if (!result)
    {
        value.i = -1;
    }
    if (reverse)
    {
        outstr += value.ch[3];
        outstr += value.ch[2];
        outstr += value.ch[1];
        outstr += value.ch[0];
    }
    else
    {
        outstr += value.ch[0];
        outstr += value.ch[1];
        outstr += value.ch[2];
        outstr += value.ch[3];
    }
}

void parseUInt(QString& outstr, const QString& instr, bool hex, bool reverse)
{
    bool result;
    union
    {
        unsigned int i;
        char ch[4];
    } value;
    value.i = instr.toUInt(&result, hex ? 16 : 10);
    if (!result)
    {
        value.i = 0xFFFFFF;
    }
    if (reverse)
    {
        outstr += value.ch[3];
        outstr += value.ch[2];
        outstr += value.ch[1];
        outstr += value.ch[0];
    }
    else
    {
        outstr += value.ch[0];
        outstr += value.ch[1];
        outstr += value.ch[2];
        outstr += value.ch[3];
    }
}

void parseShort(QString& outstr, const QString& instr, bool hex, bool reverse)
{
    bool result;
    union
    {
        short i;
        char ch[2];
    } value;
    value.i = (short)instr.toInt(&result, hex ? 16 : 10);
    if (!result)
    {
        value.i = -1;
    }
    if (reverse)
    {
        outstr += value.ch[1];
        outstr += value.ch[0];
    }
    else
    {
        outstr += value.ch[0];
        outstr += value.ch[1];
    }
}

void parseUShort(QString& outstr, const QString& instr, bool hex, bool reverse)
{
    bool result;
    union
    {
        unsigned short i;
        char ch[2];
    } value;
    value.i = (unsigned short)instr.toUInt(&result, hex ? 16 : 10);
    if (!result)
    {
        value.i = 0xFFFF;
    }
    if (reverse)
    {
        outstr += value.ch[1];
        outstr += value.ch[0];
    }
    else
    {
        outstr += value.ch[0];
        outstr += value.ch[1];
    }
}

void parseChar(QString& outstr, const QString& instr, bool hex)
{
    bool result;
    char value;
    value = (char)instr.toInt(&result, hex ? 16 : 10);
    if (!result)
    {
        value = -1;
    }
    outstr += value;
}

void parseUChar(QString& outstr, const QString& instr, bool hex)
{
    bool result;
    unsigned char value;
    value = (unsigned char)instr.toUInt(&result, hex ? 16 : 10);
    if (!result)
    {
        value = 0xFF;
    }
    outstr += value;
}

void parseFloat(QString& outstr, const QString& instr, bool reverse)
{
    bool result;
    union
    {
        float f;
        int i;
        char ch[4];
    } value;
    value.f = instr.toFloat(&result);
    if (!result)
    {
        value.i = -1;
    }
    if (reverse)
    {
        outstr += value.ch[3];
        outstr += value.ch[2];
        outstr += value.ch[1];
        outstr += value.ch[0];
    }
    else
    {
        outstr += value.ch[0];
        outstr += value.ch[1];
        outstr += value.ch[2];
        outstr += value.ch[3];
    }
}

void parseDouble(QString& outstr, const QString& instr, bool reverse)
{
    bool result;
    union
    {
        double f;
        int i[2];
        char ch[8];
    } value;
    value.f = instr.toDouble(&result);
    if (!result)
    {
        value.i[0] = -1;
        value.i[1] = -1;
    }
    if (reverse)
    {
        outstr += value.ch[7];
        outstr += value.ch[6];
        outstr += value.ch[5];
        outstr += value.ch[4];
        outstr += value.ch[3];
        outstr += value.ch[2];
        outstr += value.ch[1];
        outstr += value.ch[0];
    }
    else
    {
        outstr += value.ch[0];
        outstr += value.ch[1];
        outstr += value.ch[2];
        outstr += value.ch[3];
        outstr += value.ch[4];
        outstr += value.ch[5];
        outstr += value.ch[6];
        outstr += value.ch[7];
    }
}

inline void* r_memcpy(void* dest, const void* src, size_t num)
{
    char* d=reinterpret_cast<char*>(dest);
    const char* s=reinterpret_cast<const char*>(src);
    for (int i=0; i<static_cast<int>(num); ++i)
    {
        d[i] = s[num-i-1];
    }
    return dest;
}

void rmupdate(void* mod, unsigned int id, const cache_typ* buf)
{
    RMModule* cls = reinterpret_cast<RMModule*>(mod);
    cls->display(id, buf);
#ifdef WITH_C_SCRIPT
    if (_callback.callback[0] != '\0')
    {
        cls->invoke_setlist_callback(id, buf);
    }
#endif
}

bool receive_thread(void* obj)
{
    return ((RMModule*)obj)->receive();
}

RMModule::RMModule(QObject* parent, const char* name)
    : RMTestModule(parent, name), sock(NULL), th(0)
{
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
}
RMModule::~RMModule()
{
    delete th;
    delete sock;
}
bool RMModule::commandIn(QString cmd)
{
    static QRegExp regexp(QString("^(typedef|setlist|setlistx|broadcast|broadcastx|getlist|getlistx|register|registerx|unregister)\\s(\\w|_)*\\{(\\d+)\\.\\.\\.(\\d+)\\}"));

    cmd = cmd.simplified();

    if (regexp.indexIn(cmd) != -1)
    {
        repeat_call(this, cmd, regexp);
        return true;
    }

    QString cmdarg(getNext(cmd));
    QString arg2;

    if ((cmdarg == "typedef") ||
        (cmdarg == "getlistx") ||
        (cmdarg == "setlistx") ||
        (cmdarg == "messagex") ||
        (cmdarg == "broadcastx") ||
        (cmdarg == "registerx"))
    {
        if (!cmd.length())
        {
            if (cmdarg != "typedef")
            {
                lineOut("[Error RMMod: RMName and format required]");
            }
            else
            {
                QMap<QString,int>::ConstIterator iter;
                for (iter = name2fmt.begin(); iter != name2fmt.end(); ++iter)
                {
                    QString outstr("[RMMod] typedef ");
                    outstr += iter.key();
                    outstr += ' ';
                    outstr += fmt[iter.value()];
                    lineOut(outstr);
                }
            }
            return true;
        }
        arg2 = getNext(cmd);
        if (!cmd.length())
        {
            if (cmdarg != "typedef")
            {
                lineOut("[Error RMMod: Format required]");
            }
            else
            {
                QMap<QString,int>::ConstIterator iter = name2fmt.find(arg2);
                QString outstr;
                if (iter == name2fmt.end())
                {
                    outstr = "[Error RMMod: ";
                    outstr += arg2;
                    outstr += " format not defined]";
                }
                else
                {
                    outstr = "[RMMod] typedef ";
                    outstr += arg2;
                    outstr += ' ';
                    outstr += fmt[iter.value()];
                }
                lineOut(outstr);
            }
            return true;
        }
        QMap<QString,int>::ConstIterator iter = name2fmt.find(arg2);
        if (iter == name2fmt.end())
        {
            name2fmt.insert(arg2, fmt.count());
            fmt.append(getNext(cmd));
        }
        else
        {
            fmt[iter.value()] = getNext(cmd);
        }
    }
    else if ((cmdarg == "setlist") ||
             (cmdarg == "broadcast") ||
             (cmdarg == "message") ||
             (cmdarg == "getlist") ||
             (cmdarg == "register") ||
             (cmdarg == "unregister") ||
             (cmdarg == "client"))
    {
        if (!cmd.length())
        {
            lineOut("[Error RMMod: RMName required]");
            return true;
        }
        arg2 = getNext(cmd);
    }

    if (cmdarg == "help")
    {
        lineOut("");
        lineOut("RM Module");
        lineOut("    connect [port] [hostname] [TCP|SM] [maxtry=4]");
        lineOut("    close");
        lineOut("    client clientname");
        lineOut("    typedef [rmname] [format]");
        lineOut("    setlist rmname [values ...]");
        lineOut("    setlistx rmname format [values ...]");
        lineOut("    broadcast rmname [values ...]");
        lineOut("    broadcastx rmname format [values ...]");
        lineOut("    message rmname [values ...]");
        lineOut("    messagex rmname format [values ...]");
        lineOut("    getlist rmname");
        lineOut("    getlistx rmname format");
        lineOut("    register rmname");
        lineOut("    registerx rmname format");
        lineOut("    unregister rmname");
        lineOut("    help-typedef");

        return false;
    }
    else if (cmdarg == "help-typedef")
    {
        lineOut("");
        lineOut("typedef formats");
        lineOut("    c = 1-byte signed decimal (-128 to 127)");
        lineOut("    C = 1-byte unsigned decimal (0 to 255)");
        lineOut("    s = 2-byte signed decimal (-32787 to 32786)");
        lineOut("    S = 2-byte signed decimal (0 to 65535)");
        lineOut("    i = 4-byte signed decimal (-2^31 to 2^31-1)");
        lineOut("    I = 4-byte signed decimal (0 to 2^32)");
        lineOut("    f = 4-byte floating point value");
        lineOut("    d = 8-byte floating point value");
        lineOut("    y = 4-byte floating point value, 9 dec place");
        lineOut("    z = 8-byte floating point value, 9 dec place");
        lineOut("    An = ASCII string of length n, null terminated");
        lineOut("    an = ASCII string of length n");
        lineOut("    bn = n-bit field");
        lineOut("");
        lineOut("    <n--format--> = array of n elements, based on --format-- as indicated");
        lineOut("    {--format--} = flex array of elements, based on --format-- as indicated");
        lineOut("    (|--match--) = or-filter, if --match-- matches prior value; display when at least 1 filter passes");
        lineOut("    (&--match--) = and-filter, if --match-- matches prior value; display when all filter pass");
        lineOut("    [--comment--] = comment is printed, used to name or highlight certain fields");
        lineOut("    + = followed by basic type, c, C, s, S, i, I, f or d, variable length array");
        lineOut("");
        lineOut("    x = display integers in hexadecimal");
        lineOut("    X = display integers in decimal");
        lineOut("    r = interpret integers/floats in reverse endian");
        lineOut("    R = interpret integers/floats in normal endian");
        lineOut("    q = interpret bit fields in reverse bit order");
        lineOut("    Q = interpret bit fields in normal bit order");

        return true;
    }
    else if (cmdarg == "connect")
    {
        char tstr[1024];
        QString nprompt;
        int maxtry = 4;
        if (sock != NULL)
        {
            delete th;
            delete sock;
            th = 0;
            sock = 0;
        }
        if (cmd.length())
        {
            cmdarg = getNext(cmd);
            _def->setValue("PORT", cmdarg.toLatin1().constData());
        }
        if (cmd.length())
        {
            cmdarg = getNext(cmd);
            _def->setValue("HOST", cmdarg.toLatin1().constData());
        }
        if (cmd.length())
        {
            cmdarg = getNext(cmd);
            _def->setValue("ACCESS", cmdarg.toLatin1().constData());
        }
        if (cmd.length())
        {
            cmdarg = getNext(cmd);
            bool strok;
            unsigned int maxtryarg = cmdarg.toUInt(&strok);
            if (strok)
            {
                maxtry = maxtryarg;
            }
        }
        if (_def->getnode("PORT") == NULL)
        {
            lineOut("[Error RMMod: Please enter port number]");
            return true;
        }
        if (_def->getnode("HOST") == NULL)
        {
            _def->setValue("HOST", "Auto");
        }
        if (_def->getstring("ACCESS", tstr) != NULL)
        {
            sock = RMBaseSocket::Construct(tstr);
            nprompt = tstr;
            nprompt += ":/";
        }
        if (sock == NULL)
        {
            lineOut("[Error RMMod: Unable to create socket]");
            return true;
        }
        sock->AddCallback(SETLIST, this, rmupdate);
        sock->AddCallback(BROADCAST, this, rmupdate);
        sock->AddCallback(MESSAGE, this, rmupdate);
        sock->AddCallback(REGISTERID, this, rmupdate);
        sock->AddCallback(QUERYID, this, rmupdate);
        sock->AddCallback(CLIENTID, this, rmupdate);
        bool conn=false;
        for (int cnt=0; !conn && (cnt<maxtry); conn = sock->Connect(_def->getString("HOST"), _def->getInt("PORT")), ++cnt)
        {
            ;
        }
        if (conn)
        {
            int flow;
            if (_def->getint("FLOW", &flow) != NULL)
            {
                sock->FlowControl((unsigned int)flow);
            }
            if (_def->getstring("CLIENT", tstr) != NULL)
            {
                sock->ClientID(tstr);
            }
            th = new thread_typ(&receive_thread, this, true);
            timer->start(100);
            nprompt += _def->getString("HOST");
            nprompt += ':';
            nprompt += _def->getString("PORT");
            nprompt += '>';
            setPrompt(nprompt);
        }
        else
        {
            lineOut("[Error RMMod: Unable to connect to server]");
            delete sock;
            sock = NULL;
            setPrompt(QString(">"));
        }
        return true;
    }
    else if (cmdarg == "close")
    {
        if (timer->isActive())
        {
            timer->stop();
        }
        delete th;
        delete sock;
        th = 0;
        sock = 0;
        name2idx.clear();
        idx2name.clear();
        setPrompt(QString(">"));
        return true;
    }
    else if ((cmdarg == "setlist") || (cmdarg == "setlistx"))
    {
        if (sock)
        {
            QMap<QString, int>::ConstIterator iter = name2fmt.find(arg2);
            if (iter == name2fmt.end())
            {
                QString outstr("[Error RMMod: Unable to find typedef for ");
                outstr += arg2;
                outstr += ']';
                lineOut(outstr);
            }
            else
            {
                unsigned int varid = queryid(arg2, false) + 1;
                QString bufstr = pack(fmt[iter.value()], cmd);
                mut.lock();
                cache_typ* buf = sock->SetListStart(varid, bufstr.length() / 4);
                if (buf)
                {
                    memcpy(buf->buf(), bufstr.toLatin1().constData(), bufstr.length());
                    sock->SetListEnd(varid, buf);
                }
                mut.unlock();
            }
        }
        else
        {
            lineOut("[Error RMMod: Not Connected]");
        }
        return true;
    }
    else if ((cmdarg == "broadcast") || (cmdarg == "broadcastx"))
    {
        if (sock)
        {
            QMap<QString, int>::ConstIterator iter = name2fmt.find(arg2);
            if (iter == name2fmt.end())
            {
                QString outstr("[Error RMMod: Unable to find typedef for ");
                outstr += arg2;
                outstr += ']';
                lineOut(outstr);
            }
            else
            {
                unsigned int varid = queryid(arg2, false) + 1;
                QString bufstr = pack(fmt[iter.value()], cmd);
                mut.lock();
                cache_typ* buf = sock->SetListStart(varid, bufstr.length() / 4);
                if (buf)
                {
                    memcpy(buf->buf(), bufstr.toLatin1().constData(), bufstr.length());
                    sock->BroadcastEnd(varid, buf);
                }
                mut.unlock();
            }
        }
        else
        {
            lineOut("[Error RMMod: Not Connected]");
        }
        return true;
    }
    else if ((cmdarg == "message") || (cmdarg == "messagex"))
    {
        if (sock)
        {
            QMap<QString, int>::ConstIterator iter = name2fmt.find(arg2);
            if (iter == name2fmt.end())
            {
                QString outstr("[Error RMMod: Unable to find typedef for ");
                outstr += arg2;
                outstr += ']';
                lineOut(outstr);
            }
            else
            {
                unsigned int varid = queryid(arg2, false) + 1;
                QString bufstr = pack(fmt[iter.value()], cmd);
                mut.lock();
                cache_typ* buf = sock->SetListStart(varid, bufstr.length() / 4);
                if (buf)
                {
                    memcpy(buf->buf(), bufstr.toLatin1().constData(), bufstr.length());
                    sock->MessageEnd(varid, buf);
                }
                mut.unlock();
            }
        }
        else
        {
            lineOut("[Error RMMod: Not Connected]");
        }
        return true;
    }
    else if ((cmdarg == "getlist") || (cmdarg == "getlistx"))
    {
        if (sock)
        {
            const cache_typ* buf;
            unsigned int varid = queryid(arg2, false) + 1;
            mut.lock();
            buf = sock->GetListStart(varid);
            mut.unlock();
            if (buf)
            {
                display(varid, buf);
                mut.lock();
                sock->GetListEnd(varid, buf);
                mut.unlock();
            }
            else
            {
                QString outstr("[RMMod] setlist ");
                outstr += arg2;
                outstr += " [Empty]";
                lineOut(outstr);
            }
        }
        else
        {
            lineOut("[Error RMMod: Not Connected]");
        }
        return true;
    }
    else if ((cmdarg == "register") || (cmdarg == "registerx"))
    {
        if (sock)
        {
            QMap<QString, int>::ConstIterator iter = name2idx.find(arg2);
            if (iter == name2idx.end())
            {
                mut.lock();
                sock->RegisterID(arg2.toLatin1());
                mut.unlock();
                name2idx.insert(arg2, idx2name.count());
                idx2name.append(arg2);
            }
            else
            {
                mut.lock();
                sock->RegisterIDX(iter.value() + 1);
                mut.unlock();
            }
        }
        else
        {
            lineOut("[Error RMMod: Not Connected]");
        }
        return true;
    }
    else if (cmdarg == "typedef")
    {
        return true;
    }
    else if (cmdarg == "client")
    {
        if (sock)
        {
            mut.lock();
            sock->ClientID(arg2.toLatin1());
            mut.unlock();
            name2idx.clear();
            idx2name.clear();
        }
        else
        {
            lineOut("[Error RMMod: Not Connected]");
        }
        return true;
    }
    else if (cmdarg == "unregister")
    {
        if (sock)
        {
            QMap<QString, int>::ConstIterator iter = name2idx.find(arg2);
            if (iter != name2idx.end())
            {
                mut.lock();
                sock->UnregisterIDX(iter.value() + 1);
                mut.unlock();
            }
            else
            {
                QString outstr("[Error RMMod: ");
                outstr += arg2;
                outstr += " not registered]";
                lineOut(outstr);
            }
        }
        else
        {
            lineOut("[Error RMMod: Not Connected]");
        }
        return true;
    }
    return false;
}

int RMModule::queryid(QString rmname, bool reg)
{
    int rmidx = -1;
    QMap<QString, int>::ConstIterator iter = name2idx.find(rmname);
    if (iter != name2idx.end())
    {
        return iter.value();
    }
    if (sock)
    {
        mut.lock();
        if (reg)
        {
            sock->RegisterID(rmname.toLatin1());
        }
        else
        {
            sock->QueryID(rmname.toLatin1());
        }
        mut.unlock();
        rmidx = name2idx.count();
        name2idx.insert(rmname, rmidx);
        idx2name.append(rmname);
    }
    return rmidx;
}

void RMModule::update()
{
    // check if thread started
    if ((th == 0) || (!th->started()))
    {
        timer->stop();
    }
    // check if thread is still alive
    else if (th->finished())
    {
        timer->stop();
        delete th;
        th = 0;

        sock->Close();
        delete sock;
        sock = 0;
        lineOut(QString("[RMMod Error: Connection Lost]"));
        setPrompt(QString(">"));
    }
    else
    {
        mut.lock();
        sock->Flush();
        mut.unlock();
    }
}

unsigned int findNum(const QString& str, unsigned int& curr)
{
    QString tstr;
    for (; curr < (unsigned int)str.length(); ++curr)
    {
        if (str.at(curr).isNumber())
        {
            tstr += str.at(curr);
        }
        else
        {
            break;
        }
    }
    if (tstr.length() > 0)
    {
        bool ok;
        unsigned int rep = tstr.toUInt(&ok);
        if (ok)
        {
            return rep;
        }
    }
    return UINT_MAX;
}

void RMModule::display(unsigned int id, const cache_typ* buf)
{
    QString& outtext = *(new QString);
    QString varfmt;
    QString lastvalue;
    QList< unsigned int > repeat;
    QList< unsigned int > begin;
    unsigned int len = buf->get_len() * 4;
    unsigned int curr = 0;
    unsigned int bufidx = 0;
    unsigned short plus = 0;
    const unsigned char* ptr = reinterpret_cast<const unsigned char*>(buf->buf());
    bool hex = false;
    bool reverse = false;
    int filter = -1; /* -1: No filter(show), 0: Not filtered(show), 1: Filtered(don't show) */
    static const char* cmd_names[] = {
        0 /*ALIVE*/,
        "broadcast",
        "setlist",
        0 /*GETLIST*/,
        0 /*QUERYID*/,
        0 /*CLIENTID*/,
        0 /*REGISTERID*/,
        0 /*REGISTERIDX*/,
        0 /*UNREGISTERIDX*/,
        0 /*FLOWCONTROL*/,
        "message"
    };

    union
    {
        int i;
        uint ui;
        short sh;
        unsigned short ush;
        char ch;
        unsigned char uch;
        float f;
        double d;
    } tmpdata;

    switch (buf->get_cmd())
    {
    case SETLIST:
    case BROADCAST:
    case MESSAGE:
    {
        if (id <= (unsigned int)idx2name.count())
        {
            struct timeval tm;
            outtext += "[RMMod ";
            now(&tm);
            outtext += QString::number(tm.tv_sec);
            outtext += ' ';
            if (tm.tv_usec < 100000)
            {
                if (tm.tv_usec < 10000)
                {
                    if (tm.tv_usec < 1000)
                    {
                        if (tm.tv_usec < 100)
                        {
                            if (tm.tv_usec < 10)
                            {
                                outtext += "00000";
                            }
                            else
                            {
                                outtext += "0000";
                            }
                        }
                        else
                        {
                            outtext += "000";
                        }
                    }
                    else
                    {
                        outtext += "00";
                    }
                }
                else
                {
                    outtext += '0';
                }
            }
            outtext += QString::number(tm.tv_usec);
            outtext += "] ";
            outtext += cmd_names[buf->get_cmd()];
            outtext += ' ';
            outtext += idx2name[id-1];
            outtext += ' ';
            QMap<QString, int>::ConstIterator iter1 = name2fmt.find(idx2name[id-1]);
            if (iter1 != name2fmt.end())
            {
                varfmt = fmt[iter1.value()];
            }
            else
            {
                varfmt = "<xI>";
            }
        }
        else
        {
            outtext += "[Error RMMod: Unable to resolve RM Name for ";
            outtext += cmd_names[buf->get_cmd()];
            outtext += " ID = ";
            outtext += QString::number(id);
            outtext += ']';
        }
        break;
    }
    case QUERYID:
    {
        if (id <= (unsigned int)idx2name.count())
        {
            outtext += "[RMMod: queryid ";
            outtext += idx2name[id-1];
            outtext += " OK]";
        }
        else
        {
            outtext += "[Error RMMod: Unable to resolve RM Name for QueryID ID = ";
            outtext += QString::number(id);
            outtext += ']';
        }
        break;
    }
    case REGISTERID:
    {
        if (id <= (unsigned int)idx2name.count())
        {
            outtext += "[RMMod] register ";
            outtext += idx2name[id-1];
            outtext += " [OK]";
        }
        else
        {
            outtext += "[Error RMMod: Unable to resolve RM Name for RegisterID ID = ";
            outtext += QString::number(id);
            outtext += ']';
        }
        break;
    }
    case CLIENTID:
    {
        if (id != 0xFFFFFF)
        {
            outtext += "[RMMod: ClientID OK]";
        }
        else
        {
            outtext += "[Error RMMod: Unable to clientID]";
        }
        break;
    }
    default:
        return;
    }

    while ((curr < (unsigned int)varfmt.length()) &&
           ((bufidx < len) || (varfmt.at(curr)=='(')))
    {
        switch (varfmt.at(curr).toLatin1())
        {
        case 'i': // integer (32 bit)
            if (bufidx + sizeof(int) <= len)
            {
                if (reverse)
                {
                    r_memcpy(&tmpdata, ptr+bufidx, sizeof(int));
                }
                else
                {
                    memcpy(&tmpdata, ptr+bufidx, sizeof(int));
                }
                lastvalue = QString::number(tmpdata.i, hex ? 16 : 10);
                outtext += lastvalue;
                outtext += ' ';
                bufidx += sizeof(int);
            }
            else
            {
                bufidx=len;
            }
            if (plus > 0)
            {
                --plus;
            }
            else
            {
                ++curr;
            }
            break;
        case 'I': // unsigned int
            if (bufidx + sizeof(unsigned int) <= len)
            {
                if (reverse)
                {
                    r_memcpy(&tmpdata, ptr+bufidx, sizeof(unsigned int));
                }
                else
                {
                    memcpy(&tmpdata, ptr+bufidx, sizeof(unsigned int));
                }
                lastvalue = QString::number(tmpdata.ui, hex ? 16 : 10);
                outtext += lastvalue;
                outtext += ' ';
                bufidx += sizeof(unsigned int);
            }
            else
            {
                bufidx=len;
            }
            if (plus > 0)
            {
                --plus;
            }
            else
            {
                ++curr;
            }
            break;
        case 's': // short (16 bit)
            if (bufidx + sizeof(short) <= len)
            {
                if (reverse)
                {
                    r_memcpy(&tmpdata, ptr+bufidx, sizeof(short));
                }
                else
                {
                    memcpy(&tmpdata, ptr+bufidx, sizeof(short));
                }
                if (hex)
                {
                    lastvalue = QString::number((uint)tmpdata.ush, 16).right(4);
                    outtext += lastvalue;
                }
                else
                {
                    lastvalue = QString::number((int)tmpdata.sh, 10);
                    outtext += lastvalue;
                }
                outtext += ' ';
                bufidx += sizeof(short);
            }
            else
            {
                bufidx=len;
            }
            if (plus > 0)
            {
                --plus;
            }
            else
            {
                ++curr;
            }
            break;
        case 'S': // unsigned short
            if (bufidx + sizeof(unsigned short) <= len)
            {
                if (reverse)
                {
                    r_memcpy(&tmpdata, ptr+bufidx, sizeof(unsigned short));
                }
                else
                {
                    memcpy(&tmpdata, ptr+bufidx, sizeof(unsigned short));
                }
                if (hex)
                {
                    lastvalue = QString::number((uint)tmpdata.ush, 16).right(4);
                    outtext += lastvalue;
                }
                else
                {
                    lastvalue = QString::number((uint)tmpdata.ush, 10);
                    outtext += lastvalue;
                }
                outtext += ' ';
                bufidx += sizeof(unsigned short);
            }
            else
            {
                bufidx=len;
            }
            if (plus > 0)
            {
                --plus;
            }
            else
            {
                ++curr;
            }
            break;
        case 'c': // char (print as a number)
            if (bufidx + sizeof(char) <= len)
            {
                tmpdata.uch = *(ptr+bufidx);
                if (hex)
                {
                    lastvalue = QString::number((uint)tmpdata.uch, 16).right(2);
                    outtext += lastvalue;
                }
                else
                {
                    lastvalue = QString::number((int)tmpdata.ch, 10);
                    outtext += lastvalue;
                }
                outtext += ' ';
                bufidx += sizeof(char);
            }
            else
            {
                bufidx=len;
            }
            if (plus > 0)
            {
                --plus;
            }
            else
            {
                ++curr;
            }
            break;
        case 'C': // unsigned char (print as a number)
            if (bufidx + sizeof(unsigned char) <= len)
            {
                tmpdata.uch = *(ptr+bufidx);
                if (hex)
                {
                    lastvalue = QString::number((uint)tmpdata.uch, 16).right(2);
                    outtext += lastvalue;
                }
                else
                {
                    lastvalue = QString::number((uint)tmpdata.uch, 10);
                    outtext += lastvalue;
                }
                outtext += ' ';
                bufidx += sizeof(unsigned char);
            }
            else
            {
                bufidx=len;
            }
            if (plus > 0)
            {
                --plus;
            }
            else
            {
                ++curr;
            }
            break;
        case 'x': // hexdecimal mode
            hex = true;
            ++curr;
            break;
        case 'X': // decimal mode
            hex = false;
            ++curr;
            break;
        case 'r': // reverse mode
            reverse = true;
            ++curr;
            break;
        case 'R': // normal mode
            reverse = false;
            ++curr;
            break;
        case 'z': // double with precision up to 9 figure
            if (bufidx + sizeof(double) <= len)
            {
                if (reverse)
                {
                    r_memcpy(&tmpdata, ptr+bufidx, sizeof(double));
                }
                else
                {
                    memcpy(&tmpdata, ptr+bufidx, sizeof(double));
                }
                lastvalue = QString::number(tmpdata.d,'g', 12);
                outtext += lastvalue;
                outtext += ' ';
                bufidx += sizeof(double);
            }
            else
            {
                bufidx=len;
            }
            if (plus > 0)
            {
                --plus;
            }
            else
            {
                ++curr;
            }
            break;
        case 'd': // double
            if (bufidx + sizeof(double) <= len)
            {
                if (reverse)
                {
                    r_memcpy(&tmpdata, ptr+bufidx, sizeof(double));
                }
                else
                {
                    memcpy(&tmpdata, ptr+bufidx, sizeof(double));
                }
                lastvalue = QString::number(tmpdata.d);
                outtext += lastvalue;
                outtext += ' ';
                bufidx += sizeof(double);
            }
            else
            {
                bufidx=len;
            }
            if (plus > 0)
            {
                --plus;
            }
            else
            {
                ++curr;
            }
            break;
        case 'y': // float with precision up to 8 figure
            if (bufidx + sizeof(float) <= len)
            {
                if (reverse)
                {
                    r_memcpy(&tmpdata, ptr+bufidx, sizeof(float));
                }
                else
                {
                    memcpy(&tmpdata, ptr+bufidx, sizeof(float));
                }
                lastvalue = QString::number((double)tmpdata.f, 'G', 12);
                outtext += lastvalue;
                outtext += ' ';
                bufidx += sizeof(float);
            }
            else
            {
                bufidx=len;
            }
            if (plus > 0)
            {
                --plus;
            }
            else
            {
                ++curr;
            }
            break;
        case 'f': // float
            if (bufidx + sizeof(float) <= len)
            {
                if (reverse)
                {
                    r_memcpy(&tmpdata, ptr+bufidx, sizeof(float));
                }
                else
                {
                    memcpy(&tmpdata, ptr+bufidx, sizeof(float));
                }
                lastvalue = QString::number((double)tmpdata.f);
                outtext += lastvalue;
                outtext += ' ';
                bufidx += sizeof(float);
            }
            else
            {
                bufidx=len;
            }
            if (plus > 0)
            {
                --plus;
            }
            else
            {
                ++curr;
            }
            break;
        case 'a': // array of characters, print ASCII (opt: num of char, def = MAX)
        {
            unsigned int rep = findNum(varfmt, ++curr);
            unsigned int cnt1;
            lastvalue.clear();
            for (cnt1 = 0; (cnt1 < rep) && (bufidx < len); ++cnt1, ++bufidx)
            {
                lastvalue += ptr[bufidx];
            }
            outtext += lastvalue;
            outtext += ' ';
        }
        break;
        case 'A': // null-terminated string (opt: num of char, def = MAX)
        {
            unsigned int rep = findNum(varfmt, ++curr);
            unsigned int cnt1, cnt2;
            lastvalue.clear();
            for (cnt1 = 0, cnt2 = bufidx; (cnt1 < rep) && (cnt2 < len); ++cnt1, ++cnt2)
            {
                if (ptr[cnt2] == '\0')
                {
                    break;
                }
                lastvalue += ptr[cnt2];
            }
            bufidx += rep;
            outtext += lastvalue;
            outtext += ' ';
        }
        break;
        case '<': // start of array (opt: num of elements, def = MAX)
            repeat.append(findNum(varfmt, ++curr)-1);
            begin.append(curr);
            break;
        case '{': // start of flex var
            if (bufidx + sizeof(unsigned int) <= len)
            {
                if (reverse)
                {
                    r_memcpy(&tmpdata, ptr+bufidx, sizeof(unsigned int));
                }
                else
                {
                    memcpy(&tmpdata, ptr+bufidx, sizeof(unsigned int));
                }
                bufidx += sizeof(unsigned int);
                ++curr;
                if (tmpdata.ui == 0)
                {
                    int nestcnt = 0;
                    for (; curr < (unsigned int)varfmt.length(); ++curr)
                    {
                        if (varfmt.at(curr) == '}')
                        {
                            if (nestcnt == 0)
                            {
                                ++curr;
                                break;
                            }
                            else
                            {
                                --nestcnt;
                            }
                        }
                        else if (varfmt.at(curr) == '{')
                        {
                            ++nestcnt;
                        }
                    }
                }
                else
                {
                    repeat.append(tmpdata.ui-1);
                    begin.append(curr);
                }
                outtext += QString::number(tmpdata.ui, hex ? 16 : 10);
                outtext += ' ';
            }
            else
            {
                bufidx += sizeof(unsigned int);
            }
            break;
        case '[': // comment
            do
            {
                outtext += varfmt.at(curr);
                ++curr;
            }
            while ((curr < (unsigned int)varfmt.length()) && (varfmt.at(curr)!=']'));
            outtext += "] ";
            ++curr;
            break;
        case '>': // end of array
        case '}': // end of flex var
            if (repeat.last() == 0)
            {
                begin.pop_back();
                repeat.pop_back();
                ++curr;
            }
            else
            {
                --(repeat.last());
                curr = begin.last();
            }
            break;
        case '+':
            if (bufidx + sizeof(unsigned short) <= len)
            {
                if (reverse)
                {
                    r_memcpy(&tmpdata, ptr+bufidx, sizeof(unsigned short));
                }
                else
                {
                    memcpy(&tmpdata, ptr+bufidx, sizeof(unsigned short));
                }
                bufidx += sizeof(unsigned short);
                ++curr;
                plus = tmpdata.ush;
                if (plus != 0)
                {
                    outtext += QString::number(tmpdata.ush, hex ? 16 : 10);
                    outtext += ' ';
                    --plus;
                }
                else
                {
                    ++curr;
                    outtext += "0 ";
                }
            }
            break;
        case '(':
        {
            int dpos2 = varfmt.indexOf(")", curr);
            if (dpos2 == -1)
            {
                outtext += "Error unmatched parenthesis for filter expression";
                curr = varfmt.size();
            }
            else
            {
                const QChar& op = varfmt.at(curr+1);
                if (((op == '&') || (op == '|')) && (dpos2>static_cast<int>(curr)+2))
                {
                    if (lastvalue == varfmt.mid(curr+2, dpos2-curr-2))
                    {
                        filter = 0;
                    }
                    else
                    {
                        if (op == '&')
                        {
                            return;
                        }
                        if (filter==-1)
                        {
                            filter=1;
                        }
                    }
                }
                curr = dpos2+1;
            }
        }
        break;
        default:
            ++curr;
            break;
        }
    }
    if (filter!=1)
    {
        DisplayEvent* cevt = new DisplayEvent(outtext);
        QApplication::postEvent(this, cevt);
    }
}

QString RMModule::pack(const QString& varfmt, QString& cmd)
{
    QString result;
    QString cmdarg;
    unsigned int curr = 0;
    QList< unsigned int > repeat;
    QList< unsigned int > begin;
    bool hex = false;
    bool donext = true;
    unsigned short plus = 0;
    bool reverse = false;

    while ((curr < (unsigned int)varfmt.length()) && ((cmd.length() > 0) || !donext))
    {
        if (donext)
        {
            cmdarg = getNext(cmd);
        }
        switch (varfmt.at(curr).toLatin1())
        {
        case 'i': // integer (32 bit)
            parseInt(result, cmdarg, hex, reverse);
            if (plus > 0)
            {
                --plus;
            }
            else
            {
                ++curr;
            }
            donext = true;
            break;
        case 'I': // unsigned int
            parseUInt(result, cmdarg, hex, reverse);
            if (plus > 0)
            {
                --plus;
            }
            else
            {
                ++curr;
            }
            donext = true;
            break;
        case 's': // short (16 bit)
            parseShort(result, cmdarg, hex, reverse);
            if (plus > 0)
            {
                --plus;
            }
            else
            {
                ++curr;
            }
            donext = true;
            break;
        case 'S': // unsigned short
            parseUShort(result, cmdarg, hex, reverse);
            if (plus > 0)
            {
                --plus;
            }
            else
            {
                ++curr;
            }
            donext = true;
            break;
        case 'c': // char (print as a number)
            parseChar(result, cmdarg, hex);
            if (plus > 0)
            {
                --plus;
            }
            else
            {
                ++curr;
            }
            donext = true;
            break;
        case 'C': // unsigned char (print as a number)
            parseUChar(result, cmdarg, hex);
            if (plus > 0)
            {
                --plus;
            }
            else
            {
                ++curr;
            }
            donext = true;
            break;
        case 'x': // hexdecimal mode
            hex = true;
            ++curr;
            donext = false;
            break;
        case 'X': // decimal mode
            hex = false;
            ++curr;
            donext = false;
            break;
        case 'r': // reverse mode
            reverse = true;
            ++curr;
            donext = false;
            break;
        case 'R': // normal mode
            reverse = false;
            ++curr;
            donext = false;
            break;
        case 'z':
        case 'd': // double
            parseDouble(result, cmdarg, reverse);
            if (plus > 0)
            {
                --plus;
            }
            else
            {
                ++curr;
            }
            donext = true;
            break;
        case 'y':
        case 'f': // float
            parseFloat(result, cmdarg, reverse);
            if (plus > 0)
            {
                --plus;
            }
            else
            {
                ++curr;
            }
            donext = true;
            break;
        case 'a': // array of characters, print ASCII (opt: num of char, def = MAX)
        case 'A': // null-terminated string (opt: num of char, def = MAX)
        {
            unsigned int rep = findNum(varfmt, ++curr);
            unsigned int cnt;
            if (rep == UINT_MAX)
            {
                rep = cmdarg.length();
            }
            for (cnt = 0; cnt < rep; ++cnt)
            {
                if (cnt < (unsigned int)cmdarg.length())
                {
                    result += cmdarg.at(cnt);
                }
                else
                {
                    result += '\0';
                }
            }
        }
            donext = true;
            break;
        case '<': // start of array (opt: num of elements, def = MAX)
            repeat.append(findNum(varfmt, ++curr)-1);
            begin.append(curr);
            donext = false;
            break;
        case '+':
        {
            bool success;
            union
            {
                unsigned short ush;
                char ch[2];
            } tmpdata;
            tmpdata.ush = static_cast<unsigned short>(cmdarg.toUInt(&success, hex ? 16 : 10));
            if (!success)
            {
                tmpdata.ush = 0;
            }
            if (reverse)
            {
                result += tmpdata.ch[1];
                result += tmpdata.ch[0];
            }
            else
            {
                result += tmpdata.ch[0];
                result += tmpdata.ch[1];
            }
            ++curr;
            plus = tmpdata.ush;
            if (plus == 0)
            {
                ++curr;
            }
            else
            {
                --plus;
            }
        }
        break;
        case '{': // start of flex var
        {
            bool success;
            union
            {
                unsigned int i;
                char ch[4];
            } tmpdata;
            tmpdata.i = cmdarg.toUInt(&success, hex ? 16 : 10);
            if (!success)
            {
                tmpdata.i = 0;
            }
            if (reverse)
            {
                result += tmpdata.ch[3];
                result += tmpdata.ch[2];
                result += tmpdata.ch[1];
                result += tmpdata.ch[0];
            }
            else
            {
                result += tmpdata.ch[0];
                result += tmpdata.ch[1];
                result += tmpdata.ch[2];
                result += tmpdata.ch[3];
            }
            ++curr;
            if (tmpdata.i == 0)
            {
                int nestcnt = 0;
                for (; curr < (unsigned int)varfmt.length(); ++curr)
                {
                    if (varfmt.at(curr) == '}')
                    {
                        if (nestcnt == 0)
                        {
                            ++curr;
                            break;
                        }
                        else
                        {
                            --nestcnt;
                        }
                    }
                    else if (varfmt.at(curr) == '{')
                    {
                        ++nestcnt;
                    }
                }
            }
            else
            {
                repeat.append(tmpdata.i-1);
                begin.append(curr);
            }
        }
            donext = true;
            break;
        case '[': // comment
            do
            {
                ++curr;
            }
            while ((curr < (unsigned int)varfmt.length()) && (varfmt.at(curr)!=']'));
            ++curr;
            donext = false;
            break;
        case '(': // filter (ignore)
            do
            {
                ++curr;
            }
            while ((curr < (unsigned int)varfmt.length()) && (varfmt.at(curr)!=')'));
            ++curr;
            donext = false;
            break;
        case '>': // end of array
        case '}': // end of flex var
            if (repeat.last() == 0)
            {
                begin.pop_back();
                repeat.pop_back();
                ++curr;
            }
            else
            {
                --(repeat.last());
                curr = begin.last();
            }
            donext = false;
            break;
        default:
            ++curr;
            donext = false;
            break;
        }

    }
    while (result.length() % 4)
    {
        result += '\0';
    }
    return result;
}

bool RMModule::receive()
{
    int result;
    if (sock)
    {
        struct timeval tm = {
            0, 5000
        };
        mut.lock();
        result = sock->Select(&tm);
        mut.unlock();
        context_yield();
        return result == -1;
    }
    else
    {
        return true;
    }
}

bool RMModule::event(QEvent* e)
{
    if (e->type() == QEvent::User)
    {
        lineOut(static_cast<DisplayEvent*>(e)->getDisplayString());
        return true;
    }
    return QObject::event(e);
}

#ifdef WITH_C_SCRIPT
void RMModule::invoke_setlist_callback(unsigned int id, const cache_typ* buf)
{
    QString varfmt;
    QValueList< unsigned int > repeat;
    QValueList< unsigned int > begin;
    unsigned int len = buf->get_len() * 4;
    unsigned int curr = 0;
    unsigned int bufidx = 0;
    unsigned int outidx = 0;
    const unsigned char* ptr = reinterpret_cast<const unsigned char*>(buf->buf());

    union
    {
        int i;
        uint ui;
        short sh;
        char ch;
        float f;
        double d;
    } tmpdata;

    if ((buf->get_cmd() != SETLIST) || (id > idx2name.count()))
    {
        return;
    }
    QMap<QString, int>::ConstIterator iter1 = name2fmt.find(idx2name[id-1]);
    if (iter1 != name2fmt.end())
    {
        varfmt = fmt[iter1.data()];
    }
    else
    {
        varfmt = "<xI>";
    }

    while ((curr < varfmt.length()) && (bufidx < len) && (outidx < BUFMAX))
    {
        switch (varfmt.constref(curr))
        {
        case 'i': // integer (32 bit)
            if (bufidx + sizeof(int) <= len)
            {
                memcpy(&tmpdata, ptr+bufidx, sizeof(int));
                _callback.buf[outidx].data.i = tmpdata.i;
                _callback.buf[outidx].dtype = 1;
                ++outidx;
                bufidx += sizeof(int);
            }
            ++curr;
            break;
        case 'I': // unsigned int
            if (bufidx + sizeof(unsigned int) <= len)
            {
                memcpy(&tmpdata, ptr+bufidx, sizeof(unsigned int));
                _callback.buf[outidx].data.ui = tmpdata.ui;
                _callback.buf[outidx].dtype = 2;
                ++outidx;
                bufidx += sizeof(unsigned int);
            }
            ++curr;
            break;
        case 's': // short (16 bit)
            if (bufidx + sizeof(short) <= len)
            {
                memcpy(&tmpdata, ptr+bufidx, sizeof(short));
                _callback.buf[outidx].data.sh = tmpdata.sh;
                _callback.buf[outidx].dtype = 3;
                ++outidx;
                bufidx += sizeof(short);
            }
            ++curr;
            break;
        case 'S': // unsigned short
            if (bufidx + sizeof(unsigned short) <= len)
            {
                memcpy(&tmpdata, ptr+bufidx, sizeof(unsigned short));
                _callback.buf[outidx].data.sh = tmpdata.sh;
                _callback.buf[outidx].dtype = 4;
                ++outidx;
                bufidx += sizeof(unsigned short);
            }
            ++curr;
            break;
        case 'c': // char (print as a number)
            if (bufidx + sizeof(char) <= len)
            {
                tmpdata.ch = *(ptr+bufidx);
                _callback.buf[outidx].data.ch = tmpdata.ch;
                _callback.buf[outidx].dtype = 5;
                ++outidx;
                bufidx += sizeof(char);
            }
            ++curr;
            break;
        case 'C': // unsigned char (print as a number)
            if (bufidx + sizeof(unsigned char) <= len)
            {
                tmpdata.ch = *(ptr+bufidx);
                _callback.buf[outidx].data.ch = tmpdata.ch;
                _callback.buf[outidx].dtype = 6;
                ++outidx;
                bufidx += sizeof(unsigned char);
            }
            ++curr;
            break;
        case 'x': // hexdecimal mode
            ++curr;
            break;
        case 'X': // decimal mode
            ++curr;
            break;
        case 'd': // double
            if (bufidx + sizeof(double) <= len)
            {
                memcpy(&tmpdata, ptr+bufidx, sizeof(double));
                _callback.buf[outidx].data.d = tmpdata.d;
                _callback.buf[outidx].dtype = 8;
                ++outidx;
                bufidx += sizeof(double);
            }
            ++curr;
            break;
        case 'f': // float
            if (bufidx + sizeof(float) <= len)
            {
                memcpy(&tmpdata, ptr+bufidx, sizeof(float));
                _callback.buf[outidx].data.f = tmpdata.f;
                _callback.buf[outidx].dtype = 7;
                ++outidx;
                bufidx += sizeof(float);
            }
            ++curr;
            break;
        case 'a': // array of characters, print ASCII (opt: num of char, def = MAX)
        {
            unsigned int rep = findNum(varfmt, ++curr);
            unsigned int cnt1;
            for (cnt1 = 0; (cnt1 < rep) && (bufidx < len) && (cnt1 < 7); ++cnt1, ++bufidx)
            {
                _callback.buf[outidx].data.str[cnt1] = ptr[bufidx];
            }
            _callback.buf[outidx].dtype = 9;
            _callback.buf[outidx].data.str[cnt1] = '\0';
            ++outidx;
        }
        break;
        case 'A': // null-terminated string (opt: num of char, def = MAX)
        {
            unsigned int rep = findNum(varfmt, ++curr);
            unsigned int cnt1, cnt2;
            for (cnt1 = 0, cnt2 = bufidx; (cnt1 < rep) && (cnt2 < len) && (ptr[cnt2] != '\0'); ++cnt1, ++cnt2)
            {
                _callback.buf[outidx].data.str[cnt1] = ptr[cnt2];
            }
            bufidx += rep;
            _callback.buf[outidx].dtype = 9;
            _callback.buf[outidx].data.str[cnt1] = '\0';
            ++outidx;
        }
        break;
        case '<': // start of array (opt: num of elements, def = MAX)
            repeat.append(findNum(varfmt, ++curr)-1);
            begin.append(curr);
            break;
        case '{': // start of flex var
            if (bufidx + sizeof(unsigned int) <= len)
            {
                memcpy(&tmpdata, ptr+bufidx, sizeof(unsigned int));
                bufidx += sizeof(unsigned int);
                ++curr;
                if (tmpdata.ui == 0)
                {
                    int nestcnt = 0;
                    for (; curr < varfmt.length(); ++curr)
                    {
                        if (varfmt.constref(curr) == '}')
                        {
                            if (nestcnt == 0)
                            {
                                ++curr;
                                break;
                            }
                            else
                            {
                                --nestcnt;
                            }
                        }
                        else if (varfmt.constref(curr) == '{')
                        {
                            ++nestcnt;
                        }
                    }
                }
                else
                {
                    repeat.append(tmpdata.ui);
                    begin.append(curr);
                }
                _callback.buf[outidx].data.ui = tmpdata.ui;
                _callback.buf[outidx].dtype = 2;
                ++outidx;
            }
            else
            {
                bufidx += sizeof(unsigned int);
            }
            break;
        case '>': // end of array
        case '}': // end of flex var
            if (repeat.last() == 0)
            {
                begin.remove(begin.fromLast());
                repeat.remove(repeat.fromLast());
                ++curr;
            }
            else
            {
                --(repeat.last());
                curr = begin.last();
            }
            break;
        default:
            ++curr;
            break;
        }
    }
    _callback.len = outidx;
    qstrcpy(_callback.rmname, idx2name[id-1].latin1());
    EiC_parseString("%s();", _callback.callback);
    _callback.len = 0;
    _callback.rmname[0] = '\0';
}
#endif

RMTestModule* rmmod_init(defaults* def, QObject* parent)
{
    _def = def;
    return new RMModule(parent);
}

extern "C" int RMTestModule_vsprintf(char* buffer, const char* format, va_list argptr)
{
    return vsprintf(buffer, format, argptr);
}

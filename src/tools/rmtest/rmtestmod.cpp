#include "rmtestmod.h"

inline int locateOutofBracketSpaces(const QString& str)
{
    int cnt=0;
    const int len=str.length();
    for (int idx=0; idx<len; ++idx)
    {
        if (str[idx] == '[')
        {
            ++cnt;
        }
        else if ((cnt>0) && (str[idx] == ']'))
        {
            --cnt;
        }
        else if ((cnt==0) && (str[idx] == ' '))
        {
            return idx;
        }
    }
    return -1;
}

RMTestModule::RMTestModule(QObject* parent, const char* name)
    : QObject(parent)
{
    setObjectName(name);
}
RMTestModule::~RMTestModule()
{
}
bool RMTestModule::commandIn(QString)
{
    return false;
}
QString RMTestModule::getNext(QString& str)
{
    QString result(str);

    if (str.isEmpty())
    {
        return QString::null;
    }
    if (str[0] == '"')
    {
        int dQpos2 = result.indexOf("\" ");
        if (dQpos2 == 1)
        {
            str = result.right(result.length()-3);
            return QString::null;
        }
        else if (dQpos2 == -1)
        {
            if (result.length() == 1)
            {
                str = QString::null;
                return QString::null;
            }
            else if (result.at(result.length()-1) == '"')
            {
                str = QString::null;
                return result.mid(1, result.length()-2);
            }
            else
            {
                str = QString::null;
                return result.right(result.length()-1);
            }
        }
        else
        {
            str = result.right(result.length()-dQpos2-2);
            return result.mid(1, dQpos2-1);
        }
    }
    else if (str[0] == '[')
    {
        // start of string is a comment
        int dQpos2 = result.indexOf(']');
        const int len=result.length();
        if ((dQpos2 == -1) || (len == dQpos2+1))
        {
            // mis-matched square parenthesis, or
            // end of string after parenthesis
            str = QString::null;
            return QString::null;
        }
        if ((len > dQpos2+1) &&
            (result.at(dQpos2+1) == ' '))
        {
            // entire argument is a comment
            str = result.right(len-dQpos2-2);
            return getNext(str);
        }
        int pos=locateOutofBracketSpaces(str.right(len-dQpos2-1));
        if (pos == -1)
        {
            str = QString::null;
            return result;
        }
        str = str.right(len-dQpos2-pos-2);
        return result.left(dQpos2+pos+1);
    }
    else
    {
        int pos = str.indexOf(' ');
        if (pos > 0)
        {
            str = str.right(str.length() - pos - 1);
            return result.left(pos);
        }
        str.truncate(0);
        return result;
    }
}

void RMTestModule::update()
{
}


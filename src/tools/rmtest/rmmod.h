/****************************************************************************
** $Id: qrminterface.h
**
** Definition of QConsole class
**
**********************************************************************/
#ifndef RMMODULE_H
#define RMMODULE_H

#include "rmtestmod.h"
#include <qstringlist.h>
#include <qmap.h>
#include <qmutex.h>
#include <qevent.h>

class RMBaseSocket;
class defaults;
class QTimer;
class cache_typ;
class thread_typ;

class RMModule : public RMTestModule
{
    Q_OBJECT

public:
    RMModule(QObject* parent=0, const char* name=0);
    ~RMModule();

    QString pack(const QString& varfmt, QString& cmd);
    bool receive();
    virtual void display(unsigned int len, const cache_typ* buf);
    virtual bool commandIn(QString cmd);
#ifdef WITH_C_SCRIPT
    void invoke_setlist_callback(unsigned int id, const cache_typ* buf);
#endif

public slots:
    void update();

protected:
    int queryid(QString rmname, bool reg);
    virtual bool event(QEvent* e);

    RMBaseSocket* sock;
    QMap<QString, int> name2idx;
    QMap<QString, int> name2fmt;
    QStringList idx2name;
    QStringList fmt;
    QTimer* timer;
    thread_typ* th;
    QMutex mut;
};

extern RMTestModule* rmmod_init(defaults* def, QObject* parent);

#endif // RMMODULE_H

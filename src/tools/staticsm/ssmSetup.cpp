//!  ssmSetup.cpp
/*!
   Sets up the memory needed for SSM and Scramnet protocols

   Data explorer xml is in this format
   <client name="gcs">

   <instance name="test" type="var_test_data">
   <property name="access">Output</property>
   </instance>
   <instance name="lat" type="var_lat_data">
   <property name="access">Input</property>
   </instance>
   </client>

   Generate ssm.cfg in this format
   client
   {
    data = write = SEND001
    data = write = SEND002
    data = read = RECV001
    data = read = RECV002
   }
   client
   {
    data = write = RECV001
    data = write = RECV002
    data = read = SEND001
    data = read = SEND002
   }
 */
#define MM_PRIVATE
#include <util/dbgout.h>
#include <util/utilcore.h>
#include <defaults.h>
#include "../../rmstl.h"
#include "../../rm/directory.h"
#include "../../rm/smcache.h"
#include "../../rm/lockfreequeue.h"
#include "re_private.h"
#ifndef NO_SCRAMNET
#include "../../gt200/scgtapi.h"
#endif

#define MAX_SSM_CLIENT 32 // this is only used for internal structure

class RMStaticSM;

extern size_t extract_equal(const char* str, const char** after, size_t& lenbef);
extern void clear_whitespace(const char** str, size_t& len);
extern const char ssm_setup_cfg[];

void mm_cache_create(void* ptr, size_t usersize);
void addVar(unsigned int clientId, const char* variable, int flag);
void ssmGenerate(void*& ptr, defaults* def);
void parseStorageInfo(defaults* def,  char* storageName, char* storageType, int& storageSize);

struct info_typ
{
    unsigned int id;
    var_item_typ var_item;
};

typedef VECTOR(info_typ) idx2info_typ;
typedef VECTOR(unsigned int) idxx2client_typ;
typedef MAP(unsigned int, unsigned int, less<unsigned int>) id2idx_typ; //Map id to idx
typedef MAP(rmname_typ, unsigned int, less<rmname_typ>) var2id_typ; //Map var name to id

var2id_typ var2id;
idx2info_typ idx2info[MAX_SSM_CLIENT];
id2idx_typ id2idx[MAX_SSM_CLIENT];
idxx2client_typ idxx2cliS[MAX_SSM_CLIENT];
idxx2client_typ idxx2cliR[MAX_SSM_CLIENT];
id2idx_typ cli2idxxS[MAX_SSM_CLIENT];
id2idx_typ cli2idxxR[MAX_SSM_CLIENT];
unsigned int numClients;
int storageSize;

#ifndef INTEGRITY
int main(int argc, char** argv)
{
    //Load config
    defaults* def = load_defaults("+ssm.cfg", argv[0]);
    if (!def)
    {
        return 0;
    }
    def->parse_cmdline(argc, argv);
#ifndef NO_DOUT
    dout.useconsole(def->getnode("console") != NULL);
#endif

    char* storageName = new char[64];
    char* storageType = new char[64];
    rmname_typ global_name;

    parseStorageInfo(def, storageName, storageType, storageSize);

    global_name = storageName;

    void* startptr = NULL;

    if(strcmp(storageType, "SSM") == 0)
    {
        storageSize = FILESIZE;
        startptr = mm_core_create(storageSize, global_name);
    }
    ssmGenerate(startptr, def);

#ifndef NO_SCRAMNET
    if(strcmp(storageType, "SCRAM") == 0)
    {
        scgtHandle handle;
        scgtOpen(0, &handle); /* open GT, unit number 0*/
        void* scramptr = (void*)scgtMapMem(&handle);
        memcpy(scramptr, startptr, storageSize);
        scgtClose(&handle);
    }
#endif
    delete[] storageName;
    delete[] storageType;
}
#endif

void parseStorageInfo(defaults* def,  char* storageName, char* storageType, int& storageSize)
{
    //Read in storage information
    tree_node* tn = 0, * ch;

    for (tn = def->getnode("storage", tn); (tn != 0) && ((ch = def->getChild(tn)) != 0); tn = def->getnode("storage", tn))
    {
        def->getstring("name", storageName, ch);
        def->getstring("type", storageType, ch);
        def->getint("size", &storageSize, ch);
    }
}

void parseVariableInfo(defaults* def)
{
    //Read in variables' information
    char tstr[256];
    re_private re;
    numClients = 0;
    tree_node* tn = 0, * ch;

    for (tn = def->getnode("client", tn); (tn != 0) && ((ch = def->getChild(tn)) != 0); tn = def->getnode("client", tn))
    {
        while ((ch = def->getstring("data", tstr, ch)) != NULL)
        {
            const char* after;
            size_t beflen = strlen(tstr);
            size_t aftlen = extract_equal(tstr, &after, beflen);
            const char* before = &tstr[0];

            if (aftlen == 0)
            {
                continue;
            }

            clear_whitespace(&before, beflen);
            clear_whitespace(&after, aftlen);
            string bef(before, beflen);

            if (aftlen == 0)
            {
                continue;
            }

            int flag;
            if (bef == "write")
            {
                flag = 0;
            }
            else if (bef == "read")
            {
                flag = 1;
            }
            else
            {
                continue;
            }

            if (re.process(after))
            {
                for (int ival = re.start; ival <= re.end; ++ival)
                {
                    addVar(numClients, re.combine(ival), flag);
                }
            }
            else
            {
                addVar(numClients, after, flag);
            }
        }
        numClients++;
    }

}

void ssmGenerate(void*& startPtr, defaults* def)
{
    unsigned int numVar[MAX_SSM_CLIENT];        //Number of variables
    unsigned int numQueue[MAX_SSM_CLIENT];      //Number of sender queues
    unsigned int numRecv[MAX_SSM_CLIENT];       //Number of receiver offsets
    LockFreeQueue<RMStaticSM>* queueArea;       //Pointer to the queue area
    LockFreeQueue<RMStaticSM>* recvArea;        //Pointer to the recv queue area
    unsigned int offset1[MAX_SSM_CLIENT];       //Offsets to the unfilled offset area
    unsigned int offset2[MAX_SSM_CLIENT];       //Offsets to the unfilled offset area
    unsigned int cacheOffsetS[MAX_SSM_CLIENT];  //Offsets to the cacheS
    unsigned int cacheOffsetR[MAX_SSM_CLIENT];  //Offsets to the cacheM
    unsigned int queueOffset[MAX_SSM_CLIENT];   //Offsets to the queueS
    unsigned int recvOffset[MAX_SSM_CLIENT];    //Offsets to the queueM
    unsigned int clientId;                      //Id of client
    unsigned int i;
    unsigned int setup_size;
    unsigned int cache_size;
    int temp;

    //parse the defaults variables
    parseVariableInfo(def);

    //Create from heap
    if(startPtr == NULL)
    {
        startPtr = malloc(storageSize);
    }

    //Calculate cache size
    setup_size = (1 + 1 + numClients) * sizeof(unsigned int); //Magic number + Cache size + Client offsets

    for(clientId = 0; clientId < numClients; clientId++)
    {
        numVar[clientId] = idx2info[clientId].size();
        setup_size += numVar[clientId] * sizeof(info_typ);

        numQueue[clientId] = idxx2cliS[clientId].size();
        numRecv[clientId] = idxx2cliR[clientId].size();
        setup_size += 1 * sizeof(unsigned int); //No. of queue (s)
        setup_size += numQueue[clientId] * sizeof(LockFreeQueue<RMStaticSM>);
        setup_size += numQueue[clientId] * sizeof(unsigned int); //Map area S
        setup_size += 1 * sizeof(unsigned int); //No. of recv (m)
        setup_size += numRecv[clientId] * sizeof(LockFreeQueue<RMStaticSM>);
        setup_size += numRecv[clientId] * sizeof(unsigned int); //Map area R
        setup_size += 1 * sizeof(unsigned int); //No. of recv (s)
        setup_size += (numRecv[clientId] * 2) * sizeof(unsigned int);
        setup_size += 1 * sizeof(unsigned int); //No. of sender (m)
        setup_size += (numQueue[clientId] * 2) * sizeof(unsigned int);
    }
    cache_size = (storageSize - setup_size) / numClients / 2;

    void* ptr = startPtr;

    //Skip the magic number
    ptr = (unsigned int*)ptr + 1;

    //Write the cache size
    *(unsigned int*)ptr = cache_size;
    ptr = (unsigned int*)ptr + 1;

    //Skip to first client
    ptr = (unsigned int*)ptr + numClients;

    for(clientId = 0; clientId < numClients; clientId++)
    {
        //Client offset
        *((unsigned int*)startPtr + clientId + 2) = (unsigned int*)ptr - (unsigned int*)startPtr;

        //No. of variables
        *(unsigned int*)ptr = numVar[clientId];
        ptr = (unsigned int*)ptr + 1;

        //Info area
        for(unsigned int idx = 0; idx < numVar[clientId]; idx++)
        {
            *(info_typ*)ptr = idx2info[clientId][idx];
            ptr = (info_typ*)ptr + 1;
        }

        //No. of queue (s)
        *(unsigned int*)ptr = numQueue[clientId];
        ptr = (unsigned int*)ptr + 1;

        //Queue area s (skip)
        queueOffset[clientId] = (unsigned int*)ptr - (unsigned int*)startPtr;
        queueArea = (LockFreeQueue<RMStaticSM>*)ptr;
        ptr = (LockFreeQueue<RMStaticSM>*)ptr + numQueue[clientId];

        //Map area s
        for(i=0; i<numQueue[clientId]; i++)
        {
            *(unsigned int*)ptr = idxx2cliS[clientId][i];
            ptr = (unsigned int*)ptr + 1;
        }

        //No. of receivers (m)
        *(unsigned int*)ptr = numRecv[clientId];
        ptr = (unsigned int*)ptr + 1;

        //Receiver area m (skip)
        recvOffset[clientId] = (unsigned int*)ptr - (unsigned int*)startPtr;
        recvArea = (LockFreeQueue<RMStaticSM>*)ptr;
        ptr = (LockFreeQueue<RMStaticSM>*)ptr + numRecv[clientId];

        //Map area r
        for(i=0; i<numRecv[clientId]; i++)
        {
            *(unsigned int*)ptr = idxx2cliR[clientId][i];
            ptr = (unsigned int*)ptr + 1;
        }

        //No. of receivers (s)
        *(unsigned int*)ptr = numRecv[clientId];
        ptr = (unsigned int*)ptr + 1;

        //Receiver offset s
        offset1[clientId] = (unsigned int*)ptr - (unsigned int*)startPtr;
        ptr = (unsigned int*)ptr + (2*numRecv[clientId]);

        //No. of senders (m)
        *(unsigned int*)ptr = numQueue[clientId];
        ptr = (unsigned int*)ptr + 1;

        //Sender offset m
        offset2[clientId] = (unsigned int*)ptr - (unsigned int*)startPtr;
        ptr = (unsigned int*)ptr + (2*numQueue[clientId]);

        //Allocate cacheS
        cacheOffsetS[clientId] = (unsigned int*)ptr - (unsigned int*)startPtr;
        mm_cache_create(ptr, cache_size);
        cache_staticmemory_segment::Ptr = ptr;
        ptr = (unsigned int*)ptr + (cache_size/sizeof(unsigned int));

        //Generate initial nodes for queue
        for(i = 0; i < numQueue[clientId]; i++)
        {
            queueArea[i].Dummy();
        }

        //Allocate cacheR
        cacheOffsetR[clientId] = (unsigned int*)ptr - (unsigned int*)startPtr;
        mm_cache_create(ptr, cache_size);
        cache_staticmemory_segment::Ptr = ptr;
        ptr = (unsigned int*)ptr + (cache_size/sizeof(unsigned int));

        for(i = 0; i < numRecv[clientId]; i++)
        {
            recvArea[i].Dummy();
        }
    }

    //Fill in the offsets
    for(clientId = 0; clientId < numClients; clientId++)
    {
        ptr = (unsigned int*)startPtr + (offset1[clientId]);
        for(i = 0; i < numRecv[clientId]; i++)
        {
            temp = idxx2cliR[clientId][i];
            *((unsigned int*)ptr + i) = cacheOffsetS[temp];

            id2idx_typ::iterator iter1 = cli2idxxS[temp].find(clientId);
            if(iter1 != cli2idxxS[temp].end())
            {
                *((unsigned int*)ptr + numRecv[clientId] + i) = queueOffset[temp] + ((sizeof(LockFreeQueue<RMStaticSM>) * ((*iter1).second))/sizeof(int));
            }
        }

        ptr = (unsigned int*)startPtr + (offset2[clientId]);
        for(i = 0; i < numQueue[clientId]; i++)
        {
            temp = idxx2cliS[clientId][i];
            *((unsigned int*)ptr + i) = cacheOffsetR[temp];

            id2idx_typ::iterator iter2 = cli2idxxR[temp].find(clientId);
            if(iter2 != cli2idxxR[temp].end())
            {
                *((unsigned int*)ptr + numQueue[clientId] + i) = recvOffset[temp] + ((sizeof(LockFreeQueue<RMStaticSM>) * ((*iter2).second))/sizeof(int));
            }
        }
    }
    *(unsigned int*)startPtr = MAGICNUM;
}

void mm_cache_create(void* ptr, size_t usize)
{
    size_t size;

    /* determine size */
    size = usize+SIZEOF_mem_pool;

    /* fill in the memory pool structure */
    MM* mm = (MM*)ptr;
    mm->mp_size    = size;
    mm->mp_offset  = SIZEOF_mem_pool;

    /* first element of list of free chunks counts existing chunks */
    mm->mp_freechunks.mc_size      = 0; /* has to be 0 forever */
    mm->mp_freechunks.mc_usize     = 0; /* counts chunks */
    mm->mp_freechunks.mc_u.mc_next = NULL;
}


void addVar(unsigned int clientId, const char* variable, int flag)
{
    int i;
    unsigned int id;
    unsigned int idx;
    unsigned int idxx;
    rmname_typ cname;
    rmname_typ vname;
    info_typ info;

    //Find variable and populate the var2id map
    vname = variable;
    var2id_typ::iterator varIter = var2id.find(vname);
    if(varIter == var2id.end()) //does not exist
    {
        id = var2id.size();
        var2id[vname] = id;
    }
    else
    {
        id = (*varIter).second;
    }

    //Populate the idx2info vector and id2idx map
    if(flag == 0)
    {
        info.id = id | (SETLIST << 24);
    }
    if(flag == 1)
    {
        info.id = id;
    }
    idx = idx2info[clientId].size();
    idx2info[clientId].push_back(info);
    id2idx[clientId][id] = idx;

    //If SEND, then check previous clients to see who are receiving
    if(flag == 0)
    {
        //Iterate previous clients
        for(i=clientId-1; i>=0; i--)
        {
            //Find whether id exists in previous clients
            id2idx_typ::iterator idIter = id2idx[i].find(id);
            if(idIter != id2idx[i].end())
            {
                //Check whether it is a RECV variable
                if ((idx2info[i][id2idx[i][id]].id >> 24) == 0)
                {
                    //Find whether previous client exist in current cli2idxxS map
                    id2idx_typ::iterator infoIter = cli2idxxS[clientId].find(i);
                    if(infoIter == cli2idxxS[clientId].end())
                    {
                        idxx = idxx2cliS[clientId].size();
                        idxx2cliS[clientId].push_back(i);
                        cli2idxxS[clientId][i] = idxx;
                    }
                    else
                    {
                        idxx = (*infoIter).second;
                    }

                    //Set send information
                    idx2info[clientId][idx].var_item.notify.set(idxx, true);

                    //Find whether client exist in previous cli2idxxR map
                    id2idx_typ::iterator infoIter2 = cli2idxxR[i].find(clientId);
                    if(infoIter2 == cli2idxxR[i].end())
                    {
                        idxx = idxx2cliR[i].size();
                        idxx2cliR[i].push_back(clientId);
                        cli2idxxR[i][clientId] = idxx;
                    }
                }
            }
        }
    }
    //If RECV, then check previous clients to see who are sending
    if(flag == 1)
    {
        //Iterate previous clients
        for(i=clientId-1; i>=0; i--)
        {
            //Find whether id exists in previous clients
            id2idx_typ::iterator idIter = id2idx[i].find(id);
            if(idIter != id2idx[i].end())
            {
                //Check whether it is a SEND variable
                if ((idx2info[i][id2idx[i][id]].id >> 24) == SETLIST)
                {
                    //Find whether previous client exist in cli2idxxS map
                    id2idx_typ::iterator infoIter = cli2idxxS[i].find(clientId);
                    if(infoIter == cli2idxxS[i].end())
                    {
                        idxx = idxx2cliS[i].size();
                        idxx2cliS[i].push_back(clientId);
                        cli2idxxS[i][clientId] = idxx;
                    }
                    else
                    {
                        idxx = (*infoIter).second;
                    }

                    //Set send information
                    idx = id2idx[i][id];
                    idx2info[i][idx].var_item.notify.set(idxx, true);

                    //Find whether client exist in cli2idxxR map
                    id2idx_typ::iterator infoIter2 = cli2idxxR[clientId].find(i);
                    if(infoIter2 == cli2idxxR[clientId].end())
                    {
                        idxx = idxx2cliR[clientId].size();
                        idxx2cliR[clientId].push_back(i);
                        cli2idxxR[clientId][i] = idxx;
                    }
                }
            }
        }
    }
}

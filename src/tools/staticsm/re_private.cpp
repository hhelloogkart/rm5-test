#include "../../rmstl.h"
#include "re_private.h"

re_private::re_private()
{
    // set up regular expression
    const char* errptr;
    int erroffset;
    re = pcre_compile("(.*)\\[(\\d+)\\.\\.\\.(\\d+)\\](.*)", 0, &errptr, &erroffset, NULL);
}

re_private::~re_private()
{
    if (pcre_free)
    {
        (*pcre_free)(re);
    }
    else
    {
        free(re);
    }
}

bool re_private::process(const char* str)
{
    if (!re)
    {
        return false;
    }
    int ovector[18];
    char s1[32], s2[32];

    if (pcre_exec(re, NULL, str, strlen(str), 0, 0, ovector, 18) == 5)
    {
        pcre_copy_substring(str, ovector, 5, 1, before, 128);
        pcre_copy_substring(str, ovector, 5, 4, after, 128);
        pcre_copy_substring(str, ovector, 5, 2, s1, 32);
        pcre_copy_substring(str, ovector, 5, 3, s2, 32);
        start = atoi(s1);
        end = atoi(s2);
        return true;
    }
    return false;
}

const char* re_private::combine(int idx)
{
    rm_sprintf3(final, 128, "%s%d%s", before, idx, after);
    return final;
}
#ifndef _REPRIVATE_H_
#define _REPRIVATE_H_

#include <pcre/pcre.h>

class re_private
{
public:
    re_private();
    ~re_private();
    bool process(const char* str);
    const char* combine(int idx);

    int start;
    int end;
    char before[128];
    char after[128];

protected:
    pcre* re;
    char final[128];

};

#endif /* _REPRIVATE_H_ */

#include "syncdelaydef.h"
#include <defaults.h>
#include <cache.h>
#include <util/dbgout.h>
#include <util/proccore.h>
#include <time/timecore.h>
#include <pcre/pcre.h>

#define DISPATCH 50

varlist_typ sync_defaults::varlist;

#ifdef INTEGRITY
extern const char sync_cfg[];
#endif
extern RM_EXPORT size_t extract_equal(const char* str, const char** after, size_t& lenbef);
extern RM_EXPORT void clear_whitespace(const char** str, size_t& len);

inline unsigned int millsecElapsed(const struct timeval* tv_now, const struct timeval* tv_start)
{
    unsigned int result = (tv_now->tv_sec - tv_start->tv_sec) * 1000U;
    // if statement is needed to ensure that the expression never goes negative. unsigned!
    if (tv_now->tv_usec >= tv_start->tv_usec)
    {
        result += (tv_now->tv_usec - tv_start->tv_usec) / 1000U;
    }
    else
    {
        result -= (tv_start->tv_usec - tv_now->tv_usec) / 1000U;
    }
    return result;
}

inline void listWait(long* addr)
{
    while (!atomic_tas(addr))
    {
        context_yield();
    }
}

inline void listSignal(long* addr)
{
    atomic_untas(addr);
}

static void handleIncomingData(void* arg, unsigned int id, const cache_typ* buf)
{
    sync_defaults* sdef = (sync_defaults*)arg;  //initialize sync_defaults
    sdef->incomingData(id, buf);
}

static bool dispatch_packet(RMBaseSocket* sock, cache_typ* buf)
{
    cache_typ* cac = NULL;
    if (buf->get_cmd() == MESSAGE)
    {
        cac = sock->MessageStart(buf->get_id(), buf->get_len());
    }
    else
    {
        cac = sock->SetListStart(buf->get_id(), buf->get_len());
    }

    if (cac)
    {
        const unsigned int* src = buf->buf(), * end = src + buf->get_len();
        unsigned int* dst = cac->buf();
        for (; src != end; ++src, ++dst)
        {
            *dst = *src;
        }
        if (buf->get_cmd() == SETLIST)
        {
            return sock->SetListEnd(buf->get_id(), cac);
        }
        else if (buf->get_cmd() == BROADCAST)
        {
            return sock->BroadcastEnd(buf->get_id(), cac);
        }
        else
        {
            return sock->MessageEnd(buf->get_id(), cac);
        }
    }
    return false;
}

void sync_defaults::incomingData(unsigned int id, const cache_typ* buf)
{
    if ( (id > 0) && (id <= varlist.size()) )
    {
        const int cmd = (varlist[id-1].second < 4) ? SETLIST : (varlist[id-1].second < 8) ? BROADCAST : MESSAGE;

        const size_t other_queue_size = other_output_packet_id_queue->size();
        dout << "From " << myname << " to " << othername << " ID:" << id << "Len:" << buf->get_len() << "Buf:" << other_queue_size;

        cache_typ* tmp_cache = cache_typ::h_duplicate(id, buf->get_len(), buf->buf());
        tmp_cache->set_cmd(cmd);
        cache_item cac_item;
        now(&(cac_item.timestamp));
        cac_item.cac = tmp_cache;

        // === Lock the other mutex for the socket and the queue until end of function
        listWait(other_mutex);
        other_output_packet_id_queue->push_back(cac_item);
        listSignal(other_mutex);
        *other_output_queue_has_data = 1; // inform after queue has been pushed
        dout << "*\n";
    }
}

//re_private
class re_private
{
public:
    re_private();
    ~re_private();
    bool process(const char* str); //handles the original sync request ie mapping of x to x
    bool process2(const char* str); //handles the sync request of x,y
    bool process3(const char* str); //handles the sync request of x[],y[]
    const char* combine(int idx);
    const char* combine3(int idx);
    const char* get_before();
    const char* get_after();

    int start;
    int end;
    char before[128];
    char after[128];

    //only used if re3 is successful
    int start_3;
    int end_3;
    char before_3[128];
    char after_3[128];

protected:
    pcre* re; //for the comparison of X...
    pcre* re2; //for the comparison of X...,Y...
    pcre* re3; //for the comparison of X...[1...100],Y...[2...101]
    char final[128];
    char final_3[128];
};

re_private::re_private()
{
    // set up regular expression
    const char* errptr;
    int erroffset;
    const char* errptr2;
    int erroffset2;
    const char* errptr3;
    int erroffset3;
    re = pcre_compile("(.*)\\[(\\d+)\\.\\.\\.(\\d+)\\](.*)", 0, &errptr, &erroffset, NULL);
    re2 = pcre_compile("(.*)\\s*\\,\\s*(.*)", 0, &errptr2, &erroffset2, NULL);
    re3 = pcre_compile("(.*)\\[(\\d+)\\.\\.\\.(\\d+)\\](.*)\\s*\\,\\s*(.*)\\[(\\d+)\\.\\.\\.(\\d+)\\](.*)", 0, &errptr3, &erroffset3, NULL);
}

re_private::~re_private()
{
    if (pcre_free)
    {
        (*pcre_free)(re);
        (*pcre_free)(re2);
        (*pcre_free)(re3);
    }
    else
    {
        free(re);
        free(re2);
        free(re3);
    }
}

/*!
   Function Name:
   process
   \par Purpose:
   This function serves to check if the argument passed in is of the format X[]
   \par Processing:
   Function returns true if the expression passed in is of the format X[]
   Any other expression passed will be returned false.
 */

bool re_private::process(const char* str)
{
    if (!re)
    {
        return false;
    }
    int ovector[18];
    char s1[32], s2[32];

    if (pcre_exec(re, NULL, str, static_cast<int>(strlen(str)), 0, 0, ovector, 18) == 5)
    {
        //Assuming the expression passed in is A[1...10]B,
        pcre_copy_substring(str, ovector, 5, 1, before, 128); //A is passed into before
        pcre_copy_substring(str, ovector, 5, 4, after, 128); //B is passed into after
        pcre_copy_substring(str, ovector, 5, 2, s1, 32); //'1' is passed into s1
        pcre_copy_substring(str, ovector, 5, 3, s2, 32); //'10' is passed into s2
        start = atoi(s1); //start = 1
        end = atoi(s2); //end = 10
        return true;
    }
    return false;
}

/*!
   Function Name:
   process2
   \par Purpose:
   This function serves to check if the argument passed in is of the format X,Y
   \par Processing:
   Function returns true if the expression passed in is of the format X,Y.
   Any other expression passed will be returned false.
 */
bool re_private::process2(const char* str)
{
    if (!re2)
    {
        return false;
    }
    int ovector[18];

    if (pcre_exec(re2, NULL, str, static_cast<int>(strlen(str)), 0, 0, ovector, 18) == 3)
    {
        //Assuming the expression passed in is A,B
        pcre_copy_substring(str, ovector, 3, 1, before, 128); //A is passed into before
        pcre_copy_substring(str, ovector, 3, 2, after, 128); //B is passed into after
        return true;
    }
    return false;
}

/*!
   Function Name:
   process3
   \par Purpose:
   This function serves to check if the argument passed in is of the format X[],Y[]
   \par Processing:
   Function returns true if the expression passed in is of the format X[],Y[].
   Any other expression passed will be returned false.
 */
bool re_private::process3(const char* str)
{
    if (!re3)
    {
        return false;
    }
    int ovector[36];
    char s1[32], s2[32];
    char e1[32], e2[32];

    int buffer = pcre_exec(re3, NULL, str, static_cast<int>(strlen(str)), 0, 0, ovector, 36);
    if (buffer == 9)
    {
        //Assuming the expression passed in is A[1...5]B,a[6...10]b
        pcre_copy_substring(str, ovector, 9, 1, before, 128); //A is passed into before
        pcre_copy_substring(str, ovector, 9, 4, after, 128); //B is passed into after
        pcre_copy_substring(str, ovector, 9, 5, before_3, 128); //a is passed into before_3
        pcre_copy_substring(str, ovector, 9, 8, after_3, 128); //b is passed into after_3
        pcre_copy_substring(str, ovector, 9, 2, s1, 32); //'1' is passed into s1
        pcre_copy_substring(str, ovector, 9, 3, s2, 32); //'5' is passed into s2
        pcre_copy_substring(str, ovector, 9, 6, e1, 32); //'6' is passed into e1
        pcre_copy_substring(str, ovector, 9, 7, e2, 32); //'10' is passed into e2

        start = atoi(s1); //start = 1
        end = atoi(s2); //end = 5
        start_3 = atoi(e1); //start_3 = 6
        end_3 = atoi(e2); //end_3 = 10

        return true;
    }
    return false;
}

const char* re_private::combine(int idx)
{
    //This function serves to combine the expression A...idxB...
    rm_sprintf3(final, 128, "%s%d%s", before, idx, after);
    return final;
}

const char* re_private::combine3(int idx)
{
    //This function serves to combine the expression a...idxb...
    rm_sprintf3(final_3, 128, "%s%d%s", before_3, idx, after_3);
    return final_3;
}

const char* re_private::get_before()
{
    return before;
}

const char* re_private::get_after()
{
    return after;
}

sync_defaults::sync_defaults() :
    select_tv(0), txdelay(0), flow(-1), dispatch(DISPATCH), resettimeout(30), sock(0),
    sock_state(0), external_close_requested(0), output_queue_has_data(0), reject_clientID(false), mutex(0),
    other_sock_state(0), other_output_queue_has_data(0), other_output_packet_id_queue(0), other_sock(0), other_mutex(0)
{
    now(&last_connect_timestamp);
    last_connect_timestamp.tv_sec -= 1;
    last_close_timestamp.tv_sec = 0;
    last_close_timestamp.tv_usec = 0;
}

sync_defaults::~sync_defaults()
{
    delete sock;
    delete select_tv;
    delete txdelay;
    for (std_list::iterator iter = output_packet_id_queue.begin(); iter != output_packet_id_queue.end(); ++iter)
    {
        (*iter).cac->h_unreserve();
    }
}

static bool init_conn_defaults(defaults* def, sync_defaults* sdef, const char* name)
{
    int ival;
    tree_node* tn = def->getnode(name);
    if (tn)
    {
        tn = def->getChild(tn);
        if (!tn)
        {
            return false;
        }
    }
    else
    {
        return false;
    }

    // creation of RM socket
    char access[16];
    if (def->getstring("access", access, tn) == NULL)
    {
        dout << "Warning: Unable to get RM protocol for " << name << '\n';
        rm_strcpy(access, 16, "SM");
    }

    sdef->sock = RMBaseSocket::Construct(access);

    //socket add call backs
    if (sdef->sock)
    {
        sdef->sock->AddCallback(SETLIST, sdef, &handleIncomingData);
        sdef->sock->AddCallback(BROADCAST, sdef, &handleIncomingData);
        sdef->sock->AddCallback(MESSAGE, sdef, &handleIncomingData);
    }
    else
    {
        dout << "Error: Socket could not be created for " << name << '\n';
        return false;
    }

    // init select wait time
    if (def->getint("timeout_sec", &ival, tn) != NULL)
    {
        if (!sdef->select_tv)
        {
            sdef->select_tv = new struct timeval;
            sdef->select_tv->tv_usec = 0;
        }
        sdef->select_tv->tv_sec = ival;

    }
    if (def->getint("timeout_usec", &ival, tn) != NULL)
    {
        if (!sdef->select_tv)
        {
            sdef->select_tv = new struct timeval;
            sdef->select_tv->tv_sec = 0;
        }
        sdef->select_tv->tv_usec = ival;
    }
    if (!sdef->select_tv)
    {
        sdef->select_tv = new struct timeval;
        sdef->select_tv->tv_sec = 0;
        sdef->select_tv->tv_usec = 100000;
    }

    // init transmit delay
    if (def->getint("txdelay_sec", &ival, tn) != NULL)
    {
        if (!sdef->txdelay)
        {
            sdef->txdelay = new struct timeval;
            sdef->txdelay->tv_usec = 0;
        }
        sdef->txdelay->tv_sec = ival;

    }
    if (def->getint("txdelay_usec", &ival, tn) != NULL)
    {
        if (!sdef->txdelay)
        {
            sdef->txdelay = new struct timeval;
            sdef->txdelay->tv_sec = 0;
        }
        sdef->txdelay->tv_usec = ival;
    }

    // get hostnames
    if (def->getstring("host", sdef->host, tn) == NULL)
    {
        dout << "Warning: Unable to get hostname for " << name << '\n';
        rm_strcpy(sdef->host, 256, "Auto");
    }

    // get port numbers
    if (def->getint("port", &(sdef->port), tn) == NULL)
    {
        dout << "Warning: Unable to get port number for " << name << '\n';
        sdef->port = 0;
    }

    // get client name
    if (def->getstring("client", sdef->client, tn) == NULL)
    {
        dout << "Warning: Unable to get client name for " << name << '\n';
        rm_strcpy(sdef->client, 256, "RMSCRIPT");
    }

    // get flow control
    def->getint("flow", &(sdef->flow), tn);

    // get dispatch
    def->getint("dispatch", &(sdef->dispatch), tn);

    // get connection reset timeout
    def->getint("resettimeout", &(sdef->resettimeout), tn);

    // create mutex
    sdef->mutex = 0;

    return true;
}

bool init_sync_defaults(sync_defaults* sdef, int argc, char** argv)
{
    int ival, ival_3;
    tree_node* ptr = NULL;
    char tstr[256];

    // load program defaults
#ifndef INTEGRITY
    defaults* def = load_defaults("+sync.cfg", argv[0]);
    if (!def)
    {
        return false;
    }
    def->parse_cmdline(argc, argv);
#else
    defaults* def = load_defaults(sync_cfg, argv[0]);
#endif
#ifndef NO_DOUT
    // init dout
    dout.useconsole(def->getnode("console") != NULL);
    if ((def->getstring("log", tstr) != NULL) && (tstr[0] != '\0'))
    {
        dout.openfile(tstr);
    }
    if ((def->getstring("udpaddr", tstr) != NULL) &&
        (def->getint("udport", &ival) != NULL))
    {
        dout.openudp(tstr, ival);
    }
#endif

    // load connection settings
    if (!init_conn_defaults(def, &sdef[0], "connection1"))
    {
        return false;
    }
    if (!init_conn_defaults(def, &sdef[1], "connection2"))
    {
        return false;
    }

    // tie up the two connections
    sdef[0].other_output_packet_id_queue = &(sdef[1].output_packet_id_queue);
    sdef[1].other_output_packet_id_queue = &(sdef[0].output_packet_id_queue);
    sdef[0].other_sock_state = &(sdef[1].sock_state);
    sdef[1].other_sock_state = &(sdef[0].sock_state);
    sdef[0].other_sock = sdef[1].sock;
    sdef[1].other_sock = sdef[0].sock;
    sdef[0].other_output_queue_has_data = &(sdef[1].output_queue_has_data);
    sdef[1].other_output_queue_has_data = &(sdef[0].output_queue_has_data);
    sdef[0].other_mutex = &sdef[1].mutex;
    sdef[1].other_mutex = &sdef[0].mutex;
    sdef[0].myname = '1';
    sdef[1].myname = '2';
    sdef[0].othername = sdef[1].myname;
    sdef[1].othername = sdef[0].myname;
    sdef[0].other_last_close_timestamp = &(sdef[1].last_close_timestamp);
    sdef[1].other_last_close_timestamp = &(sdef[0].last_close_timestamp);

    // loading of variables
    re_private re;

    while ((ptr = def->getstring("/data/forward", tstr, ptr)) != NULL)
    {
        //test if the string is of the expression A[]B,a[]b
        if (re.process3(tstr))
        {
            ival = re.start;
            ival_3 = re.start_3;
            do
            {
                sync_defaults::varlist.push_back(varelem(1, string(re.combine(ival)),string(re.combine3(ival_3))));
            }
            while((++ival <= re.end) && (++ival_3 <= re.end_3));
        }
        //test if the string is of the expression A,B
        else if (re.process2(tstr))
        {
            sync_defaults::varlist.push_back(varelem(1, string(re.get_before()), string(re.get_after())));
        }
        //test if the string is of the expression A[]B
        else if (re.process(tstr))
        {
            for (ival = re.start; ival <= re.end; ++ival)
            {
                sync_defaults::varlist.push_back(varelem(1, string(re.combine(ival))));
            }
        }
        //The string is definately of the expression A...
        else
        {
            sync_defaults::varlist.push_back(varelem(1, string(tstr)));
        }
    }
    while ((ptr = def->getstring("/data/backward", tstr, ptr)) != NULL)
    {
        if (re.process3(tstr))
        {
            ival = re.start;
            ival_3 = re.start_3;
            do
            {
                sync_defaults::varlist.push_back(varelem(2, string(re.combine(ival)),string(re.combine3(ival_3))));
            }
            while((++ival <= re.end) && (++ival_3 <= re.end_3));
        }
        else if (re.process2(tstr))
        {
            sync_defaults::varlist.push_back(varelem(2, string(re.get_before()), string(re.get_after())));
        }
        else if (re.process(tstr))
        {
            for (ival = re.start; ival <= re.end; ++ival)
            {
                sync_defaults::varlist.push_back(varelem(2, string(re.combine(ival))));
            }
        }
        else
        {
            sync_defaults::varlist.push_back(varelem(2, string(tstr)));
        }
    }
    while ((ptr = def->getstring("/data/bidirect", tstr, ptr)) != NULL)
    {
        if (re.process3(tstr))
        {
            ival = re.start;
            ival_3 = re.start_3;
            do
            {
                sync_defaults::varlist.push_back(varelem(3, string(re.combine(ival)),string(re.combine3(ival_3))));
            }
            while((++ival <= re.end) && (++ival_3 <= re.end_3));
        }
        else if (re.process2(tstr))
        {
            sync_defaults::varlist.push_back(varelem(3, string(re.get_before()), string(re.get_after())));
        }
        else if (re.process(tstr))
        {
            for (ival = re.start; ival <= re.end; ++ival)
            {
                sync_defaults::varlist.push_back(varelem(3, string(re.combine(ival))));
            }
        }
        else
        {
            sync_defaults::varlist.push_back(varelem(3, string(tstr)));
        }
    }
    while ((ptr = def->getstring("/data/forward_bc", tstr, ptr)) != NULL)
    {
        if (re.process3(tstr))
        {
            ival = re.start;
            ival_3 = re.start_3;
            do
            {
                sync_defaults::varlist.push_back(varelem(5, string(re.combine(ival)),string(re.combine3(ival_3))));
            }
            while((++ival <= re.end) && (++ival_3 <= re.end_3));
        }
        else if (re.process2(tstr))
        {
            sync_defaults::varlist.push_back(varelem(5, string(re.get_before()), string(re.get_after())));
        }
        else if (re.process(tstr))
        {
            for (ival = re.start; ival <= re.end; ++ival)
            {
                sync_defaults::varlist.push_back(varelem(5, string(re.combine(ival))));
            }
        }
        else
        {
            sync_defaults::varlist.push_back(varelem(5, string(tstr)));
        }
    } //while
    while ((ptr = def->getstring("/data/backward_bc", tstr, ptr)) != NULL)
    {
        if (re.process3(tstr))
        {
            ival = re.start;
            ival_3 = re.start_3;
            do
            {
                sync_defaults::varlist.push_back(varelem(6, string(re.combine(ival)),string(re.combine3(ival_3))));
            }
            while((++ival <= re.end) && (++ival_3 <= re.end_3));
        }
        else if (re.process2(tstr))
        {
            sync_defaults::varlist.push_back(varelem(6, string(re.get_before()), string(re.get_after())));
        }
        else if (re.process(tstr))
        {
            for (ival = re.start; ival <= re.end; ++ival)
            {
                sync_defaults::varlist.push_back(varelem(6, string(re.combine(ival))));
            }
        }
        else
        {
            sync_defaults::varlist.push_back(varelem(6, string(tstr)));
        }
    }
    while ((ptr = def->getstring("/data/bidirect_bc", tstr, ptr)) != NULL)
    {
        if (re.process3(tstr))
        {
            ival = re.start;
            ival_3 = re.start_3;
            do
            {
                sync_defaults::varlist.push_back(varelem(7, string(re.combine(ival)),string(re.combine3(ival_3))));
            }
            while((++ival <= re.end) && (++ival_3 <= re.end_3));
        }
        else if (re.process2(tstr))
        {
            sync_defaults::varlist.push_back(varelem(7, string(re.get_before()), string(re.get_after())));
        }
        else if (re.process(tstr))
        {
            for (ival = re.start; ival <= re.end; ++ival)
            {
                sync_defaults::varlist.push_back(varelem(7, string(re.combine(ival))));
            }
        }
        else
        {
            sync_defaults::varlist.push_back(varelem(7, string(tstr)));
        }
    }
    while ((ptr = def->getstring("/data/forward_dt", tstr, ptr)) != NULL)
    {
        if (re.process3(tstr))
        {
            ival = re.start;
            ival_3 = re.start_3;
            do
            {
                sync_defaults::varlist.push_back(varelem(9, string(re.combine(ival)),string(re.combine3(ival_3))));
            }
            while((++ival <= re.end) && (++ival_3 <= re.end_3));
        }
        else if (re.process2(tstr))
        {
            sync_defaults::varlist.push_back(varelem(9, string(re.get_before()), string(re.get_after())));
        }
        else if (re.process(tstr))
        {
            for (ival = re.start; ival <= re.end; ++ival)
            {
                sync_defaults::varlist.push_back(varelem(9, string(re.combine(ival))));
            }
        }
        else
        {
            sync_defaults::varlist.push_back(varelem(9, string(tstr)));
        }
    }
    while ((ptr = def->getstring("/data/backward_dt", tstr, ptr)) != NULL)
    {
        if (re.process3(tstr))
        {
            ival = re.start;
            ival_3 = re.start_3;
            do
            {
                sync_defaults::varlist.push_back(varelem(10, string(re.combine(ival)),string(re.combine3(ival_3))));
            }
            while((++ival <= re.end) && (++ival_3 <= re.end_3));
        }
        else if (re.process2(tstr))
        {
            sync_defaults::varlist.push_back(varelem(10, string(re.get_before()), string(re.get_after())));
        }
        else if (re.process(tstr))
        {
            for (ival = re.start; ival <= re.end; ++ival)
            {
                sync_defaults::varlist.push_back(varelem(10, string(re.combine(ival))));
            }
        }
        else
        {
            sync_defaults::varlist.push_back(varelem(10, string(tstr)));
        }
    }
    while ((ptr = def->getstring("/data/bidirect_dt", tstr, ptr)) != NULL)
    {
        if (re.process3(tstr))
        {
            ival = re.start;
            ival_3 = re.start_3;
            do
            {
                sync_defaults::varlist.push_back(varelem(11, string(re.combine(ival)),string(re.combine3(ival_3))));
            }
            while((++ival <= re.end) && (++ival_3 <= re.end_3));
        }
        else if (re.process2(tstr))
        {
            sync_defaults::varlist.push_back(varelem(11, string(re.get_before()), string(re.get_after())));
        }
        else if (re.process(tstr))
        {
            for (ival = re.start; ival <= re.end; ++ival)
            {
                sync_defaults::varlist.push_back(varelem(11, string(re.combine(ival))));
            }
        }
        else
        {
            sync_defaults::varlist.push_back(varelem(11, string(tstr)));
        }
    }
    while ((ptr = def->getstring("/data/data", tstr, ptr)) != NULL)
    {
        const char* after;
        size_t beflen = strlen(tstr);
        size_t aftlen = extract_equal(tstr, &after, beflen);
        const char* before = &tstr[0];

        if (aftlen == 0)
        {
            continue;
        }

        clear_whitespace(&before, beflen);
        clear_whitespace(&after, aftlen);

        if (aftlen == 0)
        {
            continue;
        }

        string bef(before, beflen);
        string aft(after, aftlen);
        int second;
        if (bef == "forward")
        {
            second = 1;
        }
        else if (bef == "backward")
        {
            second = 2;
        }
        else if (bef == "bidirect")
        {
            second = 3;
        }
        else if (bef == "forward_bc")
        {
            second = 5;
        }
        else if (bef == "backward_bc")
        {
            second = 6;
        }
        else if (bef == "bidirect_bc")
        {
            second = 7;
        }
        else if (bef == "forward_dt")
        {
            second = 9;
        }
        else if (bef == "backward_dt")
        {
            second = 10;
        }
        else if (bef == "bidirect_dt")
        {
            second = 11;
        }
        else
        {
            continue;
        }

        //test if the string is of the expression A[]B,a[]b
        if (re.process3(aft.c_str()))
        {
            ival = re.start;
            ival_3 = re.start_3;
            do
            {
                sync_defaults::varlist.push_back(varelem(second, string(re.combine(ival)),string(re.combine3(ival_3))));
            }
            while((++ival <= re.end) && (++ival_3 <= re.end_3));
        }
        //test if the string is of the expression A,B
        else if (re.process2(aft.c_str()))
        {
            sync_defaults::varlist.push_back(varelem(second, string(re.get_before()), string(re.get_after())));
        }
        //test if the string is of the expression A[]B
        else if (re.process(aft.c_str()))
        {
            for (ival = re.start; ival <= re.end; ++ival)
            {
                sync_defaults::varlist.push_back(varelem(second, string(re.combine(ival))));
            }
        }
        //The string is definately of the expression A...
        else
        {
            sync_defaults::varlist.push_back(varelem(second, aft));
        }
    }
    delete def;

    return true;
}

void sync_defaults::outputQueuedPackets()
{
    // dispatch up to x packets in the holding queue at one time
    if (output_queue_has_data)
    {
        dout << "Sending queued packets on connection " << myname << '\n';

        bool ok = true;
        for (int cnt = 0; (cnt < dispatch) && ok; ++cnt)
        {
            listWait(&mutex);
            const cache_item& cac_item = output_packet_id_queue.front();
            listSignal(&mutex);

            struct timeval tv_now;
            now(&tv_now);
            const struct timeval& tv_tstamp = cac_item.timestamp;
            const int timediff = millsecElapsed(&tv_now, &tv_tstamp);

            if (!txdelay || (timediff >= (txdelay->tv_sec * 1000 + txdelay->tv_usec / 1000)))
            {
                listWait(&mutex);
                ok = dispatch_packet(sock, cac_item.cac);
                dout << "tx:" << cac_item.timestamp.tv_sec << " now:" << tv_now.tv_sec << " buf1:" << *cac_item.cac->buf() << "\n";
                if (ok)
                {
                    output_packet_id_queue.pop_front();
                    output_queue_has_data = static_cast<int>(output_packet_id_queue.size());
                    ok = (output_queue_has_data > 0);
                }
                listSignal(&mutex);
            }
            else
            {
                ok = false;
            }
        }
    }
}

bool sync_defaults::waitForIncomingData()
{
    struct timeval tv = {
        1, 0
    };
    if (sock_state != 2)
    {
        return true;
    }
    else if (sock->Select(&tv) == -1)
    {
        external_close_requested = 1;
        return true;
    }
    else
    {
        return false;
    }
}

void sync_defaults::maintainConnection()
{
    switch (sock_state)
    {
    case 0: // Not connected
    {
        struct timeval nowtime;
        now(&nowtime);
        const unsigned int timediff = millsecElapsed(&nowtime, &last_connect_timestamp);
        if (timediff > 1000U)
        {
            if (sock->Connect(host, port))
            {
                if (flow != -1)
                {
                    sock->FlowControl(flow);
                }
                sock->ClientID(client);
                dout << "Connection " << myname << " established\n";
                sock_state = 1;
            }
            else
            {
                now(&last_connect_timestamp);
            }
        }
        else
        {
            millisleep(timediff);
        }
        break;
    }
    case 1: // Connected
    {
        if (*other_sock_state != 0)
        {
            dout << "Connection " << myname << " handshake\n";
            const int mask = 1 << (myname - '1');
            varlist_typ::const_iterator iter;
            for (iter = varlist.begin(); iter != varlist.end(); ++iter)
            {
                if (((*iter).second & mask) != 0)
                {
                    sock->RegisterID((*iter).first.c_str());
                }
                else
                {
                    sock->QueryID((*iter).first.c_str());
                }

                struct timeval tv = {
                    0, 0
                };
                if (sock->Select(&tv, true) == -1)
                {
                    sock->Close();
                    dout << "Connection " << myname << " handshake failed\n";
                    sock_state = 0;
                    now(&last_close_timestamp);
                    break;
                }
            }
            if (iter == varlist.end())
            {
                sock_state = 2;
                reject_clientID = true;
                dout << "Connection " << myname << " ready\n";
            }
        }
        else
        {
            if (sock->Select(select_tv, true) == -1)
            {
                sock->Close();
                dout << "Connection " << myname << " closed due to errors\n";
                sock_state = 0;
                now(&last_close_timestamp);
            }
            else
            {
                dout << "Connection " << myname << " waiting for other connection to be established\n";
                millisleep(5);
            }
        }
        break;
    }
    case 2: // Ready
    {
        struct timeval nowtime;
        now(&nowtime);
        if (external_close_requested)
        {
            sock->Close();
            dout << "Connection " << myname << " closed due to errors\n";
            sock_state = 0;
            last_close_timestamp = nowtime;
            external_close_requested = 0;
        }
        else if (*other_sock_state == 0 && millsecElapsed(&nowtime, other_last_close_timestamp) > (resettimeout * 1000U))
        {
            sock->ClientID(client);
            dout << "Connection " << myname << " suspended as other socket is non-viable > " << resettimeout << "sec\n";
            sock_state = 1;
        }
        else if (output_queue_has_data)
        {
            outputQueuedPackets();
            sock->Flush();
            millisleep(30);
        }
        else
        {
            sock->KeepAlive();
            sock->Flush();
            millisleep(30);
        }
        break;
    }
    }
}
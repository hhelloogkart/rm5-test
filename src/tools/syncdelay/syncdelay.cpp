#include "syncdelaydef.h"
#include <task/threadt.h>
#include <util/dbgout.h>
#include <time/timecore.h>
#include <signal.h>
#ifdef _WIN32
#include <windows.h>
#endif

static bool exit_flag = false;

static bool maintainConnection(void* arg)
{
    sync_defaults* sdef = (sync_defaults*)arg;  //initialize sync_defaults
    sdef->maintainConnection();
    return false;
}

static bool waitForIncomingData(void* arg)
{
    sync_defaults* sdef = (sync_defaults*)arg;  //initialize sync_defaults
    return sdef->waitForIncomingData();
}

static void signalled(int)
{
    exit_flag = true;
}

int main(int argc, char** argv)
{
    sync_defaults connection[2];
    if (!init_sync_defaults(connection, argc, argv))
    {
        dout << "Unable to initialize properly. Check sync.cfg.\n";
        return 1;
    }

    signal(SIGINT, &signalled);
    signal(SIGTERM, &signalled);

    thread_typ conn_thread1(&::maintainConnection, &(connection[0]), true);
    thread_typ conn_thread2(&::maintainConnection, &(connection[1]), true);
    thread_typ* select_thread1 = 0;
    thread_typ* select_thread2 = 0;

    while (!exit_flag)
    {
        if (select_thread1 != 0)
        {
            if (select_thread1->finished())
            {
                delete select_thread1;
                select_thread1 = 0;
            }
        }
        else if (connection[0].sock_state == 2 && connection[0].external_close_requested == 0)
        {
            select_thread1 = new thread_typ(&::waitForIncomingData, &(connection[0]), true);
        }

        if (select_thread2 != 0)
        {
            if (select_thread2->finished())
            {
                delete select_thread2;
                select_thread2 = 0;
            }
        }
        else if (connection[1].sock_state == 2 && connection[1].external_close_requested == 0)
        {
            select_thread2 = new thread_typ(&::waitForIncomingData, &(connection[1]), true);
        }

#ifdef _WIN32
        MSG msg;
        if (PeekMessage(&msg, 0, WM_CLOSE, WM_CLOSE, PM_REMOVE))
        {
            exit_flag = true;
        }
#endif
        millisleep(10);
    }

    delete select_thread1;
    delete select_thread2;

    return 0;
}

#ifdef __MAKECINT__
#include <socket.h>
#endif

#include "rm_var.h"
#include "defutil.h"
#include "rmsocket.h"

extern "C"
{

/* def_util */

RM_EXPORT rmclient_init* rmclient_init_new(const char* defstr);
RM_EXPORT void rmclient_init_delete(rmclient_init* init);
RM_EXPORT void rmclient_init_set_version_string(rmclient_init* init, const char* ver);
RM_EXPORT void rmclient_init_set_clientname(rmclient_init* init, const char* cname);
RM_EXPORT void rmclient_init_set_host(rmclient_init* init, const char* host);
RM_EXPORT void rmclient_init_set_port(rmclient_init* init, int port);
RM_EXPORT void rmclient_init_set_flow(rmclient_init* init, unsigned int flow);
RM_EXPORT bool rmclient_init_client_exit(rmclient_init* init);
RM_EXPORT defaults* rmclient_init_client_defaults(rmclient_init* init);

/* defaults */

RM_EXPORT defaults* defaults_new(const char* _data = NULL, size_t len = ULONG_MAX);
RM_EXPORT void defaults_delete(defaults* def);
RM_EXPORT void defaults_load(defaults* def, const char* _data, size_t len = ULONG_MAX);
RM_EXPORT void defaults_clear(defaults* def);
RM_EXPORT const char* defaults_getString(defaults* def, const char* srchkey, bool* b_fail = 0);
RM_EXPORT int defaults_getInt(defaults* def, const char* srchkey, bool* b_fail = 0);
RM_EXPORT double defaults_getDouble(defaults* def, const char* srchkey, bool* b_fail = 0);

/* rm_var */
RM_EXPORT int lvCheckReceive(unsigned* len, unsigned* id);
RM_EXPORT int lvReceive(unsigned* len, unsigned* id, unsigned char* buf);
RM_EXPORT void lv_set_callback(RMBaseSocket* s);

RM_EXPORT rm_var* rm_var_new(const char nm[], bool _regflag = false);
RM_EXPORT void rm_var_delete(rm_var* var);
RM_EXPORT void rm_var_initialize(
    const char* client, int port, const char* host = (const char*)NULL,
    RMBaseSocket* clientsock = (RMBaseSocket*)NULL,
    unsigned int flow = 0, unsigned long wait_us = (unsigned long)-1);
RM_EXPORT void rm_var_outgoing(void);
RM_EXPORT void rm_var_incoming(void);
RM_EXPORT bool rm_var_check_connect(void);
RM_EXPORT rm_var* rm_var_findVariable(const char _name[]);
RM_EXPORT void rm_var_set_host(const char* host);
RM_EXPORT void rm_var_set_port(int port);
RM_EXPORT void rm_var_set_client(const char* client);
RM_EXPORT void rm_var_set_flow(unsigned int flow);
RM_EXPORT RMBaseSocket* rm_var_Socket();
RM_EXPORT void rm_var_setName(rm_var* var, const char _name[]);
RM_EXPORT const char* rm_var_getName(rm_var* var);
RM_EXPORT bool rm_var_chkName(rm_var* var, const char _name[]);
RM_EXPORT unsigned long rm_var_getID(rm_var* var);
RM_EXPORT void rm_var_setID(rm_var* var, unsigned long _id);
RM_EXPORT bool rm_var_getRegister(rm_var* var);
RM_EXPORT void rm_var_setRegister(rm_var* var, bool _regflag);
RM_EXPORT bool rm_var_queryRM(rm_var* var);
RM_EXPORT bool rm_var_registerRM(rm_var* var);
RM_EXPORT bool rm_var_unregisterRM(rm_var* var);
RM_EXPORT void rm_var_refreshRM(rm_var* var);
RM_EXPORT void rm_var_remove(rm_var* var);
RM_EXPORT void rm_var_destroy(rm_var* var);
RM_EXPORT void rm_var_setRMDirty(rm_var* var);
//RM_EXPORT void rm_var_write(rm_var *var, outbuf &buf);

/* mpt_show */
typedef LIST(mpt_var*) showlist_var_typ;
class labview_show : public mpt_autoshow
{
public:
    labview_show();
    virtual void setDirty(mpt_var*);
    mpt_var* getDirty();
    virtual void operator()();
protected:
    showlist_var_typ dirty;
};

RM_EXPORT void mpt_show_refresh(void);
RM_EXPORT void mpt_show_setDirty(mpt_baseshow* show, mpt_var* var);
RM_EXPORT void mpt_show_notDirty(mpt_baseshow* show, mpt_var* var);
RM_EXPORT void mpt_autoshow_addCallback(mpt_autoshow* show, mpt_var* _var);
RM_EXPORT void mpt_autoshow_removeCallback(mpt_autoshow* show, mpt_var* _var);
RM_EXPORT labview_show* labview_show_new(void);
RM_EXPORT void labview_show_delete(labview_show* show);
RM_EXPORT mpt_var* labview_show_getDirty(labview_show* show);

/* mpt_var */
//create variable
//destroy variable
//get at variable from rmname/varname/varname, return mpt_var *
//from variable pointer get value from var or set value into var.

RM_EXPORT int mpt_var_extract(mpt_var* var, int len, unsigned char* buf);
RM_EXPORT int mpt_var_size(mpt_var* var);
RM_EXPORT void mpt_var_output(mpt_var* var, outbuf* strm);
RM_EXPORT bool mpt_var_isValid(mpt_var* var);
RM_EXPORT void mpt_var_setInvalid(mpt_var* var);
RM_EXPORT void mpt_var_setRMDirty(mpt_var* var);
RM_EXPORT void mpt_var_addCallback(mpt_var* var, mpt_baseshow* show);
RM_EXPORT void mpt_var_removeCallback(mpt_var* var, mpt_baseshow* show);
RM_EXPORT void mpt_var_setDirty(mpt_var* var);
RM_EXPORT void mpt_var_notDirty(mpt_var* var);
RM_EXPORT void mpt_var_refresh(mpt_var* var);
RM_EXPORT void mpt_var_nullValue(mpt_var* var);

/* RMBaseSocket */

RM_EXPORT bool RMSocket_Connect(RMBaseSocket* s, const char* host, int port, unsigned long wait_us = (unsigned long)-1);
RM_EXPORT bool RMSocket_Connected(RMBaseSocket* s);
RM_EXPORT bool RMSocket_Close(RMBaseSocket* s);
RM_EXPORT int  RMSocket_Select(RMBaseSocket* s, unsigned long wait_us = (unsigned long)-1, bool defercb = false);
RM_EXPORT const cache_typ* RMSocket_GetListStart(RMBaseSocket* s, unsigned int id);
RM_EXPORT void RMSocket_GetListEnd(RMBaseSocket* s, unsigned int id, const cache_typ* pbuf);
RM_EXPORT cache_typ* RMSocket_SetListStart(RMBaseSocket* s, unsigned int id, size_t len);
RM_EXPORT bool RMSocket_SetListEnd(RMBaseSocket* s, unsigned int id, cache_typ* pbuf);
RM_EXPORT bool RMSocket_BroadcastEnd(RMBaseSocket* s, unsigned int id, cache_typ* pbuf);
RM_EXPORT bool RMSocket_RegisterID(RMBaseSocket* s, const char* str);
RM_EXPORT bool RMSocket_QueryID(RMBaseSocket* s, const char* str);
//RM_EXPORT bool RMSocket_QueryID2(RMBaseSocket *s, const char *str[], int num);

RM_EXPORT bool RMSocket_ClientID(RMBaseSocket* s, const char* client);
RM_EXPORT bool RMSocket_RegisterIDX(RMBaseSocket* s, unsigned int id);
RM_EXPORT bool RMSocket_UnregisterIDX(RMBaseSocket* s, unsigned int id);
RM_EXPORT bool RMSocket_FlowControl(RMBaseSocket* s, unsigned int interval);
RM_EXPORT bool RMSocket_WaitForHandshakeCompletion(RMBaseSocket* s, unsigned long wait_us = (unsigned long)-1);
RM_EXPORT void RMSocket_KeepAlive(RMBaseSocket* s);
RM_EXPORT bool RMSocket_Broadcast(RMBaseSocket* s, unsigned int id, size_t len, const unsigned int* buf);
RM_EXPORT bool RMSocket_SetList(RMBaseSocket* s, unsigned int id, size_t len, const unsigned int* buf);
RM_EXPORT size_t RMSocket_GetList(RMBaseSocket* s, unsigned int id, size_t len, unsigned int* buf);
//RM_EXPORT void RMSocket_AddCallback(RMBaseSocket *s, unsigned int cmd, void *arg, RMCallback func);
RM_EXPORT void RMSocket_Sleep(int msec);
RM_EXPORT RMBaseSocket* RMSocket_Construct(const char* accessstr);

/* RM TCP Socket */

RM_EXPORT bool RMTCPSocket_SendMessageC(RMTCPSocket* t, const cache_typ* ptr);
RM_EXPORT bool RMTCPSocket_SendMessage(RMTCPSocket* t, unsigned int id, size_t len, const unsigned int* buf, int cmd);

/* cache_factory */

RM_EXPORT unsigned cache_factory_output(unsigned len, unsigned int* buf, const cache_typ* ptr);


/* cache_typ */

/*
   RM_EXPORT void cache_construct(cache_typ *c, unsigned int _len, MM *fac = (MM *)0);
   RM_EXPORT const unsigned int *cache_constbuf(cache_typ *c);
   RM_EXPORT unsigned int *cache_buf(cache_typ *c);
   RM_EXPORT const unsigned int *cache_constpacket(cache_typ *c);
   RM_EXPORT unsigned int *cache_packet(cache_typ *c);
   RM_EXPORT void cache_reserve(cache_typ *c);
   RM_EXPORT void cache_unreserve(cache_typ *c);
   RM_EXPORT unsigned int cache_get_refcount(cache_typ *c);
   RM_EXPORT unsigned int cache_get_id(cache_typ *c);
   RM_EXPORT void cache_set_id(cache_typ *c, unsigned int _id);
   RM_EXPORT unsigned int cache_get_len(cache_typ *c);
   RM_EXPORT void cache_set_len(cache_typ *c, unsigned int _len);
   RM_EXPORT void cache_set_seq(cache_typ *c, unsigned short _seq);
   RM_EXPORT unsigned int cache_get_cmd(cache_typ *c);
   RM_EXPORT void cache_set_cmd(cache_typ *c, unsigned int _cmd);
   RM_EXPORT void cache_copy_header(cache_typ *c, unsigned int *buf);
   RM_EXPORT void cache_rd_lock(cache_typ *c);
   RM_EXPORT void cache_lock(cache_typ *c);
   RM_EXPORT void cache_unlock(cache_typ *c);
 */

}

#include "rm4cwrap.h"

#include <string>
#include <string.h>

/* defutil : rmclient_init */

RM_EXPORT rmclient_init* rmclient_init_new(const char* defstr)
{
    return new rmclient_init(defstr);
}

RM_EXPORT void rmclient_init_delete(rmclient_init* init)
{
    delete(init);
    init = NULL;
}

RM_EXPORT void rmclient_init_set_version_string(rmclient_init* init, const char* ver)
{
    init->set_version_string(ver);
}

RM_EXPORT void rmclient_init_set_clientname(rmclient_init* init, const char* cname)
{
    init->set_clientname(cname);
}

RM_EXPORT void rmclient_init_set_host(rmclient_init* init, const char* host)
{
    init->set_host(host);
}

RM_EXPORT void rmclient_init_set_port(rmclient_init* init, int port)
{
    init->set_port(port);
}

RM_EXPORT void rmclient_init_set_flow(rmclient_init* init, unsigned int flow)
{
    init->set_flow(flow);
}

RM_EXPORT bool rmclient_init_client_exit(rmclient_init* init)
{
    return init->client_exit();
}

RM_EXPORT defaults* rmclient_init_client_defaults(rmclient_init* init)
{
    return init->client_defaults();
}

/* defaults */

RM_EXPORT defaults* defaults_new(const char* _data, size_t len)
{
    return new defaults(_data, len);
}

RM_EXPORT void defaults_delete(defaults* def)
{
    delete def;
}

RM_EXPORT void defaults_load(defaults* def, const char* _data, size_t len)
{
    def->load(_data, len);
}

RM_EXPORT void defaults_clear(defaults* def)
{
    def->clear();
}

RM_EXPORT const char* defaults_getString(defaults* def, const char* srchkey, bool* b_fail)
{
    return def->getString(srchkey, b_fail);
}

RM_EXPORT int defaults_getInt(defaults* def, const char* srchkey, bool* b_fail)
{
    return def->getInt(srchkey, b_fail);
}

RM_EXPORT double defaults_getDouble(defaults* def, const char* srchkey, bool* b_fail)
{
    return def->getDouble(srchkey, b_fail);
}

/* call back to store packet received */
struct pkt_typ
{
    void* buf;
    unsigned len;
    unsigned id;
};
typedef LIST(pkt_typ) pkt_list_typ;

static pkt_list_typ pkt_list;

static void lvDoSetList(void*, unsigned int id, const cache_typ* buf)
{
    /*
       if (id <= rmlist().size())
       {
       rmlist()[id-1]->extract(buf->get_len()*sizeof(unsigned int),
        (unsigned char *)(buf->buf()));
       }
       else */
    //3 variable for @client, !client and lvClient
    if(id >= 4)
    {
        pkt_typ p = { 0, buf->get_len()*sizeof(unsigned int), id };
        if(p.len && (p.buf = (void*)new char[p.len]))
        {
            memcpy(p.buf, buf->buf(), p.len);
        }
        pkt_list.push_back(p);
    }
}

RM_EXPORT void lv_set_callback(RMBaseSocket* s)
{
    s->AddCallback(SETLIST, NULL, lvDoSetList);
}

RM_EXPORT int lvCheckReceive(unsigned* len, unsigned* id)
{
    int found = 0;
    if(!pkt_list.empty())
    {
        pkt_typ p = pkt_list.front();
        found = 1;
        *len = p.len;
        *id = p.id;
    }
    return found;
}

RM_EXPORT int lvReceive(unsigned* len, unsigned* id, unsigned char* buf)
{
    int found = 0;
    if(!pkt_list.empty())
    {
        pkt_typ p = pkt_list.front();
        pkt_list.pop_front();
        found = 1;
        *len = p.len;
        *id = p.id;
        if(p.len && buf)
        {
            memcpy(buf, p.buf, p.len);
        }
    }
    return found;
}


/* rm_var */

RM_EXPORT rm_var* rm_var_new(const char nm[], bool _regflag)
{
    return new rm_var(nm, _regflag);
}

RM_EXPORT void rm_var_delete(rm_var* var)
{
    delete var;
}

RM_EXPORT void rm_var_initialize(
    const char* client, int port, const char* host,
    RMBaseSocket* clientsock,
    unsigned int flow, unsigned long wait_us)
{
    struct timeval val, * tv = NULL;
    if(wait_us != (unsigned long)-1)
    {
        val.tv_sec  = wait_us / 1000000UL;
        val.tv_usec = wait_us % 1000000UL;
        tv = &val;
    }
    rm_var::initialize(client, port, host, clientsock, flow, tv);
    lv_set_callback(clientsock);
}

RM_EXPORT void rm_var_outgoing(void)
{
    rm_var::outgoing();
}

RM_EXPORT void rm_var_incoming(void)
{
    rm_var::incoming();
}

RM_EXPORT bool rm_var_check_connect(void)
{
    return rm_var::check_connect();
}

RM_EXPORT rm_var* rm_var_findVariable(const char _name[])
{
    return rm_var::findVariable(_name);
}

RM_EXPORT void rm_var_set_host(const char* host)
{
    rm_var::set_host(host);
}

RM_EXPORT void rm_var_set_port(int port)
{
    rm_var::set_port(port);
}

RM_EXPORT void rm_var_set_client(const char* client)
{
    rm_var::set_client(client);
}

RM_EXPORT void rm_var_set_flow(unsigned int flow)
{
    rm_var::set_flow(flow);
}

RM_EXPORT RMBaseSocket* rm_var_Socket()
{
    return &(rm_var::Socket());
}

RM_EXPORT void rm_var_setName(rm_var* var, const char _name[])
{
    var->setName(_name);
}

RM_EXPORT const char* rm_var_getName(rm_var* var)
{
    return var->getName();
}

RM_EXPORT bool rm_var_chkName(rm_var* var, const char _name[])
{
    return var->chkName(_name);
}

RM_EXPORT unsigned long rm_var_getID(rm_var* var)
{
    return var->getID();
}

RM_EXPORT void rm_var_setID(rm_var* var, unsigned long _id)
{
    var->setID(_id);
}

RM_EXPORT bool rm_var_getRegister(rm_var* var)
{
    return var->getRegister();
}

RM_EXPORT void rm_var_setRegister(rm_var* var, bool _regflag)
{
    var->setRegister(_regflag);
}

RM_EXPORT bool rm_var_queryRM(rm_var* var)
{
    return var->queryRM();
}

RM_EXPORT bool rm_var_registerRM(rm_var* var)
{
    return var->registerRM();
}

RM_EXPORT bool rm_var_unregisterRM(rm_var* var)
{
    return var->unregisterRM();
}

RM_EXPORT void rm_var_refreshRM(rm_var* var)
{
    var->refreshRM();
}

RM_EXPORT void rm_var_remove(rm_var* var)
{
    var->remove();
}

RM_EXPORT void rm_var_destroy(rm_var* var)
{
    var->destroy();
}

RM_EXPORT void rm_var_setRMDirty(rm_var* var)
{
    var->setRMDirty();
}

RM_EXPORT void rm_var_write(rm_var* var, outbuf& buf)
{
    var->write(buf);
}


/* mpt_show */

labview_show::labview_show() : mpt_autoshow(), dirty()
{
}

void labview_show::setDirty(mpt_var* var)
{
    if(var)
    {
        showlist_var_typ::iterator it;
        for(it = dirty.begin(); it != dirty.end(); ++it)
        {
            if(*it == var)
            {
                return;
            }
        }
        dirty.push_back(var);
    }
}

mpt_var* labview_show::getDirty()
{
    mpt_var* var = NULL;
    if(!dirty.empty())
    {
        var = dirty.front();
        dirty.pop_front();
    }
    return var;
}

void labview_show::operator()()
{
    dirty.clear();
}


RM_EXPORT void mpt_show_refresh(void)
{
    mpt_show::refresh();
}

RM_EXPORT void mpt_show_setDirty(mpt_baseshow* show, mpt_var* var)
{
    show->setDirty(var);
}

RM_EXPORT void mpt_show_notDirty(mpt_baseshow* show, mpt_var* var)
{
    show->notDirty(var);
}

RM_EXPORT void mpt_autoshow_addCallback(mpt_autoshow* show, mpt_var* _var)
{
    show->addCallback(_var);
}

RM_EXPORT void mpt_autoshow_removeCallback(mpt_autoshow* show, mpt_var* _var)
{
    show->removeCallback(_var);
}

RM_EXPORT labview_show* labview_show_new(void)
{
    return new labview_show();
}

RM_EXPORT void labview_show_delete(labview_show* show)
{
    delete show;
}

RM_EXPORT mpt_var* labview_show_getDirty(labview_show* show)
{
    return show->getDirty();
}

/* mpt_var */
//create variable
//destroy variable
//get at variable from rmname/varname/varname, return mpt_var *
//from variable pointer get value from var or set value into var.

RM_EXPORT int mpt_var_extract(mpt_var* var, int len, unsigned char* buf)
{
    return var->extract(len, buf);
}

RM_EXPORT int mpt_var_size(mpt_var* var)
{
    return var->size();
}

RM_EXPORT void mpt_var_output(mpt_var* var, outbuf* strm)
{
    var->output(*strm);
}

RM_EXPORT bool mpt_var_isValid(mpt_var* var)
{
    return var->isValid();
}

RM_EXPORT void mpt_var_setInvalid(mpt_var* var)
{
    var->setInvalid();
}

RM_EXPORT void mpt_var_setRMDirty(mpt_var* var)
{
    var->setRMDirty();
}

RM_EXPORT void mpt_var_addCallback(mpt_var* var, mpt_baseshow* show)
{
    var->addCallback(show);
}

RM_EXPORT void mpt_var_removeCallback(mpt_var* var, mpt_baseshow* show)
{
    var->removeCallback(show);
}

RM_EXPORT void mpt_var_setDirty(mpt_var* var)
{
    var->setDirty();
}

RM_EXPORT void mpt_var_notDirty(mpt_var* var)
{
    var->notDirty();
}

RM_EXPORT void mpt_var_refresh(mpt_var* var)
{
    var->refresh();
}

RM_EXPORT void mpt_var_nullValue(mpt_var* var)
{
    var->nullValue();
}

RM_EXPORT mpt_var* mpt_var_getNext(mpt_var* var)
{
    return var->getNext();
}

/* RMBaseSocket */

RM_EXPORT bool RMSocket_Connect(RMBaseSocket* s, const char* host, int port, unsigned long wait_us)
{
    struct timeval val, * tv = NULL;
    if(wait_us != (unsigned long)-1)
    {
        val.tv_sec  = wait_us / 1000000UL;
        val.tv_usec = wait_us % 1000000UL;
        tv = &val;
    }
    return s->Connect(host, port, tv);
}

RM_EXPORT bool RMSocket_Connected(RMBaseSocket* s)
{
    return s->Connected();
}

RM_EXPORT bool RMSocket_Close(RMBaseSocket* s)
{
    return s->Close();
}

RM_EXPORT int  RMSocket_Select(RMBaseSocket* s, unsigned long wait_us, bool defercb)
{
    struct timeval val, * tv = NULL;
    if(wait_us != (unsigned long)-1)
    {
        val.tv_sec  = wait_us / 1000000UL;
        val.tv_usec = wait_us % 1000000UL;
        tv = &val;
    }
    return s->Select(tv, defercb);
}

RM_EXPORT const cache_typ* RMSocket_GetListStart(RMBaseSocket* s, unsigned int id)
{
    return s->GetListStart(id);
}

RM_EXPORT void RMSocket_GetListEnd(RMBaseSocket* s, unsigned int id, const cache_typ* pbuf)
{
    s->GetListEnd(id, pbuf);
}

RM_EXPORT cache_typ* RMSocket_SetListStart(RMBaseSocket* s, unsigned int id, size_t len)
{
    return s->SetListStart(id, len);
}

RM_EXPORT bool RMSocket_SetListEnd(RMBaseSocket* s, unsigned int id, cache_typ* pbuf)
{
    return s->SetListEnd(id, pbuf);
}

RM_EXPORT bool RMSocket_BroadcastEnd(RMBaseSocket* s, unsigned int id, cache_typ* pbuf)
{
    return s->BroadcastEnd(id, pbuf);
}

RM_EXPORT bool RMSocket_RegisterID(RMBaseSocket* s, const char* str)
{
    return s->RegisterID(str);
}

RM_EXPORT bool RMSocket_QueryID(RMBaseSocket* s, const char* str)
{
    return s->QueryID(str);
}

/*
   RM_EXPORT bool RMSocket_QueryID2(RMBaseSocket *s, const char *str[], int num)
   {
   return s->QueryID(str, num);
   }
 */

RM_EXPORT bool RMSocket_ClientID(RMBaseSocket* s, const char* client)
{
    return s->ClientID(client);
}

RM_EXPORT bool RMSocket_RegisterIDX(RMBaseSocket* s, unsigned int id)
{
    return s->RegisterIDX(id);
}

RM_EXPORT bool RMSocket_UnregisterIDX(RMBaseSocket* s, unsigned int id)
{
    return s->UnregisterIDX(id);
}

RM_EXPORT bool RMSocket_FlowControl(RMBaseSocket* s, unsigned int interval)
{
    return s->FlowControl(interval);
}

RM_EXPORT bool RMSocket_WaitForHandshakeCompletion(RMBaseSocket* s, unsigned long wait_us)
{
    struct timeval val, * tv = NULL;
    if(wait_us != (unsigned long)-1)
    {
        val.tv_sec  = wait_us / 1000000UL;
        val.tv_usec = wait_us % 1000000UL;
        tv = &val;
    }
    return s->WaitForHandshakeCompletion(tv);
}

RM_EXPORT void RMSocket_KeepAlive(RMBaseSocket* s)
{
    s->KeepAlive();
}

RM_EXPORT bool RMSocket_Broadcast(RMBaseSocket* s, unsigned int id, size_t len, const unsigned int* buf)
{
    return s->Broadcast(id, len, buf);
}

RM_EXPORT bool RMSocket_SetList(RMBaseSocket* s, unsigned int id, size_t len, const unsigned int* buf)
{
    return s->SetList(id, len, buf);
}

RM_EXPORT size_t RMSocket_GetList(RMBaseSocket* s, unsigned int id, size_t len, unsigned int* buf)
{
    return s->GetList(id, len, buf);
}

/*
   RM_EXPORT void RMSocket_AddCallback(RMBaseSocket *s, unsigned int cmd, void *arg, RMCallback func)
   {
   s->AddCallback(cmd, arg, func);
   }
 */

RM_EXPORT void RMSocket_Sleep(int msec)
{
    RMBaseSocket::Sleep(msec);
}

RM_EXPORT RMBaseSocket* RMSocket_Construct(const char* accessstr)
{
    return RMBaseSocket::Construct(accessstr);
}

/* RM TCP Socket */

RM_EXPORT bool RMTCPSocket_SendMessageC(RMTCPSocket* t, const cache_typ* ptr)
{
    return t->SendMessageC(ptr);
}

RM_EXPORT bool RMTCPSocket_SendMessage(RMTCPSocket* t, unsigned int id, size_t len, const unsigned int* buf, int cmd)
{
    return t->SendMessage(id, len, buf, cmd);
}

/* cache_factory */

RM_EXPORT unsigned cache_factory_output(unsigned len, unsigned int* buf, const cache_typ* ptr)
{
    return cache_factory::output(len, buf, ptr);
}


/* cache_typ */

/*
   RM_EXPORT void cache_construct(cache_typ *c, unsigned int _len, MM *fac = (MM *)0)
   {
    c->construct(_len, fac);
   }

   RM_EXPORT const unsigned int *cache_constbuf(cache_typ *c)
   {
    return c->buf();
   }

   RM_EXPORT unsigned int *cache_buf(cache_typ *c)
   {
    return c->buf();
   }

   RM_EXPORT const unsigned int *cache_constpacket(cache_typ *c)
   {
    return c->packet();
   }

   RM_EXPORT unsigned int *cache_packet(cache_typ *c)
   {
    return c->packet();
   }

   RM_EXPORT void cache_reserve(cache_typ *c)
   {
    c->reserve();
   }

   RM_EXPORT void cache_unreserve(cache_typ *c)
   {
    c->unreserve();
   }

   RM_EXPORT unsigned int cache_get_refcount(cache_typ *c)
   {
    return c->get_refcount();
   }

   RM_EXPORT unsigned int cache_get_id(cache_typ *c)
   {
    return c->get_id();
   }

   RM_EXPORT void cache_set_id(cache_typ *c, unsigned int _id)
   {
    c->set_id(_id);
   }

   RM_EXPORT unsigned int cache_get_len(cache_typ *c)
   {
    return c->get_len();
   }

   RM_EXPORT void cache_set_len(cache_typ *c, unsigned int _len)
   {
    c->set_len(_len);
   }

   RM_EXPORT void cache_set_seq(cache_typ *c, unsigned short _seq)
   {
    c->set_seq(_seq);
   }

   RM_EXPORT unsigned int cache_get_cmd(cache_typ *c)
   {
    return c->get_cmd();
   }

   RM_EXPORT void cache_set_cmd(cache_typ *c, unsigned int _cmd)
   {
    c->set_cmd(_cmd);
   }

   RM_EXPORT void cache_copy_header(cache_typ *c, unsigned int *buf)
   {
    c->copy_header(buf);
   }

   RM_EXPORT void cache_rd_lock(cache_typ *c)
   {
    c->rd_lock();
   }

   RM_EXPORT void cache_lock(cache_typ *c)
   {
    c->lock();
   }

   RM_EXPORT void cache_unlock(cache_typ *c)
   {
   c->unclock();
   }
 */

#include "rm_var.h"
#include "gen_var.h"

/* create a dummy variableto initialize everything. needed by labview client */

class var_labview_client : public rm_var
{
public:
    RMCONST(var_labview_client,"lvClient",0)
    generic_var<unsigned long> value;
};

var_labview_client lvClient;


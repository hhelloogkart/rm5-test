/******************************************************************/
/* Copyright DSO National Laboratories 2016. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "rdecode_bit_var.h"

inline static int bit_size_to_code_typ(int bsz)
{
    if (bsz < 0)
    {
        return 0x700 | (-bsz & 0xff);
    }
    else
    {
        return 0x300 | (bsz & 0xff);
    }
}

inline static int bit_size_to_code_typ(int bsz, int wsz)
{
    if (bsz < 0)
    {
        return (wsz << 16) | 0x700 | (-bsz & 0xff);
    }
    else
    {
        return (wsz << 16) | 0x300 | (bsz & 0xff);
    }
}

/**
   @param _bit_size Number of bits to represent encoded value. It is always treated as unsigned
   @param _range Range of value to encoded, if range is 0, scaling is 1.0
   @param _offset Offset of encoded value
   @param _scale_offset encoded_value = value * range / (2.0^(abs(_bit_size)) - _scale_offset) + _offset
    e.g. for 8 bit, range 360, _scale_offset = 0, scaling = 360/256
    e.g. for 8 bit, range 360, _scale_offset = 1, scaling = 360/255 (for most ATP data)
 */
rdecode_bit_var::rdecode_bit_var(int _byte_size, double _range, double _offset, int _scale_offset) :
    vdecode_var(bit_size_to_code_typ(_byte_size), _range, _offset, _scale_offset)
{
}

rdecode_bit_var::rdecode_bit_var(int _bit_size, int _word_size, double _range, double _offset, int _scale_offset) :
    vdecode_var(bit_size_to_code_typ(_bit_size, _word_size), _range, _offset, _scale_offset)
{
}

const mpt_var& rdecode_bit_var::operator=(const mpt_var& right)
{
    return vdecode_var::operator=(right);
}

double rdecode_bit_var::operator=(double right)
{
    return vdecode_var::operator=(right);
}

const rdecode_bit_var& rdecode_bit_var::operator=(const rdecode_bit_var& right)
{
    vdecode_var::operator=(right);
    return *this;
}

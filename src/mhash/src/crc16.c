/*
 */


#include "libdefs.h"
#include "mutils/mutils.h"

#ifdef ENABLE_CRC16

#include "mhash_crc16.h"

#define CRC16_POLYNOMIAL    0x1021

static mutils_word16 crc16_table[256];

void
mhash_crc16_init(void)
{
    unsigned short i = 0;
    for (; i < 256; i++)
    {
        unsigned short k = i << 8;
        unsigned short crc = 0;
        unsigned short j = 0;
        for (; j < 8; j++)
        {
            if ((crc ^ k) & 0x8000)
            {
                crc = (crc << 1) ^ CRC16_POLYNOMIAL;
            }
            else
            {
                crc <<= 1;
            }
            k <<= 1;
        }
        crc16_table[i] = crc;
    }
}

void
mhash_clear_crc16(mutils_word16* crc)
{
    if(crc16_table[1] == 0)
    {
        mhash_crc16_init();
    }

    *crc = 0xffff;
}

void
mhash_get_crc16(__const mutils_word16* crc, void* ret)
{
    if (ret != NULL)
    {
        mutils_word16 tmp = ~(*crc);
        /*
         * transmit complement, per CRC-16 spec
         */
#if defined(WORDS_BIGENDIAN)
        tmp = mutils_swapendian16(tmp);
#endif
        mutils_memcpy(ret, &tmp, sizeof(mutils_word16));
    }
}

void
mhash_get_crc16b(__const mutils_word16* crc, void* ret)
{
    if (ret != NULL)
    {
        mutils_word16 tmp = ~(*crc);
        /*
         * transmit complement, per CRC-16 spec
         */
#if !defined(WORDS_BIGENDIAN)
        tmp = mutils_swapendian16(tmp);
#endif
        mutils_memcpy(ret, &tmp, sizeof(mutils_word16));
    }
}

void
mhash_crc16(mutils_word16* crc, __const void* given_buf, mutils_word32 len)
{
    if ((crc != NULL) && (given_buf != NULL) && (len != 0))
    {
        __const mutils_word8* p = given_buf;
        for (; len > 0; ++p, --len)
        {
            (*crc) = ((*crc) << 8) ^ crc16_table[(unsigned char)(((*crc) >> 8) ^ *p)];
        }
    }
}

#endif /* ENABLE_CRC16 */

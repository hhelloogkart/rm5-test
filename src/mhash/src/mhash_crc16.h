#if defined(ENABLE_CRC16)

#if !defined(__MHASH_CRC16_H)
#define __MHASH_CRC16_H

#include "libdefs.h"

void mhash_clear_crc16(mutils_word16* crc);
void mhash_get_crc16( __const mutils_word16* crc, void* ret);
void mhash_get_crc16b( __const mutils_word16* crc, void* ret);
void mhash_crc16(mutils_word16* crc, __const void* given_buf, mutils_word32 len);

#endif

#endif

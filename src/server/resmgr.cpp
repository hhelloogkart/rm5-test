/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#if defined(_WIN32)
# include <winsock2.h>
# include <windows.h>
# include <process.h>
extern "C" void cleanupThread(void);
#elif defined(VXWORKS)
# include <taskLib.h>
#else
# include <unistd.h>     /* for fork */
# include <stdlib.h>     /* for exit */
# include <sys/wait.h>   /* for wait */
#endif

#include <signal.h>     /* for signal, sigaction */

#include <rmstl.h>
#include <rm/rmname.h>
#include <defaults.h>
#include <rm/rmsocket.h>
#include <rm/rmtcp.h>
#include <rm/rmsm.h>
#include <rm/storage.h>
#include <util/dbgout.h>
#include <comm/tcpcore.h>

#include "resmgr.h"
#include "roamsvr.h"


/*!
   \page pageConfigFile RM configuration file format
   This page contains the RM server config file template

   \verbatim
   server
   {
    port =
    console     [optional, default no console output, add to enable output]
    udpport =   [optional UDP port, default no udp]
    udpaddr =   [optional UDP address, default no udp]
    log =       [optional lof file name , default no log]
    timeout_sec =  [optional number of sec to wait on select, default 0]
    timeout_usec = [optional number of usec to wait on select, default 50000]
   }
   client
   {
    name =
    read =
    write =
    limit = (default no limit)
   }
   ...
   storage
   {
    name =
   }
   ...
   \endverbatim
 */

extern "C"
{

#if !defined(VXWORKS)
//! Hangup or interrput signal handler
void sigintrhandler(int);

#if !defined(_WIN32) || defined(__CYGWIN__)
//! Child stopped or terminated signal handler
void sigpipehandler(int);
#else
unsigned __stdcall ThreadTcpClient(LPVOID lpvThreadParam);
#endif
#endif

}

static void set_name(char* tgt, char ch)
{
    char str[128];
    rm_sprintf(str, RMNAME_LEN, "%c", ch);
    rm_strcat(tgt, RMNAME_LEN, str);
}

static void set_name(char* tgt, char ch, int idx)
{
    char str[128];
    str[0] = ch;
    rm_sprintf(str + 1, RMNAME_LEN - 1, "%02d", idx);
    rm_strcat(tgt, RMNAME_LEN, str);
}

volatile unsigned Resmgr::flag_exit = 0U;
volatile unsigned Resmgr::flag_sigpipe = 0U;


Resmgr::Resmgr(const char* filename) :
    configfile(filename), port(RMPORT),
    debugconsole(false),
    debugport(0), debugaddr(), debuglog(),
    storageidx(-1),
    stgname(RMSTGNAME),
    select_timeout_sec(0),
    select_timeout_usec(50000)
{
}

Resmgr::~Resmgr()
{
    client_setting_list_typ::iterator citer;

    for(citer = clients.begin(); citer != clients.end(); ++citer)
    {
        delete(*citer);
    }
}

/*!
   \param argc argument count from main function
   \param argv argument string from main function

   RM command line arguments:\n
    -c <config file>    : specify the configuration to use\n
    -p <port no>        : specify the port number to use\n
    -d <debug port>     : specify the debug port\n
    -l <log file>       : specify the log file\n
    -h                  : display this help and exit\n
    --\n
 */
bool Resmgr::ParseArguments(int argc, char** argv)
{
    int i;
    char flag, * val;
    string arg_debuglog;
    int arg_debugport = -1;
    int arg_port = -1;

    bool ret = ParseDefaults();
    for(i = 1; i < argc; ++i)
    {
        if(argv[i][0] == '-')
        {
            flag = argv[i][1];
            val = (argv[i][2] != '\0')? &argv[i][2] : (++i < argc)? argv[i] : NULL;

            switch(flag)
            {
            case 'c':
                if(!val)
                {
                    PrintUsage(argv[0]);
                    return false;
                }
                configfile = (const char*)val;
                ret = ParseDefaults();
                break;

            case 'p':
                if(!val)
                {
                    PrintUsage(argv[0]);
                    return false;
                }
                arg_port = atoi(val);
                break;

            case 'd':
                if(!val)
                {
                    PrintUsage(argv[0]);
                    return false;
                }
                arg_debugport = atoi(val);
                break;

            case 'l':
                if(!val)
                {
                    PrintUsage(argv[0]);
                    return false;
                }
                arg_debuglog = val;
                break;

            case 'h':
            default:
                PrintUsage(argv[0]);
                return false;
            }
        }
        else
        {
            def.parse_cmdline(2, &argv[i-1]);
        }
    }


    tree_node* t, * t1, * t2;
    char str[41], rmname[41];
    int ivalue;
    re_typ* re;
    client_setting_typ* cli;

    //read server\... key/value pair
    if((t = def.getstring("server")) != NULL)
    {
        //if key not found, use initialized default
        t1 = def.getChild(t);

        if(def.getint("port", &ivalue, t1))
        {
            port = ivalue;
        }
        if(def.getint("udpport", &ivalue, t1))
        {
            debugport = ivalue;
        }
        if(def.getstring("udpaddr", str, t1))
        {
            debugaddr = str;
        }
        if(def.getstring("log", str, t1))
        {
            debuglog = str;
        }

        if(debuglog.size() > 0)
        {
            dout.openfile(debuglog.c_str());
            dout << "RM log file: " << debuglog << endl << flush;
        }

        // disable console output if server\console keyword not present
        if(!def.getnode("console", t1))
        {
            dout.useconsole(false);
        }

        // read timeout values
        def.getint("timeout_sec", &select_timeout_sec, t1);
        def.getint("timeout_usec", &select_timeout_usec, t1);
    }

    //read client\... key/value pair
    for(t = def.getstring("client");
        t != NULL;
        t = def.getstring("client", NULL, t))
    {
        t1 = def.getChild(t);

        if(def.getstring("name", rmname, t1) == NULL)
        { //skip this client
            dout << "ERROR: Invalid client with no name" << endl << flush;
            continue;
        }
        else
        {
            cli = new client_setting_typ;
            cli->name = rmname;

            cli->limit = (def.getint("limit", &ivalue, t1))? ivalue : 0;

            for(t2 = def.getstring("read", str, t1);
                t2 != NULL;
                t2 = def.getstring("read", str, t2))
            {
                re = new re_typ;
                re->re = (const char*)str;
                cli->read.push_back(re);
            }

            for(t2 = def.getstring("write", str, t1);
                t2 != NULL;
                t2 = def.getstring("write", str, t2))
            {
                re = new re_typ;
                re->re = (const char*)str;
                cli->write.push_back(re);
            }

            clients.push_back(cli);
        }
    }

    //read storage\... key/value pair
    for(t = def.getstring("storage");
        t != NULL;
        t = def.getstring("storage", NULL, t))
    {
        char arg_access[64];
        t1 = def.getChild(t);

        if(def.getstring("name", rmname, t1) == NULL)
        { //skip this storage
            dout << "ERROR: Invalid storage with no name" << endl << flush;
            continue;
        }
        if(def.getstring("type", arg_access, t1) == NULL)
        {
            access = "SM";
        }
        else
        {
            access = arg_access;
        }

        def.getint("index", &storageidx);

        dout << "SM storage " << rmname << " using " << access << endl;

        stgname = rmname;
#if defined(WIN32) && !defined(NDEBUG)
        if (access == "SM")
        {
            stgname += "D";
        }
#endif
    }

    if(clients.size() == 0)
    {
        dout << "ERROR: No client defined" << endl;
        return false;
    }

    if (arg_port != -1)
    {
        port = arg_port;
    }
    if (arg_debugport != -1)
    {
        debugport = arg_debugport;
    }
    if (!arg_debuglog.empty())
    {
        debuglog = arg_debuglog;
    }

    dout << "RM port " << port << endl << flush;

    if(debugport > 1024 && debugaddr.size() >= 7) //min len is len(0.0.0.0)
    {
        dout.openudp(debugaddr.c_str(), debugport);
        dout << "RM UDP debug: " << debugport << " " << debugaddr << endl << flush;
    }

    return ret;
}

void Resmgr::PrintUsage(char* progname)
{
    dout << "Usage: " << progname << " [-c <config file>] [options] [KEY=VAL]\n";
    dout << "Options includes:\n";
    dout << "\t-c <config file>    : specify the configuration file to use\n";
    dout << "\t                      default ";
    dout << RMCFGFILE;
    dout << "\n";
    dout << "\t-p <port no>        : specify the port number to use\n";
    dout << "\t-d <debug port>     : specify the debug port number\n";
    dout << "\t-l <log file>       : specify the debug log file name\n";
    dout << "\t-h                  : display this help and exit\n";
}

bool Resmgr::ParseDefaults()
{
    if(configfile == NULL)
    {
        configfile = RMCFGFILE;
    }

    file_buffer fb("");
    if(!fb.setfile(configfile))
    {
        dout << "ERROR: failed to open config file" << endl << flush;
        return false;
    }

    def.load(fb);

    return true;
}

bool Resmgr::CompileDefaults()
{
    client_setting_list_typ::iterator citer;
    re_list_typ::iterator reiter;
    re_typ* re;
    const char* errptr;
    int erroff;

    //compile the regular expression for each clients
    for(citer = clients.begin(); citer != clients.end(); ++citer)
    {
        for(reiter = (*citer)->read.begin();
            reiter != (*citer)->read.end();
            ++reiter)
        {
            re = (*reiter);
            re->cre = pcre_compile(re->re.c_str(), 0, &errptr, &erroff, NULL);
            if(re->cre == NULL)
            {
                dout << "ERROR: Error compiling read rule " << (char*)re->re.c_str() << endl << flush;
                return false;
            }
        }

        for(reiter = (*citer)->write.begin();
            reiter != (*citer)->write.end();
            ++reiter)
        {
            re = (*reiter);
            re->cre = pcre_compile(re->re.c_str(), 0, &errptr, &erroff, NULL);
            if(re->cre == NULL)
            {
                dout << "ERROR: Error compiling write rule " << (char*)re->re.c_str() << endl;
                return false;
            }
        }
    }

    return true;
}

client_setting_typ* Resmgr::FindClientSetting(rmname_typ& client_name)
{
    return ::FindClientSetting(&clients, client_name);
}

bool Resmgr::WaitConnection()
{
    tcp_core lsock, asock;
    process_typ pid;
    pid_list_typ::iterator pid_iter;
    struct timeval tv_def = {
        0, 500000
    };
    struct timeval tv_roam = {
        0, 500000
    };

#if !defined(VXWORKS)
#if defined(_WIN32) && !defined(__CYGWIN__)
    signal(SIGINT, sigintrhandler);
    signal(SIGTERM, sigintrhandler);
    signal(SIGABRT, sigintrhandler); //when call abort
#else
    struct sigaction act;
    sigemptyset(&act.sa_mask);
#if defined(__QNXNTO__)
    act.sa_flags = 0;
#else
    act.sa_flags = SA_RESTART;     //restart system call
#endif

    act.sa_handler = sigintrhandler;
    sigaction(SIGINT, &act, NULL);
    sigaction(SIGTERM, &act, NULL);
    sigaction(SIGABRT, &act, NULL);
    sigaction(SIGQUIT, &act, NULL);

    act.sa_handler = sigpipehandler;
    sigaction(SIGPIPE, &act, NULL);
#endif
#endif

    //create a listening socket
    lsock.sethost("", port);
    if(lsock.listen(32) == -1)
    {
        dout << "ERROR: Cannot listen at port " << port << endl << flush;
        return false;
    }

    //create a roaming socket
    RMRoamSvr rsock(port);

    while(!(flag_exit && client_pids.empty()))
    {
        //wait for connection
        rsock.Select(&tv_roam);
        if(!flag_exit && lsock.select(&tv_def))
        {
            asock = (lsock.accept());

            if(asock.connected())
            {
#if defined(_WIN32) && !defined(__CYGWIN__)
                tcp_core* data = new tcp_core(asock);
                if(data)
                {
                    pid = _beginthreadex(
                        0, // security
                        0, // initial thread stack size, in bytes
                        &ThreadTcpClient, // pointer to thread function
                        (LPVOID)data, // argument for new thread
                        0, // init
                        0); // thaddr
                    if(pid == 0)
                    {
                        delete data;
                        asock.close();
                        dout << "ERROR: Unable to fork process to handle client" << endl << flush;
                    }
                    else
                    {
                        //save pid into pid list
                        client_pids.push_back(pid);
                    }
                }
                else
                {
                    asock.close();
                    dout << "ERROR: Unable to allocate memory for thread data" << endl << flush;
                }
#else
                //upon connection, fork
                pid = fork();
                if(pid == 0)
                { //child
                    flag_exit = flag_sigpipe = 0;
                    lsock.close();
                    rsock.Close();
                    exit(RunTcpClient(asock));
                }
                else if(pid > 0)
                { //parent
                    asock.close();
                    //save pid into pid list
                    client_pids.push_back(pid);
                }
                else
                {
                    asock.close();
                    dout << "ERROR: Unable to fork process to handle client" << endl << flush;
                }
#endif
            }
            else
            {
                dout << "ERROR: Accept failed" << endl << flush;
            }
        }

#if !defined(_WIN32) || defined(__CYGWIN__)
        if(flag_sigpipe)
        {
            lsock.close();
            if(lsock.listen(32) == -1)
            {
                dout << "ERROR: Cannot listen at port " << port << endl << flush;
                return false;
            }
            flag_sigpipe = 0;
        }
#endif

#if !defined(_WIN32) || defined(__CYGWIN__)
        //Check for child exit
        for(pid_iter = client_pids.begin();
            pid_iter != client_pids.end(); )
        {
            pid = (*pid_iter);


            int pstatus;
            if(waitpid(pid, &pstatus, WNOHANG | WUNTRACED) == pid)
            {
                if(!WIFSTOPPED(pstatus))
                {
                    pid_iter = client_pids.erase(pid_iter);
                    continue;
                }
            }
            ++pid_iter;
        }
#else
        //Check for child exit
        for (pid_iter = client_pids.begin();
             pid_iter != client_pids.end(); )
        {
            pid = (*pid_iter);
            if (WaitForSingleObject((HANDLE)pid, 0) == WAIT_OBJECT_0)
            {
                CloseHandle((HANDLE)pid);
                pid_iter = client_pids.erase(pid_iter);
            }
            else
            {
                ++pid_iter;
            }
        }
#endif
#ifdef _WIN32
        MSG msg;
        if (PeekMessage(&msg, 0, WM_CLOSE, WM_CLOSE, PM_REMOVE))
        {
            flag_exit = true;
        }
#endif
    } //while(!flag_exit)

    lsock.close();
    dout << "Resmgr Exiting..." << endl << flush;
    return true;
}

/*!
   Run will parse the configuration file, create the storage and
   wait for client connection
 */
int Resmgr::Run()
{
    //load configuration file
    if(!CompileDefaults())
    {
        return (-1);
    }

    //create a listening socket and wait for connection
    if(!WaitConnection())
    {
        return (-1);
    }

    if (def.getnode("storage/cleanup") && access == "SM")
    {
        const char* storage = (const char*)stgname;
        char shmname[RMNAME_LEN];
        rm_strcpy(shmname, RMNAME_LEN, storage);
        set_name(shmname, 'C');
        mm_core_create_delete(CACHE_SIZE, shmname);

        unsigned int i;
        for (i = 0; i < MAX_CLIENT; ++i)
        {
            rm_strcpy(shmname, RMNAME_LEN, storage);
            set_name(shmname, 'Q', i);
            mm_core_create_delete(QUEUE_SIZE, shmname);
        }

        rm_strcpy(shmname, RMNAME_LEN, storage);
        set_name(shmname, 'V');
        mm_core_create_delete(VAR_SIZE, shmname);

        mm_core_create_delete(sizeof(rmglobal_private_typ), storage);
    }

    return 0;
}

int Resmgr::RunTcpClient(tcp_core& asock)
{
    RMTCPServerSocket* svrsock;
    RMBaseSocket* smsock;
    const char* host = asock.gethost();
    int ret = 0;

    smsock = RMBaseSocket::Construct(access.c_str());
    svrsock = new RMTCPServerSocket(smsock, clients, asock);
    dout << "CONNECT: " << host << endl << flush;

    if(smsock && svrsock)
    {
        char svrhost[256];
        if(storageidx!=-1)
        {
            rm_sprintf3(svrhost, 256, "%s,%d %d", (const char*)stgname, storageidx, storageidx);
            ++storageidx;
        }
        else
        {
            rm_sprintf(svrhost, 256, "%s", (const char*)stgname);
        }

        if(smsock->Connect(svrhost, 0))
        {
            while(!flag_exit)
            {
                struct timeval tv = {
                    select_timeout_sec, select_timeout_usec
                };
                //check for data from socket and shared memory
                int result = svrsock->Select(&tv);

                //exit on disconnection
#if defined(_WIN32) && !defined(__CYGWIN__)
                if((result < 0) || !svrsock->Connected())
#else
                if((result < 0) || !svrsock->Connected() || flag_sigpipe)
#endif
                {
                    svrsock->Close();
                    dout << "DISCONNECT: " << host << endl;
                    ret = -1;
                    break;
                }
            }

            //close the socket connection
            svrsock->Close();
        }
        else
        {
            dout << "ERROR: Unable to connect to " << access << endl;
        }
    }
    else
    {
        dout << "Unable allocate memory for client data" << endl;
    }

    delete svrsock;
    delete smsock;
#ifdef _WIN32
    cleanupThread();
#endif
    return ret;
}

#if !defined(VXWORKS)
void sigintrhandler(int)
{
    Resmgr::flag_exit = 1U;
}

#if !defined(_WIN32) || defined(__CYGWIN__)
void sigpipehandler(int)
{
    Resmgr::flag_sigpipe = 1U;
}
#endif
#endif

Resmgr rmsvr;

#if defined(_WIN32) && !defined(__CYGWIN__)
unsigned __stdcall ThreadTcpClient(LPVOID lpvThreadParam)
{
    if(lpvThreadParam)
    {
        tcp_core* asock = (tcp_core*)lpvThreadParam;
        rmsvr.RunTcpClient(*asock);
        delete asock;
    }

    return 0;
}
#endif

int main(int argc, char** argv)
{
    dout.useconsole(true);

    //load configuration file
    if(!rmsvr.ParseArguments(argc, argv))
    {
        return -1;
    }

    //run resmgr program
    return rmsvr.Run();
}


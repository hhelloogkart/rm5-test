#include <rm/rmtcp.h>
#include <rm/rmsm.h>

#include <server/resmgr.h>
#include <server/svrtcp.h>


// Callbacks for Client socket to storage (shared memory/TCP)
// ================================================

// SetList
void sock_store_setlist_cb(void* arg, unsigned int id, const cache_typ* buf)
{
    RMServerSocket* sock = reinterpret_cast<RMServerSocket*>(arg);
    sock->ClientSetList(id, buf);
}

// Broadcast
void sock_store_broadcast_cb(void* arg, unsigned int id, const cache_typ* buf)
{
    RMServerSocket* sock = reinterpret_cast<RMServerSocket*>(arg);
    sock->ClientBroadcast(id, buf);
}

// GetList
void sock_store_getlist_cb(void* arg, unsigned int id, const cache_typ*)
{
    RMServerSocket* sock = reinterpret_cast<RMServerSocket*>(arg);
    sock->ClientGetList(id);
}

// QueryID
void sock_store_queryid_cb(void* arg, unsigned int, const cache_typ* buf)
{
    RMServerSocket* sock = reinterpret_cast<RMServerSocket*>(arg);
    sock->ClientQueryID(buf);
}

// ClientID
void sock_store_clientid_cb(void* arg, unsigned int, const cache_typ* buf)
{
    RMServerSocket* sock = reinterpret_cast<RMServerSocket*>(arg);
    sock->ClientClientID(buf);
}

// RegisterID
void sock_store_registerid_cb(void* arg, unsigned int, const cache_typ* buf)
{
    RMServerSocket* sock = reinterpret_cast<RMServerSocket*>(arg);
    sock->ClientRegisterID(buf);
}

// RegisterIDX
void sock_store_registeridx_cb(void* arg, unsigned int id, const cache_typ*)
{
    RMServerSocket* sock = reinterpret_cast<RMServerSocket*>(arg);
    sock->ClientRegisterIDX(id);
}

// UnregisterIDX
void sock_store_unregisteridx_cb(void* arg, unsigned int id, const cache_typ*)
{
    RMServerSocket* sock = reinterpret_cast<RMServerSocket*>(arg);
    sock->ClientUnRegisterIDX(id);
}


// Client socket, handled by application
// =====================================

// FlowControl
void sock_flow_cb(void* arg, unsigned int id, const cache_typ*)
{
    //call the client socket's method for flow control
    RMBaseSocket* sock = reinterpret_cast<RMBaseSocket*>(arg);
    sock->FlowControl(id);
}


// Storage to client socket
// ========================

// SetList
void store_sock_setlist_cb(void* arg, unsigned int id, const cache_typ* buf)
{
    //call setlist of client socket to return packet
    RMBaseSocket* sock = reinterpret_cast<RMBaseSocket*>(arg);
    sock->SetList(id, buf->get_len(), buf->buf());
}

// Broadcast
void store_sock_broadcast_cb(void* arg, unsigned int id, const cache_typ* buf)
{
    //call broadcast of client socket to return packet
    RMBaseSocket* sock = reinterpret_cast<RMBaseSocket*>(arg);
    sock->Broadcast(id, buf->get_len(), buf->buf());
}

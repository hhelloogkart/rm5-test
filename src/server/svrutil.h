/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef __SVRUTIL_H_DEF__
#define __SVRUTIL_H_DEF__

#if defined(_WIN32)
#include <winsock2.h>
#include <windows.h>
#endif

#include <rmstl.h>
#include <rm/rmname.h>
#include <rm/rmsocket.h>

#include <pcre/pcre.h>

//Forward declaration

struct re_typ;
struct client_setting_typ;
struct variable_typ;

//Type definition

#if defined(_WIN32) && !defined(__CYGWIN__)
typedef uintptr_t process_typ;
#else
typedef int process_typ;
#endif

typedef VECTOR(re_typ*) re_list_typ;
typedef VECTOR(client_setting_typ*) client_setting_list_typ;
typedef LIST(process_typ) pid_list_typ;
typedef VECTOR(bool) bool_list_typ;
typedef VECTOR(variable_typ) variable_list_typ;
typedef SET(int, less<int>) iset_typ;

//! regular expression
struct re_typ
{
    pcre* cre;          //! PCRE compiled regular expression
    string re;          //! the RE as an unprocessed string

    re_typ();
    ~re_typ();
};

//! client permissions
struct client_setting_typ
{
    rmname_typ name;    //! client name
    re_list_typ read;
    re_list_typ write;
    unsigned limit;     //! Max no of client connections

    client_setting_typ();
    ~client_setting_typ();

    bool readable(rmname_typ& var);
    bool writable(rmname_typ& var);
};

struct variable_typ
{
    rmname_typ name;
    bool write, read, reg;

    variable_typ();
};

extern client_setting_typ* FindClientSetting(client_setting_list_typ*, rmname_typ&);

#endif /* __SVRUTIL_H_DEF__ */

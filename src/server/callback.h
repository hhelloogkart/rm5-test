#ifndef _CALLBACK_H_
#define _CALLBACK_H_

#include <rm/rmsocket.h>

// Client socket to storage (shared memory/TCP)
// ================================================

// Broadcast
extern void sock_store_broadcast_cb(void* arg, unsigned int id, const cache_typ* buf);

// SetList
extern void sock_store_setlist_cb(void* arg, unsigned int id, const cache_typ* buf);

// GetList
extern void sock_store_getlist_cb(void* arg, unsigned int id, const cache_typ* buf);

// QueryID
extern void sock_store_queryid_cb(void* arg, unsigned int id, const cache_typ* buf);

// ClientID
extern void sock_store_clientid_cb(void* arg, unsigned int id, const cache_typ* buf);

// RegisterID
extern void sock_store_registerid_cb(void* arg, unsigned int id, const cache_typ* buf);

// RegisterIDX
extern void sock_store_registeridx_cb(void* arg, unsigned int id, const cache_typ* buf);

// UnregisterIDX
extern void sock_store_unregisteridx_cb(void* arg, unsigned int id, const cache_typ* buf);


// Client socket, handled by application
// =====================================

// FlowControl
extern void sock_flow_cb(void* arg, unsigned int id, const cache_typ* buf);

// Storage to client socket
// ========================

// SetList
extern void store_sock_setlist_cb(void* arg, unsigned int id, const cache_typ* buf);

// Broadcast
extern void store_sock_broadcast_cb(void* arg, unsigned int id, const cache_typ* buf);

#endif // _CALLBACK_H_

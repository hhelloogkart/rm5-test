/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include <server/svrtcp.h>
#include <server/resmgr.h>
#include <server/callback.h>

#include <util/dbgout.h>

// Constructor
RMTCPServerSocket::RMTCPServerSocket(
    RMBaseSocket* stg, client_setting_list_typ& s, tcp_core& sock) :
    RMServerSocket(stg, NULL, s), tcpsock(sock)
{
    SetClientSocket(&tcpsock);
}

// Destructor
RMTCPServerSocket::~RMTCPServerSocket()
{
}

RMTCPSocket& RMTCPServerSocket::GetSocket()
{
    return tcpsock;
}

int RMTCPServerSocket::SendMessage(unsigned int id, size_t len, const unsigned int* buf, int cmd)
{
    return tcpsock.SendMessageT(id, len, buf, cmd);
}

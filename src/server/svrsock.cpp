/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include <server/svrsock.h>
#include <server/callback.h>

#include <rmstl.h>
#include <util/dbgout.h>
#include <util/utilcore.h>
#include <cache.h>

RMServerSocket::RMServerSocket(
    RMBaseSocket* stg, RMBaseSocket* cli, client_setting_list_typ& s) :
    flag_exit(stg ? false : true),
    stgsock(stg), clisock(cli), settings(s), cursetting(NULL)
{
    if(cli)
    {
        SetClientSocket(cli);
    }
}

void RMServerSocket::SetClientSocket(RMBaseSocket* cli)
{
    clisock = cli;

    //attach callbacks
    clisock->AddCallback(BROADCAST, (void*)this, sock_store_broadcast_cb);
    clisock->AddCallback(SETLIST, (void*)this, sock_store_setlist_cb);
    clisock->AddCallback(GETLIST, (void*)this, sock_store_getlist_cb);
    clisock->AddCallback(QUERYID, (void*)this, sock_store_queryid_cb);
    clisock->AddCallback(CLIENTID, (void*)this, sock_store_clientid_cb);
    clisock->AddCallback(REGISTERID, (void*)this, sock_store_registerid_cb);
    clisock->AddCallback(REGISTERIDX, (void*)this, sock_store_registeridx_cb);
    clisock->AddCallback(UNREGISTERIDX, (void*)this, sock_store_unregisteridx_cb);

    clisock->AddCallback(FLOWCONTROL, (void*)clisock, sock_flow_cb);

    stgsock->AddCallback(SETLIST, (void*)clisock, store_sock_setlist_cb);
    stgsock->AddCallback(BROADCAST, (void*)clisock, store_sock_broadcast_cb);
}

RMServerSocket::~RMServerSocket()
{
}

// Connection and status
bool RMServerSocket::Connected()
{
    if(flag_exit)
    {
        clisock->Close();
        return false;
    }
    return clisock->Connected() ? stgsock->Connected() : false;
}

int RMServerSocket::Select(struct timeval* stm, bool defercb)
{
    if(flag_exit)
    {
        clisock->Close();
        return 0;
    }
    int ret = clisock->Select(stm, defercb);
    struct timeval zerotv = {
        0, 0
    };
    stgsock->Select(&zerotv, defercb);
    return ret;
}

void RMServerSocket::Close()
{
    clisock->Close();
    stgsock->Close();
}

client_setting_typ* RMServerSocket::FindClientSetting(rmname_typ& name)
{
    return ::FindClientSetting(&settings, name);
}

// Callbacks

void RMServerSocket::ClientBroadcast(unsigned int id, const cache_typ* buf)
{
    //check for write permission and idx not exceed variable list
    if(!flag_exit && cursetting && (id < varlist.size()) &&
       (varlist[id].write))
    {
        //call broadcast method of storage
        stgsock->Broadcast(id, buf->get_len(), buf->buf());
    }
}

void RMServerSocket::ClientSetList(unsigned int id, const cache_typ* buf)
{
    //check for write permission and idx not exceed variable list
    if(!flag_exit && cursetting && (id < varlist.size()) &&
       (varlist[id].write))
    {
        //call setlist method of storage
        stgsock->SetList(id, buf->get_len(), buf->buf());
    }
    else
    {
        dout << "ERROR: clientid not set or invalid id or var not writable" << endl << flush;
    }
}

void RMServerSocket::ClientGetList(unsigned int id)
{
    //check for read permission and idx not exceed variable list
    if(!flag_exit && cursetting && (id < varlist.size()) &&
       (varlist[id].read))
    {
        //call getlist method of storage
        const cache_typ* ptr = stgsock->GetListStart(id);

        if(ptr == NULL)
        {
            //if ptr == NULL, return length 0 by calling client's setlist method
            //clisock->SendMessage(id, 0 , NULL, SETLIST);
            SendMessage(id, 0, NULL, SETLIST);
        }
        else
        {
            //else return the packet by calling client's setlist method
            clisock->SetList(id, ptr->get_len(), ptr->buf());
            stgsock->GetListEnd(id, ptr);
        }
    }
    else
    {
        dout << "ERROR: clientid not set or invalid id or var not readable" << endl << flush;
    }
}

bool RMServerSocket::ClientClientID(const cache_typ* buf)
{
    unsigned int id = 0, hid;

    if(!flag_exit && (buf->get_len() > 0))
    {
        const char* name = reinterpret_cast<const char*>(buf->buf());
        rmname_typ client_name(name);

        //clear the variable list on clientid command
        if(cursetting)
        {
            varlist.clear();
        }

        //check for valid client name in defaults
        cursetting = FindClientSetting(client_name);

        if(cursetting != NULL)
        {
            variable_typ var;

            var.read = true;
            var.write = true;
            var.reg = false;
            var.name = client_name;

            //add client name variable to permission list as index 0
            varlist.push_back(var);

            //call ClientID of storage
            stgsock->ClientID(name);

            //call GetList(0) to get the number of clients.
            stgsock->GetList(0, 1, &id);

            //convert big endian id to host endian
            hid = BE(id);

            dout << "CLIENT: " << (char*)name << " no " << hid << endl << flush;

            //check with limits to ensure limits not exceeded
            if((!cursetting->limit) || (hid <= cursetting->limit))
            {
                //clisock->SendMessage(id, 0, NULL, CLIENTID);
                SendMessage(id, 0, NULL, CLIENTID);
                return true;
            }
            else
            {
                dout << "ERROR: Client " << (char*)name << " connection exceed limit " << cursetting->limit << endl << flush;
            }
        }
        else
        {
            dout << "ERROR: Client " << (char*)name << " connection disallowed" << endl << flush;
        }
    }

    //clisock->SendMessage(0xFFFFFF, 0, NULL, CLIENTID);
    SendMessage(0xFFFFFF, 0, NULL, CLIENTID);
    flag_exit = true; //flag to close this connection
    return false;
}

void RMServerSocket::ClientQueryID(const cache_typ* buf)
{
    if(buf->get_len() <= 0)
    {
        dout << "ERROR: received query with len 0" << endl << flush;
    }
    else if(!cursetting)
    {
        dout << "ERROR: clientid not set" << endl << flush;
    }
    else if(!flag_exit) //if((buf->get_len() > 0) && client_cur)
    {
        const char* name = reinterpret_cast<const char*>(buf->buf());
        rmname_typ var_name(name);
        variable_typ var;

        //apply the client's regexp for read and write
        var.read = cursetting->readable(var_name);
        var.write = cursetting->writable(var_name);
        var.reg = false;
        var.name = var_name;

        //add this variable to permission list
        varlist.push_back(var);

        //call queryid of storage
        stgsock->QueryID(name);

        //clisock->SendMessage(varlist.size() - 1, 0, NULL, QUERYID);
        SendMessage(varlist.size() - 1, 0, NULL, QUERYID);
    }
}

void RMServerSocket::ClientRegisterID(const cache_typ* buf)
{
    if(buf->get_len() <= 0)
    {
        dout << "ERROR: received register id with len 0" << endl << flush;
    }
    else if(!cursetting)
    {
        dout << "ERROR: clientid not set" << endl << flush;
    }
    else if(!flag_exit) //if((buf->get_len() > 0) && client_cur)
    {
        const char* name = reinterpret_cast<const char*>(buf->buf());
        rmname_typ var_name(name);
        variable_typ var;

        //apply the client's regexp for read and write
        var.read = cursetting->readable(var_name);
        var.write = cursetting->writable(var_name);
        var.reg = true;
        var.name = var_name;

        //add this variable to permission list
        varlist.push_back(var);

        //check for read permission
        if(var.read)
        {
            //call registerid of storage
            stgsock->RegisterID(name);
            //clisock->SendMessage(varlist.size() - 1, 0, NULL, REGISTERID);
            SendMessage(varlist.size() - 1, 0, NULL, REGISTERID);
        }
        else
        {
            //call queryid of storage
            stgsock->QueryID(name);
            //clisock->SendMessage(varlist.size() - 1, 0, NULL, QUERYID);
            SendMessage(varlist.size() - 1, 0, NULL, QUERYID);
            dout << "WARNING: registerid " << (char*)name << " not readable" <<
                " id=" << (varlist.size() - 1) << endl << flush;
        }
    }
}

void RMServerSocket::ClientRegisterIDX(unsigned int id)
{
    //check for read permission
    if(!flag_exit && cursetting && (id < varlist.size()) &&
       (varlist[id].read))
    {
        //call registeridx of storage
        stgsock->RegisterIDX(id);
    }
    else
    {
        dout << "ERROR: received regidx idx " << id << " invalid" << endl << flush;
    }
}

void RMServerSocket::ClientUnRegisterIDX(unsigned int id)
{
    //check for read permission
    if(!flag_exit && cursetting && (id < varlist.size()) &&
       (varlist[id].read))
    {
        //call unregisteridx of storage
        stgsock->UnregisterIDX(id);
    }
    else
    {
        dout << "ERROR: received unregidx idx " << id << " invalid" << endl << flush;
    }
}


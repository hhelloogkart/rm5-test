/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef __SVRTCP_H_DEF__
#define __SVRTCP_H_DEF__

#include <server/svrsock.h>
#include <rm/rmtcp.h>

class RMTCPServerSocket : public RMServerSocket
{
public:
    RMTCPServerSocket(RMBaseSocket* stg, client_setting_list_typ& s, tcp_core& sock);
    virtual ~RMTCPServerSocket();

    RMTCPSocket& GetSocket();

    virtual int SendMessage(unsigned int id, size_t len, const unsigned int* buf, int cmd);

protected:
    RMTCPSocket tcpsock;                //RMTCPSocket for client connection
};

#endif /* __SVRTCP_H_DEF__ */

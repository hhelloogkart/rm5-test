/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "roamsvr.h"
#include <util/dbgout.h>
#include <util/utilcore.h>
#include <rmstl.h>

#if defined(_WIN32)
#include <winsock2.h>
#elif defined(NET_OS)
extern "C"
{
#include <sockapi.h>
}
#else
#include <netinet/in.h>
#endif

RMRoamSvr::RMRoamSvr(unsigned short _rmport) :
    rmport(_rmport)
{
    sock.bind("0.0.0.0", _rmport);
    dout << "Roaming on port " << _rmport << endl;
}

RMRoamSvr::~RMRoamSvr()
{
    Close();
}

void RMRoamSvr::Close()
{
    sock.close();
}

bool RMRoamSvr::Select(const struct timeval* stm)
{
    int result = sock.select(stm);
    if(result > 0)
    {
        OnDataAvail();
        return true;
    }
    if(result < 0)
    {
        sock.close();
        sock.bind(NULL, rmport);
    }
    return false;
}

void RMRoamSvr::OnDataAvail()
{
    short recvport;
    struct sockaddr_in clientaddr;

    if(sock.recv((char*)&recvport, sizeof(short), &clientaddr)
       == sizeof(short))
    {
        clientaddr.sin_port = recvport;
        dout << "Receive roam client request on port " << rmport << endl;
        recvport = BE((unsigned short)rmport);
        sock.send((char*)&recvport, sizeof(short), &clientaddr);
    }
}

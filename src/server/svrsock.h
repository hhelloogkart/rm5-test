/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef __SVRSOCK_H_DEF__
#define __SVRSOCK_H_DEF__

#include <rm/rmsocket.h>
#include <server/svrutil.h>

class RMServerSocket
{
public:
    RMServerSocket(RMBaseSocket* stg, RMBaseSocket* cli, client_setting_list_typ& s);
    virtual ~RMServerSocket();

    virtual void SetClientSocket(RMBaseSocket* cli);

    // Connection and status
    virtual bool Connected();
    virtual int  Select(struct timeval* stm = 0, bool defercb = false);
    virtual void Close();

    // Callbacks for clients

    //! Process client BROADCAST request
    virtual void ClientBroadcast(unsigned int id, const cache_typ* buf);

    //! Process client SETLIST request
    virtual void ClientSetList(unsigned int id, const cache_typ* buf);

    //! Process client GETLIST request
    virtual void ClientGetList(unsigned int id);

    //! Process client CLIENTID request
    virtual bool ClientClientID(const cache_typ* buf);

    //! Process client QUERYID request
    virtual void ClientQueryID(const cache_typ* buf);

    //! Process client REGISTERID request
    virtual void ClientRegisterID(const cache_typ* buf);

    //! Process client REGISTERIDX request
    virtual void ClientRegisterIDX(unsigned int id);

    //! Process client UNREGISTERIDX request
    virtual void ClientUnRegisterIDX(unsigned int id);

    virtual int SendMessage(unsigned int id, size_t len, const unsigned int* buf, int cmd) = 0;

    //! Get the client ID
    int GetClientID();

    //! Find the client read/write permissions for client name
    client_setting_typ* FindClientSetting(rmname_typ& name);


protected:
    bool flag_exit;                 //! flag to close connection on CLIENTID invalid

    RMBaseSocket* stgsock;          //! point to the storage RMSocket
    RMBaseSocket* clisock;          //! the client RMSocket
    client_setting_list_typ& settings; //! permission settings for all clients
    client_setting_typ* cursetting; //! client permission etc for this client
    variable_list_typ varlist;      //! list of variables requested by client

};

#endif /* __SVRSOCK_H_DEF__ */

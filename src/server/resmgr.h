/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _RESMGR_H_DEF_
#define _RESMGR_H_DEF_

#ifdef _MSC_VER
#pragma warning(disable: 4786) /* disable warning C4786: symbol greater than 255 character */
#endif

#include <string>

#include <defaults.h>
#include <rm/rmsocket.h>
#include <rm/rmtcp.h>
#include <rm/rmsm.h>

#include <server/svrtcp.h>

//Default RM setting

//! default RM port number
#define RMPORT                  5000

//! default RM debug message UDP addr
#define RMDEBUGADDR             "255.255.255.255"

//! default RM debug port for UDP
#define RMDEBUGPORT             5555

//! default RM debug log file
#define RMDEBUGLOG              "rm.log"

//! RM configuration file
#define RMCFGFILE               "rm.cfg"

//! RM storage name
#define RMSTGNAME               "RESMGR4"


class Resmgr
{
public:
    /*! constructor for Resmgr class
       \param configfile the RM configuration file to use.
       \sa RMCFGFILE \ref pageConfigFile
     */
    Resmgr(const char* configfile = NULL);
    ~Resmgr();

    //! Parse command line arguments
    bool ParseArguments(int argc, char** argv);

    //! Main RM process loop
    int Run();

    //! RunTcpClient will handle client connection to RM
    int RunTcpClient(tcp_core& asock);

    //! Print the usage help
    void PrintUsage(char* progname);

protected:

    //! Wait for connections from clients
    bool WaitConnection();

    //Configuration file processing:

    //! Parse the configuration file
    bool ParseDefaults();

    //! Compile the regular expressions in loaded configuration file
    bool CompileDefaults();

    //! Search for client name in client setting list in config file
    client_setting_typ* FindClientSetting(rmname_typ& client_name);

public:
    //! set by ^C signal handler to signify exit condition
    static volatile unsigned flag_exit;

    //! set by SIGPIPE signal handler to signify broken socket connection
    static volatile unsigned flag_sigpipe;

private:
    const char* configfile;
    defaults def;

    //! port where RM is listening on
    int port;

    //! control whether debug output are printed on console
    int debugconsole;

    //! UDP port where RM debug message are sent
    int debugport;

    //! UDP addr where RM debug message are sent
    string debugaddr;

    //! debug log filename, NULL to not create a log file
    string debuglog;

    //! RM storage protocol type (expecting SM, SSM, SCRAMNET or SPREAD)
    string access;

    //! Storage index
    int storageidx;

    client_setting_list_typ clients;    //clients permissions/settings
    rmname_typ stgname;                 //storage name
    pid_list_typ client_pids;           //list of client connection pid
    int select_timeout_sec;
    int select_timeout_usec;
};


#endif /* _RESMGR_H_DEF_ */

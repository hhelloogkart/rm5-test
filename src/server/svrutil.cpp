/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include <server/svrutil.h>

#include <rm/rmsm.h>

re_typ::re_typ() : cre((pcre*) 0)
{
}

re_typ::~re_typ()
{
    if(cre)
    {
        pcre_free(cre);
    }
}

client_setting_typ::client_setting_typ() : limit(0)
{
}

client_setting_typ::~client_setting_typ()
{
    re_list_typ::iterator iter;
    for(iter = read.begin(); iter != read.end(); ++iter)
    {
        delete (*iter);
    }
    for(iter = write.begin(); iter != write.end(); ++iter)
    {
        delete (*iter);
    }
}

bool client_setting_typ::readable(rmname_typ& var)
{
    re_list_typ::iterator iter;
    pcre* re;
    int ovec[60];

    for(iter = read.begin(); iter != read.end(); ++iter)
    {
        re = (*iter)->cre;
        if(pcre_exec(re, NULL, (const char*)var, var.size(), 0, PCRE_NOTEMPTY,
                     ovec, sizeof(ovec)/sizeof(ovec[0])) > 0)
        {
            return true;
        }
    }
    return false;
}

bool client_setting_typ::writable(rmname_typ& var)
{
    re_list_typ::iterator iter;
    pcre* re;
    int ovec[60];

    for(iter = write.begin(); iter != write.end(); ++iter)
    {
        re = (*iter)->cre;
        if(pcre_exec(re, NULL, (const char*)var, var.size(), 0, PCRE_NOTEMPTY,
                     ovec, sizeof(ovec)/sizeof(ovec[0])) > 0)
        {
            return true;
        }
    }
    return false;
}

variable_typ::variable_typ() : write(false), read(false), reg(false)
{
}

client_setting_typ* FindClientSetting(client_setting_list_typ* cslist,
                                      rmname_typ& name)
{
    client_setting_list_typ::iterator iter;

    for(iter = cslist->begin(); iter != cslist->end(); ++iter)
    {
        if((*iter)->name == name)
        {
            return (*iter);
        }
    }

    return NULL;
}


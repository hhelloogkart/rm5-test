/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _ROAMSVR_H_DEF_
#define _ROAMSVR_H_DEF_

#include <comm/udpcore.h>

class RMRoamSvr
{
public:
    RMRoamSvr(unsigned short _rmport);
    ~RMRoamSvr();

    void Close();
    bool Select(const struct timeval* stm = 0);
    void OnDataAvail();

protected:
    unsigned short rmport;
    udp_core sock;
};

#endif /* _ROAMSVR_H_DEF_ */

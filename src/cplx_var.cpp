/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "rmstl.h"
#include "cplx_var.h"
#include "outbuf.h"
#include <util/dbgout.h>
#include <assert.h>

typedef VECTOR(mpt_var*) varlist_typ;

struct complex_var_private
{
    varlist_typ varlist;
};

complex_var::complex_var() :
    prvt(new complex_var_private)
{
    push();
}

complex_var::complex_var(const complex_var&) :
    mpt_var(), prvt(new complex_var_private)
{
    push();
}

complex_var::~complex_var()
{
    delete prvt;
}

void complex_var::add(mpt_var* p)
{
    p->parent = this;
    prvt->varlist.push_back(p);
}

void complex_var::remove(int idx)
{
    if ((idx < static_cast<int>(prvt->varlist.size())) && (idx >= 0))
    {
        delete prvt->varlist[idx];
        prvt->varlist.erase(prvt->varlist.begin() + idx);
    }
}

void complex_var::remove(mpt_var* child)
{
    disown(child);
    delete child;
}

void complex_var::disown(mpt_var* child)
{
    for (varlist_typ::iterator p = prvt->varlist.begin(); p != prvt->varlist.end(); ++p)
    {
        if (*p == child)
        {
            prvt->varlist.erase(p);
            break;
        }
    }
}

void complex_var::insert(mpt_var* p, int idx)
{
    if (idx < static_cast<int>(prvt->varlist.size()))
    {
        p->parent = this;
        if (idx <= 0)
        {
            prvt->varlist.insert(prvt->varlist.begin(), p);
        }
        else
        {
            prvt->varlist.insert(prvt->varlist.begin() + idx, p);
        }
    }
    else
    {
        add(p);
    }
}

mpt_var* complex_var::operator[](int idx) const
{
    mpt_var *var = 0;
    if (idx < static_cast<int>(prvt->varlist.size()))
    {
        var = prvt->varlist[idx];
    }
    return var;
}

int complex_var::cardinal() const
{
    return static_cast<int>(prvt->varlist.size());
}

void complex_var::delete_children()
{
    const int mx = cardinal();
    for (int cnt1 = 0; cnt1 < mx; ++cnt1)
    {
        delete prvt->varlist[cnt1];
    }
    prvt->varlist.clear();
}

void complex_var::shuffle_index(int idx)
{
    prvt->varlist.insert(prvt->varlist.begin() + idx, prvt->varlist.back());
    prvt->varlist.pop_back();
}

void complex_var::refresh()
{
    mpt_var::refresh();
    for (varlist_typ::iterator p = prvt->varlist.begin(); p != prvt->varlist.end(); ++p)
    {
        (*p)->refresh();
    }
}

int complex_var::extract(int len, const unsigned char* buf)
{
    varlist_typ::iterator p;
    int tsz = 0;

    for (p = prvt->varlist.begin(); (p != prvt->varlist.end()) && (len > 0); ++p)
    {
        tsz = len;
        len = (*p)->extract(len, buf);
        buf += tsz - len;
    }
    for (; (p != prvt->varlist.end()) && (*p)->extract_zero(buf); ++p)
    {
        ;
    }
    if (prvt->varlist.empty())
    {
        notDirty();
    }
    return len;
}

int complex_var::size() const
{
    int total = 0;
    for (varlist_typ::const_iterator p = prvt->varlist.begin(); p != prvt->varlist.end(); ++p)
    {
        total += (*p)->size();
    }
    return total;
}

void complex_var::output(outbuf& strm)
{
    for (varlist_typ::iterator p = prvt->varlist.begin(); p != prvt->varlist.end(); ++p)
    {
        (*p)->output(strm);
    }
}

bool complex_var::isValid() const
{
    for (varlist_typ::const_iterator p = prvt->varlist.begin(); p != prvt->varlist.end(); ++p)
    {
        if (!(*p)->isValid())
        {
            return false;
        }
    }
    return true;
}

bool complex_var::setInvalid()
{
    bool result = false;
    for (varlist_typ::iterator p = prvt->varlist.begin(); p != prvt->varlist.end(); ++p)
    {
        result |= (*p)->setInvalid();
    }
    return result;
}

const mpt_var& complex_var::operator=(const mpt_var& right)
{
    const complex_var* gen = RECAST(const complex_var*,&right);
    if (gen)
    {
        varlist_typ::iterator p;
        varlist_typ::const_iterator q = gen->prvt->varlist.begin();
        for (p = prvt->varlist.begin(); (p != prvt->varlist.end()) && (q != gen->prvt->varlist.end()); ++p)
        {
            (*(*p)) = (*(*q));
            ++q;
        }
    }
    return *this;
}

const complex_var& complex_var::operator=(const complex_var& right)
{
    varlist_typ::iterator p;
    varlist_typ::const_iterator q;
    q = right.prvt->varlist.begin();
    for (p = prvt->varlist.begin(); (p != prvt->varlist.end()) && (q != right.prvt->varlist.end()); ++p)
    {
        (*(*p)) = (*(*q));
        ++q;
    }
    return *this;
}

bool complex_var::operator==(const complex_var& right) const
{
    varlist_typ::const_iterator p;
    varlist_typ::const_iterator q;
    if (prvt->varlist.size() != right.prvt->varlist.size())
    {
        return false;
    }
    q = right.prvt->varlist.begin();
    for (p = prvt->varlist.begin(); p != prvt->varlist.end(); ++p)
    {
        if (**p != **q)
        {
            return false;
        }
        ++q;
    }
    return true;
}

bool complex_var::operator==(const mpt_var& right) const
{
    const complex_var* gen = RECAST(const complex_var*,&right);
    if (gen)
    {
        return (*this) == *gen;
    }
    return mpt_var::operator==(right);
}

void complex_var::child_copy_constructor(const complex_var& gen)
{
    pop();
    varlist_typ::const_iterator q = gen.prvt->varlist.begin();
    for (varlist_typ::iterator p = prvt->varlist.begin(); p != prvt->varlist.end(); ++p)
    {
        (**p) = (**q);
        ++q;
    }
}

istream& complex_var::operator>>(istream& s)
{
    varlist_typ::iterator p;
    for (p = prvt->varlist.begin(); p != prvt->varlist.end(); ++p)
    {
        (*p)->operator>>(s);
    }
    return s;
}

ostream& complex_var::operator<<(ostream& s) const
{
    varlist_typ::const_iterator p = prvt->varlist.begin();
    if (p != prvt->varlist.end())
    {
        (*p)->operator<<(s);
        ++p;
        for (; p != prvt->varlist.end(); ++p)
        {
            s << ' ';
            (*p)->operator<<(s);
        }
    }
    return s;
}

void complex_var::resize(int nsize)
{
    // default behaviour is to assert if the size is incorrect
    assert(nsize == cardinal());
}

/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include <flun_var.h>
#include <gen_var.h>
#include <flun_show.h>
#include <list>
#include <map>
#include <stdlib.h>
#include <iostream>

#ifndef MAX_ELEMENT
#define MAX_ELEMENT 200 //to limit number of elements per sending, excluding deleted children
#endif

struct element_info
{
    complex_var* var;
    int type;
    unsigned int dirty_len; /* zero = not dirty, otherwise bytes to write out */
};

typedef std::map<int,complex_factory_base*> complex_factory_map_typ;
typedef std::list<complex_factory_base*> complex_factory_list_typ;
typedef std::list<flex_union_show*> childshow_list_typ;
typedef std::list<int> id_list_typ;
typedef std::map<unsigned short,element_info> id_map_typ; //<unique id, element info>

class flex_union_show;

class flex_union_prvt_show : public flex_union_show
{
public:
    flex_union_prvt_show(flex_union_var* var);
    virtual ~flex_union_prvt_show();
    /*reimp*/ void setDirty(mpt_var*);
    /*reimp*/ void compute(int id);
    void setEnabled(bool enable);

private:
    bool enb;

};

static complex_factory_list_typ factory_list;

complex_factory_base::complex_factory_base()
{
    factory_list.push_back(this);
}

struct flex_union_var_private
{
    complex_factory_map_typ factory_map;
    id_map_typ id_map;
    id_list_typ deleted_children;
    childshow_list_typ childshow_list; //reading
    flex_union_prvt_show* shw; //writing
};

flex_union_var::flex_union_var() : prvt(new flex_union_var_private)
{
    prvt->shw = new flex_union_prvt_show(this);
}

flex_union_var::~flex_union_var()
{
    delete prvt->shw;
    delete prvt;
}

int flex_union_var::extract(int len, const unsigned char* buf)
{
    //std::cout << "extract " << std::endl;
    const unsigned short INVALID = ~0; //check for ffff
    generic_var<unsigned short> id;
    generic_var<unsigned short> length;
    generic_var<unsigned short> type; //no type when lenght is 0

    while (static_cast<unsigned int>(len) >= sizeof(short))
    {
        // 1. unpack the id, check that it is not the end
        len = id.extract(len, buf);

        if (id == INVALID)
        {
            return len;
        }
        if (len <= 0)
        {
            return 0;
        }
        buf += sizeof(short);

        // 2. unpack the length, check if we are deleting
        len = length.extract(len, buf);

        if (length.to_unsigned_short() == 0)
        {

            destroy(id.to_int());
            for (childshow_list_typ::iterator it=prvt->childshow_list.begin(); it!=prvt->childshow_list.end(); ++it)
            {
                (*it)->setIDDirty(id.to_int());
            }
            if (len <= 0)
            {
                return 0;
            }
            buf += sizeof(short);
        }
        else
        {
            // 3. unpack the type, check if the id exists, get the buffer
            if (static_cast<unsigned int>(len) <= sizeof(short))
            {
                return 0;
            }
            buf += sizeof(short);
            len = type.extract(len, buf);
            buf += sizeof(short);

            complex_var* var = item(id);
            if (var == 0)
            {
                var = create(type.to_int(), id.to_int());
            }
            if (var != 0)
            {
                // 4. ask the child to unpack
                prvt->shw->setEnabled(false);
                var->extract(len, buf);
                prvt->shw->setEnabled(true);
            }
            // 5. increment to next element
            buf += length.to_int();
            len -= length.to_unsigned_short();
        } //else
    } //while
    return 0;
}

//remainder of division
int flex_union_var::make_unique_id() const
{
    int id;
    do
    {
        id = rand() % (1<<15); /* limit id to 16 bits */
    }
    while (prvt->id_map.find(id)!=prvt->id_map.end());
    return id;
}

complex_var* flex_union_var::create(int type, int* id)
{
    *id = make_unique_id();
    return create(type, *id);
}

complex_var* flex_union_var::create(int type, int id)
{
    complex_factory_map_typ::iterator it = prvt->factory_map.find(type);
    if (it==prvt->factory_map.end())
    {
        return 0;
    }

    element_info info;
    push(); //parent of complex_var
    info.var = (*it).second->create();
    pop();
    info.type = type;
    info.dirty_len = 0;
    prvt->id_map[id] = info;
    for (childshow_list_typ::iterator its=prvt->childshow_list.begin(); its!=prvt->childshow_list.end(); ++its)
    {
        info.var->addCallback(*its);
    }

    info.var->addCallback(prvt->shw);

    return info.var;
}


void flex_union_var::destroy(int id)
{
    id_map_typ::iterator it = prvt->id_map.find(id);
    if (it == prvt->id_map.end())
    {
        return;
    }

    remove((*it).second.var);
    prvt->id_map.erase(it);

    for (childshow_list_typ::iterator its=prvt->childshow_list.begin(); its!=prvt->childshow_list.end(); ++its)
    {
        (*its)->setIDDirty(id);
    }

    prvt->deleted_children.push_back(id); //for size() and output()
    setRMDirty();
}

int flex_union_var::typeOf(int id)
{
    id_map_typ::iterator it = prvt->id_map.find(id);
    if (it == prvt->id_map.end())
    {
        return -1;
    }

    return it->second.type;
}


int flex_union_var::item_cardinal() const
{
    return static_cast<int>(prvt->id_map.size());
}

int flex_union_var::item_first_id() const
{
    id_map_typ::iterator it = prvt->id_map.begin();
    return (it != prvt->id_map.end()) ? it->first : -1;
}

int flex_union_var::item_next_id(int id) const
{
    id_map_typ::iterator it = prvt->id_map.find(id);
    if (it == prvt->id_map.end())
    {
        return -1;
    }
    ++it;
    return (it != prvt->id_map.end()) ? it->first : -1;
}

int flex_union_var::type_cardinal(int type) const
{
    int iSize = 0;
    for(id_map_typ::iterator it = prvt->id_map.begin(); it != prvt->id_map.end(); ++it)
    {
        if(it->second.type == type)
        {
            iSize++;
        }
    }
    return iSize;
}

int flex_union_var::type_first_id(int type) const
{
    for(id_map_typ::iterator it = prvt->id_map.begin(); it != prvt->id_map.end(); ++it)
    {
        if(it->second.type == type)
        {
            return it->first;
        }
    }
    return -1;
}

int flex_union_var::type_next_id(int id) const
{
    id_map_typ::iterator it = prvt->id_map.find(id);
    if (it == prvt->id_map.end())
    {
        return -1;
    }
    int type = it->second.type;
    for (++it; it != prvt->id_map.end(); ++it)
    {
        if(it->second.type == type)
        {
            return it->first;
        }
    }
    return -1;
}

complex_var* flex_union_var::item(int id) const
{
    id_map_typ::iterator it = prvt->id_map.find(id);
    return (it == prvt->id_map.end()) ? 0 : (*it).second.var;
}

void flex_union_var::setup_factories()
{
    for (complex_factory_list_typ::iterator it=factory_list.begin(); it!=factory_list.end(); ++it)
    {
        prvt->factory_map[(*it)->type()] = *it;
    }
    factory_list.clear();
}

void flex_union_var::addChildCallback(flex_union_show* shw)
{
    for (id_map_typ::iterator it=prvt->id_map.begin(); it!=prvt->id_map.end(); ++it)
    {
        (*it).second.var->addCallback(shw);
    }
    prvt->childshow_list.push_back(shw);
}


void flex_union_var::removeChildCallback(flex_union_show* shw)
{
    prvt->childshow_list.remove(shw);
    for (id_map_typ::iterator it=prvt->id_map.begin(); it!=prvt->id_map.end(); ++it)
    {
        (*it).second.var->removeCallback(shw);
    }
}

int flex_union_var::addr_to_id(mpt_var* var)
{
    for (id_map_typ::iterator it=prvt->id_map.begin(); it!=prvt->id_map.end(); ++it)
    {
        if ((*it).second.var == var)
        {
            return (*it).first;
        }
    }
    return -1;
}

int flex_union_var::size() const
{
#ifdef MAX_ELEMENT
    int iCnt = 0;
#endif
    int total = 0;
    for (id_map_typ::iterator it=prvt->id_map.begin(); it!=prvt->id_map.end(); ++it)
    {
        int dirty_len = (*it).second.dirty_len;
        if (dirty_len)
        {
            total += sizeof(short)*3 + dirty_len;
#ifdef MAX_ELEMENT
            iCnt++;
            if(iCnt==MAX_ELEMENT)
            {
                break;
            }
#endif
        }
    }
    total += sizeof(short) * (static_cast<int>(prvt->deleted_children.size())*2+1);
    return total;
}

void flex_union_var::output(outbuf& strm)
{
#ifdef MAX_ELEMENT
    int iCnt = 0;
#endif

    for (id_map_typ::iterator it=prvt->id_map.begin(); it!=prvt->id_map.end(); ++it)
    {
        int dirty_len = (*it).second.dirty_len;
        if (dirty_len)
        {
            generic_var<unsigned short> id((*it).first);
            generic_var<unsigned short> length(dirty_len);
            generic_var<unsigned short> type((*it).second.type);
            id.output(strm);
            length.output(strm);
            type.output(strm);
            (*it).second.var->output(strm);
            (*it).second.dirty_len = 0;
#ifdef MAX_ELEMENT
            iCnt++;
            if(iCnt==MAX_ELEMENT)
            {
                break;
            }
#endif
        }
    }

    for (id_list_typ::iterator it=prvt->deleted_children.begin(); it!=prvt->deleted_children.end(); ++it)
    {
        generic_var<unsigned short> id(*it);
        generic_var<unsigned short> length(0);
        id.output(strm);
        length.output(strm);
    }
    generic_var<unsigned short> the_end(static_cast<unsigned short>(~0));
    the_end.output(strm);
    //clear deleted_children
    prvt->deleted_children.clear();

}

bool flex_union_var::setInvalid()
{
    bool result = false;
    for(id_map_typ::iterator it = prvt->id_map.begin(); it != prvt->id_map.end(); ++it)
    {
        result |= it->second.var->setInvalid();
    }
    return result;
}

void flex_union_var::setChildDirty(int id)
{
    id_map_typ::iterator it = prvt->id_map.find(id);
    if (it != prvt->id_map.end())
    {
        (*it).second.dirty_len = (*it).second.var->size();
    }
}

bool flex_union_var::operator==(const mpt_var& right) const
{
    return complex_var::operator==(right);
}

const mpt_var& flex_union_var::operator=(const mpt_var& right)
{
    const flex_union_var* _r = RECAST(const flex_union_var*,&right);
    id_map_typ old_id_map;
    swap(prvt->id_map, old_id_map);
    for (id_map_typ::iterator it=_r->prvt->id_map.begin(); it!=_r->prvt->id_map.end(); ++it)
    {
        // can we find the matching ID and type?
        id_map_typ::iterator it2=old_id_map.find((*it).first);
        if (it2!=old_id_map.end() && ((*it2).second.type == (*it).second.type))
        {
            *((*it2).second.var) = *((*it).second.var);
            prvt->id_map[(*it2).first] = (*it2).second;
            old_id_map.erase(it2);
        }
        else
        {
            // we make new one
            complex_var* var = create((*it).second.type, (*it).first);
            *var = *((*it).second.var);
        }
    }
    id_list_typ new_deleted_children;
    for (id_map_typ::iterator it=old_id_map.begin(); it!=old_id_map.end(); ++it)
    {
        new_deleted_children.push_back((*it).first);
    }

    for (id_list_typ::iterator it=new_deleted_children.begin(); it!=new_deleted_children.end(); ++it)
    {
        destroy(*it);
    }
    return *this;
}

flex_union_prvt_show::flex_union_prvt_show(flex_union_var* var) : enb(true)
{
    obj = var;
}

flex_union_prvt_show::~flex_union_prvt_show()
{
}

void flex_union_prvt_show::setEnabled(bool enable)
{
    enb = enable;
}


void flex_union_prvt_show::setDirty(mpt_var* var)
{
    if (enb)
    {
        if (var)
        {
            int id = obj->addr_to_id(var);
            if (id != -1)
            {
                setIDDirty(id);
            }
        }
    }
}

void flex_union_prvt_show::compute(int id)
{
    obj->setChildDirty(id);
}

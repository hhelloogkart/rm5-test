/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifdef VXWORKS
#define __PUT_STATIC_DATA_MEMBERS_HERE
#endif

#include <mpt_var.h>
#include <cplx_var.h>
#include <link_var.h>
#include "rmstl.h"
#include <util/dbgout.h>

typedef MAP(unsigned int, int, less<unsigned int>) classlist_typ;
typedef LIST(complex_var*) createlist_typ;
typedef mpt_LinkedList<mpt_baseshow*> showlist_typ;
typedef mpt_LinkedList<mpt_var*> fosterparent_typ;

struct mpt_var_private
{
    showlist_typ showlist;
    fosterparent_typ parentlist;
};

#if __unix
#include <pthread.h>
static pthread_key_t createkey = 0;
static void destroy_createlist(void* ptr)
{
    createlist_typ* p = reinterpret_cast<createlist_typ*>(ptr);
    delete p;
    pthread_setspecific(createkey, 0);
}
static createlist_typ& createlist()
{
    if (!createkey)
    {
        pthread_key_create(&createkey, &destroy_createlist);
    }
    createlist_typ* p = reinterpret_cast<createlist_typ*>(pthread_getspecific(createkey));
    if (!p)
    {
        p = new createlist_typ;
        pthread_setspecific(createkey, p);
    }
    return *p;
}
#elif defined(_WIN32)
#include <windows.h>
static DWORD createkey = TLS_OUT_OF_INDEXES;
struct createlist_cleanup
{
    ~createlist_cleanup();
};
struct createlist_proc_cleanup
{
    ~createlist_proc_cleanup();
};
__declspec(thread) static createlist_cleanup cleanup;
static createlist_proc_cleanup proc_cleanup;
createlist_cleanup::~createlist_cleanup()
{
    if (createkey != TLS_OUT_OF_INDEXES)
    {
        createlist_typ* p = reinterpret_cast<createlist_typ*>(TlsGetValue(createkey));
        delete p;
    }
}
createlist_proc_cleanup::~createlist_proc_cleanup()
{
    if (createkey != TLS_OUT_OF_INDEXES)
    {
        TlsFree(createkey);
    }
}
static createlist_typ& createlist()
{
    if (createkey == TLS_OUT_OF_INDEXES)
    {
        createkey = TlsAlloc();
    }
    createlist_typ* p = reinterpret_cast<createlist_typ*>(TlsGetValue(createkey));
    if (!p)
    {
        p = new createlist_typ;
        TlsSetValue(createkey, p);
    }
    return *p;
}
#else
static createlist_typ& createlist()
{
    static createlist_typ s;
    return s;
}
#endif

bool isValid(const float& data)
{
    return (reinterpret_cast< const int& >(data) & 0x7F800000) != 0x7F800000;
}

#if defined _MSC_VER
typedef unsigned __int64 uint64;
typedef unsigned int uintptr;
#elif cplusplus >= 201103L
#include <cstdint>
typedef uint64_t uint64;
typedef uintptr_t uintptr;
#else
#include <stdint.h>
typedef uint64_t uint64;
typedef uintptr_t uintptr;
#endif

/*
    Adapted from OpenCV. See include/opencv2/hal/defs.h
 */
inline int isinf(double x)
{
    union
    {
        uint64 u;
        double f;
    } ieee754;
    ieee754.f = x;
    return ((unsigned)(ieee754.u >> 32) & 0x7fffffff) == 0x7ff00000 &&
           ((unsigned)ieee754.u == 0);
}

inline int isnan(double x)
{
    union
    {
        uint64 u;
        double f;
    } ieee754;
    ieee754.f = x;
    return ((unsigned)(ieee754.u >> 32) & 0x7fffffff) +
           ((unsigned)ieee754.u != 0) > 0x7ff00000;
}

bool isValid(const double& data)
{
    return !(isinf(data) || isnan(data));
}

istream& operator>>(istream& s, mpt_var& right)
{
    return right.operator>>(s);
}

ostream& operator<<(ostream& s, const mpt_var& right)
{
    return right.operator<<(s);
}

mpt_var::mpt_var() :
    prvt(0)
{
    if (!createlist().empty())
    {
        complex_var* p = createlist().back();
        if (p)
        {
            p->add(this);
        }
    }
    else
    {
        parent = (mpt_var*)NULL;
    }
}

mpt_var::~mpt_var()
{
    delete prvt;
}

void mpt_var::refresh()
{
    if (!prvt)
    {
        return;
    }

    for (showlist_typ::iterator p = prvt->showlist.begin(); !p.end();)
    {
        showlist_typ::iterator q = p;
        ++q; // store the next iterator
        (*(*p))();
        p = q; // move p to the next iterator, even if the shw removes
               // callback from this var, which would render p invalid
    }
}

void mpt_var::installFosterParent(mpt_var* fparent)
{
    if (!prvt)
    {
        prvt = new mpt_var_private;
    }
    prvt->parentlist.push_back(fparent);
}

int mpt_var::classIndex()
{
    static classlist_typ classlist;
    const unsigned int rtype = (rtti() ^ (uintptr)parent);
    const int cnt = classlist[rtype];
    classlist[rtype] = cnt + 1;
    return cnt;
}

void mpt_var::setDirty()
{
    if (prvt)
    {
        for (showlist_typ::iterator p = prvt->showlist.begin(); !p.end(); ++p)
        {
            (*p)->setDirty(this);
        }
        for (fosterparent_typ::iterator it = prvt->parentlist.begin(); !it.end(); ++it)
        {
            (*it)->setDirty();
        }
    }
    if (parent)
    {
        parent->setDirty();
    }
}

void mpt_var::notDirty()
{
    if (prvt)
    {
        for (showlist_typ::iterator p = prvt->showlist.begin(); !p.end(); ++p)
        {
            (*p)->notDirty(this);
        }
        for (fosterparent_typ::iterator it = prvt->parentlist.begin(); !it.end(); ++it)
        {
            (*it)->notDirty();
        }
    }
    if (parent)
    {
        parent->notDirty();
    }
}

void mpt_var::setRMDirty()
{
    if (prvt)
    {
        for (fosterparent_typ::iterator it = prvt->parentlist.begin(); !it.end(); ++it)
        {
            (*it)->setRMDirty();
        }
    }
    if (parent)
    {
        parent->setRMDirty();
    }
}

bool mpt_var::isRMDirty() const
{
    return (parent) ? parent->isRMDirty() : false;
}

void mpt_var::nullValue()
{
    if (setInvalid())
    {
        setRMDirty();
    }
}

const mpt_var& mpt_var::operator=(const mpt_var&)
{
    dout << "The wrong operator= for mpt_var is called\n";
    return *this;
}

bool mpt_var::operator==(const mpt_var& right) const
{
    const link_var* rlink = RECAST(const link_var*, &right);
    return (rlink) ? rlink->operator==(*this) : false;
}

void mpt_var::push()
{
    createlist().push_back(reinterpret_cast<complex_var*>(this));
}

void mpt_var::pop()
{
    createlist().pop_back();
}

bool mpt_var::check_stack()
{
    if (createlist().size() != 0)
    {
        dout << "Warning stack is not clear\n";
        return false;
    }
    return true;
}

void mpt_var::addCallback(mpt_baseshow* show)
{
    if (!prvt)
    {
        prvt = new mpt_var_private;
    }
    prvt->showlist.push_back(show);
}
void mpt_var::removeCallback(mpt_baseshow* show)
{
    if (!prvt)
    {
        return;
    }
    prvt->showlist.remove(show);
}

bool mpt_var::extract_zero(const unsigned char*)
{
    return false;
}

extern "C" RM_EXPORT
unsigned int rm_hash_string(const char* file)
{
    return hash_string(file);
}

/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "avl_var.h"
#include "outbuf.h"
#include <iomanip>
#include <string.h>

array_value_var::array_value_var() : data(0), sz(0)
{
}

array_value_var::array_value_var(const array_value_var& cpy) : mpt_var()
{
    if (cpy.sz == 0)
    {
        data = 0;
        sz = 0;
    }
    else
    {
        data = new char[cpy.sz]; // parasoft-suppress  MISRA2008-18_4_1 "Tool error" // parasoft-suppress  MISRA2012-DIR-4_12 "Tool error"
        sz = cpy.sz;
        memcpy(data, cpy.data, sz);
    }
}

array_value_var::~array_value_var()
{
    delete[] data;
}

array_value_var::operator const void* () const
{
    return data;
}

array_value_var::operator void* ()
{
    if (data) setDirty();
    return data;
}

int array_value_var::cardinal() const
{
    return sz / width();
}

void array_value_var::resize(int len)
{
    const int wlen = len*width();
    if (wlen != static_cast<int>(sz))
    {
        char* ndata = (wlen > 0) ? new char[wlen] : 0;
        if ((wlen > 0) && (sz > 0))
        {
            memcpy(ndata, data, (wlen < static_cast<int>(sz)) ? wlen : sz);
        }
        delete[] data;
        data = ndata;
        if (wlen > static_cast<int>(sz))
        {
            memset(ndata+sz, 0xFF, wlen-sz);
        }
        sz = wlen;
        setDirty();
        setRMDirty();
    }
}

int array_value_var::cardinal_width() const
{
    return 2;
}

int array_value_var::cardinal_offset() const
{
    return 0;
}

int array_value_var::size_cardinal() const
{
    const int wd = cardinal_width();
    return (wd > 0) ? wd : -wd;
}

void array_value_var::output_cardinal(outbuf& strm)
{
    const int wd = cardinal_width();
    const int of = cardinal_offset();
    int ct = 0;
    const int sz = size_cardinal();
    union
    {
        unsigned long long vll;
        unsigned long vl;
        unsigned short vs;
        unsigned char vc;
        char c[8];
    } car;
    switch (wd)
    {
    case -8:
    case 8:
        car.vll = cardinal();
        break;
    case -4:
    case 4:
        car.vl = cardinal();
        break;
    case -2:
    case 2:
        car.vs = cardinal();
        break;
    case -1:
    case 1:
        car.vc = cardinal();
        break;
    default:
        break;
    }

    // apply offset via padding
    for (int cnt=0; (cnt<of) && (ct<sz); ++cnt, ++ct)
    {
        strm += '\0';
    }

    // apply cardinal
    if (wd > 0)
    {
        for (int cnt=0; (cnt<wd) && (ct<sz); ++cnt, ++ct)
        {
            strm += car.c[cnt];
        }
    }
    else if (wd < 0)
    {
        for (int cnt=-wd-1; (cnt>=0) && (ct<sz); --cnt, ++ct)
        {
            strm += car.c[cnt];
        }
    }

    // apply padding
    for (; ct<sz; ++ct)
    {
        strm += '\0';
    }
}

int array_value_var::extract_cardinal(int buflen, const unsigned char* buf, int& car)
{
    int left = 0;
    if (buflen < size_cardinal())
    {
        car = 0;
    }
    else
    {
        union
        {
            unsigned long long vll;
            unsigned long vl;
            unsigned short vs;
            unsigned char vc;
            char c[8];
        } ucar;
        int wd = cardinal_width();
        if (wd>0)
        {
            memcpy(ucar.c, buf+cardinal_offset(), wd);
        }
        else if (wd<0)
        {
            buf += cardinal_offset();
            for (int cnt= -wd-1; cnt>=0; --cnt, ++buf)
            {
                ucar.c[cnt] = *buf;
            }
        }
        switch (wd)
        {
        case -8:
        case 8:
            car = static_cast<int>(ucar.vll);
            break;
        case -4:
        case 4:
            car = ucar.vl;
            break;
        case -2:
        case 2:
            car = ucar.vs;
            break;
        case -1:
        case 1:
            car = ucar.vc;
            break;
        default: // includes case 0
            car = buflen/width();
            break;
        }
        left = buflen-size_cardinal();
    }
    return left;
}

int array_value_var::extract(int len, const unsigned char* buf)
{
    int car=0, tmpsz=len, left=0;
    // extract the cardinal by calling a virtual function
    len = extract_cardinal(len, buf, car);
    buf += tmpsz - len;

    if (len == 0)
    {
        if (sz != 0)
        {
            delete[] data;
            data = 0;
            sz = 0;
            setDirty();
        }
        else
        {
            notDirty();
        }
        // nothing more to do
    }
    else
    {
        // cardinal adjustment
        if ((len / width()) < car)
        {
            car = len / width();
        }

        int nsz = car * width();
        char* ndata = (nsz > 0) ? new char[nsz] : 0;

#ifndef FLIP
        if (nsz > 0)
        {
            memcpy(ndata, buf, nsz);
        }
#else
        for (int cnt1=width()-1; cnt1<nsz; cnt1+=width())
        {
            for (int cnt2=0; cnt2<width(); ++cnt2, ++buf)
            {
                ndata[cnt1-cnt2] = *buf;
            }
        }
#endif

        if ((static_cast<int>(sz)==nsz) && (memcmp(data, ndata, sz) == 0))
        {
            notDirty();
            delete[] ndata;
        }
        else
        {
            setDirty();
            delete[] data;
            data = ndata;
            sz = nsz;
        }
        left = len - sz;
    }
    return left;
}

int array_value_var::flip_extract(int len, const unsigned char* buf)
{
    int car=0, tmpsz=len, left=0;
    // extract the cardinal by calling a virtual function
    len = extract_cardinal(len, buf, car);
    buf += tmpsz - len;

    if (len == 0)
    {
        if (sz != 0)
        {
            delete[] data;
            data = 0;
            sz = 0;
            setDirty();
        }
        else
        {
            notDirty();
        }
        // nothing more to do
    }
    else
    {
        // cardinal adjustment
        if ((len / width()) < car)
        {
            car = len / width();
        }

        const int nsz = car * width();
        char* ndata = (nsz > 0) ? new char[nsz] : 0;

#ifdef FLIP
        if (nsz > 0)
        {
            memcpy(ndata, buf, nsz);
        }
#else
        for (int cnt1=width()-1; cnt1<nsz; cnt1+=width())
        {
            for (int cnt2=0; cnt2<width(); ++cnt2, ++buf)
            {
                ndata[cnt1-cnt2] = *buf;
            }
        }
#endif

        if ((static_cast<int>(sz)==nsz) && (memcmp(data, ndata, sz) == 0))
        {
            notDirty();
            delete[] ndata;
        }
        else
        {
            setDirty();
            delete[] data;
            data = ndata;
            sz = nsz;
        }
        left = len - sz;
    }
    return left;
}

int array_value_var::size() const
{
    return size_cardinal() + sz;
}

void array_value_var::output(outbuf& strm)
{
    output_cardinal(strm);
#ifndef FLIP
    for (int cnt=0; cnt<static_cast<int>(sz); ++cnt)
    {
        strm += data[cnt];
    }
#else
    for (int cnt1=width()-1; cnt1<sz; cnt1+=width())
    {
        for (int cnt2=0; cnt2<width(); ++cnt2)
        {
            strm += data[cnt1-cnt2];
        }
    }
#endif
}

void array_value_var::flip_output(outbuf& strm)
{
    output_cardinal(strm);
#ifdef FLIP
    for (int cnt=0; cnt<(int)sz; ++cnt)
    {
        strm += data[cnt];
    }
#else
    for (int cnt1=width()-1; cnt1<static_cast<int>(sz); cnt1+=width())
    {
        for (int cnt2=0; cnt2<width(); ++cnt2)
        {
            strm += data[cnt1-cnt2];
        }
    }
#endif
}

bool array_value_var::isValid() const
{
    return sz > 0;
}

bool array_value_var::setInvalid()
{
    const bool havedata = data != 0;
    if (havedata)
    {
        delete[] data;
        sz = 0;
        data = 0;
        setDirty();
    }
    else
    {
        notDirty();
    }
    return havedata;
}

const array_value_var& array_value_var::operator=(const array_value_var& right)
{
    if ((sz != right.sz) || (memcmp(data, right.data, sz) != 0))
    {
        delete[] data;

        sz = right.sz;
        data = (sz > 0) ? new char[sz] : 0;
        if (sz > 0)
        {
            memcpy(data, right.data, sz);
        }

        setDirty();
        setRMDirty();
    }
    else
    {
        notDirty();
    }
    return *this;
}

const mpt_var& array_value_var::operator=(const mpt_var& right)
{
    const array_value_var* gen = RECAST(const array_value_var*,&right);
    if (gen != NULL)
    {
        operator=(*gen);
    }
    return *this;
}


bool array_value_var::operator==(const array_value_var& right) const
{
    bool equals = false;
    if (right.sz == sz)
    {
        equals = (sz != 0) ? (memcmp(right.data, data, sz) == 0) : true;
    }
    return equals;
}
bool array_value_var::operator==(const mpt_var& right) const
{
    const array_value_var* gen = RECAST(const array_value_var*,&right);
    return (gen != NULL) ? operator==(*gen) : mpt_var::operator==(right);
}
istream& array_value_var::operator>>(istream& s)
{
    setInvalid();
    s >> sz;
    if (sz != 0)
    {
        s >> std::hex;
        data = new char[sz];
        int idx = 0;
        for (; (idx<static_cast<int>(sz)) && !s.eof() && !s.fail(); ++idx)
        {
            unsigned long tmp = 0;
            s >> tmp;
            if (s.fail())
            {
                break;
            }
            data[idx] = static_cast<char>(tmp);
        }
        s >> std::dec;
        if (idx==0)
        {
            delete[] data;
            data = 0;
            sz = 0;
        }
        else if (idx!=static_cast<int>(sz))
        {
            char* ndata = new char[idx];
            memcpy(ndata, data, idx);
            delete[] data;
            data = ndata;
            sz = idx;
        }
    }
    return s;
}
ostream& array_value_var::operator<<(ostream& s) const
{
    s << sz;
    s << std::hex;
    for (int idx=0; idx<static_cast<int>(sz); ++idx)
    {
        s << ' ' << std::setw(2) << std::setfill('0') << (0xFF & data[idx]);
    }
    s << std::dec;
    return s;
}
void array_value_var::toValue(unsigned int idx, long& value) const
{
    value = to_long(idx);
}
void array_value_var::toValue(unsigned int idx, int& value) const
{
    value = to_int(idx);
}
void array_value_var::toValue(unsigned int idx, short& value) const
{
    value = to_short(idx);
}
void array_value_var::toValue(unsigned int idx, char& value) const
{
    value = to_char(idx);
}
void array_value_var::toValue(unsigned int idx, unsigned long& value) const
{
    value = to_unsigned_long(idx);
}
void array_value_var::toValue(unsigned int idx, unsigned int& value) const
{
    value = to_unsigned_int(idx);
}
void array_value_var::toValue(unsigned int idx, unsigned short& value) const
{
    value = to_unsigned_short(idx);
}
void array_value_var::toValue(unsigned int idx, unsigned char& value) const
{
    value = to_unsigned_char(idx);
}
void array_value_var::toValue(unsigned int idx, float& value) const
{
    value = to_float(idx);
}
void array_value_var::toValue(unsigned int idx, double& value) const
{
    value = to_double(idx);
}
void array_value_var::toValue(unsigned int idx, long long& value) const
{
    value = to_long_long(idx);
}
void array_value_var::toValue(unsigned int idx, unsigned long long& value) const
{
    value = to_unsigned_long_long(idx);
}
void array_value_var::fromValue(unsigned int idx, long value)
{
    from_long(idx, value);
}
void array_value_var::fromValue(unsigned int idx, int value)
{
    from_int(idx, value);
}
void array_value_var::fromValue(unsigned int idx, short value)
{
    from_short(idx, value);
}
void array_value_var::fromValue(unsigned int idx, char value)
{
    from_char(idx, value);
}
void array_value_var::fromValue(unsigned int idx, unsigned long value)
{
    from_unsigned_long(idx, value);
}
void array_value_var::fromValue(unsigned int idx, unsigned int value)
{
    from_unsigned_int(idx, value);
}
void array_value_var::fromValue(unsigned int idx, unsigned short value)
{
    from_unsigned_short(idx, value);
}
void array_value_var::fromValue(unsigned int idx, unsigned char value)
{
    from_unsigned_char(idx, value);
}
void array_value_var::fromValue(unsigned int idx, float value)
{
    from_float(idx, value);
}
void array_value_var::fromValue(unsigned int idx, double value)
{
    from_double(idx, value);
}
void array_value_var::fromValue(unsigned int idx, const char* value)
{
    from_char_ptr(idx, value);
}
void array_value_var::fromValue(unsigned int idx, long long value)
{
    from_long_long(idx, value);
}
void array_value_var::fromValue(unsigned int idx, unsigned long long value)
{
    from_unsigned_long_long(idx, value);
}

/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "compress_var.h"
#include <outbuf.h>

compress_var::compress_var() : complex_var(), dirty(false), compression_level(0)
{
}

compress_var::compress_var(const compress_var& right) : complex_var(), dirty(false), compression_level(right.compression_level)
{
}

compress_var::~compress_var()
{
    delete[] enc_buf.get();
    delete[] raw_buf.get();
    delete[] dec_buf.get();
}

void compress_var::init()
{
    const int sz = complex_var::size();
    const int ovr = compressionOverhead();

    enc_buf.set(new unsigned char[sz + ovr], sz + ovr);
    raw_buf.set(new unsigned char[sz], sz);
    dec_buf.set(new unsigned char[sz], sz);
}

int compress_var::extract(int len, const unsigned char* buf)
{
    dec_buf.setsize(0);
    int olen = decompress(dec_buf, len, buf);
    if (dec_buf.size() == 0)
    {
        // maybe too small
        if ((len * 20) > static_cast<int>(dec_buf.maxsize()))
        {
            outbuf dec_buf2;
            dec_buf2.set(new unsigned char[len * 20], len * 20);
            olen = decompress(dec_buf2, len, buf);
            if (dec_buf2.size() == 0)
            {
                notDirty();
            }
            else
            {
                complex_var::extract(dec_buf2.size(), dec_buf2.get());
            }
            delete[] dec_buf2.get();
        }
        else
        {
            notDirty();
        }
    }
    else
    {
        complex_var::extract(dec_buf.size(), dec_buf.get());
    }
    return olen;
}

void compress_var::output(outbuf& strm)
{
    if ((size() > 0) && (enc_buf.size() <= (strm.maxsize() - strm.size())))
    {
        strm += enc_buf;
    }
}

int compress_var::size() const
{
    if (dirty)
    {
        compress_var* tis = (const_cast <compress_var*> (this));
        tis->dirty = false;
        const int childsz = complex_var::size();
        if (childsz > static_cast<int>(raw_buf.maxsize()))
        {
            delete[] raw_buf.get();
            tis->raw_buf.set(new unsigned char[childsz], childsz);
            delete[] enc_buf.get();
            const int ovr = compressionOverhead();
            tis->enc_buf.set(new unsigned char[childsz + ovr], childsz + ovr);
        }
        else
        {
            tis->raw_buf.setsize(0);
            tis->enc_buf.setsize(0);
        }
        tis->complex_var::output(tis->raw_buf);
        tis->compress(tis->enc_buf, tis->raw_buf);
    }
    return (const_cast <compress_var*> (this))->enc_buf.size();
}

void compress_var::setRMDirty()
{
    dirty = true;
    mpt_var::setRMDirty();
}

bool compress_var::setInvalid()
{
    dirty = true;
    return complex_var::setInvalid();
}

unsigned int compress_var::compressionLevel()
{
    return compression_level;
}

void compress_var::setCompressionLevel(unsigned int l)
{
    compression_level = l;
}

const compress_var& compress_var::operator=(const compress_var& right)
{
    if (this != &right)
    {
        compression_level = right.compression_level;
        complex_var::operator=(right);
        dirty = true;
    }
    return *this;
}

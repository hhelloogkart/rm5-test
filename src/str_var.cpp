/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "str_var.h"
#include <string.h>
#include <stdlib.h>

void string_stream_helper_read(istream& strm, char* str, int sz)
{
    // clear off any extra spaces
    char& tch = str[0];
    do
    {
        strm.get(tch);
        if (strm.eof() || strm.fail())
        {
            str[0] = '\0';
            return;
        }
    }
    while (tch == ' ' || (tch <= 0x0D));

    // detect for double quotation
    if (tch == '\"')
    {
        char* p = str, * q = p+sz;
        for (; (p != q) && strm; ++p)
        {
            strm.get(*p);
            if (*p == '\\' && (p + 1 != q))
            {
                strm.get(*p);
                if (*p == '\"')
                {
                    ;
                }
                else if (*p == '\\')
                {
                    ;
                }
                else if (*p == 't')
                {
                    *p = '\t';                   // tab
                }
                else if (*p == 'r')
                {
                    *p = '\r';                   // carriage return
                }
                else if (*p == 'n')
                {
                    *p = '\n';                   // new line
                }
                else if (*p == 'v')
                {
                    *p = '\v';                   // vertical tab
                }
                else if (*p == 'f')
                {
                    *p = '\f';                   // form feed
                }
                else
                {
                    *(p + 1) = *p;
                    *p = '\\';
                    ++p;
                }
            }
            else if (*p == '\"')
            {
                *p = '\0';
                while (++p != q)
                {
                    *p = '\0';
                }
                return;
            }
        }

        // we ran out of space, need to bleed the buffer
        char tch1, last = str[sz-1];
        while (strm)
        {
            strm.get(tch1);
            if ((tch1 == '\"') && (last != '\\'))
            {
                break;
            }
            last = tch1;
        }
    }
    else
    {
        strm.get(str + 1, sz, ' ');

        // bleed the buffer for non-quoted
        char discard;
        do
        {
            strm.get(discard);
            if (strm.eof())
            {
                return;
            }
        }
        while (discard != ' ');
    }
}

void string_stream_helper_write(ostream& strm, const char* str, int sz)
{
    if ((str == NULL) || (sz == 0))
    {
        return;
    }
    // check if it is a null string
    if (str[0] == 0)
    {
        strm << "\"\"";
        return;
    }
    // check if it has spaces
    const char* p = str, * q = p + sz;
    for (; (*p != 0) && (p != q); ++p)
    {
        if (isspace(*p) || str[0] == '"')   // need to escape if 1st char is "
        {
            strm << '\"';
            for (p = str; (*p != 0) && (p != q); ++p)
            {
                if (*p == '\"')
                {
                    strm << "\\\"";
                }
                else if (*p == '\\')
                {
                    strm << "\\\\";
                }
                else if (*p == '\t')
                {
                    strm << "\\t";                    // tab
                }
                else if (*p == '\r')
                {
                    strm << "\\r";                    // carriage return
                }
                else if (*p == '\n')
                {
                    strm << "\\n";                    // new line
                }
                else if (*p == '\v')
                {
                    strm << "\\v";                    // vertical tab
                }
                else if (*p == '\f')
                {
                    strm << "\\f";                    // form feed
                }
                else
                {
                    strm << *p;
                }
            }
            strm << "\"";
            return;
        }
    }
    strm << str;
}

size_t helper_strlen(const char* str, size_t len)
{
    size_t cnt;
    for (cnt = 0; cnt < len; ++cnt)
    {
        if (str[cnt] == '\0')
        {
            return cnt + 1;
        }
    }
    return len;
}
/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "delyshow.h"
#include "mpt_var.h"
#include <time/timecore.h>

void delay_show::getTime(unsigned int& _sec, unsigned int& _usec)
{
    struct timeval tv_now;
    now(&tv_now);
    _sec = tv_now.tv_sec;
    _usec = tv_now.tv_usec;
}

delay_show::delay_show(unsigned int _sec, unsigned int _usec) :
    sec(_sec), usec(_usec), trigger_sec(0), trigger_usec(0)
{
}

void delay_show::setDirty(mpt_var*)
{
    if (!trigger_sec && !trigger_usec)
    {
        getTime(trigger_sec, trigger_usec);
    }
}

bool delay_show::getDirty()
{
    bool dir = false;
    if (trigger_sec || trigger_usec)
    {
        // Compute time to set dirty
        unsigned int tsec = trigger_sec + sec;
        unsigned int tusec = trigger_usec + usec;
        if (tusec >= 1000000)
        {
            ++tsec;
            tusec -= 1000000;
        }

        // Get current time
        unsigned int _sec = 0, _usec = 0;
        getTime(_sec, _usec);

        // Check
        if ((_sec > tsec) ||
            ((_sec == tsec) && (_usec > (tusec))))
        {
            trigger_sec = trigger_usec = 0;
            dir = true;
        }
    }
    return dir;
}

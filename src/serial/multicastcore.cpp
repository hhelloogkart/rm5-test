/******************************************************************/
/* Copyright DSO National Laboratories 2014. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "multicastcore.h"
#include <stdlib.h>

#if defined(WIN32)       /* Windows */
#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>
#include <ws2tcpip.h>
#include <mswsock.h>
#include <iphlpapi.h>
typedef int socklen_t;
#else                   /* UNIX */
#ifdef __CYGWIN__       /* cygwin */
typedef int socklen_t;
#endif

#if defined(__osf__) || defined(__ia64)    /* Dec Alpha OSF4.0 or IA64 */
#include <stropts.h>
typedef int socklen_t;
#endif

#if defined(_HPUX_SOURCE) && !defined(__ia64) /* HP-UX 10.20 */
typedef int socklen_t;
#endif

#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define closesocket ::close
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#ifdef __ANDROID_API__
# if __ANDROID_API__ >= 24
#include <ifaddrs.h>
# else
extern "C" int getifaddrs(struct ifaddrs** ifap);
# endif
#endif // __ANDROID_API__
#endif

#ifndef _WIN32
typedef int SOCKET;
#endif

struct multicast_core_private
{
    SOCKET sock;
    bool bound;
    struct sockaddr_in baddr;
};

multicast_core::multicast_core() : prvt(new multicast_core_private)
{
    prvt->sock = INVALID_SOCKET;
    prvt->bound = false;

#if defined(WIN32)
    WORD wVersionRequested;
    WSADATA wsaData;
    int err;

    // Request it support at least version 2.0
    wVersionRequested = MAKEWORD( 2, 0 );

    err = WSAStartup( wVersionRequested, &wsaData );
    if ( err != 0 )   // zero == successful
    {
        return;
    }

    if ( LOBYTE( wsaData.wVersion ) != 2 || HIBYTE( wsaData.wVersion ) != 0 )
    {
        // Cannot find suitable winsock.dll
        WSACleanup( );
        return;
    }
#endif
}

multicast_core::multicast_core(const multicast_core& other)
    : prvt(new multicast_core_private)
{
    prvt->sock = other.prvt->sock;
    prvt->bound = other.prvt->bound;
}

multicast_core::~multicast_core()
{
    close();
    delete prvt;
}

bool multicast_core::isvalid() const
{
    return prvt->sock != INVALID_SOCKET;
}

int multicast_core::get_sock() const
{
    return prvt->sock;
}

int multicast_core::recv(char* buf, size_t len, struct sockaddr_in* rxaddr)
{
    if (isvalid())
    {
        int ret;
        struct sockaddr_in _rxaddr;
        socklen_t rxsocksz = sizeof(struct sockaddr_in);
        ret = ::recvfrom(prvt->sock, buf, static_cast<int>(len), 0, (sockaddr*)&_rxaddr, &rxsocksz);
        if (ret == SOCKET_ERROR)
        {
#ifdef WIN32
            if (WSAGetLastError() == WSAEWOULDBLOCK)
            {
                ret = 0;
            }
            if ((WSAGetLastError() == WSAEHOSTUNREACH) ||
                (WSAGetLastError() == WSAECONNABORTED) ||
                (WSAGetLastError() == WSAECONNRESET))
            {
                ret = SOCKET_ERROR;
            }
#else
            if (errno == EWOULDBLOCK)
            {
                ret = 0;
            }
#endif
        }
        else if ((rxsocksz == sizeof(struct sockaddr_in)) && rxaddr)
        {
            memcpy(rxaddr, &_rxaddr, rxsocksz);
        }
        return ret;
    }
    return SOCKET_ERROR;
}

int multicast_core::select(const struct timeval* stm)
{
    if (prvt->sock == INVALID_SOCKET)
    {
        return -1;
    }
    struct timeval def = {
        0, 200000
    };
    if (stm != (const struct timeval*)0)
    {
        def = *stm;
    }

    fd_set rd_mask;
    FD_ZERO(&rd_mask);
    FD_SET(prvt->sock, &rd_mask);
    return ::select(prvt->sock + 1, &rd_mask, 0, 0, &def);
}

bool multicast_core::close()
{
    if (isvalid())
    {
        closesocket(prvt->sock);
        prvt->sock = INVALID_SOCKET;
        prvt->bound = false;
    }
    return true;
}

bool multicast_core::bind(const char* host, int port)
{
    // create a new socket if needed
    if (!isvalid())
    {
#ifdef WIN32
        prvt->sock = (int)::WSASocket(AF_INET, SOCK_DGRAM, IPPROTO_UDP, 0, 0, 0);
#else
        prvt->sock = socket(AF_INET, SOCK_DGRAM, 17); /* 17 is for udp */
#endif
        int opt = 1;
        setsockopt(prvt->sock, SOL_SOCKET, SO_REUSEADDR, (const char*)&opt, sizeof(opt));
        setsockopt(prvt->sock, SOL_SOCKET, SO_BROADCAST, (const char*)&opt, sizeof(opt));
    }

    // bind to a local socket if needed
    if (host[0] == '@')
    {
        // @0 - 1st network card, @1 - 2nd network card, etc.
        if (prvt->bound)
        {
            return false;
        }
        const int arg = atoi(host+1);
        int interf = 0;
#ifdef WIN32
        DWORD adapter_addresses_buffer_size = 32*1024;
        IP_ADAPTER_ADDRESSES* adapter, * adapter_addresses=(IP_ADAPTER_ADDRESSES*)malloc(adapter_addresses_buffer_size);
        DWORD error = GetAdaptersAddresses(AF_INET,
                                           GAA_FLAG_SKIP_ANYCAST | GAA_FLAG_SKIP_MULTICAST | GAA_FLAG_SKIP_DNS_SERVER | GAA_FLAG_SKIP_FRIENDLY_NAME,
                                           0, adapter_addresses, &adapter_addresses_buffer_size);

        if (ERROR_SUCCESS == error)
        {
            for (adapter = adapter_addresses; 0 != adapter; adapter = adapter->Next)
            {
                if (adapter->IfType == IF_TYPE_SOFTWARE_LOOPBACK)
                {
                    continue;
                }

                for (IP_ADAPTER_UNICAST_ADDRESS* address = adapter->FirstUnicastAddress; address!=0; address=address->Next)
                {
                    if (interf == arg)
                    {
                        memcpy(&prvt->baddr, address->Address.lpSockaddr, sizeof(SOCKADDR_IN));
                        prvt->baddr.sin_port = htons(port);
                        prvt->bound = (::bind(prvt->sock, (PSOCKADDR)&prvt->baddr, sizeof(SOCKADDR_IN)) != -1);
                        return prvt->bound;
                    }
                    else
                    {
                        ++interf;
                    }
                }
            }
        }

        free(adapter_addresses);
#else
        struct ifaddrs* ifap, * ifa;

        getifaddrs(&ifap);
        for (ifa = ifap; ifa; ifa=ifa->ifa_next)
        {
            if (ifa->ifa_addr->sa_family==AF_INET)
            {
                if (interf == arg)
                {
                    memcpy(&prvt->baddr, ifa->ifa_addr, sizeof(struct sockaddr_in));
                    prvt->baddr.sin_port = htons(port);
                    prvt->bound = (::bind(prvt->sock, (const struct sockaddr*)&prvt->baddr, sizeof(struct sockaddr_in)) == 0);
                    return prvt->bound;
                }
                else
                {
                    ++interf;
                }
            }
        }
#endif
    }
    else
    {
        // get the first part of the IP4 address
        int addr_i = 0;
        for (int cnt=0; (cnt<3) && (host[cnt] >= '0') && (host[cnt] <= '9'); ++cnt)
        {
            addr_i = (addr_i * 10) + static_cast<int>(host[cnt] - '0');
        }

        // check for multicast address
        if ((addr_i >= 224) && (addr_i <= 239))
        {
            if (!prvt->bound)
            {
                // bind to any
                memset((char*)&prvt->baddr, 0, sizeof(struct sockaddr_in));
                prvt->baddr.sin_family = AF_INET;
                prvt->baddr.sin_addr.s_addr = INADDR_ANY;
                prvt->baddr.sin_port = htons(port);
                if (::bind(prvt->sock, (const struct sockaddr*)&prvt->baddr, sizeof(struct sockaddr_in)) != 0)
                {
                    return false;
                }
                prvt->bound = true;
            }
            // do a multicast join
            struct ip_mreq mreq;
            memset((char*)&mreq, 0, sizeof(mreq));
            mreq.imr_multiaddr.s_addr = inet_addr(host);
            mreq.imr_interface.s_addr = prvt->baddr.sin_addr.s_addr;
            return setsockopt(prvt->sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, (const char*)&mreq, sizeof(mreq)) != -1;
        }
        else if (!prvt->bound)
        {
            // do a bind
            memset((char*)&prvt->baddr, 0, sizeof(struct sockaddr_in));
            prvt->baddr.sin_family = AF_INET;
            prvt->baddr.sin_addr.s_addr = inet_addr(host);
            prvt->baddr.sin_port = htons(port);
            prvt->bound = (::bind(prvt->sock, (const struct sockaddr*)&prvt->baddr, sizeof(struct sockaddr_in)) == 0);
            return prvt->bound;
        }
    }
    return false;
}

int multicast_core::send(const char* buf, size_t len, const char* host, int port)
{
    if (!isvalid() || (host[0] == '@'))
    {
        return -1;
    }
    // get the first part of the IP4 address
    int addr_i = 0;
    for (int cnt=0; (cnt<3) && (host[cnt] >= '0') && (host[cnt] <= '9'); ++cnt)
    {
        addr_i = (addr_i * 10) + static_cast<int>(host[cnt] - '0');
    }

    // check for multicast address
    if ((addr_i >= 224) && (addr_i <= 239))
    {
        struct sockaddr_in txaddr;
        memset((char*)&txaddr, 0, sizeof(struct sockaddr_in));
        txaddr.sin_family = AF_INET;
        txaddr.sin_addr.s_addr = inet_addr(host);
        txaddr.sin_port = htons(port);
        return sendto(prvt->sock, buf, static_cast<int>(len), 0, (const sockaddr*)&txaddr, sizeof(struct sockaddr_in));
    }
    else
    {
        return -1;
    }
}

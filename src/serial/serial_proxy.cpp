#include "serial_proxy.h"
#include <util/proccore.h>
#include <string.h>

inline int find_protocol_type(const char* host, char* protocol)
{
    for (int idx=0; (idx<8) && (host[idx]!='\0'); ++idx)
    {
        if (host[idx]==':')
        {
            protocol[idx] = '\0';
            return idx+1;
        }
        else
        {
            protocol[idx] = host[idx];
        }
    }
    return 0;
}

#if !(defined(NO_TCP_PROTOCOL) && defined(NO_TCPSVR_PROTOCOL) && defined(NO_MCAST_PROTOCOL))
#ifndef _WIN32
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif
inline void internal_set_nagle(int sock, bool enable)
{
    char enb = static_cast<char>(enable);
    setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, &enb, 1);
}
#endif

serial_proxy::~serial_proxy()
{
}

void serial_proxy::setNagleAlgo(bool)
{
}

serial_proxy* serial_proxy::sethost(const char* host, int port)
{
    char prot[8];
    const int offset = find_protocol_type(host, prot);
#ifndef NO_SERIAL_PROTOCOL
    if ((strcmp("serial", prot)==0) || (offset==0))
    {
        serial_proxy* prox = new serial_proxy_serial;
        prox->internal_set_host(host+offset, port);
        return prox;
    }
#endif
#ifndef NO_SW_PROTOCOL
    if ((strcmp("sw", prot)==0) || (offset==0))
    {
        serial_proxy* prox = new serial_proxy_sw;
        prox->internal_set_host(host+offset, port);
        return prox;
    }
#endif
#ifndef NO_TCP_PROTOCOL
    if ((strcmp("tcp", prot)==0) || (offset==0))
    {
        serial_proxy* prox = new serial_proxy_tcp;
        prox->internal_set_host(host+offset, port);
        return prox;
    }
#endif
#ifndef NO_TCPSVR_PROTOCOL
    if ((strcmp("tcpsvr", prot)==0) || (offset==0))
    {
        serial_proxy* prox = new serial_proxy_tcpsvr;
        prox->internal_set_host(host+offset, port);
        return prox;
    }
#endif
#ifndef NO_MCAST_PROTOCOL
    if ((strcmp("mcast", prot)==0) || (offset==0))
    {
        serial_proxy* prox = new serial_proxy_mcast;
        prox->internal_set_host(host+offset, port);
        return prox;
    }
    if ((strcmp("scast", prot)==0) || (offset==0))
    {
        serial_proxy* prox = new serial_proxy_mcast(true);
        prox->internal_set_host(host+offset, port);
        return prox;
    }
#ifndef NO_ATP_PROTOCOL
    if ((strcmp("atp", prot)==0) || (offset==0))
    {
        serial_proxy* prox = new serial_proxy_mcast;
        prox->internal_set_host(host+offset, port);
        return prox;
    }
#endif
#endif
    return 0;
}

#ifndef NO_SERIAL_PROTOCOL
int serial_proxy_serial::send(const char* msg, size_t len)
{
    return sock.send(msg, len);
}

int serial_proxy_serial::recv(char* buf, size_t len)
{
    return sock.recv(buf, len);
}

void serial_proxy_serial::internal_set_host(const char* host, int port)
{
    sock.sethost(host, port);
}

int serial_proxy_serial::select(const struct timeval* stm)
{
    return sock.select(stm);
}

int serial_proxy_serial::connect()
{
    return sock.connect();
}

bool serial_proxy_serial::isblock()
{
    return false;
}

int serial_proxy_serial::get_sock() const
{
    return sock.get_sock();
}

void serial_proxy_serial::close()
{
    sock.close();
}
#endif

#ifndef NO_SW_PROTOCOL
int serial_proxy_sw::send(const char* msg, size_t len)
{
    return sock.send(msg, len);
}

int serial_proxy_sw::recv(char* buf, size_t len)
{
    return sock.recv(buf, len);
}

void serial_proxy_sw::internal_set_host(const char* host, int port)
{
    sock.sethost(host, port);
    sock.connect(0);
}

int serial_proxy_sw::select(const struct timeval* stm)
{
    return sock.select(stm);
}

int serial_proxy_sw::connect()
{
    return sock.connect();
}

bool serial_proxy_sw::isblock()
{
    return false;
}

int serial_proxy_sw::get_sock() const
{
    return sock.get_sock();
}

void serial_proxy_sw::close()
{
    sock.close();
}
#endif

#ifndef NO_TCP_PROTOCOL
int serial_proxy_tcp::send(const char* msg, size_t len)
{
    if (!sock.connected())
    {
        if (sock.connect(1) != 1)
        {
            return 0;
        }
        internal_set_nagle(sock.get_sock(), false);
    }

    return sock.send(msg, len);
}

int serial_proxy_tcp::recv(char* buf, size_t len)
{
    return sock.recv(buf, len);
}

void serial_proxy_tcp::internal_set_host(const char* host, int port)
{
    sock.sethost(host, port);
}

int serial_proxy_tcp::select(const struct timeval* stm)
{
    if (!sock.connected())
    {
        if (sock.connect(1)!=1)
        {
            return 0;
        }
        internal_set_nagle(sock.get_sock(), false);
    }

    const int ret = sock.select(stm);
    if (ret < 0)
    {
        sock.close();
        sock.reset();
        return 0;
    }
    return ret;
}

int serial_proxy_tcp::connect()
{
    return 0;
}

bool serial_proxy_tcp::isblock()
{
    return false;
}
void serial_proxy_tcp::setNagleAlgo(bool enable)
{
    if (sock.connected())
    {
        internal_set_nagle(sock.get_sock(), enable);
    }
}

int serial_proxy_tcp::get_sock() const
{
    return -1;
}

void serial_proxy_tcp::close()
{
    sock.close();
}
#endif

#ifndef NO_TCPSVR_PROTOCOL
serial_proxy_tcpsvr::serial_proxy_tcpsvr() : next(socklist.end()), list_lock(0)
{
}

int serial_proxy_tcpsvr::send(const char* msg, size_t len)
{
    // wait for the list to be available
    while (!atomic_tas(&list_lock))
    {
        context_yield();
    }

    struct timeval stm = {
        0, 0
    };
    if (sock.select(&stm) > 0)
    {
        socklist.push_back(sock.accept());
        internal_set_nagle(socklist.back().get_sock(), false);
    }
    for (socklist_typ::iterator iter=socklist.begin(); iter!=socklist.end(); ++iter)
    {
        (*iter).send(msg, len);
    }
    atomic_untas(&list_lock);
    return static_cast<int>(len);
}

int serial_proxy_tcpsvr::recv(char* buf, size_t len)
{
    return (next != socklist.end()) ? (*next).recv(buf, len) : 0;
}

void serial_proxy_tcpsvr::internal_set_host(const char* host, int port)
{
    sock.sethost(host, port);
    sock.connect(0);
}

int serial_proxy_tcpsvr::select(const struct timeval* stm)
{
    if (next == socklist.end())
    {
        next = socklist.begin();
    }
    else
    {
        ++next;
    }

    if (next == socklist.end())
    {
        if (sock.select(stm) > 0)
        {
            // wait for the list to be available
            while (!atomic_tas(&list_lock))
            {
                context_yield();
            }

            socklist.push_back(sock.accept());
            internal_set_nagle(socklist.back().get_sock(), false);

            atomic_untas(&list_lock);
        }
        return 0;
    }
    else
    {
        int ret=(*next).select(stm);
        if (ret==-1)
        {
            // wait for the list to be available
            while (!atomic_tas(&list_lock))
            {
                context_yield();
            }

            (*next).close();
            next = socklist.erase(next);

            atomic_untas(&list_lock);
        }
        return ret;
    }
}

int serial_proxy_tcpsvr::connect()
{
    int ret = sock.listen(5);
    if (ret == 0)
    {
        ret = 1;
    }
    return ret;
}

bool serial_proxy_tcpsvr::isblock()
{
    return false;
}

int serial_proxy_tcpsvr::get_sock() const
{
    return -1; // cannot be multiplex because it may have multiple sockets
}

void serial_proxy_tcpsvr::close()
{
    sock.close();
}

void serial_proxy_tcpsvr::setNagleAlgo(bool enable)
{
    for (socklist_typ::iterator iter=socklist.begin(); iter!=socklist.end(); ++iter)
    {
        internal_set_nagle((*iter).get_sock(), enable);
    }
}
#endif

#ifndef NO_MCAST_PROTOCOL

serial_proxy_mcast::serial_proxy_mcast(bool stream) : block(!stream)
{
}

int serial_proxy_mcast::send(const char* buf, size_t len)
{
    int ret = -1;
    if (mcast_sock.isvalid() || connect() == 1)
    {
        hostlist_typ::iterator it;
        for (it = hosts.begin(); it != hosts.end(); ++it)
        {
            int r = mcast_sock.send(buf, len, (*it).c_str(), port);
            if (r > ret)
            {
                ret = r;
            }
        }
    }
    return ret;
}

int serial_proxy_mcast::recv(char* buf, size_t len)
{
    return mcast_sock.recv(buf, len);
}

void serial_proxy_mcast::internal_set_host(const char* host, int port)
{
    const char* h = host;

    hosts.clear();
    while (*h != '\0')
    {
        if (*(h++) == ';')
        {
            hosts.push_back(std::string(host, h - host - 1));
            host = h;
        }
    }
    hosts.push_back(std::string(host));
    serial_proxy_mcast::port = port;
}

int serial_proxy_mcast::select(const struct timeval* stm)
{
    if (mcast_sock.isvalid() || connect() == 1)
    {
        const int ret = mcast_sock.select(stm);
        if (ret < 0)
        {
            mcast_sock.close();
        }
        return ret;
    }
    return -1;
}

int serial_proxy_mcast::connect()
{
    hostlist_typ::iterator it;
    for (it = hosts.begin(); it != hosts.end(); ++it)
    {
        mcast_sock.bind((*it).c_str(), port);
    }
    return mcast_sock.isvalid() ? 1 : -1;
}

bool serial_proxy_mcast::isblock()
{
    return block;
}

int serial_proxy_mcast::get_sock() const
{
    return mcast_sock.get_sock();
}

void serial_proxy_mcast::close()
{
    mcast_sock.close();
}

#ifndef NO_ATP_PROTOCOL

serial_proxy_atp::serial_proxy_atp() : seq_tx(0)
{
}

int serial_proxy_atp::send(const char* buf, size_t len)
{
    char* buf2 = new char[len + 5];
    buf2[1] = (char)(seq_tx >> 24);
    buf2[2] = (char)(seq_tx >> 16);
    buf2[3] = (char)(seq_tx >> 8);
    buf2[4] = (char)(seq_tx & 0xff);
    memcpy(buf2 + 5, buf, len);

    int ret = -1;
    hostlist_typ::iterator it;
    for (it = hosts.begin(); it != hosts.end(); ++it)
    {
        unsigned long addr = inet_addr((*it).c_str());
        buf2[0] = (char)((ntohl(addr) & 0xff) - 100);
        int r=mcast_sock.send(buf2, len, (*it).c_str(), port);
        if (r > ret)
        {
            ret = r;
        }
    }
    ++seq_tx;
    delete[] buf2;
    return ret;
}

int serial_proxy_atp::recv(char* buf, size_t len)
{
    int sz = mcast_sock.recv(buf, len);
    if(sz > 5)
    {
        memmove(buf, buf + 5, sz -= 5);
        return sz;
    }
    else if(sz >= 0)
    {
        return 0;
    }
    return sz;
}
#endif /* NO_ATP_PROTOCOL */

#endif /* NO_MCAST_PROTOCOL */

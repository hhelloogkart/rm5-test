#include "serial_output_var.h"
#include <outbuf.h>
#include <string.h>
#include <stdio.h>
#include "serial_proxy.h"
#include "serial_interf.h"

const int SERIAL_OUTPUT_BUF = 2048;

serial_output_var::serial_output_var(const char* hdr, unsigned short hdrlen, unsigned short fixmsglen,
                                     unsigned short msglenoffset, unsigned short msglenwidth, unsigned short msglenbias,
                                     const char* trailer, unsigned short trailerlen, unsigned short msgoffset, int interf_num, bool dle_flg) :
    m_fixmsglen(fixmsglen),
    m_msglenoffset(msglenoffset),
    m_msglenwidth(msglenwidth),
    m_msglenbias(msglenbias),
    m_msgoffset(msgoffset),
    m_dle_flag(dle_flg),
    dirty(false),
    proxy(0),
    proxy_owner(false)
{
    // Header
    if (hdr)
    {
        if (hdrlen<=0)
        {
            hdrlen = static_cast<unsigned short>(strlen(hdr));
        }
        if (hdrlen>0)
        {
            char* buf = new char[hdrlen];
            memcpy(buf, hdr, hdrlen);
            m_hdr = buf;
            m_hdrlen = hdrlen;
        }
        else
        {
            m_hdr = 0;
            m_hdrlen = 0;
        }
    }
    else
    {
        m_hdr = 0;
        m_hdrlen = 0;
    }
    // Trailer
    if (trailer)
    {
        if (trailerlen<=0)
        {
            trailerlen = static_cast<unsigned short>(strlen(trailer));
        }
        if (trailerlen>0)
        {
            char* buf = new char[trailerlen];
            memcpy(buf, trailer, trailerlen);
            m_trailer = buf;
            m_trailerlen = trailerlen;
        }
        else
        {
            m_trailer = 0;
            m_trailerlen = 0;
        }
    }
    else
    {
        m_trailer = 0;
        m_trailerlen = 0;
    }
    serial_interf_typ::addOutputMessage(this, interf_num);
}

serial_output_var::~serial_output_var(void)
{
    delete[] m_hdr;
    delete[] m_trailer;
    if (proxy_owner)
    {
        delete proxy;
    }
}

void serial_output_var::setProxy(serial_proxy* p, bool owner)
{
    proxy = p;
    proxy_owner = owner;
}

void serial_output_var::refreshRM()
{
    if (dirty && proxy)
    {
        char tmpbuf2[SERIAL_OUTPUT_BUF];
        outbuf strm;
        strm.set((unsigned char*)&tmpbuf2[m_msgoffset], SERIAL_OUTPUT_BUF);
        const int sz = size();

        // output the contents
        complex_var::output(strm);
        // write header
        if (m_hdr)
        {
            memcpy(&tmpbuf2[0], m_hdr, m_hdrlen);
        }

        // write msg size
        const int totalsz = sz + m_msgoffset + m_trailerlen;
        if (m_msglenwidth)
        {
            unsigned int len = totalsz - m_msglenbias;
            if (m_msglenwidth > 0)
            {
                for (int idx = 0; idx < m_msglenwidth; ++idx)
                {
                    tmpbuf2[m_msglenoffset + idx] = (len % 256);
                    len /= 256;
                }
            }
            else
            {
                for (int idx=0; idx>m_msglenwidth; --idx)
                {
                    tmpbuf2[m_msglenoffset - m_msglenwidth - 1 + idx] = (len % 256);
                    len /= 256;
                }
            }
        }
        // write trailer
        if (m_trailer)
        {
            memcpy(&tmpbuf2[0] + m_msgoffset + sz, m_trailer, m_trailerlen);
        }

        // dle protocol
        if (m_dle_flag)
        {
            // count dle chars
            unsigned int dle_count = 0;
            char* srcptr = tmpbuf2 + m_hdrlen;
            char* dstptr = tmpbuf2 + totalsz - m_trailerlen;

            for (; srcptr < dstptr; ++srcptr)
            {
                if (*srcptr == 0x10)
                {
                    ++dle_count; // counts DLE
                }
            }

            if (dle_count > 0)
            {
                srcptr = tmpbuf2 + totalsz;
                dstptr = tmpbuf2 + totalsz + dle_count;

                // copy trailer
                for (int i = 0; i < m_trailerlen; ++i)
                {
                    *(--dstptr) = *(--srcptr);
                }

                // apply dle char copying
                while (srcptr < dstptr)
                {
                    *(--dstptr) = *(--srcptr);
                    if (*srcptr == 0x10)
                    {
                        *(--dstptr) = 0x10;
                    }
                }
            }
            dirty = (send(tmpbuf2, totalsz + dle_count) != totalsz + dle_count);
        }
        else
        {
            dirty = (send(tmpbuf2, totalsz) != totalsz);
        }
    }
}

int serial_output_var::send(const char* buf, int len)
{
    return proxy->send(buf, len);
}

const mpt_var& serial_output_var::operator=(const mpt_var& right)
{
    const serial_output_var* gen1 = RECAST(const serial_output_var*,&right);
    if (gen1)
    {
        operator=(*gen1);
    }
    else
    {
        const complex_var* gen2 = RECAST(const complex_var*, &right);
        if (gen2)
        {
            complex_var::operator=(*gen2);
        }
    }
    return *this;
}

const serial_output_var& serial_output_var::operator=(const serial_output_var& right)
{
    delete[] m_hdr;
    if (right.m_hdrlen)
    {
        char* buf = new char[right.m_hdrlen];
        memcpy(buf, right.m_hdr, right.m_hdrlen);
        m_hdr = buf;
        m_hdrlen = right.m_hdrlen;
    }
    else
    {
        m_hdr = 0;
        m_hdrlen = 0;
    }
    delete[] m_trailer;
    if (right.m_trailerlen)
    {
        char* buf = new char[right.m_trailerlen];
        memcpy(buf, right.m_trailer, right.m_trailerlen);
        m_trailer = buf;
        m_trailerlen = right.m_trailerlen;
    }
    else
    {
        m_trailer = 0;
        m_trailerlen = 0;
    }
    m_fixmsglen = right.m_fixmsglen;
    m_msglenoffset = right.m_msglenoffset;
    m_msglenwidth = right.m_msglenwidth;
    m_msglenbias = right.m_msglenbias;
    m_dle_flag = right.m_dle_flag;
    dirty = false;
    proxy = right.proxy;
    return *this;
}

void serial_output_var::setRMDirty()
{
    dirty = true;
}

mpt_var* serial_output_var::getNext()
{
    return this+1;
}

istream& serial_output_var::operator>>(istream& s)
{
    delete[] m_hdr;
    delete[] m_trailer;

    char str[512];

    s >> m_hdrlen;
    if (m_hdrlen)
    {
        s >> str;
        const int sz = static_cast<int>(strlen(str));
        char* buf = new char[sz/2];
        memset(buf, 0, sz/2);
        for (int cnt=0; cnt<sz; ++cnt)
        {
            const char ch = str[cnt];
            if ((ch >= 'a') && (ch <= 'f'))
            {
                buf[cnt/2] += (ch - 'a' + '\xa') << (4 * (cnt%2));
            }
            else if ((ch >= '0') && (ch <= '9'))
            {
                buf[cnt/2] += (ch - '0') << (4 * (cnt%2));
            }
        }
        m_hdr = buf;
    }
    else
    {
        m_hdr = 0;
    }
    s >> m_fixmsglen;
    s >> m_msglenoffset >> m_msglenwidth >> m_msglenbias;
    s >> m_trailerlen;
    if (m_trailerlen)
    {
        s >> str;
        const int sz = static_cast<int>(strlen(str));
        char* buf = new char[sz/2];
        memset(buf, 0, sz/2);
        for (int cnt=0; cnt<sz; ++cnt)
        {
            const char ch = str[cnt];
            if ((ch >= 'a') && (ch <= 'f'))
            {
                buf[cnt/2] += (ch - 'a' + '\xa') << (4 * (cnt%2));
            }
            else if ((ch >= '0') && (ch <= '9'))
            {
                buf[cnt/2] += (ch - '0') << (4 * (cnt%2));
            }
        }
        m_trailer = buf;
    }
    else
    {
        m_trailer = 0;
    }
    s >> m_msgoffset;
    s >> m_dle_flag;

    return complex_var::operator>>(s);
}

ostream& serial_output_var::operator<<(ostream& s) const
{
    char str[512];

    s << m_hdrlen << ' ';
    if (m_hdrlen)
    {
        for (int cnt=0; cnt<m_hdrlen; ++cnt)
        {
            rm_sprintf(str+cnt*2, 512-cnt*2, "%02x", static_cast<int>(m_hdr[cnt]));
        }
        str[m_hdrlen*2] = '\0';
        s << str << ' ';
    }
    s << m_fixmsglen  << ' ';
    s << m_msglenoffset << ' ' << m_msglenwidth << ' ' << m_msglenbias << ' ';
    s << m_trailerlen << ' ';
    if (m_trailerlen)
    {
        for (int cnt=0; cnt<m_trailerlen; ++cnt)
        {
            rm_sprintf(str+cnt*2, 512-cnt*2, "%02x", static_cast<int>(m_trailer[cnt]));
        }
        str[m_trailerlen*2] = '\0';
        s << str << ' ';
    }
    s << m_msgoffset << ' ';
    s << m_dle_flag << ' ';
    return complex_var::operator<<(s);
}

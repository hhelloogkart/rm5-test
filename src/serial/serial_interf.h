/******************************************************************/
/* Copyright DSO National Laboratories 2010. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _SERIAL_INTERF_H_
#define _SERIAL_INTERF_H_

#include <comm/sercore.h>
#include <list>
#include "serial_proxy.h"

class serial_var;
class serial_output_var;
const int SERIAL_BUF = 1024*8; // shared buffer for input and output

class serial_interf_typ
{
private:
    // serial socket
    serial_proxy* sock;
    // receive buffer to read and store partial packets
    char buf[SERIAL_BUF];
    unsigned short buflen;
    // contains the common identifier for all serial packets from this interface, if any
    char* cmnhdr;
    unsigned short cmnhdrlen;
    // sorted array containing the unique identifier for the serial packet
    // headers should all have a fixed length, the size of the array is msghdrlen * varlen
    char* msghdr;
    unsigned short msghdrlen;
    // sorted array containing the unique mask for each serial packet
    // masks should all have a fixed length, the same size as hdrlen, the size of the array is msghdrlen * varlen
    // for non-masking logic hdrmask will be 0
    char* hdrmask;  ///< The hdrmask
    // hdrmask_output is only valid when hdrmask is not 0
    char* hdrmask_output;
    // array containing pointers to the serial_var, ordered relative to msghdr
    serial_var** var;
    unsigned short varlen;
    // buffer lock
    long buf_lock;
    // timeout in milliseconds to block on serial port
    unsigned int timeout_msec;

public:
    // create the object
    serial_interf_typ();
    // destroy the object
    ~serial_interf_typ();
    // close all interfaces
    static void close();
    // reconnect the ports after they have been closed
    static void reconnect();
    // checks for data from the serial ports
    static void incoming();
    // checks for dirty flag and outputs the var
    static void outgoing();
    // initiate each serial_interf, and registering messages as required
    static void init();
    // add a new message, only before init
    static void addMessage(const char* mask, const char* mask_output, const char* header, unsigned short hdrlen, serial_var* var, int interf = 0);
    // add a new output message, only before init
    static void addOutputMessage(serial_output_var* var, int interf = 0);
    // add a new interface parameter, only before init
    static void addInterface(const char* host, int port, unsigned int timeout=0, int interf = 0);
    // returns a portion of the buffer suitable for output, sz is size in bytes for the payload
    char* get_buffer(int sz);
    // write a buffer to the serial port, adding the header, trailer and length
    bool write_buffer(char* buf, int sz, int idx);
    // read data from serial port and route it to the correct variable
    bool read_data();

private:
    // locates the serial var to a matching var from the internal buffer, returns offset found
    short (serial_interf_typ::* find_var)(serial_var** varptr, char* buffer, unsigned short bufferlen);
    short find_var_1(serial_var** varptr, char* buffer, unsigned short bufferlen);
    short find_var_2(serial_var** varptr, char* buffer, unsigned short bufferlen);
    short find_var_3(serial_var** varptr, char* buffer, unsigned short bufferlen);  //find_var_2 with mask
    short find_var_4(serial_var** varptr, char* buffer, unsigned short bufferlen);  //single message, 0 header
};

#endif /* _SERIAL_INTERF_H_ */

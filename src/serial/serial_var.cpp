/******************************************************************/
/* Copyright DSO National Laboratories 2010. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include <string.h>
#include <outbuf.h>
#include <defaults.h>
#include <defutil.h>
#include <time/timecore.h>
#include <util/dbgout.h>
#include <list>
#include <serial_var.h>
#include "serial_interf.h"

static protocol_description& description()
{
    static protocol_description d = {
        1,
        &serial_var::initialize,
        &serial_var::incoming,
        &serial_var::outgoing,
        0
    };
    return d;
}

class serial_helper_trailer : public serial_helper_typ
{
public:
    serial_helper_trailer(const char* trailer, unsigned short trailerlen, unsigned short headerlen, unsigned short msgoffset);
    ~serial_helper_trailer();
    int calculateSize(int sz) const;
    int write(char* buf, int sz);
    int extract(char* buf, int left, serial_var* parent);
    static serial_helper_trailer* provideTrailerHelper(const char* trailer, unsigned short trailerlen, unsigned short headerlen, unsigned short msgoffset);
private:
    // contains the trailer for its parent
    char* trailer;
    unsigned short trailerlen;
    // headerlen used for length calculations
    unsigned short headerlen;
};

class serial_helper_fixedlen : public serial_helper_typ
{
public:
    serial_helper_fixedlen(int msglen, unsigned short msgoffset);
    int calculateSize(int sz) const;
    int write(char* buf, int sz);
    int extract(char* buf, int left, serial_var* parent);
    void attachHelper(serial_helper_trailer* helper);
    static serial_helper_fixedlen* provideFixedlenHelper(int msglen, unsigned short msgoffset);
private:
    int fixedlen;
    serial_helper_trailer* helper;
};

class serial_helper_findlen : public serial_helper_typ
{
public:
    serial_helper_findlen(unsigned short msglenoff, unsigned short msglenwid, unsigned short msglenbias, unsigned short msgoffset);
    int calculateSize(int sz) const;
    int write(char* buf, int sz);
    int extract(char* buf, int left, serial_var* parent);
    void attachHelper(serial_helper_trailer* helper);
    static serial_helper_findlen* provideFindlenHelper(unsigned short msglenoff, unsigned short msglenwid, unsigned short msglenbias, unsigned short msgoffset);
private:
    // getting the length of the packet
    unsigned short msglenoff;
    short msglenwid;
    short msglenbias;
    serial_helper_trailer* helper;
};

class serial_helper_dle : public serial_helper_typ
{
public:
    serial_helper_dle(const char* trailer, unsigned short trailerlen, unsigned short headerlen, unsigned short msgoffset);
    ~serial_helper_dle();
    int calculateSize(int sz) const;
    int write(char* buf, int sz);
    int extract(char* buf, int left, serial_var* parent);
    static serial_helper_dle* provideDleHelper(const char* trailer, unsigned short trailerlen, unsigned short headerlen, unsigned short msgoffset);
private:
    // contains the trailer for its parent
    // trailer must end with 0x1003 to comply with the dle protocol
    char* trailer;
    unsigned short trailerlen;
    // headerlen used for length calculations
    // header should begin with 0x1002 to comply with the dle protocol
    unsigned short headerlen;
};

typedef std::list<serial_helper_typ*> serial_helper_list_typ;
typedef std::list<serial_helper_fixedlen*> serial_fixedlen_list_typ;
typedef std::list<serial_helper_findlen*> serial_findlen_list_typ;
typedef std::list<serial_helper_trailer*> serial_trailer_list_typ;
typedef std::list<serial_helper_dle*> serial_dle_list_typ;

static bool helpersInitialized = false;
static serial_helper_list_typ* serial_helper_list = 0;
static serial_fixedlen_list_typ* serial_fixedlen_list = 0;
static serial_findlen_list_typ* serial_findlen_list = 0;
static serial_trailer_list_typ* serial_trailer_list = 0;
static serial_dle_list_typ* serial_dle_list = 0;


/* Base Helper */

serial_helper_typ::serial_helper_typ(unsigned short _msgoffset) : msgoffset(_msgoffset), attachers(1)
{

}

serial_helper_typ::~serial_helper_typ()
{
}

int serial_helper_typ::calculateSize(int sz) const
{
    return msgoffset + sz;
}

int serial_helper_typ::write(char* /*buf*/, int sz)
{
    return sz;
}

int serial_helper_typ::extract(char* buf, int left, serial_var* parent)
{
    if (parent->extract(left - msgoffset, reinterpret_cast<unsigned char*>(buf+msgoffset)) == 0)
    {
        return left;
    }
    else
    {
        return 1;
    }
}

serial_helper_typ* serial_helper_typ::provideHelper(unsigned short _msgoffset)
{
    if (helpersInitialized)
    {
        return 0;
    }

    if (serial_helper_list == 0)
    {
        serial_helper_list = new serial_helper_list_typ;
    }
    else
    {
        for (serial_helper_list_typ::iterator i = serial_helper_list->begin(); i != serial_helper_list->end(); i++)
        {
            if ((*i)->msgoffset == _msgoffset)
            {
                (*i)->attach();
                return (*i);
            }
        }
    }
    serial_helper_typ* helper = new serial_helper_typ(_msgoffset);
    serial_helper_list->push_back(helper);
    return helper;
}

void serial_helper_typ::initHelpers()
{
    helpersInitialized = true;
    delete serial_helper_list;
    delete serial_fixedlen_list;
    delete serial_findlen_list;
    delete serial_trailer_list;
    delete serial_dle_list;
}

unsigned short serial_helper_typ::msgOffset() const
{
    return msgoffset;
}

serial_helper_typ* serial_helper_typ::attach()
{
    attachers++;
    return this;
}

void serial_helper_typ::detach()
{
    if (--attachers <= 0)
    {
        delete this;
    }
}

/* Trailer Helper */

serial_helper_trailer::serial_helper_trailer(const char* _trailer, unsigned short _trailerlen, unsigned short _headerlen, unsigned short msgoffset)
    : serial_helper_typ(msgoffset), trailer(0), trailerlen(_trailerlen), headerlen(_headerlen)
{
    trailer = new char[trailerlen];
    memcpy(trailer, _trailer, trailerlen);
}

serial_helper_trailer::~serial_helper_trailer()
{
    delete[] trailer;
}

int serial_helper_trailer::calculateSize(int sz) const
{
    return msgoffset + trailerlen + sz;
}

int serial_helper_trailer::write(char* buf, int sz)
{
    memcpy(buf + sz - trailerlen, trailer, trailerlen);
    return sz;
}

int serial_helper_trailer::extract(char* buf, int left, serial_var* parent)
{
    // c. attempt to find the trailer
    for (int tridx = headerlen; left >= tridx + trailerlen; ++tridx)
    {
        if (memcmp(buf + tridx, trailer, trailerlen) == 0)
        {
            // found
            if (parent->extract(tridx - msgoffset, reinterpret_cast<unsigned char*>(buf + msgoffset)) == 0)
            {
                return tridx + trailerlen;
            }
            else
            {
                return 1;
            }
        }
    }

    // the packet is incomplete
    return -1;
}

serial_helper_trailer* serial_helper_trailer::provideTrailerHelper(const char* _trailer, unsigned short _trailerlen, unsigned short _headerlen, unsigned short _msgoffset)
{
    if (helpersInitialized)
    {
        return 0;
    }

    if (serial_trailer_list == 0)
    {
        serial_trailer_list = new serial_trailer_list_typ;
    }
    else
    {
        for (serial_trailer_list_typ::iterator i = serial_trailer_list->begin(); i != serial_trailer_list->end(); i++)
        {
            if (((*i)->msgoffset == _msgoffset) && ((*i)->trailerlen == _trailerlen) && !memcmp((*i)->trailer, _trailer, _trailerlen))
            {
                (*i)->attach();
                return (*i);
            }
        }
    }
    serial_helper_trailer* helper = new serial_helper_trailer(_trailer, _trailerlen, _headerlen, _msgoffset);
    serial_trailer_list->push_back(helper);
    return helper;
}

/* Fixed Len Helper */

serial_helper_fixedlen::serial_helper_fixedlen(int msglen, unsigned short msgoffset)
    : serial_helper_typ(msgoffset), fixedlen(msglen), helper(0)
{

}

int serial_helper_fixedlen::calculateSize(int sz) const
{
    return helper ? helper->calculateSize(sz) : msgoffset + sz;
}

int serial_helper_fixedlen::write(char* buf, int sz)
{
    return helper ? helper->write(buf, sz) : sz;
}

int serial_helper_fixedlen::extract(char* buf, int left, serial_var* parent)
{
    // b. use a fixed length
    if (left < fixedlen)
    {
        return -1; // the packet is incomplete

    }
    // pass the packet for processing
    if (parent->extract(fixedlen - msgoffset, reinterpret_cast<unsigned char*>(buf + msgoffset)) == 0)
    {
        return fixedlen; // advance the packet
    }
    else
    {
        return 1;
    }
}

void serial_helper_fixedlen::attachHelper(serial_helper_trailer* _helper)
{
    helper = _helper;
}

serial_helper_fixedlen* serial_helper_fixedlen::provideFixedlenHelper(int _msglen, unsigned short _msgoffset)
{
    if (helpersInitialized)
    {
        return 0;
    }

    if (serial_fixedlen_list == 0)
    {
        serial_fixedlen_list = new serial_fixedlen_list_typ;
    }
    else
    {
        for (serial_fixedlen_list_typ::iterator i = serial_fixedlen_list->begin(); i != serial_fixedlen_list->end(); i++)
        {
            if (((*i)->msgoffset == _msgoffset) && ((*i)->fixedlen == _msglen))
            {
                (*i)->attach();
                return (*i);
            }
        }
    }
    serial_helper_fixedlen* helper = new serial_helper_fixedlen(_msglen, _msgoffset);
    serial_fixedlen_list->push_back(helper);
    return helper;
}

/* Find Len Helper */

serial_helper_findlen::serial_helper_findlen(unsigned short _msglenoff, unsigned short _msglenwid, unsigned short _msglenbias, unsigned short msgoffset)
    : serial_helper_typ(msgoffset), msglenoff(_msglenoff), msglenwid(_msglenwid), msglenbias(_msglenbias), helper(0)
{

}

int serial_helper_findlen::calculateSize(int sz) const
{
    return helper ? helper->calculateSize(sz) : msgoffset + sz;
}

// \brief This function only writes the size requested
int serial_helper_findlen::write(char* buf, int sz)
{
    unsigned int len = sz - msglenbias;
    if (msglenwid > 0)
    {
        for (int idx = 0; idx < msglenwid; ++idx)
        {
            buf[msglenoff + idx] = (len % 256);
            len /= 256;
        }
    }
    else
    {
        for (int idx = 0; idx > msglenwid; --idx)
        {
            buf[msglenoff - msglenwid - 1 + idx] = (len % 256);
            len /= 256;
        }
    }

    return helper ? helper->write(buf, sz) : sz;
}

int serial_helper_findlen::extract(char* buf, int left, serial_var* parent)
{
    // a. attempt to extract length from data
    // check that there is enough to extract length
    if (left > msglenoff + abs(msglenwid))
    {
        // extract the length
        int len = 0;
        if (msglenwid < 0)
        {
            for (int idx = 0; idx < -msglenwid; ++idx)
            {
                len <<= 8;
                len += *reinterpret_cast<unsigned char*>(buf + msglenoff + idx);
            }
        }
        else
        {
            for (int idx = 1; idx <= msglenwid; ++idx)
            {
                len <<= 8;
                len += *reinterpret_cast<unsigned char*>(buf + msglenoff + msglenwid - idx);
            }
        }
        // apply bias
        len += msglenbias;

        // check for insane values
        if (len > SERIAL_BUF)
        {
            return 1;
        }

        // check that the entire packet is received
        if (left < len)
        {
            return -1; // the packet is incomplete

        }
        // pass the packet for processing
        if (parent->extract(len - msgoffset, reinterpret_cast<unsigned char*>(buf + msgoffset)) == 0)
        {
            return len;
        }
        else
        {
            return 1;
        }
    }
    else
    {
        return -1; // the packet is incomplete
    }
}

void serial_helper_findlen::attachHelper(serial_helper_trailer* _helper)
{
    helper = _helper;
}

serial_helper_findlen* serial_helper_findlen::provideFindlenHelper(unsigned short _msglenoff, unsigned short _msglenwid, unsigned short _msglenbias, unsigned short _msgoffset)
{
    if (helpersInitialized)
    {
        return 0;
    }

    if (serial_findlen_list == 0)
    {
        serial_findlen_list = new serial_findlen_list_typ;
    }
    else
    {
        for (serial_findlen_list_typ::iterator i = serial_findlen_list->begin(); i != serial_findlen_list->end(); i++)
        {
            if (((*i)->msgoffset == _msgoffset) && ((*i)->msglenoff == _msglenoff) && ((*i)->msglenwid == _msglenwid) && ((*i)->msglenbias == _msglenbias))
            {
                (*i)->attach();
                return (*i);
            }
        }
    }
    serial_helper_findlen* helper = new serial_helper_findlen(_msglenoff, _msglenwid, _msglenbias, _msgoffset);
    serial_findlen_list->push_back(helper);
    return helper;
}

/* DLE Helper */

serial_helper_dle::serial_helper_dle(const char* _trailer, unsigned short _trailerlen, unsigned short _headerlen, unsigned short msgoffset)
    : serial_helper_typ(msgoffset), trailerlen(_trailerlen), headerlen(_headerlen)
{
    trailer = new char[trailerlen];
    memcpy(trailer, _trailer, trailerlen);
}

serial_helper_dle::~serial_helper_dle()
{
    delete[] trailer;
}
int serial_helper_dle::calculateSize(int sz) const
{
    // request for size * 2 for the events where the entire message is 0x10
    return msgoffset + trailerlen + sz * 2;
}

static void applyDLE(char* srcptr, char* dstptr)
{
    while (srcptr < dstptr)
    {
        *(--dstptr) = *(--srcptr);
        if (*srcptr == 0x10)
        {
            *(--dstptr) = 0x10;
        }
    }
}

int serial_helper_dle::write(char* buf, int sz)
{
    const unsigned int originalsz = (sz - msgoffset - trailerlen) / 2;
    unsigned int payloadsz = originalsz + msgoffset;
    unsigned int dle_count = 0;

    char* srcptr = buf + headerlen;
    char* dstptr = buf + payloadsz;

    while (srcptr < dstptr)
    {
        if (*(srcptr++) == 0x10)
        {
            ++dle_count; // counts DLE
        }
    }

    if (dle_count > 0)
    {
        srcptr = buf + payloadsz;
        dstptr = buf + payloadsz + dle_count;

        applyDLE(srcptr, dstptr);

        payloadsz += dle_count;
    }

    memcpy(buf + payloadsz, trailer, trailerlen);
    return payloadsz + trailerlen;
}

static char* removeDLE(char* srcptr, char* endptr)
{
    char* dstptr = srcptr;
    while (srcptr < endptr)
    {
        if (*srcptr == 0x10)
        {
            ++srcptr;
        }
        *(dstptr++) = *(srcptr++);
    }
    return dstptr;
}

int serial_helper_dle::extract(char* buf, int left, serial_var* parent)
{
    bool found_dle = false;
    bool found_etx = false;

    // search for DLE/ETX
    char* scanptr = buf + headerlen;
    char* endptr = buf + left;
    while (scanptr < endptr)
    {
        if (!found_dle)
        {
            found_dle = (*(scanptr++) == 0x10);
        }
        else
        {
            if (*scanptr == 0x03)   // ETX found
            {
                found_etx = true;
                ++scanptr;
                break;
            }
            else if (*scanptr == 0x10)   // 0x10 found
            {
                found_dle = false;
                ++scanptr;
            }
            else
            {
                return 1; // unexpected char found
            }
        }
    }

    if (!found_etx) // packet is incomplete
    {
        return -1;
    }

    // rescan trailer in case a longer trailer was used
    if (memcmp(scanptr - trailerlen, trailer, trailerlen) == 0)
    {
        endptr = removeDLE(buf + headerlen, scanptr - trailerlen);
        if (parent->extract(static_cast<int>(endptr - buf) - msgoffset, reinterpret_cast<unsigned char*>(buf + msgoffset)) == 0)
        {
            return static_cast<int>(scanptr - buf);
        }
        else
        {
            applyDLE(scanptr - trailerlen, endptr);
            return 1;
        }
    }

    // the packet is incomplete
    return -1;
}

serial_helper_dle* serial_helper_dle::provideDleHelper(const char* _trailer, unsigned short _trailerlen, unsigned short _headerlen, unsigned short _msgoffset)
{
    if (helpersInitialized)
    {
        return 0;
    }

    if (serial_dle_list == 0)
    {
        serial_dle_list = new serial_dle_list_typ;
    }
    else
    {
        for (serial_dle_list_typ::iterator i = serial_dle_list->begin(); i != serial_dle_list->end(); i++)
        {
            if (((*i)->msgoffset == _msgoffset) && ((*i)->trailerlen == _trailerlen) && !memcmp((*i)->trailer, _trailer, _trailerlen))
            {
                (*i)->attach();
                return (*i);
            }
        }
    }
    serial_helper_dle* helper = new serial_helper_dle(_trailer, _trailerlen, _headerlen, _msgoffset);
    serial_dle_list->push_back(helper);
    return helper;
}

/**
    @param mask - value of mask for serial message header, mask size is hdrlen
    @param hdr - value of header of serial message, header size is hdrlen
    @param hdrlen - size of header in bytes
    @param fixmsglen - fixed message length from header to end of checksum in bytes, 0 if not-fixed
    @param msglenoffset - if not fixed length, byte offset to recover the length of packet, 0 if N/A
    @param msglenwidth - if not fixed length, number of bytes to recover the length of packet, 0 if N/A
    @param msglenbias - the bias to apply to extracted length to convert it to number of bytes in the entire packet, 0 if N/A
    @param msgoffset - number of bytes from the beginning of serial packet to offset for extraction by child objects
 */
serial_var::serial_var(const char* hdr, unsigned short hdrlen, unsigned short fixmsglen,
                       unsigned short msglenoffset, unsigned short msglenwidth, unsigned short msglenbias,
                       const char* trailer, unsigned short trailerlen, unsigned short msgoffset, int interf_num, bool ddle_flg, const char* mask, const char* mask_output)
    : msgidx(0), interf(0), helper(0), dirty(false)
{
    rmclient_init::install_protocol(description(), false);
    assistedConstruct(hdr, hdrlen, fixmsglen, msglenoffset, msglenwidth, msglenbias,
                      trailer, trailerlen, msgoffset, interf_num, ddle_flg, mask, mask_output);
}

serial_var::serial_var()
    : msgidx(0), interf(0), helper(0), dirty(false)
{
    rmclient_init::install_protocol(description(), false);
}

void serial_var::assistedConstruct(const char* hdr, unsigned short hdrlen, unsigned short fixmsglen,
                                   unsigned short msglenoffset, unsigned short msglenwidth, unsigned short msglenbias,
                                   const char* trailer, unsigned short trailerlen, unsigned short msgoffset, int interf_num, bool ddle_flg, const char* mask, const char* mask_output)
{
    serial_interf_typ::addMessage(mask, mask_output, hdr, hdrlen, this, interf_num);

    if (ddle_flg)
    {
        helper = serial_helper_dle::provideDleHelper(trailer, trailerlen, hdrlen, msgoffset);
    }
    else if (msglenwidth)
    {
        serial_helper_findlen* tmp = serial_helper_findlen::provideFindlenHelper(msglenoffset, msglenwidth, msglenbias, msgoffset);
        if (trailerlen)
        {
            tmp->attachHelper(serial_helper_trailer::provideTrailerHelper(trailer, trailerlen, hdrlen, msgoffset));
        }
        helper = tmp;
    }
    else if (fixmsglen)
    {
        serial_helper_fixedlen* tmp = serial_helper_fixedlen::provideFixedlenHelper(fixmsglen, msgoffset);
        if (trailerlen)
        {
            tmp->attachHelper(serial_helper_trailer::provideTrailerHelper(trailer, trailerlen, hdrlen, msgoffset));
        }
        helper = tmp;
    }
    else if (trailerlen)
    {
        helper = serial_helper_trailer::provideTrailerHelper(trailer, trailerlen, hdrlen, msgoffset);
    }
    else
    {
        helper = serial_helper_typ::provideHelper(msgoffset);
    }
}

serial_var::~serial_var()
{
    if (helper)
    {
        helper->detach();
    }
}

mpt_var* serial_var::getNext()
{
    return this + 1;
}

istream& serial_var::operator>>(istream& s)
{
    s >> msgidx;
    return complex_var::operator>>(s);
}

ostream& serial_var::operator<<(ostream& s) const
{
    s << msgidx << ' ';
    return complex_var::operator<<(s);
}

void serial_var::setRMDirty()
{
    dirty = true;
}

void serial_var::refreshRM()
{
    if (dirty && interf)
    {
        outbuf buf;
        int sz = helper->calculateSize(size()); // helper might add extra length here
        char* ptr = interf->get_buffer(sz);
        if (ptr)
        {
            buf.set(reinterpret_cast<unsigned char*>(ptr + helper->msgOffset()), sz - helper->msgOffset());
            output(buf);
            sz = helper->write(ptr, sz); // helper to do its work here
            dirty = !interf->write_buffer(ptr, sz, msgidx);
        }
    }
}

void serial_var::outgoing()
{
    static bool reentrant = false;
    if (!reentrant)
    {
        reentrant = true;
        serial_interf_typ::outgoing();
        reentrant = false;
    }
}

void serial_var::incoming()
{
    static bool reentrant = false;
    if (!reentrant)
    {
        reentrant = true;
        serial_interf_typ::incoming();
        reentrant = false;
    }
}

void serial_var::initialize(defaults* def)
{
    if (def != 0)
    {
        set_serial_param(def);
    }
    serial_interf_typ::init();
}

void serial_var::close()
{
    serial_interf_typ::close();
}

void serial_var::reconnect()
{
    serial_interf_typ::reconnect();
}

void serial_var::set_serial_param(const char* host, int port, unsigned int timeout, int interf_num)
{
    serial_interf_typ::addInterface(host, port, timeout, interf_num);
}

void serial_var::set_serial_param(defaults* def)
{
    if (def == 0)
    {
        return;
    }

    tree_node* tn1 = 0, * tn2 = 0, * tn3 = 0, * tn4 = 0;
    char host[128] = "COM1";
    int port = 9600;
    int sec = 0;
    int usec = 0;

    for (int interf_no = 0; (tn1 = def->getstring("serial_host", host, tn1)) != 0; ++interf_no)
    {
        if ((interf_no == 0) || (tn2 != 0))
        {
            tn2=def->getint("serial_port", &port, tn2);
        }

        if ((interf_no == 0) || (tn3 != 0))
        {
            tn3=def->getint("serial_timeout_sec", &sec, tn3);
        }

        if ((interf_no == 0) || (tn4 != 0))
        {
            tn4=def->getint("serial_timeout_usec", &usec, tn4);
        }

        set_serial_param(host, port, sec*1000 + usec / 1000, interf_no);
    }
}

int serial_var::assistedExtract(char* buf, int len)
{
    return helper->extract(buf, len, this);
}

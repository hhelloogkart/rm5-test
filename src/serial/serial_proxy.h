/******************************************************************/
/* Copyright DSO National Laboratories 2010. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _SERIAL_PROXY_H_
#define _SERIAL_PROXY_H_

#include <stddef.h>
#include <rmglobal.h>

class RM_EXPORT serial_proxy
{
public:
    virtual ~serial_proxy();
    virtual int  connect() = 0;
    virtual int  select(const struct timeval* stm) = 0;
    virtual int  send(const char* msg, size_t len) = 0;
    virtual int  recv(char* buf, size_t len) = 0;
    virtual bool isblock() = 0;
    virtual void setNagleAlgo(bool);
    virtual int  get_sock() const = 0;
    virtual void close() = 0;
    static serial_proxy* sethost(const char* host, int port);
protected:
    virtual void internal_set_host(const char* host, int port) = 0;
};

#ifndef NO_SW_PROTOCOL
#include <comm/swsercore.h>
#elif !defined(NO_SERIAL_PROTOCOL)
#include <comm/swsercore.h>
#endif

#ifndef NO_SERIAL_PROTOCOL
class serial_proxy_serial : public serial_proxy
{
public:
    int  connect();
    int  select(const struct timeval* stm);
    int  send(const char* msg, size_t len);
    int  recv(char* buf, size_t len);
    bool isblock();
    int  get_sock() const;
    void close();
protected:
    void internal_set_host(const char* host, int port);
private:
    serial_core sock;
};
#endif

#ifndef NO_SW_PROTOCOL
class serial_proxy_sw : public serial_proxy
{
public:
    int  connect();
    int  select(const struct timeval* stm);
    int  send(const char* msg, size_t len);
    int  recv(char* buf, size_t len);
    bool isblock();
    int  get_sock() const;
    void close();
protected:
    void internal_set_host(const char* host, int port);
private:
    sw_serial_core sock;
};
#endif

#if !(defined(NO_TCP_PROTOCOL) && defined(NO_TCPSVR_PROTOCOL))
#include <comm/tcpcore.h>
#endif

#ifndef NO_TCP_PROTOCOL
class serial_proxy_tcp : public serial_proxy
{
public:
    int  connect();
    int  select(const struct timeval* stm);
    int  send(const char* msg, size_t len);
    int  recv(char* buf, size_t len);
    bool isblock();
    void setNagleAlgo(bool);
    int  get_sock() const;
    void close();
protected:
    void internal_set_host(const char* host, int port);
private:
    tcp_core sock;
};
#endif

#ifndef NO_TCPSVR_PROTOCOL
#include <list>
class serial_proxy_tcpsvr : public serial_proxy
{
    typedef std::list<tcp_core> socklist_typ;
public:
    serial_proxy_tcpsvr();
    int  connect();
    int  select(const struct timeval* stm);
    int  send(const char* msg, size_t len);
    int  recv(char* buf, size_t len);
    bool isblock();
    void setNagleAlgo(bool);
    int  get_sock() const;
    void close();
protected:
    void internal_set_host(const char* host, int port);
private:
    tcp_core sock;
    socklist_typ socklist;
    socklist_typ::iterator next;
    long list_lock;
};
#endif

#ifndef NO_MCAST_PROTOCOL
#include <list>
#include <string>
#include "multicastcore.h"
#ifndef _WIN32
#include <netinet/in.h>
#endif

class serial_proxy_mcast : public serial_proxy
{
public:
    typedef std::list<std::string> hostlist_typ;
    serial_proxy_mcast(bool stream = false);
    int  connect();
    int  select(const struct timeval* stm);
    int  send(const char* msg, size_t len);
    int  recv(char* buf, size_t len);
    bool isblock();
    int  get_sock() const;
    void close();
protected:
    void internal_set_host(const char* host, int port);
    hostlist_typ hosts;
    int port;
    multicast_core mcast_sock;
    bool block;
};

#ifndef NO_ATP_PROTOCOL
class serial_proxy_atp : public serial_proxy_mcast
{
public:
    serial_proxy_atp();
    int  send(const char* msg, size_t len);
    int  recv(char* buf, size_t len);
private:
    unsigned int seq_tx;
};
#endif

#endif /* NO_MCAST_PROTOCOL */

#endif /* _SERIAL_PROXY_H_ */

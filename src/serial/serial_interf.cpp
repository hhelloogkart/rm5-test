#include "serial_interf.h"
#include "serial_var.h"
#include "serial_output_var.h"
#include <list>
#include <vector>
#include <string>
#include <util/proccore.h>
#include <comm/tcpcore.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

using std::string;

struct serial_rec
{
    serial_rec(int inf, const char* mask, const char* mask_output, const char* hdr, unsigned short hdrlen, serial_var* v);
    serial_rec(const serial_rec& right);
    int interf;
    string hdrMask;
    string hdrMask_output;
    string header;
    string header_output;
    serial_var* var;
};

struct param_rec
{
    param_rec(int inf, const char* hst, int prt, unsigned int to);
    param_rec(const param_rec& right);
    int interf;
    string host;
    int port;
    unsigned int timeout_msec;
};

struct serial_output_rec
{
    serial_output_rec(serial_output_var* v, int ifnum);
    serial_output_rec(const serial_output_rec& right);
    int interf;
    serial_output_var* var;
};

typedef std::list<serial_rec> serial_rec_list_typ;
typedef std::list<param_rec> param_rec_list_typ;
typedef std::vector<int> sock_array_typ;
typedef std::vector<serial_interf_typ*> interf_array_typ;
typedef std::list<serial_interf_typ> serial_interf_list_typ;
typedef std::list<serial_output_rec> serial_output_rec_list_typ;
static serial_rec_list_typ* serial_rec_list = 0;
static param_rec_list_typ* param_rec_list = 0;
static serial_output_rec_list_typ& output_rec_list()
{
    static serial_output_rec_list_typ prvt;
    return prvt;
}
static serial_interf_list_typ& serial_interf_list()
{
    static serial_interf_list_typ prvt;
    return prvt;
}

serial_rec::serial_rec(int inf, const char* mask, const char* mask_output, const char* hdr, unsigned short hdrlen, serial_var* v)
    : interf(inf), header(hdr, hdrlen), header_output(hdr, hdrlen), var(v)
{
    if (mask != 0)
    {
        hdrMask.assign(mask, hdrlen);
        for (int i = 0; i < hdrlen; ++i)
        {
            header[i] &= hdrMask[i];
        }
    }


    if(mask_output !=0)
    {
        hdrMask_output.assign(mask_output, hdrlen);
        for (int i = 0; i < hdrlen; ++i)
        {
            header_output[i] &= hdrMask_output[i];
        }
    }
}

serial_rec::serial_rec(const serial_rec& right)
    : interf(right.interf), hdrMask(right.hdrMask), hdrMask_output(right.hdrMask_output), header(right.header), header_output(right.header_output), var(right.var)
{
}

param_rec::param_rec(int inf, const char* hst, int prt, unsigned int to) : interf(inf), host(hst), port(prt), timeout_msec(to)
{
}

param_rec::param_rec(const param_rec& right) : interf(right.interf), host(right.host), port(right.port), timeout_msec(right.timeout_msec)
{
}

serial_output_rec::serial_output_rec(serial_output_var* v, int ifnum) : interf(ifnum), var(v)
{
}

serial_output_rec::serial_output_rec(const serial_output_rec& right)
{
    interf = right.interf;
    var = right.var;
}

serial_interf_typ::serial_interf_typ() :
    sock(0),
    buflen(0),
    cmnhdr(0),
    cmnhdrlen(0),
    msghdr(0),
    msghdrlen(0),
    hdrmask(0),
    hdrmask_output(0),
    var(0),
    varlen(0),
    buf_lock(0),
    timeout_msec(0)
{
}

serial_interf_typ::~serial_interf_typ()
{
    delete[] cmnhdr;
    delete[] msghdr;
    delete[] hdrmask;
    delete[] hdrmask_output;
    delete[] var;
    delete sock;
}

void serial_interf_typ::addMessage(const char* mask, const char* mask_output, const char* header, unsigned short hdrlen, serial_var* var, int interf)
{
    serial_rec item(interf, mask, mask_output, header, hdrlen, var);
    // check that init has not been called (TODO: Throw assert?)
    if (!serial_interf_list().empty())
    {
        return;
    }

    // create a new record list if not created
    if (serial_rec_list == 0)
    {
        serial_rec_list = new serial_rec_list_typ;
        serial_rec_list->push_back(item);
    }
    else
    {
        // insert into the list ordered based on the host (unsorted), followed by header (sorted)
        serial_rec_list_typ::iterator it;
        for (it = serial_rec_list->begin(); it != serial_rec_list->end(); ++it)
        {
            // match the host
            if ((*it).interf == interf)
            {
                break;
            }
        }
        for (; it != serial_rec_list->end(); ++it)
        {
            // break if header is more than
            if (memcmp((*it).header.data(), item.header.data(), hdrlen) > 0)
            {
                break;
            }
            // break if its at the last message
            else if ((*it).interf != interf)
            {
                break;
            }
        }
        serial_rec_list->insert(it, item);
    }
}

void serial_interf_typ::addInterface(const char* host, int port, unsigned int timeout, int interf)
{
    // check that init has not been called (TODO: Throw assert?)
    if (!serial_interf_list().empty())
    {
        return;
    }

    // create a new record list if not created
    if (param_rec_list == 0)
    {
        param_rec_list =  new param_rec_list_typ;
    }
    param_rec_list->push_front(param_rec(interf, host, port, timeout));
}

void serial_interf_typ::addOutputMessage(serial_output_var* var, int interf)
{
    output_rec_list().push_back(serial_output_rec(var, interf));
}

void serial_interf_typ::init()
{
    // sanity check that list is created and not empty
    if ((serial_rec_list == 0) || serial_rec_list->empty())
    {
        return;
    }

    // sanity check that list is created and not empty
    if ((param_rec_list == 0) || param_rec_list->empty())
    {
        return;
    }

    serial_interf_list_typ::iterator it2;
    serial_rec_list_typ::const_iterator it = serial_rec_list->begin();
    int lastinterf = -1;
    string lastcmnhdr;
    serial_interf_typ* interf = 0;
    int cnt = 0;
    bool mflag = false; //flag to indicate mask being used in interface
    bool moflag = false; //flag to indicate maskoutput being used in interface
    // the first pass is to count the number of messages and instantiate the serial_interf_typ
    for (; it != serial_rec_list->end();)
    {
        if ((*it).interf != lastinterf)
        {
            cnt = 1;
            lastinterf = (*it).interf;
            // need to instantiate a new serial_interf_typ and open the port
            serial_interf_list().push_back(serial_interf_typ());
            interf = &(serial_interf_list().back());
            mflag = !(*it).hdrMask.empty(); //indicate this msg uses mask (reset)
            moflag = !(*it).hdrMask_output.empty(); //indicate this msg uses maskoutput (reset)
            interf->cmnhdrlen = static_cast<unsigned short>((*it).header.size());
            lastcmnhdr = (*it).header;
            for (param_rec_list_typ::const_iterator it4=param_rec_list->begin(); it4!=param_rec_list->end(); ++it4)
            {
                if ((*it).interf == (*it4).interf)
                {
                    interf->sock = serial_proxy::sethost((*it4).host.c_str(), (*it4).port);
                    assert(interf->sock != 0);
                    interf->sock->connect();
                    interf->timeout_msec = (*it4).timeout_msec;
                    for (serial_output_rec_list_typ::iterator it5=output_rec_list().begin(); it5!=output_rec_list().end(); ++it5)
                    {
                        if ((*it5).interf == (*it4).interf)
                        {
                            (*it5).var->setProxy(interf->sock);
                            (*it5).interf = -1; /* signify that it is done */
                        }
                    }
                    break;
                }
            }
        }
        else
        {
            // count up one message
            ++cnt;
            mflag |= !(*it).hdrMask.empty(); //indicate this msg uses mask
            moflag |= !(*it).hdrMask_output.empty(); //indicate this msg uses maskoutput
            // check if there is still common header
            if (lastcmnhdr.size() > 0)
            {
                unsigned int match;
                for (match=0; ((match < lastcmnhdr.size()) && (lastcmnhdr[match] == (*it).header[match])); ++match)
                {
                    ;
                }
                if (lastcmnhdr.size() > match)
                {
                    lastcmnhdr.resize(match);
                    interf->cmnhdrlen = match;
                    interf->msghdrlen = static_cast<unsigned short>((*it).header.size() - match);
                }
            }
        }

        // == INCREMENT == INCREMENT == INCREMENT ==
        ++it;
        // == INCREMENT == INCREMENT == INCREMENT ==

        if ((it == serial_rec_list->end()) || ((*it).interf != lastinterf))
        {
            // instantiate the array for the last interf

            // for the special case where there is only 1 message for an interface
            if (cnt == 1)
            {
                // only specific, no common for single message
                interf->msghdrlen = interf->cmnhdrlen;
                interf->cmnhdrlen = 0;
            }

            if (mflag)
            {
                //must use original header for mask interface
                interf->msghdrlen += interf->cmnhdrlen;
                interf->cmnhdrlen = 0;
                interf->hdrmask = new char[cnt*(interf->msghdrlen)];
                interf->find_var = &serial_interf_typ::find_var_3;
            }
            else if (interf->cmnhdrlen!=0)
            {
                interf->cmnhdr = new char[interf->cmnhdrlen];
                memcpy(interf->cmnhdr, lastcmnhdr.data(), interf->cmnhdrlen);
                interf->find_var = &serial_interf_typ::find_var_1;
            }
            else if (cnt == 1 && interf->msghdrlen == 0)
            {
                interf->find_var = &serial_interf_typ::find_var_4;
            }
            else
            {
                interf->find_var = &serial_interf_typ::find_var_2;
            }

            if (interf->msghdrlen != 0)
            {
                interf->msghdr = new char[cnt*(interf->msghdrlen)];
            }

            if (moflag)
            {
                interf->hdrmask_output = new char[cnt*(interf->msghdrlen)];
            }

            interf->var = new serial_var*[cnt];
            interf->varlen = cnt;
        }
    }


    lastinterf = -1;
    // the second pass is to copy the contents of the msg header and var
    for (it = serial_rec_list->begin(), it2 = serial_interf_list().begin(); it != serial_rec_list->end(); ++it, ++cnt)
    {
        if ((*it).interf != lastinterf)
        {
            lastinterf = (*it).interf;
            interf = &(*it2);
            ++it2;
            cnt = 0;
        }

        if(interf->hdrmask != 0)
        {
            //copy hdrmask if exist else set to default, note: cmnhdrlen should be zero in this situation
            memcpy(interf->msghdr  + (cnt*(interf->msghdrlen)), (*it).header.data(), interf->msghdrlen);

            //assign header mask
            if(!(*it).hdrMask.empty())
            {
                memcpy(interf->hdrmask + (cnt*(interf->msghdrlen)), (*it).hdrMask.data(), interf->msghdrlen);
            }
            else
            {
                memset(interf->hdrmask + (cnt*(interf->msghdrlen)), '\xFF', interf->msghdrlen);
            }

            //finally apply the mask and output mask onto the header
            const char* srchdr = (*it).header.data();
            const char* srcmsk = (*it).hdrMask.data();
            char* dstend = interf->msghdr + (cnt+1)*(interf->msghdrlen);
            for (char* dst = interf->msghdr + cnt*(interf->msghdrlen);
                 dst != dstend;
                 ++dst, ++srchdr, ++srcmsk)
            {
                *dst = *srchdr & *srcmsk;
            }
        }
        else
        {
            memcpy(interf->msghdr + (cnt*(interf->msghdrlen)), (*it).header.data()+interf->cmnhdrlen, interf->msghdrlen);
        }
        if (interf->hdrmask_output != 0)
        {
            //assign header output mask
            if (!(*it).hdrMask_output.empty())
            {
                memcpy(interf->hdrmask_output + (cnt*(interf->msghdrlen)), (*it).hdrMask_output.data(), interf->msghdrlen);
            }
            else
            {
                memset(interf->hdrmask_output + (cnt*(interf->msghdrlen)), '\xFF', interf->msghdrlen);
            }
        }

        interf->var[cnt] = (*it).var;
        (*it).var->msgidx = cnt;
        (*it).var->interf = interf;
    }

    // check if there are any interfaces that are missed out for output
    for (serial_output_rec_list_typ::iterator it4 = output_rec_list().begin(); it4 != output_rec_list().end(); ++it4)
    {
        if ((*it4).interf != -1)
        {
            for (param_rec_list_typ::const_iterator it5 = param_rec_list->begin(); it5 != param_rec_list->end(); ++it5)
            {
                if ((*it4).interf == (*it5).interf)
                {
                    serial_proxy* proxy = serial_proxy::sethost((*it5).host.c_str(), (*it5).port);
                    assert(proxy);
                    proxy->connect();
                    (*it4).var->setProxy(proxy, true);
                    serial_output_rec_list_typ::iterator it3=it4;
                    for (++it3; it4!=output_rec_list().end(); ++it3)
                    {
                        if ((*it3).interf == (*it4).interf)
                        {
                            (*it3).var->setProxy(proxy);
                            (*it3).interf = -1;
                        }
                    }
                    break;
                }
            }
        }
    }

    // clear the list
    delete serial_rec_list;
    delete param_rec_list;
    serial_helper_typ::initHelpers();
}

short serial_interf_typ::find_var_1(serial_var** varptr, char* buffer, unsigned short bufferlen)
{
    const unsigned short headerlen = cmnhdrlen + msghdrlen;
    // find a match to the common part
    for (unsigned short offset = 0; bufferlen-offset >= headerlen; ++offset)
    {
        if (memcmp(cmnhdr, buffer+offset, cmnhdrlen) == 0)
        {
            // we start from the middle of the sorted list. The search takes O(logN)
            int lvaridx = 0;
            int rvaridx = varlen-1;
            while (lvaridx <= rvaridx)
            {
                const int varidx = (lvaridx + rvaridx) / 2;
                int cmp = memcmp(buffer + offset + cmnhdrlen, msghdr + (varidx * msghdrlen), msghdrlen);
                if (cmp == 0)
                {
                    // match is found
                    *varptr = var[varidx];
                    return offset;
                }
                else if (cmp < 0)
                {
                    // smaller
                    rvaridx = varidx - 1;
                }
                else
                {
                    // larger
                    lvaridx = varidx + 1;
                }
            }
        }
    }
    return -1;
}

short serial_interf_typ::find_var_2(serial_var** varptr, char* buffer, unsigned short bufferlen)
{
    // find a match using only specific part, which is slow
    for (unsigned short offset = 0; bufferlen-offset >= msghdrlen; ++offset)
    {
        int lvaridx = 0;
        int rvaridx = varlen-1;
        while (lvaridx <= rvaridx)
        {
            const int varidx = (lvaridx + rvaridx) / 2;
            int cmp = memcmp(buffer + offset, msghdr + (varidx * msghdrlen), msghdrlen);
            if (cmp == 0)
            {
                // match is found
                *varptr = var[varidx];
                return offset;
            }
            else if (cmp < 0)
            {
                // smaller
                rvaridx = varidx - 1;
            }
            else
            {
                // larger
                lvaridx = varidx + 1;
            }
        }
    }
    return -1;
}


short serial_interf_typ::find_var_3(serial_var** varptr, char* buffer, unsigned short bufferlen)
{
    // find a match using only specific part, which is slow
    for (unsigned short offset = 0; bufferlen-offset >= msghdrlen; ++offset)
    {
        int lvaridx = 0;
        int rvaridx = varlen-1;
        while (lvaridx <= rvaridx)
        {
            const int varidx = (lvaridx + rvaridx) / 2;
            const unsigned char* hdr = (unsigned char*)msghdr +(varidx*msghdrlen);
            const unsigned char* msk = (unsigned char*)hdrmask+(varidx*msghdrlen);
            const unsigned char* buf = (unsigned char*)buffer+offset;
            const unsigned char* end = buf+msghdrlen;
            for (;; ++buf, ++hdr, ++msk)
            {
                if (buf == end)
                {
                    // match is found
                    *varptr = var[varidx];
                    return offset;
                }
                if ((*buf & *msk) < *hdr)
                {
                    // smaller
                    rvaridx = varidx - 1;
                    break;
                }
                if ((*buf & *msk) > *hdr)
                {
                    // larger
                    lvaridx = varidx + 1;
                    break;
                }
            }
        }
    }
    return -1;
}

short serial_interf_typ::find_var_4(serial_var** varptr, char*, unsigned short len)
{
    if (len == 0)
    {
        return -1;
    }
    *varptr = var[0];
    return 0;
}

char* serial_interf_typ::get_buffer(int sz)
{
    // use the end of read buffer for temporary write buffer
    const int reqlen = sz;
    if ((reqlen + buflen > SERIAL_BUF))
    {
        return 0;
    }
    else if (atomic_tas(&buf_lock))
    {
        return buf + buflen;
    }
    else
    {
        for (int cnt = 0; cnt < 1000; ++cnt)
        {
            context_yield();
            if (atomic_tas(&buf_lock))
            {
                return buf + buflen;
            }
        }
        return 0;
    }
}

bool serial_interf_typ::write_buffer(char* buf, int sz, int idx)
{
    // check for valid index
    if (idx >= varlen)
    {
        atomic_untas(&buf_lock);
        return false;
    }

    if ((hdrmask != 0) || (hdrmask_output != 0))
    {
        // zero away from the buffer the parts covered by the mask and OR the header in
        const char* hdr = msghdr +(idx*msghdrlen);
        const char* msk = hdrmask_output+(idx*msghdrlen);
        for (int i = 0; i < msghdrlen; ++i, ++hdr, ++msk)
        {
            buf[i] = ( (*hdr & *msk) | (buf[i] & ~(*msk)) );
        }
    }
    else
    {
        // write the common header if any
        if (cmnhdrlen != 0)
        {
            memcpy(buf, cmnhdr, cmnhdrlen);
        }
        // write the specific header
        memcpy(buf + cmnhdrlen, msghdr + (idx * msghdrlen), msghdrlen);
    }

    int result = sock->send(buf, sz);
    atomic_untas(&buf_lock);
    return result == sz;
}

bool serial_interf_typ::read_data()
{
    bool more = true;

    while (more)
    {
        // wait for the buffer to be available
        while (!atomic_tas(&buf_lock))
        {
            context_yield();
        }

        // read from serial port
        int result = sock->recv(buf + buflen, SERIAL_BUF - buflen);
        if (result > 0)
        {
            buflen += result;
            atomic_untas(&buf_lock);
            if (buflen != SERIAL_BUF)
            {
                more = false;
            }
            else if (sock->isblock())
            {
                struct timeval tv = {
                    0, 0
                };
                more = (sock->select(&tv) > 0);
            }
        }
        else
        {
            if (result < 0)
            {
                buflen = 0;
            }

            atomic_untas(&buf_lock);
            return result == 0;
        }

        serial_var* varptr;
        int offset;
        char* bufptr = &buf[0];
        int left = buflen;
        const int crud = cmnhdrlen + msghdrlen - 1; // amount that could be partial header
        while (left > crud)
        {
            if ((offset = (this->*find_var)(&varptr, bufptr, left)) != -1)
            {
                // a matching header has been found
                // advance the bufptr
                bufptr += offset;
                left -= offset;

                const int bufadvance = varptr->assistedExtract(bufptr, left);

                if (bufadvance >= 0)
                {
                    bufptr += bufadvance;
                    left -= bufadvance;
                    continue;
                }
                else
                {
                    break;
                }
            }
            else
            {
                //Unable to find a header in the packet, we keep only (header len - 1) bytes of data.
                if (left > (crud + 10))
                {
                    left = crud + 10;                     //add 10 byte guard for pre-header checksum
                }
                break;
            }
        }
        // if the socket is a datagram type, then dump the buffer
        if (sock->isblock())
        {
            buflen = 0;
        }
        // store incomplete packet for next pass
        // we don't bother to lock coz write is operating to the right of readbuf of which
        // we are now shrinking
        else
        {
            if ((left > 0) && (left != buflen))
            {
                memmove(buf, buf + (buflen - left), left);
            }
            buflen = left;
        }
    }

    return true;
}

void serial_interf_typ::incoming()
{
    sock_array_typ socks;
    interf_array_typ interfs;
    unsigned int timeout_sec = 0;
    int timeout_msec = 0;

    // first pass for those that cannot be multiplexed
    for (serial_interf_list_typ::iterator it=serial_interf_list().begin(); it!=serial_interf_list().end(); ++it)
    {
        int fd = (*it).sock->get_sock();
        if (fd == -1)
        {
            struct timeval tm = {
                static_cast<long>((*it).timeout_msec) / 1000L, (static_cast<long>((*it).timeout_msec) % 1000L) * 1000L
            };
            int ret = (*it).sock->select(&tm);
            if (ret > 0)
            {
                (*it).read_data();
            }
            else if (ret < 0 && atomic_tas(&((*it).buf_lock)))
            {
                (*it).buflen = 0;
                atomic_untas(&((*it).buf_lock));
            }
        }
        else
        {
            socks.push_back(fd);
            interfs.push_back(&(*it));
            timeout_sec += (*it).timeout_msec / 1000;
            timeout_msec += (*it).timeout_msec % 1000; // note will overflow
        }
    }

    // second pass for those that can be multiplexed
    if (!socks.empty())
    {
        struct timeval tm = {
            static_cast<long>(timeout_sec + timeout_msec / 1000), (timeout_msec % 1000) * 1000
        };
        if (core_select(&(socks[0]), static_cast<int>(socks.size()), &tm) != 0)
        {
            for (int cnt = 0; cnt<(int)socks.size(); ++cnt)
            {
                if (socks[cnt] == -1)
                {
                    if (atomic_tas(&(interfs[cnt]->buf_lock)))
                    {
                        interfs[cnt]->buflen = 0;
                    }
                    atomic_untas(&(interfs[cnt]->buf_lock));
                }
                else if (socks[cnt] >= 0)
                {
                    interfs[cnt]->read_data();
                }
            }
        }
    }
}

void serial_interf_typ::outgoing()
{
    // serial_var sending
    for (serial_interf_list_typ::iterator it=serial_interf_list().begin(); it!=serial_interf_list().end(); ++it)
    {
        serial_var* prevVar = 0;
        for (int cnt = 0; cnt < (*it).varlen; ++cnt)
        {
            if ((*it).var[cnt]->isRMDirty())
            {
                if (prevVar)
                {
                    prevVar->refreshRM();
                }
                else
                {
                    (*it).sock->setNagleAlgo(true); // turn on Nagle Algo to join up all packets in one block
                }
                prevVar = (*it).var[cnt];
            }
        }
        (*it).sock->setNagleAlgo(false); // turn off Nagle Algo to send last packet out immediately without waiting
        if (prevVar)
        {
            prevVar->refreshRM();
        }
    }
    // serial_output_var sending
    // we are unable to use Nagle Algo because output_var may have diff interfaces and are not sorted
    for (serial_output_rec_list_typ::iterator it=output_rec_list().begin(); it!=output_rec_list().end(); ++it)
    {
        (*it).var->refreshRM();
    }
}

void serial_interf_typ::close()
{
    for (serial_interf_list_typ::iterator it=serial_interf_list().begin(); it!=serial_interf_list().end(); ++it)
    {
        (*it).sock->close();
    }
}

void serial_interf_typ::reconnect()
{
    for (serial_interf_list_typ::iterator it=serial_interf_list().begin(); it!=serial_interf_list().end(); ++it)
    {
        (*it).sock->connect();
    }
}

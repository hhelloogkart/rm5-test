/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _MULTICASTCORE_H_
#define _MULTICASTCORE_H_

#include <time.h>
#include <rmglobal.h>

struct timeval;
struct sockaddr_in;
struct multicast_core_private;

class multicast_core
{
public:
    multicast_core();
    multicast_core(const multicast_core&);
    ~multicast_core();

    bool isvalid() const;
    int  get_sock() const;
    int  recv(char* buf, size_t len, struct sockaddr_in* rxaddr = 0);
    int  select(const struct timeval* stm = 0);
    bool close();
    bool bind(const char* host, int port);
    int  send(const char* buf, size_t len, const char* host, int port);

protected:
    multicast_core_private* prvt;

};
#endif
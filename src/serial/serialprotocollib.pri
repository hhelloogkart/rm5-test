win32: LIBS += ws2_32.lib Iphlpapi.lib

DEFINES += NO_SW_PROTOCOL

# Input
HEADERS += $$PWD/../../include/serial_var.h \
	$$PWD/../../include/serial_output_var.h \
	$$PWD/serial_interf.h \
	$$PWD/serial_proxy.h
SOURCES += $$PWD/serial_var.cpp \
	$$PWD/serial_output_var.cpp \
	$$PWD/serial_interf.cpp \
	$$PWD/serial_proxy.cpp

android:SOURCES += $$PWD/ifaddrs.c
!no_mcast{
	HEADERS += $$PWD/multicastcore.h
	SOURCES += $$PWD/multicastcore.cpp
}else{
	DEFINES += NO_MCAST_PROTOCOL
}

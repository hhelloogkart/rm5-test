/******************************************************************/
/* Copyright DSO National Laboratories 2008. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include <lzip_var.h>
#include <outbuf.h>

#include "lzlib/lzlib.h"

#define MAX_COMPRESSION_LEVEL   10
#ifndef LLONG_MAX
#define LLONG_MAX 0x7FFFFFFFFFFFFFFFULL
#endif

lzip_var::lzip_var() : compress_var()
{
}

lzip_var::lzip_var(const lzip_var& right) : compress_var(right)
{
}

int lzip_var::compressionOverhead() const
{
    return 26 + 1 + LZ_min_dictionary_size();
}

int lzip_var::compress(outbuf& buf, outbuf& rbuf)
{
    static int table[MAX_COMPRESSION_LEVEL][2] =
    {
        { 1 << 16, 5 },
        { 1 << 20, 10 },
        { 3 << 19, 12 },
        { 1 << 21, 17 },
        { 3 << 20, 26 },
        { 1 << 22, 44 },
        { 1 << 23, 80 },
        { 1 << 24, 108 },
        { 3 << 23, 163 },
        { 1 << 25, 273 }
    };

    if (compression_level >= MAX_COMPRESSION_LEVEL)
    {
        compression_level = MAX_COMPRESSION_LEVEL - 1;
    }

    LZ_Encoder* enc_ptr = LZ_compress_open(table[compression_level][0],
                                           table[compression_level][1], LLONG_MAX);
    if (LZ_compress_errno(enc_ptr) != LZ_ok)
    {
        LZ_compress_close(enc_ptr);
        return 0;
    }

    const int rsz = rbuf.size(), wsz = buf.maxsize();
    int roff = 0, woff = 0;

    do
    {
        // writing portion
        if (woff < rsz)
        {
            const int mxw = LZ_compress_write_size(enc_ptr);
            const int isz = rsz - woff;
            const int csz = (isz > mxw) ? mxw : isz;
            const int ret = LZ_compress_write(enc_ptr, rbuf.get() + woff, csz);
            if (ret > 0)
            {
                woff += ret;
            }
        }
        if (woff == rsz)
        {
            LZ_compress_finish(enc_ptr);
        }

        // reading portion
        {
            const int ret = LZ_compress_read(enc_ptr, buf.get() + roff, wsz - roff);
            if (ret > 0)
            {
                roff += ret;
            }
            else if (ret <= 0 && woff == rsz)
            {
                break;
            }
        }
    }
    while (roff < rsz && LZ_compress_finished(enc_ptr) != 1);

    // check if there was insufficient read buffer
    if (roff == rsz && LZ_compress_total_out_size(enc_ptr) > wsz)
    {
        roff = 0;
    }
    buf.setsize(roff);
    if (roff > 0)
    {
        buf += 'Z';          //append a dummy byte so that two consecutive lzip_var can work
    }
    LZ_compress_close(enc_ptr);
    return roff;
}

int lzip_var::decompress(outbuf& dbuf, int len, const unsigned char* sbuf)
{
    LZ_Decoder* dec_ptr = LZ_decompress_open();
    if (LZ_decompress_errno(dec_ptr) != LZ_ok)
    {
        LZ_decompress_close(dec_ptr);
        return len; // cannot process, return full length
    }

    const int rsz = dbuf.maxsize();
    int woff = 0, roff = 0;
    int dsz = 0;

    do
    {
        // writing portion
        if (woff < len)
        {
            const int mxw = LZ_decompress_write_size(dec_ptr);
            const int isz = len - woff;
            const int wsz = (isz > mxw) ? mxw : isz;
            const int ret = LZ_decompress_write(dec_ptr, sbuf + woff, wsz);
            if (ret > 0)
            {
                woff += ret;
            }
        }
        if (woff == len)
        {
            LZ_decompress_finish(dec_ptr);
        }

        // reading portion
        {
            const int ret = LZ_decompress_read(dec_ptr, dbuf.get() + roff, rsz - roff);
            if (ret > 0)
            {
                roff += ret;
            }
            else if (ret <= 0 && woff == len)
            {
                break;
            }
            // no more left or decompression failed because of corruption
        }
    }
    while (roff < rsz && LZ_decompress_finished(dec_ptr) != 1);

    // check if dummy byte was detected
    const int dts = static_cast<int>(LZ_decompress_total_in_size(dec_ptr));
    if (dts < len && sbuf[dts] == 'Z')
    {
        dsz = dts;
    }
    // decompression ended because dummy byte was detected, or
    // dump rest of buffer, because dummy was not found
    len = (dsz > 0 && roff > 0) ? len - dsz - 1 : 0;

    // check if there was insufficient read buffer
    if (roff == rsz && LZ_decompress_total_out_size(dec_ptr) > rsz)
    {
        roff = 0;
    }
    dbuf.setsize(roff);
    LZ_decompress_close(dec_ptr);
    return len;
}

const mpt_var& lzip_var::operator=(const mpt_var& right)
{
    const lzip_var* gen = RECAST(const lzip_var*,&right);
    if (gen)
    {
        lzip_var::operator=(* gen);
    }
    return *this;
}

const lzip_var& lzip_var::operator=(const lzip_var& right)
{
    compress_var::operator = ((const compress_var&)right);
    return *this;
}


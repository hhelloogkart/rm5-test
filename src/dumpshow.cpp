/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "rmstl.h"
#include "dumpshow.h"
#include <util/dbgout.h>
#include "mpt_var.h"

struct dump_show_private
{
    string msg;
};

dump_show::dump_show(mpt_var* _data, const char* _msg) :
    data(_data), prvt(new dump_show_private)
{
    if (_msg)
    {
        prvt->msg = _msg;
    }
    data->addCallback(this);
}

dump_show::~dump_show()
{
    data->removeCallback(this);
    delete prvt;
}

void dump_show::operator()()
{
    if (dirty)
    {
        dout << prvt->msg << *data << '\n';
    }
    dirty = false;
}

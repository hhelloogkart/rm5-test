/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include <gen_var.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

bool negative_string(const char* str)
{
    for (; *str != '\0'; ++str)
    {
        if (*str == '-')
        {
            return true;
        }
        else if (!isspace(*str))
        {
            return false;
        }
    }
    return false;
}

template<class C>
bool integer_check(const char* str, C& value)
{
    const size_t len = strlen(str);
    for (size_t idx = 0; idx < len; ++idx)
    {
        if (str[idx] == '.' || str[idx] == 'e' || str[idx] == 'E' || str[idx] == 'd' || str[idx] == 'D')
        {
            double result;
            value_var::str_to_value(str, result);
            value = rm_internal::generic_var_convert<double, C>(result);
            return false;
        }
    }
    return true;
}

void value_var::str_to_value(const char* str, long& value)
{
    if (integer_check(str, value))
    {
        value = strtol(str, 0, 10);
    }
}
void value_var::str_to_value(const char* str, int& value)
{
    long result;
    str_to_value(str, result);
    if (result > INT_MAX)
    {
        value = INT_MAX;
    }
    else if (result < INT_MIN)
    {
        value = INT_MIN;
    }
    else
    {
        value = static_cast<int>(result);
    }
}
void value_var::str_to_value(const char* str, short& value)
{
    long result;
    str_to_value(str, result);
    if (result > SHRT_MAX)
    {
        value = SHRT_MAX;
    }
    else if (result < SHRT_MIN)
    {
        value = SHRT_MIN;
    }
    else
    {
        value = static_cast<short>(result);
    }
}
void value_var::str_to_value(const char* str, char& value)
{
    long result;
    str_to_value(str, result);
    if (result > CHAR_MAX)
    {
        value = CHAR_MAX;
    }
    else if (result < CHAR_MIN)
    {
        value = CHAR_MIN;
    }
    else
    {
        value = static_cast<char>(result);
    }
}
void value_var::str_to_value(const char* str, unsigned long& value)
{
    if (negative_string(str))
    {
        value = 0;
    }
    else if (integer_check(str, value))
    {
        value = strtoul(str, 0, 10);
    }
}
void value_var::str_to_value(const char* str, unsigned int& value)
{
    if (negative_string(str))
    {
        value = 0;
    }
    else
    {
        unsigned long result;
        str_to_value(str, result);
        if (result > UINT_MAX)
        {
            value = UINT_MAX;
        }
        else
        {
            value = static_cast<unsigned int>(result);
        }
    }
}
void value_var::str_to_value(const char* str, unsigned short& value)
{
    if (negative_string(str))
    {
        value = 0;
    }
    else
    {
        unsigned long result;
        str_to_value(str, result);
        if (result > USHRT_MAX)
        {
            value = USHRT_MAX;
        }
        else
        {
            value = static_cast<unsigned short>(result);
        }
    }
}
void value_var::str_to_value(const char* str, unsigned char& value)
{
    if (negative_string(str))
    {
        value = 0;
    }
    else
    {
        unsigned long result;
        str_to_value(str, result);
        if (result > UCHAR_MAX)
        {
            value = UCHAR_MAX;
        }
        else
        {
            value = static_cast<unsigned char>(result);
        }
    }
}
void value_var::str_to_value(const char* str, float& value)
{
    double result;
    str_to_value(str, result);
    value = static_cast<float>(result);
}
void value_var::str_to_value(const char* str, double& value)
{
    value = strtod(str, 0);
}
void value_var::str_to_value(const char* str, unsigned long long& value)
{
    if (negative_string(str))
    {
        value = 0;
    }
    else if (integer_check(str, value))
    {
#if defined(_MSC_VER) && _MSC_VER <= 1600
        value = _strtoui64(str, 0, 10);
#else
        value = strtoull(str, 0, 10);
#endif
    }
}
void value_var::str_to_value(const char* str, long long& value)
{
    if (integer_check(str, value))
    {
#if defined(_MSC_VER) && _MSC_VER <= 1600
        value = _strtoi64(str, 0, 10);
#else
        value = strtoll(str, 0, 10);
#endif
    }
}
char* value_var::value_to_str(long value)
{
    char* str = get_char_buffer();
    rm_sprintf(str, 256, get_int_format(), value);
    return str;
}
char* value_var::value_to_str(int value)
{
    return value_to_str(static_cast<long>(value));
}
char* value_var::value_to_str(short value)
{
    return value_to_str(static_cast<long>(value));
}
char* value_var::value_to_str(char value)
{
    return value_to_str(static_cast<long>(value));
}
char* value_var::value_to_str(unsigned long value)
{
    char* str = get_char_buffer();
    rm_sprintf(str, 256, get_uint_format(), value);
    return str;
}
char* value_var::value_to_str(unsigned int value)
{
    return value_to_str(static_cast<unsigned long>(value));
}
char* value_var::value_to_str(unsigned short value)
{
    return value_to_str(static_cast<unsigned long>(value));
}
char* value_var::value_to_str(unsigned char value)
{
    return value_to_str(static_cast<unsigned long>(value));
}
char* value_var::value_to_str(float value)
{
    return value_to_str(static_cast<double>(value));
}
char* value_var::value_to_str(double value)
{
    char* str = get_char_buffer();
    rm_sprintf(str, 256, get_float_format(), value);
    return str;
}
char* value_var::value_to_str(unsigned long long value)
{
    char* str = get_char_buffer();
    rm_sprintf(str, 256, "%llu", value);
    return str;
}
char* value_var::value_to_str(long long value)
{
    char* str = get_char_buffer();
    rm_sprintf(str, 256, "%lld", value);
    return str;
}
char* value_var::get_char_buffer()
{
    static char tstr[256];
    return tstr;
}
char* value_var::get_uint_format()
{
    static char ufmt[16] = "%lu";
    return ufmt;
}
char* value_var::get_int_format()
{
    static char ifmt[16] = "%ld";
    return ifmt;
}
char* value_var::get_float_format()
{
    static char ffmt[16] = "%.4g";
    return ffmt;
}
void value_var::set_uint_format(const char* ufmt)
{
    rm_strcpy(get_uint_format(), 16, ufmt);
}
void value_var::set_int_format(const char* ifmt)
{
    rm_strcpy(get_int_format(), 16, ifmt);
}
void value_var::set_float_format(const char* ffmt)
{
    rm_strcpy(get_float_format(), 16, ffmt);
}
void value_var::toValue(long long& value) const
{
    value = to_long_long();
}
void value_var::toValue(long& value) const
{
    value = to_long();
}
void value_var::toValue(int& value) const
{
    value = to_int();
}
void value_var::toValue(short& value) const
{
    value = to_short();
}
void value_var::toValue(char& value) const
{
    value = to_char();
}
void value_var::toValue(unsigned long& value) const
{
    value = to_unsigned_long();
}
void value_var::toValue(unsigned long long& value) const
{
    value = to_unsigned_long_long();
}
void value_var::toValue(unsigned int& value) const
{
    value = to_unsigned_int();
}
void value_var::toValue(unsigned short& value) const
{
    value = to_unsigned_short();
}
void value_var::toValue(unsigned char& value) const
{
    value = to_unsigned_char();
}
void value_var::toValue(float& value) const
{
    value = to_float();
}
void value_var::toValue(double& value) const
{
    value = to_double();
}
void value_var::fromValue(long long value)
{
    from_long_long(value);
}
void value_var::fromValue(long value)
{
    from_long(value);
}
void value_var::fromValue(int value)
{
    from_int(value);
}
void value_var::fromValue(short value)
{
    from_short(value);
}
void value_var::fromValue(char value)
{
    from_char(value);
}
void value_var::fromValue(unsigned long long value)
{
    from_unsigned_long_long(value);
}
void value_var::fromValue(unsigned long value)
{
    from_unsigned_long(value);
}
void value_var::fromValue(unsigned int value)
{
    from_unsigned_int(value);
}
void value_var::fromValue(unsigned short value)
{
    from_unsigned_short(value);
}
void value_var::fromValue(unsigned char value)
{
    from_unsigned_char(value);
}
void value_var::fromValue(float value)
{
    from_float(value);
}
void value_var::fromValue(double value)
{
    from_double(value);
}
void value_var::fromValue(const char* value)
{
    from_char_ptr(value);
}

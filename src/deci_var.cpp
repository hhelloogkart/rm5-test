/******************************************************************/
/* Copyright DSO National Laboratories 2008. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "deci_var.h"
#include "mpt_show.h"
#include <outbuf.h>

class decision_var_private_show : public mpt_baseshow
{
public:
    decision_var_private_show(decision_var* var);
    ~decision_var_private_show();
    virtual void setDirty(mpt_var* var);
    virtual void operator()();
    void setEnabled(bool enable);
    void setInvalid();
    int getOutIdx() const
    {
        return outidx;
    }

private:
    decision_var* obj;  // add callback for each child and to check which child triggered
    int childcnt; // number of children
    int outidx; // contains the current index being written out
    bool enb;
    bool* dirtyarry; // dirty flag for each child

};

decision_var_private_show::decision_var_private_show(decision_var* var) :
    obj(var), childcnt(var->cardinal()), outidx(-1), enb(true), dirtyarry(new bool[childcnt])
{
    for (int cnt=0; cnt<childcnt; ++cnt)
    {
        dirtyarry[cnt] = false;
        (*var)[cnt]->addCallback(this);
    }
}

decision_var_private_show::~decision_var_private_show()
{
    delete[] dirtyarry;
    // no need to remove callback from the children because by the time
    // the parent destructor is called, which destroys this show function
    // the children no longer exist
}

void decision_var_private_show::setInvalid()
{
    for (int cnt=0; cnt<childcnt; ++cnt)
    {
        dirtyarry[cnt] = false;
    }
    outidx = -1;
}

void decision_var_private_show::setDirty(mpt_var* var)
{
    if (enb)
    {
        int cnt = 0;
        for (; (cnt<childcnt) && (var != (*obj)[cnt]); ++cnt)
        {
            ;
        }
        if (cnt<childcnt)
        {
            dirtyarry[cnt] = true;
            if (outidx == -1)
            {
                outidx=cnt;
            }
        }
    }
}

void decision_var_private_show::operator()()
{
    if (!obj->isRMDirty())
    {
        // check if an outidx is currently active
        if ((outidx >= 0) && (outidx < childcnt))
        {
            // this outidx has probably been packeted out, remove it
            dirtyarry[outidx] = false;
            outidx=-1;
        }
        // check if there are pending packets to write out
        for (int cnt=0; cnt<childcnt; ++cnt)
        {
            if (dirtyarry[cnt])
            {
                outidx = cnt;
                obj->setRMDirty();
                break;
            }
        }
    }
}

void decision_var_private_show::setEnabled(bool enable)
{
    enb = enable;
}

decision_var::decision_var() : complex_var(), active(0), shw(0), lastout(-1)
{
}

decision_var::decision_var(const decision_var&) : complex_var(), active(0), shw(0), lastout(-1)
{
    // similar to complex_var's copy constructor because derived class macro will do it correctly
}

decision_var::~decision_var()
{
    delete shw;
}

unsigned int decision_var::getOutputChild() const
{
    int shwactive = -1;
    if (shw)
    {
        shwactive = shw->getOutIdx();
    }
    if (shwactive != -1)
    {
        // return index based on last show
        lastout = shwactive;
    }
    else if (lastout != -1)
    {
        // return last index that was written out
        shwactive = lastout;
    }
    else
    {
        shwactive = active;
    }
    // return last index that was last extracted
    return shwactive;
}

int decision_var::extract(int len, const unsigned char* buf)
{
    int sz = size();
    mpt_var* p = complex_var::operator [] (active);
    if (!p)
    {
        notDirty();
        len = (len > sz) ? (len - sz) : 0;
    }
    else
    {
        if (shw)
        {
            shw->setEnabled(false);
        }
        len = p->extract(len, buf);
        if (shw)
        {
            shw->setEnabled(true);
        }
    }
    return len;
}

void decision_var::output(outbuf& strm)
{
    mpt_var* p = complex_var::operator [](getOutputChild());
    if (p)
    {
        p->output(strm);
    }
}

int decision_var::size() const
{
    int max_size = 0;
    if (shw && isRMDirty())
    {
        int idx=shw->getOutIdx();
        if (idx>=0)
        {
            mpt_var* p = complex_var::operator[] (idx);
            if (p)
            {
                max_size = p->size();
            }
        }
    }

    if (max_size == 0)
    {
        const int c = cardinal();
        for (int i = 0; i < c; ++i)
        {
            mpt_var* p = complex_var::operator[] (i);
            int sz = p->size();
            if(sz > max_size)
            {
                max_size = sz;
            }
        }
    }
    return max_size;
}

const mpt_var& decision_var::operator=(const mpt_var& right)
{
    const decision_var* gen = RECAST(const decision_var*,&right);
    if (gen)
    {
        decision_var::operator=(* gen);
    }
    return *this;
}

const decision_var& decision_var::operator=(const decision_var& right)
{
    complex_var::operator=(right);
    active = right.active;
    return *this;
}

bool decision_var::operator==(const mpt_var& right) const
{
    const decision_var* gen = RECAST(const decision_var*,&right);
    return (gen) ? ((*this) == *gen) : complex_var::operator==(right);
}

bool decision_var::operator==(const decision_var& right) const
{
    return complex_var::operator==(right);
}

void decision_var::init_child_observers()
{
    delete shw;
    shw = new decision_var_private_show(this);
}

bool decision_var::setInvalid()
{
    if (shw)
    {
        shw->setInvalid();
    }
    return complex_var::setInvalid();
}
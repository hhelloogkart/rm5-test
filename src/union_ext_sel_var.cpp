/******************************************************************/
/* Copyright DSO National Laboratories 2008. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "union_ext_sel_var.h"

union_ext_sel_var::union_ext_sel_var(unsigned int (*selfx)(void), void (*writefx)(unsigned int)) :
    decision_var(), selector_fx(selfx), write_selector_fx(writefx)
{
}

union_ext_sel_var::union_ext_sel_var(const union_ext_sel_var& right) : decision_var()
{
    (*this) = right;
}

int union_ext_sel_var::extract(int len, const unsigned char* buf)
{
    if (selector_fx)
    {
        active = (*selector_fx)();
    }

    if ((active < cardinal()) && (active >= 0))
    {
        return decision_var::extract(len, buf);
    }
    else
    {
        active = 0;  // need to restore in case of problems with output
        int sz = size();
        notDirty();
        return (len > sz) ? len - sz : 0;
    }
}

void union_ext_sel_var::output(outbuf& strm)
{
    if (write_selector_fx)
    {
        (*write_selector_fx)(getOutputChild());
    }
    decision_var::output(strm);
}

const mpt_var& union_ext_sel_var::operator=(const mpt_var& right)
{
    const union_ext_sel_var* gen = RECAST(const union_ext_sel_var*,&right);
    if (gen)
    {
        union_ext_sel_var::operator=(* gen);
    }
    return *this;
}

const union_ext_sel_var& union_ext_sel_var::operator=(const union_ext_sel_var& right)
{
    decision_var::operator = ((const decision_var&)right);
    selector_fx = right.selector_fx;
    return *this;
}

bool union_ext_sel_var::operator==(const mpt_var& right) const
{
    const union_ext_sel_var* gen = RECAST(const union_ext_sel_var*,&right);
    if (gen)
    {
        return (*this) == *gen;
    }
    return decision_var::operator==(right);
}

bool union_ext_sel_var::operator==(const union_ext_sel_var& right) const
{
    if (selector_fx != right.selector_fx)
    {
        return false;
    }
    return decision_var::operator == ((const decision_var&)right);
}

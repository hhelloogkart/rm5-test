/******************************************************************/
/* Copyright DSO National Laboratories 2010. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "fec_var.h"
#include "rmstl.h"
#include "fec/fec.h"
#include <assert.h>
#include <string.h>
#include <outbuf.h>

#define MAX_BLOCKS      255
#define BLOCK_OVERHEAD  4  // 1 byte sequence, 8 bit num input blocks (k), 16 bit block number
#define OFFSET_SEQ      0
#define OFFSET_NBLK     1
#define OFFSET_CBLK     2

#define COMPUTE_M(k, correction)    (((100 + (correction)) * (k) + 99) / 100) //round up

static int build_decode_id(set<unsigned int> s, unsigned int k,
                           unsigned int* id_ptr, unsigned char** in_ptr, unsigned char** out_ptr,
                           unsigned char* data_ptr, unsigned blk_sz);
static bool decode(unsigned char* in_data, set<unsigned int> in_blks,
                   unsigned int n_input, unsigned int n_output,
                   unsigned int blk_sz);

struct fec_var_private
{
    unsigned int correction;    //correction factor
    unsigned int packet_size;   //size of a block

    //receive
    set<unsigned int> in_blks;  //block number of all received blocks
    unsigned char* in_data;     //input buffer, store all the received blocks of current frame
    unsigned int in_data_bsz;   //size of in_data buffer, in number of blocks
    unsigned int n_err_rx;      //number of error frames received that fail FEC decoding
    unsigned char n_input_rx;   //number of blocks of source data (k) of current received frame
    unsigned char cur_seq_rx;   //current received frame sequence number

    //send
    unsigned char* out_data;    //output buffer, store all the blocks of current frame to be sent
    unsigned int out_data_bsz;  //size of out_data buffer, in number of blocks
    unsigned int n_input_tx;    //current number of source blocks (k) to be sent
    unsigned int n_output_tx;   //current number of encoded blocks (m) to be sent
    unsigned int cur_blk_tx;    //current output block no
    unsigned char cur_seq_tx;   //current output sequence number
};

fec_var::fec_var(unsigned int bs, unsigned int c) :
    complex_var(), prvt(new fec_var_private)
{
    prvt->correction=c;
    prvt->packet_size=bs - BLOCK_OVERHEAD;
    prvt->in_data=0;
    prvt->in_data_bsz=0;
    prvt->n_err_rx=0;
    prvt->n_input_rx=0;
    prvt->cur_seq_rx=0;
    prvt->out_data=0;
    prvt->out_data_bsz=0;
    prvt->n_input_tx=0;
    prvt->n_output_tx=0;
    prvt->cur_blk_tx=0;
    prvt->cur_seq_tx=0;
#ifdef NDEBUG
    if(prvt->correction > 100)
    {
        prvt->correction = 100;
    }
    if(bs <= BLOCK_OVERHEAD)
    {
        prvt->packet_size = 5 * BLOCK_OVERHEAD;
    }
#else
    assert(prvt->correction >= 0 && prvt->correction <= 100);
    assert(bs > BLOCK_OVERHEAD);
#endif
}

fec_var::fec_var(const fec_var& right) :
    complex_var(), prvt(new fec_var_private)
{
    prvt->correction=right.prvt->correction;
    prvt->packet_size=right.prvt->packet_size;
    prvt->in_data=0;
    prvt->in_data_bsz=0;
    prvt->n_err_rx=0;
    prvt->n_input_rx=0;
    prvt->cur_seq_rx=0;
    prvt->out_data=0;
    prvt->out_data_bsz=0;
    prvt->n_input_tx=0;
    prvt->n_output_tx=0;
    prvt->cur_blk_tx=0;
    prvt->cur_seq_tx=0;
    (*this) = right;
}

fec_var::~fec_var()
{
    delete[] prvt->in_data;
    delete[] prvt->out_data;
    delete prvt;
}

int fec_var::extract(int len, const unsigned char* buf)
{
    if (len < (int)size())
    {
        return 0;
    }

    //rx new seq no => next frame, decode last frame
    if (buf[OFFSET_SEQ] != prvt->cur_seq_rx ||
        prvt->n_input_rx != buf[OFFSET_NBLK])
    {
        if (prvt->in_blks.size() >= prvt->n_input_rx)
        {
            if (decode(prvt->in_data, prvt->in_blks, prvt->n_input_rx, COMPUTE_M(prvt->n_input_rx, prvt->correction), prvt->packet_size))
            {
                complex_var::extract(prvt->n_input_rx * prvt->packet_size, prvt->in_data);
            }
            else
            {
                ++prvt->n_err_rx;
            }
        }
        else
        {
            ++prvt->n_err_rx;
        }
        prvt->cur_seq_rx = buf[OFFSET_SEQ];
        prvt->in_blks.clear();
        prvt->n_input_rx = buf[OFFSET_NBLK];
    }

    unsigned int n_output_rx = COMPUTE_M(prvt->n_input_rx, prvt->correction);
    if (n_output_rx > prvt->in_data_bsz)
    {
        delete[] prvt->in_data;
        prvt->in_data_bsz = n_output_rx;
        prvt->in_data = new unsigned char [prvt->in_data_bsz * prvt->packet_size];
        if (prvt->in_data == (unsigned char*)0)
        {
            prvt->in_data_bsz = 0;
            return 0;
        }
    }

    unsigned int c_blk = (unsigned int)buf[OFFSET_CBLK] | ((unsigned int)buf[OFFSET_CBLK+1] << 8); //current block
    if (c_blk < n_output_rx)
    {
        prvt->in_blks.insert(c_blk);
        memcpy(prvt->in_data + (c_blk * prvt->packet_size), buf + BLOCK_OVERHEAD, prvt->packet_size);
    }

    //start decode if all blocks received
    if (prvt->in_blks.size() >= n_output_rx)
    {
        if (decode(prvt->in_data, prvt->in_blks, prvt->n_input_rx, n_output_rx, prvt->packet_size))
        {
            complex_var::extract(prvt->n_input_rx * prvt->packet_size, prvt->in_data);
        }
        else
        {
            ++prvt->n_err_rx;
        }
        prvt->in_blks.clear();
        ++prvt->cur_seq_rx;
        prvt->n_input_rx = 0;
    }

    return len - size();
}

void fec_var::output(outbuf& strm)
{
    if ((int)(strm.maxsize() - strm.size()) < size())
    {
        return;
    }

    if (prvt->cur_blk_tx == 0)
    {
        prvt->n_input_tx = (complex_var::size() + prvt->packet_size - 1) / prvt->packet_size;
        assert(prvt->n_input_tx <= MAX_BLOCKS);
        prvt->n_output_tx = COMPUTE_M(prvt->n_input_tx, prvt->correction);

        //allocate buffer for encoded data
        if (prvt->n_output_tx > prvt->out_data_bsz)
        {
            delete[] prvt->out_data;
            prvt->out_data = new unsigned char [prvt->n_output_tx * prvt->packet_size];
            if (!prvt->out_data)
            {
                prvt->out_data_bsz = 0;
                return;
            }
            else
            {
                prvt->out_data_bsz = prvt->n_output_tx;
            }
        }

        //output original data into buffer
        outbuf ob;
        ob.set(prvt->out_data, prvt->n_output_tx * prvt->packet_size);
        complex_var::output(ob);

        //pad buffer to packet_size
        unsigned int sz = ob.size();
        sz %= prvt->packet_size;
        if (sz > 0)
        {
            for (; sz < prvt->packet_size; ++sz)
            {
                ob += '\xFF';
            }
        }

        unsigned char** ptr = new unsigned char*[prvt->n_output_tx];
        unsigned int* id_ptr = new unsigned int [prvt->n_output_tx - prvt->n_input_tx];
        bool err = true;

        if (ptr && id_ptr)
        {
            unsigned char* srcptr;
            unsigned int i, j;

            srcptr = prvt->out_data;
            for (i = 0; i < prvt->n_input_tx; ++i, srcptr += prvt->packet_size)
            {
                ptr[i] = srcptr;
            }

            for (j = 0; i < prvt->n_output_tx; ++i, ++j, srcptr += prvt->packet_size)
            {
                ptr[i] = srcptr;
                id_ptr[j] = i;
            }

            fec_t* fec_data = fec_new(prvt->n_input_tx, prvt->n_output_tx);
            if (fec_data)
            {
                fec_encode(fec_data, ptr, ptr + prvt->n_input_tx, id_ptr, prvt->n_output_tx - prvt->n_input_tx, prvt->packet_size);
                fec_free(fec_data);
                err = false;
            }
        }
        delete[] ptr;
        delete[] id_ptr;
        if (err)
        {
            return;
        }
    }

    //write header to output stream
    strm += prvt->cur_seq_tx;
    strm += (unsigned char)prvt->n_input_tx;
    strm += (unsigned char)prvt->cur_blk_tx;
    strm += (unsigned char)(prvt->cur_blk_tx >> 8);

    //write to output stream
    unsigned char* p = prvt->out_data + prvt->cur_blk_tx * prvt->packet_size;
    unsigned char* endp = p + prvt->packet_size;
    for (; p < endp; ++p)
    {
        strm += *p;
    }

    //calculate next block no
    ++prvt->cur_blk_tx;
    if (prvt->cur_blk_tx >= prvt->n_output_tx)
    {
        prvt->cur_blk_tx = 0;
        ++prvt->cur_seq_tx;
    }
    else
    {
        setRMDirty();
    }
}

int fec_var::size() const
{
    return prvt->packet_size + BLOCK_OVERHEAD;
}

const mpt_var& fec_var::operator=(const mpt_var& right)
{
    const fec_var* gen = RECAST(const fec_var*,&right);
    if (gen)
    {
        fec_var::operator=(*gen);
    }
    return *this;
}

const fec_var& fec_var::operator=(const fec_var& right)
{
    if (this != &right)
    {
        complex_var::operator = ((const complex_var&)right);
    }
    return *this;
}

unsigned int fec_var::getRxErrors()
{
    return prvt->n_err_rx;
}

/**
   build the pointer and id array needed by fec_decode.
   @return -1 if data cannot be decoded, 0 if all input are available (ie no need FEC), 1 if need FEC.
   @param k [in] number of source data block in a frame, array size of id_ptr, in_ptr and out_ptr
   @param id_ptr [out] array[k] of received block number
   @param in_ptr [out] array[k] of block pointer corresponding to the block_number
   @param out_ptr [out] array[k] of block pointer for block_number >= k in id_ptr
   @param data_ptr [in] the pointer to data size (m * blk_sz), where m is no of FEC encoded blks in a frame
   @param blk_sz [in] the block size of each input data block (without BLOCK_OVERHEAD)
 */
static int build_decode_id(set<unsigned int> s, unsigned int k,
                           unsigned int* id_ptr, unsigned char** in_ptr, unsigned char** out_ptr,
                           unsigned char* data_ptr, unsigned blk_sz)
{
    set<unsigned int>::iterator it;
    set<unsigned int>::iterator fec_it, fec_it2; //point to first id of FEC block
    unsigned int i = 0;
    int ret = 0;

    fec_it = fec_it2 = s.lower_bound(k);

    for (it = s.begin(); (i < k) && (it != fec_it); ++i, ++id_ptr, ++in_ptr)
    {
        //check if the ith block is received?
        if (*it == i)
        {
            *id_ptr = i;
            ++it;
        }
        else if (fec_it2 != s.end()) //if not, use the FEC block
        {
            *id_ptr = *fec_it2;
            *out_ptr = data_ptr + i * blk_sz;
            ++out_ptr;
            ++fec_it2;
            ret = 1;
        }
        else //there is not enough correct frames to recover
        {
            return -1;
        }
        *in_ptr = data_ptr + *id_ptr * blk_sz;
    }
    for (; i < k && it != s.end(); ++it, ++i, ++id_ptr, ++in_ptr)
    {
        *id_ptr = *it;
        *out_ptr = data_ptr + i * blk_sz;
        *in_ptr = data_ptr + *id_ptr * blk_sz;
        ret = 1;
    }

    return i >= k ? ret : -1;
}

static bool decode(unsigned char* in_data, set<unsigned int> in_blks,
                   unsigned int n_input, unsigned int n_output,
                   unsigned int blk_sz)
{
    unsigned int* id_ptr = new unsigned int [n_input];
    unsigned char** in_ptr = new unsigned char*[n_input];
    unsigned char** out_ptr = new unsigned char*[n_input];
    int ret = -1;

    if (id_ptr && in_ptr && out_ptr)
    {
        ret = build_decode_id(in_blks, n_input, id_ptr, in_ptr, out_ptr, in_data, blk_sz);
        if (ret > 0)
        {
            fec_t* fec_data = fec_new(n_input, n_output);
            if (fec_data)
            {
                fec_decode(fec_data, in_ptr, out_ptr, id_ptr, blk_sz);
                fec_free(fec_data);
            }
            else
            {
                ret = -1;
            }
        }
    }

    delete[] id_ptr;
    delete[] in_ptr;
    delete[] out_ptr;
    return ret >= 0;
}

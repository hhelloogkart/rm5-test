#include <decodeprefix_var.h>
#include <prefiximpl.h>

vdecode_prefix_var::vdecode_prefix_var(double _scale, double _offset) :
    scaling(_scale), offset(_offset)
{
}

int vdecode_prefix_var::extract(int buflen, const unsigned char* buf)
{
    double value = 0;
    const int len = decodeValue(value, reinterpret_cast<const char*>(buf), buflen);
    if (len > 0)
    {
        value = value * scaling + offset;
        generic_var<double>::extract(sizeof(value), reinterpret_cast<unsigned char*>(&value));
    }
    return buflen - len;
}

void vdecode_prefix_var::output(outbuf& strm)
{
    char buf[9];
    const double value = decode_round((data - offset) / scaling);
    const int len = encodeValue(value, buf);
    for (int cnt = 0; cnt < len; ++cnt)
    {
        strm += buf[cnt];
    }
}

int vdecode_prefix_var::size() const
{
    char buf[9];
    const double value = decode_round((data - offset) / scaling);
    return encodeValue(value, buf);
}

mpt_var* vdecode_prefix_var::getNext()
{
    return this + 1;
}

vdecode_prefix_unsigned_var::vdecode_prefix_unsigned_var(double _scale, double _offset) :
    vdecode_prefix_var(_scale, _offset)
{
}

const mpt_var& vdecode_prefix_unsigned_var::operator=(const mpt_var& right)
{
    return generic_var<double>::operator=(right);
}

double vdecode_prefix_unsigned_var::operator=(double right)
{
    return generic_var<double>::operator=(right);
}

const vdecode_prefix_unsigned_var& vdecode_prefix_unsigned_var::operator=(const vdecode_prefix_unsigned_var& right)
{
    generic_var<double>::operator=(right);
    return *this;
}

int vdecode_prefix_unsigned_var::encodeValue(double value, char* output) const
{
    return prefix_encode<unsigned long long>(output, static_cast<unsigned long long>(value));
}

int vdecode_prefix_unsigned_var::decodeValue(double& value, const char* buf, int buflen) const
{
    unsigned long long ival = 0;
    const int len = prefix_decode<unsigned long long>(buf, buflen, ival);
    value = static_cast<double>(ival);
    return len;
}

vdecode_prefix_signed_var::vdecode_prefix_signed_var(double _scale, double _offset) :
    vdecode_prefix_var(_scale, _offset)
{
}

const mpt_var& vdecode_prefix_signed_var::operator=(const mpt_var& right)
{
    return generic_var<double>::operator=(right);
}

double vdecode_prefix_signed_var::operator=(double right)
{
    return generic_var<double>::operator=(right);
}

const vdecode_prefix_signed_var& vdecode_prefix_signed_var::operator=(const vdecode_prefix_signed_var& right)
{
    generic_var<double>::operator=(right);
    return *this;
}

int vdecode_prefix_signed_var::encodeValue(double value, char* output) const
{
    return prefix_encode<long long>(output, static_cast<long long>(value));
}

int vdecode_prefix_signed_var::decodeValue(double& value, const char* buf, int buflen) const
{
    long long ival = 0;
    const int len = prefix_decode<long long>(buf, buflen, ival);
    value = static_cast<double>(ival);
    return len;
}

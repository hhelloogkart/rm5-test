/******************************************************************/
/* Copyright DSO National Laboratories 2017. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "arry_var.h"
#include "vlu_var.h"
#include <string>

extern RM_EXPORT void string_stream_helper_read(istream& s, char*, int);
extern RM_EXPORT void string_stream_helper_write(ostream& s, const char*, int);

typedef std::string array_str_var_str;

array_str_var::array_str_var() : array_var<char>(), pvt(new array_str_var_str)
{
}

array_str_var::array_str_var(const array_str_var& cpy) : array_var<char>(cpy), pvt(new array_str_var_str)
{
}

array_str_var::array_str_var(const char* cpy) : array_var<char>(), pvt(new array_str_var_str)
{
    from_char_ptr(0, cpy);
}

array_str_var::~array_str_var()
{
    delete reinterpret_cast<array_str_var_str*>(pvt);
}

mpt_var* array_str_var::getNext()
{
    return this+1;
}

const array_str_var& array_str_var::operator=(const array_str_var& right)
{
    return static_cast<const array_str_var&>(array_value_var::operator=(right));
}

const array_str_var& array_str_var::operator=(const char* right)
{
    from_char_ptr(0, right);
    return *this;
}

const mpt_var& array_str_var::operator=(const mpt_var& right)
{
    return array_value_var::operator=(right);
}

bool array_str_var::operator==(const array_str_var& right) const
{
    return operator==(right.data);
}

bool array_str_var::operator==(const mpt_var& right) const
{
    bool ret = false;
    const array_str_var* gen = RECAST(const array_str_var*,&right);
    if (gen != 0)
    {
        ret = operator==(gen->data);
    }
    else
    {
        const value_var* vlu = RECAST(const value_var*, &right);
        if (vlu != 0)
        {
            ret = operator==(vlu->to_const_char_ptr());
        }
        else
        {
            ret = mpt_var::operator==(right);
        }
    }
    return ret;
}

bool array_str_var::operator==(const char* right) const
{
    bool eq = true;

    if (!sz)
    {
        eq = (right == 0);
    }
    else if (right == 0)
    {
        eq = false;
    }
    else
    {
        const char* p = data;
        const char* q = data + sz;
        while (p != q)
        {
            if (*p != *right)
            {
                eq = false;
                p = q; // sneaky way to leave the loop
            }
            else if (*p == '\0')
            {
                eq = true;
                p = q; // sneaky way to leave the loop
            }
            else
            {
                ++p;
                ++right;
            }
        }
    }
    return eq;
}

bool array_str_var::operator==(char* right) const
{
    return operator==(static_cast<const char*>(right));
}

istream& array_str_var::operator>>(istream& s)
{
    char buf[4096];
    string_stream_helper_read(s, buf, sizeof(buf));
    from_char_ptr(0, buf);
    return s;
}

ostream& array_str_var::operator<<(ostream& s) const
{
    if (isValid())
    {
        // the string is used to ensure a null termination
        const array_str_var_str str(data, sz);
        string_stream_helper_write(s, str.c_str(), sz);
    }
    else
    {
        s << "\"\"";
    }
    return s;
}

char array_str_var::operator[](int idx) const
{
    return (static_cast<unsigned int>(idx) < sz) ? data[idx] : 0;
}

char* array_str_var::to_char_ptr(unsigned int)
{
    if (data != 0) setDirty();
    return data;
}

const char* array_str_var::to_const_char_ptr(unsigned int) const
{
    array_str_var_str* pvtptr=reinterpret_cast<array_str_var_str*>(pvt);
    pvtptr->assign(data, sz);
    return pvtptr->c_str();
}

void array_str_var::from_char_ptr(unsigned int, const char* right)
{
    unsigned int nsz = static_cast<unsigned int>(strlen(right)) + 1; // account for null

    if (nsz > 1)
    {
        char* ndata = new char[nsz];
        memcpy(ndata, right, nsz);
        if ((static_cast<int>(sz) != nsz) || (memcmp(data, ndata, sz) != 0))
        {
            setDirty();
            setRMDirty();
            delete[] data;
            data = ndata;
            sz = nsz;
        }
        else
        {
            notDirty();
            delete[] ndata;
        }
    }
    else
    {
        if (sz == 0)
        {
            notDirty();
        }
        else
        {
            nullValue();
        }
    }
}

char array_str_var::to_char(unsigned int) const
{
    char result=0;
    if (sz > 0)
    {
        value_var::str_to_value(data, result);
    }
    return result;
}

unsigned char array_str_var::to_unsigned_char(unsigned int) const
{
    unsigned char result=0;
    if (sz > 0)
    {
        value_var::str_to_value(data, result);
    }
    return result;
}

short array_str_var::to_short(unsigned int) const
{
    short result=0;
    if (sz > 0)
    {
        value_var::str_to_value(data, result);
    }
    return result;
}

unsigned short array_str_var::to_unsigned_short(unsigned int) const
{
    unsigned short result=0;
    if (sz > 0)
    {
        value_var::str_to_value(data, result);
    }
    return result;
}

int array_str_var::to_int(unsigned int) const
{
    int result=0;
    if (sz > 0)
    {
        value_var::str_to_value(data, result);
    }
    return result;
}

unsigned int array_str_var::to_unsigned_int(unsigned int) const
{
    unsigned int result=0;
    if (sz > 0)
    {
        value_var::str_to_value(data, result);
    }
    return result;
}

long array_str_var::to_long(unsigned int) const
{
    long result=0;
    if (sz > 0)
    {
        value_var::str_to_value(data, result);
    }
    return result;
}

unsigned long array_str_var::to_unsigned_long(unsigned int) const
{
    unsigned long result=0;
    if (sz > 0)
    {
        value_var::str_to_value(data, result);
    }
    return result;
}

long long array_str_var::to_long_long(unsigned int) const
{
    long long result = 0;
    if (sz > 0)
    {
        value_var::str_to_value(data, result);
    }
    return result;
}

unsigned long long array_str_var::to_unsigned_long_long(unsigned int) const
{
    unsigned long long result = 0;
    if (sz > 0)
    {
        value_var::str_to_value(data, result);
    }
    return result;
}

float array_str_var::to_float(unsigned int) const
{
    float result=0;
    if (sz > 0)
    {
        value_var::str_to_value(data, result);
    }
    return result;
}

double array_str_var::to_double(unsigned int) const
{
    double result=0;
    if (sz > 0)
    {
        value_var::str_to_value(data, result);
    }
    return result;
}

void array_str_var::from_char(unsigned int, char val)
{
    from_char_ptr(0, value_var::value_to_str(val));
}

void array_str_var::from_unsigned_char(unsigned int, unsigned char val)
{
    from_char_ptr(0, value_var::value_to_str(val));
}

void array_str_var::from_short(unsigned int, short val)
{
    from_char_ptr(0, value_var::value_to_str(val));
}

void array_str_var::from_unsigned_short(unsigned int, unsigned short val)
{
    from_char_ptr(0, value_var::value_to_str(val));
}

void array_str_var::from_int(unsigned int, int val)
{
    from_char_ptr(0, value_var::value_to_str(val));
}

void array_str_var::from_unsigned_int(unsigned int, unsigned int val)
{
    from_char_ptr(0, value_var::value_to_str(val));
}

void array_str_var::from_long(unsigned int, long val)
{
    from_char_ptr(0, value_var::value_to_str(val));
}

void array_str_var::from_unsigned_long(unsigned int, unsigned long val)
{
    from_char_ptr(0, value_var::value_to_str(val));
}

void array_str_var::from_long_long(unsigned int, long long val)
{
    from_char_ptr(0, value_var::value_to_str(val));
}

void array_str_var::from_unsigned_long_long(unsigned int, unsigned long long val)
{
    from_char_ptr(0, value_var::value_to_str(val));
}

void array_str_var::from_float(unsigned int, float val)
{
    from_char_ptr(0, value_var::value_to_str(val));
}

void array_str_var::from_double(unsigned int, double val)
{
    from_char_ptr(0, value_var::value_to_str(val));
}

unsigned int array_str_var::rtti() const
{
    return rm_hash_string("array_str_var");
}
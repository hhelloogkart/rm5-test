/******************************************************************/
/* Copyright DSO National Laboratories 2016. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "decode_complement_var.h"

inline static int byte_size_to_code_typ(int bsz)
{
    return (bsz < 0) ? (0x500 | ((-bsz * 8) & 0xff)) : (0x400 | ((bsz * 8) & 0xff));
}

decode_complement_var::decode_complement_var(int _byte_size, double _range, double _offset, int _scale_offset) :
    vdecode_var(byte_size_to_code_typ(_byte_size), _range, _offset, _scale_offset), sign_mask(0xffffffff << (((_byte_size < 0) ? -_byte_size : _byte_size) *8-1))
{
}

const mpt_var& decode_complement_var::operator=(const mpt_var& right)
{
    return vdecode_var::operator=(right);
}

mpt_var* decode_complement_var::getNext()
{
    return this + 1;
}

double decode_complement_var::operator=(double right)
{
    return vdecode_var::operator=(right);
}

const decode_complement_var& decode_complement_var::operator=(const decode_complement_var& right)
{
    vdecode_var::operator=(right);
    return *this;
}

inline unsigned int complement(unsigned int enc, unsigned int sign_mask)
{
    return (enc & sign_mask) ? (~((enc & ~sign_mask) - 1) | sign_mask) : enc;
}

int decode_complement_var::encodeValueSigned(double val)
{
    return complement(vdecode_var::encodeValueSigned(val), sign_mask);
}

double decode_complement_var::decodeValueSigned(int enc)
{
    return vdecode_var::decodeValueSigned(complement(enc, sign_mask));
}
/******************************************************************/
/* Copyright DSO National Laboratories 2020. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include <defutil.h>
#include <defaults.h>
#include <mpt_var.h>
#include <delyshow.h>
#include "rmstl.h"
#include <assert.h>
#include <signal.h>
#ifdef _WIN32
#include <windows.h>
#endif
#ifndef  NO_DOUT
#include <util/dbgout.h>
#endif // ! NO_DOUT
#ifndef NO_LOG
#include "logfile.h"
#endif

typedef mpt_LinkedList<const protocol_description*> protlist_typ;

static protlist_typ& prots()
{
    static protlist_typ p;
    return p;
}

class show_exit : public delay_show
{
public:
    friend class rmclient_init;
    show_exit();
    ~show_exit();
    virtual void setDirty(mpt_var*);
    virtual void notDirty(mpt_var*);
    virtual void operator()();

private:
    bool exit_flag;
};
static show_exit* ex_sh = 0;

static void signalled(int)
{
    if (ex_sh)
    {
        ex_sh->setDirty(0);
    }
}

show_exit::show_exit() : delay_show(5, 0), exit_flag(false)
{
    signal(SIGINT, &signalled);
    signal(SIGTERM, &signalled);
}

show_exit::~show_exit()
{
    signal(SIGINT, SIG_DFL); // parasoft-suppress  MISRA2008-5_2_4 "Tool error"
    signal(SIGTERM, SIG_DFL); // parasoft-suppress  MISRA2008-5_2_4 "Tool error"
}

void show_exit::setDirty(mpt_var* var)
{
    delay_show::setDirty(var);
    exit_flag = true;
}

void show_exit::notDirty(mpt_var* var)
{
    setDirty(var);
}

void show_exit::operator()()
{
    if (getDirty())
    {
        exit(0);
    }
#ifdef _WIN32
    MSG msg;
    if (PeekMessage(&msg, 0, WM_CLOSE, WM_CLOSE, PM_REMOVE))
    {
        exit_flag = true;
    }
#endif
}

rmclient_init::rmclient_init(const char* defstr, int argc, char** argv) : _defptr(load_defaults(defstr, (argc > 0) ? argv[0] : 0)), _delonexit(_defptr)
{
    if (argc > 0)
    {
        _defptr->parse_cmdline(argc, argv);
    }
    rmclient_init_c();
}

rmclient_init::rmclient_init(defaults* defptr, int argc, char** argv) : _defptr(defptr), _delonexit(0)
{
    if (argc > 0)
    {
        _defptr->parse_cmdline(argc, argv);
    }
    rmclient_init_c();
}

rmclient_init::~rmclient_init()
{
    protlist_typ& p = prots();
    for (protlist_typ::iterator it = p.begin(); !it.end(); ++it)
    {
        if ((*it)->deinitf)
        {
            (*(*it)->deinitf)();
        }
    }
    p.clear();
    delete ex_sh;
    ex_sh = 0;
    delete _delonexit;
#ifndef NO_LOG
    VarLogFile::disable();
#endif
    shutdownIosl();
}

bool rmclient_init::client_exit()
{
    return !ex_sh || ex_sh->exit_flag;
}

void rmclient_init::incoming()
{
    protlist_typ& p = prots();
    for (protlist_typ::iterator it = p.begin(); !it.end(); ++it)
    {
        if ((*it)->incomingf)
        {
            (*(*it)->incomingf)();
        }
    }
}

void rmclient_init::outgoing()
{
    protlist_typ& p = prots();
    for (protlist_typ::iterator it = p.begin(); !it.end(); ++it)
    {
        if ((*it)->outgoingf)
        {
            (*(*it)->outgoingf)();
        }
    }
}

void rmclient_init::install_protocol(const protocol_description& des, bool force)
{
    protlist_typ& p = prots();
    for (protlist_typ::iterator it = p.begin(); !it.end(); ++it)
    {
        if ((*it)->prot == des.prot)
        {
            if (force)
            {
                *it = &des;
            }
            return;
        }
    }
    p.push_back(&des);
}

void rmclient_init::rmclient_init_c()
{
    assert(ex_sh == 0);

#if !defined(NO_DOUT) || !defined(NO_LOG)
    char hoststr[256];
#endif
#if !defined(NO_DOUT)
    int port = 0;
    dout.useconsole(_defptr->getnode("console") != NULL);
    if ((_defptr->getstring("log", hoststr) != NULL) && (hoststr[0] != '\0'))
    {
        dout.openfile(hoststr);
    }
    if ((_defptr->getstring("udpaddr", hoststr) != NULL) &&
        (_defptr->getint("udport", &port) != NULL))
    {
        dout.openudp(hoststr, port);
    }
#endif
#ifndef NO_LOG
    if (_defptr->getstring("datamonlog", hoststr) != NULL)
    {
        VarLogFile::enable(hoststr);
    }
#endif
    ex_sh = new show_exit;

    protlist_typ& p = prots();
    for (protlist_typ::iterator it = p.begin(); !it.end(); ++it)
    {
        if ((*it)->initf)
        {
            (*(*it)->initf)(_defptr);
        }
    }
}

void rmclient_init::install_exit_trigger(mpt_var* var)
{
    if (ex_sh)
    {
        var->addCallback(ex_sh);
    }
}

void rmclient_init::exit()
{
    if (ex_sh)
    {
        ex_sh->exit_flag = true;
    }
}
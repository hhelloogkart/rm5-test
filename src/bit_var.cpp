/******************************************************************/
/* Copyright DSO National Laboratories 2008. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "bit_var.h"
#include <outbuf.h>
#include <cplx_var.h>
#include <decode_var.h>

unsigned int vbit_var::rtti() const
{
    // treat all bit_vars the same, since they are all generic_var<unsigned int>
    return hash_string("bit_var");
}

vbit_var::vbit_var(int sz, int bz) : bit_size(static_cast<unsigned char>((sz < 0) ? -sz : sz)), use_bit(static_cast<unsigned char>(bz)), sign_ext((sz < 0) ? 0xffffffff << (-sz - 1) : 0)
{
    complex_var* prnt = static_cast<complex_var*>(this->parent);
    if ((prnt == 0) ||
        (prnt->cardinal() <= 1) ||
        (rtti() != (*prnt)[prnt->cardinal()-2]->rtti()))
    {
        offset = 0;
    }
    else
    {
        // previous sibling is also a bit_var
        const vbit_var* bro = dynamic_cast<const vbit_var*>((*prnt)[prnt->cardinal() - 2]);
        if (!bro)
        {
            vdecode_var* decbro = dynamic_cast<vdecode_var*>((*prnt)[prnt->cardinal() - 2]);
            if (decbro)
            {
                bro = dynamic_cast<const vbit_var*>(decbro->getMsg());
            }
        }
        offset = (bro) ? ((bro->offset + (bro->bit_size % static_cast<unsigned char>(use_bit))) % use_bit) : 0;
    }
}

vbit_var::vbit_var(const vbit_var& cpy) : generic_var<unsigned int>(cpy), bit_size(cpy.bit_size), use_bit(cpy.use_bit), sign_ext(cpy.sign_ext)
{
    complex_var* prnt = static_cast<complex_var*>(this->parent);
    if ((prnt == 0) ||
        (prnt->cardinal() <= 1) ||
        (rtti() != (*prnt)[prnt->cardinal()-2]->rtti()))
    {
        offset = 0;
    }
    else
    {
        // previous sibling is also a bit_var
        const vbit_var* bro = dynamic_cast<const vbit_var*>((*prnt)[prnt->cardinal()-2]);
        if (!bro)
        {
            vdecode_var* decbro = dynamic_cast<vdecode_var*>((*prnt)[prnt->cardinal() - 2]);
            if (decbro)
            {
                bro = dynamic_cast<const vbit_var*>(decbro->getMsg());
            }
        }
        offset = (bro) ? ((bro->offset + (bro->bit_size % static_cast<unsigned char>(use_bit))) % use_bit) : 0;
    }
}

int vbit_var::extract(int buflen, const unsigned char* buf)
{
    unsigned int ndata = 0;
    int curroff = offset;    // the current offset
    int used = 0;            // number of new bytes consumed
    --buf;               // start from previous byte
    for (int currbit = 0; currbit < bit_size; ++currbit)
    {
        if (curroff == 0)
        {
            if (buflen == 0)
            {
                return -1;
            }
            ++used;
            ++buf;
        }
        // extract one bit from curroff into curr bit
        unsigned char mask = static_cast<unsigned char>(1) << curroff;
        ndata += static_cast<unsigned int>(((*buf & mask) != 0)) << currbit;
        // increment curroff
        ++curroff;
        curroff %= use_bit;
    }
    if ((ndata & sign_ext) != 0)
    {
        ndata |= sign_ext;
    }
    if (ndata == data)
    {
        notDirty();
    }
    else
    {
        data = ndata;
        setDirty();
    }
    return buflen - used;
}

bool vbit_var::extract_zero(const unsigned char* buf)
{
    return extract(0, buf) == 0;
}

int vbit_var::size() const
{
    if (offset == 0)
    {
        // simple math will do
        bool overflow = ((bit_size % use_bit) != 0);
        return (bit_size/use_bit) + static_cast<int>(overflow);
    }
    else
    {
        // check if we overflow the previous char
        if ((offset + bit_size) <= use_bit)
        {
            // no overflow
            return 0;
        }
        else
        {
            // subtract the bits in previous char
            int leftover = static_cast<int>(bit_size) - use_bit + offset;
            // again use simple math
            bool overflow = ((leftover % use_bit) != 0);
            return (leftover/use_bit) + static_cast<int>(overflow);
        }
    }
}

void vbit_var::output(outbuf& strm)
{
    int curroff = offset;    // the current offset
    int currbit = 0;         // the current bit
    unsigned int val = data; // temp value
    if (curroff != 0)
    {
        unsigned char* buf = strm.getcur() - 1;
        for (; (currbit < bit_size) && (curroff!=0); ++currbit, (val>>=1))
        {
            *buf += static_cast<unsigned char>((val % 2) << curroff);
            ++curroff;
            curroff %= use_bit;
        }
    }
    while(currbit < bit_size)
    {
        unsigned char cval = 0;
        for (; (currbit < bit_size) && (curroff < use_bit); ++currbit, ++curroff, (val>>=1))
        {
            cval += static_cast<unsigned char>((val % 2) << curroff);
        }
        strm += cval;
        curroff = 0;
    }
}

mpt_var* vbit_var::getNext()
{
    return this+1;
}

unsigned int vbit_var::operator=(unsigned int right)
{
    return generic_var<unsigned int>::operator=(right);
}

const vbit_var& vbit_var::operator=(const vbit_var& right)
{
    generic_var<unsigned int>::operator=(right.data);
    return *this;
}

const mpt_var& vbit_var::operator=(const mpt_var& right)
{
    return generic_var<unsigned int>::operator=(right);
}

bool vbit_var::operator==(unsigned int right) const
{
    return data == right;
}

bool vbit_var::operator==(const vbit_var& right) const
{
    return this->data == right.data;
}

bool vbit_var::operator==(const mpt_var& right) const
{
    return generic_var<unsigned int>::operator==(right);
}

vbit_var::operator unsigned int() const
{
    return data;
}

unsigned int rvbit_var::rtti() const
{
    // to differentiate between the bit_var brothers
    return hash_string("rbit_var");
}

rvbit_var::rvbit_var(int sz, int bz) : vbit_var(sz, bz)
{
    /* Important, we reimplement the entire offset code here because rtti() gives a different
       value in this class compared to the base class */

    complex_var* prnt = static_cast<complex_var*>(this->parent);
    if ((prnt == 0) ||
        (prnt->cardinal() <= 1) ||
        (rtti() != (*prnt)[prnt->cardinal()-2]->rtti()))
    {
        offset = 0;
    }
    else
    {
        // previous sibling is also a rbit_var
        const vbit_var* bro = dynamic_cast<const vbit_var*>((*prnt)[prnt->cardinal() - 2]);
        if (!bro)
        {
            vdecode_var* decbro = dynamic_cast<vdecode_var*>((*prnt)[prnt->cardinal() - 2]);
            if (decbro)
            {
                bro = dynamic_cast<const vbit_var*>(decbro->getMsg());
            }
        }
        offset = (bro) ? ((bro->get_offset() + (bro->get_bit_size() % static_cast<unsigned char>(use_bit))) % use_bit) : 0;
    }
}

rvbit_var::rvbit_var(const rvbit_var& cpy) : vbit_var(cpy)
{
    /* Important, we reimplement the entire offset code here because rtti() gives a different
       value in this class compared to the base class */

    complex_var* prnt = static_cast<complex_var*>(this->parent);
    if ((prnt == 0) ||
        (prnt->cardinal() <= 1) ||
        (rtti() != (*prnt)[prnt->cardinal()-2]->rtti()))
    {
        offset = 0;
    }
    else
    {
        // previous sibling is also a rbit_var
        const vbit_var* bro = dynamic_cast<const vbit_var*>((*prnt)[prnt->cardinal() - 2]);
        if (!bro)
        {
            vdecode_var* decbro = dynamic_cast<vdecode_var*>((*prnt)[prnt->cardinal() - 2]);
            if (decbro)
            {
                bro = dynamic_cast<const vbit_var*>(decbro->getMsg());
            }
        }
        offset = (bro) ? ((bro->get_offset() + (bro->get_bit_size() % static_cast<unsigned char>(use_bit))) % use_bit) : 0;
    }
}

int rvbit_var::extract(int buflen, const unsigned char* buf)
{
    unsigned int ndata = 0;
    int curroff = offset;   // the current offset
    int used = 0;           // number of new bytes consumed
    --buf;                  // start from previous byte
    for (int currbit = 0; currbit < bit_size; ++currbit)
    {
        if (curroff == 0)
        {
            if (buflen == 0)
            {
                return -1;
            }
            ++used;
            ++buf;
        }
        unsigned char tmp = *buf; // we do not want to modify the original buffer
        reverse_byte(&tmp);
        // extract one bit from curroff into curr bit
        unsigned char mask = static_cast<unsigned char>(1) << curroff;
        ndata += static_cast<unsigned int>((tmp & mask) != 0) << currbit;
        // increment curroff
        ++curroff;
        curroff %= use_bit;
    }
    reverse_uint(&ndata);
    if ((ndata & sign_ext) != 0)
    {
        ndata |= sign_ext;
    }
    if (ndata == data)
    {
        notDirty();
    }
    else
    {
        data = ndata;
        setDirty();
    }
    return buflen - used;
}

void rvbit_var::output(outbuf& strm)
{
    int curroff = offset;       // the current offset
    int currbit = 0;            // the current bit
    unsigned int val = data;    // temp value
    reverse_uint(&val);
    if (curroff != 0)
    {
        unsigned char* buf = strm.getcur() - 1;
        for (; (currbit < bit_size) && (curroff < use_bit); ++currbit, ++curroff, (val>>=1))
        {
            *buf |= (val & 1) << (use_bit-curroff-1);
        }
        curroff = 0;
    }
    while(currbit < bit_size)
    {
        unsigned char cval = 0;
        for (; (currbit < bit_size) && (curroff < use_bit); ++currbit, ++curroff, (val>>=1))
        {
            cval |= (val & 1) << (use_bit-curroff-1);
        }
        strm += cval;
        curroff = 0;
    }
}

void rvbit_var::reverse_byte(unsigned char* byte)
{
    unsigned char tmp = 0;
    unsigned int coff = use_bit - 1;
    for (unsigned int cbit = 0; cbit < use_bit; ++cbit, --coff)
    {
        unsigned char mask = static_cast<unsigned char>(1) << coff;
        tmp += static_cast<unsigned int>((*byte & mask) != 0) << cbit;
    }
    (*byte) = tmp;
}

void rvbit_var::reverse_uint(unsigned int* uint)
{
    unsigned int tmp = 0;
    int curroff = bit_size - 1;
    for (int currbit = 0; currbit < static_cast<int>(bit_size); ++currbit, --curroff)
    {
        unsigned int mask = static_cast<unsigned int>(1) << curroff;
        tmp += static_cast<unsigned int>((*uint & mask) != 0) << currbit;
    }
    (*uint) = tmp;
}

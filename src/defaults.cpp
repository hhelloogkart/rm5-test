/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifdef _WIN32
#pragma warning(disable: 4996 4100)
#endif

#include "defaults.h"
#include "rmstl.h"
#include <util/utilcore.h>
#include <pcre/pcre.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifndef OSPACE
# include <algorithm>
# ifndef RWSTD_NO_NAMESPACE
using std::equal_range;
using std::sort;
using std::count_if;
using std::count;
# endif
#else
# include <ospace/std/algorithm>
using os_std::equal_range;
using os_std::sort;
using os_std::count_if;
using os_std::count;
#endif

typedef string string_typ;
struct tree_node
{
    pair<string_typ, string_typ> d;
    tree_node()
    {
    }
    tree_node(const tree_node& r) : d(r.d.first, r.d.second)
    {
    }
    tree_node(string_typ s1, string_typ s2) : d(s1, s2)
    {
    }
};

typedef VECTOR(tree_node) def_tree_typ;
typedef def_tree_typ::iterator tree_iter;
typedef pair<tree_iter, tree_iter> tree_iter_pair;

struct defaults_private
{
    def_tree_typ data;
    tree_node envnode;
    bool changed;
};

extern RM_EXPORT size_t extract_line(const char* str, const char** next, size_t& len);
extern RM_EXPORT size_t extract_equal(const char* str, const char** after, size_t& lenbef);
extern RM_EXPORT void clear_whitespace(const char** str, size_t& len);

defaults* load_defaults(const char* def_config, const char* prog_name)
{
    defaults* defptr = new defaults;
    if (def_config == NULL)
    {
        file_buffer fbuf("");
        if (prog_name != NULL)
        {
            char nm[1024];
            rm_strcpy(nm, 1024, prog_name);
#ifdef _WIN32
            size_t len = strlen(nm);
            if ((nm[len-1] == 'e') &&
                (nm[len-2] == 'x') &&
                (nm[len-3] == 'e') &&
                (nm[len-4] == '.'))
            {
                nm[len-4] = '\0';
            }
#endif
            rm_strcat(nm, 1024, ".cfg");
            if (fbuf.setfile(nm, true))
            {
                defptr->load(fbuf);
                return defptr;
            }
        }
        if (fbuf.setfile("rmclient.cfg"))
        {
            defptr->load(fbuf);
        }
    }
    else
    {
        if (def_config[0] == '+')
        {
            file_buffer fbuf(&(def_config[1]), true);
            defptr->load(fbuf);
        }
        else
        {
            defptr->load(def_config);
        }
    }
    return defptr;
}

size_t extract_line(const char* str, const char** next, size_t& len)
{
    size_t cnt = 0;
    for (; (cnt < len) && (str[cnt] != '\xa') && (str[cnt] != '\0'); ++cnt)
    {
        ;
    }
    if ((cnt >= len) || (str[cnt] == '\0'))
    {
        *next = 0;
        len = 0;
    }
    else
    {
        *next = str + cnt + 1;
        len = len - cnt - 1;
    }
    return ((cnt > 0) && (str[cnt-1] == '\xd')) ? (cnt - 1) : cnt;
}

size_t extract_equal(const char* str, const char** after, size_t& lenbef)
{
    size_t lenaft = 0;
    *after = 0;
    for (size_t cnt = 0; cnt < lenbef; ++cnt)
    {
        if (str[cnt] == '=')
        {
            // found it
            lenaft = lenbef - cnt - 1;
            lenbef = cnt;
            *after = str + cnt + 1;
            break;
        }
    }
    return lenaft;
}

void clear_whitespace(const char** str, size_t& len)
{
    // clear space in front
    for (; (len > 0) && ((*str)[0] <= '\x20'); --len, ++(*str))
    {
        ;
    }
    // clear space at back
    for (; (len > 1) && ((*str)[len - 1] <= '\x20'); --len)
    {
        ;
    }
}

static int regexp_find(const char* pattern, const char* subject, char* buffer = NULL, int buffersize = 0)
{
    const char* errptr;
    int erroffset = 0;
    int ovector[300];
    pcre* exp = pcre_compile(pattern, 0, &errptr, &erroffset, NULL);
    int match = pcre_exec(exp, NULL, subject, static_cast<int>(strlen(subject)), 0, 0, ovector, 300);
    if ((buffer != NULL) && (buffersize > 0))
    {
        if (match > 0)
        {
            pcre_copy_substring(subject, ovector, match, 1, buffer, buffersize);
        }
        else
        {
            buffer[0] = '\0';
        }
    }
    if (pcre_free)
    {
        (*pcre_free)(exp);
    }
    else
    {
        free(exp);
    }
    return match > 0;
}

static string_typ previous_key(const string_typ& key)
{
    char pkey[KEY_SIZE], lkey[KEY_SIZE];
    /* check if directory -- generate bare key */
    if (regexp_find("^(.*)/$", key.c_str(), pkey, KEY_SIZE))
    {
        return string_typ(pkey);
    }
    /* check if not @@1 -- generate lower number */
    if (regexp_find("@@(\\d+)$", key.c_str(), pkey, KEY_SIZE))
    {
        const int tcnt = atoi(pkey);
        if (tcnt > 1)
        {
            regexp_find("^(.*?)\\d+$", key.c_str(), pkey, KEY_SIZE);
            rm_sprintf(lkey, KEY_SIZE, "%d", tcnt - 1);
            rm_strcat(pkey, KEY_SIZE, lkey);
            return string_typ(pkey);
        }
    }
    /* remove up to the next slash */
    regexp_find("^(.*/).*?$", key.c_str(), pkey, KEY_SIZE);
    return string_typ(pkey);
}

static string_typ mangle_key(const char* key, const char* parent)
{
    string_typ nkey;
    char pkey[KEY_SIZE], lkey[KEY_SIZE];
    if (key[0] == '\0')
    {
        return (parent[0] != '\0') ? string_typ(parent) : string_typ("/");
    }
    if ((key[0] != '/') && (parent[0] != '\0'))
    {
        regexp_find("(.*/).*?", parent, pkey, KEY_SIZE);
        nkey += pkey;
    }
    else
    {
        nkey += '/';
    }
    rm_strcpy(lkey, KEY_SIZE, key);
    while (lkey[0] != '\0')
    {
        if (!regexp_find("(.*?)/.*", lkey, pkey, KEY_SIZE))
        {
            break;
        }
        if (pkey[0] != '\0')
        {
            nkey += pkey;
            if (!regexp_find("@@\\d+$", pkey))
            {
                nkey += "@@1";
            }
            nkey += '/';
        }
        rm_strcpy(pkey, KEY_SIZE, lkey);
        regexp_find(".*?/(.*)", pkey, lkey, KEY_SIZE);
    }
    if (lkey[0] != '\0')
    {
        nkey += lkey;
        if (!regexp_find("@@\\d+$", lkey))
        {
            if (regexp_find(".*/(.*?)$", nkey.c_str(), pkey, KEY_SIZE) &&
                regexp_find(".*/(.*?)@@\\d+$", parent, lkey, KEY_SIZE) &&
                (strcmp(pkey, lkey) == 0))
            {
                regexp_find("@@(\\d+)$", parent, lkey, KEY_SIZE);
                rm_sprintf(pkey, KEY_SIZE, "@@%d", atoi(lkey) + 1);
                nkey += pkey;
            }
            else
            {
                nkey += "@@1";
            }
        }
    }
    return nkey;
}

static string_typ replace_keys(const char* data, defaults* def)
{
    const char* errptr;
    int erroffset = 0;
    string_typ str = data;
    int ovector[3];
    int offset = 0;

    pcre* re = pcre_compile("%\\S+?%", 0, &errptr, &erroffset, 0);
    while (pcre_exec(re, 0, str.c_str(), static_cast<int>(str.length()), offset, 0, ovector, 3) >= 0)
    {
        char tval[1024];
        string_typ tail(str.substr(ovector[1]));
        string_typ key(str.substr(ovector[0]+1, ovector[1]-ovector[0]-2));

        str.resize(ovector[0]);
        if (def->getstring(key.c_str(), tval))
        {
            str.append(tval);
        }
        else
        {
            str.append(1, '%');
            str.append(key);
            str.append(1, '%');
            offset = ovector[0] + 1;
        }
        str.append(tail);
    }
    if (pcre_free)
    {
        (*pcre_free)(re);
    }
    else
    {
        free(re);
    }
    return str;
}

class tree_compare
{
public:
    bool operator()(const tree_node& lhs, const tree_node& rhs) const
    {
        return keyLess(lhs.d.first, rhs.d.first);
    }
    bool operator()(const tree_node& lhs, const string_typ& k) const
    {
        return keyLess(lhs.d.first, k);
    }
    bool operator()(const string_typ& k, const tree_node& rhs) const
    {
        return keyLess(k, rhs.d.first);
    }
private:
    bool keyLess(const string_typ& k1, const string_typ& k2) const
    {
        return k1 < k2;
    }
};

class tree_eq
{
public:
    tree_eq(const string_typ& _rhs) : rhs(_rhs)
    {
    }
    tree_eq(const tree_eq& _cpy) : rhs(_cpy.rhs)
    {
    }
    bool operator()(const tree_node& lhs) const
    {
        char pkey[KEY_SIZE];
        return (regexp_find("(.*)@@\\d+$", lhs.d.first.c_str(), pkey, KEY_SIZE)) ? (rhs == pkey) : (rhs == lhs.d.first);
    }
private:
    string_typ rhs;
};

defaults::defaults()
    : prvt(new defaults_private)
{
    prvt->changed = false;
}

defaults::defaults(const char* _data, size_t len)
    : prvt(new defaults_private)
{
    prvt->changed = false;
    load(_data, len);
}

defaults::~defaults()
{
    delete prvt;
}

void defaults::load(file_buffer& filebuf)
{
    size_t len = 0;
    const char* _data = filebuf.readfile(len);
    load(_data, len);
}

#ifndef NO_OUTBUF
void defaults::save(file_buffer& filebuf)
{
    unsigned char buf[16384];
    outbuf outdata;
    outdata.set(buf, 16384);
    save(outdata);
    filebuf.writefile(reinterpret_cast<const char*>(buf), outdata.size());
}

void defaults::save(outbuf& outdata, const char* prepend_str, int prepend_len)
{
    string_typ::size_type cnt = 1, slashcnt = 0;
    tree_iter iter = prvt->data.begin();
    char pkey[KEY_SIZE];
    ++iter;
    for (; iter != prvt->data.end(); ++iter)
    {
        outdata.append(prepend_str, prepend_len);
#if defined(_WIN32) || defined(NET_OS) || (defined(__GNUG__) && (__GNUG__ >= 3))
        slashcnt = count((*iter).d.first.begin(), (*iter).d.first.end(), '/');
#else
        slashcnt = 0;
        count((*iter).d.first.begin(), (*iter).d.first.end(), '/', slashcnt);
#endif
        if (slashcnt > cnt)
        {
            outdata.append(static_cast<int>(cnt) - 1, '\t');
            outdata.append("{\n");
            cnt = slashcnt;
            continue;
        }
        else if (slashcnt < cnt)
        {
            cnt = slashcnt;
            outdata.append(static_cast<int>(cnt) - 1, '\t');
            outdata.append("}\n");
        }
        outdata.append(static_cast<int>(cnt) - 1, '\t');
        regexp_find(".*/(.*?)@@\\d+$", (*iter).d.first.c_str(), pkey, KEY_SIZE);
        outdata.append(pkey);
        if (!(*iter).d.second.empty())
        {
            outdata.append(" = ");
            outdata.append((*iter).d.second.data(), static_cast<int>((*iter).d.second.length()));
        }
        outdata += '\n';
    }
    for (; cnt > 1; --cnt)
    {
        outdata.append(prepend_str, prepend_len);
        outdata.append(static_cast<int>(cnt) - 2, '\t');
        outdata.append("}\n");
    }
}
#endif

void defaults::load(const char* _data, size_t len)
{
    size_t lenbef = 0, lenaft = 0;
    const char* nextline = 0, * before = 0, * after = 0;
    string_typ tkey("/"), tvalue, parent(tkey);
    char pkey[KEY_SIZE];

    if (_data == NULL)
    {
        return;
    }

    if (len == 0)
    {
        len = strlen(_data);
    }

    clear();
    prvt->data.push_back(tree_node(tkey, string_typ()));

    // process line by line
    for (before = _data; before != NULL; before = nextline)
    {
        if ((lenbef = extract_line(before, &nextline, len)) <= 0)
        {
            continue;
        }

        lenaft = extract_equal(before, &after, lenbef);
        clear_whitespace(&before, lenbef);
        if (lenbef <= 0)
        {
            continue;
        }
        if (before[0] == '#')
        {
            continue;
        }
        if (before[0] == '{')
        {
            tkey += '/';
            parent = getnode(tkey.c_str(), NULL, true)->d.first;
        }
        else if (before[0] == '}')
        {
            regexp_find("(^.*/).*?/$", parent.c_str(), pkey, KEY_SIZE);
            parent = getnode(pkey, NULL)->d.first;
        }
        else
        {
            tkey.assign(before, lenbef);
            if (regexp_find("(/|@@\\d+)", tkey.c_str()))
            {
                continue;
            }
            tkey = parent + tkey;
#if defined(_WIN32) || defined(NET_OS) || (defined(__GNUG__) && (__GNUG__ >= 3))
            unsigned int n = static_cast<unsigned int>(count_if(prvt->data.begin(), prvt->data.end(), tree_eq(tkey)));
#else
            unsigned int n = 0;
            count_if(prvt->data.begin(), prvt->data.end(), tree_eq(tkey), n);
#endif
            rm_sprintf(pkey, KEY_SIZE, "@@%u", n+1);
            tkey += pkey;
            if (lenaft)
            {
                clear_whitespace(&after, lenaft);
                if (lenaft > 0)
                {
                    tvalue.assign(after, lenaft);
                }
                else
                {
                    tvalue.erase();
                }
            }
            else
            {
                tvalue.erase();
            }
            setValue(tkey.c_str(), tvalue.c_str());
        }
    }
}

void defaults::parse_cmdline(int argc, char** argv)
{
    string_typ nkey, nvalue;

    for (int cnt = 1; cnt < argc; ++cnt)
    {
        int len = 0;
        for (; argv[cnt][len] != '\0'; ++len)
        {
            if (argv[cnt][len] == '=')
            {
                break;
            }
        }
        nkey.assign(argv[cnt], len);
        if (argv[cnt][len] == '=')
        {
            /* Has both key and value */
            nvalue.assign(argv[cnt]+len+1);
        }
        else
        {
            /* Only has the key */
            nvalue.erase();
        }
        setValue(nkey.c_str(), nvalue.c_str());
    }
}

void defaults::clear()
{
    prvt->data.clear();
}

tree_node* defaults::getenvnode(const char* srchkey)
{
#if (defined(_MSC_VER) && (_MSC_VER >= 1400))
    char eptr[256];
    size_t requiredSize = 0;
    if ((getenv_s( &requiredSize, eptr, 256, srchkey) == 0) && (eptr[0] != '\0'))
    {
        prvt->envnode.d.first = srchkey;
        prvt->envnode.d.second = eptr;
        return &prvt->envnode;
    }
    else if (requiredSize > 0)
    {
        char* ptr = new char[requiredSize];
        getenv_s(&requiredSize, ptr, requiredSize, srchkey);
        prvt->envnode.d.first = srchkey;
        prvt->envnode.d.second = ptr;
        delete[] ptr;
        return &prvt->envnode;
    }
#else
    char* eptr = getenv(srchkey);
    if (eptr != (char*)NULL)
    {
        prvt->envnode.d.first = srchkey;
        prvt->envnode.d.second = eptr;
        return &prvt->envnode;
    }
#endif
    return 0;
}

tree_node* defaults::getnode(const char* srchkey, const tree_node* node, bool force)
{
    string_typ key = mangle_key(srchkey, (node != NULL) ? node->d.first.c_str() : "");
    tree_iter_pair pr = equal_range(prvt->data.begin(), prvt->data.end(), key, tree_compare());

    if (pr.first == pr.second)
    {
        /* Not found */
        if (!force)
        {
            return (node==0) ? getenvnode(srchkey) : 0;
        }
        def_tree_typ tdata;
        string_typ tkey(key);
        tdata.push_back(tree_node(tkey, string_typ()));
        while (!tkey.empty())
        {
            tkey = previous_key(tkey);
            if (getnode(tkey.c_str(), NULL, false) != NULL)
            {
                break;
            }
            tdata.push_back(tree_node(tkey, string_typ()));
        }
        prvt->data.insert(prvt->data.end(), tdata.begin(), tdata.end());
        sort(prvt->data.begin(), prvt->data.end(), tree_compare());
        pr = equal_range(prvt->data.begin(), prvt->data.end(), key, tree_compare());
    }
    return &(*(pr.first));
}

tree_node* defaults::getstring(const char* srchkey, char* datavalue, const tree_node* node)
{
    tree_node* tnode;
    if ((tnode = getnode(srchkey, node)) != 0)
    {
        if(datavalue)
        {
            const string_typ result = replace_keys(tnode->d.second.c_str(), this);
            strcpy(datavalue, result.c_str());
        }
        return tnode;
    }
    return 0;
}

tree_node* defaults::getdata(const char* srchkey, char* datavalue, const tree_node* node)
{
    tree_node* tnode;
    if ((tnode = getnode(srchkey, node)) != 0)
    {
        if(datavalue)
        {
            memcpy(datavalue, tnode->d.second.data(), tnode->d.second.length());
        }
        return tnode;
    }
    return 0;
}

tree_node* defaults::getint(const char* srchkey, int* datavalue, const tree_node* node)
{
    char str[FIELD_LEN];
    char* ptr;
    tree_node* retnode = getstring(srchkey, str, node);
    if (retnode == 0)
    {
        return 0;
    }
    const int value = static_cast<int>(strtoul(str, &ptr, 0));
    if (ptr == str)
    {
        return 0;
    }
    if(datavalue)
    {
        *datavalue= value;
    }
    return retnode;
}

tree_node* defaults::getdbl(const char* srchkey, double* datavalue, const tree_node* node)
{
    char str[FIELD_LEN];
    char* ptr;
    double value = 0;
    tree_node* retnode;
    retnode = getstring(srchkey, str, node);
    if (retnode == 0)
    {
        return 0;
    }
    value = strtod(str, &ptr);
    if (ptr == str)
    {
        return 0;
    }
    if(datavalue)
    {
        *datavalue = value;
    }
    return retnode;
}

tree_node* defaults::getChild(tree_node* node)
{
    if (node == NULL)
    {
        return &(prvt->data[0]);
    }
    if (regexp_find("/$", node->d.first.c_str()))
    {
        return node;
    }

    string_typ key(node->d.first);
    key += '/';
    tree_iter_pair pr = equal_range(prvt->data.begin(), prvt->data.end(), key, tree_compare());
    if (pr.first != pr.second)
    {
        return &(*(pr.first));
    }
    return NULL;
}

const char* defaults::getString(const char* srchkey, bool* b_fail)
{
    static char str[FIELD_LEN];
    if (getstring(srchkey, str) == 0)
    {
        str[0] = '\0';
        if (b_fail)
        {
            *b_fail = true;
        }
    }
    else
    {
        if (b_fail)
        {
            *b_fail = false;
        }
    }
    return str;
}

int defaults::getInt(const char* srchkey, bool* b_fail)
{
    int result = 0;
    bool flag = (getint(srchkey, &result) == 0);
    if (b_fail)
    {
        *b_fail = flag;
    }
    return result;
}

double defaults::getDouble(const char* srchkey, bool* b_fail)
{
    double result = 0;
    bool flag = (getdbl(srchkey, &result) == 0);
    if (b_fail)
    {
        *b_fail = flag;
    }
    return result;
}

tree_node* defaults::setValue(const char* srchkey, const char* datavalue, tree_node* node)
{
    tree_node* ptr = getnode(srchkey, node, true);
    if (ptr)
    {
        ptr->d.second = datavalue;
    }
    else
    {
        // error message here
    }
    return ptr;
}
tree_node* defaults::setValue(const char* srchkey, const char* datavalue, int datalen, tree_node* node)
{
    tree_node* ptr = getnode(srchkey, node, true);
    if (ptr)
    {
        ptr->d.second.assign(datavalue, datalen);
    }
    else
    {
        // error message here
    }
    return ptr;
}
tree_node* defaults::setValue(const char* srchkey, int datavalue, tree_node* node)
{
    char str[FIELD_LEN];
    rm_sprintf(str, FIELD_LEN, "%d", datavalue);
    return setValue(srchkey, str, node);
}

tree_node* defaults::setValue(const char* srchkey, double datavalue, tree_node* node)
{
    char str[FIELD_LEN];
    rm_sprintf(str, FIELD_LEN, "%f", datavalue);
    return setValue(srchkey, str, node);
}

void bin2asc(const void* binsrc, char* asctgt, unsigned int sz)
{
    const char* ascsrc = reinterpret_cast<const char*>(binsrc);
    for (unsigned int cnt = 0; cnt < sz; ++cnt)
    {
        const unsigned char uch = ascsrc[cnt];
        asctgt[cnt * 2 + 0] = (uch >> 4) + 'a';
        asctgt[cnt * 2 + 1] = (uch & 0xF) + 'a';
    }
}

void asc2bin(void* bintgt, const char* ascsrc, unsigned int sz)
{
    char* asctgt = reinterpret_cast<char*>(bintgt);
    for (unsigned int cnt = 0; cnt < sz; ++cnt)
    {
        asctgt[cnt] = ((ascsrc[cnt * 2 + 0] - 'a') << 4) |
                      ((ascsrc[cnt * 2 + 1] - 'a') & 0xF);
    }
}

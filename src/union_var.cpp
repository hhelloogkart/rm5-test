/******************************************************************/
/* Copyright DSO National Laboratories 2008. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "union_var.h"
#include <outbuf.h>
#include "rmstl.h"
#include <stdlib.h>

typedef VECTOR(unsigned int) selectorlist_typ;

struct union_var_private
{
    unsigned int offset;
    unsigned int selsize;
    selectorlist_typ selectors;
};

/**
   @param off specify selector is at off bytes from start of union
   @param sz byte size of selector
   @param selstr string containing decimal value for the selector.
              if number of selector < cardinal, the last child is used as default selector.
              the default selector output value will be last selector value + 1.
              NULL to use default selector values of 0, 1, 2...
 */
union_var::union_var(unsigned int off, unsigned int sz, const char* selstr) :
    decision_var(), prvt(new union_var_private)
{
    prvt->offset = off;
    prvt->selsize = sz;
    setSelectors(selstr);
}

union_var::union_var(const union_var& right) : decision_var(), prvt(new union_var_private)
{
    (*this) = right;
}

union_var::~union_var()
{
    delete prvt;
}

int union_var::extract(int len, const unsigned char* buf)
{
    unsigned int selector;
    if (extract_selector(len, buf, selector))
    {
        const int tmpact = findChild(selector);
        if (tmpact >= 0)
        {
            active = tmpact;
            return decision_var::extract(len, buf);
        }
    }
    notDirty();
    return 0;
}

void union_var::output(outbuf& strm)
{
    unsigned char* buf = strm.getcur();
    decision_var::output(strm);
    output_selector(static_cast<int>(strm.getcur() - buf), buf, getSelector(getOutputChild()));
}

const mpt_var& union_var::operator=(const mpt_var& right)
{
    const union_var* gen = RECAST(const union_var*,&right);
    if (gen)
    {
        union_var::operator=(*gen);
    }
    return *this;
}

const union_var& union_var::operator=(const union_var& right)
{
    decision_var::operator = ((const decision_var&)right);
    prvt->offset = right.prvt->offset;
    prvt->selsize = right.prvt->selsize;
    prvt->selectors = right.prvt->selectors;
    return *this;
}

bool union_var::operator==(const mpt_var& right) const
{
    const union_var* gen = RECAST(const union_var*,&right);
    if (gen)
    {
        return (*this) == *gen;
    }
    return decision_var::operator==(right);
}

bool union_var::operator==(const union_var& right) const
{
    if ((prvt->offset != right.prvt->offset) ||
        (prvt->selsize != right.prvt->selsize) ||
        (prvt->selectors != right.prvt->selectors))
    {
        return false;
    }
    return decision_var::operator == ((const decision_var&)right);
}

bool union_var::extract_selector(int buflen, const unsigned char* buf, unsigned int& sel)
{
    sel = 0;
    // treat as an unflip integer of 4 bytes
    if (buflen < (int)(prvt->offset + prvt->selsize))
    {
        return false;
    }
    unsigned char* p = reinterpret_cast<unsigned char*>(&sel), * q = p + prvt->selsize;
    buf += prvt->offset;
#ifndef FLIP
    while (p != q)
    {
        *p = *buf;
        ++p;
        ++buf;
    }
#else
    --q;
    --p;
    while (p != q)
    {
        *q = *buf;
        --q;
        ++buf;
    }
#endif
    return true;
}

bool union_var::output_selector(int buflen, unsigned char* buf, unsigned int sel)
{
    // treat as an unflip integer of 4 bytes
    if (buflen < (int)(prvt->offset + prvt->selsize))
    {
        return false;
    }
    unsigned char* p = reinterpret_cast<unsigned char*>(&sel), * q = p + prvt->selsize;
    buf += prvt->offset;
#ifndef FLIP
    while (p != q)
    {
        *buf = *p;
        ++p;
        ++buf;
    }
#else
    --q;
    --p;
    while (p != q)
    {
        *buf = *q;
        --q;
        ++buf;
    }
#endif
    return true;
}

/**
   Find the child base on selector value
   @param value selector value
   @return index into selectors array
 */
int union_var::findChild(unsigned int value) const
{
    selectorlist_typ::const_iterator p;
    unsigned int i = 0;
    if (prvt->selectors.size() == 0)
    {
        i = value;
        if (i < (unsigned int)cardinal())
        {
            return i;
        }
    }
    else
    {
        for (p = prvt->selectors.begin(); p != prvt->selectors.end(); ++p, ++i)
        {
            if (value == (*p))
            {
                return i;
            }
        }
        if (cardinal() > (int)prvt->selectors.size())
        {
            return cardinal() - 1;
        }
    }
    return -1;
}

/**
   Get the selector value base on index into selectors array
   @param idx index into selectors array
   @return selector value at idx
 */
unsigned int union_var::getSelector(unsigned int idx) const
{
    if (prvt->selectors.size() == 0)
    {
        return (idx);
    }
    else if (idx < (unsigned int)prvt->selectors.size())
    {
        return prvt->selectors[idx];
    }
    return prvt->selectors[prvt->selectors.size() - 1] + 1;
}

/**
   Set the selector value base on index into selectors array
   @param idx index into selectors array
   @param value selector value
 */
void union_var::setSelector(unsigned int idx, unsigned int value)
{
    if ((int)idx < cardinal())
    {
        if(prvt->selectors.size() <= idx)
        {
            prvt->selectors.resize(idx + 1);
        }
        prvt->selectors[idx] = value;
    }
}

/**
   Set the selector value base on index into selectors array
   @param idx index into selectors array
   @param value selector value
 */
void union_var::setSelectors(const char* selstr)
{
    char* endptr;
    int sel;

    prvt->selectors.clear();

    if (selstr == NULL)
    {
        return;
    }
    while (*selstr)
    {
        while (*selstr == ',' || isspace(*selstr))
        {
            ++selstr;
        }
        sel = strtol(selstr, &endptr, 0);
        if (selstr == endptr)
        {
            break;
        }
        selstr = endptr;
        prvt->selectors.push_back(sel);
    }
}

r_union_var::r_union_var(unsigned int off, unsigned int sz, const char* selstr) :
    union_var(off, sz, selstr)
{
}

r_union_var::r_union_var(const r_union_var& right) : union_var(right)
{
}

bool r_union_var::extract_selector(int buflen, const unsigned char* buf, unsigned int& sel)
{
    sel = 0;
    // treat as an unflip integer of 4 bytes
    if (buflen < (int)(prvt->offset + prvt->selsize))
    {
        return false;
    }
    unsigned char* p = reinterpret_cast<unsigned char*>(&sel), * q = p + prvt->selsize;
    buf += prvt->offset;
#ifdef FLIP
    while (p != q)
    {
        *p = *buf;
        ++p;
        ++buf;
    }
#else
    --q;
    --p;
    while (p != q)
    {
        *q = *buf;
        --q;
        ++buf;
    }
#endif
    return true;
}

bool r_union_var::output_selector(int buflen, unsigned char* buf, unsigned int sel)
{
    // treat as an unflip integer of 4 bytes
    if (buflen < (int)(prvt->offset + prvt->selsize))
    {
        return false;
    }
    unsigned char* p = reinterpret_cast<unsigned char*>(&sel), * q = p + prvt->selsize;
    buf += prvt->offset;
#ifdef FLIP
    while (p != q)
    {
        *buf = *p;
        ++p;
        ++buf;
    }
#else
    --q;
    --p;
    while (p != q)
    {
        *buf = *q;
        --q;
        ++buf;
    }
#endif
    return true;
}

const mpt_var& r_union_var::operator=(const mpt_var& right)
{
    return union_var::operator=(right);
}

const r_union_var& r_union_var::operator=(const r_union_var& right)
{
    union_var::operator=(right);
    return *this;
}

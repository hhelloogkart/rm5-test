/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "cache.h"
#include <util/dbgout.h>
#include <util/utilcore.h>

#include <stdlib.h>     // dummy include for NULL
#include <limits.h>

cache_typ* cache_typ::h_allocate(size_t sz)
{
    cache_typ* ptr = reinterpret_cast<cache_typ*>(malloc((sz * sizeof(unsigned int)) + sizeof(cache_typ)));
    if(ptr)
    {
        ptr->construct(sz);
    }
    return ptr;
}
cache_typ* cache_typ::h_duplicate(unsigned int id, size_t len, const unsigned int* src)
{
    cache_typ* dst = h_allocate(len);
    if(dst)
    {
        dst->set_id(id);
        unsigned int* dstbuf = dst->buf();

        for (unsigned int cnt = 0; cnt < len; ++cnt)
        {
            dstbuf[cnt] = src[cnt];
        }
    }
    return dst;
}

void cache_typ::h_deallocate(cache_typ* ptr)
{
    free(ptr);
}

size_t cache_typ::output(size_t len, unsigned int* buff) const
{
    const unsigned int* srcbuf = buf();
    const size_t blen = (len < get_len()) ? len : get_len();

    for (size_t cnt = 0; cnt < blen; ++cnt)
    {
        buff[cnt] = srcbuf[cnt];
    }

    return blen;
}

cache_typ::cache_typ(size_t _len)
{
    construct(_len);
}

void cache_typ::construct(size_t _len)
{
    ref = 1;
    header.magic = BE(MAGICNUM);
#ifndef LARGE_PACKET
    const unsigned int id = (SETLIST << 24);
#else
    const unsigned int id = (SETLIST << 24) | (static_cast<unsigned int>(_len) & 0xFF0000);
#endif
    header.id = BE(id);
    header.len = BE(static_cast<unsigned short>(_len));
}

const unsigned int* cache_typ::buf() const
{
    return reinterpret_cast<const unsigned int*>(this+1);
}

unsigned int* cache_typ::buf()
{
    return reinterpret_cast<unsigned int*>(this+1);
}

const unsigned int* cache_typ::packet() const
{
    return reinterpret_cast<const unsigned int*>(&header);
}

unsigned int* cache_typ::packet()
{
    return reinterpret_cast<unsigned int*>(&header);
}

void cache_typ::h_reserve()
{
    ++ref;
}

void cache_typ::h_unreserve()
{
    if (--ref == 0)
    {
        h_deallocate(this);
    }
}

void cache_typ::copy_header(unsigned int* buf)
{
    header_typ* ptr = reinterpret_cast<header_typ*>(buf);
    header.id  = ptr->id;
    header.seq = ptr->seq;
    header.len = ptr->len;
}

bool cache_typ::check_header() const
{
    return (header.magic == BE(MAGICNUM)) && (get_cmd() < ENDCMD);
}

cache_typ* cache_typ::CastToCache(void* buf, bool init)
{
    cache_typ* ptr = reinterpret_cast<cache_typ*>(buf);
    if (init)
    {
        ptr->ref = INT_MAX;
    }
    return ptr;
}

cache_typ* cache_typ::ConvertToCache(void* buf, bool init)
{
    cache_typ* ptr = reinterpret_cast<cache_typ*>(reinterpret_cast<char*>(buf) - sizeof(cache_typ) + sizeof(header_typ));
    if (init)
    {
        ptr->ref = INT_MAX;
    }
    return ptr;
}

cache_typ* cache_typ::ConvertToCache(void* buf, unsigned int _len, unsigned int _cmd)
{
    cache_typ* ptr = reinterpret_cast<cache_typ*>(reinterpret_cast<char*>(buf) - sizeof(cache_typ));
    ptr->ref = INT_MAX;
    ptr->set_cmd(_cmd);
    ptr->set_len(_len);
    return ptr;
}

unsigned int cache_typ::get_id() const
{
#ifndef LARGE_PACKET
    return BE(header.id) & 0xFFFFFF;
#else
    return BE(header.id) & 0xFFFF;
#endif
}
void cache_typ::set_id(unsigned int _id)
{
#ifndef LARGE_PACKET
    header.id = BE((BE(header.id) & 0xFF000000) | (_id & 0xFFFFFF));
#else
    header.id = BE((BE(header.id) & 0xFFFF0000) | (_id & 0xFFFF));
#endif
}
unsigned int cache_typ::get_len() const
{
#ifndef LARGE_PACKET
    return BE(header.len);
#else
    return BE(header.len) + (BE(header.id) & 0xFF0000);
#endif
}
void cache_typ::set_len(unsigned int _len)
{
    header.len = BE(static_cast<unsigned short>(_len));
#ifdef LARGE_PACKET
    header.id = BE((BE(header.id) & 0xFF00FFFF) | (_len & 0xFF0000));
#endif
}
unsigned short cache_typ::get_seq() const
{
    return BE(header.seq);
}
void cache_typ::set_seq(unsigned short _seq)
{
    header.seq = BE(_seq);
}
unsigned int cache_typ::get_cmd() const
{
    return BE(header.id) >> 24;
}
void cache_typ::set_cmd(unsigned int _cmd)
{
    header.id = BE((BE(header.id) & 0xFFFFFF) | (_cmd << 24));
}

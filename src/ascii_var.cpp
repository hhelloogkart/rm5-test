/******************************************************************/
/* Copyright DSO National Laboratories 2010. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include <ascii_var.h>
#include <outbuf.h>
#include <pcre/pcre.h>
#include <string.h>
#include <stdio.h>

#ifdef _MSC_VER
# if (_MSC_VER >= 1400)
#  define rm_snscanf _snscanf_s
# else
#  define rm_snscanf _snscanf
# endif
#else
#include <stdarg.h>
inline int rm_snscanf(const char* in, int inlen, const char* fmt, ...)
{
    va_list args;
    char wfmt[16];
    sprintf(wfmt, "%%%d%s", inlen, fmt + 1);
    va_start(args, fmt);
    const int ret = vsscanf(in, wfmt, args);
    va_end(args);
    return ret;
}
#endif

static const int ASCBUFSZ=256;
static const int ALLONES=0xFF;

static void cvt_skip(const char*, int, unsigned char*, int)
{
}

static void cvt_float(const char* in, int inlen, unsigned char* out, int)
{
    float val = 0;
    const int ret = rm_snscanf(in, inlen, "%f", &val);
    if ((ret == EOF) || (ret == 0))
    {
        memset(out, ALLONES, sizeof(float));
    }
    else
    {
        memcpy(out, &val, sizeof(float));
    }
}

static void cvt_double(const char* in, int inlen, unsigned char* out, int)
{
    double val = 0;
    const int ret = rm_snscanf(in, inlen, "%lf", &val);
    if ((ret == EOF) || (ret == 0))
    {
        memset(out, ALLONES, sizeof(double));
    }
    else
    {
        memcpy(out, &val, sizeof(double));
    }
}

static void cvt_char_signed(const char* in, int inlen, unsigned char* out, int)
{
    int val = 0;
    const int ret = rm_snscanf(in, inlen, "%d", &val);
    if ((ret == EOF) || (ret == 0))
    {
        memset(out, ALLONES, sizeof(char));
    }
    else
    {
        const char cval = static_cast<char>(val);
        *out = *(reinterpret_cast<const unsigned char*>(&cval));
    }
}

static void cvt_char_unsigned(const char* in, int inlen, unsigned char* out, int)
{
    int val = 0;
    const int ret = rm_snscanf(in, inlen, "%u", &val);
    if ((ret == EOF) || (ret == 0))
    {
        memset(out, ALLONES, sizeof(char));
    }
    else
    {
        *out = static_cast<unsigned char>(val);
    }
}

static void cvt_char_hex(const char* in, int inlen, unsigned char* out, int)
{
    int val = 0;
    const int ret = rm_snscanf(in, inlen, "%x", &val);
    if ((ret == EOF) || (ret == 0))
    {
        memset(out, ALLONES, sizeof(char));
    }
    else
    {
        *out = static_cast<unsigned char>(val);
    }
}

static void cvt_short_signed(const char* in, int inlen, unsigned char* out, int)
{
    int val = 0;
    const int ret = rm_snscanf(in, inlen, "%d", &val);
    if ((ret == EOF) || (ret == 0))
    {
        memset(out, ALLONES, sizeof(short));
    }
    else
    {
        const short cval = static_cast<short>(val);
        memcpy(out, &cval, sizeof(short));
    }
}

static void cvt_short_unsigned(const char* in, int inlen, unsigned char* out, int)
{
    unsigned int val = 0;
    const int ret = rm_snscanf(in, inlen, "%u", &val);
    if ((ret == EOF) || (ret == 0))
    {
        memset(out, ALLONES, sizeof(short));
    }
    else
    {
        const unsigned short cval = static_cast<unsigned short>(val);
        memcpy(out, &cval, sizeof(unsigned short));
    }
}

static void cvt_short_hex(const char* in, int inlen, unsigned char* out, int)
{
    unsigned int val = 0;
    const int ret = rm_snscanf(in, inlen, "%x", &val);
    if ((ret == EOF) || (ret == 0))
    {
        memset(out, ALLONES, sizeof(short));
    }
    else
    {
        const unsigned short cval = static_cast<unsigned short>(val);
        memcpy(out, &cval, sizeof(unsigned short));
    }
}

static void cvt_int_signed(const char* in, int inlen, unsigned char* out, int)
{
    int val = 0;
    const int ret = rm_snscanf(in, inlen, "%d", &val);
    if ((ret == EOF) || (ret == 0))
    {
        memset(out, ALLONES, sizeof(int));
    }
    else
    {
        memcpy(out, &val, sizeof(int));
    }
}

static void cvt_int_unsigned(const char* in, int inlen, unsigned char* out, int)
{
    unsigned int val = 0;
    const int ret = rm_snscanf(in, inlen, "%u", &val);
    if ((ret == EOF) || (ret == 0))
    {
        memset(out, ALLONES, sizeof(int));
    }
    else
    {
        memcpy(out, &val, sizeof(unsigned int));
    }
}

static void cvt_int_hex(const char* in, int inlen, unsigned char* out, int)
{
    unsigned int val = 0;
    const int ret = rm_snscanf(in, inlen, "%x", &val);
    if ((ret == EOF) || (ret == 0))
    {
        memset(out, ALLONES, sizeof(int));
    }
    else
    {
        memcpy(out, &val, sizeof(unsigned int));
    }
}

static void cvt_string(const char* in, int inlen, unsigned char* out, int outlen)
{
    if (inlen >= outlen)
    {
        memcpy(out, in, outlen);
    }
    else
    {
        memcpy(out, in, inlen);
        memset(out+inlen, 0, outlen-inlen);
    }
}

static int out_skip(const unsigned char*, int, char*, int)
{
    return 0;
}

static int out_float(const unsigned char* in, int, char* out, int outlen)
{
    float val = 0;
    memcpy(&val, in, sizeof(float));
    int ret = rm_sprintf(out, ASCBUFSZ, "%.6f", static_cast<double>(val));
    if (outlen == ASCBUFSZ)
    {
        // clear excess zeros at the back
        for (int cnt = ret-1; (out[cnt]=='0') && (out[cnt-1]!='.'); --cnt, --ret)
        {
            ;
        }
    }
    else
    {
        if (ret < outlen)
        {
            // pad zeros to the back
            memset(out+ret, '0', outlen-ret);
        }
        ret = outlen;
    }
    return ret;
}

static int out_double(const unsigned char* in, int, char* out, int outlen)
{
    double val = 0;
    memcpy(&val, in, sizeof(double));
    int ret = rm_sprintf(out, ASCBUFSZ, "%.9f", val);
    if (outlen == ASCBUFSZ)
    {
        // clear excess zeros at the back
        for (int cnt = ret-1; out[cnt]=='0'; --cnt, --ret)
        {
            ;
        }
    }
    else
    {
        if (ret < outlen)
        {
            // pad zeros to the back
            memset(out+ret, '0', outlen-ret);
        }
        ret = outlen;
    }
    return ret;
}

static int out_char_signed(const unsigned char* in, int, char* out, int outlen)
{
    int ret = rm_sprintf(out, ASCBUFSZ, "%d", static_cast<int>(*reinterpret_cast<const char*>(in)));
    if (outlen != ASCBUFSZ)
    {
        if (ret < outlen)
        {
            // pad zeros to the front, but behind minus
            if (out[0] == '-')
            {
                memmove(out+(outlen-ret)+1, out+1, ret-1);
                memset(out+1, '0', outlen-ret);
            }
            else
            {
                memmove(out+(outlen-ret), out, ret);
                memset(out, '0', outlen-ret);
            }
        }
        ret = outlen;
    }
    return ret;
}

static int out_char_unsigned(const unsigned char* in, int, char* out, int outlen)
{
    int ret = rm_sprintf(out, ASCBUFSZ, "%u", static_cast<int>(*in));
    if (outlen != ASCBUFSZ)
    {
        if (ret < outlen)
        {
            // pad zeros to the front
            memmove(out+(outlen-ret), out, ret);
            memset(out, '0', outlen-ret);
        }
        ret = outlen;
    }
    return ret;
}

static int out_char_hex(const unsigned char* in, int, char* out, int outlen)
{
    int ret = rm_sprintf(out, ASCBUFSZ, "%x", static_cast<int>(*in));
    if (outlen != ASCBUFSZ)
    {
        if (ret < outlen)
        {
            // pad zeros to the front
            memmove(out+(outlen-ret), out, ret);
            memset(out, '0', outlen-ret);
        }
        ret = outlen;
    }
    return ret;
}

static int out_short_signed(const unsigned char* in, int, char* out, int outlen)
{
    short val = 0;
    memcpy(&val, in, sizeof(short));
    int ret = rm_sprintf(out, ASCBUFSZ, "%d", static_cast<int>(val));
    if (outlen != ASCBUFSZ)
    {
        if (ret < outlen)
        {
            // pad zeros to the front, but behind minus
            if (out[0] == '-')
            {
                memmove(out+(outlen-ret)+1, out+1, ret-1);
                memset(out+1, '0', outlen-ret);
            }
            else
            {
                memmove(out+(outlen-ret), out, ret);
                memset(out, '0', outlen-ret);
            }
        }
        ret = outlen;
    }
    return ret;
}

static int out_short_unsigned(const unsigned char* in, int, char* out, int outlen)
{
    unsigned short val = 0;
    memcpy(&val, in, sizeof(short));
    int ret = rm_sprintf(out, ASCBUFSZ, "%u", static_cast<int>(val));
    if (outlen != ASCBUFSZ)
    {
        if (ret < outlen)
        {
            // pad zeros to the front
            memmove(out+(outlen-ret), out, ret);
            memset(out, '0', outlen-ret);
        }
        ret = outlen;
    }
    return ret;
}

static int out_short_hex(const unsigned char* in, int, char* out, int outlen)
{
    unsigned short val = 0;
    memcpy(&val, in, sizeof(short));
    int ret = rm_sprintf(out, ASCBUFSZ, "%x", static_cast<int>(val));
    if (outlen != ASCBUFSZ)
    {
        if (ret < outlen)
        {
            // pad zeros to the front
            memmove(out+(outlen-ret), out, ret);
            memset(out, '0', outlen-ret);
        }
        ret = outlen;
    }
    return ret;
}

static int out_int_signed(const unsigned char* in, int, char* out, int outlen)
{
    int val = 0;
    memcpy(&val, in, sizeof(int));
    int ret = rm_sprintf(out, ASCBUFSZ, "%d", static_cast<int>(val));
    if (outlen != ASCBUFSZ)
    {
        if (ret < outlen)
        {
            // pad zeros to the front, but behind minus
            if (out[0] == '-')
            {
                memmove(out+(outlen-ret)+1, out+1, ret-1);
                memset(out+1, '0', outlen-ret);
            }
            else
            {
                memmove(out+(outlen-ret), out, ret);
                memset(out, '0', outlen-ret);
            }
        }
        ret = outlen;
    }
    return ret;
}

static int out_int_unsigned(const unsigned char* in, int, char* out, int outlen)
{
    unsigned int val = 0;
    memcpy(&val, in, sizeof(int));
    int ret = rm_sprintf(out, ASCBUFSZ, "%u", static_cast<int>(val));
    if (outlen != ASCBUFSZ)
    {
        if (ret < outlen)
        {
            // pad zeros to the front
            memmove(out+(outlen-ret), out, ret);
            memset(out, '0', outlen-ret);
        }
        ret = outlen;
    }
    return ret;
}

static int out_int_hex(const unsigned char* in, int, char* out, int outlen)
{
    unsigned int val = 0;
    memcpy(&val, in, sizeof(int));
    int ret = rm_sprintf(out, ASCBUFSZ, "%x", static_cast<int>(val));
    if (outlen != ASCBUFSZ)
    {
        if (ret < outlen)
        {
            // pad zeros to the front
            memmove(out+(outlen-ret), out, ret);
            memset(out, '0', outlen-ret);
        }
        ret = outlen;
    }
    return ret;
}

static int out_string(const unsigned char* in, int inlen, char* out, int outlen)
{
    int cnt = 0;
    if (outlen < inlen)
    {
        inlen = outlen;
    }
    for (; (cnt < inlen) && (*in != '\0'); ++cnt, ++in, ++out)
    {
        *out = *in;
    }
    return cnt;
}

typedef void (* cvt_functyp)(const char*, int, unsigned char*, int);
typedef int (* out_functyp)(const unsigned char*, int, char*, int);

inline int locate_delimiter(const char* str, int len, char delimiter)
{
    int ret = -1;
    for (int cnt=0; cnt<len; ++cnt)
    {
        if (str[cnt] == delimiter)
        {
            ret = cnt;
            break;
        }
    }
    return ret;
}

inline int get_hex(int x)
{
    if ((x >= '0') && (x <= '9'))
    {
        x -= '0';
    }
    else if ((x >= 'a') && (x <= 'f'))
    {
        x = x - 'a' + 10;
    }
    else if ((x >= 'A') && (x <= 'F'))
    {
        x = x - 'A' + 10;
    }
    else
    {
        x = -1;
    }
    return x;
}

ascii_var::ascii_var(const char* _frmt) : binlen(0)
{
    char re[] = "^(\\\\x[a-fA-F0-9][a-fA-F0-9]|\\d+|\\S)([f|d|u|x|s]?)(\\d*)[%]";
    const char* errptr;
    int erroffset = 0;
    int ovector[2400];
    int offset = 0;
    size_t fmtoff = 0;
    size_t fmtlen = strlen(_frmt);
    char substr[12];
    char argstr[12];

    // the first character is the separator
    re[55] = _frmt[0];
    ++_frmt;

    // use a regular expression pattern to find and match each format
    pcre* exp = pcre_compile(re, 0, &errptr, &erroffset, NULL);
    for (;;)
    {
        int match = pcre_exec(exp, NULL, _frmt+fmtoff, static_cast<int>(fmtlen-fmtoff), 0, 0, ovector+offset, 1200-offset);
        if (match == 4)
        {
            const char* matchstr;
            pcre_get_substring(_frmt+fmtoff, ovector+offset, match, 0, &matchstr);
            fmtoff += strlen(matchstr);
            if (pcre_free)
            {
                (*pcre_free)(const_cast<char*>(matchstr));
            }
            else
            {
                free(const_cast<char*>(matchstr));
            }
            offset += 8;
            if (fmtoff >= fmtlen)
            {
                match = 0; // so that it will break
            }
        }
        if (match != 4)
        {
            // not an else condition because match can be set to 0 inside prev block
            break;
        }
    }

    frmt = new char[offset/2]; // space to store the processed format
    frmtlen = offset / 8; // number of format groups
    fmtoff = 0; // reset the fmt
    // count the size of the binary pattern
    for (int cnt=0; cnt<frmtlen; ++cnt)
    {
        // get the length of ascii string
        pcre_copy_substring(_frmt+fmtoff, ovector+(cnt*8), 4, 1, substr, 12);
        if ((substr[0] >= '0') && (substr[0] <= '9'))
        {
            frmt[4*cnt+0] = static_cast<char>(atoi(substr));
            frmt[4*cnt+1] = '\0'; // dummy
        }
        else if ((substr[0] == '\\') && (substr[1] == 'x'))
        {
            int val = 0;
            int i = get_hex(substr[2]);
            if (i > -1)
            {
                val = i * 16;
            }
            else
            {
                val = 0;
            }
            i = get_hex(substr[3]);
            if (i > -1)
            {
                val += i;
            }
            frmt[4*cnt+0] = '\0';
            frmt[4*cnt+1] = static_cast<char>(val);
        }
        else
        {
            frmt[4*cnt+0] = '\0';
            frmt[4*cnt+1] = substr[0];
        }

        // get the format string
        pcre_copy_substring(_frmt+fmtoff, ovector+(cnt*8), 4, 2, substr, 12);
        pcre_copy_substring(_frmt+fmtoff, ovector+(cnt*8), 4, 3, argstr, 12);
        switch (substr[0])
        {
        case 'f':
        {
            const int len = atoi(argstr);
            if (len == 4)
            {
                frmt[4*cnt+2] = '\x1';
                frmt[4*cnt+3] = '\x4';
                binlen += 4;
            }
            else if (len == 8)
            {
                frmt[4*cnt+2] = '\x2';
                frmt[4*cnt+3] = '\x8';
                binlen += 8;
            }
            else
            {
                frmt[4*cnt+2] = '\0';
                frmt[4*cnt+3] = '\0';
            }
            break;
        }
        case 'd':
        {
            const int len = atoi(argstr);
            if (len == 1)
            {
                frmt[4*cnt+2] = '\x3';
                frmt[4*cnt+3] = '\x1';
                binlen += 1;
            }
            else if (len == 2)
            {
                frmt[4*cnt+2] = '\x6';
                frmt[4*cnt+3] = '\x2';
                binlen += 2;
            }
            else if (len == 4)
            {
                frmt[4*cnt+2] = '\x9';
                frmt[4*cnt+3] = '\x4';
                binlen += 4;
            }
            else
            {
                frmt[4*cnt+2] = '\0';
                frmt[4*cnt+3] = '\0';
            }
            break;
        }
        case 'u':
        {
            const int len = atoi(argstr);
            if (len == 1)
            {
                frmt[4*cnt+2] = '\x4';
                frmt[4*cnt+3] = '\x1';
                binlen += 1;
            }
            else if (len == 2)
            {
                frmt[4*cnt+2] = '\x7';
                frmt[4*cnt+3] = '\x2';
                binlen += 2;
            }
            else if (len == 4)
            {
                frmt[4*cnt+2] = '\xa';
                frmt[4*cnt+3] = '\x4';
                binlen += 4;
            }
            else
            {
                frmt[4*cnt+2] = '\0';
                frmt[4*cnt+3] = '\0';
            }
            break;
        }
        case 'x':
        {
            const int len = atoi(argstr);
            if (len == 1)
            {
                frmt[4*cnt+2] = '\x5';
                frmt[4*cnt+3] = '\x1';
                binlen += 1;
            }
            else if (len == 2)
            {
                frmt[4*cnt+2] = '\x8';
                frmt[4*cnt+3] = '\x2';
                binlen += 2;
            }
            else if (len == 4)
            {
                frmt[4*cnt+2] = '\xb';
                frmt[4*cnt+3] = '\x4';
                binlen += 4;
            }
            else
            {
                frmt[4*cnt+2] = '\0';
                frmt[4*cnt+3] = '\0';
            }
            break;
        }
        case 's':
            frmt[4*cnt+2] = '\xc';
            frmt[4*cnt+3] = static_cast<char>(atoi(argstr));
            binlen += frmt[4*cnt+3];
            break;
        default:
            frmt[4*cnt+2] = '\0';
            frmt[4*cnt+3] = '\0';
        }
        // find size of frmt fragment
        pcre_copy_substring(_frmt+fmtoff, ovector+(cnt*8), 4, 0, substr, 12);
        fmtoff += strlen(substr);
    }
    // create a binary buffer
    binbuf = new unsigned char[binlen];

    if (pcre_free)
    {
        (*pcre_free)(exp);
    }
    else
    {
        free(exp);
    }
}

ascii_var::~ascii_var()
{
    delete[] frmt;
    delete[] binbuf;
}

int ascii_var::extract(int len, const unsigned char* buf)
{
    static const cvt_functyp cvt_table[] = {
        &cvt_skip,          /*0*/
        &cvt_float,         /*1*/
        &cvt_double,        /*2*/
        &cvt_char_signed,   /*3*/
        &cvt_char_unsigned, /*4*/
        &cvt_char_hex,      /*5*/
        &cvt_short_signed,  /*6*/
        &cvt_short_unsigned,/*7*/
        &cvt_short_hex,     /*8*/
        &cvt_int_signed,    /*9*/
        &cvt_int_unsigned, /*10*/
        &cvt_int_hex,      /*11*/
        &cvt_string        /*12*/
    };

    int idx = 0;
    int bincnt = 0;
    int quickret = -1;

    // Step 0: Iterate over each format definition
    for (int cnt=0; (quickret < 0) && (cnt<frmtlen); ++cnt)
    {
        // Step 1: Extract out the string to process, located at buf+idx, length at substrlen
        //      1a: Compute nextidx, which is the next string to process

        int substrlen = 0;
        int nextidx = 0;
        if ((frmt[cnt*4+0] == '\0') && (frmt[cnt*4+1] != '\0'))
        {
            // try to find delimiter, else return remaining string
            substrlen = locate_delimiter(reinterpret_cast<const char*>(buf)+idx, len-idx, frmt[cnt*4+1]);
            if (substrlen < 0)
            {
                quickret = len - idx;
            }
            nextidx = idx + substrlen + 1;
        }
        else if (frmt[cnt*4+0] == '\0')
        {
            // at least 1 byte is needed for processing
            if (idx >= len)
            {
                quickret = 0;
            }
            substrlen = len - idx;
            nextidx = len;
        }
        else
        {
            substrlen = static_cast<int>(frmt[cnt*4+0]);
            // check if there is enough data remaining
            if ((substrlen+idx) > len)
            {
                quickret = len - idx;
            }
            nextidx = idx + substrlen;
        }

        // Step 2: Convert the string to binary, put into binbuf
        (*(cvt_table[static_cast<int>(frmt[cnt*4+2])]))(reinterpret_cast<const char*>(buf)+idx, substrlen, binbuf+bincnt, static_cast<int>(frmt[cnt*4+3]));

        // Step 2a: Compute bincnt, the next binbuf location
        bincnt += static_cast<int>(frmt[cnt*4+3]);

        // Step 2b: Make nextidx the idx
        idx = nextidx;
    }

    // Step 3: Let children extract from binbuf
    return (quickret < 0) ? complex_var::extract(binlen, binbuf) : quickret;
}

int ascii_var::size() const
{
    unsigned char szbuf[1024];
    outbuf obuf;
    obuf.set(szbuf, 1024);
    const_cast<ascii_var*>(this)->output(obuf);
    return obuf.size();
}

void ascii_var::output(outbuf& strm)
{
    static const out_functyp out_table[] = {
        &out_skip,          /*0*/
        &out_float,         /*1*/
        &out_double,        /*2*/
        &out_char_signed,   /*3*/
        &out_char_unsigned, /*4*/
        &out_char_hex,      /*5*/
        &out_short_signed,  /*6*/
        &out_short_unsigned,/*7*/
        &out_short_hex,     /*8*/
        &out_int_signed,    /*9*/
        &out_int_unsigned, /*10*/
        &out_int_hex,      /*11*/
        &out_string        /*12*/
    };

    outbuf obuf;
    obuf.set(binbuf, binlen);
    complex_var::output(obuf);

    // Assuming that max ascii packet is 1024
    char ascbuf[ASCBUFSZ]; // parasoft-suppress  MISRA2008-8_5_1 "Tool error"
    int bincnt = 0;

    // Step 0: Iterate over each format definition
    for (int cnt=0; cnt<frmtlen; ++cnt)
    {
        // Step 1: Convert the binary to ASCII, put into ascbuf
        const int binbuflen = binlen-bincnt;
        const int flen = static_cast<int>(frmt[cnt*4+3]);
        const int olen = (frmt[cnt*4+0] == '\0') ? ASCBUFSZ : static_cast<int>(frmt[cnt*4+0]);
        if (binbuflen < flen)
        {
            break;
        }
        const int len = (*(out_table[static_cast<int>(frmt[cnt*4+2])]))(binbuf+bincnt, flen, ascbuf, olen);

        // Step 2: Put in delimiters, or truncate or pad if needed
        if ((frmt[cnt*4+0] == '\0') && (frmt[cnt*4+1] != '\0'))
        {
            // Copy all in ascbuf and add delimiter
            for (int idx=0; idx<len; ++idx)
            {
                strm += ascbuf[idx];
            }
            strm += frmt[cnt*4+1];
        }
        else
        {
            // Pad with spaces in front
            for (int ol = static_cast<int>(frmt[cnt*4+0])-len; ol>0; --ol)
            {
                strm += ' ';
            }

            // Truncate / copy from ascbuf
            int ol = static_cast<int>(frmt[cnt*4+0]);
            if (ol == 0)
            {
                ol = ASCBUFSZ;          // when the field has undefined length
            }
            for (int idx=0; (idx<ol)&&(idx<len); ++idx)
            {
                strm += ascbuf[idx];
            }
        }

        // Step 3: Update bincnt
        bincnt += flen;
    }
}

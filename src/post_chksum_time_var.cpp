#include "post_chksum_time_var.h"
#include "chk_var.h"
#include "cplx_var.h"
#include "outbuf.h"
#include <assert.h>
#include <time/timecore.h>

#define TIMEOUT             1000    //1 second
#define LAG_TOL             500     //500ms

struct chkts_item
{
    bool valid;
    bool firstData;
    unsigned int mReplayCount;
    unsigned int mInvalidCount;
    unsigned int mLagCount;
    long long prev_ts_msec;
    float lag_tol_ms;
    chkts_item() : valid(false), firstData(true), mReplayCount(0), mInvalidCount(0), mLagCount(0), prev_ts_msec(0), lag_tol_ms(LAG_TOL)
    {
    }
};

template <class T, bool _BE>
inline void check_timestamp(/*int len, */ const unsigned char* data, chkts_item* chk)
{
    chk->valid = true;
    timeval curr;
    long long ts_msec;

    //extract timestamp
    int tmp = 0;
    for (int i = 0; i<sizeof(tmp); ++data, ++i)
    {
        tmp |= ((*data) & 0xff) << (i * 8);
    }
    ts_msec = (_BE ? rm_BE(tmp) : rm_LE(tmp)) & 0xFFFFFFFF;
    tmp = 0;
    for (int i = 0; i<sizeof(tmp); ++data, ++i)
    {
        tmp |= ((*data) & 0xff) << (i * 8);
    }
    ts_msec |= (long long)(_BE ? rm_BE(tmp) : rm_LE(tmp)) << 32;

    //first timestamp check
    if (chk->firstData)
    {
        if (ts_msec <= 0)
        {
            chk->valid = false;
        }
    }

    //check timestamp
    if (!chk->firstData)
    {
        if (chk->prev_ts_msec > 0)
        {
            const long long time_elapsed = ts_msec - chk->prev_ts_msec;

            if (time_elapsed <= (0 - chk->lag_tol_ms))          //replay occurred
            {
                chk->valid = false;
                chk->mReplayCount++;
            }
        }
        now(&curr);

        const long long lag_ms = (curr.tv_sec * 1000LL + (curr.tv_usec / 1000)) - ts_msec;
        if (lag_ms <= (0 - chk->lag_tol_ms))    //negative lag (packet in the future)
        {
            chk->mInvalidCount++;
        }
#ifndef  NO_CHKSUM_TIMEOUT
        else if (lag_ms > TIMEOUT)      //lag larger than tolerance
        {
            chk->mLagCount++;
        }
#endif
    }

    if (chk->valid)
    {
        chk->prev_ts_msec = ts_msec;
        if (chk->firstData)
        {
            chk->firstData = false;
        }
    }
}

template<class T, bool _BE>
inline void make_chksum_x(int len, const unsigned char* buf, outbuf& strm)
{
    int sz = sizeof(T);
    T cs = 0;
    unsigned char* p = (unsigned char*)&cs;

    for (; len > 0; --len, ++buf)
    {
        cs += *buf;
    }
    cs = _BE ? rm_BE(cs) : rm_LE(cs);
    for (; sz > 0; ++p, --sz)
    {
        strm += *p;
    }
}

template<class T, bool _BE>
inline void make_chksum_x_time(outbuf& strm)
{
    unsigned char* p;
    timeval ts;
    now(&ts);
    const long long ts_msec = ts.tv_sec * 1000LL + (ts.tv_usec / 1000);
    int tmp = _BE ? rm_BE(static_cast<int>(ts_msec)) : rm_LE(static_cast<int>(ts_msec));
    p = (unsigned char*)&tmp;
    for (int i = 0; i<sizeof(tmp); ++p, ++i)
    {
        strm += *p;
    }

    tmp = ts_msec >> 32;
    tmp = _BE ? rm_BE(tmp) : rm_LE(tmp);
    p = (unsigned char*)&tmp;
    for (int i = 0; i<sizeof(tmp); ++p, ++i)
    {
        strm += *p;
    }
}

typedef void (* mkchksum_func)(int, const unsigned char*, outbuf&);
typedef void (* addts_func)(outbuf&);
typedef void (* chkts_func)(const unsigned char*, chkts_item*);
struct mkchksum_item
{
    mkchksum_func mkchksum;
    addts_func generatets;
    chkts_func cmpts;
    int size;
};

static mkchksum_item funclist[] = {
    { &make_chksum_x<unsigned short,false>, &make_chksum_x_time<unsigned short,false>, &check_timestamp<unsigned short,false>,10 },
    { &make_chksum_x<unsigned short,true>, &make_chksum_x_time<unsigned short,true>, &check_timestamp<unsigned short,true>,10 },
    { &make_chksum_x<unsigned int,false>, &make_chksum_x_time<unsigned int,false>, &check_timestamp<unsigned int,false>,12 },
    { &make_chksum_x<unsigned int,true>, &make_chksum_x_time<unsigned int,true>, &check_timestamp<unsigned int,true>,12 }
};

chksum_time_var::chksum_time_var(int _hash, unsigned char chk_loc, unsigned char front_off, unsigned char back_off) : chk_var(chk_loc, front_off, back_off), chkts(new chkts_item),
    hashtype(_hash)//.i_timeout_ms(TIMEOUT), lag_tol_ms(LAG_TOL), mReplayCount(0), mInvalidCount(0), mLagCount(0)
{
    assert(_hash < sizeof(funclist) / sizeof(mkchksum_item));
}

chksum_time_var::chksum_time_var(const chksum_time_var& right) : chk_var(right), chkts(new chkts_item),
    hashtype(right.hashtype)
{
}

chksum_time_var::~chksum_time_var()
{
    delete chkts;
}

int chksum_time_var::extract(int len, const unsigned char* buf)
{
    const int chklen = checksum_size();
    const bool validts = valid_timeinterval(len, buf + len - checksum_size());

    ++total_in_packets;
    if (chk_location == 1)
    {
        if (len < chklen + back_offset)
        {
            return len;
        }

        const unsigned char* const bufptr = buf - front_offset;
        const int buflen = len + front_offset - back_offset - chklen;
        const unsigned char* const chkptr = buf + len - back_offset - chklen;
        if(validts && valid_chksum(buflen, bufptr, chkptr))
        {
            return complex_var::extract(len, buf) - chklen;
        }
    }
    else if (chk_location == 2)
    {
        if (len < chklen + front_offset + back_offset)
        {
            return len;
        }

        const unsigned char* const bufptr = buf + chklen + front_offset;
        const int buflen = len - front_offset - chklen - back_offset;
        const unsigned char* const chkptr = buf;
        if (validts && valid_chksum(buflen, bufptr, chkptr))
        {
            return complex_var::extract(len - chklen, buf + chklen);
        }
    }
    else if (chk_location == 3)
    {
        if (len < back_offset)
        {
            return len;
        }

        const unsigned char* const bufptr = buf - front_offset;
        const int buflen = len + front_offset - back_offset;
        const unsigned char* const chkptr = buf - front_offset - chklen;
        if (validts && valid_chksum(buflen, bufptr, chkptr))
        {
            return complex_var::extract(len, buf);
        }
    }
    else
    {
        --total_in_packets; /* subtract it back, but should not get here */
        return len;  /* return all data to allow parent to continue parsing for valid chksum. */
    }
    ++total_failed_packets;
    notDirty();
    return len; /* return all data to allow parent to continue parsing for valid chksum. */
}

const chksum_time_var& chksum_time_var::operator=(const chksum_time_var& right)
{
    chk_var::operator=(static_cast<const chk_var&>(right));
    return *this;
}

const mpt_var& chksum_time_var::operator=(const mpt_var& right)
{
    return chk_var::operator=(right);
}

int chksum_time_var::checksum_size() const
{
    return funclist[hashtype].size;
}

void chksum_time_var::make_chksum(int len, const unsigned char* buf, outbuf& strm)
{
    funclist[hashtype].generatets(strm);
    funclist[hashtype].mkchksum(len + sizeof(long long), buf, strm);
}

long long chksum_time_var::getLastReceivedTimestamp(void)
{
    return chkts->prev_ts_msec;
}

void chksum_time_var::setLagTolerance(float tolerance)
{
    chkts->lag_tol_ms = tolerance;
}

void chksum_time_var::getFailureCount(int& a, int& b, int& c)
{
    a = chkts->mReplayCount;
    b = chkts->mInvalidCount;
    c = chkts->mLagCount;
}

bool chksum_time_var::valid_timeinterval(int, const unsigned char* data)
{
    funclist[hashtype].cmpts(data, chkts);
    return chkts->valid;
}

bool chksum_time_var::valid_chksum(int len, const unsigned char* buf, const unsigned char* chkptr)
{
    unsigned char computed_checksum[128]; // Assume that largest hash is 1024 bits
    outbuf obuf;
    const int timestamp_len = sizeof(long long);
    obuf.set(computed_checksum, sizeof(computed_checksum));
    funclist[hashtype].mkchksum(len + timestamp_len, buf, obuf);
    return check(computed_checksum, chkptr + timestamp_len, checksum_size() - timestamp_len);
}

/******************************************************************/
/* Copyright DSO National Laboratories 2006. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "link_var.h"

link_var::link_var() : link(0)
{
}

int link_var::extract(int len, const unsigned char* buf)
{
    return (link) ? link->extract(len, buf) : len;
}

int link_var::size() const
{
    return (link) ? link->size() : 0;
}

void link_var::output(outbuf& strm)
{
    if (link)
    {
        link->output(strm);
    }
}

bool link_var::isValid() const
{
    return (link) ? link->isValid() : false;
}

bool link_var::setInvalid()
{
    return (link) ? link->setInvalid() : false;
}

mpt_var* link_var::getNext()
{
    return (link) ? link->getNext() : this;
}

const mpt_var& link_var::operator=(const mpt_var& right)
{
    link = const_cast<mpt_var*>(&right);
    const_cast<mpt_var&>(right).installFosterParent(this);
    return right;
}

bool link_var::operator==(const mpt_var& right) const
{
    // other side is not a link
    return link && link->operator==(right);
}

istream& link_var::operator>>(istream& s)
{
    return (link) ? link->operator>>(s) : s;
}

ostream& link_var::operator<<(ostream& s) const
{
    return (link) ? link->operator<<(s) : s;
}

void link_var::refresh()
{
    mpt_var::refresh();
    if (link)
    {
        link->refresh();
    }
}

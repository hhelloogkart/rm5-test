#ifndef _LOGFILE_H_
#define _LOGFILE_H_

class VarLogFile
{
public:
    VarLogFile(const char*);
    ~VarLogFile();
    void logData(int len, const unsigned char* buf, const char* name, unsigned int rtti);

    static void enable(const char*);
    static void disable();
private:
    void* log;
    unsigned long long timestamp;
    unsigned long long counter;
    unsigned long long lastflush;
};


#endif /* _LOGFILE_H_ */
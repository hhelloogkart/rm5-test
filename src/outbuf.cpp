/******************************************************************/
/* Copyright DSO National Laboratories 2001. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "outbuf.h"
#include "cache.h"
#include <stdlib.h>   //dummy include for NULL

outbuf::outbuf() :
    cache((cache_typ*)NULL),
    start((unsigned char*)NULL), buf((unsigned char*)NULL), len(0)
{
}

void outbuf::set(unsigned char* _buf, unsigned int _len)
{
    cache = (cache_typ*)NULL;
    start = buf = _buf;
    len = _len;
}

void outbuf::set(cache_typ* _cache)
{
    cache = _cache;
    start = buf = reinterpret_cast< unsigned char* > (cache->buf());
    len = cache->get_len() * sizeof(int);
}

void outbuf::set(cache_typ* _cache, unsigned int _len)
{
    cache = _cache;
    start = buf = reinterpret_cast< unsigned char* > (cache->buf());
    len = _len * sizeof(int);
}

unsigned char* outbuf::get() const
{
    return start;
}

unsigned char* outbuf::getcur() const
{
    return buf;
}

cache_typ* outbuf::getcache() const
{
    return cache;
}

void outbuf::clr()
{
    cache = NULL;
    buf = start = NULL;
    len = 0;
}

void outbuf::setsize(unsigned _sz)
{
    if (_sz <= len)
    {
        buf = start + _sz;
    }
}

unsigned outbuf::size() const
{
    return static_cast<unsigned>(buf - start);
}

unsigned outbuf::maxsize() const
{
    return len;
}

void outbuf::append(const unsigned char* _buf, unsigned int _len)
{
    for(unsigned int idx=0; idx<_len; ++idx)
    {
        operator+=(_buf[idx]);
    }
}

void outbuf::append(const unsigned char* _buf)
{
    if (start == NULL)
    {
        return;
    }
    const unsigned char term = 0;
    for (; *_buf != term; ++_buf)
    {
        operator+=(*_buf);
    }
    if (start + len <= buf)
    {
        return;
    }
    *buf = term;
}

void outbuf::append(unsigned int fillcnt, unsigned char fillch)
{
    for(unsigned int idx=0; idx<fillcnt; ++idx)
    {
        operator+=(fillch);
    }
}

void outbuf::append(const char* _buf, unsigned int _len)
{
    append(reinterpret_cast<const unsigned char*>(_buf), _len);
}

void outbuf::append(const char* _buf)
{
    append(reinterpret_cast<const unsigned char*>(_buf));
}
void outbuf::append(unsigned int fillcnt, char fillch)
{
    append(fillcnt, static_cast<unsigned char>(fillch));
}

void outbuf::operator+=(unsigned char ch)
{
    if (start == NULL)
    {
        return;
    }
    if (start + len <= buf)
    {
        return;
    }
    *buf = ch;
    ++buf;
}

void outbuf::operator+=(outbuf& b)
{
    unsigned char* startp = b.get();
    unsigned char* endp;

    if ((start == NULL) || (startp == NULL))
    {
        return;
    }

    endp = startp + b.size();
    while ((buf < start + len) && (startp < endp))
    {
        *buf = *startp;
        ++buf;
        ++startp;
    }
}

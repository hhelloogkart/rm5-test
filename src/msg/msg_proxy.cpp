#include "msg_proxy.h"
#ifndef NO_MDT_PROTOCOL
#include "mdt_proxy.h"
#endif
#include <string.h>

inline int find_protocol_type(const char* host, char* protocol)
{
    for (int idx=0; (idx<8) && (host[idx]!='\0'); ++idx)
    {
        if (host[idx]==':')
        {
            protocol[idx] = '\0';
            return idx+1;
        }
        else
        {
            protocol[idx] = host[idx];
        }
    }
    return 0;
}

msg_proxy::~msg_proxy()
{
}

msg_proxy* msg_proxy::sethost(const char* host, int port)
{
    char prot[8];
    int offset = find_protocol_type(host, prot);
#ifndef NO_MSG_PROTOCOL
    if ((strcmp("msg", prot)==0) || (offset==0))
    {
        msg_proxy* prox = new msg_proxy_msg;
        prox->internal_set_host(host+offset, port);
        return prox;
    }
#endif
#ifndef NO_DLE_PROTOCOL
    if ((strcmp("dle", prot)==0) || (offset==0))
    {
        msg_proxy* prox = new msg_proxy_dle;
        prox->internal_set_host(host+offset, port);
        return prox;
    }
#endif
#ifndef NO_SERIAL_PROTOCOL
    if ((strcmp("ser", prot)==0) || (offset==0))
    {
        msg_proxy* prox = new msg_proxy_ser;
        prox->internal_set_host(host+offset, port);
        return prox;
    }
#endif
#ifndef NO_UDP_PROTOCOL
    if ((strcmp("udp", prot)==0) || (offset==0))
    {
        msg_proxy* prox = new msg_proxy_udp;
        prox->internal_set_host(host+offset, port);
        return prox;
    }
#endif
#ifndef NO_B1553_PROTOCOL
    if ((strcmp("1553", prot)==0) || (offset==0))
    {
        msg_proxy* prox = new msg_proxy_b1553;
        prox->internal_set_host(host+offset, port);
        return prox;
    }
#endif

#ifndef NO_MDT_PROTOCOL
    if ((strcmp("mdt_client", prot)==0) || (offset==0))
    {
        msg_proxy* prox = new mdt_proxy();
        prox->internal_set_host(host+offset, port);
        return prox;
    }
#endif
    return 0;
}

#ifndef NO_MSG_PROTOCOL
int msg_proxy_msg::send(const char* msg, size_t len)
{
    return sock.send(msg, len);
}

bool msg_proxy_msg::recv(char** buf, size_t& len)
{
    return sock.recv(buf, len);
}
void msg_proxy_msg::internal_set_host(const char* host, int port)
{
    sock.sethost(host, port);
}
#endif

#ifndef NO_DLE_PROTOCOL
int msg_proxy_dle::send(const char* msg, size_t len)
{
    return sock.send(msg, len);
}

bool msg_proxy_dle::recv(char** buf, size_t& len)
{
    int l = sock.recv(rcvbuf, DLEBUF);
    len = (l > 0) ? l : 0;
    *buf = rcvbuf;
    return l > 0;
}
void msg_proxy_dle::internal_set_host(const char* host, int port)
{
    sock.sethost(host, port);
    sock.connect(0);
}
#endif

#ifndef NO_SER_PROTOCOL
int msg_proxy_ser::send(const char* msg, size_t len)
{
    return sock.send(msg, len);
}

bool msg_proxy_ser::recv(char** buf, size_t& len)
{
    int l = sock.recv(rcvbuf, DLEBUF);
    len = (l > 0) ? l : 0;
    *buf = rcvbuf;
    return l > 0;
}
void msg_proxy_ser::internal_set_host(const char* host, int port)
{
    sock.sethost(host, port);
    sock.connect(0);
}
#endif

#ifndef NO_UDP_PROTOCOL
int msg_proxy_udp::send(const char* msg, size_t len)
{
    return sock.send(msg, len);
}

bool msg_proxy_udp::recv(char** buf, size_t& len)
{
    struct timeval tv = {
        0, 0
    };
    int ret = sock.select(&tv);
    if (ret)
    {
        ret = sock.recv(rcvbuf, UDPBUF);
        if (ret != -1)
        {
            *buf = rcvbuf;
            len = (size_t)ret;
        }
        else
        {
            len = 0;
        }
    }
    else
    {
        len = 0;
    }
    return len > 0;
}
void msg_proxy_udp::internal_set_host(const char* host, int port)
{
    sock.sethost(host, port);
    sock.bind();
}
#endif

#ifndef NO_B1553_PROTOCOL
#include "bus1553_intf.h"

msg_proxy_b1553::msg_proxy_b1553(void) : sock(0)
{
}

msg_proxy_b1553::~msg_proxy_b1553(void)
{
    if(connected())
    {
        sock->Close();
    }
    delete(sock);
}

int msg_proxy_b1553::send(const char* msg, size_t len)
{
    if(!connected())
    {
        return 0;
    }
    return sock->Send(reinterpret_cast<const unsigned short*>(msg), len/2) ? len : 0;
}

bool msg_proxy_b1553::recv(char** buf, size_t& len)
{
    if(!connected())
    {
        return false;
    }
    len = sock->Recv(rcvbuf, sizeof(rcvbuf)/2)*2;
    *buf = reinterpret_cast<char*>(rcvbuf);
    return len > 0;
}
void msg_proxy_b1553::internal_set_host(const char* host, int port)
{
    if(!connected())
    {
        sock = Bus1553Intf::Open(host, port);
    }
}
bool msg_proxy_b1553::connected()
{
    return sock != 0;
}
#endif

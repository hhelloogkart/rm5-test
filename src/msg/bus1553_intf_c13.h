#ifndef _BUS1553_INTF_C13_H_
#define _BUS1553_INTF_C13_H_

#include "bus1553_intf_ballard.h"

struct Bus1553C13IntfPrivate;

class Bus1553C13Intf : public Bus1553Intf
{
public:
    Bus1553C13Intf();
    virtual ~Bus1553C13Intf();

    virtual bool MonOpen(const char* host, int port);
    virtual bool RTOpen(const char* host, int port);
    virtual bool BCOpen(const char* host, int port);
    virtual void Close();

    virtual unsigned int MonRead(unsigned short* buf);
    virtual unsigned int MonRead(unsigned short* buf, unsigned int nbuf);
    virtual unsigned int MsgRead(unsigned short* buf);
    virtual bool MsgRead(unsigned short* buf, unsigned short cwd);
    virtual bool MsgWrite(const unsigned short* buf, unsigned short cwd);

protected:
    bool C13Open(int port);

    Bus1553C13IntfPrivate* prvt;
};

#endif

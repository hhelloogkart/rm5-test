/******************************************************************/
/* Copyright DSO National Laboratories 2016. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "bus1553_intf_bti.h"
#include <functional>
#ifdef _WIN32
#include <BTICard.h> //for OmniBusBox APIs
#include <BTI1553.h> //for OmniBusBox APIs
typedef unsigned int UINT32;
typedef unsigned short UINT16;
typedef unsigned int* LPUINT32;
typedef unsigned short* LPUINT16;
#else
#include "LINUX32_BTICard.h"
#include "LINUX32_BTI1553.h"
#endif
#include <dlfcn.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
//#include <stdio.h>	//for puts

#define    MAX_CORES     4
#define    MAX_CHANNELS  32

#ifdef _WIN32
#ifdef _WIN64
#define LIBNAME_BTI "BTICARD64.DLL"
#define LIBNAME_BTI1553 "BTI155364.DLL"
#else
#define LIBNAME_BTI "BTICARD.DLL"
#define LIBNAME_BTI1553 "BTI1553.DLL"
#endif
#define MKSYM(nm, alias) "_" nm alias
#else
#define LIBNAME_BTI "libbtiCard.so"
#define LIBNAME_BTI1553 "libbti1553.so"
#define __stdcall
#define MKSYM(nm, alias) nm
#endif

static ERRVAL(__stdcall *pfn_BTI1553_MonConfig)(UINT32, INT, HCORE) = NULL;
static ERRVAL(__stdcall *pfn_BTI1553_MonConfigEx)(UINT32, UINT32, UINT16, INT, HCORE) = NULL;
static ERRVAL(__stdcall *pfn_BTI1553_MonFilterTA)(UINT32, INT, HCORE) = NULL;
static MSGADDR(__stdcall *pfn_BTI1553_BCCreateMsg)(UINT32, UINT16, UINT16, LPUINT16, HCORE) = NULL;
static int(__stdcall *pfn_BTI1553_BCConfig)(UINT32, INT, HCORE) = NULL;
static LISTADDR(__stdcall *pfn_BTI1553_BCCreateList)(UINT32, INT, UINT32, UINT16, UINT16, LPUINT16, HCORE) = NULL;
static int(__stdcall *pfn_BTI1553_BCSchedFrame)(UINT32, INT, HCORE) = NULL;
static int(__stdcall *pfn_BTI1553_BCSchedMsg)(MSGADDR, INT, HCORE) = NULL;
static int(__stdcall *pfn_BTI1553_BCSchedLog)(UINT32, UINT16, INT, HCORE) = NULL;
static ERRVAL(__stdcall *pfn_BTI1553_RTConfig)(UINT32, INT, INT, HCORE) = NULL;
static MSGADDR(__stdcall *pfn_BTI1553_RTCreateMsg)(UINT32, BOOL, INT, BOOL, INT, INT, HCORE) = NULL;
static MSGADDR(__stdcall *pfn_BTI1553_RTGetMsg)(BOOL, INT, BOOL, INT, INT, HCORE) = NULL;
static INT(__stdcall *pfn_BTI1553_ListDataRd)(LPUINT16, INT, LISTADDR, HCORE) = NULL;
static INT(__stdcall *pfn_BTI1553_ListDataWr)(LPUINT16, INT, LISTADDR, HCORE) = NULL;
static UINT32(__stdcall *pfn_BTI1553_MsgFieldWr)(UINT32, UINT16, MSGADDR, HCORE) = NULL;
static UINT32(__stdcall *pfn_BTI1553_MsgFieldRd)(UINT16, MSGADDR, HCORE) = NULL;
static void(__stdcall *pfn_BTI1553_MsgDataRd)(LPUINT16, INT, MSGADDR, HCORE) = NULL;
static void(__stdcall *pfn_BTI1553_MsgDataWr)(LPUINT16, INT, MSGADDR, HCORE) = NULL;
static BOOL(__stdcall *pfn_BTI1553_ChIs1553)(INT, HCORE) = NULL;
static int(__stdcall *pfn_BTICard_CardClose)(HCARD) = NULL;
static void(__stdcall *pfn_BTICard_CardReset)(HCARD) = NULL;
static ERRVAL(__stdcall *pfn_BTICard_CoreOpen)(LPHCORE, INT, HCARD) = NULL;
static int(__stdcall *pfn_BTICard_CardOpen)(LPHCARD, INT) = NULL;
static BOOL(__stdcall *pfn_BTICard_CardStop)(HCORE) = NULL;
static int(__stdcall *pfn_BTICard_CardStart)(HCORE) = NULL;
static ERRVAL (__stdcall *pfn_BTICard_EventLogConfig)(UINT16, UINT16, HCORE);
static UINT32 (__stdcall *pfn_BTICard_EventLogRd)(LPUINT16, LPUINT32, LPINT, HCORE);
static UINT32(__stdcall *pfn_BTICard_SeqBlkRd)(LPUINT16, UINT32, LPUINT32, HCORE) = NULL;
static ERRVAL(__stdcall *pfn_BTICard_SeqFindInit)(LPUINT16, UINT32, LPSEQFINDINFO) = NULL;
static ERRVAL(__stdcall *pfn_BTICard_SeqFindNext1553)(LPSEQRECORD1553*, LPSEQFINDINFO) = NULL;
static LPCSTR(__stdcall *pfn_BTICard_ErrDescStr)(ERRVAL, HCORE) = NULL;
static ERRVAL(__stdcall *pfn_BTICard_SeqConfig)(UINT32, HCORE) = NULL;

struct Bus1553BtiIntfPrivate
{
    int card_no;
    int core_no;
    int channel_no;
    HCARD h_card;                    /*Handle to card*/
    HCORE h_core;                    /*Handle to core*/
    //bc data
    bc_map_typ bc_map;
    cwd_list_typ bc_list;
    cwd_list_typ::iterator bc_it;
    //rt data
    unsigned int rt;
    unsigned short sa_read;
    //mon data
    LPSEQRECORD1553 p_rec;
    LPSEQRECORDMORE1553 pRecMore1553;
    SEQFINDINFO sfinfo;
    UINT16 seqbuf[2048];
};

inline int find_protocol_type(const char* host, char* protocol, char sep)
{
    for (int idx = 0; (idx < 8) && (host[idx] != '\0'); ++idx)
    {
        if (host[idx] == sep)
        {
            protocol[idx] = '\0';
            return idx + 1;
        }
        else
        {
            protocol[idx] = host[idx];
        }
    }
    return 0;
}

Bus1553BtiIntf::Bus1553BtiIntf() : Bus1553Intf(), prvt(new Bus1553BtiIntfPrivate)
{
    //data members initialized by calling BaseDevice constructor
    void* hdl = dlopen(LIBNAME_BTI1553, RTLD_NOW | RTLD_LOCAL);
    if (hdl)
    {
        pfn_BTI1553_MonConfig = (ERRVAL(__stdcall*) (UINT32, INT, HCORE))dlsym(hdl, MKSYM("BTI1553_MonConfig", "@12"));
        assert(pfn_BTI1553_MonConfig);
        pfn_BTI1553_MonConfigEx = (ERRVAL(__stdcall*)(UINT32, UINT32, UINT16, INT, HCORE))dlsym(hdl, MKSYM("BTI1553_MonConfigEx", "@20"));
        pfn_BTI1553_MonFilterTA = (ERRVAL(__stdcall*)(UINT32, INT, HCORE))dlsym(hdl, MKSYM("BTI1553_MonFilterTA", "@12"));
        pfn_BTI1553_BCCreateMsg = (MSGADDR(__stdcall*)(UINT32, UINT16, UINT16, LPUINT16, HCORE))dlsym(hdl, MKSYM("BTI1553_BCCreateMsg", "@20"));
        pfn_BTI1553_BCConfig = (int(__stdcall*)(UINT32, INT, HCORE))dlsym(hdl, MKSYM("BTI1553_BCConfig", "@12"));
        pfn_BTI1553_BCCreateList = (LISTADDR(__stdcall*)(UINT32, INT, UINT32, UINT16, UINT16, LPUINT16, HCORE))dlsym(hdl, MKSYM("BTI1553_BCCreateList", "@28"));
        pfn_BTI1553_BCSchedFrame = (int(__stdcall*)(UINT32, INT, HCORE))dlsym(hdl, MKSYM("BTI1553_BCSchedFrame", "@12"));
        pfn_BTI1553_BCSchedMsg = (int(__stdcall*)(MSGADDR, INT, HCORE))dlsym(hdl, MKSYM("BTI1553_BCSchedMsg", "@12"));
        pfn_BTI1553_BCSchedLog = (int(__stdcall*)(UINT32, UINT16, INT, HCORE))dlsym(hdl, MKSYM("BTI1553_BCSchedLog", "@16"));
        pfn_BTI1553_RTConfig = (ERRVAL(__stdcall*)(UINT32, INT, INT, HCORE))dlsym(hdl, MKSYM("BTI1553_RTConfig", "@16"));
        pfn_BTI1553_RTCreateMsg = (MSGADDR(__stdcall*)(UINT32, BOOL, INT, BOOL, INT, INT, HCORE))dlsym(hdl, MKSYM("BTI1553_RTCreateMsg", "@28"));
        pfn_BTI1553_RTGetMsg = (MSGADDR(__stdcall*)(BOOL, INT, BOOL, INT, INT, HCORE))dlsym(hdl, MKSYM("BTI1553_RTGetMsg", "@24"));
        pfn_BTI1553_ListDataRd = (INT(__stdcall*)(LPUINT16, INT, LISTADDR, HCORE))dlsym(hdl, MKSYM("BTI1553_ListDataRd", "@16"));
        pfn_BTI1553_ListDataWr = (INT(__stdcall*)(LPUINT16, INT, LISTADDR, HCORE))dlsym(hdl, MKSYM("BTI1553_ListDataWr", "@16"));
        pfn_BTI1553_MsgFieldWr = (UINT32(__stdcall*)(UINT32, UINT16, MSGADDR, HCORE))dlsym(hdl, MKSYM("BTI1553_MsgFieldWr", "@16"));
        pfn_BTI1553_MsgFieldRd = (UINT32(__stdcall*)(UINT16, MSGADDR, HCORE))dlsym(hdl, MKSYM("BTI1553_MsgFieldRd", "@12"));
        pfn_BTI1553_MsgDataRd = (void(__stdcall*)(LPUINT16, INT, MSGADDR, HCORE))dlsym(hdl, MKSYM("BTI1553_MsgDataRd", "@16"));
        pfn_BTI1553_MsgDataWr = (void(__stdcall*)(LPUINT16, INT, MSGADDR, HCORE))dlsym(hdl, MKSYM("BTI1553_MsgDataWr", "@16"));
        pfn_BTI1553_ChIs1553 = (BOOL(__stdcall*)(INT, HCORE))dlsym(hdl, MKSYM("BTI1553_ChIs1553", "@8"));
    }
    else
    {
        //puts(dlerror());
        assert(false);
    }

    void* hd2 = dlopen(LIBNAME_BTI, RTLD_NOW | RTLD_LOCAL);
    if (hd2)
    {
        pfn_BTICard_CardClose = (int(__stdcall*)(HCARD))dlsym(hd2, MKSYM("BTICard_CardClose", "@4"));
        assert(pfn_BTICard_CardClose);
        pfn_BTICard_CardReset = (void(__stdcall*)(HCARD))dlsym(hd2, MKSYM("BTICard_CardReset", "@4"));
        pfn_BTICard_CoreOpen = (ERRVAL(__stdcall*)(LPHCORE, INT, HCARD))dlsym(hd2, MKSYM("BTICard_CoreOpen", "@12"));
        pfn_BTICard_CardOpen = (int(__stdcall*)(LPHCARD, INT))dlsym(hd2, MKSYM("BTICard_CardOpen", "@8"));
        pfn_BTICard_CardStop = (BOOL(__stdcall*)(HCORE))dlsym(hd2, MKSYM("BTICard_CardStop", "@4"));
        pfn_BTICard_CardStart = (int(__stdcall*)(HCORE))dlsym(hd2, MKSYM("BTICard_CardStart", "@4"));
        pfn_BTICard_EventLogConfig = (ERRVAL(__stdcall*)(UINT16, UINT16, HCORE))dlsym(hd2, MKSYM("BTICard_EventLogConfig", "@12"));
        pfn_BTICard_EventLogRd = (UINT32(__stdcall*)(LPUINT16, LPUINT32, LPINT, HCORE))dlsym(hd2, MKSYM("BTICard_EventLogRd", "@16"));
        pfn_BTICard_SeqBlkRd = (UINT32(__stdcall*)(LPUINT16, UINT32, LPUINT32, HCORE))dlsym(hd2, MKSYM("BTICard_SeqBlkRd", "@16"));
        pfn_BTICard_SeqFindInit = (ERRVAL(__stdcall*)(LPUINT16, UINT32, LPSEQFINDINFO))dlsym(hd2, MKSYM("BTICard_SeqFindInit", "@12"));
        pfn_BTICard_SeqFindNext1553 = (ERRVAL(__stdcall*)(LPSEQRECORD1553*, LPSEQFINDINFO))dlsym(hd2, MKSYM("BTICard_SeqFindNext1553", "@8"));
        pfn_BTICard_ErrDescStr = (LPCSTR(__stdcall*)(ERRVAL, HCORE))dlsym(hd2, MKSYM("BTICard_ErrDescStr", "@8"));
        pfn_BTICard_SeqConfig = (ERRVAL(__stdcall*)(UINT32, HCORE))dlsym(hd2, MKSYM("BTICard_SeqConfig", "@8"));
    }
    else
    {
        //puts(dlerror());
        assert(false);
    }

    prvt->card_no = (-1);
    prvt->core_no = (-1);
    prvt->channel_no = (-1);
    prvt->h_card = 0;
    prvt->h_core = 0;
    prvt->rt = 0;
    prvt->sa_read = 0;
    prvt->bc_it = prvt->bc_list.end();
    prvt->p_rec = 0;
}

Bus1553BtiIntf::~Bus1553BtiIntf()
{
    delete(prvt);
}

bool Bus1553BtiIntf::MonOpen(const char* host, int port)
{
    if (BtiOpen(port))
    {
        if (pfn_BTI1553_MonConfig(MONCFG1553_DEFAULT | MONCFG1553_BCAST, prvt->channel_no, prvt->h_core) < 0 ||
            pfn_BTICard_SeqConfig(SEQCFG_DEFAULT, prvt->h_core) < 0)
        {
            Close();
            return false;
        }

        char rtstr[8];
        int rt;
        int offset = find_protocol_type(host, rtstr, ',');
        int offset2 = 0;
        unsigned int filter_rt = 0;

        while (1)
        {
            if (offset == 0 && isdigit(host[offset2]))
            {
                rt = atoi(host + offset2);
                filter_rt |= (1 << rt);
                break;
            }
            else if (offset > 2 && isdigit(rtstr[0]))
            {
                rt = atoi(rtstr);
                filter_rt |= (1 << rt);
                offset2 += offset;
                offset = find_protocol_type(host + offset2, rtstr, ',');
            }
            else
            {
                break;
            }
        }

        if (filter_rt)
        {
            pfn_BTI1553_MonFilterTA(filter_rt, prvt->channel_no, prvt->h_core);
        }

        pfn_BTICard_CardStart(prvt->h_core);
        prvt->p_rec = 0;
        return true;
    }
    return false;
}

bool Bus1553BtiIntf::RTOpen(const char* host, int port)
{
    if (BtiOpen(port))
    {
        prvt->sa_read = 0;
        prvt->rt = atoi(host);
        if (pfn_BTI1553_RTConfig(RTCFG1553_DEFAULT, prvt->rt, prvt->channel_no, prvt->h_core) < 0)
        {
            Close();
            return false;
        }
        /*
                //read cmd list for this RT
                const char *str = strchr(host, ',');
                const char *endp;
                unsigned long cwd;
                unsigned short int cwd1, cwd2;
                while(str && *str==',')
                {
         ++str;
                    cwd = strtoul(str, const_cast<char **>(&endp), 0);
                    if(str == endp) break;
                    cwd1 = (unsigned short)(cwd & 0xFFFF);
                    cwd2 = (unsigned short)(cwd >> 16);
                    unsigned int ta;
                    unsigned int tr;
                    unsigned int sa;
                    unsigned int wc;
                    UnpackCWD(cwd1, &ta, &tr, &sa, &wc);
                    if(ta == prvt->rt)
                        prvt->bc_list.push_back(cwd1);
                    else if(cwd2)
                    {
                        UnpackCWD(cwd2, &ta, &tr, &sa, &wc);
                        if(ta == prvt->rt)
                            prvt->bc_list.push_back(cwd2);
                    }
                    str = endp;
                }
                prvt->bc_it = prvt->bc_list.end();
         */
        pfn_BTICard_CardStart(prvt->h_core);
        return true;
    }
    return false;
}

bool Bus1553BtiIntf::BCOpen(const char* host, int port)
{
    if (BtiOpen(port))
    {
        if (pfn_BTI1553_BCConfig(BCCFG1553_DEFAULT, prvt->channel_no, prvt->h_core) < 0)
        {
            Close();
            return false;
        }

        //parse frame_time_us;cwdlist[;cwdlist]* where cwdlist=cwd[,cwd]*
        int frame_time_us = atoi(host);
        const char* str = strchr(host, ';');
        const char* endp;
        unsigned long cwd;
        unsigned short int cwd1, cwd2;
        unsigned int ta;
        unsigned int tr;
        unsigned int sa;
        unsigned int wc;
        bc_map_typ::iterator it;

        pfn_BTI1553_BCSchedFrame(frame_time_us, prvt->channel_no, prvt->h_core);
        while (str && ((*str == ';') || (*str == ',')))
        {
            ++str;
            cwd = strtoul(str, const_cast<char**>(&endp), 0);
            if (str == endp)
            {
                break;
            }
            cwd1 = (unsigned short)(cwd & 0xFFFF);
            cwd2 = (unsigned short)(cwd >> 16);
            it = prvt->bc_map.find(cwd1);
            if (it == prvt->bc_map.end())
            {
                bc_data_typ d;
                unsigned int opt = MSGCRT1553_DEFAULT | MSGCRT1553_HIT | MSGCRT1553_TIMETAG;
                UnpackCWD(cwd1, &ta, &tr, &sa, &wc);
                if (cwd2)
                {
                    opt |= MSGCRT1553_RTRT;
                }
                d.alloc();
                d.msg = (MSGADDR)pfn_BTI1553_BCCreateList(LISTCRT1553_PINGPONG, 0, opt, cwd1, cwd2, d.data, prvt->h_core);
                bc_map_typ::value_type v(cwd1, d);
                it = prvt->bc_map.insert(v).first; //RT receive for RT-RT message
                prvt->bc_list.push_back(cwd1);
                if (cwd2)
                {
                    bc_map_typ::value_type v2(cwd2, d);
                    prvt->bc_map.insert(v2); //RT transmit for RT-RT message
                    prvt->bc_list.push_back(cwd2);
                }
            }
            pfn_BTI1553_BCSchedMsg((*it).second.msg, prvt->channel_no, prvt->h_core);
            if (tr == XMT) //RT to BC
            {
                pfn_BTI1553_BCSchedLog(COND1553_ALWAYS, cwd1, prvt->channel_no, prvt->h_core);
            }
            if (*endp == ';')
            {
                pfn_BTI1553_BCSchedFrame(frame_time_us, prvt->channel_no, prvt->h_core);
            }
            str = endp;
        }

        prvt->bc_it = prvt->bc_list.end();

        if (pfn_BTICard_EventLogConfig(LOGCFG_ENABLE, 1024, prvt->h_core) < 0)
        {
            Close();
            return false;
        }

        pfn_BTICard_CardStart(prvt->h_core);
        return true;
    }
    return false;
}

void Bus1553BtiIntf::Close()
{
    if (prvt->card_no >= 0)
    {
        pfn_BTICard_CardStop(prvt->h_core);
        pfn_BTICard_CardClose(prvt->h_card);

        bc_map_typ::iterator it;
        for (it = prvt->bc_map.begin(); it != prvt->bc_map.end(); ++it)
        {
            (*it).second.destroy();
        }
        prvt->bc_map.clear();
        prvt->bc_list.clear();
        prvt->h_core = (HCORE)-1;
        prvt->card_no = -1;
        prvt->p_rec = 0;
    }
}

unsigned int Bus1553BtiIntf::MonRead(unsigned short* buf)
{
    UINT32 seqcount;
    UINT32 blkcnt;
    int cnt;

    for (cnt = 0; cnt < 2; ++cnt)
    {
        if (!prvt->p_rec)
        {
            seqcount = pfn_BTICard_SeqBlkRd(prvt->seqbuf, sizeof(prvt->seqbuf) / sizeof(prvt->seqbuf[0]), &blkcnt, prvt->h_core);
            pfn_BTICard_SeqFindInit(prvt->seqbuf, seqcount, &prvt->sfinfo);
        }
        if (!pfn_BTICard_SeqFindNext1553(&prvt->p_rec, &prvt->sfinfo))
        {
            //printf(" Ch:%u",(p_rec->activity & MSGACT1553_CHMASK) >> MSGACT1553_CHSHIFT);
            //printf(" Time:%lu",p_rec->timestamp);
            //if (p_rec->activity & MSGACT1553_RCVCWD1) printf(" Cwd1:%04X",p_rec->cwd1);
            //if (p_rec->activity & MSGACT1553_RCVCWD2) printf(" Cwd2:%04X",p_rec->cwd2);
            //if (p_rec->activity & MSGACT1553_RCVSWD1) printf(" Swd1:%04X",p_rec->swd1);
            //if (p_rec->activity & MSGACT1553_RCVSWD2) printf(" Swd2:%04X",p_rec->swd2);
            //if (p_rec->error & MSGERR1553_NORESP) printf(" No response!");
            //if (p_rec->error & MSGERR1553_ANYERR) printf(" Error!");
            //if (pfn_BTICard_SeqFindCheckVersion((LPUINT16)p_rec, SEQVER_1))
            //{
            //    LPSEQRECORDMORE1553 pRecMore1553;
            //    if (!pfn_BTICard_SeqFindMore1553(&pRecMore1553, p_rec))
            //    {
            //        printf(" RespTime1:%u",pRecMore1553->resptime1);
            //    }
            //}
            if ((prvt->p_rec->error & (MSGERR1553_ANYERR | MSGERR1553_DATACOUNT)) == 0)
            {
                buf[0] = prvt->p_rec->cwd1;
                memcpy(buf + 1, prvt->p_rec->data, prvt->p_rec->datacount * 2);
                assert((prvt->p_rec->datacount <= 0x20) && (prvt->p_rec->datacount & 0x1f) == (buf[0] & 0x1f));
                return prvt->p_rec->datacount + 1;
            }
        }
        else
        {
            prvt->p_rec = 0;
        }
    }
    return 0;
}

unsigned int Bus1553BtiIntf::MonRead(unsigned short* buf, unsigned int nbuf)
{
    unsigned int i;

    for (i = 0; i < nbuf && MonRead(buf) > 0; ++i)
    {
        buf += 33;
    }
    return i;
}


unsigned int Bus1553BtiIntf::MsgRead(unsigned short* buf)
{
    unsigned int ret = 0;
    UINT16 type;
    UINT32 info;
    INT chan;

    if (mode == MODE_BC)
    {
        while (pfn_BTICard_EventLogRd(&type, &info, &chan, prvt->h_core))
        {
            if (type == EVENTTYPE_1553OPCODE)
            {
                UINT16 cwd = (UINT16)info;
                unsigned int ta;
                unsigned int tr;
                unsigned int sa;
                unsigned int wc;
                UnpackCWD(cwd, &ta, &tr, &sa, &wc);
                if (tr == XMT) //RT to BC
                {
                    bc_map_typ::iterator it = prvt->bc_map.find(cwd);
                    if (it != prvt->bc_map.end())
                    {
                        if (wc == 0)
                        {
                            wc = 32;
                        }
                        buf[0] = cwd;
                        pfn_BTI1553_ListDataRd(buf + 1, (int)wc, (*it).second.msg, prvt->h_core);
                        ret = wc + 1;
                        break;
                    }
                }
            }
        }
    }
    else if (mode == MODE_RT)
    {
        for (int i = 1; i <= 31; ++i)
        {
            if (prvt->sa_read >= 31)
            {
                prvt->sa_read = 0;
                break;
            }
            if (prvt->sa_read <= 0)
            {
                ++prvt->sa_read;
                break; //hack for msg_interf_typ::read_data() to beak
            }

            MSGADDR m = pfn_BTI1553_RTGetMsg(0, prvt->rt, RCV, prvt->sa_read, prvt->channel_no, prvt->h_core);
            ++prvt->sa_read;
            unsigned long e = pfn_BTI1553_MsgFieldRd(FIELD1553_ERROR, m, prvt->h_core);
            if (e & MSGERR1553_HIT)  //check that new message received
            {
                unsigned short c = (unsigned short)pfn_BTI1553_MsgFieldRd(FIELD1553_CWD1, m, prvt->h_core);
                unsigned int ta1;
                unsigned int tr1;
                unsigned int sa1;
                unsigned int wc1;
                UnpackCWD(c, &ta1, &tr1, &sa1, &wc1);
                if (wc1 == 0)
                {
                    wc1 = 32;
                }
                pfn_BTI1553_MsgDataRd(buf + 1, (int)wc1, m, prvt->h_core);
                pfn_BTI1553_MsgFieldWr(0, FIELD1553_ERROR, m, prvt->h_core);    //clear 'hit' bit
                buf[0] = c;
                ret = wc1 + 1;
                break;
            }
        }
    }
    return ret;
}

bool Bus1553BtiIntf::MsgRead(unsigned short* buf, unsigned short cwd)
{
    bool ret = false;
    unsigned int ta;
    unsigned int tr;
    unsigned int sa;
    unsigned int wc;
    UnpackCWD(cwd, &ta, &tr, &sa, &wc);
    if (wc == 0)
    {
        wc = 32;
    }

    if (mode == MODE_BC)
    {
        bc_map_typ::iterator it = prvt->bc_map.find(cwd);
        if (it != prvt->bc_map.end())
        {
            pfn_BTI1553_ListDataRd(buf, (int)wc, (*it).second.msg, prvt->h_core);
            ret = true;
        }
    }
    else if (mode == MODE_RT)
    {
        MSGADDR m = pfn_BTI1553_RTGetMsg(sa == 0 || sa == 0x1F, ta, tr, sa, prvt->channel_no, prvt->h_core);
        if (tr == RCV) //BC to RT
        {
            unsigned long e = pfn_BTI1553_MsgFieldRd(FIELD1553_ERROR, m, prvt->h_core);
            if (e & MSGERR1553_HIT)  //check that new message received
            {
                pfn_BTI1553_MsgDataRd(buf, (int)wc, m, prvt->h_core);
                pfn_BTI1553_MsgFieldWr(0, FIELD1553_ERROR, m, prvt->h_core);    //clear 'hit' bit
                ret = true;
            }
        }
        else //RT to BC
        {
            pfn_BTI1553_MsgDataRd(buf, (int)wc, m, prvt->h_core);
            ret = true;
        }
    }
    return ret;
}

bool Bus1553BtiIntf::MsgWrite(const unsigned short* buf, unsigned short cwd)
{
    bool ret = false;
    unsigned int ta;
    unsigned int tr;
    unsigned int sa;
    unsigned int wc;
    UnpackCWD(cwd, &ta, &tr, &sa, &wc);
    if (wc == 0)
    {
        wc = 32;
    }

    if (mode == MODE_BC)
    {
        if (tr == RCV) //BC to RT
        {
            bc_map_typ::iterator it;
            it = prvt->bc_map.find(cwd);
            if (it != prvt->bc_map.end())
            {
                pfn_BTI1553_ListDataWr(const_cast<LPUINT16>(buf), (int)wc, (*it).second.msg, prvt->h_core);
                ret = true;
            }
        }
    }
    else if (mode == MODE_RT)
    {
        if (tr == XMT) //RT to BC
        {
            MSGADDR m = pfn_BTI1553_RTGetMsg(sa == 0 || sa == 0x1F, ta, tr, sa, prvt->channel_no, prvt->h_core);
            unsigned long e = pfn_BTI1553_MsgFieldRd(FIELD1553_ERROR, m, prvt->h_core);
            if (e & MSGERR1553_HIT) //check that last message sent
            {
                pfn_BTI1553_MsgDataWr(const_cast<LPUINT16>(buf), (int)wc, m, prvt->h_core);
                pfn_BTI1553_MsgFieldWr(0, FIELD1553_ERROR, m, prvt->h_core); //clear 'hit' bit
                ret = true;
            }
        }
    }
    return ret;
}

//bti:port=channelno*100 + coreno*10 + cardno
bool Bus1553BtiIntf::BtiOpen(int port)
{
    Close();

    prvt->card_no = port % 10;
    prvt->core_no = (port / 10) % 10;
    prvt->channel_no = (port / 100) % 100;
    if (!pfn_BTICard_CardOpen(&prvt->h_card, prvt->card_no))
    {
        if (!pfn_BTICard_CoreOpen(&prvt->h_core, prvt->core_no, prvt->h_card))
        {
            if (pfn_BTI1553_ChIs1553(prvt->channel_no, prvt->h_core))
            {
                pfn_BTICard_CardReset(prvt->h_core);
                return true;
            }
        }
        pfn_BTICard_CardClose(prvt->h_card);
    }

    prvt->card_no = prvt->core_no = prvt->channel_no = -1;
    return false;
}

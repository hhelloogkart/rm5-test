#include "msg_interf.h"
#include "msg_var.h"
#include <list>
#include <string>
#include <time/timecore.h>
#include <stdlib.h>
#include <string.h>

using std::string;

struct msg_rec
{
    msg_rec(int inf, const char* mask, const char* hdr, unsigned short hdrlen,
            const char* trl, unsigned short trllen, msg_var* v,
            unsigned short lenof, short lenwd, short lenbi, unsigned short msgoff);
    msg_rec(const msg_rec& right);
    int interf;
    string hdrMask;
    string header;
    string trailer;
    msg_var* var;
    unsigned short msglenoff;
    short msglenwid;
    short msglenbias;
    unsigned short msgoffset;
};

struct msg_param_rec
{
    msg_param_rec(int inf, const char* hst, int prt, unsigned int to);
    msg_param_rec(const msg_param_rec& right);
    int interf;
    string host;
    int port;
    unsigned int timeout_msec;
};

struct msg_same_header // when there are duplicate headers, only 1st message will recv the data
{
    msg_same_header(msg_var*, msg_var*);
    msg_same_header(const msg_same_header& right);
    msg_var* first;
    msg_var* outputonly;
};

typedef std::list<msg_rec> msg_rec_list_typ;
typedef std::list<msg_param_rec> m_param_rec_list_typ;
typedef std::list<msg_interf_typ> msg_interf_list_typ;
typedef std::list<msg_same_header> msg_same_header_list_typ;
static msg_rec_list_typ* msg_rec_list = 0;
static m_param_rec_list_typ* m_param_rec_list = 0;
static msg_same_header_list_typ* msg_same_header_list = 0;
static msg_interf_list_typ& msg_interf_list()
{
    static msg_interf_list_typ prvt;
    return prvt;
}

msg_rec::msg_rec(int inf, const char* mask, const char* hdr, unsigned short hdrlen,
                 const char* trl, unsigned short trllen, msg_var* v,
                 unsigned short lenof, short lenwd, short lenbi, unsigned short msgoff) :
    interf(inf), header(hdr, hdrlen), trailer(trl, trllen), var(v),
    msglenoff(lenof), msglenwid(lenwd), msglenbias(lenbi), msgoffset(msgoff)
{
    if (mask != 0)
    {
        hdrMask.assign(mask, hdrlen);
        for (int i = 0; i < hdrlen; ++i)
        {
            header[i] &= hdrMask[i];
        }
    }
}

msg_rec::msg_rec(const msg_rec& right) :
    interf(right.interf), hdrMask(right.hdrMask), header(right.header),
    trailer(right.trailer), var(right.var),
    msglenoff(right.msglenoff), msglenwid(right.msglenwid), msglenbias(right.msglenbias), msgoffset(right.msgoffset)
{
}

msg_param_rec::msg_param_rec(int inf, const char* hst, int prt, unsigned int to) : interf(inf), host(hst), port(prt), timeout_msec(to)
{
}

msg_param_rec::msg_param_rec(const msg_param_rec& right) : interf(right.interf), host(right.host), port(right.port), timeout_msec(right.timeout_msec)
{
}

msg_same_header::msg_same_header(msg_var* p1, msg_var* p2) : first(p1), outputonly(p2)
{
}

msg_same_header::msg_same_header(const msg_same_header& right) : first(right.first), outputonly(right.outputonly)
{
}

msg_interf_typ::msg_interf_typ() :
    sock(0),
    cmntlr(0),
    cmntlrlen(0),
    msghdr(0),
    msghdrlen(0),
    hdrmask(0),
    var(0),
    varlen(0),
    msglenoff(0),
    msglenwid(0),
    msglenbias(0),
    msgoffset(0),
    buf_lock(false),
    timeout_msec(0)
{
}

msg_interf_typ::~msg_interf_typ()
{
    delete[] cmntlr;
    delete[] msghdr;
    delete[] hdrmask;
    delete[] var;
    delete sock;
}

void msg_interf_typ::addMessage(const char* mask, const char* header, unsigned short hdrlen, const char* trailer, unsigned short trllen,
                                unsigned short lenoff, short lenwid, short lenbias,
                                unsigned short msgoff, msg_var* var, int interf)
{
    msg_rec item(interf, mask, header, hdrlen, trailer, trllen, var, lenoff, lenwid, lenbias, msgoff);
    // check that init has not been called (TODO: Throw assert?)
    if (!msg_interf_list().empty())
    {
        return;
    }

    // create a new record list if not created
    if (msg_rec_list == 0)
    {
        msg_rec_list = new msg_rec_list_typ;
        msg_rec_list->push_back(item);
    }
    else
    {
        // insert into the list ordered based on the host (unsorted), followed by header (sorted)
        msg_rec_list_typ::iterator it;
        for (it = msg_rec_list->begin(); it != msg_rec_list->end(); ++it)
        {
            // match the host
            if ((*it).interf == interf)
            {
                break;
            }
        }
        for (; it != msg_rec_list->end(); ++it)
        {
            const int cmpresult = memcmp((*it).header.data(), item.header.data(), hdrlen);
            // break if header is more than
            if (cmpresult > 0)
            {
                break;
            }
            // break if its at the last message
            else if ((*it).interf != interf)
            {
                break;
            }
            else if (cmpresult == 0)
            {
                // same message
                if (msg_same_header_list == 0)
                {
                    msg_same_header_list = new msg_same_header_list_typ();
                }
                msg_same_header_list->push_back(msg_same_header((*it).var, var));
                return;
            }
        }
        msg_rec_list->insert(it, item);
    }
}

void msg_interf_typ::addInterface(const char* host, int port, unsigned int timeout, int interf)
{
    // check that init has not been called (TODO: Throw assert?)
    if (!msg_interf_list().empty())
    {
        return;
    }

    // create a new record list if not created
    if (m_param_rec_list == 0)
    {
        m_param_rec_list =  new m_param_rec_list_typ;
    }
    m_param_rec_list->push_front(msg_param_rec(interf, host, port, timeout));
}

void msg_interf_typ::init()
{
    // sanity check that list is created and not empty
    if ((msg_rec_list==0) || msg_rec_list->empty())
    {
        return;
    }

    // sanity check that list is created and not empty
    if ((m_param_rec_list==0) || m_param_rec_list->empty())
    {
        return;
    }

    msg_interf_list_typ::iterator it2;
    msg_rec_list_typ::const_iterator it = msg_rec_list->begin();
    int lastinterf = -1;
    msg_interf_typ* interf = 0;
    int cnt=0;
    bool mflag = false; //flag to indicate mask being used in interface
    // the first pass is to count the number of messages and instantiate the msg_interf_typ
    for (; it != msg_rec_list->end();)
    {
        if ((*it).interf != lastinterf)
        {
            cnt = 1;
            lastinterf = (*it).interf;
            // need to instantiate a new msg_interf_typ and open the port
            msg_interf_list().push_back(msg_interf_typ());
            interf = &(msg_interf_list().back());
            mflag = !(*it).hdrMask.empty(); //indicate this msg uses mask (reset)
            interf->msglenoff = (*it).msglenoff;
            interf->msglenwid = (*it).msglenwid;
            interf->msglenbias = (*it).msglenbias;
            interf->msgoffset = (*it).msgoffset;
            interf->msghdrlen = static_cast<unsigned short>((*it).header.size());
            if ((*it).trailer.size() > 0)
            {
                interf->cmntlrlen = static_cast<unsigned short>((*it).trailer.size());
                interf->cmntlr = new char[interf->cmntlrlen];
                memcpy(interf->cmntlr, (*it).trailer.data(), interf->cmntlrlen);
            }

            for (m_param_rec_list_typ::const_iterator it4=m_param_rec_list->begin(); it4!=m_param_rec_list->end(); ++it4)
            {
                if ((*it).interf == (*it4).interf)
                {
                    interf->sock = msg_proxy::sethost((*it4).host.c_str(), (*it4).port);
                    interf->timeout_msec = (*it4).timeout_msec;
                    break;
                }
            }
        }
        else
        {
            // count up one message
            ++cnt;
            mflag |= !(*it).hdrMask.empty(); //indicate this msg uses mask
        }

        msg_rec_list_typ::const_iterator it3 = it;
        // == INCREMENT == INCREMENT == INCREMENT ==
        ++it;
        // == INCREMENT == INCREMENT == INCREMENT ==

        if ((it == msg_rec_list->end()) || ((*it).interf != lastinterf))
        {
            // instantiate the array for the last interf

            // for the special case where there is only 1 message for an interface
            if (cnt == 1)
            {
                // check if there is a need to extract the length
                if ((*it3).msglenwid != 0)
                {
                    interf->find_var = &msg_interf_typ::find_var_2;
                }
                else
                {
                    interf->find_var = &msg_interf_typ::find_var_1;
                }
            }
            else
            {
                // check if there is a need to extract the length
                if ((*it3).msglenwid != 0)
                {
                    interf->find_var = &msg_interf_typ::find_var_4;
                }
                else
                {
                    interf->find_var = &msg_interf_typ::find_var_3;
                }
            }

            if (mflag)
            {
                //must use original header for mask interface
                interf->hdrmask = new char[cnt*(interf->msghdrlen)];
                interf->find_var = &msg_interf_typ::find_var_5;
            }

            interf->msghdr = new char[cnt*(interf->msghdrlen)];
            interf->var = new msg_var*[cnt];
            interf->varlen = cnt;
        }
    }

    lastinterf = -1;
    // the second pass is to copy the contents of the msg header and var
    for (it = msg_rec_list->begin(), it2 = msg_interf_list().begin(); it != msg_rec_list->end(); ++it, ++cnt)
    {
        if ((*it).interf != lastinterf)
        {
            lastinterf = (*it).interf;
            interf = &(*it2);
            ++it2;
            cnt = 0;
        }
        if (interf->hdrmask != 0)
        {

            //copy hdrmask if exist else set to default
            memcpy(interf->msghdr + (cnt*(interf->msghdrlen)), (*it).header.data(), interf->msghdrlen);

            //assign header mask
            if (!(*it).hdrMask.empty())
            {
                memcpy(interf->hdrmask + (cnt*(interf->msghdrlen)), (*it).hdrMask.data(), interf->msghdrlen);
            }
            else
            {
                memset(interf->hdrmask + (cnt*(interf->msghdrlen)), '\xFF', interf->msghdrlen);
            }

            //finally apply the mask and output mask onto the header and output header respectively
            const char* srchdr = (*it).header.data();
            const char* srcmsk = (*it).hdrMask.data();

            char* dstend = interf->msghdr + (cnt + 1)*(interf->msghdrlen);

            for (char* dst = interf->msghdr + cnt*(interf->msghdrlen); (dst != dstend); ++dst, ++srchdr, ++srcmsk)
            {
                *dst = *srchdr & *srcmsk;
            }
        }
        else
        {
            memcpy(interf->msghdr + (cnt*(interf->msghdrlen)), (*it).header.data(), interf->msghdrlen);
        }

        interf->var[cnt] = (*it).var;
        (*it).var->msgidx = cnt;
        (*it).var->interf = interf;
        if (msg_same_header_list)
        {
            for (msg_same_header_list_typ::iterator it5 = msg_same_header_list->begin(); it5 != msg_same_header_list->end();)
            {
                if ((*it5).first == (*it).var)
                {
                    (*it5).outputonly->msgidx = cnt;
                    (*it5).outputonly->interf = interf;
                    it5 = msg_same_header_list->erase(it5);
                }
                else
                {
                    ++it5;
                }
            }
        }
    }

    // clear the list
    delete msg_rec_list;
    delete m_param_rec_list;
    delete msg_same_header_list;
}

// single message, no length expected
short msg_interf_typ::find_var_1(msg_var** varptr, char*, unsigned short bufferlen)
{
    if ((var[0]->fixedmsglen != 0) && (bufferlen != var[0]->fixedmsglen))
    {
        return -1;
    }
    *varptr = var[0];
    return 0;
}

// single message, length check using message length field
short msg_interf_typ::find_var_2(msg_var** varptr, char* buffer, unsigned short bufferlen)
{
    if (bufferlen > msglenoff+abs(msglenwid))
    {
        // extract the length
        int len = 0;
        if (msglenwid < 0)
        {
            for (int idx=0; idx<-msglenwid; ++idx)
            {
                len <<= 8;
                len += *reinterpret_cast<unsigned char*>(buffer+msglenoff+idx);
            }
        }
        else
        {
            for (int idx=1; idx<=msglenwid; ++idx)
            {
                len <<= 8;
                len += *reinterpret_cast<unsigned char*>(buffer+msglenoff+msglenwid-idx);
            }
        }
        // apply bias
        len += msglenbias;
        if (len == bufferlen)
        {
            *varptr = var[0];
            return 0;
        }
    }
    return -1;
}

short msg_interf_typ::find_var_3_helper(msg_var** varptr, char* buffer)
{
    int lvaridx = 0;
    int rvaridx = varlen-1;
    while (lvaridx <= rvaridx)
    {
        const int varidx = (lvaridx+rvaridx)/2;
        int cmp = memcmp(buffer, msghdr+(varidx*msghdrlen), msghdrlen);
        if (cmp == 0)
        {
            // match is found
            *varptr = var[varidx];
            return 0;
        }
        else if (cmp < 0)
        {
            // smaller
            rvaridx = varidx-1;
        }
        else
        {
            // larger
            lvaridx = varidx+1;
        }
    }
    return -1;
}

short msg_interf_typ::find_var_5_helper(msg_var** varptr, char* buffer)
{
    int lvaridx = 0;
    int rvaridx = varlen - 1;
    while (lvaridx <= rvaridx)
    {
        const int varidx = (lvaridx + rvaridx) / 2;
        const unsigned char* hdr = (unsigned char*)msghdr + (varidx*msghdrlen);
        const unsigned char* msk = (unsigned char*)hdrmask + (varidx*msghdrlen);
        const unsigned char* buf = (unsigned char*)buffer;
        const unsigned char* end = buf + msghdrlen;
        for (;; ++buf, ++hdr, ++msk)
        {
            if (buf == end)
            {
                // match is found
                *varptr = var[varidx];
                return 0;
            }
            if ((*buf & *msk) < *hdr)
            {
                // smaller
                rvaridx = varidx - 1;
                break;
            }
            if ((*buf & *msk) > *hdr)
            {
                // larger
                lvaridx = varidx + 1;
                break;
            }
        }
    }
    return -1;
}

// multiple message, no length expected
short msg_interf_typ::find_var_3(msg_var** varptr, char* buffer, unsigned short bufferlen)
{
    short offs = find_var_3_helper(varptr, buffer);
    if ((offs != -1) && ((*varptr)->fixedmsglen != 0) && ((*varptr)->fixedmsglen != bufferlen))
    {
        offs = -1;
    }
    return offs;
}

// multiple message, length check using message length field
short msg_interf_typ::find_var_4(msg_var** varptr, char* buffer, unsigned short bufferlen)
{
    short offs = find_var_3_helper(varptr, buffer);
    if ((offs != -1) && (bufferlen > msglenoff+abs(msglenwid)))
    {
        // extract the length
        int len = 0;
        if (msglenwid < 0)
        {
            for (int idx=0; idx<-msglenwid; ++idx)
            {
                len <<= 8;
                len += *reinterpret_cast<unsigned char*>(buffer+msglenoff+idx);
            }
        }
        else
        {
            for (int idx=1; idx<=msglenwid; ++idx)
            {
                len <<= 8;
                len += *reinterpret_cast<unsigned char*>(buffer+msglenoff+msglenwid-idx);
            }
        }
        // apply bias
        len += msglenbias;
        if (len != bufferlen)
        {
            offs = -1;
        }
    }
    return offs;
}

short msg_interf_typ::find_var_5(msg_var** varptr, char* buffer, unsigned short bufferlen)
{
    short offs = find_var_5_helper(varptr, buffer);
    if ((offs != -1) && (msglenwid != 0) && (bufferlen > msglenoff + abs(msglenwid)))
    {
        // extract the length
        int len = 0;
        if (msglenwid < 0)
        {
            for (int idx = 0; idx < -msglenwid; ++idx)
            {
                len <<= 8;
                len += *reinterpret_cast<unsigned char*>(buffer + msglenoff + idx);
            }
        }
        else
        {
            for (int idx = 1; idx <= msglenwid; ++idx)
            {
                len <<= 8;
                len += *reinterpret_cast<unsigned char*>(buffer + msglenoff + msglenwid - idx);
            }
        }
        // apply bias
        len += msglenbias;
        if (len != bufferlen)
        {
            offs = -1;
        }
    }
    else if ((offs != -1) && ((*varptr)->fixedmsglen != 0) && ((*varptr)->fixedmsglen != bufferlen))
    {
        offs = -1;
    }
    return offs;
}

char* msg_interf_typ::get_buffer(int sz)
{
    static char buf[MSG_BUF];
    // use the end of read buffer for temporary write buffer
    const int reqlen = sz + msgoffset + cmntlrlen;
    if ((reqlen > MSG_BUF) || buf_lock)
    {
        return 0;
    }
    else
    {
        buf_lock = true;
        return buf + msgoffset;
    }
}

bool msg_interf_typ::write_buffer(char* buf, int sz, int idx)
{
    // check for valid index
    if (idx >= varlen)
    {
        buf_lock = false;
        return false;
    }

    // write the common trailer if any
    if (cmntlrlen != 0)
    {
        memcpy(buf+sz, cmntlr, cmntlrlen);
    }

    // move the buf back to beginning of packet
    buf -= msgoffset;

    // write the specific header
    memcpy(buf, msghdr+(idx*msghdrlen), msghdrlen);

    // write the length
    if (msglenwid != 0)
    {
        unsigned int len = sz + msgoffset + cmntlrlen - msglenbias;
        if (msglenwid > 0)
        {
            for (int idx1=0; idx1<msglenwid; ++idx1)
            {
                buf[msglenoff+idx1] = (len % 256);
                len /= 256;
            }
        }
        else
        {
            for (int idx1=0; idx1>msglenwid; --idx1)
            {
                buf[msglenoff-msglenwid-1+idx1] = (len % 256);
                len /= 256;
            }
        }
    }

    // send the packet
    {
        int len = sz + msgoffset + cmntlrlen;
        int result = sock->send(buf, len);
        buf_lock = false;
        return result == len;
    }
}

bool msg_interf_typ::read_data()
{
    char* buf;
    size_t len;
    bool hasdata = false;

    while (sock->recv(&buf, len))
    {
        msg_var* varptr;
        int offset = (this->*find_var)(&varptr, buf, static_cast<unsigned short>(len));
        if (offset == 0)
        {
            // pass the packet for processing
            varptr->extract(static_cast<int>(len-msgoffset-cmntlrlen), reinterpret_cast<unsigned char*>(buf+msgoffset));
        }
        hasdata = true;
    }
    return hasdata;
}

void msg_interf_typ::incoming()
{
    for (msg_interf_list_typ::iterator it=msg_interf_list().begin(); it!=msg_interf_list().end(); ++it)
    {
        if ((*it).timeout_msec == 0)
        {
            (*it).read_data();
        }
        else
        {
            struct timeval tm = {
                static_cast<long>((*it).timeout_msec)/1000L, (static_cast<long>((*it).timeout_msec)%1000L)*1000L
            };
            timeExpire te(&tm);
            while (!((*it).read_data() || te()))
                context_yield();
        }
    }
}

void msg_interf_typ::outgoing()
{
    for (msg_interf_list_typ::iterator it=msg_interf_list().begin(); it!=msg_interf_list().end(); ++it)
    {
        for (int cnt=0; cnt<(*it).varlen; ++cnt)
        {
            (*it).var[cnt]->refreshRM();
        }
    }
}

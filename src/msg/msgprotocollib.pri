!macx:!win32:CONFIG += no_1553_p13 no_1553_c13 no_1553_l13
DEFINES += NO_MDT_PROTOCOL

# Input
HEADERS += $$PWD/../../include/msg_var.h   $$PWD/msg_interf.h   $$PWD/msg_proxy.h
SOURCES += $$PWD/msg_var.cpp $$PWD/msg_interf.cpp $$PWD/msg_proxy.cpp

no_1553 {
 DEFINES += NO_B1553_PROTOCOL
} else {
 INCLUDEPATH += $$PWD/1553
 HEADERS += $$PWD/bus1553_intf.h 
 SOURCES += $$PWD/bus1553_intf.cpp
 win32 {
  HEADERS += $$PWD/dlfcn/dlfcn.h
  SOURCES += $$PWD/dlfcn/dlfcn.c
  INCLUDEPATH += $$PWD/dlfcn
  LIBS += psapi.lib
 }
 !no_1553_p13 {
  HEADERS += $$PWD/bus1553_intf_p13.h $$PWD/1553/p13w32.h 
  HEADERS *= $$PWD/bus1553_intf_ballard.h 
  SOURCES += $$PWD/bus1553_intf_p13.cpp 
 } else:DEFINES += NO_BUS1553_P13
 !no_1553_c13 {
  HEADERS += $$PWD/bus1553_intf_c13.h $$PWD/1553/c13w32.h
  HEADERS *= $$PWD/bus1553_intf_ballard.h 
  SOURCES += $$PWD/bus1553_intf_c13.cpp
 } else:DEFINES += NO_BUS1553_C13
 !no_1553_l13 {
  HEADERS += $$PWD/bus1553_intf_l13.h $$PWD/1553/l13w32.h
  HEADERS *= $$PWD/bus1553_intf_ballard.h 
  SOURCES += $$PWD/bus1553_intf_l13.cpp
 } else:DEFINES += NO_BUS1553_L13
 !no_1553_bti {
  HEADERS += $$PWD/bus1553_intf_bti.h $$PWD/1553/bti1553.h
  HEADERS *= $$PWD/bus1553_intf_ballard.h 
  SOURCES += $$PWD/bus1553_intf_bti.cpp
 } else:DEFINES += NO_BUS1553_BTI
}
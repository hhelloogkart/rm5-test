/******************************************************************/
/* Copyright DSO National Laboratories 2010. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include <outbuf.h>
#include <defaults.h>
#include <defutil.h>
#include <time/timecore.h>
#include <util/dbgout.h>
#include <msg_var.h>
#include "msg_interf.h"

static protocol_description& description()
{
    static protocol_description d = {
        2,
        &msg_var::initialize,
        &msg_var::incoming,
        &msg_var::outgoing,
        0
    };
    return d;
}

/**
    @param _nm name of variable
    @param _mask - value of mask for serial message header, mask size is hdrlen
    @param _hdr value of header of msg message, header size is hdrlen1+hdrlen2 (max is sizeof(int))
    @param _hdrlen1 size of header in bytes
 */
msg_var::msg_var(const char* hdr, unsigned short hdrlen, unsigned short fixmsglen,
                 unsigned short msglenoffset, unsigned short msglenwidth, unsigned short msglenbias,
                 const char* trailer, unsigned short trailerlen, unsigned short msgoffset, int interf_num, const char* mask) :
    msgidx(0), fixedmsglen(fixmsglen), dirty(false), interf(0)
{
    rmclient_init::install_protocol(description(), false);
    msg_interf_typ::addMessage(mask, hdr, hdrlen, trailer, trailerlen, msglenoffset, msglenwidth, msglenbias, msgoffset, this, interf_num);
}

msg_var::msg_var() :
    msgidx(0), fixedmsglen(0), dirty(false), interf(0)
{
    rmclient_init::install_protocol(description(), false);
}

void msg_var::assistedConstruct(const char * hdr, unsigned short hdrlen, unsigned short fixmsglen, unsigned short msglenoffset, unsigned short msglenwidth, unsigned short msglenbias, const char * trailer, unsigned short trailerlen, unsigned short msgoffset, int interf_num, const char * mask)
{
    fixedmsglen = fixmsglen;
    msg_interf_typ::addMessage(mask, hdr, hdrlen, trailer, trailerlen, msglenoffset, msglenwidth, msglenbias, msgoffset, this, interf_num);
}

void msg_var::setRMDirty()
{
    dirty = true;
}

bool msg_var::isRMDirty(void) const
{
    return dirty;
}

mpt_var* msg_var::getNext()
{
    return this + 1;
}

istream& msg_var::operator>>(istream& s)
{
    s >> msgidx;
    return complex_var::operator>>(s);
}

ostream& msg_var::operator<<(ostream& s) const
{
    s << msgidx << ' ';
    return complex_var::operator<<(s);
}

void msg_var::refreshRM()
{
    if (dirty && interf)
    {
        outbuf buf;
        const int sz = size();
        char* ptr = interf->get_buffer(sz);
        buf.set(reinterpret_cast<unsigned char*>(ptr), sz);
        output(buf);
        dirty = !interf->write_buffer(ptr, sz, msgidx);
    }
}

void msg_var::outgoing()
{
    static bool reentrant = false;
    if (!reentrant)
    {
        reentrant = true;
        msg_interf_typ::outgoing();
        reentrant = false;
    }
}

void msg_var::incoming()
{
    static bool reentrant = false;
    if (!reentrant)
    {
        reentrant = true;
        msg_interf_typ::incoming();
        reentrant = false;
    }
}

void msg_var::initialize(defaults* def)
{
    if (def!=0)
    {
        set_msg_param(def);
    }
    msg_interf_typ::init();
}

void msg_var::set_msg_param(const char* host, int port, unsigned int timeout, int interf_num)
{
    msg_interf_typ::addInterface(host, port, timeout, interf_num);
}

void msg_var::set_msg_param(defaults* def)
{
    if (def==0)
    {
        return;
    }

    tree_node* tn1=0, * tn2=0, * tn3=0, * tn4=0;
    char host[1024] = ".\\Private$\\dso";
    int port = 0;
    int sec = 0;
    int usec = 0;

    for (int interf_no=0; (tn1=def->getstring("msg_host", host, tn1))!=0; ++interf_no)
    {
        if ((interf_no==0)||(tn2!=0))
        {
            tn2=def->getint("msg_port", &port, tn2);
        }
        if ((interf_no==0)||(tn3!=0))
        {
            tn3=def->getint("msg_timeout_sec", &sec, tn3);
        }
        if ((interf_no==0)||(tn4!=0))
        {
            tn4=def->getint("msg_timeout_usec", &usec, tn4);
        }
        set_msg_param(host, port, sec*1000+usec/1000, interf_no);
    }
}

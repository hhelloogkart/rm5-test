#ifndef _BUS1553_INTF_BALLARD_H_
#define _BUS1553_INTF_BALLARD_H_

#include "bus1553_intf.h"
#include <map>
#include <vector>

#ifndef MSGADDR
typedef unsigned long MSGADDR;
#endif

struct bc_data_typ
{
    unsigned short* data;
    MSGADDR msg;

    bc_data_typ() : data(0), msg(0)
    {
    }

    void alloc(void)
    {
        data = new unsigned short [32];
    }

    void destroy(void)
    {
        delete[] data;
        data = 0;
    }
};

typedef std::map<unsigned short, bc_data_typ> bc_map_typ;
typedef std::vector<unsigned short> cwd_list_typ;

#endif
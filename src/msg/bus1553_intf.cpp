/******************************************************************/
/* Copyright DSO National Laboratories 2016. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "bus1553_intf.h"
#include <string.h>
#ifndef _WIN32
#define stricmp strcasecmp
#endif

#if !defined(NO_BUS1553_P13)
#include "bus1553_intf_p13.h"
#endif
#if !defined(NO_BUS1553_C13)
#include "bus1553_intf_c13.h"
#endif
#if !defined(NO_BUS1553_L13)
#include "bus1553_intf_l13.h"
#endif
#if !defined(NO_BUS1553_BTI)
#include "bus1553_intf_bti.h"
#endif

//test: host=":"            return 1 "\0"
//      host="a:"           return 2 "a\0"
//      host="abcdefg:"     return 8 "abcdefg\0"
//      host="abcdefgh:"    ignore, return 0
//      host="abcdefghi"    ignore, return 0
//      host="abc"          ignore, return 0
inline int find_protocol_type(const char* host, char* protocol, char sep)
{
    for (int idx=0; (idx<8) && (host[idx]!='\0'); ++idx)
    {
        if (host[idx]==sep)
        {
            protocol[idx] = '\0';
            return idx+1;
        }
        else
        {
            protocol[idx] = host[idx];
        }
    }
    return 0;
}

//p13:bc=frame_time_us;cwdlist[;cwdlist]* where cwdlist=cwd[,cwd]* rt-rt: rt txcwd low 16bit, rt rxcwd upper 16bit
//p13:rt=<rtno>
//p13:mon=rtno[;rtno]*
//p13:port=irqno (0 = 2, 5 etc)
//c13:port=cardnum
//bti:port=channelno*100 + coreno*10 + cardno
Bus1553Intf* Bus1553Intf::Open(const char* host, int port)
{
    Bus1553Intf* intf = 0;
    bool ret = false;
    char prot[8];
    int offset = find_protocol_type(host, prot, ':');

#ifndef NO_BUS1553_BTI
    if ((strcmp("bti", prot)==0) || (offset==0))
    {
        intf = new Bus1553BtiIntf;
    }
#endif
#ifndef NO_BUS1553_L13
    if ((strcmp("l13", prot)==0) || (offset==0))
    {
        intf = new Bus1553L13Intf;
    }
#endif
#ifndef NO_BUS1553_C13
    if ((strcmp("c13", prot)==0) || (offset==0))
    {
        intf = new Bus1553C13Intf;
    }
#endif
#ifndef NO_BUS1553_P13
    if ((strcmp("p13", prot)==0) || (offset==0))
    {
        intf = new Bus1553P13Intf;
    }
#endif

    if(intf)
    {
        int offset2 = find_protocol_type(host+offset, prot, '=');
        if (stricmp("bc", prot)==0)
        {
            intf->mode = MODE_BC;
            ret = intf->BCOpen(host+offset+offset2, port);
        }
        else if (stricmp("rt", prot)==0)
        {
            intf->mode = MODE_RT;
            ret = intf->RTOpen(host+offset+offset2, port);
        }
        else //if ((stricmp("mon", prot)==0) || (offset2==0))
        {
            intf->mode = MODE_MON;
            ret = intf->MonOpen(host+offset+offset2, port);
        }
        if(!ret)
        {
            intf->Close();
            delete intf;
            intf = 0;
        }
    }
    return intf;
}

unsigned int Bus1553Intf::Recv(unsigned short* buf, size_t len)
{
    unsigned int ret = 0;
    if(len >= 33)
    {
        if(mode == MODE_BC || mode == MODE_RT)
        {
            ret = MsgRead(buf);
        }
        else //if(mode == MODE_MON)
        {
            ret = MonRead(buf);
        }
    }
    return ret;
}

unsigned int Bus1553Intf::Send(const unsigned short* buf, size_t len)
{
    if((mode == MODE_BC || mode == MODE_RT) && len > 0)
    {
        unsigned int ta;
        unsigned int tr;
        unsigned int sa;
        unsigned int wc;
        UnpackCWD(buf[0], &ta, &tr, &sa, &wc);
        if(wc == 0)
        {
            wc = 32;
        }

        if(len >= (wc + 1))
        {
            return MsgWrite(buf + 1, buf[0]);
        }
    }
    return 0;
}

unsigned short Bus1553Intf::PackCWD(unsigned int ta, unsigned int tr, unsigned int sa, unsigned int wc)
{
    //return P13_ValPackCWD(ta, tr, sa, wc);
    return ((ta & 0x1f) << 11) | ((tr & 0x1) << 10) | ((sa & 0x1f) << 5) | (wc & 0x1f);
}

void Bus1553Intf::UnpackCWD(unsigned short cwd, unsigned int* ta, unsigned int* tr, unsigned int* sa, unsigned int* wc)
{
    //P13_ValUnpackCWD(cwd, ta, tr, sa, wc);
    *ta = cwd >> 11;
    *tr = (cwd >> 10) & 0x01;   //RT rcv = 0, RT tx = 1
    *sa = (cwd >> 5) & 0x1f;
    *wc = (cwd >> 0) & 0x1f;
}

Bus1553Intf::~Bus1553Intf()
{
}

/******************************************************************/
/* Copyright DSO National Laboratories 2016. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#include "bus1553_intf_l13.h"
#include <functional>
#include <l13w32.h>
#include <ctype.h>
#include <assert.h>
#include "dlfcn.h"

#ifdef _WIN32
#ifdef _WIN64
#define LIBNAME "L13W64.DLL"
#else
#define LIBNAME "L13W32.DLL"
#endif
#else
#define LIBNAME "libl13.so"
#endif

static ERRVAL(__stdcall *pfn_L13_BCConfig)(ULONG, HCARD) = NULL;
static LISTADDR(__stdcall *pfn_L13_BCCreateList)(ULONG, INT, ULONG, USHORT, USHORT, LPUSHORT, HCARD) = NULL;
static SCHNDX(__stdcall *pfn_L13_BCSchedFrame)(ULONG, HCARD) = NULL;
static SCHNDX(__stdcall *pfn_L13_BCSchedMsg)(MSGADDR, HCARD) = NULL;
static HCARD(__stdcall *pfn_L13_CardOpen)(INT) = NULL;
static VOID(__stdcall *pfn_L13_CardReset)(HCARD) = NULL;
static ERRVAL(__stdcall *pfn_L13_CardClose)(HCARD) = NULL;
static ERRVAL(__stdcall *pfn_L13_CardStart)(HCARD) = NULL;
static BOOL(__stdcall *pfn_L13_CardStop)(HCARD) = NULL;
static INT(__stdcall *pfn_L13_ListDataRd)(LPUSHORT, INT, LISTADDR, HCARD) = NULL;
static INT(__stdcall *pfn_L13_ListDataWr)(LPUSHORT, INT, LISTADDR, HCARD) = NULL;
static ERRVAL(__stdcall *pfn_L13_MonConfig)(ULONG, HCARD) = NULL;
static ERRVAL(__stdcall *pfn_L13_MonFilterTA)(ULONG, HCARD) = NULL;
static USHORT(__stdcall *pfn_L13_MonRd)(LPUSHORT, HCARD) = NULL;
static VOID(__stdcall *pfn_L13_MsgDataRd)(LPUSHORT, INT, MSGADDR, HCARD) = NULL;
static VOID(__stdcall *pfn_L13_MsgDataWr)(LPUSHORT, INT, MSGADDR, HCARD) = NULL;
static ULONG(__stdcall *pfn_L13_MsgFieldRd)(USHORT, MSGADDR, HCARD) = NULL;
static ULONG(__stdcall *pfn_L13_MsgFieldWr)(ULONG, USHORT, MSGADDR, HCARD) = NULL;
static ERRVAL(__stdcall *pfn_L13_RTConfig)(ULONG, INT, HCARD) = NULL;
static MSGADDR(__stdcall *pfn_L13_RTGetMsg)(BOOL, INT, BOOL, INT, HCARD) = NULL;

struct Bus1553L13IntfPrivate
{
    HCARD hc;
    //bc data
    bc_map_typ bc_map;
    cwd_list_typ bc_list;
    cwd_list_typ::iterator bc_it;
    //rt data
    unsigned int rt;
    unsigned short sa_read;
};

inline int find_protocol_type(const char* host, char* protocol, char sep)
{
    for (int idx = 0; (idx < 8) && (host[idx] != '\0'); ++idx)
    {
        if (host[idx] == sep)
        {
            protocol[idx] = '\0';
            return idx + 1;
        }
        else
        {
            protocol[idx] = host[idx];
        }
    }
    return 0;
}

Bus1553L13Intf::Bus1553L13Intf() : Bus1553Intf(), prvt(new Bus1553L13IntfPrivate)
{
    void* hdl = dlopen(LIBNAME, RTLD_NOW | RTLD_LOCAL);
    if (hdl)
    {
        pfn_L13_BCConfig = (ERRVAL(__stdcall*)(ULONG, HCARD))dlsym(hdl, "_L13_BCConfig@8");
        pfn_L13_BCCreateList = (LISTADDR(__stdcall*)(ULONG, INT, ULONG, USHORT, USHORT, LPUSHORT, HCARD))dlsym(hdl, "_L13_BCCreateList@28");
        pfn_L13_BCSchedFrame = (SCHNDX(__stdcall*)(ULONG, HCARD))dlsym(hdl, "_L13_BCSchedFrame@8");
        pfn_L13_BCSchedMsg = (SCHNDX(__stdcall*)(MSGADDR, HCARD))dlsym(hdl, "_L13_BCSchedMsg@8");
        pfn_L13_CardOpen = (HCARD(__stdcall*)(INT))dlsym(hdl, "_L13_CardOpen@4");
        pfn_L13_CardReset = (VOID(__stdcall*)(HCARD))dlsym(hdl, "_L13_CardReset@4");
        pfn_L13_CardClose = (ERRVAL(__stdcall*)(HCARD))dlsym(hdl, "_L13_CardClose@4");
        pfn_L13_CardStart = (ERRVAL(__stdcall*)(HCARD))dlsym(hdl, "_L13_CardStart@4");
        pfn_L13_CardStop = (BOOL(__stdcall*)(HCARD))dlsym(hdl, "_L13_CardStop@4");
        pfn_L13_ListDataRd = (INT(__stdcall*)(LPUSHORT, INT, LISTADDR, HCARD))dlsym(hdl, "_L13_ListDataRd@16");
        pfn_L13_ListDataWr = (INT(__stdcall*)(LPUSHORT, INT, LISTADDR, HCARD))dlsym(hdl, "_L13_ListDataWr@16");
        pfn_L13_MonConfig = (ERRVAL(__stdcall*)(ULONG, HCARD))dlsym(hdl, "_L13_MonConfig@8");
        pfn_L13_MonFilterTA = (ERRVAL(__stdcall*)(ULONG, HCARD))dlsym(hdl, "_L13_MonFilterTA@8");
        pfn_L13_MonRd = (USHORT(__stdcall*)(LPUSHORT, HCARD))dlsym(hdl, "_L13_MonRd@8");
        pfn_L13_MsgDataRd = (VOID(__stdcall*)(LPUSHORT, INT, MSGADDR, HCARD))dlsym(hdl, "_L13_MsgDataRd@16");
        pfn_L13_MsgDataWr = (VOID(__stdcall*)(LPUSHORT, INT, MSGADDR, HCARD))dlsym(hdl, "_L13_MsgDataWr@16");
        pfn_L13_MsgFieldRd = (ULONG(__stdcall*)(USHORT, MSGADDR, HCARD))dlsym(hdl, "_L13_MsgFieldRd@12");
        pfn_L13_MsgFieldWr = (ULONG(__stdcall*)(ULONG, USHORT, MSGADDR, HCARD))dlsym(hdl, "_L13_MsgFieldWr@16");
        pfn_L13_RTConfig = (ERRVAL(__stdcall*)(ULONG, INT, HCARD))dlsym(hdl, "_L13_RTConfig@12");
        pfn_L13_RTGetMsg = (MSGADDR(__stdcall*)(BOOL, INT, BOOL, INT, HCARD))dlsym(hdl, "_L13_RTGetMsg@20");
    }

    prvt->hc = (-1);
    prvt->rt = 0;
    prvt->sa_read = 0;
    prvt->bc_it = prvt->bc_list.end();
}

Bus1553L13Intf::~Bus1553L13Intf()
{
    delete(prvt);
}

bool Bus1553L13Intf::MonOpen(const char* host, int port)
{
    if (L13Open(port))
    {
        if (pfn_L13_MonConfig(MONCFG_DEFAULT | MONCFG_CONTINUOUS, prvt->hc) < 0)
        {
            Close();
            return false;
        }

        char rtstr[8];
        int rt;
        int offset = find_protocol_type(host, rtstr, ',');
        int offset2 = 0;
        unsigned int filter_rt = 0;

        while (1)
        {
            if (offset == 0 && isdigit(host[offset2]))
            {
                rt = atoi(host + offset2);
                filter_rt |= (1 << rt);
                break;
            }
            else if (offset > 2 && isdigit(rtstr[0]))
            {
                rt = atoi(rtstr);
                filter_rt |= (1 << rt);
                offset2 += offset;
                offset = find_protocol_type(host + offset2, rtstr, ',');
            }
            else
            {
                break;
            }
        }

        if (filter_rt)
        {
            pfn_L13_MonFilterTA(filter_rt, prvt->hc);
        }

        pfn_L13_CardStart(prvt->hc);
        return true;
    }
    return false;
}

bool Bus1553L13Intf::RTOpen(const char* host, int port)
{
    if (L13Open(port))
    {
        prvt->sa_read = 0;
        prvt->rt = atoi(host);
        if (pfn_L13_RTConfig(RTCFG_DEFAULT, prvt->rt, prvt->hc) < 0)
        {
            Close();
            return false;
        }
        pfn_L13_CardStart(prvt->hc);
        return true;
    }
    return false;
}

bool Bus1553L13Intf::BCOpen(const char* host, int port)
{
    if (L13Open(port))
    {
        if (pfn_L13_BCConfig(BCCFG_DEFAULT, prvt->hc) < 0)
        {
            Close();
            return false;
        }

        //parse frame_time_us;cwdlist[;cwdlist]* where cwdlist=cwd[,cwd]*
        int frame_time_us = atoi(host);
        const char* str = strchr(host, ';');
        const char* endp;
        unsigned long cwd;
        unsigned short int cwd1, cwd2;
        bc_map_typ::iterator it;

        pfn_L13_BCSchedFrame(frame_time_us, prvt->hc);
        while (str && ((*str == ';') || (*str == ',')))
        {
            ++str;
            cwd = strtoul(str, const_cast<char**>(&endp), 0);
            if (str == endp)
            {
                break;
            }
            cwd1 = (unsigned short)(cwd & 0xFFFF);
            cwd2 = (unsigned short)(cwd >> 16);
            it = prvt->bc_map.find(cwd1);
            if (it == prvt->bc_map.end())
            {
                bc_data_typ d;
                unsigned int opt = MSGCRT_DEFAULT | MSGCRT_HIT | MSGCRT_TIMETAG;
                if (cwd2)
                {
                    opt |= MSGCRT_RTRT;
                }
                d.alloc();
                d.msg = (MSGADDR)pfn_L13_BCCreateList(LISTCRT_PINGPONG, 0, opt, cwd1, cwd2, d.data, prvt->hc);
                bc_map_typ::value_type v(cwd1, d);
                it = prvt->bc_map.insert(v).first; //RT transmit
                prvt->bc_list.push_back(cwd1);
                if (cwd2)
                {
                    bc_map_typ::value_type v2(cwd2, d);
                    prvt->bc_map.insert(v2); //RT receive
                    prvt->bc_list.push_back(cwd2);
                }
            }
            if (*endp == ';')
            {
                pfn_L13_BCSchedMsg((*it).second.msg, prvt->hc);
                pfn_L13_BCSchedFrame(frame_time_us, prvt->hc);
            }
            else
            {
                pfn_L13_BCSchedMsg((*it).second.msg, prvt->hc);
            }
            str = endp;
        }

        prvt->bc_it = prvt->bc_list.end();
        pfn_L13_CardStart(prvt->hc);
        return true;
    }
    return false;
}

void Bus1553L13Intf::Close()
{
    if (prvt->hc >= 0)
    {
        pfn_L13_CardStop(prvt->hc);
        pfn_L13_CardClose(prvt->hc);

        bc_map_typ::iterator it;
        for (it = prvt->bc_map.begin(); it != prvt->bc_map.end(); ++it)
        {
            (*it).second.destroy();
        }
        prvt->bc_map.clear();
        prvt->bc_list.clear();
        prvt->hc = -1;
    }
}

unsigned int Bus1553L13Intf::MonRead(unsigned short* buf)
{
    USHORT MsgBuffer[MONRD_MAX_COUNT];
    unsigned short wc;

    for (int i = 0; i < 2; ++i)
    {
        if ((wc = pfn_L13_MonRd(MsgBuffer, prvt->hc)) > 9 &&
            (wc <= 32 + 9) &&
            (((wc - 9) & 0x1f) == (MsgBuffer[5] & 0x1f)) &&
            (MsgBuffer[3] & 0x4000) == 0) //check error
        {
            buf[0] = MsgBuffer[5]; //cwd1
            wc -= 9;
            memcpy(buf + 1, MsgBuffer + 9, wc * 2);
            return wc + 1;
        }
    }
    return 0;
}

unsigned int Bus1553L13Intf::MonRead(unsigned short* buf, unsigned int nbuf)
{
    unsigned int i;

    for (i = 0; i < nbuf && MonRead(buf) > 0; ++i)
    {
        buf += 33;
    }
    return i;
}


unsigned int Bus1553L13Intf::MsgRead(unsigned short* buf)
{
    unsigned int ret = 0;

    if (mode == MODE_BC)
    {
        if (prvt->bc_it == prvt->bc_list.end())
        {
            prvt->bc_it = prvt->bc_list.begin();
        }
        else
        {
            while (prvt->bc_it != prvt->bc_list.end())
            {
                unsigned short c = *prvt->bc_it;
                ++prvt->bc_it;

                unsigned int ta;
                unsigned int tr;
                unsigned int sa;
                unsigned int wc;
                UnpackCWD(c, &ta, &tr, &sa, &wc);
                if (tr == XMT) //RT to BC
                {
                    bc_map_typ::iterator it = prvt->bc_map.find(c);
                    if (it != prvt->bc_map.end())
                    {
                        if (wc == 0)
                        {
                            wc = 32;
                        }
                        buf[0] = c;
                        pfn_L13_ListDataRd(buf + 1, (int)wc, (*it).second.msg, prvt->hc);
                        ret = wc + 1;
                        break;
                    }
                }
            }
        }
    }
    else if (mode == MODE_RT)
    {
        for (int i = 1; i <= 31; ++i)
        {
            if (prvt->sa_read >= 31)
            {
                prvt->sa_read = 0;
                break;
            }
            if (prvt->sa_read <= 0)
            {
                ++prvt->sa_read;
                break; //hack for msg_interf_typ::read_data() to beak
            }

            MSGADDR m = pfn_L13_RTGetMsg(0, prvt->rt, RCV, prvt->sa_read, prvt->hc);
            ++prvt->sa_read;
            unsigned long e = pfn_L13_MsgFieldRd(FIELD_ERROR, m, prvt->hc);
            if (e & MSGERR_HIT)  //check that new message received
            {
                unsigned short c = (unsigned short)pfn_L13_MsgFieldRd(FIELD_CWD1, m, prvt->hc);
                unsigned int ta1;
                unsigned int tr1;
                unsigned int sa1;
                unsigned int wc1;
                UnpackCWD(c, &ta1, &tr1, &sa1, &wc1);
                if (wc1 == 0)
                {
                    wc1 = 32;
                }
                pfn_L13_MsgDataRd(buf + 1, (int)wc1, m, prvt->hc);
                pfn_L13_MsgFieldWr(0, FIELD_ERROR, m, prvt->hc);    //clear 'hit' bit
                buf[0] = c;
                ret = wc1 + 1;
                break;
            }
        }
    }
    return ret;
}

bool Bus1553L13Intf::MsgRead(unsigned short* buf, unsigned short cwd)
{
    bool ret = false;
    unsigned int ta;
    unsigned int tr;
    unsigned int sa;
    unsigned int wc;
    UnpackCWD(cwd, &ta, &tr, &sa, &wc);
    if (wc == 0)
    {
        wc = 32;
    }

    if (mode == MODE_BC)
    {
        bc_map_typ::iterator it = prvt->bc_map.find(cwd);
        if (it != prvt->bc_map.end())
        {
            pfn_L13_ListDataRd(buf, (int)wc, (*it).second.msg, prvt->hc);
            ret = true;
        }
    }
    else if (mode == MODE_RT)
    {
        MSGADDR m = pfn_L13_RTGetMsg(sa == 0 || sa == 0x1F, ta, tr, sa, prvt->hc);
        if (tr == RCV) //BC to RT
        {
            unsigned long e = pfn_L13_MsgFieldRd(FIELD_ERROR, m, prvt->hc);
            if (e & MSGERR_HIT)  //check that new message received
            {
                pfn_L13_MsgDataRd(buf, (int)wc, m, prvt->hc);
                pfn_L13_MsgFieldWr(0, FIELD_ERROR, m, prvt->hc);    //clear 'hit' bit
                ret = true;
            }
        }
        else //RT to BC
        {
            pfn_L13_MsgDataRd(buf, (int)wc, m, prvt->hc);
            ret = true;
        }
    }
    return ret;
}

bool Bus1553L13Intf::MsgWrite(const unsigned short* buf, unsigned short cwd)
{
    bool ret = false;
    unsigned int ta;
    unsigned int tr;
    unsigned int sa;
    unsigned int wc;
    UnpackCWD(cwd, &ta, &tr, &sa, &wc);
    if (wc == 0)
    {
        wc = 32;
    }

    if (mode == MODE_BC)
    {
        if (tr == RCV) //BC to RT
        {
            bc_map_typ::iterator it;
            it = prvt->bc_map.find(cwd);
            if (it != prvt->bc_map.end())
            {
                pfn_L13_ListDataWr(const_cast<LPUSHORT>(buf), (int)wc, (*it).second.msg, prvt->hc);
                ret = true;
            }
        }
    }
    else if (mode == MODE_RT)
    {
        if (tr == XMT) //RT to BC
        {
            MSGADDR m = pfn_L13_RTGetMsg(sa == 0 || sa == 0x1F, ta, tr, sa, prvt->hc);
            unsigned long e = pfn_L13_MsgFieldRd(FIELD_ERROR, m, prvt->hc);
            if (e & MSGERR_HIT) //check that last message sent
            {
                pfn_L13_MsgDataWr(const_cast<LPUSHORT>(buf), (int)wc, m, prvt->hc);
                pfn_L13_MsgFieldWr(0, FIELD_ERROR, m, prvt->hc); //clear 'hit' bit
                ret = true;
            }
        }
    }
    return ret;
}

bool Bus1553L13Intf::L13Open(int port)
{
    Close();

    prvt->hc = pfn_L13_CardOpen(port);

    if (prvt->hc < 0)
    {
        return false;
    }
    pfn_L13_CardReset(prvt->hc);
    return true;
}

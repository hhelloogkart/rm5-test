/******************************************************************/
/* Copyright DSO National Laboratories 2010. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _MSG_INTERF_H_
#define _MSG_INTERF_H_

#include "msg_proxy.h"

class msg_var;
const int MSG_BUF = 4096; // shared buffer for input and output

class msg_interf_typ
{
private:
    // msg socket
    msg_proxy* sock;
    // contains the common trailer for all messages from this interface, if any
    char* cmntlr;
    unsigned short cmntlrlen;
    // sorted array containing the unique identifier for the msg packet
    // headers should all have a fixed length, the size of the array is msghdrlen * varlen
    char* msghdr;
    unsigned short msghdrlen;
    // for non-masking logic hdrmask will be 0
    char* hdrmask;
    // array containing pointers to the msg_var, ordered relative to msghdr
    msg_var** var;
    unsigned short varlen;
    // getting the length of the packet
    unsigned short msglenoff;
    short msglenwid;
    short msglenbias;
    // message offset to pass to child for extract
    unsigned short msgoffset;
    // buffer lock
    bool buf_lock;
    // timeout in milliseconds to block on msg port
    unsigned int timeout_msec;

public:
    // create the object
    msg_interf_typ();
    // delete the object
    ~msg_interf_typ();
    // checks for data from the msg ports
    static void incoming();
    // checks for dirty flag and outputs the var
    static void outgoing();
    // initiatiate each msg_interf, and registering messages as required
    static void init();
    // add a new message, only before init
    static void addMessage(const char* mask, const char* header, unsigned short hdrlen,
                           const char* trailer, unsigned short trllen,
                           unsigned short lenoff, short lenwid, short lenbias,
                           unsigned short msgoff, msg_var* var, int interf = 0);
    // add a new interface parameter, only before init
    static void addInterface(const char* host, int port, unsigned int timeout=0, int interf = 0);
    // returns a portion of the buffer suitable for output, sz is size in bytes for the payload
    char* get_buffer(int sz);
    // write a buffer to the msg port, adding the header, trailer and length
    bool write_buffer(char* buf, int sz, int idx);
    // read data from msg port and route it to the correct variable
    bool read_data();

private:
    // locates the msg var to a matching var from the internal buffer, returns offset found
    short (msg_interf_typ::* find_var)(msg_var** varptr, char* buffer, unsigned short bufferlen);
    short find_var_1(msg_var** varptr, char* buffer, unsigned short bufferlen);
    short find_var_2(msg_var** varptr, char* buffer, unsigned short bufferlen);
    short find_var_3(msg_var** varptr, char* buffer, unsigned short bufferlen);
    short find_var_4(msg_var** varptr, char* buffer, unsigned short bufferlen);
    short find_var_5(msg_var** varptr, char* buffer, unsigned short bufferlen);
    short find_var_3_helper(msg_var** varptr, char* buffer);
    short find_var_5_helper(msg_var** varptr, char* buffer);

};

#endif /* _MSG_INTERF_H_ */

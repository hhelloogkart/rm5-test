#ifndef _BUS1553_INTF_L13_H_
#define _BUS1553_INTF_L13_H_

#include "bus1553_intf_ballard.h"

struct Bus1553L13IntfPrivate;

class Bus1553L13Intf : public Bus1553Intf
{
public:
    Bus1553L13Intf();
    virtual ~Bus1553L13Intf();

    virtual bool MonOpen(const char* host, int port);
    virtual bool RTOpen(const char* host, int port);
    virtual bool BCOpen(const char* host, int port);
    virtual void Close();

    virtual unsigned int MonRead(unsigned short* buf);
    virtual unsigned int MonRead(unsigned short* buf, unsigned int nbuf);
    virtual unsigned int MsgRead(unsigned short* buf);
    virtual bool MsgRead(unsigned short* buf, unsigned short cwd);
    virtual bool MsgWrite(const unsigned short* buf, unsigned short cwd);

protected:
    bool L13Open(int port);

    Bus1553L13IntfPrivate* prvt;
};

#endif

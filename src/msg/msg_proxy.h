/******************************************************************/
/* Copyright DSO National Laboratories 2010. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _MSG_PROXY_H_
#define _MSG_PROXY_H_

#include <comm/dlesercore.h>
#include <comm/udpcore.h>
#include <rmglobal.h>

class RM_EXPORT msg_proxy
{
public:
    virtual ~msg_proxy();
    virtual int  send(const char* msg, size_t len) = 0;
    virtual bool recv(char** buf, size_t& len) = 0;
    static msg_proxy* sethost(const char* host, int port);
protected:
    virtual void internal_set_host(const char* host, int port) = 0;
};

#ifndef NO_MSG_PROTOCOL
class msg_proxy_msg : public msg_proxy
{
public:
    int  send(const char* msg, size_t len);
    bool recv(char** buf, size_t& len);
protected:
    void internal_set_host(const char* host, int port);
private:
    msg_core sock;
};
#endif

#ifndef NO_DLE_PROTOCOL
class msg_proxy_dle : public msg_proxy
{
public:
    int  send(const char* msg, size_t len);
    bool recv(char** buf, size_t& len);
protected:
    void internal_set_host(const char* host, int port);
private:
    dle_serial_core sock;
    char rcvbuf[DLEBUF];
};
#endif

#ifndef NO_SERIAL_PROTOCOL
class msg_proxy_ser : public msg_proxy
{
public:
    int  send(const char* msg, size_t len);
    bool recv(char** buf, size_t& len);
protected:
    void internal_set_host(const char* host, int port);
private:
    serial_core sock;
    char rcvbuf[DLEBUF];
};
#endif

#ifndef NO_UDP_PROTOCOL
#define UDPBUF 8192
class msg_proxy_udp : public msg_proxy
{
public:
    int  send(const char* msg, size_t len);
    bool recv(char** buf, size_t& len);
protected:
    void internal_set_host(const char* host, int port);
private:
    udp_core sock;
    char rcvbuf[UDPBUF];
};
#endif

#ifndef NO_B1553_PROTOCOL
#define B1553BUF (33)
class Bus1553Intf;
class msg_proxy_b1553 : public msg_proxy
{
public:
    msg_proxy_b1553();
    virtual ~msg_proxy_b1553();
    int  send(const char* msg, size_t len);
    bool recv(char** buf, size_t& len);
protected:
    void internal_set_host(const char* host, int port);
    bool connected();
private:
    Bus1553Intf* sock;
    unsigned short rcvbuf[B1553BUF];
};
#endif
#endif /* _MSG_PROXY_H_ */

/******************************************************************/
/* Copyright DSO National Laboratories 2016. All Rights Reserved. */
/*                                                                */
/* This file may not be used without permission from DSO.         */
/******************************************************************/

#ifndef _BUS1553_INTF_H_
#define _BUS1553_INTF_H_

#include <stddef.h>

class Bus1553Intf
{
public:
    static Bus1553Intf* Open(const char* host, int port);
    virtual ~Bus1553Intf();
    virtual void Close() = 0;

    unsigned int Recv(unsigned short* buf, size_t len);
    unsigned int Send(const unsigned short* buf, size_t len);

    virtual unsigned int MonRead(unsigned short* buf) = 0;
    virtual unsigned int MonRead(unsigned short* buf, unsigned int nbuf) = 0;
    virtual unsigned int MsgRead(unsigned short* buf) = 0;
    virtual bool MsgRead(unsigned short* buf, unsigned short cwd) = 0;
    virtual bool MsgWrite(const unsigned short* buf, unsigned short cwd) = 0;

    static unsigned short PackCWD(unsigned int ta, unsigned int tr, unsigned int sa, unsigned int wc);
    static void UnpackCWD(unsigned short cwd, unsigned int* ta, unsigned int* tr, unsigned int* sa, unsigned int* wc);

protected:
    virtual bool MonOpen(const char* host, int port) = 0;
    virtual bool RTOpen(const char* host, int port) = 0;
    virtual bool BCOpen(const char* host, int port) = 0;

    enum bus_mode
    {
        MODE_BC, MODE_RT, MODE_MON
    } mode;
};

#endif /* _BUS1553_INTF_H_ */

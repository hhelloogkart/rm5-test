/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/* HEADER FILES TO BE INCLUDED	*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#include "rmc.h"
#include "rmcdata.h"
#include <string.h>
#include <math.h>
#include <stdlib.h>

#ifndef WIN32
#define int64 long long
#define uint64 unsigned long long
#else
#define int64 __int64
#define uint64 unsigned __int64
#endif

#define DBL_EPSILON 0.000001L

union data_union_typ
{
    char i8[8];
    unsigned char u8[8];
    short int i16[4];
    unsigned short u16[4];
    int i32[2];
    unsigned int u32[2];
    int64 i64;
    uint64 u64;
    double d;
};

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/* GENERIC DATA EXTRACTING AND PACKING	*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
unsigned int generic_extract_pack(void* dest, int destsize, const void* buf, int* len)
{
    if (*len >= destsize)           // Checks if it is possible to extract the required size (ie. num) given that *len is the remaining size
    {   // Remaining input size is the same or bigger than the size to be output
        memcpy(dest, buf, destsize);
    }
    else
    {   // Remaining input size is smaller than the size to be output -> ERROR
        unsigned int ret = (unsigned int)*len;
        *len = 0;
        return ret;                 // Tentative return value
    }

    *len -= destsize;
    return (unsigned int)destsize;  // This will shift the input pointer to the 1st non-extracted data.
}

unsigned int generic_extract_pack_ex(void* dest, int destsize, const void* buf, int* len, int destincr, int srcincr)
{
    int srccnt = *len/srcincr;
    int dstcnt = destsize/destincr;

    int mincnt = (srccnt > dstcnt) ? dstcnt : srccnt;
    int minincr = (srcincr > destincr) ? destincr : srcincr;
    for (int cnt = 0; cnt < mincnt; ++cnt)
    {
        memcpy(dest, buf, minincr);
        buf = (void*)(((char*)buf) + srcincr);
        dest = (void*)(((char*)dest) + destincr);
    }
    *len -= mincnt * srcincr;
    return mincnt * srcincr;
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/* GENERIC REVERSE DATA EXTRACTING AND PACKING	*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
template<class src_data_typ, class dest_data_typ>
unsigned int generic_r_extract_pack(dest_data_typ* dest, int num, const src_data_typ* src, int* len)
{   // 'num' refers to the number of units of the corresponding data type
    // It is necessary to know the value of 'num' as for reverse data processing, each data unit
    // needs to be endian-flipped and every data unit needs to be endian-flipped.
    // Endian flipping is dependent on the size of the dest data memory, NOT src data memory
    src_data_typ temp_out[sizeof(src_data_typ)];
    const src_data_typ* temp_in = 0;

    if (*len >= (int)(num * sizeof(*dest)))         // Checks if it is possible to extract the required size (ie. num) given that *len is the remaining size
    {   // Remaining input size is the same or bigger than the size to be output
        // num refers to the number of units of data type which needs to be extracted
        for (int i = 0; i < num; i++)
        {
            /*==============================================================================*/
            /* Endian flipping implementation for ONE data type unit						*/
            /*			+---+---+---+---+								 +---+---+---+---+	*/
            /*	temp_in	| A | B | C | D |	-- Endian Flip -->	temp_out | D | C | B | A |	*/
            /*			+---+---+---+---+								 +---+---+---+---+	*/
            /*==============================================================================*/
            temp_in = src;
            for (int j = 0; j < (int)sizeof(*dest); j++)
            {
                if (j)
                {
                    temp_in++;
                }
                memcpy(&temp_out[(sizeof(*dest) - 1) - j], temp_in, 1);     // Endian flipping 1 BYTE
            }
            memcpy(dest, &temp_out[0], sizeof(*dest));  // Copies ONE data unit to the destination memory
            temp_in     = 0;
            dest++;                                     // Moves to the next data unit
            src += sizeof(*dest);                       // Moves to the next location in the destination memory
        }
    }
    else
    {   // Remaining input size is smaller than the size to be output -> ERROR
        unsigned int ret = (unsigned int)*len;
        *len = 0;
        return ret;                                 // Tentative return value
    }

    *len -= num * sizeof(*dest);
    return (unsigned int)num * sizeof(*dest);       // This will shift the input pointer to the 1st non-extracted data.
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/* THE FOLLOWING FUNCTIONS DEAL WITH DATA EXTRACTION FROM THE PACKETS	*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
unsigned int char_extract(char* dest, int num, const unsigned char* buf, int* len)
{
    return generic_extract_pack(dest, num*sizeof(char), buf, len);
}

unsigned int unsigned_char_extract(unsigned char* dest, int num, const unsigned char* buf, int* len)
{
    return generic_extract_pack(dest, num*sizeof(unsigned char), buf, len);
}

unsigned int short_extract(short* dest, int num, const unsigned char* buf, int* len)
{
    return generic_extract_pack(dest, num*sizeof(short), buf, len);
}

unsigned int unsigned_short_extract(unsigned short* dest, int num, const unsigned char* buf, int* len)
{
    return generic_extract_pack(dest, num*sizeof(unsigned short), buf, len);
}

unsigned int int_extract(int* dest, int num, const unsigned char* buf, int* len)
{
    return generic_extract_pack(dest, num*sizeof(int), buf, len);
}

unsigned int unsigned_int_extract(unsigned int* dest, int num, const unsigned char* buf, int* len)
{
    return generic_extract_pack(dest, num*sizeof(unsigned int), buf, len);
}

unsigned int long_extract(long* dest, int num, const unsigned char* buf, int* len)
{
    return generic_extract_pack(dest, num*sizeof(long), buf, len);
}

unsigned int unsigned_long_extract(unsigned long* dest, int num, const unsigned char* buf, int* len)
{
    return generic_extract_pack(dest, num*sizeof(unsigned long), buf, len);
}

unsigned int long_long_extract(long long* dest, int num, const unsigned char* buf, int* len)
{
    return generic_extract_pack(dest, num*sizeof(long long), buf, len);
}

unsigned int unsigned_long_long_extract(unsigned long long* dest, int num, const unsigned char* buf, int* len)
{
    return generic_extract_pack(dest, num*sizeof(unsigned long long), buf, len);
}

unsigned int float_extract(float* dest, int num, const unsigned char* buf, int* len)
{
    return generic_extract_pack(dest, num*sizeof(float), buf, len);
}

unsigned int double_extract(double* dest, int num, const unsigned char* buf, int* len)
{
    return generic_extract_pack(dest, num*sizeof(double), buf, len);
}

unsigned int decode_extract(double* dest, int num, int byte_size, double range, double offset, int scale_offset, const unsigned char* buf, int* len)
{
    double scaling = 1.0;
    int data_size = (byte_size < 0)? -byte_size : byte_size;
    int bytes_used = data_size * num;
    if (bytes_used > *len)
    {
        *len = 0;
        return 0;
    }

    if(range < 0.0 - DBL_EPSILON || range > 0.0 + DBL_EPSILON)
    {
        scaling = range/(pow(2.0, abs(byte_size)*8) - scale_offset);
    }

    for (int cnt=0; cnt<num; ++cnt, ++dest, buf+=data_size)
    {
        data_union_typ u;
        memcpy(&u, buf, data_size);
        switch (byte_size)
        {
        case -1:
            *dest = u.i8[0] * scaling + offset;
            break;
        case 1:
            *dest = u.u8[0] * scaling + offset;
            break;
        case -2:
            *dest = u.i16[0] * scaling + offset;
            break;
        case 2:
            *dest = u.u16[0] * scaling + offset;
            break;
        case -4:
            *dest = u.i32[0] * scaling + offset;
            break;
        case 4:
            *dest = u.u32[0] * scaling + offset;
            break;
        case -8:
            *dest = (double)u.i64 * scaling + offset;
            break;
        case 8:
            *dest = (double)u.u64 * scaling + offset;
            break;
        case 0:
            *dest = 0; //invalid
            break;
        default:
            if (data_size > 4)
            {
                unsigned int64 mask = (1ULL << (data_size * 8)) - 1ULL;
                u.u64 &= mask;
                if (byte_size < 0)
                {
                    if (u.u64 & (1ULL << (data_size * 8 - 1))) //signed
                    {
                        u.u64 |= ~mask; //sign extend to 64 bits
                    }
                    *dest = u.i64 * scaling + offset;
                }
                else
                {
                    *dest = u.u64 * scaling + offset;
                }
            }
            else
            {
                unsigned int mask = (1U << (data_size * 8)) - 1U;
                u.u32[0] &= mask;
                if (byte_size < 0)
                {
                    if (u.u32[0] & (1U << (data_size * 8 - 1))) //signed
                    {
                        u.u32[0] |= ~mask; //sign extend to 32 bits
                    }
                    *dest = u.i32[0] * scaling + offset;
                }
                else
                {
                    *dest = u.u32[0] * scaling + offset;
                }
            }
            break;
        }
    }
    *len -= bytes_used;
    return bytes_used;
}

unsigned int string_extract(char* dest, int num, int length, const unsigned char* buf, int* len)
{   // length refers to the number of string var
    // len refers to the size of the remaining unextracted data.
    // num refers to the number of target data type units to be extracted.
    int destsize = num * sizeof(*dest) * length;

    if (*len >= destsize)           // Checks if it is possible to extract the required size (ie. num) given that *len is the remaining size
    {   // Remaining input size is the same or bigger than the size to be output
        memcpy(dest, buf, destsize);
    }
    else
    {   // Remaining input size is smaller than the size to be output -> ERROR
        unsigned int ret = (unsigned int)*len;
        *len = 0;
        return ret;                 // Tentative return value
    }

    *len -= destsize;
    return (unsigned int)destsize;  // This will shift the input pointer to the 1st non-extracted data.
}

unsigned int ttag_extract(int* dest, int num, const unsigned char* buf, int* len)
{
    return generic_extract_pack_ex(dest, num * sizeof(*dest), buf, len, sizeof(*dest), 3);
}

inline unsigned char fetch_bit(int bit_offset, const unsigned char* buf)
{
    const int my_byte_offset = bit_offset / 8;
    const int my_bit_offset = bit_offset % 8;
    unsigned char c = (*(buf+my_byte_offset) >> my_bit_offset) & 1;
    return c;
}

unsigned int bit_extract(int* dest, int num, int length, int offset, const unsigned char* buf, int* len)
{
    int bytes_used = offset+(length*num);
    if (bytes_used % 8 == 0)
    {
        bytes_used /= 8;
    }
    else
    {
        bytes_used = (bytes_used / 8) + 1;
    }
    if (offset != 0)
    {
        --buf; // use the previous byte
        --bytes_used;
    }
    if (bytes_used <= *len)
    {
        for (int cnt=0; cnt < num; ++cnt, ++dest)
        {
            int result = 0;
            int offsetnegator = 0;
            for (int tmplen = length;;)
            {
                if (fetch_bit(offset-offsetnegator+tmplen-1, buf) != 0)
                {
                    ++result;
                }
                ++offset;
                ++offsetnegator; // keeps the offset at the same place
                if (--tmplen <= 0)
                {
                    break;
                }
                result <<= 1;
            }
            *dest = result;
        }

        *len -= bytes_used;
        return bytes_used;
    }
    else
    {
        // error! not enough bytes left
        *len = 0;
        return 0;
    }
}

unsigned int decode_bit_extract(double* dest, int num, int bit_size, int bit_offset, double range, double offset, int scale_offset, const unsigned char* buf, int* len)
{
    double scaling = 1.0;
    int* data = new int[num];
    int* tdata = data;
    int byte_used = bit_extract(data, num, bit_size, bit_offset, buf, len);
    if(range < 0.0 - DBL_EPSILON || range > 0.0 + DBL_EPSILON)
    {
        scaling = range/(pow(2.0, abs(bit_size)) - scale_offset);
    }
    for (int cnt=0; cnt<num; ++cnt, ++tdata, ++dest)
    {
        *dest = *tdata * scaling + offset;
    }
    delete[] data;
    return byte_used;
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/* REVERSE DATA EXTRACTION	*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~*/
unsigned int char_r_extract(char* dest, int num, const unsigned char* buf, int* len)
{
    return generic_r_extract_pack(dest, num*sizeof(char), buf, len);
}

unsigned int unsigned_char_r_extract(unsigned char* dest, int num, const unsigned char* buf, int* len)
{
    return generic_r_extract_pack(dest, num*sizeof(unsigned char), buf, len);
}

unsigned int short_r_extract(short* dest, int num, const unsigned char* buf, int* len)
{
    return generic_r_extract_pack(dest, num*sizeof(short), buf, len);
}

unsigned int unsigned_short_r_extract(unsigned short* dest, int num, const unsigned char* buf, int* len)
{
    return generic_r_extract_pack(dest, num*sizeof(unsigned short), buf, len);
}

unsigned int int_r_extract(int* dest, int num, const unsigned char* buf, int* len)
{
    return generic_r_extract_pack(dest, num*sizeof(int), buf, len);
}

unsigned int unsigned_int_r_extract(unsigned int* dest, int num, const unsigned char* buf, int* len)
{
    return generic_r_extract_pack(dest, num*sizeof(unsigned int), buf, len);
}

unsigned int long_r_extract(long* dest, int num, const unsigned char* buf, int* len)
{
    return generic_r_extract_pack(dest, num*sizeof(long), buf, len);
}

unsigned int unsigned_long_r_extract(unsigned long* dest, int num, const unsigned char* buf, int* len)
{
    return generic_r_extract_pack(dest, num*sizeof(unsigned long), buf, len);
}

unsigned int long_long_r_extract(long long* dest, int num, const unsigned char* buf, int* len)
{
    return generic_r_extract_pack(dest, num*sizeof(long long), buf, len);
}

unsigned int unsigned_long_long_r_extract(unsigned long long* dest, int num, const unsigned char* buf, int* len)
{
    return generic_r_extract_pack(dest, num*sizeof(unsigned long long), buf, len);
}

unsigned int float_r_extract(float* dest, int num, const unsigned char* buf, int* len)
{
    return generic_r_extract_pack(dest, num*sizeof(float), buf, len);
}

unsigned int double_r_extract(double* dest, int num, const unsigned char* buf, int* len)
{
    return generic_r_extract_pack(dest, num*sizeof(double), buf, len);
}

inline void rmemcpy(void* dest, const void* src, int count)
{
    char* dest1 = static_cast<char*>(dest);
    const char* src1 = static_cast<const char*>(src) + count - 1;

    for (; count > 0; --count, ++dest1, --src1)
    {
        *dest1 = *src1;
    }
}

unsigned int decode_r_extract(double* dest, int num, int byte_size, double range, double offset, int scale_offset, const unsigned char* buf, int* len)
{
    double scaling = 1.0;
    int data_size = (byte_size < 0)? -byte_size : byte_size;
    int bytes_used = data_size * num;
    if (bytes_used > *len)
    {
        *len = 0;
        return 0;
    }

    if(range < 0.0 - DBL_EPSILON || range > 0.0 + DBL_EPSILON)
    {
        scaling = range/(pow(2.0, abs(byte_size)*8) - scale_offset);
    }

    for (int cnt=0; cnt<num; ++cnt, ++dest, buf+=data_size)
    {
        data_union_typ u;
        rmemcpy(&u, buf, data_size);
        switch (byte_size)
        {
        case -1:
            *dest = u.i8[0] * scaling + offset;
            break;
        case 1:
            *dest = u.u8[0] * scaling + offset;
            break;
        case -2:
            *dest = u.i16[0] * scaling + offset;
            break;
        case 2:
            *dest = u.u16[0] * scaling + offset;
            break;
        case -4:
            *dest = u.i32[0] * scaling + offset;
            break;
        case 4:
            *dest = u.u32[0] * scaling + offset;
            break;
        case -8:
            *dest = (double)u.i64 * scaling + offset;
            break;
        case 8:
            *dest = (double)u.u64 * scaling + offset;
            break;
        case 0:
            *dest = 0; //invalid
            break;
        default:
            if (data_size > 4)
            {
                unsigned int64 mask = (1ULL << (data_size * 8)) - 1ULL;
                u.u64 &= mask;
                if (byte_size < 0)
                {
                    if (u.u64 & (1ULL << (data_size * 8 - 1))) //signed
                    {
                        u.u64 |= ~mask; //sign extend to 64 bits
                    }
                    *dest = u.i64 * scaling + offset;
                }
                else
                {
                    *dest = u.u64 * scaling + offset;
                }
            }
            else
            {
                unsigned int mask = (1U << (data_size * 8)) - 1U;
                u.u32[0] &= mask;
                if (byte_size < 0)
                {
                    if (u.u32[0] & (1U << (data_size * 8 - 1))) //signed
                    {
                        u.u32[0] |= ~mask; //sign extend to 32 bits
                    }
                    *dest = u.i32[0] * scaling + offset;
                }
                else
                {
                    *dest = u.u32[0] * scaling + offset;
                }
            }
            break;
        }
    }
    *len -= bytes_used;
    return bytes_used;
}

unsigned int bit_r_extract(int* dest, int num, int length, int offset, const unsigned char* buf, int* len)
{
    int bytes_used = offset+(length*num);
    if (bytes_used % 8 == 0)
    {
        bytes_used /= 8;
    }
    else
    {
        bytes_used = (bytes_used / 8) + 1;
    }
    if (offset != 0)
    {
        --buf; // use the previous byte
        --bytes_used;
    }
    if (bytes_used <= *len)
    {
        for (int cnt=0; cnt < num; ++cnt, ++dest)
        {
            int result = 0;
            for (int tmplen = length;;)
            {
                if (fetch_bit(offset, buf) != 0)
                {
                    ++result;
                }
                ++offset;
                if (--tmplen <= 0)
                {
                    break;
                }
                result <<= 1;
            }
            *dest = result;
        }

        *len -= bytes_used;
        return bytes_used;
    }
    else
    {
        // error! not enough bytes left
        *len = 0;
        return 0;
    }
}

unsigned int decode_bit_r_extract(double* dest, int num, int bit_size, int bit_offset, double range, double offset, int scale_offset, const unsigned char* buf, int* len)
{
    double scaling = 1.0;
    int* data = new int[num];
    int* tdata = data;
    int byte_used = bit_r_extract(data, num, bit_size, bit_offset, buf, len);
    if(range < 0.0 - DBL_EPSILON || range > 0.0 + DBL_EPSILON)
    {
        scaling = range/(pow(2.0, abs(bit_size)) - scale_offset);
    }
    for (int cnt=0; cnt<num; ++cnt, ++tdata, ++dest)
    {
        *dest = *tdata * scaling + offset;
    }
    delete[] data;
    return byte_used;
}

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/* THE FOLLOWING FUNCTIONS DEAL WITH DATA PACKING INTO THE PACKETS	*/
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

unsigned int char_pack(char* dest, int num, unsigned char* buf, int* len)
{
    return generic_extract_pack(buf, num*sizeof(char), dest, len);
}

unsigned int unsigned_char_pack(unsigned char* dest, int num, unsigned char* buf, int* len)
{
    return generic_extract_pack(buf, num*sizeof(unsigned char), dest, len);
}

unsigned int short_pack(short* dest, int num, unsigned char* buf, int* len)
{
    return generic_extract_pack(buf, num*sizeof(short), dest, len);
}

unsigned int unsigned_short_pack(unsigned short* dest, int num, unsigned char* buf, int* len)
{
    return generic_extract_pack(buf, num*sizeof(unsigned short), dest, len);
}

unsigned int int_pack(int* dest, int num, unsigned char* buf, int* len)
{
    return generic_extract_pack(buf, num*sizeof(int), dest, len);
}

unsigned int unsigned_int_pack(unsigned int* dest, int num, unsigned char* buf, int* len)
{
    return generic_extract_pack(buf, num*sizeof(unsigned int), dest, len);
}

unsigned int long_pack(long* dest, int num, unsigned char* buf, int* len)
{
    return generic_extract_pack(buf, num*sizeof(long), dest, len);
}

unsigned int unsigned_long_pack(unsigned long* dest, int num, unsigned char* buf, int* len)
{
    return generic_extract_pack(buf, num*sizeof(unsigned long), dest, len);
}

unsigned int long_long_pack(long long* dest, int num, unsigned char* buf, int* len)
{
    return generic_extract_pack(buf, num*sizeof(long long), dest, len);
}

unsigned int unsigned_long_long_pack(unsigned long long* dest, int num, unsigned char* buf, int* len)
{
    return generic_extract_pack(buf, num*sizeof(unsigned long long), dest, len);
}

unsigned int float_pack(float* dest, int num, unsigned char* buf, int* len)
{
    return generic_extract_pack(buf, num*sizeof(float), dest, len);
}

unsigned int double_pack(double* dest, int num, unsigned char* buf, int* len)
{
    return generic_extract_pack(buf, num*sizeof(double), dest, len);
}

template<typename C> C rm_round(double dval)
{
    if (dval > 0)
    {
        return static_cast<C>(dval + .5);
    }
    else
    {
        return static_cast<C>(dval - .5);
    }
}

unsigned int decode_pack(double* dest, int num, int byte_size, double range, double offset, int scale_offset, unsigned char* buf, int* len)
{
    double scaling = 1.0;
    int data_size = (byte_size < 0)? -byte_size : byte_size;
    int bytes_used = data_size * num;
    if (bytes_used > *len)
    {
        return 0;
    }

    if(range < 0.0 - DBL_EPSILON || range > 0.0 + DBL_EPSILON)
    {
        scaling = range/(pow(2.0, abs(byte_size)*8) - scale_offset);
    }

    for (int cnt=0; cnt<num; ++cnt, ++dest, buf+=data_size)
    {
        data_union_typ u;
        switch (byte_size)
        {
        case -1:
            u.i8[0] = rm_round<signed char>((*dest - offset) / scaling);
            break;
        case 1:
            u.u8[0] = rm_round<unsigned char>((*dest - offset) / scaling);
            break;
        case -2:
            u.i16[0] = rm_round<short>((*dest - offset) / scaling);
            break;
        case 2:
            u.u16[0] = rm_round<unsigned short>((*dest - offset) / scaling);
            break;
        case -4:
            u.i32[0] = rm_round<int>((*dest - offset) / scaling);
            break;
        case 4:
            u.u32[0] = rm_round<unsigned int>((*dest - offset) / scaling);
            break;
        case -8:
            u.i64 = rm_round<int64>((*dest - offset) / scaling);
            break;
        case 8:
            u.u64 = rm_round<uint64>((*dest - offset) / scaling);
            break;
        case 0:
            break;
        default:
            if (data_size > 4)
            {
                if (byte_size < 0)
                {
                    u.i64 = rm_round<int64>((*dest - offset) / scaling);
                }
                else
                {
                    u.u64 = rm_round<uint64>((*dest - offset) / scaling);
                }
#if defined(__BIG_ENDIAN_)
                u.u64 <<= 64-(data_size*8);
#else
                u.u64 >>= 64-(data_size*8);
#endif
            }
            else
            {
                if (byte_size < 0)
                {
                    u.i32[0] = rm_round<int>((*dest - offset) / scaling);
                }
                else
                {
                    u.u32[0] = rm_round<unsigned int>((*dest - offset) / scaling);
                }
#if defined(__BIG_ENDIAN_)
                u.u64 <<= 64-(data_size*8);
#else
                u.u64 >>= 64-(data_size*8);
#endif
            }
            break;
        }
        memcpy(buf, &u, data_size);
    }
    *len -= bytes_used;
    return bytes_used;
}

void put_bit(int bit_offset, unsigned char* buf, bool value)
{
    const int my_byte_offset = bit_offset / 8;
    const int my_bit_offset = bit_offset % 8;

    if (my_bit_offset == 0)
    {
        *(buf+my_byte_offset) = static_cast<unsigned char>(value);
    }
    else if (value)
    {
        const unsigned char c = '\1' << my_bit_offset;
        *(buf+my_byte_offset) |= c;
    }
}

unsigned int bit_pack(int* dest, int num, int length, int offset, unsigned char* buf, int* len)
{
    int bytes_used = offset+(length*num);
    if (bytes_used % 8 == 0)
    {
        bytes_used /= 8;
    }
    else
    {
        bytes_used = (bytes_used / 8) + 1;
    }
    if (offset != 0)
    {
        --buf; // use the previous byte
        --bytes_used;
    }
    if (bytes_used <= *len)
    {
        for (int cnt=0; cnt < num; ++cnt, ++dest)
        {
            for (int tmplen = length; tmplen > 0; --tmplen, ++offset)
            {
                int mask = 1 << (length-tmplen);
                put_bit(offset, buf, (*dest & mask) != 0);
            }
        }

        *len -= bytes_used;
        return bytes_used;
    }
    else
    {
        // error! not enough bytes left
        *len = 0;
        return 0;
    }
}

unsigned int decode_bit_pack(double* dest, int num, int bit_size, int bit_offset, double range, double offset, int scale_offset, unsigned char* buf, int* len)
{
    double scaling = 1.0;
    int* data = new int[num];
    int* tdata = data;
    if(range < 0.0 - DBL_EPSILON || range > 0.0 + DBL_EPSILON)
    {
        scaling = range/(pow(2.0, abs(bit_size)) - scale_offset);
    }
    for (int cnt=0; cnt<num; ++cnt, ++tdata, ++dest)
    {
        *tdata = rm_round<int>((*dest - offset) / scaling);
    }
    int byte_used = bit_pack(data, num, bit_size, bit_offset, buf, len);
    delete[] data;
    return byte_used;
}

unsigned int string_pack(char* dest, int num, int length, unsigned char* buf, int* len)
{   // length refers to the number of string var
    // len refers to the size of the data buffer to send out, equals output message size left.
    // num refers to the number of target data type units to be packed.
    int destsize = num * sizeof(*dest) * length;

    if (*len >= destsize)           // Checks if it is possible to extract the required size (ie. num) given that *len is the remaining size
    {   // Remaining output size is the same or bigger than the size to be output
        memcpy(buf, dest, destsize);
        *len -= destsize;
        return destsize;
    }
    memcpy(buf, dest, *len);
    length = *len;
    *len = 0;
    return (unsigned int)length;    // This will shift the input pointer to the 1st non-extracted data.
}

unsigned int ttag_pack(int* dest, int num, unsigned char* buf, int* len)
{
    return generic_extract_pack_ex(buf, num*3, dest, len, 3, sizeof(*dest));
}

/*~~~~~~~~~~~~~~~~~~~~~~*/
/* REVERSE DATA PACKING	*/
/*~~~~~~~~~~~~~~~~~~~~~~*/
unsigned int char_r_pack(char* dest, int num, unsigned char* buf, int* len)
{
    return generic_r_extract_pack(buf, num*sizeof(char), dest, len);
}

unsigned int unsigned_char_r_pack(unsigned char* dest, int num, unsigned char* buf, int* len)
{
    return generic_r_extract_pack(buf, num*sizeof(unsigned char), dest, len);
}

unsigned int short_r_pack(short* dest, int num, unsigned char* buf, int* len)
{
    return generic_r_extract_pack(buf, num*sizeof(short), dest, len);
}

unsigned int unsigned_short_r_pack(unsigned short* dest, int num, unsigned char* buf, int* len)
{
    return generic_r_extract_pack(buf, num*sizeof(unsigned short), dest, len);
}

unsigned int int_r_pack(int* dest, int num, unsigned char* buf, int* len)
{
    return generic_r_extract_pack(buf, num*sizeof(int), dest, len);
}

unsigned int unsigned_int_r_pack(unsigned int* dest, int num, unsigned char* buf, int* len)
{
    return generic_r_extract_pack(buf, num*sizeof(unsigned int), dest, len);
}

unsigned int long_r_pack(long* dest, int num, unsigned char* buf, int* len)
{
    return generic_r_extract_pack(buf, num*sizeof(long), dest, len);
}

unsigned int unsigned_long_r_pack(unsigned long* dest, int num, unsigned char* buf, int* len)
{
    return generic_r_extract_pack(buf, num*sizeof(unsigned long), dest, len);
}

unsigned int long_long_r_pack(long long* dest, int num, unsigned char* buf, int* len)
{
    return generic_r_extract_pack(buf, num*sizeof(long long), dest, len);
}

unsigned int unsigned_long_long_r_pack(unsigned long long* dest, int num, unsigned char* buf, int* len)
{
    return generic_r_extract_pack(buf, num*sizeof(unsigned long long), dest, len);
}

unsigned int float_r_pack(float* dest, int num, unsigned char* buf, int* len)
{
    return generic_r_extract_pack(buf, num*sizeof(float), dest, len);
}

unsigned int double_r_pack(double* dest, int num, unsigned char* buf, int* len)
{
    return generic_r_extract_pack(buf, num*sizeof(double), dest, len);
}

unsigned int decode_r_pack(double* dest, int num, int byte_size, double range, double offset, int scale_offset, unsigned char* buf, int* len)
{
    double scaling = 1.0;
    int data_size = (byte_size < 0)? -byte_size : byte_size;
    int bytes_used = data_size * num;
    if (bytes_used > *len)
    {
        return 0;
    }

    if(range < 0.0 - DBL_EPSILON || range > 0.0 + DBL_EPSILON)
    {
        scaling = range/(pow(2.0, abs(byte_size)*8) - scale_offset);
    }

    for (int cnt=0; cnt<num; ++cnt, ++dest, buf+=data_size)
    {
        data_union_typ u;
        switch (byte_size)
        {
        case -1:
            u.i8[0] = rm_round<signed char>((*dest - offset) / scaling);
            break;
        case 1:
            u.u8[0] = rm_round<unsigned char>((*dest - offset) / scaling);
            break;
        case -2:
            u.i16[0] = rm_round<short>((*dest - offset) / scaling);
            break;
        case 2:
            u.u16[0] = rm_round<unsigned short>((*dest - offset) / scaling);
            break;
        case -4:
            u.i32[0] = rm_round<int>((*dest - offset) / scaling);
            break;
        case 4:
            u.u32[0] = rm_round<unsigned int>((*dest - offset) / scaling);
            break;
        case -8:
            u.i64 = rm_round<int64>((*dest - offset) / scaling);
            break;
        case 8:
            u.u64 = rm_round<uint64>((*dest - offset) / scaling);
            break;
        case 0:
            break;
        default:
            if (data_size > 4)
            {
                if (byte_size < 0)
                {
                    u.i64 = rm_round<int64>((*dest - offset) / scaling);
                }
                else
                {
                    u.u64 = rm_round<uint64>((*dest - offset) / scaling);
                }
#if defined(__BIG_ENDIAN_)
                u.u64 <<= 64-(data_size*8);
#else
                u.u64 >>= 64-(data_size*8);
#endif
            }
            else
            {
                if (byte_size < 0)
                {
                    u.i32[0] = rm_round<int>((*dest - offset) / scaling);
                }
                else
                {
                    u.u32[0] = rm_round<unsigned int>((*dest - offset) / scaling);
                }
#if defined(__BIG_ENDIAN_)
                u.u64 <<= 64-(data_size*8);
#else
                u.u64 >>= 64-(data_size*8);
#endif
            }
            break;
        }
        rmemcpy(buf, &u, data_size);
    }
    *len -= bytes_used;
    return bytes_used;
}

unsigned int bit_r_pack(int* dest, int num, int length, int offset, unsigned char* buf, int* len)
{
    int bytes_used = offset+(length*num);
    if (bytes_used % 8 == 0)
    {
        bytes_used /= 8;
    }
    else
    {
        bytes_used = (bytes_used / 8) + 1;
    }
    if (offset != 0)
    {
        --buf; // use the previous byte
        --bytes_used;
    }
    if (bytes_used <= *len)
    {
        for (int cnt=0; cnt < num; ++cnt, ++dest)
        {
            int offsetnegator = 0; // keeps the offset at the same place
            for (int tmplen = length; tmplen > 0; --tmplen, ++offset, ++offsetnegator)
            {
                int mask = 1 << (tmplen-1);
                put_bit(offset, buf, (*dest & mask) != 0);
            }
        }

        *len -= bytes_used;
        return bytes_used;
    }
    else
    {
        // error! not enough bytes left
        *len = 0;
        return 0;
    }
}

unsigned int decode_bit_r_pack(double* dest, int num, int bit_size, int bit_offset, double range, double offset, int scale_offset, unsigned char* buf, int* len)
{
    double scaling = 1.0;
    int* data = new int[num];
    int* tdata = data;
    if(range < 0.0 - DBL_EPSILON || range > 0.0 + DBL_EPSILON)
    {
        scaling = range/(pow(2.0, abs(bit_size)) - scale_offset);
    }
    for (int cnt=0; cnt<num; ++cnt, ++tdata, ++dest)
    {
        *tdata = rm_round<int>((*dest - offset) / scaling);
    }
    int byte_used = bit_r_pack(data, num, bit_size, bit_offset, buf, len);
    delete[] data;
    return byte_used;
}

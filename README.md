# Building RM

RM is a cross-platform library and uses Qt qmake system to facilitate build in different environments. Currently the following operating systems are avialable:

- Windows (Visual Studio 2003-2017)
- Linux (x86, x86_64, ARM, ARM64)
- GreenHills Integrity
- QNX
- Android
- Vxworks
- NetOS

RM has no dependency on Qt (other than needing qmake, any version from Qt4-5 will work). You should have qmake on the path with the QMAKESPEC properly defined to your target environment. Your compilation environment should be also properly set up.

RM depends on _oscore_. You need to have oscore as a sibling sub-directory to the rm directory and build it first.

In Windows, you type:

    nmake win

In all other platforms, you type:

    make

Alternatively, you can choose to build individual components separately. In Visual Studio, you can install the Qt plugin/addon to open the Qt project (pro) files and build what you need. The main part of RM is:

- rm5lib.pro - RM library itself

If you are writing an application that is not C++ that needs to communicate to RM middleware:

- rm5labview.pro - C interface that can be used from Labview
- rmc.pro - C interface for RM middleware, suitable for Lab Windows

If you are using RM as a middleware, you may wish to build the following components:


- rm5clr.pro - Utility to clear RM Shared Memory
- rm5recv.pro - Utility to test RM protocol performance (use with send)
- rm5send.pro - Utility to test RM protocol performance (use with recv)
- rm5ssm.pro - Utility to set up RM Static Shared Memory
- rm5sync.pro - Utility to sync data between two RM
- rm5sync_delay.pro - Utility to sync but with configurable delay for simulation
- rmtcp.pro - TCP server for RM-TCP protocol (do not use in Windows)
- rmtcp_static.pro - TCP server with statically linked RM library
- rmtest.pro - Interactive tester to probe the RM. Needs Qt 4 or 5.
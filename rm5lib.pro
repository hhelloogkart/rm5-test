######################################################################
# Automatically generated by qmake (1.02a) Fri Mar 15 12:24:17 2002
######################################################################

TEMPLATE = lib
CONFIG += warn_on dll
CONFIG += no_rmiconn
CONFIG -= qt
TARGET = rm
INCLUDEPATH += include ../oscore/include .
LANGUAGE = C++
!android:OBJECTS_DIR = .objs/rm5lib/release
DESTDIR = $${PWD}/lib
VERSION = 5.0
out_of_source_build{
    ########################################################
    # Specify the out-of-source build directory
    ########################################################
    !defined(BUILD_DIR_BASE, var):error(BUILD_DIR_BASE must be defined!)
    iosl:BUILD_DIR=$$BUILD_DIR_BASE/iosl
    else:win32:contains(QMAKE_TARGET.arch, x86_64):BUILD_DIR=$$BUILD_DIR_BASE/win64
    else:win32:BUILD_DIR=$$BUILD_DIR_BASE/win32
    else:qnx:PARASOFT:omap:BUILD_DIR=$$BUILD_DIR_BASE/qnx/omap/parasoft
    else:qnx:omap:BUILD_DIR=$$BUILD_DIR_BASE/qnx/omap
    else:qnx:imx:BUILD_DIR=$$BUILD_DIR_BASE/qnx/imx
    else:qnx:zynq:BUILD_DIR=$$BUILD_DIR_BASE/qnx/zynq
    else:baremetal:zynq:BUILD_DIR=$$BUILD_DIR_BASE/baremetal/zynq
    else:unix:BUILD_DIR=$$BUILD_DIR_BASE/unix
    else:BUILD_DIR=$$BUILD_DIR_BASE/unknown
    ########################################################
    # Specify the out-of-source objects directory
    # Note: This must happen before
    # LIB_SUFFIX is appended to TARGET
    ########################################################
    OBJECTS_DIR=$${BUILD_DIR}/obj/$${TARGET}
    ########################################################
    # Specify the out-of-source objects directory
    # for debug and release
    ########################################################
    CONFIG(debug, debug|release) {
        LIB_SUFFIX=d
        TARGET = $${TARGET}$${LIB_SUFFIX}
        OBJECTS_DIR = $${OBJECTS_DIR}/debug
    }
    CONFIG(release, debug|release) {
        OBJECTS_DIR = $${OBJECTS_DIR}/release
    }
    ########################################################
    # Specify the destination build directory
    ########################################################
    contains(TEMPLATE, vclib)|contains(TEMPLATE, lib){
        DESTDIR = $${BUILD_DIR}/lib
        DLLDESTDIR = $${BUILD_DIR}
    }else:contains(TEMPLATE, vcapp)|contains(TEMPLATE, app){
        DESTDIR = $${BUILD_DIR}
        DLLDESTDIR = $${BUILD_DIR}
    }
    ########################################################
    # Specify the config to build for
    # both debug and release
    ########################################################
    CONFIG +=  warn_on build_all debug_and_release
    ########################################################
    #  Specify LIB dependencies
    ########################################################
    iosl{
        LIBS += $$BUILD_DIR/lib/oscore$${LIB_SUFFIX}1.lib ws2_32.lib
    }else:win32-msvc | win32-msvc2015 | win32-msvc2013{
        LIBS += $$BUILD_DIR/lib/oscore$${LIB_SUFFIX}1.lib ws2_32.lib
        QMAKE_LFLAGS += /SAFESEH:NO
    }else:win32-msvc2008{
        LIBS += $$BUILD_DIR/lib/oscore$${LIB_SUFFIX}1.lib ws2_32.lib
    }else:win32{
        LIBS += $$BUILD_DIR/lib/oscore$${LIB_SUFFIX}1.lib ws2_32.lib
    }else:unix {
        LIBS += -L$$BUILD_DIR/lib -loscore$${LIB_SUFFIX}
    }
    ########################################################
    #  Specify customized options
    ########################################################
    zynq:baremetal{
        CONFIG -= dll shared dylib
        CONFIG += static create_prl
        CONFIG += no_rm no_thread no_rtti no_lzip_var
        CONFIG += no_fec_var no_state no_proto
        CONFIG += no_dout
    }else:qnx{
        CONFIG -= dll shared dylib
        CONFIG += static create_prl
        CONFIG += no_rm no_thread no_rtti no_lzip_var
        CONFIG += no_fec_var no_state no_proto
    }else{
        CONFIG += dll shared dylib
        CONFIG -= static create_prl
    }
}else{
    CONFIG(debug, debug|release) {
        LIB_SUFFIX=d
        TARGET = $${TARGET}$${LIB_SUFFIX}
        !android:OBJECTS_DIR = .objs/rm5lib/debug
    }
    win32-msvc2008 {
      LIBS += ../oscore/lib/oscore$${LIB_SUFFIX}1.lib ws2_32.lib
      QMAKE_CXXFLAGS *= /Zc:wchar_t
      QMAKE_CXXFLAGS -= -Zc:wchar_t-
    } else:win32-msvc2010 {
      LIBS += ../oscore/lib/oscore$${LIB_SUFFIX}1.lib ws2_32.lib
      QMAKE_CXXFLAGS *= /Zc:wchar_t
      QMAKE_CXXFLAGS -= -Zc:wchar_t-
      QMAKE_LFLAGS += /SAFESEH:NO
    } else:win32-msvc2012 | win32-msvc2013 | win32-msvc2015 | win32-msvc {
      LIBS += ../oscore/lib/oscore$${LIB_SUFFIX}1.lib ws2_32.lib
      QMAKE_LFLAGS += /SAFESEH:NO
    } else:win32 {
      LIBS += ../oscore/lib/oscore$${LIB_SUFFIX}10.lib ws2_32.lib
    } else:android {
      LIBS += -L../oscore/lib -loscore$${LIB_SUFFIX}_$${QT_ARCH}
    } else:unix {
      LIBS += -L../oscore/lib -loscore$${LIB_SUFFIX}
    }
}

!exists($$PWD/include/rm_ver.h){
    win32 | qnx: system($$PWD/makever.bat > $$PWD/include/rm_ver.h)
    else:   system($$PWD/makever.sh > $$PWD/include/rm_ver.h)
}

# Platform Defines
win32:DEFINES += LIBRARY
win32:DEFINES -= _MBCS UNICODE
qnx-*:LIBS += -lsocket
win32-msvc:QMAKE_CFLAGS += /MDd /Zi
win32-msvc:QMAKE_CXXFLAGS += /GX /GR /MDd /Zi
win32-msvc:QMAKE_CXXFLAGS -= /GZ /ZI
win32-g++:DEFINES += STATIC
win32-g++:QMAKE_LFLAGS += -shared
qnx-*:LIBS += -lsocket
netos-*:CONFIG -= dll
netos-*:CONFIG += static
netos-*:INCLUDEPATH += /netos60/h /netos60/h/threadx /netos60/h/tcpip /netos60/src/bsp/h
netos-*:DEFINES += __BIG_ENDIAN_
netos-*:CONFIG += no_rmiconn no_thread no_dout no_fs no_mhash no_chksum_var no_lzip_var no_fec_var no_frame_var
netos-*:CONFIG += no_state no_proto no_sm no_rm3 no_rmtcp no_rmserial no_rmdserial no_ssm no_scramnet no_spread no_rmdudp
linux-g++:DEFINES += __SMALL_ENDIAN_
linux-icc:QMAKE_LFLAGS += -cxxlib-icc
linux-icc:QMAKE_CXXFLAGS += -cxxlib-icc
linux-icc:QMAKE_CFLAGS += -cxxlib-icc
hpux-acc:DEFINES += _HPUX_SOURCE
irix-cc:QMAKE_CXXFLAGS += -LANG:std
tru64-cxx:DEFINES += __USE_STD_IOSTREAM NO_RTTI
tru64-cxx:QMAKE_CXXFLAGS += -nortti
vxworks-*:CONFIG += no_sm
vxworks-ccppc:CONFIG += no_rtti
vxworks-ccppc:DEFINES += NO_MEMBER_TEMPLATE NO_DEF_ALLOCATOR NO_WCHAR
linux-g++:QMAKE_LFLAGS += -Wl,-rpath,$(QTDIR)/lib
macx-*:CONFIG+= no_scramnet
android:CONFIG += no_rmiconn no_scramnet no_ssm no_sm
android:DEFINES += _GLIBCXX_PERMIT_BACKWARD_HASH NO_EXEC
linux-jetson-*:CONFIG += no_rmiconn no_scramnet

# Input
HEADERS	+= include/arry_var.h \
           include/ascii_var.h \
           include/avl_var.h \
           include/bit_var.h \
           include/block_var.h \
           include/cache.h \
           include/cplx_var.h \
           include/cyclshow.h \
           include/decode_bit_var.h \
           include/decode_complement_var.h \
           include/decodeprefix_var.h \
           include/decode_var.h \
           include/delyshow.h \
           include/dumpshow.h \
           include/fcpl_var.h \
           include/flex_var.h \
           include/flun_show.h \
           include/flun_var.h \
           include/gen_var.h \
           include/link_var.h \
           include/mpt_show.h \
           include/mpt_var.h \
           include/muxshow.h \
           include/outbuf.h \
           include/pad_var.h \
           include/rdecode_bit_var.h \
           include/rdecode_var.h \
           include/rflex_var.h \
           include/rgen_var.h \
           include/rm_ver.h \
           include/rmglobal.h \
           include/stlcast.h \
           include/str_var.h \
           include/struct_var.h \
           include/ttag_var.h \
           include/deci_var.h \
           include/union_var.h \
           include/union_ext_sel_var.h \
           include/vlu_var.h \
           src/rmstl.h

SOURCES	+= src/arry_var.cpp \
           src/ascii_var.cpp \
           src/avl_var.cpp \
           src/bit_var.cpp \
           src/block_var.cpp \
           src/cache.cpp \
           src/cplx_var.cpp \
           src/cyclshow.cpp \
           src/decode_bit_var.cpp \
           src/decode_complement_var.cpp \
           src/decodeprefix_var.cpp \
           src/decode_var.cpp \
           src/delyshow.cpp \
           src/dumpshow.cpp \
           src/fcpl_var.cpp \
           src/flun_show.cpp \
           src/flun_var.cpp \
           src/link_var.cpp \
           src/mpt_show.cpp \
           src/mpt_var.cpp \
           src/muxshow.cpp \
           src/outbuf.cpp \
           src/pad_var.cpp \
           src/rdecode_bit_var.cpp \
           src/rdecode_var.cpp \
           src/str_var.cpp \
           src/struct_var.cpp \
           src/ttag_var.cpp \
           src/deci_var.cpp \
           src/union_var.cpp \
           src/union_ext_sel_var.cpp \
           src/vlu_var.cpp

# OPTIONAL COMPONENTS

#DISABLE ALL RM PROTOCOLS
no_rm {
CONFIG *= no_rminterf
CONFIG *= no_sm
CONFIG *= no_rmtcp
CONFIG *= no_rm3
CONFIG *= no_rmudp
CONFIG *= no_rmserial
CONFIG *= no_rmdserial
CONFIG *= no_ssm
CONFIG *= no_scramnet
CONFIG *= no_spread
CONFIG *= no_rmdudp
CONFIG *= no_rmiconn
} else {
HEADERS += include/buf_var.h \
           include/rm_var.h \
           include/rmmsg_var.h \
           include/rmdata_var.h \
           include/val_var.h \
           src/rm/rmsocket.h
SOURCES += src/rm/buf_var.cpp \
           src/rm/rm_var.cpp \
           src/rm/rmmsg_var.cpp \
           src/rm/rmdata_var.cpp \
           src/rm/rmsocket.cpp \
           src/rm/val_var.cpp \
}

#DISABLE MULTIPLE RM INTERFACES
no_rminterf {
DEFINES += NO_RMINTERF
} else {
HEADERS += include/rminterf.h
SOURCES += src/rm/rminterf.cpp
}

#DISABLE SERIAL
!no_serial {
include ($$PWD/src/serial/serialprotocollib.pri)
}

#DISABLE MSG
!no_msg {
CONFIG += no_1553
DEFINES += NO_MSG_PROTOCOL
include ($$PWD/src/msg/msgprotocollib.pri)
}

#DISABLE MULTI THREADING IN LIBRARY
no_thread {
DEFINES += NO_THREAD
}

#DISABLE LOG FILE
no_log {
DEFINES += NO_LOG
} else {
HEADERS += src/logfile.h
SOURCES += src/logfile.cpp
}

#DISABLE THE USE OF PCRE REGULAR EXPRESSION LIBRARY. CERTAIN FEATURES THAT REQUIRE REGULAR EXPRESSIONS WILL FAIL TO WORK
no_pcre {
DEFINES += NO_PCRE
} else {
SOURCES +=  src/defaults.cpp \
            src/defutil.cpp
HEADERS +=  include/defaults.h \
            include/defutil.h
win32:LIBS += user32.lib
}

#DISABLE THE USE OF RTTI
no_rtti {
DEFINES += NO_RTTI
vxworks-*: QMAKE_CXXFLAGS += -fno-rtti
#vxworks-*: QMAKE_CXXFLAGS += -fno-rtti -fno-exception
}

#DISABLE THE USE OF dout. CALLS WILL BE ROUTED TO cout.
no_dout {
DEFINES += NO_DOUT
}

#DISABLE THE USE OF FILESYSTEM. FILE OPERATIONS WILL FAIL
no_fs:DEFINES += NO_FS

!no_mhash {
INCLUDEPATH *= include src/mhash/include
HEADERS += src/mhash/include/mhash.h \
           src/mhash/include/mutils/mhash.h \
           src/mhash/include/mutils/mincludes.h \
           src/mhash/include/mutils/mhash_config.h \
           src/mhash/include/mutils/mutils.h \
           src/mhash/include/mutils/mtypes.h \
           src/mhash/include/mutils/mglobal.h \
           src/mhash/src/mhash_sha256.h \
           src/mhash/src/mhash_adler32.h \
           src/mhash/src/mhash_gost.h \
           src/mhash/src/mhash_md2.h \
           src/mhash/src/mhash_md4.h \
           src/mhash/src/mhash_md5.h \
           src/mhash/src/mhash_ripemd.h \
           src/mhash/src/mhash_sha1.h \
           src/mhash/src/mhash_sha256_sha224.h \
           src/mhash/src/mhash_sha3.h \
           src/mhash/src/mhash_sha512_sha384.h \
           src/mhash/src/mhash_snefru.h \
           src/mhash/src/mhash_tiger.h \
           src/mhash/src/mhash_whirlpool.h \
           src/mhash/src/mhash_haval.h \
           src/mhash/src/mhash_int.h \
           src/mhash/src/mhash_crc16.h \
           include/mhash_var.h
HEADERS *= include/chk_var.h
SOURCES += src/mhash_var.cpp \
           src/mhash/src/sha256.c \
           src/mhash/src/sha256_sha224.c \
           src/mhash/src/sha3.c \
           src/mhash/src/sha512_sha384.c \
           src/mhash/src/sha1.c \
           src/mhash/src/mhash.c \
           src/mhash/src/md5.c \
           src/mhash/src/crc16.c
SOURCES *= src/mhash/src/crc32.c \
           src/mhash/src/stdfns.c \
           src/chk_var.cpp
}

!no_chksum_var {
HEADERS += include/post_chksum_time_var.h
HEADERS += include/chksum_var.h
HEADERS *= include/chk_var.h
SOURCES += src/post_chksum_time_var.cpp
SOURCES += src/chksum_var.cpp
SOURCES *= src/chk_var.cpp
}

!no_counter_var {
HEADERS += include/counter_var.h
HEADERS *= include/chk_var.h
SOURCES += src/counter_var.cpp
SOURCES *= src/chk_var.cpp
}

!no_lzip_var {
INCLUDEPATH += src/lzlib
HEADERS += src/lzlib/decoder.h \
           src/lzlib/encoder.h \
           src/lzlib/lzip.h \
           src/lzlib/lzlib.h
SOURCES += src/lzlib/lzlib.c \
           src/lzlib/decoder.c
HEADERS += include/compress_var.h \
           include/lzip_var.h
SOURCES += src/compress_var.cpp \
           src/lzip_var.cpp
}

!no_fec_var {
INCLUDEPATH += src/fec
HEADERS += src/fec/fec.h \
           include/fec_var.h
SOURCES += src/fec/fec.c \
           src/fec_var.cpp
}

!no_frame_var {
HEADERS += include/frame_var.h
SOURCES += src/frame_var.cpp
}

#PROTOCOL BUFFERS
!no_proto {
win32:DEFINES += WIN32 LIBPROTOBUF_EXPORTS LIBPROTOC_EXPORTS PROTOBUF_USE_DLLS
SOURCES += src/mut_var.cpp \
           src/google/protobuf/descriptor.cc \
           src/google/protobuf/descriptor.pb.cc \
           src/google/protobuf/descriptor_database.cc \
           src/google/protobuf/dynamic_message.cc \
           src/google/protobuf/extension_set.cc \
           src/google/protobuf/extension_set_heavy.cc \
           src/google/protobuf/generated_message_reflection.cc \
           src/google/protobuf/generated_message_util.cc \
           src/google/protobuf/message.cc \
           src/google/protobuf/message_lite.cc \
           src/google/protobuf/reflection_ops.cc \
           src/google/protobuf/repeated_field.cc \
           src/google/protobuf/service.cc \
           src/google/protobuf/text_format.cc \
           src/google/protobuf/unknown_field_set.cc \
           src/google/protobuf/wire_format.cc \
           src/google/protobuf/wire_format_lite.cc \
           src/google/protobuf/compiler/importer.cc \
           src/google/protobuf/compiler/parser.cc \
           src/google/protobuf/stubs/common.cc \
           src/google/protobuf/stubs/once.cc \
           src/google/protobuf/stubs/stringprintf.cc \
           src/google/protobuf/stubs/structurally_valid.cc \
           src/google/protobuf/stubs/strutil.cc \
           src/google/protobuf/stubs/substitute.cc \
           src/google/protobuf/io/coded_stream.cc \
           src/google/protobuf/io/printer.cc \
           src/google/protobuf/io/strtod.cc \
           src/google/protobuf/io/tokenizer.cc \
           src/google/protobuf/io/zero_copy_stream.cc \
           src/google/protobuf/io/zero_copy_stream_impl.cc \
           src/google/protobuf/io/zero_copy_stream_impl_lite.cc
HEADERS += include/mut_var.h \
           include/google/protobuf/descriptor.h \
           include/google/protobuf/descriptor.pb.h \
           include/google/protobuf/extension_set.h \
           include/google/protobuf/generated_enum_reflection.h \
           include/google/protobuf/generated_message_reflection.h \
           include/google/protobuf/generated_message_util.h \
           include/google/protobuf/message.h \
           include/google/protobuf/message_lite.h \
           include/google/protobuf/reflection_ops.h \
           include/google/protobuf/repeated_field.h \
           include/google/protobuf/unknown_field_set.h \
           include/google/protobuf/wire_format.h \
           include/google/protobuf/wire_format_lite.h \
           include/google/protobuf/wire_format_lite_inl.h \
           include/google/protobuf/stubs/atomicops.h \
           include/google/protobuf/stubs/common.h \
           include/google/protobuf/stubs/once.h \
           include/google/protobuf/io/coded_stream.h \
           src/google/protobuf/descriptor_database.h \
           src/google/protobuf/dynamic_message.h \
           src/google/protobuf/service.h \
           src/google/protobuf/text_format.h \
           src/google/protobuf/compiler/importer.h \
           src/google/protobuf/compiler/parser.h \
           src/google/protobuf/stubs/hash.h \
           src/google/protobuf/stubs/map_util.h \
           src/google/protobuf/stubs/stl_util.h \
           src/google/protobuf/stubs/stringprintf.h \
           src/google/protobuf/stubs/strutil.h \
           src/google/protobuf/stubs/substitute.h \
           src/google/protobuf/io/coded_stream_inl.h \
           src/google/protobuf/io/printer.h \
           src/google/protobuf/io/strtod.h \
           src/google/protobuf/io/tokenizer.h \
           src/google/protobuf/io/zero_copy_stream.h \
           src/google/protobuf/io/zero_copy_stream_impl.h \
           src/google/protobuf/io/zero_copy_stream_impl_lite.h
win32:HEADERS += src/google/protobuf/win_config.h
win32:SOURCES += src/google/protobuf/stubs/atomicops_internals_x86_msvc.cc
unix:HEADERS += src/google/protobuf/config.h
unix:!android:SOURCES += src/google/protobuf/stubs/atomicops_internals_x86_gcc.cc
INCLUDEPATH += src
}

#SHARED MEMORY - SHARED MEMORY PROTOCOL
no_sm {
DEFINES += NO_SM
} else {
SOURCES += src/rm/rmsm.cpp \
           src/rm/storage.cpp \
           src/rm/queue.cpp \
           src/rm/varcol.cpp \
           src/rm/varitem.cpp
SOURCES *= src/rm/rmname.cpp \
           src/rm/smcache.cpp
HEADERS += src/rm/rmsm.h \
           src/rm/rel_ptr.h \
           src/rm/storage.h \
           src/rm/directory.h \
           src/rm/queue.h \
           src/rm/varcol.h \
           src/rm/varitem.h
HEADERS *= src/rm/rmname.h \
           src/rm/smcache.h
}

#TCP - TCP PROTOCOL
no_rmtcp {
DEFINES += NO_RMTCP
CONFIG *= no_rm3
} else {
SOURCES += src/rm/rmtcp.cpp \
           src/rm/roamcli.cpp
HEADERS += src/rm/rmtcp.h \
           src/rm/roamcli.h
}

#IF TCP PROTOCOL NOT ENABLED THEN RM3 IS DISABLED
no_rm3 {
DEFINES += NO_RM3
} else {
HEADERS += src/rm/rmoldtcp.h
SOURCES += src/rm/rmoldtcp.cpp
}

#UDP - UDP PROTOCOL
no_rmudp {
DEFINES += NO_RMUDP
} else {
SOURCES += src/rm/rmudp.cpp
HEADERS += src/rm/rmudp.h
}

#SERIAL PROTOCOL
no_rmserial {
DEFINES += NO_RMSERIAL
} else {
INCLUDEPATH *= include src/mhash/include
SOURCES += src/rm/rmserial.cpp
HEADERS += src/rm/rmserial.h
SOURCES *= src/mhash/src/crc32.c \
           src/mhash/src/stdfns.c
}

#DUAL SERIAL PROTOCOL
no_rmdserial {
DEFINES += NO_RMDSERIAL
} else {
INCLUDEPATH *= include src/mhash/include
SOURCES += src/rm/rmdserial.cpp
HEADERS += src/rm/rmdserial.h
SOURCES *= src/mhash/src/crc32.c \
           src/mhash/src/stdfns.c
}

#STATIC SHARED MEMORY POOL PROTOCOL
no_ssm {
DEFINES += NO_SSM
} else {
SOURCES += src/rm/rmstaticsm.cpp
SOURCES *= src/rm/rmname.cpp \
           src/rm/varitem.cpp \
           src/rm/smcache.cpp
HEADERS += src/rm/rmstaticsm.h \
           src/rm/lockfreequeue.h
HEADERS *= src/rm/rmname.h \
           src/rm/varitem.h \
           src/rm/rel_ptr.h \
           src/rm/directory.h \
           src/rm/smcache.h
}

#SCRAMNET PROTOCOL
no_scramnet {
DEFINES += NO_SCRAMNET
} else {
win32:DEFINES *= WIN32
SOURCES += src/rm/rmscram.cpp
SOURCES *= src/rm/rmname.cpp \
           src/rm/varitem.cpp
HEADERS += src/rm/rmscram.h
HEADERS *= src/rm/rmname.h \
           src/rm/varitem.h \
           src/rm/rel_ptr.h \
           src/rm/directory.h \
           src/rm/lockfreequeue.h
win32: {
  contains(QMAKE_TARGET.arch, x86_64) {
    LIBS += src/gt200/scgtapi_64.lib
  } else {
    LIBS += src/gt200/scgtapi.lib
  }
  QMAKE_LFLAGS += /NODEFAULTLIB:libcmt
} else:!qnx-*:unix:{
  contains(QMAKE_TARGET.arch, x86_64) | contains(QMAKE_HOST.arch, x86_64) {
    LIBS += -L../rm5/src/gt200/scgt/linux-2.6/lib -lscgtapi_x86_64
  } else {
    LIBS += -L../rm5/src/gt200/scgt/linux-2.6/lib -lscgtapi
  }
}
}

#SPREAD PROTOCOL
no_spread {
DEFINES += NO_SPREAD
} else {
SOURCES += src/rm/rmspread.cpp \
           src/spread/alarm.c \
           src/spread/arch.c \
           src/spread/events.c \
           src/spread/memory.c \
           src/spread/sp.c
SOURCES *= src/rm/rmname.cpp
HEADERS += src/rm/rmspread.h \
           src/spread/acm.h \
           src/spread/arch.h \
           src/spread/config.h \
           src/spread/defines.h \
           src/spread/errors.h \
           src/spread/mutex.h \
           src/spread/sess_types.h \
           src/spread/sp.h \
           src/spread/spread_params.h \
           src/spread/spu_alarm.h \
           src/spread/spu_alarm_types.h \
           src/spread/spu_data_link.h \
           src/spread/spu_events.h \
           src/spread/spu_memory.h \
           src/spread/spu_objects.h \
           src/spread/spu_objects_local.h \
           src/spread/spu_scatter.h \
           src/spread/spu_system.h \
           src/spread/spu_system_defs.h \
           src/spread/spu_system_defs_autoconf.h \
           src/spread/spu_system_defs_windows.h \
           src/spread/sp_events.h \
           src/spread/sp_func.h
HEADERS *= src/rmname.h
win32:DEFINES += ARCH_PC_WIN95 _REENTRANT DISABLE_FUNCTION_NAME_LOOKUP
}

#RM DUAL UDP PROTOCOL
no_rmdudp{
DEFINES += NO_RMDUDP
} else {
SOURCES += src/rm/rmdudp.cpp
HEADERS += src/rm/rmdudp.h
}

#RM INTEGRITY CONNECTION PROTOCOL
no_rmiconn {
DEFINES += NO_RMICONN
} else {
SOURCES += src/rm/rmconn.cpp
HEADERS += src/rm/rmconn.h
}

rtx {
DEFINES += RTX
INCLUDEPATH += "C:\Program Files\Ardence\RTX\include" "C:\Program Files\Ardence\RTX\RT-TCPIP SDK\include"
LIBS += "C:\Program Files\Ardence\RTX\lib\rtapi_w32.lib"
}

#!/usr/bin/perl

$version = "5.0";
$username = getlogin();

require Config;
$OS = $Config::Config{'osname'};

($host1, @1) = gethostbyname(localhost);
$host2 = $ENV{HOST};
if ($host1 eq "localhost") { $hostname = $host2; }
else { $hostname = $host1; }

($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
$mon = $mon + 1;
$year = $year + 1900;

print "#define COMPILE_VER \"$version\"\n";
print "#define COMPILE_BY  \"$username\"\n";
print "#define COMPILE_HOST \"$hostname\"\n";
print "#define COMPILE_OSNAME \"$OS\"\n";
print "#define COMPILE_DATE \"$mday/$mon/$year\"\n";
print "#define COMPILE_TIME \"$hour:$min:$sec\"\n";


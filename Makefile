MAKEFILES = rm5lib rm5tcp_static rm5tcp rm5clr rmtest rm5sync rmc rm5send rm5recv rm5sync_delay rm5ssm
CYGWIN_FLAGS = -spec win32-g++ -unix
TOR2_FLAGS = -spec vxworks-cc386 -unix

all:
	test -f rm5lib.mak || qmake -o rm5lib.mak rm5lib.pro
	make -q -f rm5lib.mak || ./makever.sh > include/rm_ver.h
	for i in $(MAKEFILES); do test -f $$i.mak || qmake -o $$i.mak $$i.pro; make -f $$i.mak all; done
	make -f rm5lib.mak staticlib

clean:
	for i in $(MAKEFILES); do test -f $$i.mak || qmake -o $$i.mak $$i.pro; make -f $$i.mak clean; done
	rm -f include/rm_ver.h
	for i in $(MAKEFILES); do rm -f $$i.mak; done

tor2:
	if not exist rm5lib.mak qmake $(TOR2_FLAGS) -o rm5lib.mak rm5lib.pro
	make -q -f rm5lib.mak || makever.bat > include/rm_ver.h
	make -f rm5lib.mak staticlib
	if not exist rm5sync.mak qmake $(TOR2_FLAGS) -o rm5sync.mak rm5sync.pro
	make -f rm5sync.mak

qnx:
	test -f rm5lib.mak || qmake -unix -o rm5lib.mak rm5lib.pro
	make -q -f rm5lib.mak || ./makever.sh > include/rm_ver.h
	for i in $(MAKEFILES); do test -f $$i.mak || qmake -unix -o $$i.mak $$i.pro; make -f $$i.mak; done
	make -f rm5lib.mak staticlib

qnx-clean:
	for i in $(MAKEFILES); do test -f $$i.mak || qmake -unix -o $$i.mak $$i.pro; make -f $$i.mak clean; done
	rm -f include/rm_ver.h
	for i in $(MAKEFILES); do rm -f $$i.mak; done

cygwin:
	test -f rm5lib.mak || qmake -o rm5lib.mak rm5lib.pro
	make -q -f rm5lib.mak || ./makever.sh > include/rm_ver.h
	for i in $(MAKEFILES); do test -f $$i.mak || qmake $(CYGWIN_FLAGS) -o $$i.mak $$i.pro; make -f $$i.mak; done
	make -f rm5lib.mak staticlib

win:
	for %%i in ( $(MAKEFILES) ); do qmake -o %i.mak %i.pro
	nmake /q /f rm5lib.mak || makever.bat > include/rm_ver.h
	for %%i in ( $(MAKEFILES) ); do nmake -f %i.mak all

win-clean:
	for %%i in ( $(MAKEFILES) ); do nmake -f %i.mak clean

doc: Doxyfile
	doxygen
	make -C docs/latex pdf


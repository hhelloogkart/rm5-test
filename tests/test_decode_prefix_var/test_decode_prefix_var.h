#ifndef _TEST_DECODE_PREFIX_VAR_H_
#define _TEST_DECODE_PREFIX_VAR_H_

#include <cxxtest/TestSuite.h>

class DecodePrefixVarTestSuite : public CxxTest::TestSuite
{
public:
	DecodePrefixVarTestSuite();
	void testExtract();
	void testNestedExtract();
	void testMisc();
};

#endif
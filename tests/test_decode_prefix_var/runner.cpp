/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_decode_prefix_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_DecodePrefixVarTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_decode_prefix_var\test_decode_prefix_var.h"

static DecodePrefixVarTestSuite suite_DecodePrefixVarTestSuite;

static CxxTest::List Tests_DecodePrefixVarTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_DecodePrefixVarTestSuite( "test_decode_prefix_var/test_decode_prefix_var.h", 6, "DecodePrefixVarTestSuite", suite_DecodePrefixVarTestSuite, Tests_DecodePrefixVarTestSuite );

static class TestDescription_suite_DecodePrefixVarTestSuite_testExtract : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_DecodePrefixVarTestSuite_testExtract() : CxxTest::RealTestDescription( Tests_DecodePrefixVarTestSuite, suiteDescription_DecodePrefixVarTestSuite, 10, "testExtract" ) {}
 void runTest() { suite_DecodePrefixVarTestSuite.testExtract(); }
} testDescription_suite_DecodePrefixVarTestSuite_testExtract;

static class TestDescription_suite_DecodePrefixVarTestSuite_testNestedExtract : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_DecodePrefixVarTestSuite_testNestedExtract() : CxxTest::RealTestDescription( Tests_DecodePrefixVarTestSuite, suiteDescription_DecodePrefixVarTestSuite, 11, "testNestedExtract" ) {}
 void runTest() { suite_DecodePrefixVarTestSuite.testNestedExtract(); }
} testDescription_suite_DecodePrefixVarTestSuite_testNestedExtract;

static class TestDescription_suite_DecodePrefixVarTestSuite_testMisc : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_DecodePrefixVarTestSuite_testMisc() : CxxTest::RealTestDescription( Tests_DecodePrefixVarTestSuite, suiteDescription_DecodePrefixVarTestSuite, 12, "testMisc" ) {}
 void runTest() { suite_DecodePrefixVarTestSuite.testMisc(); }
} testDescription_suite_DecodePrefixVarTestSuite_testMisc;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

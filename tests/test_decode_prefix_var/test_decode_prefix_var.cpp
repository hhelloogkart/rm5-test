#include "test_decode_prefix_var.h"

#include <decodeprefix_var.h>
#include <prefiximpl.h>
#include <cplx_var.h>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>
#include <limits.h>

#define NELEM(a) (sizeof(a)/sizeof(a[0]))

#define DEFINE_TEMPL_DECODE_VAR(t,rg,os) \
struct t##_s \
{ \
	static double SCALE_FACTOR; \
	static double OFFSET; \
}; \
double t##_s::SCALE_FACTOR = rg; \
double t##_s::OFFSET = os; \
templ_decode_prefix_unsigned_var<t##_s> t##u[2]; \
templ_decode_prefix_signed_var<t##_s> t##s[2];

DEFINE_TEMPL_DECODE_VAR(d1,0.001,0);
DEFINE_TEMPL_DECODE_VAR(d2,0.01,500);
DEFINE_TEMPL_DECODE_VAR(d3,100,50000);

vdecode_prefix_var *var[] =
{
	&d1u[0],
	&d1s[0],
	&d2u[0],
	&d2s[0],
	&d3u[0],
	&d3s[0]
};

vdecode_prefix_var *mirror[] =
{
	&d1u[1],
	&d1s[1],
	&d2u[1],
	&d2s[1],
	&d3u[1],
	&d3s[1]
};

DEFINE_DECODE_PREFIX_SIGNED_VAR(lat_dfv_typ,0.000001,0);
DEFINE_DECODE_PREFIX_SIGNED_VAR(lon_dfv_typ,0.000001,103);
DEFINE_DECODE_PREFIX_UNSIGNED_VAR(alt_dfv_typ,0.001,-200);

struct c_typ : public complex_var
{
	COMPLEXCONST(c_typ)
	lat_dfv_typ lat;
	lon_dfv_typ lon;
	alt_dfv_typ alt;
};

DecodePrefixVarTestSuite::DecodePrefixVarTestSuite()
{
	srand((unsigned int)time(NULL));
	value_var::set_float_format("%.18g");
}

void DecodePrefixVarTestSuite::testExtract()
{
	unsigned char buf[100];
	outbuf ob;
	double incr[6] = { 0.001, 0.002, 0.01, 0.02, 100, 200 };
	d1u[0] = 0;
	d1s[0] = -10000.; // below the bias point
	d2u[0] = 500;
	d2s[0] = 0.; // below the bias point
	d3u[0] = 50000.;
	d3s[0] = 5000.; // below the bias point
	for (int i = 0; i < NELEM(var); ++i)
	{
		for (int j = 0; j < 2000; ++j, var[i]->fromValue(var[i]->to_double() + incr[i]))
		{
			memset(buf, 0, sizeof(buf));
			ob.set(buf, sizeof(buf));
			var[i]->output(ob);
			TS_ASSERT_EQUALS(ob.size(), var[i]->size());

			mirror[i]->extract(ob.size(), ob.get());
			TS_ASSERT_DELTA(var[i]->to_double(), mirror[i]->to_double(), 0.0001);
		}
	}
}

void DecodePrefixVarTestSuite::testNestedExtract()
{
	unsigned char buf[100];
	outbuf ob;
	c_typ pos[2];
	pos[0].lat = 0.0;
	pos[0].lon = 103.0;
	pos[0].alt = -200.0;
	int lastsz = 0;
	do
	{
		memset(buf, 0, sizeof(buf));
		ob.set(buf, sizeof(buf));
		pos[0].output(ob);
		const int sz = ob.size();
		TS_ASSERT_LESS_THAN_EQUALS(lastsz, sz);
		if (lastsz < sz) lastsz = sz;

		pos[1].extract(sz, buf);
		TS_ASSERT_DELTA(pos[0].lat, pos[1].lat, 0.0000005);
		TS_ASSERT_DELTA(pos[0].lon, pos[1].lon, 0.0000005);
		TS_ASSERT_DELTA(pos[0].alt, pos[1].alt, 0.0005);

		pos[0].lat += 0.000001;
		pos[0].lon += 0.000001;
		pos[0].alt += 0.001;
	} while (lastsz < 12);

	pos[0].lat = 0.0;
	pos[0].lon = 103.0;
	pos[0].alt = -200.0;
	lastsz = 0;
	do
	{
		memset(buf, 0, sizeof(buf));
		ob.set(buf, sizeof(buf));
		pos[0].output(ob);
		const int sz = ob.size();
		TS_ASSERT_LESS_THAN_EQUALS(lastsz, sz);
		if (lastsz < sz) lastsz = sz;

		pos[1].extract(sz, buf);
		TS_ASSERT_DELTA(pos[0].lat, pos[1].lat, 0.0000005);
		TS_ASSERT_DELTA(pos[0].lon, pos[1].lon, 0.0000005);
		TS_ASSERT_DELTA(pos[0].alt, pos[1].alt, 0.0005);

		pos[0].lat -= 0.000001;
		pos[0].lon -= 0.000001;
		pos[0].alt += 0.001;
	} while (lastsz < 12);
}

void DecodePrefixVarTestSuite::testMisc()
{
	TS_ASSERT_EQUALS(d1u[0].rtti(), d1u[1].rtti());
	TS_ASSERT_EQUALS(d1s[0].rtti(), d1s[1].rtti());
	TS_ASSERT_DIFFERS(d1s[0].rtti(), d1u[0].rtti());
	templ_decode_prefix_unsigned_var<d1_s> d4u(d1u[0]);
	TS_ASSERT_EQUALS(d4u, d1u[0]);
	d4u.setInvalid();
	TS_ASSERT(!d4u.isValid());
	TS_ASSERT_DIFFERS(d4u, d1u[0]);
	d4u = d1u[0];
	TS_ASSERT_EQUALS(d4u, d1u[0]);
	d4u.setInvalid();
	mpt_var &mptr = d1u[0];
	d4u = mptr;
	TS_ASSERT_EQUALS(d4u, d1u[0]);
	TS_ASSERT_EQUALS(d4u, mptr);

	templ_decode_prefix_signed_var<d1_s> d4s(d1s[0]);
	TS_ASSERT_EQUALS(d4s, d1s[0]);
	d4s.setInvalid();
	TS_ASSERT(!d4s.isValid());
	TS_ASSERT_DIFFERS(d4s, d1s[0]);
	d4s = d1s[0];
	TS_ASSERT_EQUALS(d4s, d1s[0]);
	d4s.setInvalid();
	mpt_var &mptr2 = d1s[0];
	d4s = mptr2;
	TS_ASSERT_EQUALS(d4s, d1s[0]);
	TS_ASSERT_EQUALS(d4s, mptr2);

	TS_ASSERT_EQUALS(d1u[0].getNext(), &(d1u[1]));

	char pfbuf[9];
	int pfsz;
	int pfex;

	unsigned char uch[2];
	uch[0] = rand();
	pfsz = prefix_encode(pfbuf, uch[0]);
	TS_ASSERT_LESS_THAN_EQUALS(pfsz, 2);
	pfex = prefix_decode(pfbuf, pfsz, uch[1]);
	TS_ASSERT_EQUALS(pfex, pfsz);
	TS_ASSERT_EQUALS(uch[0], uch[1]);

	unsigned short ush[2];
	ush[0] = rand();
	pfsz = prefix_encode(pfbuf, ush[0]);
	TS_ASSERT_LESS_THAN_EQUALS(pfsz, 3);
	pfex = prefix_decode(pfbuf, pfsz, ush[1]);
	TS_ASSERT_EQUALS(pfex, pfsz);
	TS_ASSERT_EQUALS(ush[0], ush[1]);

	unsigned int uit[2];
	uit[0] = rand();
	pfsz = prefix_encode(pfbuf, uit[0]);
	TS_ASSERT_LESS_THAN_EQUALS(pfsz, 5);
	pfex = prefix_decode(pfbuf, pfsz, uit[1]);
	TS_ASSERT_EQUALS(pfex, pfsz);
	TS_ASSERT_EQUALS(uit[0], uit[1]);

	unsigned long ulg[2];
	ulg[0] = rand();
	pfsz = prefix_encode(pfbuf, ulg[0]);
	TS_ASSERT_LESS_THAN_EQUALS(pfsz, 5);
	pfex = prefix_decode(pfbuf, pfsz, ulg[1]);
	TS_ASSERT_EQUALS(pfex, pfsz);
	TS_ASSERT_EQUALS(ulg[0], ulg[1]);

	unsigned long long ull[2];
	ull[0] = rand() * rand();
	pfsz = prefix_encode(pfbuf, ull[0]);
	TS_ASSERT_LESS_THAN_EQUALS(pfsz, 9);
	pfex = prefix_decode(pfbuf, pfsz, ull[1]);
	TS_ASSERT_EQUALS(pfex, pfsz);
	TS_ASSERT_EQUALS(ull[0], ull[1]);

	char sch[2];
	sch[0] = rand();
	pfsz = prefix_encode(pfbuf, sch[0]);
	TS_ASSERT_LESS_THAN_EQUALS(pfsz, 2);
	pfex = prefix_decode(pfbuf, pfsz, sch[1]);
	TS_ASSERT_EQUALS(pfex, pfsz);
	TS_ASSERT_EQUALS(sch[0], sch[1]);

	short ssh[2];
	ssh[0] = rand();
	pfsz = prefix_encode(pfbuf, ssh[0]);
	TS_ASSERT_LESS_THAN_EQUALS(pfsz, 3);
	pfex = prefix_decode(pfbuf, pfsz, ssh[1]);
	TS_ASSERT_EQUALS(pfex, pfsz);
	TS_ASSERT_EQUALS(ssh[0], ssh[1]);

	int sit[2];
	sit[0] = rand();
	pfsz = prefix_encode(pfbuf, sit[0]);
	TS_ASSERT_LESS_THAN_EQUALS(pfsz, 5);
	pfex = prefix_decode(pfbuf, pfsz, sit[1]);
	TS_ASSERT_EQUALS(pfex, pfsz);
	TS_ASSERT_EQUALS(sit[0], sit[1]);

	long slg[2];
	slg[0] = rand();
	pfsz = prefix_encode(pfbuf, slg[0]);
	TS_ASSERT_LESS_THAN_EQUALS(pfsz, 5);
	pfex = prefix_decode(pfbuf, pfsz, slg[1]);
	TS_ASSERT_EQUALS(pfex, pfsz);
	TS_ASSERT_EQUALS(slg[0], slg[1]);

	long long sll[2];
	sll[0] = rand() * rand();
	pfsz = prefix_encode(pfbuf, sll[0]);
	TS_ASSERT_LESS_THAN_EQUALS(pfsz, 9);
	pfex = prefix_decode(pfbuf, pfsz, sll[1]);
	TS_ASSERT_EQUALS(pfex, pfsz);
	TS_ASSERT_EQUALS(sll[0], sll[1]);

	ull[0] = 18446744073141661493ULL;
	pfsz = prefix_encode(pfbuf, ull[0]);
	TS_ASSERT_LESS_THAN_EQUALS(pfsz, 9);
	pfex = prefix_decode(pfbuf, pfsz, ull[1]);
	TS_ASSERT_EQUALS(pfex, pfsz);
	TS_ASSERT_EQUALS(ull[0], ull[1]);

	double dbl[2];
	dbl[0] = 3.141592653589793238462643383279502884197e52;
	dbl[1] = decode_round(dbl[0]);
	TS_ASSERT_EQUALS(dbl[0], dbl[1]);

	dbl[0] = nan("");
	dbl[1] = decode_round(dbl[0]);
	TS_ASSERT(isnan(dbl[1]));
	TS_TRACE("End");
}

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ttag_var.h>
#include <link_var.h>
#include <mpt_show.h>
#include "test_ttag_var.h"

class dirty_checker : public mpt_baseshow
{
public:
	dirty_checker(mpt_var *myvar);
	virtual void setDirty(mpt_var *myvar);
	virtual void notDirty(mpt_var *myvar);
	int getStateAndReset();
protected:
	int state; // 0 - not called, 1 - dirty, 2 - notdirty, 3 - error
	mpt_var *var;
};

dirty_checker::dirty_checker(mpt_var *myvar) : state(0), var(myvar)
{
	myvar->addCallback(this);
}

void dirty_checker::setDirty(mpt_var *myvar)
{
	if (myvar == var) state = 1;
	else state = 3;
}

void dirty_checker::notDirty(mpt_var *myvar)
{
	if (myvar == var) state = 2;
	else state = 3;
}

int dirty_checker::getStateAndReset()
{
	int ret = state;
	state = 0;
	return ret;
}

TtagVarTestSuite::TtagVarTestSuite()
{
	srand((unsigned int)time(NULL));
}

void TtagVarTestSuite::testExtractAndPack(void)
{
	ttag_var tt;
	tt = rand();
	unsigned char buf[8];
	outbuf ob;
	ob.set(buf, sizeof(buf));
	tt.output(ob);
	TS_ASSERT_EQUALS(ob.size(), 3);

	ttag_var tt2;
	dirty_checker chk(&tt2);
	tt2.extract(ob.size(), buf);
	TS_ASSERT_EQUALS(tt, tt2);
	TS_ASSERT_EQUALS(chk.getStateAndReset(), 1);
	tt2.extract(ob.size(), buf);
	TS_ASSERT_EQUALS(chk.getStateAndReset(), 2);
}

void TtagVarTestSuite::testCopyAndAssigment(void)
{
	ttag_var tt;
	tt = rand();
	ttag_var tt2(tt);
	TS_ASSERT_EQUALS(tt, tt2);
	tt2.setInvalid();
	TS_ASSERT_DIFFERS(tt, tt2);
	tt2 = tt;
	TS_ASSERT_EQUALS(tt, tt2);
	mpt_var &mptr = tt2;
	TS_ASSERT_EQUALS(tt, mptr);
	link_var lv;
	lv = tt2;
	TS_ASSERT_EQUALS(tt, lv);
	tt.setInvalid();
	TS_ASSERT_DIFFERS(tt, tt2);
	tt = mptr;
	TS_ASSERT_EQUALS(tt, tt2);
}

void TtagVarTestSuite::testMisc(void)
{
	generic_var<unsigned int> ui;
	ttag_var tt[2];
	TS_ASSERT_EQUALS(tt[0].rtti(), tt[1].rtti());
	TS_ASSERT_DIFFERS(tt[0].rtti(), ui.rtti());
	TS_ASSERT_EQUALS(tt[0].getNext(), &tt[1]);
	TS_ASSERT_EQUALS(tt[0].size(), 3);
	TS_TRACE("End");
}

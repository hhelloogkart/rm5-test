/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_ttag_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_TtagVarTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_ttag_var\test_ttag_var.h"

static TtagVarTestSuite suite_TtagVarTestSuite;

static CxxTest::List Tests_TtagVarTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_TtagVarTestSuite( "test_ttag_var/test_ttag_var.h", 6, "TtagVarTestSuite", suite_TtagVarTestSuite, Tests_TtagVarTestSuite );

static class TestDescription_suite_TtagVarTestSuite_testExtractAndPack : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_TtagVarTestSuite_testExtractAndPack() : CxxTest::RealTestDescription( Tests_TtagVarTestSuite, suiteDescription_TtagVarTestSuite, 10, "testExtractAndPack" ) {}
 void runTest() { suite_TtagVarTestSuite.testExtractAndPack(); }
} testDescription_suite_TtagVarTestSuite_testExtractAndPack;

static class TestDescription_suite_TtagVarTestSuite_testCopyAndAssigment : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_TtagVarTestSuite_testCopyAndAssigment() : CxxTest::RealTestDescription( Tests_TtagVarTestSuite, suiteDescription_TtagVarTestSuite, 11, "testCopyAndAssigment" ) {}
 void runTest() { suite_TtagVarTestSuite.testCopyAndAssigment(); }
} testDescription_suite_TtagVarTestSuite_testCopyAndAssigment;

static class TestDescription_suite_TtagVarTestSuite_testMisc : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_TtagVarTestSuite_testMisc() : CxxTest::RealTestDescription( Tests_TtagVarTestSuite, suiteDescription_TtagVarTestSuite, 12, "testMisc" ) {}
 void runTest() { suite_TtagVarTestSuite.testMisc(); }
} testDescription_suite_TtagVarTestSuite_testMisc;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

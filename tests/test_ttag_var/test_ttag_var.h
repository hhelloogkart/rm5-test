#ifndef _TEST_TTAG_VAR_H_DEF_
#define _TEST_TTAG_VAR_H_DEF_

#include <cxxtest/TestSuite.h>

class TtagVarTestSuite : public CxxTest::TestSuite
{
public:
	TtagVarTestSuite();
    void testExtractAndPack(void);
    void testCopyAndAssigment(void);
    void testMisc(void);
};

#endif


/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_pad_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_PadVarTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_pad_var\test_pad_var.h"

static PadVarTestSuite suite_PadVarTestSuite;

static CxxTest::List Tests_PadVarTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_PadVarTestSuite( "test_pad_var/test_pad_var.h", 6, "PadVarTestSuite", suite_PadVarTestSuite, Tests_PadVarTestSuite );

static class TestDescription_suite_PadVarTestSuite_testOutput : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_PadVarTestSuite_testOutput() : CxxTest::RealTestDescription( Tests_PadVarTestSuite, suiteDescription_PadVarTestSuite, 9, "testOutput" ) {}
 void runTest() { suite_PadVarTestSuite.testOutput(); }
} testDescription_suite_PadVarTestSuite_testOutput;

static class TestDescription_suite_PadVarTestSuite_testAssignment : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_PadVarTestSuite_testAssignment() : CxxTest::RealTestDescription( Tests_PadVarTestSuite, suiteDescription_PadVarTestSuite, 10, "testAssignment" ) {}
 void runTest() { suite_PadVarTestSuite.testAssignment(); }
} testDescription_suite_PadVarTestSuite_testAssignment;

static class TestDescription_suite_PadVarTestSuite_testRtti : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_PadVarTestSuite_testRtti() : CxxTest::RealTestDescription( Tests_PadVarTestSuite, suiteDescription_PadVarTestSuite, 11, "testRtti" ) {}
 void runTest() { suite_PadVarTestSuite.testRtti(); }
} testDescription_suite_PadVarTestSuite_testRtti;

static class TestDescription_suite_PadVarTestSuite_testGetNext : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_PadVarTestSuite_testGetNext() : CxxTest::RealTestDescription( Tests_PadVarTestSuite, suiteDescription_PadVarTestSuite, 12, "testGetNext" ) {}
 void runTest() { suite_PadVarTestSuite.testGetNext(); }
} testDescription_suite_PadVarTestSuite_testGetNext;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

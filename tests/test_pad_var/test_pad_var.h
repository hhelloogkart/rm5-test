#ifndef _TEST_PAD_VAR_H_DEF_
#define _TEST_PAD_VAR_H_DEF_

#include <cxxtest/TestSuite.h>

class PadVarTestSuite : public CxxTest::TestSuite
{
public:
	void testOutput(void);
	void testAssignment(void);
	void testRtti();
	void testGetNext();
};

#endif


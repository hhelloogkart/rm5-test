#include "test_pad_var.h"
#include <pad_var.h>
#include <arry_var.h>
#include <link_var.h>
#include <mpt_show.h>
#include <outbuf.h>
#include <util/utilcore.h>
#include <sstream>
#include <vector>
#include <string.h>

#define TS_ASSERT_NOT(x) TS_ASSERT(!(x))

class dirty_checker : public mpt_baseshow
{
public:
	dirty_checker(mpt_var *myvar);
	virtual void setDirty(mpt_var *myvar);
	virtual void notDirty(mpt_var *myvar);
	int getStateAndReset();
protected:
	int state; // 0 - not called, 1 - dirty, 2 - notdirty, 3 - error
	mpt_var *var;
};

dirty_checker::dirty_checker(mpt_var *myvar) : state(0), var(myvar)
{
	myvar->addCallback(this);
}

void dirty_checker::setDirty(mpt_var *myvar)
{
	if (myvar == var) state = 1;
	else state = 3;
}

void dirty_checker::notDirty(mpt_var *myvar)
{
	if (myvar == var) state = 2;
	else state = 3;
}

int dirty_checker::getStateAndReset()
{
	int ret = state;
	state = 0;
	return ret;
}

class Pvtest : public pad_var<200>
{
public:
	PADCONST(Pvtest)
	array_var<int> data;
};

class Cvtest : public complex_var
{
public:
	COMPLEXCONST(Cvtest)
	array_var<int> data;
};

void PadVarTestSuite::testOutput(void)
{
	std::vector<int> vec;
	Pvtest t1;
	t1.data.resize(20);
	for (int idx = 0; idx < t1.data.cardinal(); ++idx)
	{
		vec.push_back(rand());
		t1.data.fromValue(idx, vec.back());
	}
	TS_ASSERT_EQUALS(t1.size(), 200);
	unsigned char buf[300];
	outbuf ob;
	ob.set(buf, sizeof(buf));
	t1.output(ob);
	TS_ASSERT_EQUALS(ob.size(), 200);
	// first 2 byte is cardinal
	unsigned char *ptr = ob.get();
	short car = t1.data.cardinal();
	TS_ASSERT_SAME_DATA(ptr, &car, sizeof(short));
	ptr += sizeof(unsigned short);
	for (int idx = 0; idx < car; ++idx)
	{
		int val = vec[idx];
		TS_ASSERT_SAME_DATA(ptr, &val, sizeof(int));
		ptr += sizeof(int);
	}
	for (; ptr != ob.getcur(); ++ptr)
	{
		TS_ASSERT_EQUALS(*ptr, 0);
	}

	Pvtest t2;
	dirty_checker chk2(&t2);
	t2.extract(ob.size(), buf); // full 200 bytes
	TS_ASSERT_EQUALS(t1, t2);
	TS_ASSERT_EQUALS(chk2.getStateAndReset(), 1);

	Pvtest t3;
	dirty_checker chk3(&t3); // shorter still can
	t3.extract(t1.data.cardinal() * sizeof(int) + sizeof(short), buf); // full 200 bytes
	TS_ASSERT_EQUALS(t1, t3);
	TS_ASSERT_EQUALS(chk3.getStateAndReset(), 1);

	t1.data.resize(49);
	for (int idx = 20; idx < t1.data.cardinal(); ++idx)
	{
		vec.push_back(rand());
		t1.data.fromValue(idx, vec.back());
	}
	TS_ASSERT_EQUALS(t1.size(), 200);
	ob.set(buf, sizeof(buf));
	t1.output(ob);
	TS_ASSERT_EQUALS(ob.size(), 200);
	ptr = ob.get();
	car = t1.data.cardinal();
	TS_ASSERT_SAME_DATA(ptr, &car, sizeof(short));
	ptr += sizeof(unsigned short);
	for (int idx = 0; idx < car; ++idx)
	{
		int val = vec[idx];
		TS_ASSERT_SAME_DATA(ptr, &val, sizeof(int));
		ptr += sizeof(int);
	}
	for (; ptr != ob.getcur(); ++ptr)
	{
		TS_ASSERT_EQUALS(*ptr, 0);
	}

	t1.data.resize(60);
	for (int idx = 49; idx < t1.data.cardinal(); ++idx)
	{
		vec.push_back(rand());
		t1.data.fromValue(idx, vec.back());
	}
	TS_ASSERT_EQUALS(t1.size(), 60*4+2);
	ob.set(buf, sizeof(buf));
	t1.output(ob);
	TS_ASSERT_EQUALS(ob.size(), 60*4+2);
	ptr = ob.get();
	car = t1.data.cardinal();
	TS_ASSERT_SAME_DATA(ptr, &car, sizeof(short));
	ptr += sizeof(unsigned short);
	for (int idx = 0; idx < car; ++idx)
	{
		int val = vec[idx];
		TS_ASSERT_SAME_DATA(ptr, &val, sizeof(int));
		ptr += sizeof(int);
	}

	Pvtest t4;
	dirty_checker chk4(&t4);
	t4.extract(ob.size(), buf); // more than 200 bytes
	TS_ASSERT_EQUALS(t1, t4);
	TS_ASSERT_EQUALS(chk4.getStateAndReset(), 1);
}

void PadVarTestSuite::testAssignment(void)
{
	std::vector<int> vec;
	Pvtest t1;
	t1.data.resize(20);
	for (int idx = 0; idx < t1.data.cardinal(); ++idx)
	{
		vec.push_back(rand());
		t1.data.fromValue(idx, vec.back());
	}
	Pvtest t2(t1);
	TS_ASSERT_EQUALS(t1, t2);
	t2.setInvalid();
	TS_ASSERT_DIFFERS(t1, t2);
	t2 = t1;
	TS_ASSERT_EQUALS(t1, t2);
	t2.setInvalid();
	TS_ASSERT_DIFFERS(t1, t2);
	mpt_var &mtr = t1;
	t2 = mtr;
	TS_ASSERT_EQUALS(t1, t2);
	TS_ASSERT_EQUALS(t1, mtr);
	link_var lv;
	lv = mtr;
	TS_ASSERT_EQUALS(t1, lv);
}

void PadVarTestSuite::testRtti()
{
	Pvtest t1, t2;
	Cvtest c1;
	generic_var<int> gv;
	array_var<char> av;
	link_var lv;

	lv = t2;

	TS_ASSERT_EQUALS(t1.rtti(), t2.rtti());
	TS_ASSERT_DIFFERS(t1.rtti(), c1.rtti());
	TS_ASSERT_DIFFERS(t1.rtti(), gv.rtti());
	TS_ASSERT_DIFFERS(t1.rtti(), av.rtti());
	TS_ASSERT_DIFFERS(t2.rtti(), lv.rtti());
}

void PadVarTestSuite::testGetNext()
{
	Pvtest p[2];
	TS_ASSERT_EQUALS(p[0].getNext(), &p[1]);
	TS_TRACE("End");
}

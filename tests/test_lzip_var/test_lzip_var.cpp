#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <gen_var.h>
#include <arry_var.h>
#include <lzip_var.h>
#include "test_lzip_var.h"

struct lz1_typ : public lzip_var
{
    LZIPCONST( lz1_typ )

    struct c_typ : public complex_var
    {
        COMPLEXCONST( c_typ )
        generic_var<int> a;
        generic_var<char> b;
    } c;
    generic_var<int> a[256];
    generic_var<char> b[256];
};

//compress a small data
struct lz2_typ : public lzip_var
{
    LZIPCONST( lz2_typ )

    generic_var<char> a;
};

//compress a big data
struct lz3_typ : public lzip_var
{
    LZIPCONST( lz3_typ )

    generic_var<char> a[32768];
};

//multiple consecutive compress data
struct lz4_typ : public complex_var
{
    COMPLEXCONST( lz4_typ );

    lz1_typ lz1;
    lz2_typ lz2;
    lz3_typ lz3;
    //test following data
    generic_var<int> a;
};

//variable length data
struct lz5_typ : public lzip_var
{
    LZIPCONST2(lz5_typ, 9)
    array_var<int> a;
};

lz1_typ lz1, mirror1;
lz2_typ lz2, mirror2;
lz3_typ lz3, mirror3;
lz4_typ lz4, mirror4;

void LzipVarTestSuite::test00(void)
{
    testValue(0);
}

void LzipVarTestSuite::testFF(void)
{
    testValue(0xFFFFFFFF);
}

void LzipVarTestSuite::test55AA(void)
{
    testValue(0x55AA55AA);
}

void LzipVarTestSuite::testRunningNumber(void)
{
    testValue(1);
}

void LzipVarTestSuite::testRandom(void)
{
    srand((unsigned int)time(NULL));
    testValue(2);
}

void LzipVarTestSuite::testCorruptData(void)
{
    static unsigned char buf[65536];
    complex_var *var = &lz1;
    complex_var *mirror = &mirror1;
    outbuf ob;
    int ret;

    memset(buf, 0, sizeof(buf));
    setValue(2);

    *mirror = *var;
    ob.set(buf, sizeof(buf));
    var->output(ob);
    TS_ASSERT(ob.size() > 0);

    //corrupt the data
    buf[20] = ~buf[20];
    buf[21] = ~buf[21];

    setValue(0);

    ret = var->extract(ob.size(), ob.get());
    TS_ASSERT(ret == 0);
    TS_ASSERT(*((mpt_var*)var) != *((mpt_var *)mirror));

    lz3.setCompressionLevel(9);
    for (int idx = 0; idx < lz3.cardinal(); ++idx)
        reinterpret_cast<value_var &>(*lz3[idx]).fromValue(0);
    int sz = lz3.size();
    unsigned char *buf2 = new unsigned char[sz];
    ob.set(buf2, sz);
    lz3.output(ob);

    // corrupting the dummy doesn't matter
    buf2[sz - 1] = ~buf2[sz - 1];
    mirror3.extract(sz, buf2);
    for (int idx = 0; idx < mirror3.cardinal(); ++idx)
        TS_ASSERT_EQUALS(mirror3.a[idx], 0);
    TS_ASSERT_EQUALS(lz3, mirror3);

    // corrupting the next byte does (doesn't for LZIP 1.11 which is more robust)
    mirror3.setInvalid();
    buf2[sz - 2] = ~buf2[sz - 2];
    mirror3.extract(sz, buf2);
    TS_ASSERT_EQUALS(lz3, mirror3);

    mirror3.setInvalid();
    for (int idx=3; idx<30; ++idx)
        buf2[sz - idx] = ~buf2[sz - idx];
    mirror3.extract(sz, buf2);
    TS_ASSERT_DIFFERS(lz3, mirror3);

    delete[] buf2;
}

void LzipVarTestSuite::testCompressionLevels(void)
{
    for (int com = 0; com < 10; ++com)
    {
        lz1.setCompressionLevel(com);
        lz2.setCompressionLevel(com);
        lz3.setCompressionLevel(com);
        lz4.lz1.setCompressionLevel(com);
        lz4.lz2.setCompressionLevel(com);
        lz4.lz3.setCompressionLevel(com);
        testValue(1);
        testValue(2);
    }
}

void LzipVarTestSuite::testVariableLength(void)
{
    lz5_typ lz5;
    TS_ASSERT_EQUALS(lz5.compressionLevel(), 9);
    lz5.a.resize(2000);
    for (int idx = 0; idx < 2000; ++idx)
        lz5.a.fromValue(idx, rand());
    unsigned char buf[8000];
    outbuf ob;
    ob.set(buf, sizeof(buf));
    lz5.output(ob);

    lz5_typ mirror5;
    TS_ASSERT_EQUALS(mirror5.extract(ob.size(), buf), 0);
    TS_ASSERT_EQUALS(lz5, mirror5);
    mirror5.setInvalid();
    TS_ASSERT_DIFFERS(lz5, mirror5);
    TS_ASSERT_EQUALS(mirror5.extract(ob.size()-1, buf), 0); // skip the Z
    TS_ASSERT_EQUALS(lz5, mirror5);
    mirror5.setInvalid();
    TS_ASSERT(!mirror5.isValid());
    TS_ASSERT_EQUALS(mirror5.extract(ob.size()-300, buf), 0); // some loss, but still have data
    TS_ASSERT_LESS_THAN(mirror5.a.cardinal(), lz5.a.cardinal());
    for (int idx = 0; idx < mirror5.a.cardinal(); ++idx) // whatever we got was ok
        TS_ASSERT_EQUALS(lz5.a[idx], mirror5.a[idx]);

    lz5.a.resize(65535);
    for (int idx = 2000; idx < 65535; ++idx)
        lz5.a.fromValue(idx, rand());
    unsigned char *buf2 = new unsigned char[lz5.size()]; // new compute
    ob.set(buf2, lz5.size()); // reused
    lz5.output(ob);

    TS_ASSERT_EQUALS(mirror5.extract(ob.size(), buf2), 0);
    TS_ASSERT_EQUALS(lz5, mirror5);
    mirror5.setInvalid();
    TS_ASSERT_EQUALS(mirror5.extract(ob.size() - 2, buf2), 0); // too short decode some but not all
    TS_ASSERT(mirror5.isValid());
    for (int idx = 0; idx < mirror5.a.cardinal(); ++idx) // whatever we got was ok
        TS_ASSERT_EQUALS(lz5.a[idx], mirror5.a[idx]);
    delete[] buf2;
}

void LzipVarTestSuite::testCopyAndAssignment(void)
{
    lz1_typ cpy(lz1);
    TS_ASSERT_EQUALS(lz1, cpy);
    TS_ASSERT_EQUALS(cpy.compressionLevel(), 9);
}

void LzipVarTestSuite::testCompressVar(void)
{
    TS_TRACE("End");
}

void LzipVarTestSuite::setValue(int val, bool bInitGuard)
{
    int i;

    if(val == 1) //incrementing
    {
        lz1.c.a = ++val;
        lz1.c.b = (char)++val;
        for(i = 0; i < 256; ++i)
        {
            lz1.a[i] = ++val;
            lz1.b[i] = (char)++val;
        }

        lz2.a = (char)++val;

        for(i = 0; i < 32768; ++i)
        {
            lz3.a[i] = (char)++val;
        }

        lz4.lz1.c.a = ++val;
        lz4.lz1.c.b = (char)++val;
        for(i = 0; i < 256; ++i)
        {
            lz4.lz1.a[i] = ++val;
            lz4.lz1.b[i] = (char)++val;
        }

        lz4.lz2.a = (char)++val;

        for(i = 0; i < 32768; ++i)
        {
            lz4.lz3.a[i] = (char)++val;
        }

        if(bInitGuard)
            lz4.a = ++val;
    }
    else if(val == 2) //random
    {
        lz1.c.a = rand();
        lz1.c.b = (char)rand();
        for(i = 0; i < 256; ++i)
        {
            lz1.a[i] = rand();
            lz1.b[i] = (char)rand();
        }

        lz2.a = (char)rand();

        for(i = 0; i < 32768; ++i)
        {
            lz3.a[i] = (char)rand();
        }

        lz4.lz1.c.a = rand();
        lz4.lz1.c.b = (char)rand();
        for(i = 0; i < 256; ++i)
        {
            lz4.lz1.a[i] = rand();
            lz4.lz1.b[i] = (char)rand();
        }

        lz4.lz2.a = (char)rand();

        for(i = 0; i < 32768; ++i)
        {
            lz4.lz3.a[i] = (char)rand();
        }

        if(bInitGuard)
            lz4.a = rand();
    }
    else
    {
        lz1.c.a = val;
        lz1.c.b = (char)val;
        for(i = 0; i < 256; ++i)
        {
            lz1.a[i] = val;
            lz1.b[i] = (char)val;
        }

        lz2.a = (char)val;

        for(i = 0; i < 32768; ++i)
        {
            lz3.a[i] = (char)val;
        }

        lz4.lz1.c.a = val;
        lz4.lz1.c.b = (char)val;
        for(i = 0; i < 256; ++i)
        {
            lz4.lz1.a[i] = val;
            lz4.lz1.b[i] = (char)val;
        }

        lz4.lz2.a = (char)val;

        for(i = 0; i < 32768; ++i)
        {
            lz4.lz3.a[i] = (char)val;
        }

        if(bInitGuard)
            lz4.a = val;
    }
}

void LzipVarTestSuite::testValue(int val)
{
    static unsigned char buf[4][65536];
    complex_var *var[4] = { &lz1, &lz2, &lz3, &lz4 };
    complex_var *mirror[4] = { &mirror1, &mirror2, &mirror3, &mirror4 };
    outbuf ob[4];
    char printbuf[100];
    int i;
    int ret;

    memset(buf, 0, sizeof(buf));
    setValue(val);

    for(i = 0; i < 4; ++i)
    {
        *mirror[i] = *var[i];
        ob[i].set(buf[i], sizeof(buf[i]));
        var[i]->output(ob[i]);

        sprintf(printbuf, "Generating variable lz%d\n", i + 1);
        TS_TRACE(printbuf);
        TS_ASSERT(ob[i].size() > 0);
    }

    setValue(~val);

    for(i = 0; i < 4; ++i)
    {
        sprintf(printbuf, "variable lz%d in:%d out:%d %.1f\n", i + 1,
            var[i]->size(), ob[i].size(), 100.0f * ob[i].size()/var[i]->size());
        TS_TRACE(printbuf);

        ret = var[i]->extract(ob[i].size(), ob[i].get());
        TS_ASSERT(ret == 0);
        TS_ASSERT_EQUALS(*(var[i]), *(mirror[i]));
        TS_ASSERT(*((mpt_var*)var[i]) == *((mpt_var *)mirror[i]));
    }
}

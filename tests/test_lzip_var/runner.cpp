/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_lzip_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_LzipVarTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_lzip_var\test_lzip_var.h"

static LzipVarTestSuite suite_LzipVarTestSuite;

static CxxTest::List Tests_LzipVarTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_LzipVarTestSuite( "test_lzip_var/test_lzip_var.h", 6, "LzipVarTestSuite", suite_LzipVarTestSuite, Tests_LzipVarTestSuite );

static class TestDescription_suite_LzipVarTestSuite_test00 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_LzipVarTestSuite_test00() : CxxTest::RealTestDescription( Tests_LzipVarTestSuite, suiteDescription_LzipVarTestSuite, 9, "test00" ) {}
 void runTest() { suite_LzipVarTestSuite.test00(); }
} testDescription_suite_LzipVarTestSuite_test00;

static class TestDescription_suite_LzipVarTestSuite_testFF : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_LzipVarTestSuite_testFF() : CxxTest::RealTestDescription( Tests_LzipVarTestSuite, suiteDescription_LzipVarTestSuite, 10, "testFF" ) {}
 void runTest() { suite_LzipVarTestSuite.testFF(); }
} testDescription_suite_LzipVarTestSuite_testFF;

static class TestDescription_suite_LzipVarTestSuite_test55AA : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_LzipVarTestSuite_test55AA() : CxxTest::RealTestDescription( Tests_LzipVarTestSuite, suiteDescription_LzipVarTestSuite, 11, "test55AA" ) {}
 void runTest() { suite_LzipVarTestSuite.test55AA(); }
} testDescription_suite_LzipVarTestSuite_test55AA;

static class TestDescription_suite_LzipVarTestSuite_testRunningNumber : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_LzipVarTestSuite_testRunningNumber() : CxxTest::RealTestDescription( Tests_LzipVarTestSuite, suiteDescription_LzipVarTestSuite, 12, "testRunningNumber" ) {}
 void runTest() { suite_LzipVarTestSuite.testRunningNumber(); }
} testDescription_suite_LzipVarTestSuite_testRunningNumber;

static class TestDescription_suite_LzipVarTestSuite_testRandom : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_LzipVarTestSuite_testRandom() : CxxTest::RealTestDescription( Tests_LzipVarTestSuite, suiteDescription_LzipVarTestSuite, 13, "testRandom" ) {}
 void runTest() { suite_LzipVarTestSuite.testRandom(); }
} testDescription_suite_LzipVarTestSuite_testRandom;

static class TestDescription_suite_LzipVarTestSuite_testCorruptData : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_LzipVarTestSuite_testCorruptData() : CxxTest::RealTestDescription( Tests_LzipVarTestSuite, suiteDescription_LzipVarTestSuite, 14, "testCorruptData" ) {}
 void runTest() { suite_LzipVarTestSuite.testCorruptData(); }
} testDescription_suite_LzipVarTestSuite_testCorruptData;

static class TestDescription_suite_LzipVarTestSuite_testCompressionLevels : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_LzipVarTestSuite_testCompressionLevels() : CxxTest::RealTestDescription( Tests_LzipVarTestSuite, suiteDescription_LzipVarTestSuite, 15, "testCompressionLevels" ) {}
 void runTest() { suite_LzipVarTestSuite.testCompressionLevels(); }
} testDescription_suite_LzipVarTestSuite_testCompressionLevels;

static class TestDescription_suite_LzipVarTestSuite_testVariableLength : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_LzipVarTestSuite_testVariableLength() : CxxTest::RealTestDescription( Tests_LzipVarTestSuite, suiteDescription_LzipVarTestSuite, 16, "testVariableLength" ) {}
 void runTest() { suite_LzipVarTestSuite.testVariableLength(); }
} testDescription_suite_LzipVarTestSuite_testVariableLength;

static class TestDescription_suite_LzipVarTestSuite_testCopyAndAssignment : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_LzipVarTestSuite_testCopyAndAssignment() : CxxTest::RealTestDescription( Tests_LzipVarTestSuite, suiteDescription_LzipVarTestSuite, 17, "testCopyAndAssignment" ) {}
 void runTest() { suite_LzipVarTestSuite.testCopyAndAssignment(); }
} testDescription_suite_LzipVarTestSuite_testCopyAndAssignment;

static class TestDescription_suite_LzipVarTestSuite_testCompressVar : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_LzipVarTestSuite_testCompressVar() : CxxTest::RealTestDescription( Tests_LzipVarTestSuite, suiteDescription_LzipVarTestSuite, 18, "testCompressVar" ) {}
 void runTest() { suite_LzipVarTestSuite.testCompressVar(); }
} testDescription_suite_LzipVarTestSuite_testCompressVar;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

#ifndef __RUNUTIL_H_
#define __RUNUTIL_H_

#include <QCoreApplication>
#include <QProcess>
#include <QList>
#include <QMap>

class QProcess;
class QFile;

class RunApplication : public QCoreApplication
{
    Q_OBJECT
public:
    RunApplication(int argc, char *argv[]);
    ~RunApplication();

private slots:
    void finished(int exitCode, QProcess::ExitStatus exitStatus);
    void output();
    void error();

private:
    QList<QProcess *> tests;
    QList<QProcess *> supports;
    QMap<QProcess *, QFile *> logfiles;

};

#endif
#include <QSettings>
#include <QStringList>
#include <QProcessEnvironment>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QDebug>
#include "runutil.h"

void readSettings(QSettings &settings, QList<QProcess *> &cont, const char *typ)
{
    const int sz = settings.beginReadArray(typ);
    for (int idx = 0; idx < sz; ++idx)
    {
        settings.setArrayIndex(idx);
        const QString cmdstr = settings.value("exec").toString();
        QStringList args = cmdstr.split(' ', QString::SkipEmptyParts);
        // first pass to clear quoted arguments
        QStringList::iterator quoted_arg = args.end();
        for (QStringList::iterator it = args.begin(); it != args.end();)
        {
            if (quoted_arg == args.end())
            {
                if ((*it).at(0) == '\"') quoted_arg = it;
                ++it;
            }
            else
            {
                bool endquote = (*it).at((*it).size() - 1) == '\"';
                if (endquote && (*it).size() > 1 && (*it).at((*it).size() - 2) == '\\')
                    endquote = false;
                (*quoted_arg) += *it;
                it = args.erase(it);
                if (endquote) quoted_arg = args.end();
            }
        }

        if (!args.empty())
        {
            QProcess *proc = new QProcess;
            proc->setProgram(args.takeFirst());
            if (!args.empty()) proc->setArguments(args);
            const QString wdir = settings.value("working").toString();
            if (!wdir.isEmpty()) proc->setWorkingDirectory(wdir);
            QString path = settings.value("path").toString();
            if (!path.isEmpty())
            {
#ifdef WIN32
                const QChar sep = ';';
#else
                const QChar sep = ':';
#endif
                QStringList pathlist = path.split(';', QString::SkipEmptyParts);
                for (QStringList::iterator it = pathlist.begin(); it != pathlist.end(); ++it)
                {
                    const QDir dir(*it);
                    if (dir.isRelative()) *it = QDir::toNativeSeparators(dir.absolutePath());
                }
                path = pathlist.join(sep);

#if QT_VERSION >= 0x040600
                QProcessEnvironment qpe = QProcessEnvironment::systemEnvironment();
#else
                QProcessEnvironment qpe = QProcess::systemEnvironment();
#endif
                const QString qpepath = qpe.value("PATH");
                if (qpepath.isEmpty()) qpe.insert("PATH", path);
                else {
                    qpe.remove("PATH");
                    path += sep;
                    path += qpepath;
                    qpe.insert("PATH", path);
                }
                proc->setProcessEnvironment(qpe);
            }
            cont.push_back(proc);
        }
    }
    settings.endArray();
}

static QString get_ini(int argc, char *argv[])
{
    for (int i = 1; i < argc; ++i)
    {
        QString arg(argv[i]);
        if (arg.endsWith(".ini") && QFile::exists(arg))
        {
            arg = QDir::fromNativeSeparators(arg);
            const int slash = arg.lastIndexOf('/');
            if (slash > -1) {
                QDir::setCurrent(arg.left(slash + 1));
                return arg.mid(slash + 1);
            }
            else return arg;
        }
    }
    return "run.ini";
}

static QString get_logfile(int argc, char *argv[])
{
    for (int i = 1; i < argc; ++i)
    {
        QString arg(argv[i]);
        if (!arg.endsWith(".ini"))
            return arg;
    }
    return QString::null;
}

RunApplication::RunApplication(int argc, char *argv[]) : QCoreApplication(argc, argv)
{
    const QString logfilename = get_logfile(argc, argv);
    const bool wildcard = logfilename.contains("%1");
    const QDir curr = QDir::current();

    QSettings settings(get_ini(argc, argv), QSettings::IniFormat);
    readSettings(settings, tests, "test");
    readSettings(settings, supports, "support");
    for (QList<QProcess *>::iterator it = supports.begin(); it != supports.end(); ++it)
    {
        connect((*it), SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(finished(int, QProcess::ExitStatus)));
        connect((*it), SIGNAL(readyReadStandardOutput()), this, SLOT(output()));
        connect((*it), SIGNAL(readyReadStandardError()), this, SLOT(error()));
        (*it)->start();
    }
    for (QList<QProcess *>::iterator it = supports.begin(); it != supports.end(); ++it)
    {
        (*it)->waitForStarted();
    }
    int idx = 0;
    for (QList<QProcess *>::iterator it = tests.begin(); it != tests.end(); ++it, ++idx)
    {
        if (!logfilename.isEmpty()) {
            QFileInfo info(curr, (wildcard) ? logfilename.arg(idx) : logfilename);
            QFile *file = new QFile(info.absoluteFilePath());
            if (file && file->open(QIODevice::WriteOnly))
                logfiles[*it] = file;
            else delete file;
        }

        connect((*it), SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(finished(int, QProcess::ExitStatus)));
        connect((*it), SIGNAL(readyReadStandardOutput()), this, SLOT(output()));
        connect((*it), SIGNAL(readyReadStandardError()), this, SLOT(error()));
        (*it)->start();
    }
}

RunApplication::~RunApplication()
{
    for (QList<QProcess *>::iterator it = tests.begin(); it != tests.end();)
    {
        if ((*it)->state() == QProcess::Running) {
            (*it)->terminate();
            ++it;
        }
        else {
            delete *it;
            it = tests.erase(it);
        }
    }
    for (int cnt = 0; cnt < 3; ++cnt)
    {
        for (QList<QProcess *>::iterator it = tests.begin(); it != tests.end();)
        {
            if ((*it)->waitForFinished(200))
            {
                delete *it;
                it = tests.erase(it);
            }
            else ++it;
        }
    }
    for (QList<QProcess *>::iterator it = tests.begin(); it != tests.end(); ++it)
        (*it)->kill();
    for (int cnt = 0; cnt < 5; ++cnt)
    {
        for (QList<QProcess *>::iterator it = tests.begin(); it != tests.end();)
        {
            if ((*it)->waitForFinished(500))
            {
                delete *it;
                it = tests.erase(it);
            }
            else ++it;
        }
    }

    for (QList<QProcess *>::iterator it = supports.begin(); it != supports.end();)
    {
        if ((*it)->state() == QProcess::Running) {
            (*it)->terminate();
            ++it;
        }
        else {
            delete *it;
            it = supports.erase(it);
        }
    }
    for (int cnt = 0; cnt < 3; ++cnt)
    {
        for (QList<QProcess *>::iterator it = supports.begin(); it != supports.end();)
        {
            if ((*it)->waitForFinished(200))
            {
                delete *it;
                it = supports.erase(it);
            }
            else ++it;
        }
    }
    for (QList<QProcess *>::iterator it = supports.begin(); it != supports.end(); ++it)
        (*it)->kill();
    for (int cnt = 0; cnt < 5; ++cnt)
    {
        for (QList<QProcess *>::iterator it = supports.begin(); it != supports.end();)
        {
            if ((*it)->waitForFinished(500))
            {
                delete *it;
                it = supports.erase(it);
            }
            else ++it;
        }
    }

    qDeleteAll(tests);
    qDeleteAll(supports);

    Q_FOREACH(QFile *f, logfiles)
    {
        f->close();
        delete f;
    }
}

void RunApplication::output()
{
    QProcess *proc = (QProcess *)sender();
    QMap<QProcess *, QFile *>::iterator it = logfiles.find(proc);

    if (it != logfiles.end())
        it.value()->write(proc->readAllStandardOutput());
    else
        qDebug().noquote() << proc->readAllStandardOutput();
}

void RunApplication::error()
{
    QProcess *proc = (QProcess *)sender();
    qDebug().noquote() << proc->readAllStandardError();
}

void RunApplication::finished(int exitCode, QProcess::ExitStatus exitStatus)
{
    QProcess *proc = (QProcess *)sender();
    qDebug() << proc->program() << ((exitStatus == 0) ? " exited normally, code:" : " crashed, code:") << exitCode;
    if (tests.contains(proc))
    {
        QList<QProcess *>::iterator it;
        for (it = tests.begin(); it != tests.end(); ++it)
            if ((*it)->state() == QProcess::Running) break;
        if (it == tests.end()) exit();
    }
}

int main(int argc, char *argv[])
{
    RunApplication app(argc, argv);
    return app.exec();
}

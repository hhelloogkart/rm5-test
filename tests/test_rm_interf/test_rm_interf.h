#ifndef _TEST_RM_VAR_H_DEF_
#define _TEST_RM_VAR_H_DEF_

#include <cxxtest/TestSuite.h>
#include <cxxtest/GlobalFixture.h>

class rmclient_init;

class RmVarTestSuiteFixture : public CxxTest::GlobalFixture
{
public:
    bool setUpWorld();
    bool tearDownWorld();
};

class client1 : public CxxTest::TestSuite
{
public:
    void testRttiGetNext();
    void testNorminal();
};

class client2 : public CxxTest::TestSuite
{
public:
    void testNorminal();
};

#endif

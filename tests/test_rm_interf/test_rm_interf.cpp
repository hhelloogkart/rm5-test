#include <buf_var.h>
#include <val_var.h>
#include <rmmsg_var.h>
#include <rmdata_var.h>
#include <rminterf.h>
#include <arry_var.h>
#include <defutil.h>
#include <defaults.h>
#include <mpt_show.h>
#include "test_rm_interf.h"
#include <time/timecore.h>
#include <random>
#include <set>
#include <string>
#include <sstream>
#include <string.h>
#include <stdio.h>

class Rmv1 : public rm_var
{
public:
    RMCONST(Rmv1,"TEST1",true)
    array_var<unsigned int> data;
};
class Rmv2 : public rm_var
{
public:
    RMCONST(Rmv2, "TEST2", true)
    array_var<unsigned int> data;
};
class Rmv3 : public rm_message_var
{
public:
    RMMSGCONST(Rmv3, "TEST3", true)
    array_var<unsigned int> data;
};
class Rmv4 : public rm_message_var
{
public:
    RMMSGCONST(Rmv4, "TEST4", true)
    array_var<unsigned int> data;
};
class Rmv5 : public rm_data_var
{
public:
    RMDATACONST(Rmv5, "TEST5", true)
    array_var<unsigned int> data;
};
class Rmv6 : public rm_data_var
{
public:
    RMDATACONST(Rmv6, "TEST6", true)
    array_var<unsigned int> data;
};
class Rma1 : public rm_var
{
public:
    RMCONST_ARRAY(Rma1, "ARRAY1_%d", true)
    array_var<unsigned int> data;
};
class Rma2 : public rm_var
{
public:
    RMCONST_ARRAY(Rma2, "ARRAY2_%d", true)
    array_var<unsigned int> data;
};
class Rma3 : public rm_message_var
{
public:
    RMMSGCONST_ARRAY(Rma3, "ARRAY3_%d", true)
    array_var<unsigned int> data;
};
class Rma4 : public rm_message_var
{
public:
    RMMSGCONST_ARRAY(Rma4, "ARRAY4_%d", true)
    array_var<unsigned int> data;
};
class Rma5 : public rm_data_var
{
public:
    RMDATACONST_ARRAY(Rma5, "ARRAY5_%d", true)
    array_var<unsigned int> data;
};
class Rma6 : public rm_data_var
{
public:
    RMDATACONST_ARRAY(Rma6, "ARRAY6_%d", true)
    array_var<unsigned int> data;
};
class ClientStatus : public rm_var
{
public:
    RMCONST(ClientStatus, "CLIENT_STATUS", true)
    int extract(int len, const unsigned char* buf);
    generic_var<int> state;
    void addhit(mpt_show *shw, int hits);
private:
    std::map<mpt_show *, int> hitmap;
};

int ClientStatus::extract(int len, const unsigned char* buf)
{
    const int ret = complex_var::extract(len, buf);
    printf("Status: %d\n", state.to_int());
    return ret;
}

void ClientStatus::addhit(mpt_show *shw, int hits)
{
    std::map<mpt_show *, int>::iterator it = hitmap.find(shw);
    if (it == hitmap.end())
    {
        hitmap[shw] = hits;
    }
    else
    {
        (*it).second += hits;
        hits = (*it).second;
    }
    for (it = hitmap.begin(); it != hitmap.end(); ++it)
        if ((*it).second != hits) break;
    if (it == hitmap.end())
        state = hits;
}

class Connection1 : public rm_interface
{
public:
    RMINTERFACECONST(Connection1)
    Rmv1 rmv1;
    Rmv2 rmv2;
    Rmv3 rmv3;
    Rmv4 rmv4;
    Rmv5 rmv5;
    Rmv6 rmv6;
    Rma1 arry1[10];
    Rma3 arry3[10];
    Rma5 arry5[10];
    buf_var bv1{"BUFFER1", true};
    buf_var bv3{"BUFFER3", true};
    valid_var vv1{"VALID1", true};
    valid_var vv3{"VALID3", true};
    ClientStatus client1;
} interface1;

class Connection2 : public rm_interface
{
public:
    RMINTERFACECONST(Connection2)
    Rmv1 rmv1;
    Rmv2 rmv2;
    Rmv3 rmv3;
    Rmv4 rmv4;
    Rmv5 rmv5;
    Rmv6 rmv6;
    Rma2 arry2[10];
    Rma4 arry4[10];
    Rma6 arry6[10];
    buf_var bv2{"BUFFER2", true};
    buf_var bv4{"BUFFER4", true};
    valid_var vv2 {"VALID2", true};
    valid_var vv4 {"VALID4", true};
    ClientStatus client2;
} interface2;

static timeElapsed te;
static RmVarTestSuiteFixture fixture;
static rmclient_init *init = 0;
static int tries = 100;
static int cycle = 400;
static int timeout = 1500;

bool RmVarTestSuiteFixture::setUpWorld()
{
    IOSL_INIT("iosl.cfg");
    init = new rmclient_init("+test_rm_interf.cfg");
    init->client_defaults()->getint("tries", &tries);
    init->client_defaults()->getint("cycle", &cycle);
    static_cast<defaults *>(*init)->getint("timeout", &timeout);
    return init != 0;
}

bool RmVarTestSuiteFixture::tearDownWorld()
{
    delete init;
    return true;
}

inline bool chk_avi(array_var<unsigned int> &data, std::mt19937 &r)
{
    std::uniform_int_distribution<int> d(1, 61);
    const int sz = d(r);
    TS_ASSERT_EQUALS(sz, data.cardinal());
    for (int i = 0; i < data.cardinal(); ++i)
    {
        const unsigned int val = r();
        TS_ASSERT_EQUALS(data.to_unsigned_int(i), val);
        if (val != data.to_unsigned_int(i))
            return false;
    }
    return true;
}

class avi_show : public mpt_autoshow
{
public:
    avi_show(array_var<unsigned int> *var_, std::mt19937 &r_, int id_, int client);
    void operator()();

private:
    array_var<unsigned int> *var;
    std::mt19937 &r;
    int count;
    int id;
    ClientStatus &notify;
};

avi_show::avi_show(array_var<unsigned int>* var_, std::mt19937 & r_, int id_, int client) :
    var(var_), r(r_), count(0), id(id_), notify((client == 1) ? interface1.client1 : interface2.client2)
{
    addCallback(var_);
}

void avi_show::operator()()
{
    if (dirty)
    {
        dirty = false;
        if (!chk_avi(*var, r))
        {
            printf("Count %d, id %d: Fail\n", count, id);
        }
        else
        {
            printf("Count %d, id %d: OK\n", count, id);
        }
        ++count;
        te();
        notify.addhit(this, 1);
    }
}

class buf_show : public mpt_autoshow
{
public:
    buf_show(buf_var *var_, std::mt19937 &r_, int id_, int client);
    void operator()();

private:
    buf_var *var;
    std::mt19937 &r;
    int count;
    int id;
    ClientStatus &notify;
};

buf_show::buf_show(buf_var* var_, std::mt19937 & r_, int id_, int client) :
    var(var_), r(r_), count(0), id(id_), notify((client == 1) ? interface1.client1 : interface2.client2)
{
    addCallback(var_);
}

void buf_show::operator()()
{
    if (dirty)
    {
        dirty = false;
        std::uniform_int_distribution<int> d(1, 60);
        const unsigned int sz = d(r);
        TS_ASSERT_EQUALS(var->size() / 4, sz);
        bool fail = (var->size() / 4 != sz);
        const unsigned int *data = reinterpret_cast<const unsigned int *>(var->getBuffer());
        for (int i = 0; i < sz; ++i)
        {
            const unsigned int val = r();
            TS_ASSERT_EQUALS(data[i], val);
            fail |= (data[i] != val);
        }
        if (fail)
        {
            printf("Count %d, id %d: Fail\n", count, id);
        }
        else
        {
            printf("Count %d, id %d: OK\n", count, id);
        }
        ++count;
        te();
        notify.addhit(this, 1);
    }
}

class val_show : public mpt_autoshow
{
public:
    val_show(valid_var *var_, std::mt19937 &r_, int id_, int client);
    void operator()();
    void addElement();
    void setElement(unsigned int obj);
    void clrElement(unsigned int obj);
    bool chkElement(unsigned int obj) const;
    int getCount() const;

private:
    valid_var *var;
    std::mt19937 &r;
    int count;
    int id;
    std::set<unsigned int, std::less<unsigned int> > mirror;
    ClientStatus &notify;
};

val_show::val_show(valid_var* var_, std::mt19937 & r_, int id_, int client) :
    var(var_), r(r_), count(0), id(id_), notify((client == 1) ? interface1.client1 : interface2.client2)
{
    addCallback(var_);
}

void val_show::operator()()
{
    if (dirty)
    {
        dirty = false;
        std::uniform_int_distribution<int> d(1, 30);
        const unsigned int obj = d(r);
        if (obj > 26)
            addElement();
        else if (chkElement(obj))
            clrElement(obj);
        else setElement(obj);
        TS_ASSERT_EQUALS(static_cast<unsigned int>(var->size()) / 4U, mirror.size());
        bool fail = (var->size() / 4 != mirror.size());
        if (mirror.empty())
        {
            for (int i = 0; i < 20; ++i) {
                TS_ASSERT_EQUALS(var->chkElement(i), false);
                fail |= (var->chkElement(i) != chkElement(i));
            }
        }
        else
        {
            std::set<unsigned int>::reverse_iterator it = mirror.rbegin();
            const unsigned int sz = (*it) + 5U;
            for (int i = 0; i < sz; ++i) {
                TS_ASSERT_EQUALS(var->chkElement(i), chkElement(i));
                fail |= (var->chkElement(i) != chkElement(i));
            }
        }
        if (fail)
        {
            printf("Count %d, id %d: Fail\n", count, id);
        }
        else
        {
            printf("Count %d, id %d: OK\n", count, id);
        }
        ++count;
        te();
        notify.addhit(this, 1);
    }
}

void val_show::addElement()
{
    unsigned int obj = 0;
    while (chkElement(obj)) ++obj;
    setElement(obj);
}

void val_show::setElement(unsigned int obj)
{
    mirror.insert(obj);
}

void val_show::clrElement(unsigned int obj)
{
    mirror.erase(obj);
}

bool val_show::chkElement(unsigned int obj) const
{
    return mirror.find(obj) != mirror.cend();
}

int val_show::getCount() const
{
    return count;
}

inline void pop_avi(array_var<unsigned int> &data, std::mt19937 &r)
{
    // 256 bytes - 12 bytes overhead = 244 (61 ints)
    std::uniform_int_distribution<int> d(1, 61);
    const int sz = d(r);
    data.resize(sz);
    for (int i = 0; i < sz; ++i)
        data.fromValue(i, r());
}

inline void pop_buf(buf_var &data, std::mt19937 &r)
{
    std::uniform_int_distribution<int> d(1, 60);
    const unsigned int sz = d(r);
    if (sz < 30)
    {
        data.set(sz);
        for (int i = 0; i < sz; ++i)
            data[i] = r();
        data.setRMDirty();
    }
    else
    {
        unsigned int *buf = new unsigned int[sz];
        for (int i = 0; i < sz; ++i)
            buf[i] = r();
        data.setBuffer(buf, sz * 4);
        delete[] buf;;
    }
}

inline void pop_val(valid_var &data, std::mt19937 &r)
{
    std::uniform_int_distribution<int> d(1, 30);
    const unsigned int obj = d(r);
    if (obj > 26)
        data.addElement();
    else if (data.chkElement(obj))
        data.clrElement(obj);
    else data.setElement(obj);
}

void client1::testRttiGetNext()
{
    TS_ASSERT_EQUALS(interface1.arry1[0].getNext(), &interface1.arry1[1]);
    TS_ASSERT_EQUALS(interface1.arry1[1].getNext(), &interface1.arry1[2]);
    TS_ASSERT_EQUALS(interface1.arry1[2].getNext(), &interface1.arry1[3]);
    TS_ASSERT_EQUALS(interface1.arry1[3].getNext(), &interface1.arry1[4]);
    TS_ASSERT_EQUALS(interface1.arry1[4].getNext(), &interface1.arry1[5]);

    TS_ASSERT_EQUALS(interface2.arry2[0].getNext(), &interface2.arry2[1]);
    TS_ASSERT_EQUALS(interface2.arry2[1].getNext(), &interface2.arry2[2]);
    TS_ASSERT_EQUALS(interface2.arry2[2].getNext(), &interface2.arry2[3]);
    TS_ASSERT_EQUALS(interface2.arry2[3].getNext(), &interface2.arry2[4]);
    TS_ASSERT_EQUALS(interface2.arry2[4].getNext(), &interface2.arry2[5]);

    TS_ASSERT_EQUALS(interface1.arry1[0].rtti(), interface1.arry1[1].rtti());
    TS_ASSERT_DIFFERS(interface1.arry1[0].rtti(), interface1.arry3[0].rtti());
    TS_ASSERT_EQUALS(interface1.rmv1.rtti(), interface1.rmv1.rtti());
    TS_ASSERT_DIFFERS(interface1.arry3[0].rtti(), interface2.arry4[0].rtti());
    TS_ASSERT_EQUALS(interface2.arry4[0].rtti(), interface2.arry4[1].rtti());
    TS_ASSERT_DIFFERS(interface1.arry5[0].rtti(), interface2.arry6[0].rtti());
    TS_ASSERT_EQUALS(interface1.bv1.rtti(), interface2.bv2.rtti());
    TS_ASSERT_EQUALS(interface1.vv1.rtti(), interface2.vv2.rtti());
    TS_ASSERT_DIFFERS(interface1.rtti(), interface2.rtti());
}

void client1::testNorminal(void)
{
    std::mt19937 r1(1);
    std::mt19937 r2(10);
    std::mt19937 r3(100);
    std::mt19937 r4[10];
    std::mt19937 r5[10];
    std::mt19937 r6[10];
    std::mt19937 r7(500);
    std::mt19937 r8(1000);
    std::mt19937 r9(2000);
    std::mt19937 r10(3001);
    for (int i = 0; i < 10; ++i)
    {
        r4[i].seed(200 + i);
        r5[i].seed(300 + i);
        r6[i].seed(400 + i);
    }

    std::mt19937 r11(3);
    std::mt19937 r12(13);
    std::mt19937 r13(103);
    std::mt19937 r14[10];
    std::mt19937 r15[10];
    std::mt19937 r16[10];
    std::mt19937 r17(503);
    std::mt19937 r18(1003);
    std::mt19937 r19(2003);
    std::mt19937 r20(3004);
    std::mt19937 r21(4005);
    std::mt19937 r22(5006);
    std::mt19937 r23(6007);
    std::mt19937 r24(7008);
    std::mt19937 r25(8009);
    std::mt19937 r26(9010);
    for (int i = 0; i < 10; ++i)
    {
        r14[i].seed(4000 + i);
        r15[i].seed(4100 + i);
        r16[i].seed(4200 + i);
    }

    avi_show s1_1(&interface1.rmv1.data, r1, 0, 1);
    avi_show s1_2(&interface1.rmv3.data, r2, 1, 1);
    avi_show s1_3(&interface1.rmv5.data, r3, 2, 1);
    avi_show *s1_4[10];
    avi_show *s1_5[10];
    avi_show *s1_6[10];
    for (int i = 0; i < 10; ++i)
    {
        s1_4[i] = new avi_show(&(interface1.arry1[i].data), r4[i], 10 + i, 1);
        s1_5[i] = new avi_show(&(interface2.arry2[i].data), r5[i], 20 + i, 1);
        s1_6[i] = new avi_show(&(interface1.arry3[i].data), r6[i], 30 + i, 1);
    }
    buf_show s1_7(&interface1.bv1, r7, 4, 1);
    buf_show s1_8(&interface2.bv2, r8, 5, 1);
    val_show s1_9(&interface1.vv1, r9, 6, 1);
    val_show s1_10(&interface2.vv2, r10, 7, 1);
    avi_show s1_11(&interface2.rmv1.data, r21, 8, 1);
    avi_show s1_12(&interface2.rmv2.data, r22, 9, 1);
    avi_show s1_13(&interface2.rmv3.data, r23, 81, 1);

    interface1.client1.addhit(&s1_1, 0);
    interface1.client1.addhit(&s1_2, 1);
    interface1.client1.addhit(&s1_3, 0);
    for (int i = 0; i < 10; ++i)
    {
        interface1.client1.addhit(s1_4[i], 0);
        interface1.client1.addhit(s1_5[i], 0);
        interface1.client1.addhit(s1_6[i], 1);
    }
    interface1.client1.addhit(&s1_7, 0);
    interface1.client1.addhit(&s1_8, 0);
    interface1.client1.addhit(&s1_9, 0);
    interface1.client1.addhit(&s1_10, 0);
    interface1.client1.addhit(&s1_11, 0);
    interface1.client1.addhit(&s1_12, 0);
    interface1.client1.addhit(&s1_13, 1);

    for (int cnt = 0; cnt < 10; ++cnt)
    {
        init->incoming();
        init->outgoing();
        millisleep(cycle);
    }
    if (te.isValid()) te();

    int count = 0;
    while (!init->client_exit() && (!te.isValid() || te.elapsed() < timeout))
    {
        init->incoming();
        mpt_show::refresh();
        if (count <= interface2.client2.state.to_int() && count < tries)
        {
            pop_avi(interface1.rmv2.data, r11);
            if (count > 0) pop_avi(interface1.rmv4.data, r12);
            pop_avi(interface1.rmv6.data, r13);
            for (int i = 0; i < 10; ++i)
            {
                if (count > 0) pop_avi(interface2.arry4[i].data, r14[i]);
                pop_avi(interface1.arry5[i].data, r15[i]);
                pop_avi(interface2.arry6[i].data, r16[i]);
            }
            pop_buf(interface1.bv3, r17);
            pop_buf(interface2.bv4, r18);
            pop_val(interface1.vv3, r19);
            pop_val(interface2.vv4, r20);
            if (count > 0) pop_avi(interface2.rmv4.data, r24);
            pop_avi(interface2.rmv5.data, r25);
            pop_avi(interface2.rmv6.data, r26);
            if (te.isValid()) te();
            ++count;
        }
        init->outgoing();
    }
    for (int i = 0; i < 10; ++i)
    {
        delete s1_4[i];
        delete s1_5[i];
        delete s1_6[i];
    }
}

void client2::testNorminal()
{
    std::mt19937 r1(1);
    std::mt19937 r2(10);
    std::mt19937 r3(100);
    std::mt19937 r4[10];
    std::mt19937 r5[10];
    std::mt19937 r6[10];
    std::mt19937 r7(500);
    std::mt19937 r8(1000);
    std::mt19937 r9(2000);
    std::mt19937 r10(3001);
    for (int i = 0; i < 10; ++i)
    {
        r4[i].seed(200 + i);
        r5[i].seed(300 + i);
        r6[i].seed(400 + i);
    }

    std::mt19937 r11(3);
    std::mt19937 r12(13);
    std::mt19937 r13(103);
    std::mt19937 r14[10];
    std::mt19937 r15[10];
    std::mt19937 r16[10];
    std::mt19937 r17(503);
    std::mt19937 r18(1003);
    std::mt19937 r19(2003);
    std::mt19937 r20(3004);
    std::mt19937 r21(4005);
    std::mt19937 r22(5006);
    std::mt19937 r23(6007);
    std::mt19937 r24(7008);
    std::mt19937 r25(8009);
    std::mt19937 r26(9010);
    for (int i = 0; i < 10; ++i)
    {
        r14[i].seed(4000 + i);
        r15[i].seed(4100 + i);
        r16[i].seed(4200 + i);
    }

    avi_show s1_1(&interface1.rmv2.data, r11, 40, 2);
    avi_show s1_2(&interface1.rmv4.data, r12, 41, 2);
    avi_show s1_3(&interface1.rmv6.data, r13, 42, 2);
    avi_show *s1_4[10];
    avi_show *s1_5[10];
    avi_show *s1_6[10];
    for (int i = 0; i < 10; ++i)
    {
        s1_4[i] = new avi_show(&(interface2.arry4[i].data), r14[i], 50 + i, 2);
        s1_5[i] = new avi_show(&(interface1.arry5[i].data), r15[i], 60 + i, 2);
        s1_6[i] = new avi_show(&(interface2.arry6[i].data), r16[i], 70 + i, 2);
    }
    buf_show s1_7(&interface1.bv3, r17, 44, 2);
    buf_show s1_8(&interface2.bv4, r18, 45, 2);
    val_show s1_9(&interface1.vv3, r19, 46, 2);
    val_show s1_10(&interface2.vv4, r20, 47, 2);
    avi_show s1_11(&interface2.rmv4.data, r24, 48, 2);
    avi_show s1_12(&interface2.rmv5.data, r25, 49, 2);
    avi_show s1_13(&interface2.rmv6.data, r26, 80, 2);

    interface2.client2.addhit(&s1_1, 0);
    interface2.client2.addhit(&s1_2, 1);
    interface2.client2.addhit(&s1_3, 0);
    for (int i = 0; i < 10; ++i)
    {
        interface2.client2.addhit(s1_4[i], 1);
        interface2.client2.addhit(s1_5[i], 0);
        interface2.client2.addhit(s1_6[i], 0);
    }
    interface2.client2.addhit(&s1_7, 0);
    interface2.client2.addhit(&s1_8, 0);
    interface2.client2.addhit(&s1_9, 0);
    interface2.client2.addhit(&s1_10, 0);
    interface2.client2.addhit(&s1_11, 1);
    interface2.client2.addhit(&s1_12, 0);
    interface2.client2.addhit(&s1_13, 0);

    for (int cnt = 0; cnt < 10; ++cnt)
    {
        init->incoming();
        init->outgoing();
        millisleep(cycle);
    }
    if (te.isValid()) te();

    int count = 0;
    while (!init->client_exit() && (!te.isValid() || te.elapsed() < timeout))
    {
        init->incoming();
        mpt_show::refresh();
        if (count <= interface1.client1.state.to_int() && count < tries)
        {
            pop_avi(interface1.rmv1.data, r1);
            if (count > 0) pop_avi(interface1.rmv3.data, r2);
            pop_avi(interface1.rmv5.data, r3);
            for (int i = 0; i < 10; ++i)
            {
                pop_avi(interface1.arry1[i].data, r4[i]);
                pop_avi(interface2.arry2[i].data, r5[i]);
                if (count > 0) pop_avi(interface1.arry3[i].data, r6[i]);
            }
            pop_buf(interface1.bv1, r7);
            pop_buf(interface2.bv2, r8);
            pop_val(interface1.vv1, r9);
            pop_val(interface2.vv2, r10);
            pop_avi(interface2.rmv1.data, r21);
            pop_avi(interface2.rmv2.data, r22);
            if (count > 0) pop_avi(interface2.rmv3.data, r23);
            if (te.isValid()) te();
            ++count;
        }
        init->outgoing();
    }
    for (int i = 0; i < 10; ++i)
    {
        delete s1_4[i];
        delete s1_5[i];
        delete s1_6[i];
    }
    TS_TRACE("End");
}

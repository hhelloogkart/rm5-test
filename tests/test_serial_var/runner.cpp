/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_serial_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_SerialVarTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_serial_var\test_serial_var.h"

static SerialVarTestSuite suite_SerialVarTestSuite;

static CxxTest::List Tests_SerialVarTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_SerialVarTestSuite( "test_serial_var/test_serial_var.h", 16, "SerialVarTestSuite", suite_SerialVarTestSuite, Tests_SerialVarTestSuite );

static class TestDescription_suite_SerialVarTestSuite_testRecv : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SerialVarTestSuite_testRecv() : CxxTest::RealTestDescription( Tests_SerialVarTestSuite, suiteDescription_SerialVarTestSuite, 19, "testRecv" ) {}
 void runTest() { suite_SerialVarTestSuite.testRecv(); }
} testDescription_suite_SerialVarTestSuite_testRecv;

static class TestDescription_suite_SerialVarTestSuite_testSend : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_SerialVarTestSuite_testSend() : CxxTest::RealTestDescription( Tests_SerialVarTestSuite, suiteDescription_SerialVarTestSuite, 20, "testSend" ) {}
 void runTest() { suite_SerialVarTestSuite.testSend(); }
} testDescription_suite_SerialVarTestSuite_testSend;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

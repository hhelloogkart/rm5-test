#ifndef _TEST_SERIAL_VAR_H_DEF_
#define _TEST_SERIAL_VAR_H_DEF_

#include <cxxtest/TestSuite.h>
#include <cxxtest/GlobalFixture.h>

class rmclient_init;

class SerialVarTestSuiteFixture : public CxxTest::GlobalFixture
{
public:
    bool setUpWorld();
    bool tearDownWorld();
};

class SerialVarTestSuite : public CxxTest::TestSuite
{
public:
    void testRecv();
    void testSend();
};

#endif

#include <serial_var.h>
#include <arry_var.h>
#include <str_var.h>
#include <chksum_var.h>
#include <defutil.h>
#include <defaults.h>
#include <mpt_show.h>
#include "test_serial_var.h"
#include <time/timecore.h>
#include <random>
#include <string.h>
#include <stdio.h>

static timeElapsed te;

class avi_typ : public chksum32le_var
{
public:
    CHKSUMCONST(avi_typ, chksum32le_var)
    array_var<unsigned int> data;
};

class str1_typ : public chksum32le_var
{
public:
    CHKSUMCONST(str1_typ, chksum32le_var)
    string_var<41> data;
};

class str2_typ : public chksum32le_var
{
public:
    CHKSUMCONST(str2_typ, chksum32le_var)
    string_var<51> data;
};

class str3_typ : public chksum32le_var
{
public:
    CHKSUMCONST(str3_typ, chksum32le_var)
    string_var<61> data;
};

class str4_typ : public chksum32le_var
{
public:
    CHKSUMCONST(str4_typ, chksum32le_var)
    string_var<71> data;
};

class str5_typ : public chksum32le_var
{
public:
    CHKSUMCONST(str5_typ, chksum32le_var)
    string_var<81> data;
};

class str6_typ : public chksum32le_var
{
public:
    CHKSUMCONST(str6_typ, chksum32le_var)
    string_var<91> data;
};

/* Embedded length width 4 */
class var_msg_1_1 : public serial_var
{
public:
    SERIALCONST(var_msg_1_1, "\xA0\x00", 2, 0, 2, 4, 0, 0, 0, 6, 0, 0, 0, 0)
    avi_typ chk;
} msg_1_1;

class var_msg_1_2 : public serial_var
{
public:
    SERIALCONST(var_msg_1_2, "\xA0\x02", 2, 0, 2, 4, 0, 0, 0, 6, 0, 0, 0, 0)
    avi_typ chk;
} msg_1_2;

class var_msg_1_3 : public serial_var
{
public:
    SERIALCONST(var_msg_1_3, "\xA0\x04", 2, 0, 2, 4, 0, 0, 0, 6, 0, 0, 0, 0)
    avi_typ chk;
} msg_1_3;

class var_msg_1_4 : public serial_var
{
public:
    SERIALCONST(var_msg_1_4, "\xA0\x06", 2, 0, 2, 4, 0, 0, 0, 6, 0, 0, 0, 0)
    avi_typ chk;
} msg_1_4;

class var_msg_1_5 : public serial_var
{
public:
    SERIALCONST(var_msg_1_5, "\xA0\x37", 2, 0, 2, 4, 0, 0, 0, 6, 0, 0, 0, 0)
    avi_typ chk;
} msg_1_5;

class var_msg_1_6 : public serial_var
{
public:
    SERIALCONST(var_msg_1_6, "\xA0\x08", 2, 0, 2, 4, 0, 0, 0, 6, 0, 0, 0, 0)
    avi_typ chk;
} msg_1_6;

class var_msg_1_7 : public serial_var
{
public:
    SERIALCONST(var_msg_1_7, "\xA0\x09", 2, 0, 2, 4, 0, 0, 0, 6, 0, 0, 0, 0)
    avi_typ chk;
} msg_1_7;

class var_msg_1_8 : public serial_var
{
public:
    SERIALCONST(var_msg_1_8, "\xA0\x0A", 2, 0, 2, 4, 0, 0, 0, 6, 0, 0, 0, 0)
    avi_typ chk;
} msg_1_8;

class var_msg_1_9 : public serial_var
{
public:
    SERIALCONST(var_msg_1_9, "\xA0\x1D", 2, 0, 2, 4, 0, 0, 0, 6, 0, 0, 0, 0)
    avi_typ chk;
} msg_1_9;

class var_msg_1_10 : public serial_var
{
public:
    SERIALCONST(var_msg_1_10, "\xA0\x2F", 2, 0, 2, 4, 0, 0, 0, 6, 0, 0, 0, 0)
    avi_typ chk;
} msg_1_10;

class var_msg_1_11 : public serial_var
{
public:
    SERIALCONST(var_msg_1_11, "\xA0\x40", 2, 0, 2, 4, 0, 0, 0, 6, 0, 0, 0, 0)
    avi_typ chk;
} msg_1_11;

class var_msg_1_12 : public serial_var
{
public:
    SERIALCONST(var_msg_1_12, "\xA0\xB4", 2, 0, 2, 4, 0, 0, 0, 6, 0, 0, 0, 0)
    avi_typ chk;
} msg_1_12;

/* Embedded length width 2 offset 0 size 4 */
class var_msg_2_1 : public serial_var
{
public:
    SERIALCONST(var_msg_2_1, "\xA0\x0A", 2, 0, 2, 2, 0, 0, 0, 6, 1, 0, 0, 0)
    avi_typ chk;
} msg_2_1;

class var_msg_2_2 : public serial_var
{
public:
    SERIALCONST(var_msg_2_2, "\xA0\x0C", 2, 0, 2, 2, 0, 0, 0, 6, 1, 0, 0, 0)
    avi_typ chk;
} msg_2_2;

class var_msg_2_3 : public serial_var
{
public:
    SERIALCONST(var_msg_2_3, "\xA0\x0E", 2, 0, 2, 2, 0, 0, 0, 6, 1, 0, 0, 0)
    avi_typ chk;
} msg_2_3;

class var_msg_2_4 : public serial_var
{
public:
    SERIALCONST(var_msg_2_4, "\xA0\x10", 2, 0, 2, 2, 0, 0, 0, 6, 1, 0, 0, 0)
    avi_typ chk;
} msg_2_4;

class var_msg_2_5 : public serial_var
{
public:
    SERIALCONST(var_msg_2_5, "\xA0\x20", 2, 0, 2, 2, 0, 0, 0, 6, 1, 0, 0, 0)
    avi_typ chk;
} msg_2_5;

class var_msg_2_6 : public serial_var
{
public:
    SERIALCONST(var_msg_2_6, "\xA0\x30", 2, 0, 2, 2, 0, 0, 0, 6, 1, 0, 0, 0)
    avi_typ chk;
} msg_2_6;

/* Embedded length width 2 offset 1 size 4 bias -6 */
class var_msg_3_1 : public serial_var
{
public:
    SERIALCONST(var_msg_3_1, "\xA0\x10", 2, 0, 3, 2, -6, 0, 0, 6, 2, 0, 0, 0)
    avi_typ chk;
} msg_3_1;

class var_msg_3_2 : public serial_var
{
public:
    SERIALCONST(var_msg_3_2, "\xA0\x20", 2, 0, 3, 2, -6, 0, 0, 6, 2, 0, 0, 0)
    avi_typ chk;
} msg_3_2;

class var_msg_3_3 : public serial_var
{
public:
    SERIALCONST(var_msg_3_3, "\xA0\x30", 2, 0, 3, 2, -6, 0, 0, 6, 2, 0, 0, 0)
    avi_typ chk;
} msg_3_3;

class var_msg_3_4 : public serial_var
{
public:
    SERIALCONST(var_msg_3_4, "\xA0\x40", 2, 0, 3, 2, -6, 0, 0, 6, 2, 0, 0, 0)
    avi_typ chk;
} msg_3_4;

class var_msg_3_5 : public serial_var
{
public:
    SERIALCONST(var_msg_3_5, "\xA0\x50", 2, 0, 3, 2, -6, 0, 0, 6, 2, 0, 0, 0)
    avi_typ chk;
} msg_3_5;

class var_msg_3_6 : public serial_var
{
public:
    SERIALCONST(var_msg_3_6, "\xA0\x60", 2, 0, 3, 2, -6, 0, 0, 6, 2, 0, 0, 0)
    avi_typ chk;
} msg_3_6;

/* Embedded length width 2 offset 2 size 4 */
class var_msg_4_1 : public serial_var
{
public:
    SERIALCONST(var_msg_4_1, "\xA0\x10", 2, 0, 4, 2, 0, 0, 0, 6, 3, 0, 0, 0)
    avi_typ chk;
} msg_4_1;

class var_msg_4_2 : public serial_var
{
public:
    SERIALCONST(var_msg_4_2, "\xA0\x20", 2, 0, 4, 2, 0, 0, 0, 6, 3, 0, 0, 0)
    avi_typ chk;
} msg_4_2;

class var_msg_4_3 : public serial_var
{
public:
    SERIALCONST(var_msg_4_3, "\xA0\x30", 2, 0, 4, 2, 0, 0, 0, 6, 3, 0, 0, 0)
    avi_typ chk;
} msg_4_3;

class var_msg_4_4 : public serial_var
{
public:
    SERIALCONST(var_msg_4_4, "\xA0\x40", 2, 0, 4, 2, 0, 0, 0, 6, 3, 0, 0, 0)
    avi_typ chk;
} msg_4_4;

class var_msg_4_5 : public serial_var
{
public:
    SERIALCONST(var_msg_4_5, "\xA0\x50", 2, 0, 4, 2, 0, 0, 0, 6, 3, 0, 0, 0)
    avi_typ chk;
} msg_4_5;

class var_msg_4_6 : public serial_var
{
public:
    SERIALCONST(var_msg_4_6, "\xA0\x60", 2, 0, 4, 2, 0, 0, 0, 6, 3, 0, 0, 0)
    avi_typ chk;
} msg_4_6;

/* Embedded length width 2 bias -4 */
class var_msg_5_1 : public serial_var
{
public:
    SERIALCONST(var_msg_5_1, "\xA0\x10", 2, 0, 2, 2, -4, 0, 0, 4, 4, 0, 0, 0)
    avi_typ chk;
} msg_5_1;

class var_msg_5_2 : public serial_var
{
public:
    SERIALCONST(var_msg_5_2, "\xA0\x20", 2, 0, 2, 2, -4, 0, 0, 4, 4, 0, 0, 0)
    avi_typ chk;
} msg_5_2;

class var_msg_5_3 : public serial_var
{
public:
    SERIALCONST(var_msg_5_3, "\xA0\x30", 2, 0, 2, 2, -4, 0, 0, 4, 4, 0, 0, 0)
    avi_typ chk;
} msg_5_3;

class var_msg_5_4 : public serial_var
{
public:
    SERIALCONST(var_msg_5_4, "\xA0\x40", 2, 0, 2, 2, -4, 0, 0, 4, 4, 0, 0, 0)
    avi_typ chk;
} msg_5_4;

class var_msg_5_5 : public serial_var
{
public:
    SERIALCONST(var_msg_5_5, "\xA0\x50", 2, 0, 2, 2, -4, 0, 0, 4, 4, 0, 0, 0)
    avi_typ chk;
} msg_5_5;

class var_msg_5_6 : public serial_var
{
public:
    SERIALCONST(var_msg_5_6, "\xA0\x60", 2, 0, 2, 2, -4, 0, 0, 4, 4, 0, 0, 0)
    avi_typ chk;
} msg_5_6;

/* Embedded length width 1 offset 1 size 2 */
class var_msg_6_1 : public serial_var
{
public:
    SERIALCONST(var_msg_6_1, "\x1F\x40", 2, 0, 3, 1, 0, 0, 0, 4, 5, 0, 0, 0)
    avi_typ chk;
} msg_6_1;

class var_msg_6_2 : public serial_var
{
public:
    SERIALCONST(var_msg_6_2, "\x1F\x41", 2, 0, 3, 1, 0, 0, 0, 4, 5, 0, 0, 0)
    avi_typ chk;
} msg_6_2;

class var_msg_6_3 : public serial_var
{
public:
    SERIALCONST(var_msg_6_3, "\x1F\x42", 2, 0, 3, 1, 0, 0, 0, 4, 5, 0, 0, 0)
    avi_typ chk;
} msg_6_3;

class var_msg_6_4 : public serial_var
{
public:
    SERIALCONST(var_msg_6_4, "\x1F\x43", 2, 0, 3, 1, 0, 0, 0, 4, 5, 0, 0, 0)
    avi_typ chk;
} msg_6_4;

class var_msg_6_5 : public serial_var
{
public:
    SERIALCONST(var_msg_6_5, "\x1F\x44", 2, 0, 3, 1, 0, 0, 0, 4, 5, 0, 0, 0)
    avi_typ chk;
} msg_6_5;

class var_msg_6_6 : public serial_var
{
public:
    SERIALCONST(var_msg_6_6, "\x1F\x45", 2, 0, 3, 1, 0, 0, 0, 4, 5, 0, 0, 0)
    avi_typ chk;
} msg_6_6;

/* Embedded length width 1 offset 0 size 1 */
class var_msg_7_1 : public serial_var
{
public:
    SERIALCONST(var_msg_7_1, "\x1F\x40", 2, 0, 2, 1, 0, 0, 0, 3, 6, 0, 0, 0)
    avi_typ chk;
} msg_7_1;

class var_msg_7_2 : public serial_var
{
public:
    SERIALCONST(var_msg_7_2, "\x1F\x41", 2, 0, 2, 1, 0, 0, 0, 3, 6, 0, 0, 0)
    avi_typ chk;
} msg_7_2;

class var_msg_7_3 : public serial_var
{
public:
    SERIALCONST(var_msg_7_3, "\x1F\x42", 2, 0, 2, 1, 0, 0, 0, 3, 6, 0, 0, 0)
    avi_typ chk;
} msg_7_3;

class var_msg_7_4 : public serial_var
{
public:
    SERIALCONST(var_msg_7_4, "\x1F\x43", 2, 0, 2, 1, 0, 0, 0, 3, 6, 0, 0, 0)
    avi_typ chk;
} msg_7_4;

class var_msg_7_5 : public serial_var
{
public:
    SERIALCONST(var_msg_7_5, "\x1F\x44", 2, 0, 2, 1, 0, 0, 0, 3, 6, 0, 0, 0)
    avi_typ chk;
} msg_7_5;

class var_msg_7_6 : public serial_var
{
public:
    SERIALCONST(var_msg_7_6, "\x1F\x45", 2, 0, 2, 1, 0, 0, 0, 3, 6, 0, 0, 0)
    avi_typ chk;
} msg_7_6;

/* Embedded length width -4 */
class var_msg_8_1 : public serial_var
{
public:
    SERIALCONST(var_msg_8_1, "\x1F\x40", 2, 0, 2, -4, 0, 0, 0, 6, 7, 0, 0, 0)
    avi_typ chk;
} msg_8_1;

class var_msg_8_2 : public serial_var
{
public:
    SERIALCONST(var_msg_8_2, "\x1F\x41", 2, 0, 2, -4, 0, 0, 0, 6, 7, 0, 0, 0)
    avi_typ chk;
} msg_8_2;

class var_msg_8_3 : public serial_var
{
public:
    SERIALCONST(var_msg_8_3, "\x1F\x42", 2, 0, 2, -4, 0, 0, 0, 6, 7, 0, 0, 0)
    avi_typ chk;
} msg_8_3;

class var_msg_8_4 : public serial_var
{
public:
    SERIALCONST(var_msg_8_4, "\x1F\x43", 2, 0, 2, -4, 0, 0, 0, 6, 7, 0, 0, 0)
    avi_typ chk;
} msg_8_4;

class var_msg_8_5 : public serial_var
{
public:
    SERIALCONST(var_msg_8_5, "\x1F\x44", 2, 0, 2, -4, 0, 0, 0, 6, 7, 0, 0, 0)
    avi_typ chk;
} msg_8_5;

class var_msg_8_6 : public serial_var
{
public:
    SERIALCONST(var_msg_8_6, "\x1F\x45", 2, 0, 2, -4, 0, 0, 0, 6, 7, 0, 0, 0)
    avi_typ chk;
} msg_8_6;

/* Embedded length width -2 bias -2 */
class var_msg_9_1 : public serial_var
{
public:
    SERIALCONST(var_msg_9_1, "\x1F\x40", 2, 0, 2, -2, -2, 0, 0, 4, 8, 0, 0, 0)
    avi_typ chk;
} msg_9_1;

class var_msg_9_2 : public serial_var
{
public:
    SERIALCONST(var_msg_9_2, "\x1F\x41", 2, 0, 2, -2, -2, 0, 0, 4, 8, 0, 0, 0)
    avi_typ chk;
} msg_9_2;

class var_msg_9_3 : public serial_var
{
public:
    SERIALCONST(var_msg_9_3, "\x1F\x42", 2, 0, 2, -2, -2, 0, 0, 4, 8, 0, 0, 0)
    avi_typ chk;
} msg_9_3;

class var_msg_9_4 : public serial_var
{
public:
    SERIALCONST(var_msg_9_4, "\x1F\x43", 2, 0, 2, -2, -2, 0, 0, 4, 8, 0, 0, 0)
    avi_typ chk;
} msg_9_4;

class var_msg_9_5 : public serial_var
{
public:
    SERIALCONST(var_msg_9_5, "\x1F\x44", 2, 0, 2, -2, -2, 0, 0, 4, 8, 0, 0, 0)
    avi_typ chk;
} msg_9_5;

class var_msg_9_6 : public serial_var
{
public:
    SERIALCONST(var_msg_9_6, "\x1F\x45", 2, 0, 2, -2, -2, 0, 0, 6, 8, 0, 0, 0)
    avi_typ chk;
} msg_9_6;

/* Fixed size */
class var_msg_11_1 : public serial_var
{
public:
    SERIALCONST(var_msg_11_1, "\xA0\x00", 2, 47, 0, 0, 0, 0, 0, 2, 10, 0, 0, 0)
    str1_typ chk;
} msg_11_1;

class var_msg_11_2 : public serial_var
{
public:
    SERIALCONST(var_msg_11_2, "\xA0\x02", 2, 57, 0, 0, 0, 0, 0, 2, 10, 0, 0, 0)
    str2_typ chk;
} msg_11_2;

class var_msg_11_3 : public serial_var
{
public:
    SERIALCONST(var_msg_11_3, "\xA0\x04", 2, 67, 0, 0, 0, 0, 0, 2, 10, 0, 0, 0)
    str3_typ chk;
} msg_11_3;

class var_msg_11_4 : public serial_var
{
public:
    SERIALCONST(var_msg_11_4, "\xA0\x06", 2, 77, 0, 0, 0, 0, 0, 2, 10, 0, 0, 0)
    str4_typ chk;
} msg_11_4;

class var_msg_11_5 : public serial_var
{
public:
    SERIALCONST(var_msg_11_5, "\xA0\x37", 2, 87, 0, 0, 0, 0, 0, 2, 10, 0, 0, 0)
    str5_typ chk;
} msg_11_5;

class var_msg_11_6 : public serial_var
{
public:
    SERIALCONST(var_msg_11_6, "\xA0\x08", 2, 97, 0, 0, 0, 0, 0, 2, 10, 0, 0, 0)
    str6_typ chk;
} msg_11_6;

/* Trailer */
class var_msg_10_1 : public serial_var
{
public:
    SERIALCONST(var_msg_10_1, "\x1F\x40", 2, 0, 0, 0, 0, "\x1F\x10", 2, 2, 9, 0, 0, 0)
    avi_typ chk;
} msg_10_1;

class var_msg_10_2 : public serial_var
{
public:
    SERIALCONST(var_msg_10_2, "\x1F\x41", 2, 0, 0, 0, 0, "\x1F\x10", 2, 2, 9, 0, 0, 0)
    avi_typ chk;
} msg_10_2;

class var_msg_10_3 : public serial_var
{
public:
    SERIALCONST(var_msg_10_3, "\x1F\x42", 2, 0, 0, 0, 0, "\x1F\x10", 2, 2, 9, 0, 0, 0)
    avi_typ chk;
} msg_10_3;

class var_msg_10_4 : public serial_var
{
public:
    SERIALCONST(var_msg_10_4, "\x1F\x43", 2, 0, 0, 0, 0, "\x1F\x10", 2, 2, 9, 0, 0, 0)
    avi_typ chk;
} msg_10_4;

class var_msg_10_5 : public serial_var
{
public:
    SERIALCONST(var_msg_10_5, "\x1F\x44", 2, 0, 0, 0, 0, "\x1F\x10", 2, 2, 9, 0, 0, 0)
    avi_typ chk;
} msg_10_5;

class var_msg_10_6 : public serial_var
{
public:
    SERIALCONST(var_msg_10_6, "\x1F\x45", 2, 0, 0, 0, 0, "\x1F\x10", 2, 2, 9, 0, 0, 0)
    avi_typ chk;
} msg_10_6;

static SerialVarTestSuiteFixture fixture;
static rmclient_init *init = 0;
static bool sendflag = false;

bool SerialVarTestSuiteFixture::setUpWorld()
{
    IOSL_INIT("iosl.cfg");
    init = new rmclient_init("+test_serial_var.cfg");
    sendflag = init->client_defaults()->getnode("send") != 0;
    return init != 0;
}

bool SerialVarTestSuiteFixture::tearDownWorld()
{
    delete init;
    return true;
}

inline void chk_str(value_var &data, std::mt19937 &r)
{
    const int sz = data.size();
    char *str = static_cast<string_var<1> &>(data);
    for (int i = 0; i < sz;)
    {
        if (sz - i >= sizeof(int))
        {
            TS_ASSERT_EQUALS(*reinterpret_cast<int *>(str + i), r());
            i += sizeof(int);
        }
        else
        {
            TS_ASSERT_EQUALS(*(str + i), static_cast<char>(r() % 256));
            ++i;
        }
    }
}

inline bool chk_avi(array_var<unsigned int> &data, std::mt19937 &r)
{
    std::uniform_int_distribution<int> d(1, 61);
    const int sz = d(r);
    TS_ASSERT_EQUALS(sz, data.cardinal());
    for (int i = 0; i < data.cardinal(); ++i)
    {
        const unsigned int val = r();
        TS_ASSERT_EQUALS(data.to_unsigned_int(i), val);
        if (val != data.to_unsigned_int(i))
            return false;
    }
    return true;
}

class str_show : public mpt_autoshow
{
public:
    str_show(value_var *var_, std::mt19937 &r_);
    void operator()();

private:
    value_var *var;
    std::mt19937 &r;
    int count;

};

str_show::str_show(value_var * var_, std::mt19937 & r_) :
    var(var_), r(r_), count(0)
{
    addCallback(var_);
}

void str_show::operator()()
{
    if (dirty)
    {
        dirty = false;
        chk_str(*var, r);
        printf("Count %d\n", count);
        ++count;
        te();
    }
}

class avi_show : public mpt_autoshow
{
public:
    avi_show(array_var<unsigned int> *var_, std::mt19937 &r_, int id_);
    void operator()();

private:
    array_var<unsigned int> *var;
    std::mt19937 &r;
    int count;
    int id;
};

avi_show::avi_show(array_var<unsigned int>* var_, std::mt19937 & r_, int id_) :
    var(var_), r(r_), count(0), id(id_)
{
    addCallback(var_);
}

void avi_show::operator()()
{
    if (dirty)
    {
        dirty = false;
        if (!chk_avi(*var, r))
        {
            printf("Count %d, id %d: Fail\n", count, id);
        }
        else
        {
            printf("Count %d, id %d: OK\n", count, id);
        }
        ++count;
        te();
    }
}

void SerialVarTestSuite::testRecv(void)
{
    TS_ASSERT_DIFFERS(msg_1_1.rtti(), msg_1_2.rtti());
    TS_ASSERT_DIFFERS(msg_1_2.rtti(), msg_1_3.rtti());
    TS_ASSERT_DIFFERS(msg_1_3.rtti(), msg_2_1.rtti());

    if (!sendflag)
    {
        std::mt19937 r1(1);
        std::mt19937 r2(10);
        std::mt19937 r3(100);
        std::mt19937 r4(200);
        std::mt19937 r5(300);
        std::mt19937 r6(400);
        std::mt19937 r7(500);
        std::mt19937 r8(1000);
        std::mt19937 r9(2000);
        std::mt19937 r10(3001);
        std::mt19937 r11(5000);
        avi_show s1_1(&msg_1_1.chk.data, r1, 11);
        avi_show s1_2(&msg_1_2.chk.data, r1, 12);
        avi_show s1_3(&msg_1_3.chk.data, r1, 13);
        avi_show s1_4(&msg_1_4.chk.data, r1, 14);
        avi_show s1_5(&msg_1_5.chk.data, r1, 15);
        avi_show s1_6(&msg_1_6.chk.data, r1, 16);
        avi_show s1_7(&msg_1_7.chk.data, r1, 17);
        avi_show s1_8(&msg_1_8.chk.data, r1, 18);
        avi_show s1_9(&msg_1_9.chk.data, r1, 19);
        avi_show s1_10(&msg_1_10.chk.data, r1, 8);
        avi_show s1_11(&msg_1_11.chk.data, r1, 9);
        avi_show s1_12(&msg_1_12.chk.data, r1, 10);
        avi_show s2_1(&msg_2_1.chk.data, r2, 21);
        avi_show s2_2(&msg_2_2.chk.data, r2, 22);
        avi_show s2_3(&msg_2_3.chk.data, r2, 23);
        avi_show s2_4(&msg_2_4.chk.data, r2, 24);
        avi_show s2_5(&msg_2_5.chk.data, r2, 25);
        avi_show s2_6(&msg_2_6.chk.data, r2, 26);
        avi_show s3_1(&msg_3_1.chk.data, r3, 31);
        avi_show s3_2(&msg_3_2.chk.data, r3, 32);
        avi_show s3_3(&msg_3_3.chk.data, r3, 33);
        avi_show s3_4(&msg_3_4.chk.data, r3, 34);
        avi_show s3_5(&msg_3_5.chk.data, r3, 35);
        avi_show s3_6(&msg_3_6.chk.data, r3, 36);
        avi_show s4_1(&msg_4_1.chk.data, r4, 41);
        avi_show s4_2(&msg_4_2.chk.data, r4, 42);
        avi_show s4_3(&msg_4_3.chk.data, r4, 43);
        avi_show s4_4(&msg_4_4.chk.data, r4, 44);
        avi_show s4_5(&msg_4_5.chk.data, r4, 45);
        avi_show s4_6(&msg_4_6.chk.data, r4, 46);
        avi_show s5_1(&msg_5_1.chk.data, r5, 51);
        avi_show s5_2(&msg_5_2.chk.data, r5, 52);
        avi_show s5_3(&msg_5_3.chk.data, r5, 53);
        avi_show s5_4(&msg_5_4.chk.data, r5, 54);
        avi_show s5_5(&msg_5_5.chk.data, r5, 55);
        avi_show s5_6(&msg_5_6.chk.data, r5, 56);
        avi_show s6_1(&msg_6_1.chk.data, r6, 61);
        avi_show s6_2(&msg_6_2.chk.data, r6, 62);
        avi_show s6_3(&msg_6_3.chk.data, r6, 63);
        avi_show s6_4(&msg_6_4.chk.data, r6, 64);
        avi_show s6_5(&msg_6_5.chk.data, r6, 65);
        avi_show s6_6(&msg_6_6.chk.data, r6, 66);
        avi_show s7_1(&msg_7_1.chk.data, r7, 71);
        avi_show s7_2(&msg_7_2.chk.data, r7, 72);
        avi_show s7_3(&msg_7_3.chk.data, r7, 73);
        avi_show s7_4(&msg_7_4.chk.data, r7, 74);
        avi_show s7_5(&msg_7_5.chk.data, r7, 75);
        avi_show s7_6(&msg_7_6.chk.data, r7, 76);
        avi_show s8_1(&msg_8_1.chk.data, r8, 81);
        avi_show s8_2(&msg_8_2.chk.data, r8, 82);
        avi_show s8_3(&msg_8_3.chk.data, r8, 83);
        avi_show s8_4(&msg_8_4.chk.data, r8, 84);
        avi_show s8_5(&msg_8_5.chk.data, r8, 85);
        avi_show s8_6(&msg_8_6.chk.data, r8, 86);
        avi_show s9_1(&msg_9_1.chk.data, r9, 91);
        avi_show s9_2(&msg_9_2.chk.data, r9, 92);
        avi_show s9_3(&msg_9_3.chk.data, r9, 93);
        avi_show s9_4(&msg_9_4.chk.data, r9, 94);
        avi_show s9_5(&msg_9_5.chk.data, r9, 95);
        avi_show s9_6(&msg_9_6.chk.data, r9, 96);
        avi_show s10_1(&msg_10_1.chk.data, r10, 101);
        avi_show s10_2(&msg_10_2.chk.data, r10, 102);
        avi_show s10_3(&msg_10_3.chk.data, r10, 103);
        avi_show s10_4(&msg_10_4.chk.data, r10, 104);
        avi_show s10_5(&msg_10_5.chk.data, r10, 105);
        avi_show s10_6(&msg_10_6.chk.data, r10, 106);
        str_show s11_1(&msg_11_1.chk.data, r11);
        str_show s11_2(&msg_11_2.chk.data, r11);
        str_show s11_3(&msg_11_3.chk.data, r11);
        str_show s11_4(&msg_11_4.chk.data, r11);
        str_show s11_5(&msg_11_5.chk.data, r11);
        str_show s11_6(&msg_11_6.chk.data, r11);
        while (!init->client_exit() && (!te.isValid() || te.elapsed() < 1500))
        {
            init->incoming();
            mpt_show::refresh();
            init->outgoing();
        }
    }
}

inline void pop_str(value_var &data, std::mt19937 &r)
{
    const int sz = data.size();
    char *str = static_cast<string_var<1> &>(data);
    for (int i = 0; i < sz;)
    {
        if (sz - i >= sizeof(int))
        {
            *reinterpret_cast<int *>(str + i) = r();
            i += sizeof(int);
        }
        else
        {
            *(str + i) = r() % 256;
            ++i;
        }
    }
    data.setRMDirty();
}

inline void pop_avi(array_var<unsigned int> &data, std::mt19937 &r)
{
    // 256 bytes - 12 bytes overhead = 244 (61 ints)
    std::uniform_int_distribution<int> d(1, 61);
    const int sz = d(r);
    data.resize(sz);
    for (int i = 0; i < sz; ++i)
        data.fromValue(i, r());
}

void SerialVarTestSuite::testSend()
{
    if (sendflag)
    {
        millisleep(500);
        std::mt19937 r1(1);
        std::mt19937 r2(10);
        std::mt19937 r3(100);
        std::mt19937 r4(200);
        std::mt19937 r5(300);
        std::mt19937 r6(400);
        std::mt19937 r7(500);
        std::mt19937 r8(1000);
        std::mt19937 r9(2000);
        std::mt19937 r10(3001);
        std::mt19937 r11(5000);
        for (int i = 0; i < 100; ++i)
        {
            pop_avi(msg_1_1.chk.data, r1);
            pop_avi(msg_1_2.chk.data, r1);
            pop_avi(msg_1_3.chk.data, r1);
            pop_avi(msg_1_4.chk.data, r1);
            pop_avi(msg_1_5.chk.data, r1);
            pop_avi(msg_1_6.chk.data, r1);
            pop_avi(msg_1_7.chk.data, r1);
            pop_avi(msg_1_8.chk.data, r1);
            pop_avi(msg_1_9.chk.data, r1);
            pop_avi(msg_1_10.chk.data, r1);
            pop_avi(msg_1_11.chk.data, r1);
            pop_avi(msg_1_12.chk.data, r1);
            pop_avi(msg_2_1.chk.data, r2);
            pop_avi(msg_2_2.chk.data, r2);
            pop_avi(msg_2_3.chk.data, r2);
            pop_avi(msg_2_4.chk.data, r2);
            pop_avi(msg_2_5.chk.data, r2);
            pop_avi(msg_2_6.chk.data, r2);
            pop_avi(msg_3_1.chk.data, r3);
            pop_avi(msg_3_2.chk.data, r3);
            pop_avi(msg_3_3.chk.data, r3);
            pop_avi(msg_3_4.chk.data, r3);
            pop_avi(msg_3_5.chk.data, r3);
            pop_avi(msg_3_6.chk.data, r3);
            pop_avi(msg_4_1.chk.data, r4);
            pop_avi(msg_4_2.chk.data, r4);
            pop_avi(msg_4_3.chk.data, r4);
            pop_avi(msg_4_4.chk.data, r4);
            pop_avi(msg_4_5.chk.data, r4);
            pop_avi(msg_4_6.chk.data, r4);
            pop_avi(msg_5_1.chk.data, r5);
            pop_avi(msg_5_2.chk.data, r5);
            pop_avi(msg_5_3.chk.data, r5);
            pop_avi(msg_5_4.chk.data, r5);
            pop_avi(msg_5_5.chk.data, r5);
            pop_avi(msg_5_6.chk.data, r5);
            pop_avi(msg_6_1.chk.data, r6);
            pop_avi(msg_6_2.chk.data, r6);
            pop_avi(msg_6_3.chk.data, r6);
            pop_avi(msg_6_4.chk.data, r6);
            pop_avi(msg_6_5.chk.data, r6);
            pop_avi(msg_6_6.chk.data, r6);
            pop_avi(msg_7_1.chk.data, r7);
            pop_avi(msg_7_2.chk.data, r7);
            pop_avi(msg_7_3.chk.data, r7);
            pop_avi(msg_7_4.chk.data, r7);
            pop_avi(msg_7_5.chk.data, r7);
            pop_avi(msg_7_6.chk.data, r7);
            pop_avi(msg_8_1.chk.data, r8);
            pop_avi(msg_8_2.chk.data, r8);
            pop_avi(msg_8_3.chk.data, r8);
            pop_avi(msg_8_4.chk.data, r8);
            pop_avi(msg_8_5.chk.data, r8);
            pop_avi(msg_8_6.chk.data, r8);
            pop_avi(msg_9_1.chk.data, r9);
            pop_avi(msg_9_2.chk.data, r9);
            pop_avi(msg_9_3.chk.data, r9);
            pop_avi(msg_9_4.chk.data, r9);
            pop_avi(msg_9_5.chk.data, r9);
            pop_avi(msg_9_6.chk.data, r9);
            pop_avi(msg_10_1.chk.data, r10);
            pop_avi(msg_10_2.chk.data, r10);
            pop_avi(msg_10_3.chk.data, r10);
            pop_avi(msg_10_4.chk.data, r10);
            pop_avi(msg_10_5.chk.data, r10);
            pop_avi(msg_10_6.chk.data, r10);
            pop_str(msg_11_1.chk.data, r11);
            pop_str(msg_11_2.chk.data, r11);
            pop_str(msg_11_3.chk.data, r11);
            pop_str(msg_11_4.chk.data, r11);
            pop_str(msg_11_5.chk.data, r11);
            pop_str(msg_11_6.chk.data, r11);

            init->outgoing();
            millisleep(400);
        }
        TS_TRACE("End");
    }
}

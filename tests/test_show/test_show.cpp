#include "test_show.h"
#include <gen_var.h>
#include <link_var.h>
#include <cplx_var.h>
#include <muxshow.h>
#include <delyshow.h>
#include <cyclshow.h>
#include <dumpshow.h>
#include <time/timecore.h>
#include <util/dbgout.h>
#include <fstream>
#include <vector>
#include <stdio.h>

#define TS_ASSERT_NOT(x) TS_ASSERT(!(x))

class Test : public complex_var
{
public:
	COMPLEXCONST(Test)
	generic_var<int> data[20];
	generic_var<short> shrt[20];
} test[10];

class My_mpt_show : public mpt_show
{
public:
	My_mpt_show();
	void notDirty(mpt_var *);
	void operator()();
	int getStateAndReset();

private:
	bool ndirty;
	bool odirty;
};

My_mpt_show::My_mpt_show() : ndirty(false), odirty(false)
{
}

void My_mpt_show::notDirty(mpt_var *)
{
	ndirty = true;
}
void My_mpt_show::operator()()
{
	odirty = true;
}

int My_mpt_show::getStateAndReset()
{
	int result = 0;
	if (dirty) result += 1;
	if (ndirty) result += 2;
	if (odirty) result += 4;
	dirty = false;
	ndirty = false;
	odirty = false;
	return result;
}

void ShowTestSuite::testMptshow()
{
	My_mpt_show shw1[10][20];
	My_mpt_show shw2[10][20];
	My_mpt_show shw3[10];
	for (int i=0; i < 10; ++i)
	{
		test[i].addCallback(&shw3[i]);
		for (int j=0; j < 20; ++j)
		{
			test[i].data[j].addCallback(&shw1[i][j]);
			test[i].shrt[j].addCallback(&shw2[i][j]);
		}
	}
	for (int i=0; i < 10; ++i)
	{
		for (int j=0; j < 20; ++j)
		{
			test[i].data[j] = rand();
			TS_ASSERT_EQUALS(shw3[i].getStateAndReset(), 1);
			TS_ASSERT_EQUALS(shw2[i][j].getStateAndReset(), 0);
			TS_ASSERT_EQUALS(shw1[i][j].getStateAndReset(), 1);
		}
	}
	for (int i=0; i < 10; ++i)
	{
		for (int j=0; j < 20; ++j)
		{
			test[i].shrt[j] = rand();
			TS_ASSERT_EQUALS(shw3[i].getStateAndReset(), 1);
			TS_ASSERT_EQUALS(shw2[i][j].getStateAndReset(), 1);
			TS_ASSERT_EQUALS(shw1[i][j].getStateAndReset(), 0);
		}
	}
	for (int i=0; i < 10; ++i)
	{
		for (int j=0; j < 20; ++j)
		{
			test[i].data[j] = test[i].data[j].to_int();
			TS_ASSERT_EQUALS(shw3[i].getStateAndReset(), 2);
			TS_ASSERT_EQUALS(shw2[i][j].getStateAndReset(), 0);
			TS_ASSERT_EQUALS(shw1[i][j].getStateAndReset(), 2);
		}
	}
	for (int i=0; i < 10; ++i)
	{
		for (int j=0; j < 20; ++j)
		{
			test[i].data[j] = rand();
			test[i].shrt[j] = test[i].shrt[j].to_short();
			TS_ASSERT_EQUALS(shw3[i].getStateAndReset(), 3);
			TS_ASSERT_EQUALS(shw2[i][j].getStateAndReset(), 2);
			TS_ASSERT_EQUALS(shw1[i][j].getStateAndReset(), 1);
		}
	}
	mpt_show::refresh();
	for (int i=0; i < 10; ++i)
	{
		TS_ASSERT_EQUALS(shw3[i].getStateAndReset(), 4);
		for (int j=0; j < 20; ++j)
		{
			TS_ASSERT_EQUALS(shw2[i][j].getStateAndReset(), 4);
			TS_ASSERT_EQUALS(shw1[i][j].getStateAndReset(), 4);
		}
	}
	for (int i = 0; i < 10; ++i)
	{
		test[i].refresh();
		TS_ASSERT_EQUALS(shw3[i].getStateAndReset(), 4);
		for (int j = 0; j < 20; ++j)
		{
			TS_ASSERT_EQUALS(shw2[i][j].getStateAndReset(), 4);
			TS_ASSERT_EQUALS(shw1[i][j].getStateAndReset(), 4);
		}
	}
	for (int i=0; i < 10; ++i)
	{
		test[i].removeCallback(&shw3[i]);
		for (int j=0; j < 20; ++j)
		{
			test[i].data[j].removeCallback(&shw1[i][j]);
			test[i].shrt[j].removeCallback(&shw2[i][j]);
		}
	}
}

class My_mpt_autoshow : public mpt_autoshow
{
public:
	My_mpt_autoshow();
	My_mpt_autoshow(const My_mpt_autoshow &);
	void notDirty(mpt_var *);
	void operator()();
	int getStateAndReset();

private:
	bool ndirty;
	bool odirty;
};

My_mpt_autoshow::My_mpt_autoshow() : ndirty(false), odirty(false)
{
}

My_mpt_autoshow::My_mpt_autoshow(const My_mpt_autoshow &r) : mpt_autoshow(r)
{
}

void My_mpt_autoshow::notDirty(mpt_var *)
{
	ndirty = true;
}
void My_mpt_autoshow::operator()()
{
	odirty = true;
}

int My_mpt_autoshow::getStateAndReset()
{
	int result = 0;
	if (dirty) result += 1;
	if (ndirty) result += 2;
	if (odirty) result += 4;
	dirty = false;
	ndirty = false;
	odirty = false;
	return result;
}

std::vector<My_mpt_autoshow> shwlst;

void ShowTestSuite::testAutoshow()
{
	My_mpt_autoshow shw1[10][20];
	My_mpt_autoshow shw2[10][20];
	My_mpt_autoshow shw3[10];
	for (int i = 0; i < 10; ++i)
	{
		shw3[i].addCallback(&test[i]);
		for (int j = 0; j < 20; ++j)
		{
			shw1[i][j].addCallback(&test[i].data[j]);
			shw2[i][j].addCallback(&test[i].shrt[j]);
		}
	}
	for (int i = 0; i < 10; ++i)
	{
		for (int j = 0; j < 20; ++j)
		{
			test[i].data[j] = rand();
			TS_ASSERT_EQUALS(shw3[i].getStateAndReset(), 1);
			TS_ASSERT_EQUALS(shw2[i][j].getStateAndReset(), 0);
			TS_ASSERT_EQUALS(shw1[i][j].getStateAndReset(), 1);
		}
	}
	for (int i = 0; i < 10; ++i)
	{
		for (int j = 0; j < 20; ++j)
		{
			test[i].shrt[j] = rand();
			TS_ASSERT_EQUALS(shw3[i].getStateAndReset(), 1);
			TS_ASSERT_EQUALS(shw2[i][j].getStateAndReset(), 1);
			TS_ASSERT_EQUALS(shw1[i][j].getStateAndReset(), 0);
		}
	}
	for (int i = 0; i < 10; ++i)
	{
		for (int j = 0; j < 20; ++j)
		{
			test[i].data[j] = test[i].data[j].to_int();
			TS_ASSERT_EQUALS(shw3[i].getStateAndReset(), 2);
			TS_ASSERT_EQUALS(shw2[i][j].getStateAndReset(), 0);
			TS_ASSERT_EQUALS(shw1[i][j].getStateAndReset(), 2);
		}
	}
	for (int i = 0; i < 10; ++i)
	{
		for (int j = 0; j < 20; ++j)
		{
			test[i].data[j] = rand();
			test[i].shrt[j] = test[i].shrt[j].to_short();
			TS_ASSERT_EQUALS(shw3[i].getStateAndReset(), 3);
			TS_ASSERT_EQUALS(shw2[i][j].getStateAndReset(), 2);
			TS_ASSERT_EQUALS(shw1[i][j].getStateAndReset(), 1);
		}
	}
	mpt_show::refresh();
	for (int i = 0; i < 10; ++i)
	{
		TS_ASSERT_EQUALS(shw3[i].getStateAndReset(), 4);
		for (int j = 0; j < 20; ++j)
		{
			TS_ASSERT_EQUALS(shw2[i][j].getStateAndReset(), 4);
			TS_ASSERT_EQUALS(shw1[i][j].getStateAndReset(), 4);
		}
	}
	for (int i = 0; i < 10; ++i)
	{
		test[i].refresh();
		TS_ASSERT_EQUALS(shw3[i].getStateAndReset(), 4);
		for (int j = 0; j < 20; ++j)
		{
			TS_ASSERT_EQUALS(shw2[i][j].getStateAndReset(), 4);
			TS_ASSERT_EQUALS(shw1[i][j].getStateAndReset(), 4);
		}
	}
	for (int i = 0; i < 10; ++i)
	{
		for (int j = 0; j < 20; ++j)
		{
			shw1[i][j].removeCallback(&test[i].data[j]);
			shw2[i][j].removeCallback(&test[i].shrt[j]);
		}
	}
	// autoshow is designed to fit into a container
	for (int i = 0; i < 10; ++i)
	{
		for (int j = 0; j < 20; ++j)
		{
			My_mpt_autoshow shw1, shw2;
			shw1.addCallback(&test[i].data[j]);
			shw2.addCallback(&test[i].shrt[j]);
			shwlst.push_back(shw1);
			shwlst.push_back(shw2);
		}
	}
	// we leave this function and check if the
	// show in the list is still viable in testMisc
}

class My_mux_show : public mux_show
{
public:
	My_mux_show(mpt_var *st, unsigned int cnt, mpt_var *offset);
	void operator()();
	int getStateAndReset();

private:
	bool odirty;
};

My_mux_show::My_mux_show(mpt_var *st, unsigned int cnt, mpt_var *offset) : mux_show(st, cnt, offset), odirty(false)
{
}

void My_mux_show::operator()()
{
	odirty = true;
}

int My_mux_show::getStateAndReset()
{
	int result = 0;
	if (odirty) result += 1;
	odirty = false;
	return result;
}

void ShowTestSuite::testMuxshow(void)
{
	My_mux_show mux1(&test[0], 10, 0);
	My_mux_show mux2(&test[0], 10, &test[0].data[0]);
	My_mux_show mux3(&test[0], 10, &test[0].shrt[0]);
	My_mux_show mux4(&test[0].data[0], 20, 0);
	My_mux_show mux5(&test[0].shrt[0], 20, 0);
	// inner forward order
	for (int i = 0; i < 20; ++i)
		test[0].data[i] = rand();
	TS_ASSERT_EQUALS(mux1.getDirty(), 0);
	TS_ASSERT_EQUALS(mux1.getDirty(), -1);
	TS_ASSERT_EQUALS(mux2.getDirty(), 0);
	TS_ASSERT_EQUALS(mux2.getDirty(), -1);
	TS_ASSERT_EQUALS(mux3.getDirty(), -1);
	for (int i = 0; i < 20; ++i)
		TS_ASSERT_EQUALS(mux4.getDirty(), i);
	TS_ASSERT_EQUALS(mux4.getDirty(), -1);
	TS_ASSERT_EQUALS(mux5.getDirty(), -1);

	// outer forward order
	for (int i = 0; i < 10; ++i)
		test[i].data[0] = rand();
	for (int i = 0; i < 10; ++i)
		TS_ASSERT_EQUALS(mux1.getDirty(), i);
	TS_ASSERT_EQUALS(mux1.getDirty(), -1);
	for (int i = 0; i < 10; ++i)
		TS_ASSERT_EQUALS(mux2.getDirty(), i);
	TS_ASSERT_EQUALS(mux2.getDirty(), -1);
	TS_ASSERT_EQUALS(mux3.getDirty(), -1);
	TS_ASSERT_EQUALS(mux4.getDirty(), 0);
	TS_ASSERT_EQUALS(mux4.getDirty(), -1);
	TS_ASSERT_EQUALS(mux5.getDirty(), -1);

	// inner forward order, offsetted
	for (int i = 0; i < 20; ++i)
		test[0].shrt[i] = rand();
	TS_ASSERT_EQUALS(mux1.getDirty(), 0);
	TS_ASSERT_EQUALS(mux1.getDirty(), -1);
	TS_ASSERT_EQUALS(mux2.getDirty(), -1);
	TS_ASSERT_EQUALS(mux3.getDirty(), 0);
	TS_ASSERT_EQUALS(mux3.getDirty(), -1);
	TS_ASSERT_EQUALS(mux4.getDirty(), -1);
	for (int i = 0; i < 20; ++i)
		TS_ASSERT_EQUALS(mux5.getDirty(), i);
	TS_ASSERT_EQUALS(mux5.getDirty(), -1);

	// outer forward order, offsetted
	for (int i = 0; i < 10; ++i)
		test[i].shrt[0] = rand();
	for (int i = 0; i < 10; ++i)
		TS_ASSERT_EQUALS(mux1.getDirty(), i);
	TS_ASSERT_EQUALS(mux1.getDirty(), -1);
	TS_ASSERT_EQUALS(mux2.getDirty(), -1);
	for (int i = 0; i < 10; ++i)
		TS_ASSERT_EQUALS(mux3.getDirty(), i);
	TS_ASSERT_EQUALS(mux3.getDirty(), -1);
	TS_ASSERT_EQUALS(mux4.getDirty(), -1);
	TS_ASSERT_EQUALS(mux5.getDirty(), 0);
	TS_ASSERT_EQUALS(mux5.getDirty(), -1);

	// inner reverse order
	for (int i = 19; i >= 0; --i)
		test[0].data[i] = rand();
	TS_ASSERT_EQUALS(mux1.getDirty(), 0);
	TS_ASSERT_EQUALS(mux1.getDirty(), -1);
	TS_ASSERT_EQUALS(mux2.getDirty(), 0);
	TS_ASSERT_EQUALS(mux2.getDirty(), -1);
	TS_ASSERT_EQUALS(mux3.getDirty(), -1);
	for (int i = 19; i >= 0; --i)
		TS_ASSERT_EQUALS(mux4.getDirty(), i);
	TS_ASSERT_EQUALS(mux4.getDirty(), -1);
	TS_ASSERT_EQUALS(mux5.getDirty(), -1);

	// outer reverse order
	for (int i = 9; i >= 0; --i)
		test[i].data[0] = rand();
	for (int i = 9; i >= 0; --i)
		TS_ASSERT_EQUALS(mux1.getDirty(), i);
	TS_ASSERT_EQUALS(mux1.getDirty(), -1);
	for (int i = 9; i >= 0; --i)
		TS_ASSERT_EQUALS(mux2.getDirty(), i);
	TS_ASSERT_EQUALS(mux2.getDirty(), -1);
	TS_ASSERT_EQUALS(mux3.getDirty(), -1);
	TS_ASSERT_EQUALS(mux4.getDirty(), 0);
	TS_ASSERT_EQUALS(mux4.getDirty(), -1);
	TS_ASSERT_EQUALS(mux5.getDirty(), -1);

	// inner reverse order, offsetted
	for (int i = 19; i >= 0; --i)
		test[0].shrt[i] = rand();
	TS_ASSERT_EQUALS(mux1.getDirty(), 0);
	TS_ASSERT_EQUALS(mux1.getDirty(), -1);
	TS_ASSERT_EQUALS(mux2.getDirty(), -1);
	TS_ASSERT_EQUALS(mux3.getDirty(), 0);
	TS_ASSERT_EQUALS(mux3.getDirty(), -1);
	TS_ASSERT_EQUALS(mux4.getDirty(), -1);
	for (int i = 19; i >= 0; --i)
		TS_ASSERT_EQUALS(mux5.getDirty(), i);
	TS_ASSERT_EQUALS(mux5.getDirty(), -1);

	// outer reverse order, offsetted
	for (int i = 9; i >= 0; --i)
		test[i].shrt[0] = rand();
	for (int i = 9; i >= 0; --i)
		TS_ASSERT_EQUALS(mux1.getDirty(), i);
	TS_ASSERT_EQUALS(mux1.getDirty(), -1);
	TS_ASSERT_EQUALS(mux2.getDirty(), -1);
	for (int i = 9; i >= 0; --i)
		TS_ASSERT_EQUALS(mux3.getDirty(), i);
	TS_ASSERT_EQUALS(mux3.getDirty(), -1);
	TS_ASSERT_EQUALS(mux4.getDirty(), -1);
	TS_ASSERT_EQUALS(mux5.getDirty(), 0);
	TS_ASSERT_EQUALS(mux5.getDirty(), -1);

	// inner skip order
	for (int i = 0; i < 60; i += 3)
		test[0].data[i % 20] = rand();
	TS_ASSERT_EQUALS(mux1.getDirty(), 0);
	TS_ASSERT_EQUALS(mux1.getDirty(), -1);
	TS_ASSERT_EQUALS(mux2.getDirty(), 0);
	TS_ASSERT_EQUALS(mux2.getDirty(), -1);
	TS_ASSERT_EQUALS(mux3.getDirty(), -1);
	for (int i = 0; i < 60; i += 3)
		TS_ASSERT_EQUALS(mux4.getDirty(), i % 20);
	TS_ASSERT_EQUALS(mux4.getDirty(), -1);
	TS_ASSERT_EQUALS(mux5.getDirty(), -1);

	// outer skip order
	for (int i = 0; i < 30; i += 3)
		test[i % 10].data[0] = rand();
	for (int i = 0; i < 30; i += 3)
		TS_ASSERT_EQUALS(mux1.getDirty(), i % 10);
	TS_ASSERT_EQUALS(mux1.getDirty(), -1);
	for (int i = 0; i < 30; i += 3)
		TS_ASSERT_EQUALS(mux2.getDirty(), i % 10);
	TS_ASSERT_EQUALS(mux2.getDirty(), -1);
	TS_ASSERT_EQUALS(mux3.getDirty(), -1);
	TS_ASSERT_EQUALS(mux4.getDirty(), 0);
	TS_ASSERT_EQUALS(mux4.getDirty(), -1);
	TS_ASSERT_EQUALS(mux5.getDirty(), -1);

	// inner reverse order, offsetted
	for (int i = 0; i < 60; i += 3)
		test[0].shrt[i % 20] = rand();
	TS_ASSERT_EQUALS(mux1.getDirty(), 0);
	TS_ASSERT_EQUALS(mux1.getDirty(), -1);
	TS_ASSERT_EQUALS(mux2.getDirty(), -1);
	TS_ASSERT_EQUALS(mux3.getDirty(), 0);
	TS_ASSERT_EQUALS(mux3.getDirty(), -1);
	TS_ASSERT_EQUALS(mux4.getDirty(), -1);
	for (int i = 0; i < 60; i += 3)
		TS_ASSERT_EQUALS(mux5.getDirty(), i % 20);
	TS_ASSERT_EQUALS(mux5.getDirty(), -1);

	// outer reverse order, offsetted
	for (int i = 0; i < 30; i += 3)
		test[i%10].shrt[0] = rand();
	for (int i = 0; i < 30; i += 3)
		TS_ASSERT_EQUALS(mux1.getDirty(), i % 10);
	TS_ASSERT_EQUALS(mux1.getDirty(), -1);
	TS_ASSERT_EQUALS(mux2.getDirty(), -1);
	for (int i = 0; i < 30; i += 3)
		TS_ASSERT_EQUALS(mux3.getDirty(), i % 10);
	TS_ASSERT_EQUALS(mux3.getDirty(), -1);
	TS_ASSERT_EQUALS(mux4.getDirty(), -1);
	TS_ASSERT_EQUALS(mux5.getDirty(), 0);
	TS_ASSERT_EQUALS(mux5.getDirty(), -1);

	// last significant index order
	for (int i = 0; i < 10; ++i)
		test[0].data[i] = rand(); // 0...9
	test[0].data[5] = rand(); // 5
	test[0].data[2] = rand(); // 2
	test[0].data[5] = rand(); // 5
	int idxanswr[] = { 0,1,3,4,6,7,8,9,2,5 };
	for (int i = 0; i < 10; ++i)
		TS_ASSERT_EQUALS(mux4.getDirty(), idxanswr[i]);
	TS_ASSERT_EQUALS(mux4.getDirty(), -1);
}

void ShowTestSuite::testCycleshow(void)
{
	cycle_show shw(0, 50000); // 50 msec
	timeElapsed te(true);
	for (int i = 1; i < 15; ++i)
	{
		while (!shw.getDirty());
		TS_ASSERT_DELTA(te.elapsed(), 50*i, 5);
	}
	cycle_show shw2(1, 20000); // 1.02 sec
	te();
	for (int i = 1; i < 15; ++i)
	{
		while (!shw2.getDirty()) millisleep(2);
		TS_ASSERT_DELTA(te.elapsed(), 1020 * i, 5);
	}
	te();
	shw2.setDirty(0);
	TS_ASSERT_DELTA(te.elapsed(), 0, 5);

	struct timeval tv;
	now(&tv);
	unsigned int usec = 1000000 - tv.tv_usec + 5000;
	cycle_show shw3(0, usec);
	te();
	while (!shw3.getDirty());
	TS_ASSERT_DELTA(te.elapsed(), usec / 1000, 5);
}

void ShowTestSuite::testDelayshow()
{
	timeElapsed te;
	delay_show shw(0, 50000); // 50 msec
	test[0].addCallback(&shw);
	for (int i = 0; i < 20; ++i)
	{
		test[0].data[i] = rand();
		te();
		while (!shw.getDirty());
		TS_ASSERT_DELTA(te.elapsed(), 50, 5);
	}
	test[0].removeCallback(&shw);

	delay_show shw2(1, 20000); // 1.02 sec
	test[0].addCallback(&shw2);
	for (int i = 0; i < 12; ++i)
	{
		test[0].data[i] = rand();
		te();
		while (!shw2.getDirty()) millisleep(2);
		TS_ASSERT_DELTA(te.elapsed(), 1020, 5);
	}
	test[0].removeCallback(&shw2);
}

void ShowTestSuite::testDumpshow()
{
	dump_show shw(&test[0], "Unit Test ");
	dout.openfile("log");
	test[0].data[4] = rand();
	mpt_show::refresh();
	dout.closefile();

	std::ifstream  strm("log");
	std::string str;
	int it;
	strm >> str;
	TS_ASSERT_EQUALS(str, "Unit");
	strm >> str;
	TS_ASSERT_EQUALS(str, "Test");
	for (int i = 0; i < 20; ++i)
	{
		strm >> it;
		TS_ASSERT_EQUALS(it, test[0].data[i]);
	}
	for (int i = 0; i < 20; ++i)
	{
		strm >> it;
		TS_ASSERT_EQUALS(it, test[0].shrt[i]);
	}
	strm.close();

	remove("log");
}

void ShowTestSuite::testMisc()
{
	unsigned int tv1_tv_sec, tv1_tv_usec;
	struct timeval tv2;
	delay_show::getTime(tv1_tv_sec, tv1_tv_usec);
	now(&tv2);
	unsigned long long t1 = tv1_tv_sec * 1000000ULL + tv1_tv_usec;
	unsigned long long t2 = tv2.tv_sec * 1000000ULL + tv2.tv_usec;
	TS_ASSERT_DELTA(t1, t2, 1000);

	// reset the shows in the vector
	for (int i = 0; i < shwlst.size(); ++i)
		shwlst[i].getStateAndReset();

	for (int i = 0; i < 10; ++i)
	{
		for (int j = 0; j < 20; ++j)
		{
			test[i].data[j] = rand();
			test[i].shrt[j] = rand();
		}
	}

	for (int i = 0; i < shwlst.size(); ++i)
		TS_ASSERT_EQUALS(shwlst[i].getStateAndReset(), 1);

	My_mpt_autoshow tmp;
	tmp.addCallback(&test[5]);

	shwlst.back() = tmp;
	test[9].shrt[19] = rand();

	TS_ASSERT_EQUALS(shwlst.back().getStateAndReset(), 0); // because it has been moved

	test[5].data[3] = rand();
	TS_ASSERT_EQUALS(shwlst.back().getStateAndReset(), 1);
	TS_ASSERT_EQUALS(tmp.getStateAndReset(), 1);

	TS_TRACE("End");
}

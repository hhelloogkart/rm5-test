#ifndef _TEST_SHOW_H_DEF_
#define _TEST_SHOW_H_DEF_

#include <cxxtest/TestSuite.h>

class ShowTestSuite : public CxxTest::TestSuite
{
public:
	void testMptshow();
	void testAutoshow();
	void testMuxshow(void);
	void testCycleshow(void);
	void testDelayshow();
	void testDumpshow();
	void testMisc();
};

#endif


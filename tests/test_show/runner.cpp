/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_show";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_ShowTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_show\test_show.h"

static ShowTestSuite suite_ShowTestSuite;

static CxxTest::List Tests_ShowTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_ShowTestSuite( "test_show/test_show.h", 6, "ShowTestSuite", suite_ShowTestSuite, Tests_ShowTestSuite );

static class TestDescription_suite_ShowTestSuite_testMptshow : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ShowTestSuite_testMptshow() : CxxTest::RealTestDescription( Tests_ShowTestSuite, suiteDescription_ShowTestSuite, 9, "testMptshow" ) {}
 void runTest() { suite_ShowTestSuite.testMptshow(); }
} testDescription_suite_ShowTestSuite_testMptshow;

static class TestDescription_suite_ShowTestSuite_testAutoshow : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ShowTestSuite_testAutoshow() : CxxTest::RealTestDescription( Tests_ShowTestSuite, suiteDescription_ShowTestSuite, 10, "testAutoshow" ) {}
 void runTest() { suite_ShowTestSuite.testAutoshow(); }
} testDescription_suite_ShowTestSuite_testAutoshow;

static class TestDescription_suite_ShowTestSuite_testMuxshow : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ShowTestSuite_testMuxshow() : CxxTest::RealTestDescription( Tests_ShowTestSuite, suiteDescription_ShowTestSuite, 11, "testMuxshow" ) {}
 void runTest() { suite_ShowTestSuite.testMuxshow(); }
} testDescription_suite_ShowTestSuite_testMuxshow;

static class TestDescription_suite_ShowTestSuite_testCycleshow : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ShowTestSuite_testCycleshow() : CxxTest::RealTestDescription( Tests_ShowTestSuite, suiteDescription_ShowTestSuite, 12, "testCycleshow" ) {}
 void runTest() { suite_ShowTestSuite.testCycleshow(); }
} testDescription_suite_ShowTestSuite_testCycleshow;

static class TestDescription_suite_ShowTestSuite_testDelayshow : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ShowTestSuite_testDelayshow() : CxxTest::RealTestDescription( Tests_ShowTestSuite, suiteDescription_ShowTestSuite, 13, "testDelayshow" ) {}
 void runTest() { suite_ShowTestSuite.testDelayshow(); }
} testDescription_suite_ShowTestSuite_testDelayshow;

static class TestDescription_suite_ShowTestSuite_testDumpshow : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ShowTestSuite_testDumpshow() : CxxTest::RealTestDescription( Tests_ShowTestSuite, suiteDescription_ShowTestSuite, 14, "testDumpshow" ) {}
 void runTest() { suite_ShowTestSuite.testDumpshow(); }
} testDescription_suite_ShowTestSuite_testDumpshow;

static class TestDescription_suite_ShowTestSuite_testMisc : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ShowTestSuite_testMisc() : CxxTest::RealTestDescription( Tests_ShowTestSuite, suiteDescription_ShowTestSuite, 15, "testMisc" ) {}
 void runTest() { suite_ShowTestSuite.testMisc(); }
} testDescription_suite_ShowTestSuite_testMisc;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

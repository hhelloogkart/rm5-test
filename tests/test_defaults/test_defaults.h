#ifndef _TEST_DEFAULTS_H_DEF_
#define _TEST_DEFAULTS_H_DEF_

#include <cxxtest/TestSuite.h>

class DefaultsTestSuite : public CxxTest::TestSuite
{
public:
	void testload(void);
	void testgetnode(void);
	void testgetChild(void);
	void testgetInt(void);
	void testgetString(void);
	void testgetDouble(void);
	void testgetdata(void);
	void testsetValue(void);
	void testsave(void);
	void testload2(void);
	void testcmdline(void);
	void testvarreplacement(void);
	void testascbincoversion(void);
};

#endif


#include "test_defaults.h"
#include <defaults.h>
#include <string>
#include <util/utilcore.h>
#include <pcre/pcre.h>

void DefaultsTestSuite::testload(void){

    //Test of RM_EXPORT defaults
    defaults* test = load_defaults("rm.cfg");
    TS_ASSERT(test != NULL);
    delete test;
}

void DefaultsTestSuite::testgetnode(void){

    //Test of getnode
    defaults* test = load_defaults("+rm.cfg");
    tree_node* node0;
    tree_node* node1;
    tree_node* node2;

    node0 = test->getnode("/server/");
    node1 = test->getnode("port", node0);
    node2 = test->getnode("/server/port");
    TS_ASSERT(node0 != NULL);
    TS_ASSERT(node1 == node2);
    delete test;
}

void DefaultsTestSuite::testgetChild(void){

    //Test of getChild
    defaults* test = load_defaults("+rm.cfg");
    tree_node* node;
    tree_node* server;

    server = test->getnode("/server/");
    node = test->getChild(server);
    TS_ASSERT(node != NULL);

    node = test->getnode("/server/port");
    TS_ASSERT(node != NULL);

    node = test->getChild(node);
    TS_ASSERT(node == NULL);

    delete test;
}

void DefaultsTestSuite::testgetInt(void) {

    defaults* test = load_defaults("+rm.cfg");

    //Test of getint
    tree_node* node = NULL;
    node = test->getint("/server/port");
    TS_ASSERT(node != NULL);

    //Test of getInt
    int value = test->getInt("/server/port");
    TS_ASSERT(value == 5540);

    node = test->getint("/server/key");
    TS_ASSERT(node == NULL);

    delete test;
}

void DefaultsTestSuite::testgetString(void){

    defaults* test = load_defaults("+rm.cfg");

    //Test of getstring
    tree_node* node = NULL;
    node = test->getstring("/storage/name");
    TS_ASSERT(node != NULL);

    //Test of getString
    std::string nodename = test->getString("/storage/name");
    int value = atoi(nodename.c_str());
    TS_ASSERT(value == 0);

    char val[8192];
    node = test->getstring("/storage/name", val);
    TS_ASSERT(node != NULL);
    TS_ASSERT_SAME_DATA(val, "RESMGR5", 8);

#ifdef WIN32
    node = test->getstring("USERNAME", val);
#else
    node = test->getstring("USER", val);
#endif
    TS_ASSERT(node != NULL);

    node = test->getstring("PATH", val);
    TS_ASSERT(node != NULL);

    node = test->getstring("/server/key");
    TS_ASSERT(node == NULL);

    bool fail;
    const char *ret = test->getString("/server/key", &fail);
    TS_ASSERT_EQUALS(fail, true);
    TS_ASSERT(ret != 0);
    TS_ASSERT_EQUALS(*ret, '\0');

    delete test;
}

void DefaultsTestSuite::testgetDouble(void) {

    defaults* test = load_defaults(0, "rm.exe");

    //Test of getdbl
    tree_node* node = NULL;
    node = test->getdbl("/server/port");
    TS_ASSERT(node != NULL);

    //Test of getDouble
    double value = test->getDouble("/server/port");
    TS_ASSERT(value == 5540);

    node = test->getdbl("/server/key");
    TS_ASSERT(node == NULL);
    delete test;
}

void DefaultsTestSuite::testgetdata(void) {

    defaults* test = load_defaults("+rm.cfg");

    //Test of getdata
    tree_node* node = NULL;
    node = test->getdata("/server/port");
    TS_ASSERT(node != NULL);

    node = test->getdata("/server/key");
    TS_ASSERT(node == NULL);
    delete test;
}

void DefaultsTestSuite::testsetValue(void) {

    defaults* test = load_defaults("+rm.cfg");

    //Test for setValue -- String
    test->setValue("/client/name", "lenovo");
    std::string nodevalue = test->getString("/client/name");
    int value = atoi(nodevalue.c_str());
    TS_ASSERT_EQUALS(value, 0);

    //Test for setValue -- Int
    int  toset = test->getInt("/server/udpaddr");
    TS_ASSERT(toset == NULL);
    test->setValue("/server/udpaddr", 2);
    toset = test->getInt("/server/udpaddr");
    TS_ASSERT(toset = 2);

    //Test for setValue -- Double
    double set = test->getDouble("/server/log");
    TS_ASSERT(set == NULL);
    test->setValue("/server/log", 123456);
    set = test->getDouble("/server/log");
    TS_ASSERT_EQUALS(set, 123456);

    //Test for setValue -- Data
    tree_node *tn = test->getnode("server");
    TS_ASSERT(tn != 0);
    tree_node *cd = test->getChild(tn);
    TS_ASSERT(cd != 0);
    char ch[128], mr[128];
    for (int i = 0; i < 64; ++i)
        ch[i] = rand();
    ch[64] = '\0';
    for (int i = 65; i < 128; ++i)
        ch[i] = rand();
    test->setValue("key", ch, 128, cd);
    test->getdata("/server/key", mr);
    TS_ASSERT_SAME_DATA(ch, mr, 128);
    delete test;
}

void DefaultsTestSuite::testsave(void)
{
    int value = rand(), mirror;
    defaults * test = new defaults;
    {
        file_buffer filebuf("rm.cfg", true);
        test->load(filebuf);
    }
    /*read the value*/
    test->setValue("/server/udpaddr", value);

    {
        file_buffer filebuf("rmclient.cfg", true);
        test->save(filebuf);
    }
    delete test;

    /*read the value again*/
    test = load_defaults();
    TS_ASSERT_DIFFERS(test->getint("/server/udpaddr", &mirror), (tree_node *)0);
    TS_ASSERT_EQUALS(value, mirror);
    delete test;
    remove("rmclient.cfg");
}

void DefaultsTestSuite::testload2(void) {
    //Test of load defaults
    defaults* test = load_defaults("test.cfg");
    TS_ASSERT(test != NULL);

    int set;
    double set1;
    int set2;
    std::string set3;

    test->setValue("/bk101/price", 33);
    set = test->getInt("/bk101/price");
    TS_ASSERT_EQUALS(set, 33);

    test->setValue("/bk139/price/discount", 34.90);
    set1 = test->getDouble("/bk139/price/discount");
    TS_ASSERT_DELTA(set1, 34.90, 0.001);

    test->setValue("/bk119/serial_number", 11);
    set2 = test->getInt("/bk119/serial_number");
    TS_ASSERT_EQUALS(set2, 11);

    test->setValue("/bk122/genre", "Action");
    set3 = test->getString("/bk122/genre");
    int value = atoi(set3.c_str());
    TS_ASSERT_EQUALS(value, 0);

    delete test;
}

void DefaultsTestSuite::testcmdline(void)
{
    char line01[] = "log=data.txt";
    char line02[] = "/client/write=DATA1";
    char line03[] = "/client/write@@2=DATA2";
    char line04[] = "/client/write@@3=DATA3";
    char line05[] = "/client@@2/name=dcm";
    char line06[] = "/client@@2/read@@1=DCM1";
    char line07[] = "/client@@2/read@@2=DCM2";
    char line08[] = "/client@@2/write@@1=DWM1";
    char line09[] = "/client@@2/write@@2=DWM2";
    char line10[] = "/client@@2/write@@3=DWM3";
    char line11[] = "/storage/index=2";
    char line12[] = "/storage/smallidx";
    char *argv[] = { line01, line02, line03, line04, line05,
        line06, line07, line08, line09, line10, line11, line12 };
    const int argc = sizeof(argv) / sizeof(char *);
    char svalue[512];
    int ivalue;
    int icnt1, icnt2;
    tree_node *tn = 0;
    defaults* test = load_defaults("+rm.cfg");
    test->parse_cmdline(argc, argv);
    TS_ASSERT(test->getint("/server/port", &ivalue) != 0);
    TS_ASSERT_EQUALS(ivalue, 5540);
    TS_ASSERT(test->getnode("/server/console") != 0);
    TS_ASSERT(test->getint("/server/udpport", &ivalue) != 0);
    TS_ASSERT_EQUALS(ivalue, 0);
    TS_ASSERT(test->getnode("/server/udpaddr") != 0);
    TS_ASSERT(test->getnode("/server/log") != 0);

    icnt1 = 0;
    for (tn = test->getnode("client"); tn != 0; tn = test->getnode("client", tn), ++icnt1)
    {
        tree_node *ch = test->getChild(tn);
        TS_ASSERT(ch != 0);
        icnt2 = 0;
        if (icnt1 == 0)
        {
            TS_ASSERT(test->getstring("name", svalue, ch) != 0);
            TS_ASSERT_SAME_DATA(svalue, "RMSCRIPT", 9);

            for (tree_node *vn = test->getstring("read", svalue, ch); vn != 0; vn = test->getstring("read", svalue, vn), ++icnt2)
            {
                TS_ASSERT_SAME_DATA(svalue, "\\S+", 4);
            }
            TS_ASSERT_EQUALS(icnt2, 1);
            icnt2 = 0;
            for (tree_node *vn = test->getstring("write", svalue, ch); vn != 0; vn = test->getstring("write", svalue, vn), ++icnt2)
            {
                switch (icnt2)
                {
                case 0:
                    TS_ASSERT_SAME_DATA(svalue, "DATA1", 6);
                    break;
                case 1:
                    TS_ASSERT_SAME_DATA(svalue, "DATA2", 6);
                    break;
                case 2:
                    TS_ASSERT_SAME_DATA(svalue, "DATA3", 6);
                    break;
                }
            }
            TS_ASSERT_EQUALS(icnt2, 3);
        }
        else if (icnt1 == 1)
        {
            TS_ASSERT(test->getstring("name", svalue, ch) != 0);
            TS_ASSERT_SAME_DATA(svalue, "dcm", 4);

            for (tree_node *vn = test->getstring("read", svalue, ch); vn != 0; vn = test->getstring("read", svalue, vn), ++icnt2)
            {
                switch (icnt2)
                {
                case 0:
                    TS_ASSERT_SAME_DATA(svalue, "DCM1", 6);
                    break;
                case 1:
                    TS_ASSERT_SAME_DATA(svalue, "DCM2", 6);
                    break;
                }
            }
            TS_ASSERT_EQUALS(icnt2, 2);
            icnt2 = 0;
            for (tree_node *vn = test->getstring("write", svalue, ch); vn != 0; vn = test->getstring("write", svalue, vn), ++icnt2)
            {
                switch (icnt2)
                {
                case 0:
                    TS_ASSERT_SAME_DATA(svalue, "DWM1", 6);
                    break;
                case 1:
                    TS_ASSERT_SAME_DATA(svalue, "DWM2", 6);
                    break;
                case 2:
                    TS_ASSERT_SAME_DATA(svalue, "DWM3", 6);
                    break;
                }
            }
            TS_ASSERT_EQUALS(icnt2, 3);
        }
    }
    TS_ASSERT_EQUALS(icnt1, 2);
    tn = test->getnode("storage");
    {
        tree_node *ch = test->getChild(tn);
        TS_ASSERT(test->getstring("name", svalue, ch));
        TS_ASSERT_SAME_DATA(svalue, "RESMGR5", 8);
        TS_ASSERT(test->getstring("type", svalue, ch));
        TS_ASSERT_SAME_DATA(svalue, "SM", 3);
        TS_ASSERT(test->getint("index", &ivalue, ch));
        TS_ASSERT_EQUALS(ivalue, 2);
        TS_ASSERT(test->getnode("smallidx", ch) != 0);
    }
    delete test;
}

void DefaultsTestSuite::testvarreplacement(void)
{
    char settings[] =
        "host = 192.168.1.100\n"
        "port = 5221\n"
        "prog = abcd\n"
        "module\n"
        "{\n"
        "exec = %prog% host=%host% port=%port% console\n"
        "\t  kill  =   killall %prog%    \n"
        "ps =\techo  host:%maximus%  ip:%host%\n"
        "}\n";
    defaults test(settings, sizeof(settings));
    test.load(0, 0);
    char svalue[512];
    TS_ASSERT(test.getstring("/module/exec", svalue) != 0);
    TS_ASSERT_SAME_DATA(svalue, "abcd host=192.168.1.100 port=5221 console", 42);
    TS_ASSERT(test.getstring("/module/kill", svalue) != 0);
    TS_ASSERT_SAME_DATA(svalue, "killall abcd", 13);
    pcre_free = 0;
    TS_ASSERT(test.getstring("/module/ps", svalue) != 0);
    TS_ASSERT_SAME_DATA(svalue, "echo  host:%maximus%  ip:192.168.1.100", 39);
}

void DefaultsTestSuite::testascbincoversion(void)
{
    char bin[512];
    char asc[1024];
    char mir[512];
    for (int i = 0; i < sizeof(bin); ++i)
        bin[i] = rand();
    bin2asc(bin, asc, sizeof(bin));
    for (int i = 0; i < sizeof(asc); ++i)
    {
        TS_ASSERT_LESS_THAN_EQUALS('a', asc[i]);
        TS_ASSERT_LESS_THAN_EQUALS(asc[i], 'z');
    }
    asc2bin(mir, asc, sizeof(mir));
    for (int i = 0; i < sizeof(bin); ++i)
        TS_ASSERT_EQUALS(bin[i], mir[i]);
    TS_TRACE("End");
}

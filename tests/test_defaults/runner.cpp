/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_defaults";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_DefaultsTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_defaults\test_defaults.h"

static DefaultsTestSuite suite_DefaultsTestSuite;

static CxxTest::List Tests_DefaultsTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_DefaultsTestSuite( "test_defaults/test_defaults.h", 6, "DefaultsTestSuite", suite_DefaultsTestSuite, Tests_DefaultsTestSuite );

static class TestDescription_suite_DefaultsTestSuite_testload : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_DefaultsTestSuite_testload() : CxxTest::RealTestDescription( Tests_DefaultsTestSuite, suiteDescription_DefaultsTestSuite, 9, "testload" ) {}
 void runTest() { suite_DefaultsTestSuite.testload(); }
} testDescription_suite_DefaultsTestSuite_testload;

static class TestDescription_suite_DefaultsTestSuite_testgetnode : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_DefaultsTestSuite_testgetnode() : CxxTest::RealTestDescription( Tests_DefaultsTestSuite, suiteDescription_DefaultsTestSuite, 10, "testgetnode" ) {}
 void runTest() { suite_DefaultsTestSuite.testgetnode(); }
} testDescription_suite_DefaultsTestSuite_testgetnode;

static class TestDescription_suite_DefaultsTestSuite_testgetChild : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_DefaultsTestSuite_testgetChild() : CxxTest::RealTestDescription( Tests_DefaultsTestSuite, suiteDescription_DefaultsTestSuite, 11, "testgetChild" ) {}
 void runTest() { suite_DefaultsTestSuite.testgetChild(); }
} testDescription_suite_DefaultsTestSuite_testgetChild;

static class TestDescription_suite_DefaultsTestSuite_testgetInt : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_DefaultsTestSuite_testgetInt() : CxxTest::RealTestDescription( Tests_DefaultsTestSuite, suiteDescription_DefaultsTestSuite, 12, "testgetInt" ) {}
 void runTest() { suite_DefaultsTestSuite.testgetInt(); }
} testDescription_suite_DefaultsTestSuite_testgetInt;

static class TestDescription_suite_DefaultsTestSuite_testgetString : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_DefaultsTestSuite_testgetString() : CxxTest::RealTestDescription( Tests_DefaultsTestSuite, suiteDescription_DefaultsTestSuite, 13, "testgetString" ) {}
 void runTest() { suite_DefaultsTestSuite.testgetString(); }
} testDescription_suite_DefaultsTestSuite_testgetString;

static class TestDescription_suite_DefaultsTestSuite_testgetDouble : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_DefaultsTestSuite_testgetDouble() : CxxTest::RealTestDescription( Tests_DefaultsTestSuite, suiteDescription_DefaultsTestSuite, 14, "testgetDouble" ) {}
 void runTest() { suite_DefaultsTestSuite.testgetDouble(); }
} testDescription_suite_DefaultsTestSuite_testgetDouble;

static class TestDescription_suite_DefaultsTestSuite_testgetdata : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_DefaultsTestSuite_testgetdata() : CxxTest::RealTestDescription( Tests_DefaultsTestSuite, suiteDescription_DefaultsTestSuite, 15, "testgetdata" ) {}
 void runTest() { suite_DefaultsTestSuite.testgetdata(); }
} testDescription_suite_DefaultsTestSuite_testgetdata;

static class TestDescription_suite_DefaultsTestSuite_testsetValue : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_DefaultsTestSuite_testsetValue() : CxxTest::RealTestDescription( Tests_DefaultsTestSuite, suiteDescription_DefaultsTestSuite, 16, "testsetValue" ) {}
 void runTest() { suite_DefaultsTestSuite.testsetValue(); }
} testDescription_suite_DefaultsTestSuite_testsetValue;

static class TestDescription_suite_DefaultsTestSuite_testsave : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_DefaultsTestSuite_testsave() : CxxTest::RealTestDescription( Tests_DefaultsTestSuite, suiteDescription_DefaultsTestSuite, 17, "testsave" ) {}
 void runTest() { suite_DefaultsTestSuite.testsave(); }
} testDescription_suite_DefaultsTestSuite_testsave;

static class TestDescription_suite_DefaultsTestSuite_testload2 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_DefaultsTestSuite_testload2() : CxxTest::RealTestDescription( Tests_DefaultsTestSuite, suiteDescription_DefaultsTestSuite, 18, "testload2" ) {}
 void runTest() { suite_DefaultsTestSuite.testload2(); }
} testDescription_suite_DefaultsTestSuite_testload2;

static class TestDescription_suite_DefaultsTestSuite_testcmdline : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_DefaultsTestSuite_testcmdline() : CxxTest::RealTestDescription( Tests_DefaultsTestSuite, suiteDescription_DefaultsTestSuite, 19, "testcmdline" ) {}
 void runTest() { suite_DefaultsTestSuite.testcmdline(); }
} testDescription_suite_DefaultsTestSuite_testcmdline;

static class TestDescription_suite_DefaultsTestSuite_testvarreplacement : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_DefaultsTestSuite_testvarreplacement() : CxxTest::RealTestDescription( Tests_DefaultsTestSuite, suiteDescription_DefaultsTestSuite, 20, "testvarreplacement" ) {}
 void runTest() { suite_DefaultsTestSuite.testvarreplacement(); }
} testDescription_suite_DefaultsTestSuite_testvarreplacement;

static class TestDescription_suite_DefaultsTestSuite_testascbincoversion : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_DefaultsTestSuite_testascbincoversion() : CxxTest::RealTestDescription( Tests_DefaultsTestSuite, suiteDescription_DefaultsTestSuite, 21, "testascbincoversion" ) {}
 void runTest() { suite_DefaultsTestSuite.testascbincoversion(); }
} testDescription_suite_DefaultsTestSuite_testascbincoversion;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

#include "test_defaults2.h"
#include <defaults.h>
#include <string>

void DefaultsTestSuite2::testload2(void) {
	//Test of load defaults
	defaults* test = load_defaults("test.cfg");
	TS_ASSERT(test != NULL);

	int set;
	double set1;
	int set2;
	std::string set3;

	test->setValue("/bk101/price", 33);
	set = test->getInt("/bk101/price");
	TS_ASSERT(set == 33);

	test->setValue("/bk139/price/discount", 34.90);
	set1 = test->getDouble("/bk139/price/discount");
	TS_ASSERT(set == 34.90);

	test->setValue("/bk119/serial_number", 11);
	set2 = test->getInt("/bk119/serial_number");
	TS_ASSERT(set2 == 11);

	test->setValue("/bk122/genre", "Action");
	set3 = test->getString("/bk122/genre");
	int value = atoi(set3.c_str());
	TS_ASSERT_EQUALS(value, 0);
}

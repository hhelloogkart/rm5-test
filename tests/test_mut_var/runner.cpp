/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_mut_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_MutVarTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_mut_var\test_mut_var.h"

static MutVarTestSuite suite_MutVarTestSuite;

static CxxTest::List Tests_MutVarTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_MutVarTestSuite( "test_mut_var/test_mut_var.h", 6, "MutVarTestSuite", suite_MutVarTestSuite, Tests_MutVarTestSuite );

static class TestDescription_suite_MutVarTestSuite_testExtractAndPack : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_MutVarTestSuite_testExtractAndPack() : CxxTest::RealTestDescription( Tests_MutVarTestSuite, suiteDescription_MutVarTestSuite, 10, "testExtractAndPack" ) {}
 void runTest() { suite_MutVarTestSuite.testExtractAndPack(); }
} testDescription_suite_MutVarTestSuite_testExtractAndPack;

static class TestDescription_suite_MutVarTestSuite_testNestedExtAndPack : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_MutVarTestSuite_testNestedExtAndPack() : CxxTest::RealTestDescription( Tests_MutVarTestSuite, suiteDescription_MutVarTestSuite, 11, "testNestedExtAndPack" ) {}
 void runTest() { suite_MutVarTestSuite.testNestedExtAndPack(); }
} testDescription_suite_MutVarTestSuite_testNestedExtAndPack;

static class TestDescription_suite_MutVarTestSuite_testCopyAndAssignment : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_MutVarTestSuite_testCopyAndAssignment() : CxxTest::RealTestDescription( Tests_MutVarTestSuite, suiteDescription_MutVarTestSuite, 12, "testCopyAndAssignment" ) {}
 void runTest() { suite_MutVarTestSuite.testCopyAndAssignment(); }
} testDescription_suite_MutVarTestSuite_testCopyAndAssignment;

static class TestDescription_suite_MutVarTestSuite_testMisc : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_MutVarTestSuite_testMisc() : CxxTest::RealTestDescription( Tests_MutVarTestSuite, suiteDescription_MutVarTestSuite, 13, "testMisc" ) {}
 void runTest() { suite_MutVarTestSuite.testMisc(); }
} testDescription_suite_MutVarTestSuite_testMisc;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

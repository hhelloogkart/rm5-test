#include <gen_var.h>
#include <arry_var.h>
#include <str_var.h>
#include <mut_var.h>
#include <bit_var.h>
#include <ttag_var.h>
#include <flex_var.h>
#include <link_var.h>
#include "test_mut_var.h"
#include "test_mut_var.pb.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

class dirty_checker : public mpt_baseshow
{
public:
	dirty_checker(mpt_var *myvar);
	virtual void setDirty(mpt_var *myvar);
	virtual void notDirty(mpt_var *myvar);
	int getStateAndReset();
protected:
	int state; // 0 - not called, 1 - dirty, 2 - notdirty, 3 - error
	mpt_var *var;
};

dirty_checker::dirty_checker(mpt_var *myvar) : state(0), var(myvar)
{
	myvar->addCallback(this);
}

void dirty_checker::setDirty(mpt_var *myvar)
{
	if (myvar == var) state |= 1;
	else state |= 4;
}

void dirty_checker::notDirty(mpt_var *myvar)
{
	if (myvar == var) state |= 2;
	else state |= 4;
}

int dirty_checker::getStateAndReset()
{
	int ret = state;
	state = 0;
	return ret;
}

class TestVar : public mutable_var
{
public:
	MUTABLECONST(TestVar)
	string_var<32> str;
	array_str_var asv;
	array_var<int> avi;
	array_var<double> avd;
	generic_var<int> gvit;
	generic_var<short> gvsh;
	generic_var<char> gvch;
	generic_var<unsigned int> gvui;
	generic_var<unsigned short> gvus;
	generic_var<unsigned char> gvuc;
	generic_var<long long> gvll;
	generic_var<unsigned long long> gvul;
	generic_var<float> gvft;
	generic_var<double> gvdb;
	bit_var<1> bv1;
	bit_var<5> bv5;
	ext_bit_var<5, 6> ebv5;
	r_bit_var<4> rbv4;
	r_ext_bit_var<5, 6> rebv5;
	ttag_var tt;
	array_var<short> avs;
	array_var<unsigned char> avc;
};

class NestedVar : public mutable_var
{
public:
	MUTABLECONST(NestedVar)
	flex_var<TestVar> flex;
	TestVar fixed;
	flex_var<string_var<32> > str;
	flex_var<array_str_var> asv;
	flex_var<array_var<int> > avi;
	flex_var<array_var<double> > avd;
	flex_var<generic_var<int> > gvit;
	flex_var<generic_var<short> > gvsh;
	flex_var<generic_var<char> > gvch;
	flex_var<generic_var<unsigned int> > gvui;
	flex_var<generic_var<unsigned short> > gvus;
	flex_var<generic_var<unsigned char> > gvuc;
	flex_var<generic_var<long long> > gvll;
	flex_var<generic_var<unsigned long long> > gvul;
	flex_var<generic_var<float> > gvft;
	flex_var<generic_var<double> > gvdb;
	flex_var<bit_var<1> > bv1;
	flex_var<bit_var<5> > bv5;
	flex_var<ext_bit_var<5, 6> > ebv5;
	flex_var<r_bit_var<4> > rbv4;
	flex_var<r_ext_bit_var<5, 6> > rebv5;
	flex_var<ttag_var> tt;
	flex_var<array_var<short> > avs;
	flex_var<array_var<unsigned char> > avc;
};

class TopVar : public complex_var
{
public:
	COMPLEXCONST(TopVar)
	void setRMDirty();

	NestedVar nest;
	bool dirty;
};

void TopVar::setRMDirty()
{
	dirty = true;
}

void fillValues(complex_var &c)
{
	int car = c.cardinal();
	for (int idx = 0; idx < car; ++idx)
	{
		value_var *vv = dynamic_cast<value_var *>(c[idx]);
		if (vv) vv->fromValue(rand() % 256);
		else {
			array_str_var *asv = dynamic_cast<array_str_var *>(c[idx]);
			if (asv) asv->fromValue(0, rand());
			else {
				array_value_var *av = dynamic_cast<array_value_var *>(c[idx]);
				int asz = rand() % 100;
				while (asz == 0) asz = rand() % 100;
				if (av)
				{
					av->resize(asz);
					for (int i = 0; i < asz; ++i)
						av->fromValue(i, rand());
				}
				else assert(false);
			}
		}
	}
}

inline void nullifystring(string_var<32> &str)
{
	char *sv = str;
	int i = 0;
	for (; sv[i] != 0 && i < str.size(); ++i);
	for (; i < str.size(); ++i)
		sv[i] = 0;
}

MutVarTestSuite::MutVarTestSuite()
{
	srand((unsigned int)time(NULL));
}

void MutVarTestSuite::testExtractAndPack(void)
{
	TestVar test;
	fillValues(test);
	nullifystring(test.str);
	test.bv1 = test.bv1.to_int() & 1;
	int sz = test.size();
	unsigned char *buf = new unsigned char[sz];
	outbuf ob;
	ob.set(buf, sz);
	test.output(ob);

	TS_ASSERT_EQUALS(ob.size(), sz);

	TestVar mirror;
	dirty_checker chk(&mirror);
	mirror.extract(sz, buf);
	TS_ASSERT_EQUALS(test, mirror);
	TS_ASSERT_EQUALS(chk.getStateAndReset(), 1);

	TS_ASSERT_EQUALS(mirror.extract(2, buf), 0);
	generic_var<int> dummy;
	mirror = dummy;
	// unchanged
	TS_ASSERT_EQUALS(test, mirror);
	TS_ASSERT_EQUALS(chk.getStateAndReset(), 0);

	mirror.extract(sz, buf);
	TS_ASSERT_EQUALS(chk.getStateAndReset(), 2);
}

void MutVarTestSuite::testNestedExtAndPack(void)
{
	NestedVar test;
	for (int idx = 0; idx < test.cardinal(); ++idx)
	{
		flex_complex_var *fcv = dynamic_cast<flex_complex_var *>(test[idx]);
		if (fcv) fcv->resize(rand() % 100);
	}
	for (int idx = 0; idx < test.flex.cardinal(); ++idx)
	{
		fillValues(*test.flex[idx]);
		nullifystring(test.flex[idx]->str);
		test.flex[idx]->bv1 = test.flex[idx]->bv1.to_int() & 1;
	}
	for (int idx = 1; idx < test.cardinal(); ++idx)
		fillValues(*dynamic_cast<complex_var *>(test[idx]));
	nullifystring(test.fixed.str);
	for (int idx = 0; idx < test.bv1.cardinal(); ++idx)
		*test.bv1[idx] = *test.bv1[idx] & 1;
	for (int idx = 0; idx < test.str.cardinal(); ++idx)
		nullifystring(*test.str[idx]);
	test.fixed.bv1 = test.fixed.bv1.to_int() & 1;

	int sz = test.size();
	unsigned char *buf = new unsigned char[sz];
	outbuf ob;
	ob.set(buf, sz);
	test.output(ob);

	TS_ASSERT_EQUALS(ob.size(), sz);

	NestedVar mirror;
	dirty_checker chk(&mirror);

	mirror.extract(sz, buf);
	delete[] buf;
	TS_ASSERT_EQUALS(test, mirror);
	TS_ASSERT_EQUALS(chk.getStateAndReset(), 3);

	for (int idx = 0; idx < test.flex.cardinal(); ++idx)
	{
		fillValues(*test.flex[idx]);
		nullifystring(test.flex[idx]->str);
		test.flex[idx]->bv1 = test.flex[idx]->bv1.to_int() & 1;
	}
	for (int idx = 1; idx < test.cardinal(); ++idx)
		fillValues(*dynamic_cast<complex_var *>(test[idx]));
	nullifystring(test.fixed.str);
	for (int idx = 0; idx < test.bv1.cardinal(); ++idx)
		*test.bv1[idx] = *test.bv1[idx] & 1;
	for (int idx = 0; idx < test.str.cardinal(); ++idx)
		nullifystring(*test.str[idx]);
	test.fixed.bv1 = test.fixed.bv1.to_int() & 1;
	TS_ASSERT_DIFFERS(test, mirror);

	sz = test.size();
	buf = new unsigned char[sz];
	ob.set(buf, sz);
	test.output(ob);
	TS_ASSERT_EQUALS(ob.size(), sz);
	mirror.extract(sz, buf);
	TS_ASSERT_EQUALS(test, mirror);
	TS_ASSERT_EQUALS(chk.getStateAndReset(), 3);
	mirror.extract(sz, buf);
	delete[] buf;
	TS_ASSERT_EQUALS(chk.getStateAndReset(), 2);

	for (int idx = 0; idx < test.cardinal(); ++idx)
	{
		flex_complex_var *fcv = dynamic_cast<flex_complex_var *>(test[idx]);
		if (fcv && fcv->cardinal() > 0) fcv->resize(fcv->cardinal() - 1);
	}
	sz = test.size();
	buf = new unsigned char[sz];
	ob.set(buf, sz);
	test.output(ob);
	TS_ASSERT_EQUALS(ob.size(), sz);
	mirror.extract(sz, buf);
	TS_ASSERT_EQUALS(test, mirror);
	TS_ASSERT_EQUALS(chk.getStateAndReset(), 3);
	delete[] buf;
}

void MutVarTestSuite::testCopyAndAssignment(void)
{
	TestVar test;
	fillValues(test);
	nullifystring(test.str);
	test.bv1 = test.bv1.to_int() & 1;

	TestVar mirror(test);
	TS_ASSERT_EQUALS(test, mirror);

	mpt_var &mptr = mirror;
	TS_ASSERT_EQUALS(test, mptr);

	test.setInvalid();
	TS_ASSERT_DIFFERS(test, mptr);

	test = mptr;
	TS_ASSERT_EQUALS(test, mptr);

	test.setInvalid();
	TS_ASSERT_DIFFERS(test, mptr);

	test = mirror;
	TS_ASSERT_EQUALS(test, mirror);

	link_var lv;
	lv = mirror;
	TS_ASSERT_EQUALS(test, lv);
}

void MutVarTestSuite::testMisc(void)
{
	TopVar test;
	test.dirty = false;
	fillValues(test.nest.fixed);
	nullifystring(test.nest.fixed.str);
	test.nest.fixed.bv1 = test.nest.fixed.bv1.to_int() & 1;
	TS_ASSERT_EQUALS(test.dirty, true);

	int sz = test.size();
	unsigned char *buf = new unsigned char[sz];
	outbuf ob;
	ob.set(buf, sz);
	test.output(ob);

	TS_ASSERT_EQUALS(ob.size(), sz);
	test.dirty = false;

	TopVar mirror;
	mirror.dirty = false;
	mirror.extract(sz, buf);
	TS_ASSERT_EQUALS(test, mirror);
	TS_ASSERT_EQUALS(mirror.dirty, false);

	TS_ASSERT_EQUALS(test.nest.rtti(), mirror.nest.rtti());
	TS_ASSERT_EQUALS(test.nest.fixed.rtti(), mirror.nest.fixed.rtti());
	TS_ASSERT_DIFFERS(test.nest.rtti(), mirror.nest.fixed.rtti());

	mirror.nest.flex.resize(rand() % 10);
	TS_ASSERT_EQUALS(mirror.dirty, true);
	TS_ASSERT_EQUALS(test.nest.fixed.rtti(), mirror.nest.flex[0]->rtti());

	TS_TRACE("End");
}

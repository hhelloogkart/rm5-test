#ifndef _TEST_MUT_VAR_H_DEF_
#define _TEST_MUT_VAR_H_DEF_

#include <cxxtest/TestSuite.h>

class MutVarTestSuite : public CxxTest::TestSuite
{
public:
    MutVarTestSuite();
    void testExtractAndPack(void);
    void testNestedExtAndPack(void);
    void testCopyAndAssignment(void);
    void testMisc(void);

};

#endif


TARGET = test_mut_var
HEADERS += test_mut_var.h
SOURCES += test_mut_var.cpp test_mut_var.pb.cc
DEFINES += PROTOBUF_USE_DLLS

include (../tests.pri)
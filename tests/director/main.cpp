#ifndef no_gui
#include "directorwindow.h"
#include <QApplication>
#else
#include "directorload.h"
#include <QCoreApplication>
#endif
#include <QFile>

int main(int argc, char *argv[])
{
	printf("App start.\n");
#ifndef no_gui
    QApplication a(argc, argv);
    DirectorWindow w;
#else
	QCoreApplication a(argc, argv);
	DirectorLoad w;
#endif
	printf("Test.\n");
    if (argc > 1)
    {
        for (int idx = 1; idx < argc; ++idx)
        {
            const QString arg(argv[idx]);
            if (QFile::exists(arg))
            {
				printf("Load.\n");
                w.load(arg);
                break;
            }
        }
    }

#ifndef no_gui
    w.show();
#endif

    return a.exec();
}
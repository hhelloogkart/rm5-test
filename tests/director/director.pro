TEMPLATE = app
CONFIG += warn_on
QT += core gui xml
greaterThan(QT_MAJOR_VERSION, 4){
  QT += widgets
}

TARGET = director
DESTDIR = ../bin
ZMQ_DIR = ../zmq

CONFIG(debug, debug|release) {
  LIB_SUFFIX=d
  TARGET = $${TARGET}$${LIB_SUFFIX}
}

win32:DEFINES += ZMQ_STATIC
INCLUDEPATH += . $${ZMQ_DIR}/include
DEPENDPATH += .
win32:LIBS += -L$${ZMQ_DIR}/lib -llibzmq$${LIB_SUFFIX} -lws2_32 -lIphlpapi
unix:LIBS += -L$${ZMQ_DIR}/lib -lzmq

HEADERS += directorwindow.h   clienttablemodel.h   errinjectdlg.h   qconsole.h   directorconsole.h   variable.h directorload.h
SOURCES += directorwindow.cpp clienttablemodel.cpp errinjectdlg.cpp qconsole.cpp directorconsole.cpp variable.cpp main.cpp directorload.cpp
FORMS += directorwindow.ui errinjectdlg.ui

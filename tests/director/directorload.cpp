#include "directorload.h"
#include <QCoreApplication>
#include <QFile>
#include <QDomElement>
#include <QDebug>
#include <QTimer>
#include <zmq.h>
#include <string.h>
#include <assert.h>

#include "clienttablemodel.h"

inline void sendBroadcast(void *socket, char v)
{
	zmq_msg_t msg;
	zmq_msg_init_size(&msg, 10);
	memcpy(zmq_msg_data(&msg), "director", 9);
	*(static_cast<char *>(zmq_msg_data(&msg)) + 9) = v;
	zmq_msg_send(&msg, socket, 0);
}
inline int get_oct(int x)
{
	return x >= '0'&&x <= '7' ? x - '0' : -1;
}

inline int get_hex(int x)
{

	if (x >= '0' && x <= '9')
		x -= '0';
	else if (x >= 'a' && x <= 'f')
		x = x - 'a' + 10;
	else if (x >= 'A' && x <= 'F')
		x = x - 'A' + 10;
	else
		x = -1;
	return x;
}

// convert a QString with C escape sequences in text to a CString
static QString process_C_escapes(QString inp)
{
	QString out;
	for (int idx = 0; idx < inp.size(); ++idx) {
		QChar ch = inp.at(idx);
		if (ch == '\\') {
			++idx;
			if (idx < inp.size()) {
				ch = inp.at(idx);
				switch (ch.toLatin1()) {
				case 'n': out += '\n'; break; /* newline */
				case 't': out += '\t'; break; /* tabspace */
				case 'v': out += '\v'; break; /* vertical tab */
				case 'b': out += '\b'; break; /* backspace */
				case 'r': out += '\r'; break; /* carriage return */
				case 'f': out += '\f'; break; /* formfeed */
				case 'a': out += '\a'; break; /* bell */
				case '\\': out += '\\'; break; /* backslash */
				case '\'': out += '\''; break; /* single quote */
				case '"': out += '\"'; break; /* double quote */
				case 'x':			/* string of hex characters */
				case 'X':
				{
					int i, val = 0;
					for (++idx; (idx < inp.size()) && ((i = get_hex(inp.at(idx).toLatin1())) > -1); ++idx)
						val = val * 16 + i;
					out += QChar(val);
					--idx; // last value non-number or no more, decrement back to compensate for loop increment
					break;
				}
				default:
					if (ch.isDigit()) {	/* treat as octal characters */
						int i, val = get_oct(ch.toLatin1());
						if (val >= 0) {
							for (++idx; (idx < inp.size()) && ((i = get_oct(inp.at(idx).toLatin1())) > -1); ++idx)
								val = val * 8 + i;
							out += QChar(val);
							--idx; // last value non-number or no more, decrement back to compensate for loop increment
						}
						else {
							qDebug() << "Illegal character escape sequence `\\" << ch << "'";
						}
					}
					else {
						qDebug() << "Illegal character escape sequence `\\" << ch << "'";
					}
					break;
				}
			}
		}
		else {
			out += ch;
		}
	}
	return out;
}

DirectorLoad::DirectorLoad(QObject *parent)
	:QObject(parent),
	zmq_context(zmq_ctx_new()),
	zmq_publisher(0),
	zmq_subscriber(zmq_socket(zmq_context, ZMQ_SUB)),
	zmq_reply(0),
	autostart(false),
	timer_id(0)
{
	zmq_setsockopt(zmq_subscriber, ZMQ_SUBSCRIBE, "vtime", 5);
	model = new ClientTableModel(this);
}

DirectorLoad::~DirectorLoad()
{
	if (zmq_reply) zmq_close(zmq_reply);
	zmq_close(zmq_subscriber);
	if (zmq_publisher) zmq_close(zmq_publisher);
	zmq_ctx_destroy(zmq_context);
}
void DirectorLoad::on_startButton_clicked()
{
	sendBroadcast(zmq_publisher, 0);
}

void DirectorLoad::on_stopButton_clicked()
{
	sendBroadcast(zmq_publisher, '\1');
	model->clearClientInfo();
}

QByteArray DirectorLoad::generateClientName()
{
	for (;;) {
		QString name("%1");
		name = name.arg(QString::number(name_gen_index), 3, '0');
		bool done = true;
		++name_gen_index;
		// check for conflict with loaded names
		QDomElement docElem = m_messagesDoc.documentElement();
		for (QDomElement elem = docElem.firstChildElement("connection");
			!elem.isNull(); elem = elem.nextSiblingElement("connection"))
		{
			if (elem.attribute("name") == name) {
				done = false;
				break;
			}
		}
		if (done) return name.toLatin1();
	}
}

void DirectorLoad::timerEvent(QTimerEvent *)
{
	zmq_pollitem_t item[2];
	item[0].socket = zmq_subscriber;
	item[0].events = ZMQ_POLLIN;
	item[1].socket = zmq_reply;
	item[1].events = ZMQ_POLLIN;
	for (int loopcnt = 0; loopcnt < 20 && zmq_poll(item, 2, 0) > 0; ++loopcnt) {
		if (item[0].revents == ZMQ_POLLIN) {
			zmq_msg_t msg;
			assert(zmq_msg_init(&msg) == 0);
			if (zmq_msg_recv(&msg, zmq_subscriber, ZMQ_DONTWAIT) > 0) {
				// look for client name
				const char *buf = (const char *)zmq_msg_data(&msg);
				QByteArray header(buf);
				if (header == "vtime") {
					// locate unique name
					const int start_of_unique_name = header.size() + 1 + sizeof(int) + sizeof(int) + 1;
					int end_of_unique_name;
					for (end_of_unique_name = start_of_unique_name; end_of_unique_name < static_cast<int>(zmq_msg_size(&msg)); ++end_of_unique_name)
						if (buf[end_of_unique_name] == ' ') break;
					QByteArray ba = QByteArray::fromRawData(buf + start_of_unique_name, end_of_unique_name - start_of_unique_name);

					// look for clientinfo
					ClientInfo *cinfo = model->findClientByName(ba);
					if (cinfo) {
						unsigned int cl_cap;
						memcpy(&(cinfo->vtime), buf + header.size() + 1, sizeof(int));
						memcpy(&cl_cap, buf + header.size() + 1 + sizeof(int), sizeof(int));
						const char flag = buf[header.size() + 1 + sizeof(int) + sizeof(int)];
						cinfo->time_running = ((flag & '\1') != '\0');
						if ((flag & '\2') != '\0') {
							/* Note: Does not handle multiple timeInterval destruction, use a list if this is needed */
							if (cl_cap == cinfo->estimate_cap)
								cinfo->estimate_cap = 0;
						}
						else {
							if ((cl_cap != 0) &&
								((cinfo->estimate_cap <= cinfo->vtime) ||
								(cinfo->estimate_cap > cl_cap))) {
								cinfo->estimate_cap = cl_cap;
							}
						}
						cinfo->changed = true;
						cinfo->last_packet_recv = QDateTime::currentDateTime();
					}
				}
			}
			zmq_msg_close(&msg);
		}
		if (item[1].revents == ZMQ_POLLIN) {
			zmq_msg_t msg;
			assert(zmq_msg_init(&msg) == 0);
			if (zmq_msg_recv(&msg, zmq_reply, ZMQ_DONTWAIT) > 0) {
				const char *buf = static_cast<const char *>(zmq_msg_data(&msg));
				if (*buf == 0) {
					// ** new client **

					// get client name, note client can be anonymous (no name)!
					QByteArray client(++buf);

					// make a unique name
					QByteArray ba = generateClientName();

					// subscribe to unique ID
					zmq_setsockopt(zmq_subscriber, ZMQ_SUBSCRIBE, ba.data(), ba.size());
					zmq_connect(zmq_subscriber, buf + client.size() + 1);
					zmq_msg_close(&msg);

					// reply with unique ID
					zmq_msg_init_size(&msg, ba.size());
					memcpy(zmq_msg_data(&msg), ba.data(), ba.size());
					zmq_msg_send(&msg, zmq_reply, 0);

					// add to internal storage
					ClientInfo *info = new ClientInfo;
					info->uniquename = ba;
					info->client = client;
					info->vtime = 0;
					info->time_running = 3;
					info->changed = false;
					info->last_packet_recv = QDateTime::currentDateTime();
					model->insertClientInfo(info);
				}
				else if (*buf == 1) {
					// ** new connection **
					bool done = false;

					// get connection name
					QByteArray connection(++buf);

					// get unique name
					QByteArray ba(buf + connection.size() + 1);

					// ready the document element
					QDomElement docElem = m_messagesDoc.documentElement();

					// look for clientinfo
					ClientInfo *client = model->findClientByName(ba);
					if (client) {
						// client unique name was matched
						model->insertConnection(client, connection);

						for (QDomElement elem = docElem.firstChildElement("connection"); !done &&
							!elem.isNull(); elem = elem.nextSiblingElement("connection"))
						{
							// data was found in DOM
							if ((elem.attribute("name") == connection) &&
								(!elem.hasAttribute("client") || (elem.attribute("client") == client->client)))
							{
								char flag;
								ConnectionInfo &cinfo = client->connections.back();
								QByteArray data1 = getConnectionProp(elem, &flag);
								getErrorInject(elem, client->vtime, cinfo);
								QByteArray data2 = cinfo.injectMessage(0);
								if (data2.size() > 0) {
									data1 += data2;
									flag |= 1 << 6;
								}
								if (data1.size() > 0) {
									zmq_msg_t outmsg;
									zmq_msg_init_size(&outmsg, data1.size() + 1);
									char *outbuf = static_cast<char *>(zmq_msg_data(&outmsg));
									*outbuf = flag; // flag
									memcpy(outbuf + 1, data1.data(), data1.size());
									zmq_msg_send(&outmsg, zmq_reply, 0);
									done = true;
								}
							}
						}
					}
					if (!done) {
						// did not find a match in the settings, still need to send out an ack
						zmq_msg_t outmsg;
						zmq_msg_init_size(&outmsg, 1);
						char *outbuf = static_cast<char *>(zmq_msg_data(&outmsg));
						*outbuf = 0; // flag
						zmq_msg_send(&outmsg, zmq_reply, 0);
					}
					if (autostart) {
						bool ready = true;
						// auto-start when all connections are connected
						for (QDomElement elem = docElem.firstChildElement("connection"); !elem.isNull() && ready; elem = elem.nextSiblingElement("connection"))
						{
							QString nm = elem.attribute("name");
							QString cl = elem.attribute("client");
							if (!nm.isEmpty())
							{
								if (cl.isEmpty())
									ready &= model->getConnectionCountByName(nm.toLatin1()) > 1;
								else
									ready &= model->hasClientConnectionByName(nm.toLatin1(), cl.toLatin1());
							}
						}
						ready &= model->getClientCount() > 1;
						if (ready) {
							printf("Button clicked");
							QTimer::singleShot(0, this, [this]()->void {on_startButton_clicked(); });
						}
					}
				} /* flag=1 */
			} /* recv */
			zmq_msg_close(&msg);
		}
	}

	model->updateChanges();
	if (model->checkClientsExited()) {
		QTimer::singleShot(0, this, [this]()->void {on_stopButton_clicked(); });
	}
}

bool DirectorLoad::load(const QString &filename)
{
	QFile file(filename);
	if (!file.open(QIODevice::ReadOnly)) {
		qCritical("Load Error. Unable to open file %s", qUtf8Printable(filename));
		return false;
	}

	QString errorMsg;
	int errorLine, errorColumn;
	if (!m_messagesDoc.setContent(&file, &errorMsg, &errorLine, &errorColumn)) {
		qCritical("Load Error. Error loading XML in line %d, column %d: %s.", errorLine, errorColumn, qUtf8Printable(errorMsg));
		file.close();
		return false;
	}
	file.close();

	QDomElement docElem = m_messagesDoc.documentElement();
	if (docElem.tagName() != QLatin1String("director")) {
		qCritical("Load Error. File format error, expected element \"messages\", found \"%s\".", docElem.tagName());
		return false;
	}

	QDomElement elem = docElem.firstChildElement("pub");
	if (!elem.isNull() &&
		!(pub_address == elem.text())) {
		pub_address = elem.text().toLatin1();
		if (zmq_publisher) zmq_close(zmq_publisher);
		zmq_publisher = zmq_socket(zmq_context, ZMQ_PUB);
		if (zmq_bind(zmq_publisher, pub_address) == -1) {
			zmq_close(zmq_publisher);
			zmq_publisher = 0;
		}
	}

	elem = docElem.firstChildElement("rep");
	if (!elem.isNull() &&
		!(rep_address == elem.text())) {
		rep_address = elem.text().toLatin1();
		if (zmq_reply) zmq_close(zmq_reply);
		zmq_reply = zmq_socket(zmq_context, ZMQ_REP);
		if (zmq_bind(zmq_reply, rep_address) == 1) {
			zmq_close(zmq_reply);
			zmq_reply = 0;
		}
	}

	elem = docElem.firstChildElement("autostart");
	autostart = !elem.isNull() && (elem.text() == "1");

	if (autostart) printf("Autostart true. Loaded xml.");
	if (zmq_reply && zmq_publisher) {
		if (!timer_id) timer_id = startTimer(10);
		return true;
	}
	else {
		if (timer_id) {
			killTimer(timer_id);
			timer_id = 0;
		}
		return false;
	}
}

QByteArray DirectorLoad::getConnectionProp(QDomElement elem, char *flag)
{
	QByteArray result;
	float fltval;
	int intval;
	bool ok;
	*flag = 0;

	QDomElement child = elem.firstChildElement("recv_a");
	if (!child.isNull()) {
		fltval = child.text().toFloat(&ok);
		if (ok) {
			result.append(QByteArray::fromRawData(reinterpret_cast<char *>(&fltval), sizeof(float)));
			*flag |= 1; // bit 0
		}
	}
	child = elem.firstChildElement("recv_b");
	if (!child.isNull()) {
		intval = child.text().toInt(&ok);
		if (ok) {
			result.append(QByteArray::fromRawData(reinterpret_cast<char *>(&intval), sizeof(float)));
			*flag |= (1 << 1); // bit 1
		}
	}
	child = elem.firstChildElement("send_a");
	if (!child.isNull()) {
		fltval = child.text().toFloat(&ok);
		if (ok) {
			result.append(QByteArray::fromRawData(reinterpret_cast<char *>(&fltval), sizeof(float)));
			*flag |= (1 << 2); // bit 2
		}
	}
	child = elem.firstChildElement("send_b");
	if (!child.isNull()) {
		intval = child.text().toInt(&ok);
		if (ok) {
			result.append(QByteArray::fromRawData(reinterpret_cast<char *>(&intval), sizeof(float)));
			*flag |= (1 << 3); // bit 3
		}
	}
	child = elem.firstChildElement("baud");
	if (!child.isNull()) {
		intval = child.text().toInt(&ok);
		if (ok) {
			result.append(QByteArray::fromRawData(reinterpret_cast<char *>(&intval), sizeof(float)));
			*flag |= (1 << 4); // bit 4
		}
	}
	child = elem.firstChildElement("cap");
	if (!child.isNull()) {
		intval = child.text().toInt(&ok);
		if (ok) {
			result.append(QByteArray::fromRawData(reinterpret_cast<char *>(&intval), sizeof(float)));
			*flag |= (1 << 5); // bit 5
		}
	}
	return result;
}

void DirectorLoad::getErrorInject(QDomElement elem, unsigned int clienttime, ConnectionInfo &connection)
{
	for (elem = elem.firstChildElement("error"); !elem.isNull(); elem = elem.nextSiblingElement("error"))
	{
		const char *errname[] = {
			"drop", "freeze", "replace", "lengthen", "shorten", "duplicate", "fragment", "delay", ""
		};
		QByteArray result;
		InjectInfo inject;
		QString match;
		QString data;
		QString errtype = elem.attribute("type");
		inject.error = ~0;
		for (unsigned char cnt = 0; errname[cnt][0] != '\0'; ++cnt)
			if (errtype == errname[cnt]) {
				inject.error = cnt;
				break;
			}
		if (inject.error == ~'\xFF') continue;
		QDomElement errelem = elem.firstChildElement("end");
		if (!errelem.isNull()) {
			bool ok;
			inject.end_time = errelem.text().toULong(&ok);
			if (!ok) inject.end_time = 0;
			else if (inject.end_time < clienttime) continue;
		}
		else inject.end_time = ~0;
		errelem = elem.firstChildElement("start");
		if (!errelem.isNull()) {
			bool ok;
			inject.start_time = errelem.text().toULong(&ok);
			if (!ok) inject.start_time = 0;
		}
		else inject.start_time = 0;
		if (inject.end_time > inject.start_time) continue;
		errelem = elem.firstChildElement("streak");
		if (!errelem.isNull()) {
			bool ok;
			inject.streak = errelem.text().toUShort(&ok);
			if (!ok) inject.streak = 0;
		}
		else inject.streak = 0;
		errelem = elem.firstChildElement("chance");
		if (!errelem.isNull()) {
			bool ok;
			inject.probability = static_cast<unsigned char>(errelem.text().toUShort(&ok));
			if (!ok) inject.probability = 0;
		}
		else inject.probability = 0;
		errelem = elem.firstChildElement("match");
		if (!errelem.isNull()) {
			match = errelem.text();
			inject.match_len = match.size();
		}
		else inject.match_len = 0;
		switch (inject.error) {
		case 0: // drop
			inject.data_len = 0;
			break;
		case 1: // freeze
			inject.data_len = 0;
			break;
		case 2: // replace
		case 3: // lengthen
			errelem = elem.firstChildElement("pos");
			if (!errelem.isNull()) {
				bool ok;
				inject.value1 = errelem.text().toInt(&ok);
				if (!ok) inject.value1 = 0;
			}
			else inject.value1 = 0;
			errelem = elem.firstChildElement("len");
			if (!errelem.isNull()) {
				bool ok;
				inject.value2 = errelem.text().toInt(&ok);
				if (!ok) inject.value2 = 1;
			}
			else inject.value2 = 1;
			errelem = elem.firstChildElement("data");
			if (!errelem.isNull()) {
				data = process_C_escapes(errelem.text());
				inject.data_len = data.size();
			}
			else inject.data_len = 0;
			break;
		case 4: // shorten
			errelem = elem.firstChildElement("pos");
			if (!errelem.isNull()) {
				bool ok;
				inject.value1 = errelem.text().toInt(&ok);
				if (!ok) inject.value1 = -1;
			}
			else inject.value1 = -1;
			errelem = elem.firstChildElement("len");
			if (!errelem.isNull()) {
				bool ok;
				inject.value2 = errelem.text().toInt(&ok);
				if (!ok) inject.value2 = 1;
			}
			else inject.value2 = 1;
			inject.data_len = 0;
			break;
		case 5: // duplicate
			errelem = elem.firstChildElement("copy");
			if (!errelem.isNull()) {
				bool ok;
				inject.value1 = errelem.text().toInt(&ok);
				if (!ok) inject.value1 = 1;
			}
			else inject.value1 = 1;
			errelem = elem.firstChildElement("delay");
			if (!errelem.isNull()) {
				bool ok;
				inject.value2 = errelem.text().toInt(&ok);
				if (!ok) inject.value2 = 1;
			}
			else inject.value2 = 1;
			inject.data_len = 0;
			break;
		case 6: // fragment
			errelem = elem.firstChildElement("piece");
			if (!errelem.isNull()) {
				bool ok;
				inject.value1 = errelem.text().toInt(&ok);
				if (!ok) inject.value1 = 10;
			}
			else inject.value1 = 10;
			errelem = elem.firstChildElement("delay");
			if (!errelem.isNull()) {
				bool ok;
				inject.value2 = errelem.text().toInt(&ok);
				if (!ok) inject.value2 = 1;
			}
			else inject.value2 = 1;
			inject.data_len = 0;
			break;
		case 7: // delay
			errelem = elem.firstChildElement("delay");
			if (!errelem.isNull()) {
				bool ok;
				inject.value1 = errelem.text().toInt(&ok);
				if (!ok) inject.value1 = 1;
			}
			else inject.value1 = 1;
			inject.data_len = 0;
			break;
		}
		result.append(QByteArray::fromRawData(reinterpret_cast<char *>(&inject), sizeof(inject)));
		if (inject.match_len > 0)
			result.append(match.toLatin1());
		if (inject.data_len > 0)
			result.append(data.toLatin1());
		model->insertInject(&connection, result);
	}
}
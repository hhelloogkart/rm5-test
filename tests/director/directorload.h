#include <QDomDocument>
#include <QObject>
#include "ui_directorwindow.h"

struct ClientInfo;
struct ConnectionInfo;
class ClientTableModel;

class DirectorLoad : public QObject
{
public:
	DirectorLoad(QObject *parent = 0);
	~DirectorLoad();
	bool load(const QString &filename);

protected:
	void timerEvent(QTimerEvent * evt);
	ClientTableModel *model;

private:
	void *zmq_context;
	void *zmq_publisher;
	void *zmq_subscriber;
	void *zmq_reply;
	QByteArray pub_address;
	QByteArray rep_address;
	QDomDocument m_messagesDoc;
	bool autostart;
	int name_gen_index;
	int timer_id;

	void on_startButton_clicked();
	void on_stopButton_clicked();
	void getErrorInject(QDomElement elem, unsigned int clienttime, ConnectionInfo &connection);
	QByteArray generateClientName();
	QByteArray getConnectionProp(QDomElement elem, char *flag);
};

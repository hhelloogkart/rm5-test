#ifndef _TEST_GEN_VAR_H_DEF_
#define _TEST_GEN_VAR_H_DEF_

#include <cxxtest/TestSuite.h>

class BitVarTestSuite : public CxxTest::TestSuite
{
public:
    void testExtractAndPack(void);
    void testExtExtractAndPack(void);
    void testStream(void);
    void testCopying(void);
    void testRtti(void);
    void testMath(void);
    void testRMath(void);
};

#endif


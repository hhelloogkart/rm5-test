#include "test_bit_var.h"
#include <bit_var.h>
#include <cplx_var.h>
#include <link_var.h>
#include <mpt_show.h>
#include <outbuf.h>
#include <util/utilcore.h>
#include <sstream>
#include <bitset>
#include <vector>
#include <string.h>
#include <limits.h>

#define TS_ASSERT_NOT(x) TS_ASSERT(!(x))

class dirty_checker : public mpt_baseshow
{
public:
	dirty_checker(mpt_var *myvar);
	virtual void setDirty(mpt_var *myvar);
	virtual void notDirty(mpt_var *myvar);
	int getStateAndReset();
protected:
	int state; // 0 - not called, 1 - dirty, 2 - notdirty, 3 - error
	mpt_var *var;
};

dirty_checker::dirty_checker(mpt_var *myvar) : state(0), var(myvar)
{
	myvar->addCallback(this);
}

void dirty_checker::setDirty(mpt_var *myvar)
{
	if (myvar == var) state = 1;
	else state = 3;
}

void dirty_checker::notDirty(mpt_var *myvar)
{
	if (myvar == var) state = 2;
	else state = 3;
}

int dirty_checker::getStateAndReset()
{
	int ret = state;
	state = 0;
	return ret;
}

struct bithelper
{
	std::bitset<64> bits;
	int position;
	void reset()
	{
		position = 0;
		bits.reset();
	}
	void append(unsigned int val, int bitsz, bool flip)
	{
		if (flip)
		{
			position += bitsz;
			for (int cnt = 0; cnt < bitsz; ++cnt, val /= 2)
				bits.set(64-position+cnt, val % 2);
		}
		else
		{
			for (int cnt = 0; cnt < bitsz; ++cnt, val /= 2, ++position)
				bits.set(position, val % 2);
		}
	}
	char byte(int byteidx, bool flip = false) const
	{
		unsigned long long result = bits.to_ullong();
		if (flip)
			result >>= 8 * (7 - byteidx);
		else
			result >>= 8 * byteidx;
		result &= 0xFF;
		return static_cast<char>(result);
	}
	char extbyte(int byteidx, int bytesz, bool flip = false)
	{
		char result = 0;
		if (flip)
		{
			int sidx = 63 - (byteidx * bytesz);
			for (int idx = 0; idx < bytesz; ++idx, --sidx)
			{
				result <<= 1;
				result += bits.test(sidx);
			}
		}
		else
		{
			int sidx = byteidx * bytesz + bytesz - 1;
			for (int idx = 0; idx < bytesz; ++idx, --sidx)
			{
				result <<= 1;
				result += bits.test(sidx);
			}
		}
		return result;
	}
};

class Btest1 : public complex_var
{
public:
	COMPLEXCONST(Btest1)
	bit_var<3> b1;
	bit_var<3> b2;
	bit_var<3> b3;
	bit_var<3> b4;
	bit_var<3> b5; // Byte0,1 15 bits, 1 bit not used
	generic_var<int> it1; // Byte 2,3,4,5
	bit_var<4> b6;
	bit_var<2> b7; // Byte 6
	r_bit_var<3> rb1;
	r_bit_var<3> rb2;
	r_bit_var<3> rb3;
	r_bit_var<3> rb4;
	r_bit_var<3> rb5; // Byte 7,8, 1 bit not used
	bit_var<12> b8; // Byte 9, 10
	bit_var<24> b9; // Byte 11, 12, 13, 4 bit not used
	r_bit_var<2> rb6; // Byte 14
	r_bit_var<10> rb7; // Byte 15
	r_bit_var<24> rb8; // Byte 16, 17, 18
	r_bit_var<4> rb9; // nil
};

class Btest2 : public complex_var
{
public:
	COMPLEXCONST(Btest2)
	ext_bit_var<4, 6> b1;
	ext_bit_var<4, 6> b2;
	ext_bit_var<4, 6> b3;
	ext_bit_var<4, 6> b4;
	ext_bit_var<4, 6> b5; // Byte 0,1,2,3 20bits
	r_ext_bit_var<4, 6> rb1;
	r_ext_bit_var<4, 6> rb2;
	r_ext_bit_var<4, 6> rb3;
	r_ext_bit_var<4, 6> rb4;
	r_ext_bit_var<4, 6> rb5; // Byte 4,5,6,7 20bits
	ext_bit_var<11, 6> b6; // Byte 8,9
	ext_bit_var<22, 6> b7; // Byte 10,11,12,13 3 bits not used
	r_ext_bit_var<10, 6> rb6; // Byte 14,15
	r_ext_bit_var<21, 6> rb7; // Byte 16,17,18,19
	r_ext_bit_var<5, 6> rb8; // nill
};

class Btest3 : public complex_var
{
public:
	RUNTIME_TYPE(t)
	mpt_var* getNext()
	{
		return this + 1;
	}
	Btest3();
	Btest3(const Btest3 &right);
	bit_var<3> b1;
	bit_var<3> b2;
	bit_var<3> b3;
	bit_var<3> b4;
	bit_var<3> b5;
	r_bit_var<3> rb1;
	r_bit_var<3> rb2;
	r_bit_var<3> rb3;
	r_bit_var<3> rb4;
	r_bit_var<3> rb5; // Byte0,1 15 bits, 1 bit not used
};

Btest3::Btest3()
{
	pop();
}

Btest3::Btest3(const Btest3 &right) :
	b1(right.b1),
	b2(right.b2),
	b3(right.b3),
	b4(right.b4),
	b5(right.b5),
	rb1(right.rb1),
	rb2(right.rb2),
	rb3(right.rb3),
	rb4(right.rb4),
	rb5(right.rb5)
{
	pop();
}

void populate(bithelper &bh, std::vector<int> &vec, int bitsz, bool flip = false)
{
	const int mask = (1 << bitsz) - 1;
	const int value = rand() & mask;
	bh.append(value, bitsz, flip);
	vec.push_back(value);
}

void BitVarTestSuite::testExtractAndPack(void)
{
	std::vector<int> values;
	Btest1 bt1;
	dirty_checker chk(&bt1);
	unsigned char buf[19];
	outbuf ob;
	bithelper bh;
	ob.set(buf, sizeof(buf));
	bh.reset();
	populate(bh, values, 3);
	populate(bh, values, 3);
	populate(bh, values, 3);
	populate(bh, values, 3);
	populate(bh, values, 3);
	ob += bh.byte(0);
	ob += bh.byte(1);
	bh.reset();
	{
		const int it = rand();
		values.push_back(it);
		const unsigned char *ptr = reinterpret_cast<const unsigned char *>(&it);
		ob += *ptr;
		ob += *(++ptr);
		ob += *(++ptr);
		ob += *(++ptr);
	}
	populate(bh, values, 4);
	populate(bh, values, 2);
	ob += bh.byte(0);
	bh.reset();
	populate(bh, values, 3, true);
	populate(bh, values, 3, true);
	populate(bh, values, 3, true);
	populate(bh, values, 3, true);
	populate(bh, values, 3, true);
	ob += bh.byte(0, true);
	ob += bh.byte(1, true);
	bh.reset();
	populate(bh, values, 12);
	populate(bh, values, 24);
	ob += bh.byte(0);
	ob += bh.byte(1);
	ob += bh.byte(2);
	ob += bh.byte(3);
	ob += bh.byte(4);
	bh.reset();
	populate(bh, values, 2, true);
	populate(bh, values, 10, true);
	populate(bh, values, 24, true);
	populate(bh, values, 4, true);
	ob += bh.byte(0, true);
	ob += bh.byte(1, true);
	ob += bh.byte(2, true);
	ob += bh.byte(3, true);
	ob += bh.byte(4, true);
	bh.reset();
	bt1.extract(ob.size(), buf);
	TS_ASSERT_EQUALS(chk.getStateAndReset(), 1);
	for (int idx = 0; idx < bt1.cardinal(); ++idx)
		TS_ASSERT_EQUALS(static_cast<value_var *>(bt1[idx])->to_int(), values[idx]);

	TS_ASSERT_EQUALS(bt1.b1.size(), 1);
	TS_ASSERT_EQUALS(bt1.b2.size(), 0);
	TS_ASSERT_EQUALS(bt1.b3.size(), 1);
	TS_ASSERT_EQUALS(bt1.b4.size(), 0);
	TS_ASSERT_EQUALS(bt1.b5.size(), 0);
	TS_ASSERT_EQUALS(bt1.b6.size(), 1);
	TS_ASSERT_EQUALS(bt1.b7.size(), 0);
	TS_ASSERT_EQUALS(bt1.rb1.size(), 1);
	TS_ASSERT_EQUALS(bt1.rb2.size(), 0);
	TS_ASSERT_EQUALS(bt1.rb3.size(), 1);
	TS_ASSERT_EQUALS(bt1.rb4.size(), 0);
	TS_ASSERT_EQUALS(bt1.rb5.size(), 0);
	TS_ASSERT_EQUALS(bt1.b8.size(), 2);
	TS_ASSERT_EQUALS(bt1.b9.size(), 3);
	TS_ASSERT_EQUALS(bt1.rb6.size(), 1);
	TS_ASSERT_EQUALS(bt1.rb7.size(), 1);
	TS_ASSERT_EQUALS(bt1.rb8.size(), 3);
	TS_ASSERT_EQUALS(bt1.rb9.size(), 0);
}

void BitVarTestSuite::testExtExtractAndPack(void)
{
	std::vector<int> values;
	Btest2 bt1;
	dirty_checker chk(&bt1);
	unsigned char buf[20];
	outbuf ob;
	bithelper bh;
	ob.set(buf, sizeof(buf));
	bh.reset();
	populate(bh, values, 4);
	populate(bh, values, 4);
	populate(bh, values, 4);
	populate(bh, values, 4);
	populate(bh, values, 4);
	ob += bh.extbyte(0, 6);
	ob += bh.extbyte(1, 6);
	ob += bh.extbyte(2, 6);
	ob += bh.extbyte(3, 6);
	bh.reset();
	populate(bh, values, 4, true);
	populate(bh, values, 4, true);
	populate(bh, values, 4, true);
	populate(bh, values, 4, true);
	populate(bh, values, 4, true);
	ob += bh.extbyte(0, 6, true);
	ob += bh.extbyte(1, 6, true);
	ob += bh.extbyte(2, 6, true);
	ob += bh.extbyte(3, 6, true);
	bh.reset();
	populate(bh, values, 11);
	populate(bh, values, 22);
	ob += bh.extbyte(0, 6);
	ob += bh.extbyte(1, 6);
	ob += bh.extbyte(2, 6);
	ob += bh.extbyte(3, 6);
	ob += bh.extbyte(4, 6);
	ob += bh.extbyte(5, 6);
	bh.reset();
	populate(bh, values, 10, true);
	populate(bh, values, 21, true);
	populate(bh, values, 5, true);
	ob += bh.extbyte(0, 6, true);
	ob += bh.extbyte(1, 6, true);
	ob += bh.extbyte(2, 6, true);
	ob += bh.extbyte(3, 6, true);
	ob += bh.extbyte(4, 6, true);
	ob += bh.extbyte(5, 6, true);

	bt1.extract(ob.size(), buf);
	TS_ASSERT_EQUALS(chk.getStateAndReset(), 1);
	for (int idx = 0; idx < bt1.cardinal(); ++idx)
		TS_ASSERT_EQUALS(static_cast<value_var *>(bt1[idx])->to_int(), values[idx]);

	TS_ASSERT_EQUALS(bt1.b1.size(), 1);
	TS_ASSERT_EQUALS(bt1.b2.size(), 1);
	TS_ASSERT_EQUALS(bt1.b3.size(), 0);
	TS_ASSERT_EQUALS(bt1.b4.size(), 1);
	TS_ASSERT_EQUALS(bt1.b5.size(), 1);
	TS_ASSERT_EQUALS(bt1.rb1.size(), 1);
	TS_ASSERT_EQUALS(bt1.rb2.size(), 1);
	TS_ASSERT_EQUALS(bt1.rb3.size(), 0);
	TS_ASSERT_EQUALS(bt1.rb4.size(), 1);
	TS_ASSERT_EQUALS(bt1.rb5.size(), 1);
	TS_ASSERT_EQUALS(bt1.b6.size(), 2);
	TS_ASSERT_EQUALS(bt1.b7.size(), 4);
	TS_ASSERT_EQUALS(bt1.rb6.size(), 2);
	TS_ASSERT_EQUALS(bt1.rb7.size(), 4);
	TS_ASSERT_EQUALS(bt1.rb8.size(), 0);
}

void populate(Btest1 &bh, std::vector<int> &vec, int bitsz)
{
	const int mask = (1 << bitsz) - 1;
	const int value = rand() & mask;
	static_cast<value_var *>(bh[vec.size()])->fromValue(value);
	vec.push_back(value);
}

void BitVarTestSuite::testStream(void)
{
	std::vector<int> values;
	Btest1 bh;
	populate(bh, values, 3);
	populate(bh, values, 3);
	populate(bh, values, 3);
	populate(bh, values, 3);
	populate(bh, values, 3);
	values.push_back(rand());
	bh.it1 = values.back();
	populate(bh, values, 4);
	populate(bh, values, 2);
	populate(bh, values, 3);
	populate(bh, values, 3);
	populate(bh, values, 3);
	populate(bh, values, 3);
	populate(bh, values, 3);
	populate(bh, values, 12);
	populate(bh, values, 24);
	populate(bh, values, 2);
	populate(bh, values, 10);
	populate(bh, values, 24);
	populate(bh, values, 4);
	for (int idx = 0; idx < bh.cardinal(); ++idx)
		TS_ASSERT_EQUALS(static_cast<value_var *>(bh[idx])->to_int(), values[idx]);

	std::ostringstream oss;
	oss << bh;

	Btest1 bi;

	std::istringstream iss(oss.str());
	iss >> bi;

	TS_ASSERT_EQUALS(bh, bi);
	for (int idx = 0; idx < bi.cardinal(); ++idx)
		TS_ASSERT_EQUALS(static_cast<value_var *>(bi[idx])->to_int(), values[idx]);

	std::ostringstream oss1;
	for (int idx = 0; idx < values.size(); ++idx)
	{
		if (idx != 0) oss1 << ' ';
		oss1 << values[idx];
	}
	TS_ASSERT_EQUALS(oss.str(), oss1.str());

	// bit_var does not clamp down on the number of bits is can represent
	std::ostringstream oss2;
	for (int idx = 0; idx < values.size(); ++idx)
	{
		if (idx != 0) oss2 << ' ';
		values[idx] = rand();
		oss2 << values[idx];
	}
	iss.str(oss2.str());
	iss.clear();
	iss >> bh;
	for (int idx = 0; idx < bh.cardinal(); ++idx)
		TS_ASSERT_EQUALS(static_cast<value_var *>(bh[idx])->to_int(), values[idx]);
	std::ostringstream oss3;
	oss3 << bh;
	TS_ASSERT_EQUALS(oss2.str(), oss3.str());
}

void BitVarTestSuite::testCopying(void)
{
	std::vector<int> values;
	Btest1 bh;
	populate(bh, values, 3);
	populate(bh, values, 3);
	populate(bh, values, 3);
	populate(bh, values, 3);
	populate(bh, values, 3);
	values.push_back(rand());
	bh.it1 = values.back();
	populate(bh, values, 4);
	populate(bh, values, 2);
	populate(bh, values, 3);
	populate(bh, values, 3);
	populate(bh, values, 3);
	populate(bh, values, 3);
	populate(bh, values, 3);
	populate(bh, values, 12);
	populate(bh, values, 24);
	populate(bh, values, 2);
	populate(bh, values, 10);
	populate(bh, values, 24);
	populate(bh, values, 4);

	Btest1 bi(bh);
	TS_ASSERT_EQUALS(bh, bi);

	bi.setInvalid();
	TS_ASSERT_DIFFERS(bh, bi);

	bi = bh;
	TS_ASSERT_EQUALS(bh, bi);

	bit_var<25> bv1;
	bv1 = rand();
	bit_var<25> bv2(bv1);
	TS_ASSERT_EQUALS(bv1, bv2);

	mpt_var &mv = bv2;
	TS_ASSERT_EQUALS(bv1, mv);
	TS_ASSERT_EQUALS(mv, bv1);

	link_var lv;
	lv = mv;
	TS_ASSERT_EQUALS(bv1, lv);
	TS_ASSERT_EQUALS(lv, bv1);

	bv2 = rand();

	TS_ASSERT_DIFFERS(bv1, bv2);
	TS_ASSERT_DIFFERS(bv1, mv);
	TS_ASSERT_DIFFERS(mv, bv1);
	TS_ASSERT_DIFFERS(bv1, lv);
	TS_ASSERT_DIFFERS(lv, bv1);

	bv1 = mv;
	TS_ASSERT_EQUALS(bv1, bv2);

	r_bit_var<30> rbv1;
	rbv1 = rand();
	r_bit_var<30> rbv2(rbv1);
	TS_ASSERT_EQUALS(rbv1, rbv2);

	mpt_var &rmv = rbv2;
	TS_ASSERT_EQUALS(rbv1, rmv);
	TS_ASSERT_EQUALS(rmv, rbv1);

	link_var rlv;
	rlv = rmv;
	TS_ASSERT_EQUALS(rbv1, rlv);
	TS_ASSERT_EQUALS(rlv, rbv1);

	rbv2 = rand();

	TS_ASSERT_DIFFERS(rbv1, rbv2);
	TS_ASSERT_DIFFERS(rbv1, rmv);
	TS_ASSERT_DIFFERS(rmv, rbv1);
	TS_ASSERT_DIFFERS(rbv1, rlv);
	TS_ASSERT_DIFFERS(rlv, rbv1);

	rbv1 = rmv;
	TS_ASSERT_EQUALS(rbv1, rbv2);

	ext_bit_var<25,6> ebv1;
	ebv1 = rand();
	ext_bit_var<25,6> ebv2(ebv1);
	TS_ASSERT_EQUALS(ebv1, ebv2);

	mpt_var &emv = ebv2;
	TS_ASSERT_EQUALS(ebv1, emv);
	TS_ASSERT_EQUALS(emv, ebv1);

	link_var elv;
	elv = emv;
	TS_ASSERT_EQUALS(ebv1, elv);
	TS_ASSERT_EQUALS(elv, ebv1);

	ebv2 = rand();

	TS_ASSERT_DIFFERS(ebv1, ebv2);
	TS_ASSERT_DIFFERS(ebv1, emv);
	TS_ASSERT_DIFFERS(emv, ebv1);
	TS_ASSERT_DIFFERS(ebv1, elv);
	TS_ASSERT_DIFFERS(elv, ebv1);

	ebv1 = emv;
	TS_ASSERT_EQUALS(ebv1, ebv2);

	r_ext_bit_var<30,6> erbv1;
	erbv1 = rand();
	r_ext_bit_var<30,6> erbv2(erbv1);
	TS_ASSERT_EQUALS(erbv1, erbv2);

	mpt_var &ermv = erbv2;
	TS_ASSERT_EQUALS(erbv1, ermv);
	TS_ASSERT_EQUALS(ermv, erbv1);

	link_var erlv;
	erlv = ermv;
	TS_ASSERT_EQUALS(erbv1, erlv);
	TS_ASSERT_EQUALS(erlv, erbv1);

	erbv2 = rand();

	TS_ASSERT_DIFFERS(erbv1, erbv2);
	TS_ASSERT_DIFFERS(erbv1, ermv);
	TS_ASSERT_DIFFERS(ermv, erbv1);
	TS_ASSERT_DIFFERS(erbv1, erlv);
	TS_ASSERT_DIFFERS(erlv, erbv1);

	erbv1 = ermv;
	TS_ASSERT_EQUALS(erbv1, erbv2);

	Btest3 bt3a;
	for (int idx = 0; idx < bt3a.cardinal(); ++idx)
		static_cast<value_var *>(bt3a[idx])->fromValue(rand() & 0x7);
	Btest3 bt3b(bt3a);
	TS_ASSERT_EQUALS(bt3a, bt3b);
	bt3b.setInvalid();
	TS_ASSERT_DIFFERS(bt3a, bt3b);
	bt3a.b1 = bt3b.b1;
	TS_ASSERT_EQUALS(bt3a.b1, bt3b.b1);
	bt3a.rb1 = bt3b.rb1;
	TS_ASSERT_EQUALS(bt3a.rb1, bt3b.rb1);
}

void BitVarTestSuite::testRtti(void)
{
	// the RTTI of bit_var and r_bit_var is different so
	// that bit_var siblings will continue the bit_field
	// and likewise for r_bit_var, but the two cannot mix
	bit_var<10> bv1;
	bit_var<20> bv2;
	r_bit_var<11> rbv1;
	r_bit_var<21> rbv2;
	TS_ASSERT_EQUALS(bv1.rtti(), bv2.rtti());
	TS_ASSERT_EQUALS(rbv1.rtti(), rbv2.rtti());
	TS_ASSERT_DIFFERS(bv1.rtti(), rbv1.rtti());
	TS_ASSERT_DIFFERS(bv2.rtti(), rbv2.rtti());
}

void BitVarTestSuite::testMath(void)
{
	bit_var<25> bv1;
	bit_var<20> bv2;
	bit_var<21> bv2a;
	generic_var<unsigned int> uit;
	unsigned int const1 = 50;
	unsigned int const2 = 100;
	bv1 = const1;
	bv2 = const2;
	bv2a = const2;
	uit = const1;

	TS_ASSERT(bv1 < bv2);
	TS_ASSERT(bv2 <= bv2a);
	TS_ASSERT(bv2 > bv1);
	TS_ASSERT(bv2 >= bv2a);
	TS_ASSERT(uit < bv2);
	TS_ASSERT(uit <= bv1);
	TS_ASSERT(bv2 > uit);
	TS_ASSERT(bv1 >= uit);
	TS_ASSERT(bv1 < const2);
	TS_ASSERT(bv2 <= const2);
	TS_ASSERT(bv2 > const1);
	TS_ASSERT(bv2 >= const2);

	bit_var<26> bv3;
	unsigned int const3 = 150;
	generic_var<unsigned int> uit3;
	bv3 = const3;
	uit3 = const3;

	TS_ASSERT_EQUALS(bv1 + bv2, bv3);
	TS_ASSERT_EQUALS(bv1 + const2, bv3);
	TS_ASSERT_EQUALS(const1 + bv2, bv3);
	TS_ASSERT_EQUALS(bv1 + bv2, const3);
	TS_ASSERT_EQUALS(bv1 + const2, const3);
	TS_ASSERT_EQUALS(const1 + bv2, const3);
	TS_ASSERT_EQUALS(bv1 + bv2, uit3);
	TS_ASSERT_EQUALS(bv1 + const2, uit3);
	TS_ASSERT_EQUALS(const1 + bv2, uit3);

	TS_ASSERT_EQUALS(bv2 - bv1, bv1);
	TS_ASSERT_EQUALS(bv2 - const1, bv1);
	TS_ASSERT_EQUALS(const2 - bv1, bv1);
	TS_ASSERT_EQUALS(bv2 - bv1, const1);
	TS_ASSERT_EQUALS(bv2 - const1, const1);
	TS_ASSERT_EQUALS(const2 - bv1, const1);
	TS_ASSERT_EQUALS(bv2 - bv1, uit);
	TS_ASSERT_EQUALS(bv2 - const1, uit);
	TS_ASSERT_EQUALS(const2 - bv1, uit);

	bit_var<26> bv4;
	unsigned int const4 = 5000;
	generic_var<unsigned int> uit4;
	bv4 = const4;
	uit4 = const4;

	TS_ASSERT_EQUALS(bv1 * bv2, bv4);
	TS_ASSERT_EQUALS(bv1 * const2, bv4);
	TS_ASSERT_EQUALS(const1 * bv2, bv4);
	TS_ASSERT_EQUALS(bv1 * bv2, const4);
	TS_ASSERT_EQUALS(bv1 * const2, const4);
	TS_ASSERT_EQUALS(const1 * bv2, const4);
	TS_ASSERT_EQUALS(bv1 * bv2, uit4);
	TS_ASSERT_EQUALS(bv1 * const2, uit4);
	TS_ASSERT_EQUALS(const1 * bv2, uit4);

	bit_var<26> bv5;
	unsigned int const5 = 2;
	generic_var<unsigned int> uit5;
	bv5 = const5;
	uit5 = const5;

	TS_ASSERT_EQUALS(bv2 / bv1, bv5);
	TS_ASSERT_EQUALS(bv2 / const1, bv5);
	TS_ASSERT_EQUALS(const2 / bv1, bv5);
	TS_ASSERT_EQUALS(bv2 / bv1, const5);
	TS_ASSERT_EQUALS(bv2 / const1, const5);
	TS_ASSERT_EQUALS(const2 / bv1, const5);
	TS_ASSERT_EQUALS(bv2 / bv1, uit5);
	TS_ASSERT_EQUALS(bv2 / const1, uit5);
	TS_ASSERT_EQUALS(const2 / bv1, uit5);

	bv3 = bv1;
	bv3 += bv2;
	TS_ASSERT_EQUALS(bv3, const3);
	bv3 = const1;
	bv3 += const2;
	TS_ASSERT_EQUALS(bv3, const3);
	bv3 = const2;
	bv3 += uit;
	TS_ASSERT_EQUALS(bv3, const3);

	bv3 = bv2;
	bv3 -= bv1;
	TS_ASSERT_EQUALS(bv3, const1);
	bv3 = const2;
	bv3 -= const1;
	TS_ASSERT_EQUALS(bv3, const1);
	bv3 = bv2;
	bv3 -= uit;
	TS_ASSERT_EQUALS(bv3, const1);

	bv3 = bv1;
	bv3 *= bv2;
	TS_ASSERT_EQUALS(bv3, const4);
	bv3 = const1;
	bv3 *= const2;
	TS_ASSERT_EQUALS(bv3, const4);
	bv3 = bv2;
	bv3 *= uit;
	TS_ASSERT_EQUALS(bv3, const4);

	bv3 = bv2;
	bv3 /= bv1;
	TS_ASSERT_EQUALS(bv3, const5);
	bv3 = const2;
	bv3 /= const1;
	TS_ASSERT_EQUALS(bv3, const5);
	bv3 = bv2;
	bv3 /= uit;
	TS_ASSERT_EQUALS(bv3, const5);

	// Negate has no effect
	TS_ASSERT_EQUALS(-bv1, const1);

	// Not equal
	TS_ASSERT(bv1 != bv2);

	// Next
	bit_var<10> nbvt[2];
	TS_ASSERT_EQUALS(nbvt[0].getNext(), &nbvt[1]);
}

void BitVarTestSuite::testRMath(void)
{
	r_bit_var<25> bv1;
	r_bit_var<20> bv2;
	r_bit_var<21> bv2a;
	generic_var<unsigned int> uit;
	unsigned int const1 = 50;
	unsigned int const2 = 100;
	bv1 = const1;
	bv2 = const2;
	bv2a = const2;
	uit = const1;

	TS_ASSERT(bv1 < bv2);
	TS_ASSERT(bv2 <= bv2a);
	TS_ASSERT(bv2 > bv1);
	TS_ASSERT(bv2 >= bv2a);
	TS_ASSERT(uit < bv2);
	TS_ASSERT(uit <= bv1);
	TS_ASSERT(bv2 > uit);
	TS_ASSERT(bv1 >= uit);
	TS_ASSERT(bv1 < const2);
	TS_ASSERT(bv2 <= const2);
	TS_ASSERT(bv2 > const1);
	TS_ASSERT(bv2 >= const2);

	r_bit_var<26> bv3;
	unsigned int const3 = 150;
	generic_var<unsigned int> uit3;
	bv3 = const3;
	uit3 = const3;

	TS_ASSERT_EQUALS(bv1 + bv2, bv3);
	TS_ASSERT_EQUALS(bv1 + const2, bv3);
	TS_ASSERT_EQUALS(const1 + bv2, bv3);
	TS_ASSERT_EQUALS(bv1 + bv2, const3);
	TS_ASSERT_EQUALS(bv1 + const2, const3);
	TS_ASSERT_EQUALS(const1 + bv2, const3);
	TS_ASSERT_EQUALS(bv1 + bv2, uit3);
	TS_ASSERT_EQUALS(bv1 + const2, uit3);
	TS_ASSERT_EQUALS(const1 + bv2, uit3);

	TS_ASSERT_EQUALS(bv2 - bv1, bv1);
	TS_ASSERT_EQUALS(bv2 - const1, bv1);
	TS_ASSERT_EQUALS(const2 - bv1, bv1);
	TS_ASSERT_EQUALS(bv2 - bv1, const1);
	TS_ASSERT_EQUALS(bv2 - const1, const1);
	TS_ASSERT_EQUALS(const2 - bv1, const1);
	TS_ASSERT_EQUALS(bv2 - bv1, uit);
	TS_ASSERT_EQUALS(bv2 - const1, uit);
	TS_ASSERT_EQUALS(const2 - bv1, uit);

	r_bit_var<26> bv4;
	unsigned int const4 = 5000;
	generic_var<unsigned int> uit4;
	bv4 = const4;
	uit4 = const4;

	TS_ASSERT_EQUALS(bv1 * bv2, bv4);
	TS_ASSERT_EQUALS(bv1 * const2, bv4);
	TS_ASSERT_EQUALS(const1 * bv2, bv4);
	TS_ASSERT_EQUALS(bv1 * bv2, const4);
	TS_ASSERT_EQUALS(bv1 * const2, const4);
	TS_ASSERT_EQUALS(const1 * bv2, const4);
	TS_ASSERT_EQUALS(bv1 * bv2, uit4);
	TS_ASSERT_EQUALS(bv1 * const2, uit4);
	TS_ASSERT_EQUALS(const1 * bv2, uit4);

	r_bit_var<26> bv5;
	unsigned int const5 = 2;
	generic_var<unsigned int> uit5;
	bv5 = const5;
	uit5 = const5;

	TS_ASSERT_EQUALS(bv2 / bv1, bv5);
	TS_ASSERT_EQUALS(bv2 / const1, bv5);
	TS_ASSERT_EQUALS(const2 / bv1, bv5);
	TS_ASSERT_EQUALS(bv2 / bv1, const5);
	TS_ASSERT_EQUALS(bv2 / const1, const5);
	TS_ASSERT_EQUALS(const2 / bv1, const5);
	TS_ASSERT_EQUALS(bv2 / bv1, uit5);
	TS_ASSERT_EQUALS(bv2 / const1, uit5);
	TS_ASSERT_EQUALS(const2 / bv1, uit5);

	bv3 = bv1;
	bv3 += bv2;
	TS_ASSERT_EQUALS(bv3, const3);
	bv3 = const1;
	bv3 += const2;
	TS_ASSERT_EQUALS(bv3, const3);
	bv3 = const2;
	bv3 += uit;
	TS_ASSERT_EQUALS(bv3, const3);

	bv3 = bv2;
	bv3 -= bv1;
	TS_ASSERT_EQUALS(bv3, const1);
	bv3 = const2;
	bv3 -= const1;
	TS_ASSERT_EQUALS(bv3, const1);
	bv3 = bv2;
	bv3 -= uit;
	TS_ASSERT_EQUALS(bv3, const1);

	bv3 = bv1;
	bv3 *= bv2;
	TS_ASSERT_EQUALS(bv3, const4);
	bv3 = const1;
	bv3 *= const2;
	TS_ASSERT_EQUALS(bv3, const4);
	bv3 = bv2;
	bv3 *= uit;
	TS_ASSERT_EQUALS(bv3, const4);

	bv3 = bv2;
	bv3 /= bv1;
	TS_ASSERT_EQUALS(bv3, const5);
	bv3 = const2;
	bv3 /= const1;
	TS_ASSERT_EQUALS(bv3, const5);
	bv3 = bv2;
	bv3 /= uit;
	TS_ASSERT_EQUALS(bv3, const5);

	// Negate has no effect
	TS_ASSERT_EQUALS(-bv1, const1);

	// Not equal
	TS_ASSERT(bv1 != bv2);

	// Next
	r_bit_var<10> nbvt[2];
	TS_ASSERT_EQUALS(nbvt[0].getNext(), &nbvt[1]);

	unsigned int ui = bv1;
	TS_ASSERT_EQUALS(bv1, ui);

	TS_TRACE("End");
}
/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_bit_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_BitVarTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_bit_var\test_bit_var.h"

static BitVarTestSuite suite_BitVarTestSuite;

static CxxTest::List Tests_BitVarTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_BitVarTestSuite( "test_bit_var/test_bit_var.h", 6, "BitVarTestSuite", suite_BitVarTestSuite, Tests_BitVarTestSuite );

static class TestDescription_suite_BitVarTestSuite_testExtractAndPack : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BitVarTestSuite_testExtractAndPack() : CxxTest::RealTestDescription( Tests_BitVarTestSuite, suiteDescription_BitVarTestSuite, 9, "testExtractAndPack" ) {}
 void runTest() { suite_BitVarTestSuite.testExtractAndPack(); }
} testDescription_suite_BitVarTestSuite_testExtractAndPack;

static class TestDescription_suite_BitVarTestSuite_testExtExtractAndPack : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BitVarTestSuite_testExtExtractAndPack() : CxxTest::RealTestDescription( Tests_BitVarTestSuite, suiteDescription_BitVarTestSuite, 10, "testExtExtractAndPack" ) {}
 void runTest() { suite_BitVarTestSuite.testExtExtractAndPack(); }
} testDescription_suite_BitVarTestSuite_testExtExtractAndPack;

static class TestDescription_suite_BitVarTestSuite_testStream : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BitVarTestSuite_testStream() : CxxTest::RealTestDescription( Tests_BitVarTestSuite, suiteDescription_BitVarTestSuite, 11, "testStream" ) {}
 void runTest() { suite_BitVarTestSuite.testStream(); }
} testDescription_suite_BitVarTestSuite_testStream;

static class TestDescription_suite_BitVarTestSuite_testCopying : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BitVarTestSuite_testCopying() : CxxTest::RealTestDescription( Tests_BitVarTestSuite, suiteDescription_BitVarTestSuite, 12, "testCopying" ) {}
 void runTest() { suite_BitVarTestSuite.testCopying(); }
} testDescription_suite_BitVarTestSuite_testCopying;

static class TestDescription_suite_BitVarTestSuite_testRtti : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BitVarTestSuite_testRtti() : CxxTest::RealTestDescription( Tests_BitVarTestSuite, suiteDescription_BitVarTestSuite, 13, "testRtti" ) {}
 void runTest() { suite_BitVarTestSuite.testRtti(); }
} testDescription_suite_BitVarTestSuite_testRtti;

static class TestDescription_suite_BitVarTestSuite_testMath : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BitVarTestSuite_testMath() : CxxTest::RealTestDescription( Tests_BitVarTestSuite, suiteDescription_BitVarTestSuite, 14, "testMath" ) {}
 void runTest() { suite_BitVarTestSuite.testMath(); }
} testDescription_suite_BitVarTestSuite_testMath;

static class TestDescription_suite_BitVarTestSuite_testRMath : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BitVarTestSuite_testRMath() : CxxTest::RealTestDescription( Tests_BitVarTestSuite, suiteDescription_BitVarTestSuite, 15, "testRMath" ) {}
 void runTest() { suite_BitVarTestSuite.testRMath(); }
} testDescription_suite_BitVarTestSuite_testRMath;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

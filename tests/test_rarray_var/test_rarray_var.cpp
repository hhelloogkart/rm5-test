#include "test_rarray_var.h"
#include <arry_var.h>
#include <mpt_show.h>
#include <outbuf.h>
#include <util/utilcore.h>
#include <sstream>
#include <flex_var.h>
#include <gen_var.h>

template<class C>
C rb(C in)
{
	C out = in;
	char *left = reinterpret_cast<char *>(&out);
	char *right = left + sizeof(C) - 1;
	while (left < right)
	{
		const char tmp = *left;
		*left = *right;
		*right = tmp;
		++left;
		--right;
	}
	return out;
}

class dirty_checker : public mpt_baseshow
{
public:
    dirty_checker(mpt_var *myvar);
    virtual void setDirty(mpt_var *myvar);
    virtual void notDirty(mpt_var *myvar);
    int getStateAndReset();
protected:
    int state; // 0 - not called, 1 - dirty, 2 - notdirty, 3 - error
    mpt_var *var;
};

dirty_checker::dirty_checker(mpt_var *myvar) : state(0), var(myvar)
{
    myvar->addCallback(this);
}

void dirty_checker::setDirty(mpt_var *myvar)
{
    if (myvar == var) state = 1;
    else state = 3;
}

void dirty_checker::notDirty(mpt_var *myvar)
{
    if (myvar == var) state = 2;
    else state = 3;
}

int dirty_checker::getStateAndReset()
{
    int ret = state;
    state = 0;
    return ret;
}

void ArrayVarTestSuite::testExtract(void)
{
    r_array_var<double> test;
    dirty_checker checker(&test);
    double seed;
    short count;
    char buf[5*sizeof(double)+sizeof(short)];

    TS_ASSERT(!test.isValid());

    // 1. Test basic extract from a buffer
    count = rb<short>(3);
    memcpy(buf, &count, sizeof(short));
    seed = rb(150.5);
    memcpy(buf+sizeof(short), &seed, sizeof(double));
    seed = rb(4056.9);
    memcpy(buf+sizeof(short)+sizeof(double), &seed, sizeof(double));
    seed = rb(12345.67);
    memcpy(buf+sizeof(short)+2*sizeof(double), &seed, sizeof(double));

    test.extract(3*sizeof(double)+sizeof(short), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 3*sizeof(double)+sizeof(short));
    TS_ASSERT(test.cardinal() == 3);
    TS_ASSERT(test[0] == 150.5);
    TS_ASSERT(test[1] == 4056.9);
    TS_ASSERT(test[2] == 12345.67);
    TS_ASSERT(checker.getStateAndReset() == 1);
    TS_ASSERT(test.isValid());

    // 2. Extract again - same cardinal
    seed = rb(-110.5);
    memcpy(buf+sizeof(short), &seed, sizeof(double));
    seed = rb(-8901.4);
    memcpy(buf+sizeof(short)+sizeof(double), &seed, sizeof(double));
    seed = rb(-98765.43);
    memcpy(buf+sizeof(short)+2*sizeof(double), &seed, sizeof(double));

    test.extract(3*sizeof(double)+sizeof(short), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 3*sizeof(double)+sizeof(short));
    TS_ASSERT(test.cardinal() == 3);
    TS_ASSERT(test[0] == -110.5);
    TS_ASSERT(test[1] == -8901.4);
    TS_ASSERT(test[2] == -98765.43);
    TS_ASSERT(checker.getStateAndReset() == 1);

    // 3. Extract cardinal is too large
    seed = rb(65.5);
    memcpy(buf+sizeof(short), &seed, sizeof(double));
    seed = rb(-128.4);
    memcpy(buf+sizeof(short)+sizeof(double), &seed, sizeof(double));

    test.extract(2*sizeof(double)+sizeof(short), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 2*sizeof(double)+sizeof(short));
    TS_ASSERT(test.cardinal() == 2);
    TS_ASSERT(test[0] == 65.5);
    TS_ASSERT(test[1] == -128.4);
    TS_ASSERT(checker.getStateAndReset() == 1);

    // 4. Extract nothing
    count = 0;

    test.extract(sizeof(short), reinterpret_cast<const unsigned char *>(&count));

    TS_ASSERT(test.size() == sizeof(short));
    TS_ASSERT(test.cardinal() == 0);
    TS_ASSERT(test[0] == 0.0);
    TS_ASSERT(test[500] == 0.0);
    TS_ASSERT(checker.getStateAndReset() == 1);
    TS_ASSERT(!test.isValid());

    // 5. Extract nothing because there is no data left
    count = rb<short>(15);

    test.extract(sizeof(short), reinterpret_cast<const unsigned char *>(&count));

    TS_ASSERT(test.size() == sizeof(short));
    TS_ASSERT(test.cardinal() == 0);
    TS_ASSERT(test[0] == 0.0);
    TS_ASSERT(test[500] == 0.0);
    TS_ASSERT(checker.getStateAndReset() == 2);

    // 6. Extract something else
    count = rb<short>(5);
    memcpy(buf, &count, sizeof(short));
    seed = rb(7150.5);
    memcpy(buf+sizeof(short), &seed, sizeof(double));
    seed = rb(74056.9);
    memcpy(buf+sizeof(short)+sizeof(double), &seed, sizeof(double));
    seed = rb(712345.67);
    memcpy(buf+sizeof(short)+2*sizeof(double), &seed, sizeof(double));
    seed = rb(88235.1);
    memcpy(buf+sizeof(short)+3*sizeof(double), &seed, sizeof(double));
    seed = rb(-345.67);
    memcpy(buf+sizeof(short)+4*sizeof(double), &seed, sizeof(double));

    test.extract(5*sizeof(double)+sizeof(short), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 5*sizeof(double)+sizeof(short));
    TS_ASSERT(test.cardinal() == 5);
    TS_ASSERT(test[0] == 7150.5);
    TS_ASSERT(test[1] == 74056.9);
    TS_ASSERT(test[2] == 712345.67);
    TS_ASSERT(test[3] == 88235.1);
    TS_ASSERT(test[4] == -345.67);
    TS_ASSERT(checker.getStateAndReset() == 1);

    // 7. Exactly the same data
    test.extract(5*sizeof(double)+sizeof(short), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 5*sizeof(double)+sizeof(short));
    TS_ASSERT(test.cardinal() == 5);
    TS_ASSERT(test[0] == 7150.5);
    TS_ASSERT(test[1] == 74056.9);
    TS_ASSERT(test[2] == 712345.67);
    TS_ASSERT(test[3] == 88235.1);
    TS_ASSERT(test[4] == -345.67);
    TS_ASSERT(checker.getStateAndReset() == 2);

    // 8. Exactly the same data but truncated
    test.extract(2*sizeof(double)+sizeof(short), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 2*sizeof(double)+sizeof(short));
    TS_ASSERT(test.cardinal() == 2);
    TS_ASSERT(test[0] == 7150.5);
    TS_ASSERT(test[1] == 74056.9);
    TS_ASSERT(test[2] == 0.0);
    TS_ASSERT(test[3] == 0.0);
    TS_ASSERT(test[4] == 0.0);
    TS_ASSERT(checker.getStateAndReset() == 1);

    // 9. Same data with correct cardinal this time
    count = rb<short>(2);
    memcpy(buf, &count, sizeof(short));
    test.extract(2*sizeof(double)+sizeof(short), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 2*sizeof(double)+sizeof(short));
    TS_ASSERT(test.cardinal() == 2);
    TS_ASSERT(test[0] == 7150.5);
    TS_ASSERT(test[1] == 74056.9);
    TS_ASSERT(checker.getStateAndReset() == 2);

    test.setInvalid();
    TS_ASSERT(test.size() == 2);
    TS_ASSERT(test.cardinal() == 0);
    TS_ASSERT(checker.getStateAndReset() == 1);

    test.setInvalid();
    TS_ASSERT(checker.getStateAndReset() == 2);
}
void ArrayVarTestSuite::testOutput(void)
{
    r_array_var<int> test;
    r_array_var<int> test2;
    dirty_checker checker(&test);
    char buf[4 * sizeof(int) + sizeof(short)];

    test.resize(4);
    test.fromValue(0, 60);
    test.fromValue(1, 70);
    test.fromValue(2, 80);
    test.fromValue(3, 90);
    test.fromValue(4, 100); // nothing happens

    // check assignment
    TS_ASSERT(test.size() == 18);
    TS_ASSERT(test.cardinal() == 4);
    TS_ASSERT(test[0] == 60);
    TS_ASSERT(test[1] == 70);
    TS_ASSERT(test[2] == 80);
    TS_ASSERT(test[3] == 90);
    TS_ASSERT(test[4] == 0);
    TS_ASSERT(checker.getStateAndReset() == 1);

    // check output
    short counter = rb<short>(4);
    int data[4] = { rb(60), rb(70), rb(80), rb(90) };
    outbuf ob;
    ob.set(reinterpret_cast<unsigned char *>(buf), 18);
    test.output(ob);

    TS_ASSERT(memcmp(buf, &counter, sizeof(short)) == 0);
    TS_ASSERT(memcmp(buf+sizeof(short), data, 4*sizeof(int)) == 0);

    // check assignment of entire array
    TS_ASSERT(test != test2);
    test2 = test;
    ob.set(reinterpret_cast<unsigned char *>(buf), 18);
    test2.output(ob);
    TS_ASSERT(memcmp(buf, &counter, sizeof(short)) == 0);
    TS_ASSERT(memcmp(buf+sizeof(short), data, 4*sizeof(int)) == 0);
    TS_ASSERT(test == test2);
}
void ArrayVarTestSuite::testStream(void)
{
    r_array_var<unsigned long> test;

    test.resize(4);
    test.from_unsigned_long(0, 0x11);
    test.from_unsigned_long(1, 0x22);
    test.from_unsigned_long(2, 0x33);
    test.from_unsigned_long(3, 0x44);

    std::ostringstream oss;
    oss << test;
    if (BE(0x11223344) == 0x11223344) {
        TS_ASSERT(oss.str() == "16 00 00 00 11 00 00 00 22 00 00 00 33 00 00 00 44");
    } else {
        TS_ASSERT(oss.str() == "16 11 00 00 00 22 00 00 00 33 00 00 00 44 00 00 00");
    }

    oss.rdbuf()->str(std::string ());
    test.resize(6);
    test.from_unsigned_long(0, 0x0F0E);
    test.from_unsigned_long(1, 0x0D0C);
    test.from_unsigned_long(2, 0x0B0A);
    test.from_unsigned_long(3, 0x0908);
    test.from_unsigned_long(4, 0x0706);
    test.from_unsigned_long(5, 0x0504);
    oss << test;
    if (BE(0x11223344) == 0x11223344) {
        TS_ASSERT(oss.str() == "24 00 00 0f 0e 00 00 0d 0c 00 00 0b 0a 00 00 09 08 00 00 07 06 00 00 05 04");
    } else {
        TS_ASSERT(oss.str() == "24 0e 0f 00 00 0c 0d 00 00 0a 0b 00 00 08 09 00 00 06 07 00 00 04 05 00 00");
    }

    test.setInvalid();
    TS_ASSERT(test.cardinal() == 0);
    TS_ASSERT(test.size() == 2);

    std::string sbuf;
    if (BE(0x11223344) == 0x11223344) {
        sbuf = "8 00 00 11 33 00 00 22 44 ";
    } else {
        sbuf = "8 33 11 00 00 44 22 00 00 ";
    }
    std::istringstream iss(sbuf);
    iss >> test;

    TS_ASSERT(test.cardinal() == 2);
    TS_ASSERT(test.to_long(0) == 0x1133);
    TS_ASSERT(test.to_long(1) == 0x2244);

    if (BE(0x11223344) == 0x11223344) {
        sbuf = "20 00 aa 00 bb 00 cc 00 dd 00 ee 00 ff 00 12 00 34 00 56 00 78 ";
    } else {
        sbuf = "20 bb 00 aa 00 dd 00 cc 00 ff 00 ee 00 34 00 12 00 78 00 56 00 ";
    }
    iss.str(sbuf);
    iss.clear();
    iss >> test;

    TS_ASSERT(test.cardinal() == 5);
    TS_ASSERT(test.to_long(0) == 0xaa00bb);
    TS_ASSERT(test.to_long(1) == 0xcc00dd);
    TS_ASSERT(test.to_long(2) == 0xee00ff);
    TS_ASSERT(test.to_long(3) == 0x120034);
    TS_ASSERT(test.to_long(4) == 0x560078);

    // incorrect cardinal situation
    if (BE(0x11223344) == 0x11223344) {
        sbuf = "32 00 aa 11 bb 00 cc 11 dd 00 ee 11 ff 00 12 11 34 00 56 11 78 ";
    } else {
        sbuf = "32 bb 11 aa 00 dd 11 cc 00 ff 11 ee 00 34 11 12 00 78 11 56 00 ";
    }
    iss.str(sbuf);
    iss.clear();
    iss >> test;

    TS_ASSERT(test.cardinal() == 5);
    TS_ASSERT(test.to_long(0) == 0xaa11bb);
    TS_ASSERT(test.to_long(1) == 0xcc11dd);
    TS_ASSERT(test.to_long(2) == 0xee11ff);
    TS_ASSERT(test.to_long(3) == 0x121134);
    TS_ASSERT(test.to_long(4) == 0x561178);

    // incorrect cardinal zero situation
    sbuf = "40 ";
    iss.str(sbuf);
    iss.clear();
    iss >> test;

    TS_ASSERT(test.cardinal() == 0);
    TS_ASSERT(!test.isValid());
}
void ArrayVarTestSuite::testToValue(void)
{
    // arrays
    r_array_var<unsigned char> a1;
    r_array_var<char> a2;
    r_array_var<unsigned short> a3;
    r_array_var<short> a4;
    r_array_var<unsigned long> a5;
    r_array_var<long> a6;
    r_array_var<float> a7;
    r_array_var<double> a8;
    r_array_var<unsigned long long> a9;
    r_array_var<long long> a10;

    unsigned char buf[256];
    short count = rb<short>(2);
    memcpy(buf, &count, sizeof(short));
    char carray[] = {60, 53};
    memcpy(buf+sizeof(short), carray, 2*sizeof(char));
    a1.extract(4, buf);

    TS_ASSERT(a1.to_char(1) == '\65');
    TS_ASSERT(a1.to_unsigned_char(1) == '\65');
    TS_ASSERT(a1.to_short(1) == 53);
    TS_ASSERT(a1.to_unsigned_short(1) == 53U);
    TS_ASSERT(a1.to_int(1) == 53);
    TS_ASSERT(a1.to_unsigned_int(1) == 53U);
    TS_ASSERT(a1.to_long(1) == 53);
    TS_ASSERT(a1.to_unsigned_long(1) == 53U);
    TS_ASSERT(a1.to_long_long(1) == 53LL);
    TS_ASSERT(a1.to_unsigned_long_long(1) == 53ULL);
    TS_ASSERT(a1.to_float(1) == 53.f);
    TS_ASSERT(a1.to_double(1) == 53.);
    TS_ASSERT(strcmp(a1.to_const_char_ptr(1), "53") == 0);
    TS_ASSERT(strcmp(a1.to_char_ptr(1), "53") == 0);

    a2.extract(4, buf);

    TS_ASSERT(a2.to_char(1) == '\65');
    TS_ASSERT(a2.to_unsigned_char(1) == '\65');
    TS_ASSERT(a2.to_short(1) == 53);
    TS_ASSERT(a2.to_unsigned_short(1) == 53U);
    TS_ASSERT(a2.to_int(1) == 53);
    TS_ASSERT(a2.to_unsigned_int(1) == 53U);
    TS_ASSERT(a2.to_long(1) == 53);
    TS_ASSERT(a2.to_unsigned_long(1) == 53U);
    TS_ASSERT(a2.to_long_long(1) == 53LL);
    TS_ASSERT(a2.to_unsigned_long_long(1) == 53ULL);
    TS_ASSERT(a2.to_float(1) == 53.f);
    TS_ASSERT(a2.to_double(1) == 53.);
    TS_ASSERT(strcmp(a2.to_const_char_ptr(1), "53") == 0);
    TS_ASSERT(strcmp(a2.to_char_ptr(1), "53") == 0);

    short sarray[] = {rb<short>(60), rb<short>(53)};
    memcpy(buf+sizeof(short), sarray, 2*sizeof(short));
    a3.extract(sizeof(short)+2*sizeof(short), buf);

    TS_ASSERT(a3.to_char(1) == '\65');
    TS_ASSERT(a3.to_unsigned_char(1) == '\65');
    TS_ASSERT(a3.to_short(1) == 53);
    TS_ASSERT(a3.to_unsigned_short(1) == 53U);
    TS_ASSERT(a3.to_int(1) == 53);
    TS_ASSERT(a3.to_unsigned_int(1) == 53U);
    TS_ASSERT(a3.to_long(1) == 53);
    TS_ASSERT(a3.to_unsigned_long(1) == 53U);
    TS_ASSERT(a3.to_long_long(1) == 53LL);
    TS_ASSERT(a3.to_unsigned_long_long(1) == 53ULL);
    TS_ASSERT(a3.to_float(1) == 53.f);
    TS_ASSERT(a3.to_double(1) == 53.);
    TS_ASSERT(strcmp(a3.to_const_char_ptr(1), "53") == 0);
    TS_ASSERT(strcmp(a3.to_char_ptr(1), "53") == 0);

    a4.extract(sizeof(short)+2*sizeof(short), buf);

    TS_ASSERT(a4.to_char(1) == '\65');
    TS_ASSERT(a4.to_unsigned_char(1) == '\65');
    TS_ASSERT(a4.to_short(1) == 53);
    TS_ASSERT(a4.to_unsigned_short(1) == 53U);
    TS_ASSERT(a4.to_int(1) == 53);
    TS_ASSERT(a4.to_unsigned_int(1) == 53U);
    TS_ASSERT(a4.to_long(1) == 53);
    TS_ASSERT(a4.to_long_long(1) == 53LL);
    TS_ASSERT(a4.to_unsigned_long_long(1) == 53ULL);
    TS_ASSERT(a4.to_unsigned_long(1) == 53U);
    TS_ASSERT(a4.to_float(1) == 53.f);
    TS_ASSERT(a4.to_double(1) == 53.);
    TS_ASSERT(strcmp(a4.to_const_char_ptr(1), "53") == 0);
    TS_ASSERT(strcmp(a4.to_char_ptr(1), "53") == 0);

    sarray[1] = rb<short>(-65);
    memcpy(buf+sizeof(short), sarray, 2*sizeof(short));
    a4.extract(sizeof(short)+2*sizeof(short), buf);

    TS_ASSERT(a4.to_char(1) == -65);
    TS_ASSERT(a4.to_short(1) == -65);
    TS_ASSERT(a4.to_int(1) == -65);
    TS_ASSERT(a4.to_long(1) == -65);
    TS_ASSERT(a4.to_float(1) == -65.f);
    TS_ASSERT(a4.to_double(1) == -65.);
    TS_ASSERT(a4.to_long_long(1) == -65LL);
    TS_ASSERT(strcmp(a4.to_const_char_ptr(1), "-65") == 0);
    TS_ASSERT(strcmp(a4.to_char_ptr(1), "-65") == 0);

    long larray[] = {rb(60L), rb(53L)};
    memcpy(buf+sizeof(short), larray, 2*sizeof(long));
    a5.extract(sizeof(short)+2*sizeof(long), buf);

    TS_ASSERT(a5.to_char(1) == '\65');
    TS_ASSERT(a5.to_unsigned_char(1) == '\65');
    TS_ASSERT(a5.to_short(1) == 53);
    TS_ASSERT(a5.to_unsigned_short(1) == 53U);
    TS_ASSERT(a5.to_int(1) == 53);
    TS_ASSERT(a5.to_unsigned_int(1) == 53U);
    TS_ASSERT(a5.to_long(1) == 53);
    TS_ASSERT(a5.to_unsigned_long(1) == 53U);
    TS_ASSERT(a5.to_long_long(1) == 53LL);
    TS_ASSERT(a5.to_unsigned_long_long(1) == 53ULL);
    TS_ASSERT(a5.to_float(1) == 53.f);
    TS_ASSERT(a5.to_double(1) == 53.);
    TS_ASSERT(strcmp(a5.to_const_char_ptr(1), "53") == 0);
    TS_ASSERT(strcmp(a5.to_char_ptr(1), "53") == 0);

    a6.extract(sizeof(short)+2*sizeof(long), buf);

    TS_ASSERT(a6.to_char(1) == '\65');
    TS_ASSERT(a6.to_unsigned_char(1) == '\65');
    TS_ASSERT(a6.to_short(1) == 53);
    TS_ASSERT(a6.to_unsigned_short(1) == 53U);
    TS_ASSERT(a6.to_int(1) == 53);
    TS_ASSERT(a6.to_unsigned_int(1) == 53U);
    TS_ASSERT(a6.to_long(1) == 53);
    TS_ASSERT(a6.to_unsigned_long(1) == 53U);
    TS_ASSERT(a6.to_long_long(1) == 53LL);
    TS_ASSERT(a6.to_unsigned_long_long(1) == 53ULL);
    TS_ASSERT(a6.to_float(1) == 53.f);
    TS_ASSERT(a6.to_double(1) == 53.);
    TS_ASSERT(strcmp(a6.to_const_char_ptr(1), "53") == 0);
    TS_ASSERT(strcmp(a6.to_char_ptr(1), "53") == 0);

    larray[1] = rb(-65L);
    memcpy(buf+sizeof(short), larray, 2*sizeof(long));
    a6.extract(sizeof(short)+2*sizeof(long), buf);

    TS_ASSERT(a6.to_char(1) == -65);
    TS_ASSERT(a6.to_short(1) == -65);
    TS_ASSERT(a6.to_int(1) == -65);
    TS_ASSERT(a6.to_long(1) == -65);
    TS_ASSERT(a6.to_float(1) == -65.f);
    TS_ASSERT(a6.to_double(1) == -65.);
    TS_ASSERT(a6.to_long_long(1) == -65LL);
    TS_ASSERT(strcmp(a6.to_const_char_ptr(1), "-65") == 0);
    TS_ASSERT(strcmp(a6.to_char_ptr(1), "-65") == 0);

    float farray[] = {rb(60.f), rb(53.f)};
    memcpy(buf+sizeof(short), farray, 2*sizeof(float));
    a7.extract(sizeof(short)+2*sizeof(float), buf);

    TS_ASSERT(a7.to_char(1) == '\65');
    TS_ASSERT(a7.to_unsigned_char(1) == '\65');
    TS_ASSERT(a7.to_short(1) == 53);
    TS_ASSERT(a7.to_unsigned_short(1) == 53U);
    TS_ASSERT(a7.to_int(1) == 53);
    TS_ASSERT(a7.to_unsigned_int(1) == 53U);
    TS_ASSERT(a7.to_long(1) == 53);
    TS_ASSERT(a7.to_unsigned_long(1) == 53U);
    TS_ASSERT(a7.to_long_long(1) == 53LL);
    TS_ASSERT(a7.to_unsigned_long_long(1) == 53ULL);
    TS_ASSERT(a7.to_float(1) == 53.f);
    TS_ASSERT(a7.to_double(1) == 53.);
    TS_ASSERT(strcmp(a7.to_const_char_ptr(1), "53") == 0);
    TS_ASSERT(strcmp(a7.to_char_ptr(1), "53") == 0);

    farray[1] = rb(-65.f);
    memcpy(buf+sizeof(short), farray, 2*sizeof(float));
    a7.extract(sizeof(short)+2*sizeof(float), buf);

    TS_ASSERT(a7.to_char(1) == -65);
    TS_ASSERT(a7.to_short(1) == -65);
    TS_ASSERT(a7.to_int(1) == -65);
    TS_ASSERT(a7.to_long(1) == -65);
    TS_ASSERT(a7.to_long_long(1) == -65LL);
    TS_ASSERT(a7.to_float(1) == -65.f);
    TS_ASSERT(a7.to_double(1) == -65.);
    TS_ASSERT(strcmp(a7.to_const_char_ptr(1), "-65") == 0);
    TS_ASSERT(strcmp(a7.to_char_ptr(1), "-65") == 0);

    double darray[] = {rb(60.), rb(53.)};
    memcpy(buf+sizeof(short), darray, 2*sizeof(double));
    a8.extract(sizeof(short)+2*sizeof(double), buf);

    TS_ASSERT(a8.to_char(1) == '\65');
    TS_ASSERT(a8.to_unsigned_char(1) == '\65');
    TS_ASSERT(a8.to_short(1) == 53);
    TS_ASSERT(a8.to_unsigned_short(1) == 53U);
    TS_ASSERT(a8.to_int(1) == 53);
    TS_ASSERT(a8.to_unsigned_int(1) == 53U);
    TS_ASSERT(a8.to_long(1) == 53);
    TS_ASSERT(a8.to_unsigned_long(1) == 53U);
    TS_ASSERT(a8.to_long_long(1) == 53LL);
    TS_ASSERT(a8.to_unsigned_long_long(1) == 53ULL);
    TS_ASSERT(a8.to_float(1) == 53.f);
    TS_ASSERT(a8.to_double(1) == 53.);
    TS_ASSERT(strcmp(a8.to_const_char_ptr(1), "53") == 0);
    TS_ASSERT(strcmp(a8.to_char_ptr(1), "53") == 0);

    darray[1] = rb(-65.);
    memcpy(buf+sizeof(short), darray, 2*sizeof(double));
    a8.extract(sizeof(short)+2*sizeof(double), buf);

    TS_ASSERT(a8.to_char(1) == -65);
    TS_ASSERT(a8.to_short(1) == -65);
    TS_ASSERT(a8.to_int(1) == -65);
    TS_ASSERT(a8.to_long(1) == -65);
    TS_ASSERT(a8.to_long_long(1) == -65LL);
    TS_ASSERT(a8.to_float(1) == -65.f);
    TS_ASSERT(a8.to_double(1) == -65.);
    TS_ASSERT(strcmp(a8.to_const_char_ptr(1), "-65") == 0);
    TS_ASSERT(strcmp(a8.to_char_ptr(1), "-65") == 0);

    long long llarray[] = { rb(60LL), rb(53LL) };
    memcpy(buf + sizeof(short), llarray, 2 * sizeof(long long));
    a9.extract(sizeof(short) + 2 * sizeof(long long), buf);

    TS_ASSERT(a9.to_char(1) == '\65');
    TS_ASSERT(a9.to_unsigned_char(1) == '\65');
    TS_ASSERT(a9.to_short(1) == 53);
    TS_ASSERT(a9.to_unsigned_short(1) == 53U);
    TS_ASSERT(a9.to_int(1) == 53);
    TS_ASSERT(a9.to_unsigned_int(1) == 53U);
    TS_ASSERT(a9.to_long(1) == 53);
    TS_ASSERT(a9.to_unsigned_long(1) == 53U);
    TS_ASSERT(a9.to_long_long(1) == 53LL);
    TS_ASSERT(a9.to_unsigned_long_long(1) == 53ULL);
    TS_ASSERT(a9.to_float(1) == 53.f);
    TS_ASSERT(a9.to_double(1) == 53.);
    TS_ASSERT(strcmp(a9.to_const_char_ptr(1), "53") == 0);
    TS_ASSERT(strcmp(a9.to_char_ptr(1), "53") == 0);

    a10.extract(sizeof(short) + 2 * sizeof(long long), buf);

    TS_ASSERT(a10.to_char(1) == '\65');
    TS_ASSERT(a10.to_unsigned_char(1) == '\65');
    TS_ASSERT(a10.to_short(1) == 53);
    TS_ASSERT(a10.to_unsigned_short(1) == 53U);
    TS_ASSERT(a10.to_int(1) == 53);
    TS_ASSERT(a10.to_unsigned_int(1) == 53U);
    TS_ASSERT(a10.to_long(1) == 53);
    TS_ASSERT(a10.to_unsigned_long(1) == 53U);
    TS_ASSERT(a10.to_long_long(1) == 53LL);
    TS_ASSERT(a10.to_unsigned_long_long(1) == 53ULL);
    TS_ASSERT(a10.to_float(1) == 53.f);
    TS_ASSERT(a10.to_double(1) == 53.);
    TS_ASSERT(strcmp(a10.to_const_char_ptr(1), "53") == 0);
    TS_ASSERT(strcmp(a10.to_char_ptr(1), "53") == 0);

    llarray[1] = rb(-65LL);
    memcpy(buf + sizeof(short), llarray, 2 * sizeof(long long));
    a10.extract(sizeof(short) + 2 * sizeof(long long), buf);

    TS_ASSERT(a10.to_char(1) == -65);
    TS_ASSERT(a10.to_short(1) == -65);
    TS_ASSERT(a10.to_int(1) == -65);
    TS_ASSERT(a10.to_long(1) == -65);
    TS_ASSERT(a10.to_float(1) == -65.f);
    TS_ASSERT(a10.to_double(1) == -65.);
    TS_ASSERT(a10.to_long_long(1) == -65LL);
    TS_ASSERT(strcmp(a10.to_const_char_ptr(1), "-65") == 0);
    TS_ASSERT(strcmp(a10.to_char_ptr(1), "-65") == 0);

    // Test to value with invalid index returns 0 and doesn't crash
    TS_ASSERT_EQUALS(a1.to_char(10), 0);
    TS_ASSERT_EQUALS(a1.to_unsigned_char(10), 0);
    TS_ASSERT_EQUALS(a1.to_short(10), 0);
    TS_ASSERT_EQUALS(a1.to_unsigned_short(10), 0);
    TS_ASSERT_EQUALS(a1.to_int(10), 0);
    TS_ASSERT_EQUALS(a1.to_unsigned_int(10), 0);
    TS_ASSERT_EQUALS(a1.to_long(10), 0);
    TS_ASSERT_EQUALS(a1.to_unsigned_long(10), 0);
    TS_ASSERT_EQUALS(a1.to_long_long(10), 0);
    TS_ASSERT_EQUALS(a1.to_unsigned_long_long(10), 0);
    TS_ASSERT_EQUALS(a1.to_float(10), 0);
    TS_ASSERT_EQUALS(a1.to_double(10), 0);
    TS_ASSERT(a1.to_const_char_ptr(10) == 0);
    TS_ASSERT(a1.to_char_ptr(10) == 0);

    TS_ASSERT_EQUALS(a2.to_char(10), 0);
    TS_ASSERT_EQUALS(a2.to_unsigned_char(10), 0);
    TS_ASSERT_EQUALS(a2.to_short(10), 0);
    TS_ASSERT_EQUALS(a2.to_unsigned_short(10), 0);
    TS_ASSERT_EQUALS(a2.to_int(10), 0);
    TS_ASSERT_EQUALS(a2.to_unsigned_int(10), 0);
    TS_ASSERT_EQUALS(a2.to_long(10), 0);
    TS_ASSERT_EQUALS(a2.to_unsigned_long(10), 0);
    TS_ASSERT_EQUALS(a2.to_long_long(10), 0);
    TS_ASSERT_EQUALS(a2.to_unsigned_long_long(10), 0);
    TS_ASSERT_EQUALS(a2.to_float(10), 0);
    TS_ASSERT_EQUALS(a2.to_double(10), 0);
    TS_ASSERT(a2.to_const_char_ptr(10) == 0);
    TS_ASSERT(a2.to_char_ptr(10) == 0);

    TS_ASSERT_EQUALS(a3.to_char(10), 0);
    TS_ASSERT_EQUALS(a3.to_unsigned_char(10), 0);
    TS_ASSERT_EQUALS(a3.to_short(10), 0);
    TS_ASSERT_EQUALS(a3.to_unsigned_short(10), 0);
    TS_ASSERT_EQUALS(a3.to_int(10), 0);
    TS_ASSERT_EQUALS(a3.to_unsigned_int(10), 0);
    TS_ASSERT_EQUALS(a3.to_long(10), 0);
    TS_ASSERT_EQUALS(a3.to_unsigned_long(10), 0);
    TS_ASSERT_EQUALS(a3.to_long_long(10), 0);
    TS_ASSERT_EQUALS(a3.to_unsigned_long_long(10), 0);
    TS_ASSERT_EQUALS(a3.to_float(10), 0);
    TS_ASSERT_EQUALS(a3.to_double(10), 0);
    TS_ASSERT(a3.to_const_char_ptr(10) == 0);
    TS_ASSERT(a3.to_char_ptr(10) == 0);

    TS_ASSERT_EQUALS(a4.to_char(10), 0);
    TS_ASSERT_EQUALS(a4.to_unsigned_char(10), 0);
    TS_ASSERT_EQUALS(a4.to_short(10), 0);
    TS_ASSERT_EQUALS(a4.to_unsigned_short(10), 0);
    TS_ASSERT_EQUALS(a4.to_int(10), 0);
    TS_ASSERT_EQUALS(a4.to_unsigned_int(10), 0);
    TS_ASSERT_EQUALS(a4.to_long(10), 0);
    TS_ASSERT_EQUALS(a4.to_unsigned_long(10), 0);
    TS_ASSERT_EQUALS(a4.to_long_long(10), 0);
    TS_ASSERT_EQUALS(a4.to_unsigned_long_long(10), 0);
    TS_ASSERT_EQUALS(a4.to_float(10), 0);
    TS_ASSERT_EQUALS(a4.to_double(10), 0);
    TS_ASSERT(a4.to_const_char_ptr(10) == 0);
    TS_ASSERT(a4.to_char_ptr(10) == 0);

    TS_ASSERT_EQUALS(a5.to_char(10), 0);
    TS_ASSERT_EQUALS(a5.to_unsigned_char(10), 0);
    TS_ASSERT_EQUALS(a5.to_short(10), 0);
    TS_ASSERT_EQUALS(a5.to_unsigned_short(10), 0);
    TS_ASSERT_EQUALS(a5.to_int(10), 0);
    TS_ASSERT_EQUALS(a5.to_unsigned_int(10), 0);
    TS_ASSERT_EQUALS(a5.to_long(10), 0);
    TS_ASSERT_EQUALS(a5.to_unsigned_long(10), 0);
    TS_ASSERT_EQUALS(a5.to_long_long(10), 0);
    TS_ASSERT_EQUALS(a5.to_unsigned_long_long(10), 0);
    TS_ASSERT_EQUALS(a5.to_float(10), 0);
    TS_ASSERT_EQUALS(a5.to_double(10), 0);
    TS_ASSERT(a5.to_const_char_ptr(10) == 0);
    TS_ASSERT(a5.to_char_ptr(10) == 0);

    TS_ASSERT_EQUALS(a6.to_char(10), 0);
    TS_ASSERT_EQUALS(a6.to_unsigned_char(10), 0);
    TS_ASSERT_EQUALS(a6.to_short(10), 0);
    TS_ASSERT_EQUALS(a6.to_unsigned_short(10), 0);
    TS_ASSERT_EQUALS(a6.to_int(10), 0);
    TS_ASSERT_EQUALS(a6.to_unsigned_int(10), 0);
    TS_ASSERT_EQUALS(a6.to_long(10), 0);
    TS_ASSERT_EQUALS(a6.to_unsigned_long(10), 0);
    TS_ASSERT_EQUALS(a6.to_long_long(10), 0);
    TS_ASSERT_EQUALS(a6.to_unsigned_long_long(10), 0);
    TS_ASSERT_EQUALS(a6.to_float(10), 0);
    TS_ASSERT_EQUALS(a6.to_double(10), 0);
    TS_ASSERT(a6.to_const_char_ptr(10) == 0);
    TS_ASSERT(a6.to_char_ptr(10) == 0);

    TS_ASSERT_EQUALS(a7.to_char(10), 0);
    TS_ASSERT_EQUALS(a7.to_unsigned_char(10), 0);
    TS_ASSERT_EQUALS(a7.to_short(10), 0);
    TS_ASSERT_EQUALS(a7.to_unsigned_short(10), 0);
    TS_ASSERT_EQUALS(a7.to_int(10), 0);
    TS_ASSERT_EQUALS(a7.to_unsigned_int(10), 0);
    TS_ASSERT_EQUALS(a7.to_long(10), 0);
    TS_ASSERT_EQUALS(a7.to_unsigned_long(10), 0);
    TS_ASSERT_EQUALS(a7.to_long_long(10), 0);
    TS_ASSERT_EQUALS(a7.to_unsigned_long_long(10), 0);
    TS_ASSERT_EQUALS(a7.to_float(10), 0);
    TS_ASSERT_EQUALS(a7.to_double(10), 0);
    TS_ASSERT(a7.to_const_char_ptr(10) == 0);
    TS_ASSERT(a7.to_char_ptr(10) == 0);

    TS_ASSERT_EQUALS(a8.to_char(10), 0);
    TS_ASSERT_EQUALS(a8.to_unsigned_char(10), 0);
    TS_ASSERT_EQUALS(a8.to_short(10), 0);
    TS_ASSERT_EQUALS(a8.to_unsigned_short(10), 0);
    TS_ASSERT_EQUALS(a8.to_int(10), 0);
    TS_ASSERT_EQUALS(a8.to_unsigned_int(10), 0);
    TS_ASSERT_EQUALS(a8.to_long(10), 0);
    TS_ASSERT_EQUALS(a8.to_unsigned_long(10), 0);
    TS_ASSERT_EQUALS(a8.to_long_long(10), 0);
    TS_ASSERT_EQUALS(a8.to_unsigned_long_long(10), 0);
    TS_ASSERT_EQUALS(a8.to_float(10), 0);
    TS_ASSERT_EQUALS(a8.to_double(10), 0);
    TS_ASSERT(a8.to_const_char_ptr(10) == 0);
    TS_ASSERT(a8.to_char_ptr(10) == 0);

    TS_ASSERT_EQUALS(a9.to_char(10), 0);
    TS_ASSERT_EQUALS(a9.to_unsigned_char(10), 0);
    TS_ASSERT_EQUALS(a9.to_short(10), 0);
    TS_ASSERT_EQUALS(a9.to_unsigned_short(10), 0);
    TS_ASSERT_EQUALS(a9.to_int(10), 0);
    TS_ASSERT_EQUALS(a9.to_unsigned_int(10), 0);
    TS_ASSERT_EQUALS(a9.to_long(10), 0);
    TS_ASSERT_EQUALS(a9.to_unsigned_long(10), 0);
    TS_ASSERT_EQUALS(a9.to_long_long(10), 0);
    TS_ASSERT_EQUALS(a9.to_unsigned_long_long(10), 0);
    TS_ASSERT_EQUALS(a9.to_float(10), 0);
    TS_ASSERT_EQUALS(a9.to_double(10), 0);
    TS_ASSERT(a9.to_const_char_ptr(10) == 0);
    TS_ASSERT(a9.to_char_ptr(10) == 0);

    TS_ASSERT_EQUALS(a10.to_char(10), 0);
    TS_ASSERT_EQUALS(a10.to_unsigned_char(10), 0);
    TS_ASSERT_EQUALS(a10.to_short(10), 0);
    TS_ASSERT_EQUALS(a10.to_unsigned_short(10), 0);
    TS_ASSERT_EQUALS(a10.to_int(10), 0);
    TS_ASSERT_EQUALS(a10.to_unsigned_int(10), 0);
    TS_ASSERT_EQUALS(a10.to_long(10), 0);
    TS_ASSERT_EQUALS(a10.to_unsigned_long(10), 0);
    TS_ASSERT_EQUALS(a10.to_long_long(10), 0);
    TS_ASSERT_EQUALS(a10.to_unsigned_long_long(10), 0);
    TS_ASSERT_EQUALS(a10.to_float(10), 0);
    TS_ASSERT_EQUALS(a10.to_double(10), 0);
    TS_ASSERT(a10.to_const_char_ptr(10) == 0);
    TS_ASSERT(a10.to_char_ptr(10) == 0);

    carray[0] = 33;
    carray[1] = 44;
    sarray[0] = 33;
    sarray[1] = 44;
    larray[0] = 33;
    larray[1] = 44;
    llarray[0] = 33;
    llarray[1] = 44;
    farray[0] = 33.f;
    farray[1] = 44.f;
    darray[0] = 33.;
    darray[1] = 44.;
    a1.from_char(0, carray[0]);
    a2.from_char(0, carray[0]);
    a3.from_char(0, carray[0]);
    a4.from_char(0, carray[0]);
    a5.from_char(0, carray[0]);
    a6.from_char(0, carray[0]);
    a7.from_char(0, carray[0]);
    a8.from_char(0, carray[0]);
    a9.from_char(0, carray[0]);
    a10.from_char(0, carray[0]);
    a1.from_char(1, carray[1]);
    a2.from_char(1, carray[1]);
    a3.from_char(1, carray[1]);
    a4.from_char(1, carray[1]);
    a5.from_char(1, carray[1]);
    a6.from_char(1, carray[1]);
    a7.from_char(1, carray[1]);
    a8.from_char(1, carray[1]);
    a9.from_char(1, carray[1]);
    a10.from_char(1, carray[1]);
    TS_ASSERT(memcmp(a1, carray, 2) == 0);
    TS_ASSERT(memcmp(a2, carray, 2) == 0);
    TS_ASSERT(memcmp(a3, sarray, 4) == 0);
    TS_ASSERT(memcmp(a4, sarray, 4) == 0);
    TS_ASSERT(memcmp(a5, larray, 8) == 0);
    TS_ASSERT(memcmp(a6, larray, 8) == 0);
    TS_ASSERT(memcmp(a7, farray, 8) == 0);
    TS_ASSERT(memcmp(a8, darray, 16) == 0);
    TS_ASSERT(memcmp(a9, llarray, 16) == 0);
    TS_ASSERT(memcmp(a10, llarray, 16) == 0);

    carray[0] = 55;
    carray[1] = 66;
    sarray[0] = 55;
    sarray[1] = 66;
    larray[0] = 55;
    larray[1] = 66;
    llarray[0] = 55;
    llarray[1] = 66;
    farray[0] = 55.f;
    farray[1] = 66.f;
    darray[0] = 55.;
    darray[1] = 66.;
    a1.from_short(0, sarray[0]);
    a2.from_short(0, sarray[0]);
    a3.from_short(0, sarray[0]);
    a4.from_short(0, sarray[0]);
    a5.from_short(0, sarray[0]);
    a6.from_short(0, sarray[0]);
    a7.from_short(0, sarray[0]);
    a8.from_short(0, sarray[0]);
    a9.from_short(0, sarray[0]);
    a10.from_short(0, sarray[0]);
    a1.from_short(1, sarray[1]);
    a2.from_short(1, sarray[1]);
    a3.from_short(1, sarray[1]);
    a4.from_short(1, sarray[1]);
    a5.from_short(1, sarray[1]);
    a6.from_short(1, sarray[1]);
    a7.from_short(1, sarray[1]);
    a8.from_short(1, sarray[1]);
    a9.from_short(1, sarray[1]);
    a10.from_short(1, sarray[1]);
    TS_ASSERT(memcmp(a1, carray, 2) == 0);
    TS_ASSERT(memcmp(a2, carray, 2) == 0);
    TS_ASSERT(memcmp(a3, sarray, 4) == 0);
    TS_ASSERT(memcmp(a4, sarray, 4) == 0);
    TS_ASSERT(memcmp(a5, larray, 8) == 0);
    TS_ASSERT(memcmp(a6, larray, 8) == 0);
    TS_ASSERT(memcmp(a7, farray, 8) == 0);
    TS_ASSERT(memcmp(a8, darray, 16) == 0);
    TS_ASSERT(memcmp(a9, llarray, 16) == 0);
    TS_ASSERT(memcmp(a10, llarray, 16) == 0);

    carray[0] = 77;
    carray[1] = 88;
    sarray[0] = 77;
    sarray[1] = 88;
    larray[0] = 77;
    larray[1] = 88;
    llarray[0] = 77;
    llarray[1] = 88;
    farray[0] = 77.f;
    farray[1] = 88.f;
    darray[0] = 77.;
    darray[1] = 88.;
    a1.from_long(0, larray[0]);
    a2.from_long(0, larray[0]);
    a3.from_long(0, larray[0]);
    a4.from_long(0, larray[0]);
    a5.from_long(0, larray[0]);
    a6.from_long(0, larray[0]);
    a7.from_long(0, larray[0]);
    a8.from_long(0, larray[0]);
    a9.from_long(0, larray[0]);
    a10.from_long(0, larray[0]);
    a1.from_long(1, larray[1]);
    a2.from_long(1, larray[1]);
    a3.from_long(1, larray[1]);
    a4.from_long(1, larray[1]);
    a5.from_long(1, larray[1]);
    a6.from_long(1, larray[1]);
    a7.from_long(1, larray[1]);
    a8.from_long(1, larray[1]);
    a9.from_long(1, larray[1]);
    a10.from_long(1, larray[1]);
    TS_ASSERT(memcmp(a1, carray, 2) == 0);
    TS_ASSERT(memcmp(a2, carray, 2) == 0);
    TS_ASSERT(memcmp(a3, sarray, 4) == 0);
    TS_ASSERT(memcmp(a4, sarray, 4) == 0);
    TS_ASSERT(memcmp(a5, larray, 8) == 0);
    TS_ASSERT(memcmp(a6, larray, 8) == 0);
    TS_ASSERT(memcmp(a7, farray, 8) == 0);
    TS_ASSERT(memcmp(a8, darray, 16) == 0);
    TS_ASSERT(memcmp(a9, llarray, 16) == 0);
    TS_ASSERT(memcmp(a10, llarray, 16) == 0);

    carray[0] = 99;
    carray[1] = 11;
    sarray[0] = 99;
    sarray[1] = 11;
    larray[0] = 99;
    larray[1] = 11;
    llarray[0] = 99;
    llarray[1] = 11;
    farray[0] = 99.f;
    farray[1] = 11.f;
    darray[0] = 99.;
    darray[1] = 11.;
    a1.from_float(0, farray[0]);
    a2.from_float(0, farray[0]);
    a3.from_float(0, farray[0]);
    a4.from_float(0, farray[0]);
    a5.from_float(0, farray[0]);
    a6.from_float(0, farray[0]);
    a7.from_float(0, farray[0]);
    a8.from_float(0, farray[0]);
    a9.from_float(0, farray[0]);
    a10.from_float(0, farray[0]);
    a1.from_float(1, farray[1]);
    a2.from_float(1, farray[1]);
    a3.from_float(1, farray[1]);
    a4.from_float(1, farray[1]);
    a5.from_float(1, farray[1]);
    a6.from_float(1, farray[1]);
    a7.from_float(1, farray[1]);
    a8.from_float(1, farray[1]);
    a9.from_float(1, farray[1]);
    a10.from_float(1, farray[1]);
    TS_ASSERT(memcmp(a1, carray, 2) == 0);
    TS_ASSERT(memcmp(a2, carray, 2) == 0);
    TS_ASSERT(memcmp(a3, sarray, 4) == 0);
    TS_ASSERT(memcmp(a4, sarray, 4) == 0);
    TS_ASSERT(memcmp(a5, larray, 8) == 0);
    TS_ASSERT(memcmp(a6, larray, 8) == 0);
    TS_ASSERT(memcmp(a7, farray, 8) == 0);
    TS_ASSERT(memcmp(a8, darray, 16) == 0);
    TS_ASSERT(memcmp(a9, llarray, 16) == 0);
    TS_ASSERT(memcmp(a10, llarray, 16) == 0);

    carray[0] = 22;
    carray[1] = 33;
    sarray[0] = 22;
    sarray[1] = 33;
    larray[0] = 22;
    larray[1] = 33;
    llarray[0] = 22;
    llarray[1] = 33;
    farray[0] = 22.f;
    farray[1] = 33.f;
    darray[0] = 22.;
    darray[1] = 33.;
    a1.from_double(0, darray[0]);
    a2.from_double(0, darray[0]);
    a3.from_double(0, darray[0]);
    a4.from_double(0, darray[0]);
    a5.from_double(0, darray[0]);
    a6.from_double(0, darray[0]);
    a7.from_double(0, darray[0]);
    a8.from_double(0, darray[0]);
    a9.from_double(0, darray[0]);
    a10.from_double(0, darray[0]);
    a1.from_double(1, darray[1]);
    a2.from_double(1, darray[1]);
    a3.from_double(1, darray[1]);
    a4.from_double(1, darray[1]);
    a5.from_double(1, darray[1]);
    a6.from_double(1, darray[1]);
    a7.from_double(1, darray[1]);
    a8.from_double(1, darray[1]);
    a9.from_double(1, darray[1]);
    a10.from_double(1, darray[1]);
    TS_ASSERT(memcmp(a1, carray, 2) == 0);
    TS_ASSERT(memcmp(a2, carray, 2) == 0);
    TS_ASSERT(memcmp(a3, sarray, 4) == 0);
    TS_ASSERT(memcmp(a4, sarray, 4) == 0);
    TS_ASSERT(memcmp(a5, larray, 8) == 0);
    TS_ASSERT(memcmp(a6, larray, 8) == 0);
    TS_ASSERT(memcmp(a7, farray, 8) == 0);
    TS_ASSERT(memcmp(a8, darray, 16) == 0);
    TS_ASSERT(memcmp(a9, llarray, 16) == 0);
    TS_ASSERT(memcmp(a10, llarray, 16) == 0);

    carray[0] = 34;
    carray[1] = 56;
    sarray[0] = 34;
    sarray[1] = 56;
    larray[0] = 34;
    larray[1] = 56;
    llarray[0] = 34;
    llarray[1] = 56;
    farray[0] = 34.f;
    farray[1] = 56.f;
    darray[0] = 34.;
    darray[1] = 56.;
    a1.from_char_ptr(0, "34");
    a2.from_char_ptr(0, "34");
    a3.from_char_ptr(0, "34");
    a4.from_char_ptr(0, "34");
    a5.from_char_ptr(0, "34");
    a6.from_char_ptr(0, "34");
    a7.from_char_ptr(0, "34");
    a8.from_char_ptr(0, "34");
    a9.from_char_ptr(0, "34");
    a10.from_char_ptr(0, "34");
    a1.from_char_ptr(1, "56");
    a2.from_char_ptr(1, "56");
    a3.from_char_ptr(1, "56");
    a4.from_char_ptr(1, "56");
    a5.from_char_ptr(1, "56");
    a6.from_char_ptr(1, "56");
    a7.from_char_ptr(1, "56");
    a8.from_char_ptr(1, "56");
    a9.from_char_ptr(1, "56");
    a10.from_char_ptr(1, "56");
    TS_ASSERT(memcmp(a1, carray, 2) == 0);
    TS_ASSERT(memcmp(a2, carray, 2) == 0);
    TS_ASSERT(memcmp(a3, sarray, 4) == 0);
    TS_ASSERT(memcmp(a4, sarray, 4) == 0);
    TS_ASSERT(memcmp(a5, larray, 8) == 0);
    TS_ASSERT(memcmp(a6, larray, 8) == 0);
    TS_ASSERT(memcmp(a7, farray, 8) == 0);
    TS_ASSERT(memcmp(a8, darray, 16) == 0);
    TS_ASSERT(memcmp(a9, llarray, 16) == 0);
    TS_ASSERT(memcmp(a10, llarray, 16) == 0);

    carray[0] = 78;
    carray[1] = 90;
    sarray[0] = 78;
    sarray[1] = 90;
    larray[0] = 78;
    larray[1] = 90;
    llarray[0] = 78;
    llarray[1] = 90;
    farray[0] = 78.f;
    farray[1] = 90.f;
    darray[0] = 78.;
    darray[1] = 90.;
    a1.from_long_long(0, llarray[0]);
    a2.from_long_long(0, llarray[0]);
    a3.from_long_long(0, llarray[0]);
    a4.from_long_long(0, llarray[0]);
    a5.from_long_long(0, llarray[0]);
    a6.from_long_long(0, llarray[0]);
    a7.from_long_long(0, llarray[0]);
    a8.from_long_long(0, llarray[0]);
    a9.from_long_long(0, llarray[0]);
    a10.from_long_long(0, llarray[0]);
    a1.from_long_long(1, llarray[1]);
    a2.from_long_long(1, llarray[1]);
    a3.from_long_long(1, llarray[1]);
    a4.from_long_long(1, llarray[1]);
    a5.from_long_long(1, llarray[1]);
    a6.from_long_long(1, llarray[1]);
    a7.from_long_long(1, llarray[1]);
    a8.from_long_long(1, llarray[1]);
    a9.from_long_long(1, llarray[1]);
    a10.from_long_long(1, llarray[1]);
    TS_ASSERT(memcmp(a1, carray, 2) == 0);
    TS_ASSERT(memcmp(a2, carray, 2) == 0);
    TS_ASSERT(memcmp(a3, sarray, 4) == 0);
    TS_ASSERT(memcmp(a4, sarray, 4) == 0);
    TS_ASSERT(memcmp(a5, larray, 8) == 0);
    TS_ASSERT(memcmp(a6, larray, 8) == 0);
    TS_ASSERT(memcmp(a7, farray, 8) == 0);
    TS_ASSERT(memcmp(a8, darray, 16) == 0);
    TS_ASSERT(memcmp(a9, llarray, 16) == 0);
    TS_ASSERT(memcmp(a10, llarray, 16) == 0);
}

class id_key_typ : public complex_var
{
public:
    COMPLEXCONST(id_key_typ)
    generic_var<unsigned int> serial;
    generic_var<unsigned int> id;
    r_array_var<unsigned char> key;
};

flex_var<id_key_typ> getkey()
{
    flex_var<id_key_typ> result;
    id_key_typ key;
    key.serial = 0x1234;
    key.id = 0x5678;
    key.key.resize(20);
    for (int cnt=0; cnt< 20; ++cnt)
        key.key.from_int(cnt, cnt+10);
    result.add(key);
    return result;
}

flex_var<id_key_typ> getkey2()
{
    flex_var<id_key_typ> result;
    id_key_typ key;
    key.serial = 0x4321;
    key.id = 0x8765;
    key.key.resize(10);
    for (int cnt=0; cnt< 10; ++cnt)
        key.key.from_int(cnt, cnt+10);
    result.add(key);
    key.serial = 0x9900;
    key.id = 0x1122;
    key.key.resize(5);
    for (int cnt=0; cnt< 5; ++cnt)
        key.key.from_int(cnt, cnt+5);
    result.add(key);
    return result;
}

void check_array(void *vptr, unsigned char *ucptr)
{
    TS_ASSERT(vptr == ucptr);
}

void ArrayVarTestSuite::testFlexCopy()
{
    flex_var<id_key_typ> keys = getkey();
    TS_ASSERT(keys.cardinal() == 1);
    TS_ASSERT(keys[0]->serial.to_unsigned_int() == 0x1234);
    TS_ASSERT(keys[0]->id.to_unsigned_int() == 0x5678);
    TS_ASSERT(keys[0]->key.cardinal() == 20);
    for (int cnt=0; cnt< 20; ++cnt)
        TS_ASSERT(keys[0]->key.to_int(cnt) == cnt+10);

    keys = getkey2();
    TS_ASSERT(keys.cardinal() == 2);
    TS_ASSERT(keys[0]->serial.to_unsigned_int() == 0x4321);
    TS_ASSERT(keys[0]->id.to_unsigned_int() == 0x8765);
    TS_ASSERT(keys[0]->key.cardinal() == 10);
    for (int cnt=0; cnt< 10; ++cnt)
        TS_ASSERT(keys[0]->key.to_int(cnt) == cnt+10);
    TS_ASSERT(keys[1]->serial.to_unsigned_int() == 0x9900);
    TS_ASSERT(keys[1]->id.to_unsigned_int() == 0x1122);
    TS_ASSERT(keys[1]->key.cardinal() == 5);
    for (int cnt=0; cnt< 5; ++cnt)
        TS_ASSERT(keys[1]->key.to_int(cnt) == cnt+5);

    check_array(keys[1]->key, keys[1]->key);
}

void ArrayVarTestSuite::testRtti()
{
    // arrays
    r_array_var<unsigned char> a1;
    r_array_var<char> a2;
    r_array_var<unsigned short> a3;
    r_array_var<short> a4;
    r_array_var<unsigned long> a5;
    r_array_var<long> a6;
    r_array_var<unsigned long long> a7;
    r_array_var<long long> a8;
    r_array_var<float> a9;
    r_array_var<double> a10;

    r_array_var<unsigned char> b1;
    r_array_var<char> b2;
    r_array_var<unsigned short> b3;
    r_array_var<short> b4;
    r_array_var<unsigned long> b5;
    r_array_var<long> b6;
    r_array_var<unsigned long long> b7;
    r_array_var<long long> b8;
    r_array_var<float> b9;
    r_array_var<double> b10;

    TS_ASSERT_EQUALS(a1.rtti(), b1.rtti());
    TS_ASSERT_EQUALS(a2.rtti(), b2.rtti());
    TS_ASSERT_EQUALS(a3.rtti(), b3.rtti());
    TS_ASSERT_EQUALS(a4.rtti(), b4.rtti());
    TS_ASSERT_EQUALS(a5.rtti(), b5.rtti());
    TS_ASSERT_EQUALS(a6.rtti(), b6.rtti());
    TS_ASSERT_EQUALS(a7.rtti(), b7.rtti());
    TS_ASSERT_EQUALS(a8.rtti(), b8.rtti());
    TS_ASSERT_EQUALS(a9.rtti(), b9.rtti());
    TS_ASSERT_EQUALS(a10.rtti(), b10.rtti());
    TS_ASSERT_DIFFERS(a1.rtti(), b2.rtti());
    TS_ASSERT_DIFFERS(a1.rtti(), b3.rtti());
    TS_ASSERT_DIFFERS(a1.rtti(), b4.rtti());
    TS_ASSERT_DIFFERS(a1.rtti(), b5.rtti());
    TS_ASSERT_DIFFERS(a1.rtti(), b6.rtti());
    TS_ASSERT_DIFFERS(a1.rtti(), b7.rtti());
    TS_ASSERT_DIFFERS(a1.rtti(), b8.rtti());
    TS_ASSERT_DIFFERS(a1.rtti(), b9.rtti());
    TS_ASSERT_DIFFERS(a1.rtti(), b10.rtti());
    TS_ASSERT_DIFFERS(a2.rtti(), b3.rtti());
    TS_ASSERT_DIFFERS(a2.rtti(), b4.rtti());
    TS_ASSERT_DIFFERS(a2.rtti(), b5.rtti());
    TS_ASSERT_DIFFERS(a2.rtti(), b6.rtti());
    TS_ASSERT_DIFFERS(a2.rtti(), b7.rtti());
    TS_ASSERT_DIFFERS(a2.rtti(), b8.rtti());
    TS_ASSERT_DIFFERS(a2.rtti(), b9.rtti());
    TS_ASSERT_DIFFERS(a2.rtti(), b10.rtti());
    TS_ASSERT_DIFFERS(a3.rtti(), b4.rtti());
    TS_ASSERT_DIFFERS(a3.rtti(), b5.rtti());
    TS_ASSERT_DIFFERS(a3.rtti(), b6.rtti());
    TS_ASSERT_DIFFERS(a3.rtti(), b7.rtti());
    TS_ASSERT_DIFFERS(a3.rtti(), b8.rtti());
    TS_ASSERT_DIFFERS(a3.rtti(), b9.rtti());
    TS_ASSERT_DIFFERS(a3.rtti(), b10.rtti());
    TS_ASSERT_DIFFERS(a4.rtti(), b5.rtti());
    TS_ASSERT_DIFFERS(a4.rtti(), b6.rtti());
    TS_ASSERT_DIFFERS(a4.rtti(), b7.rtti());
    TS_ASSERT_DIFFERS(a4.rtti(), b8.rtti());
    TS_ASSERT_DIFFERS(a4.rtti(), b9.rtti());
    TS_ASSERT_DIFFERS(a4.rtti(), b10.rtti());
    TS_ASSERT_DIFFERS(a5.rtti(), b6.rtti());
    TS_ASSERT_DIFFERS(a5.rtti(), b7.rtti());
    TS_ASSERT_DIFFERS(a5.rtti(), b8.rtti());
    TS_ASSERT_DIFFERS(a5.rtti(), b9.rtti());
    TS_ASSERT_DIFFERS(a5.rtti(), b10.rtti());
    TS_ASSERT_DIFFERS(a6.rtti(), b7.rtti());
    TS_ASSERT_DIFFERS(a6.rtti(), b8.rtti());
    TS_ASSERT_DIFFERS(a6.rtti(), b9.rtti());
    TS_ASSERT_DIFFERS(a6.rtti(), b10.rtti());
    TS_ASSERT_DIFFERS(a7.rtti(), b8.rtti());
    TS_ASSERT_DIFFERS(a7.rtti(), b9.rtti());
    TS_ASSERT_DIFFERS(a7.rtti(), b10.rtti());
    TS_ASSERT_DIFFERS(a8.rtti(), b9.rtti());
    TS_ASSERT_DIFFERS(a8.rtti(), b10.rtti());
    TS_ASSERT_DIFFERS(a9.rtti(), b10.rtti());
}

void ArrayVarTestSuite::testRoundingAndClipping()
{
    char sch_v = -126;
    unsigned char uch_v = 223;
    short ssh_v = -31149;
    unsigned short ush_v = 63055;
    int sit_v = -2101804716;
    unsigned int uit_v = 4249268584;
    long slg_v = -943252216;
    unsigned long ulg_v = 3801804716;
    long long sll_v = -36028797018963968LL;
    unsigned long long ull_v = 9223372036854779809ULL;
    double ndbl = -1.e21;
    float nflt = -1.e21f;
    double pdbl = 1e21;
    float pflt = 1e21f;

    char nstr[] = "-36028797018963968";
    char pstr[] = "9223372036854779809";

    int asz, aidx;
    // clip to minimum
    r_array_var<char> avch;
    asz = rand() % 150 + 1;
    avch.resize(asz);
    TS_ASSERT_EQUALS(avch.size(), sizeof(short) + asz * sizeof(char));
    aidx = rand() % asz;
    avch.fromValue(aidx, ssh_v);
    TS_ASSERT_EQUALS(avch[aidx], CHAR_MIN);
    aidx = rand() % asz;
    avch.fromValue(aidx, sit_v);
    TS_ASSERT_EQUALS(avch[aidx], CHAR_MIN);
    aidx = rand() % asz;
    avch.fromValue(aidx, slg_v);
    TS_ASSERT_EQUALS(avch[aidx], CHAR_MIN);
    aidx = rand() % asz;
    avch.fromValue(aidx, sll_v);
    TS_ASSERT_EQUALS(avch[aidx], CHAR_MIN);
    aidx = rand() % asz;
    avch.fromValue(aidx, nstr);
    TS_ASSERT_EQUALS(avch[aidx], CHAR_MIN);
    aidx = rand() % asz;
    avch.fromValue(aidx, nflt);
    TS_ASSERT_EQUALS(avch[aidx], CHAR_MIN);
    aidx = rand() % asz;
    avch.fromValue(aidx, ndbl);
    TS_ASSERT_EQUALS(avch[aidx], CHAR_MIN);

    r_array_var<unsigned char> auch;
    asz = rand() % 150 + 1;
    auch.resize(asz);
    TS_ASSERT_EQUALS(auch.size(), sizeof(short) + asz * sizeof(char));
    aidx = rand() % asz;
    auch.fromValue(aidx, sch_v);
    TS_ASSERT_EQUALS(auch[aidx], 0);
    aidx = rand() % asz;
    auch.fromValue(aidx, ssh_v);
    TS_ASSERT_EQUALS(auch[aidx], 0);
    aidx = rand() % asz;
    auch.fromValue(aidx, sit_v);
    TS_ASSERT_EQUALS(auch[aidx], 0);
    aidx = rand() % asz;
    auch.fromValue(aidx, slg_v);
    TS_ASSERT_EQUALS(auch[aidx], 0);
    aidx = rand() % asz;
    auch.fromValue(aidx, sll_v);
    TS_ASSERT_EQUALS(auch[aidx], 0);
    aidx = rand() % asz;
    auch.fromValue(aidx, nstr);
    TS_ASSERT_EQUALS(auch[aidx], 0);
    aidx = rand() % asz;
    auch.fromValue(aidx, nflt);
    TS_ASSERT_EQUALS(auch[aidx], 0);
    aidx = rand() % asz;
    auch.fromValue(aidx, ndbl);
    TS_ASSERT_EQUALS(auch[aidx], 0);

    r_array_var<short> avsh;
    asz = rand() % 150 + 1;
    avsh.resize(asz);
    TS_ASSERT_EQUALS(avsh.size(), sizeof(short) + asz * sizeof(short));
    aidx = rand() % asz;
    avsh.fromValue(aidx, sit_v);
    TS_ASSERT_EQUALS(avsh[aidx], SHRT_MIN);
    aidx = rand() % asz;
    avsh.fromValue(aidx, slg_v);
    TS_ASSERT_EQUALS(avsh[aidx], SHRT_MIN);
    aidx = rand() % asz;
    avsh.fromValue(aidx, sll_v);
    TS_ASSERT_EQUALS(avsh[aidx], SHRT_MIN);
    aidx = rand() % asz;
    avsh.fromValue(aidx, nstr);
    TS_ASSERT_EQUALS(avsh[aidx], SHRT_MIN);
    aidx = rand() % asz;
    avsh.fromValue(aidx, nflt);
    TS_ASSERT_EQUALS(avsh[aidx], SHRT_MIN);
    aidx = rand() % asz;
    avsh.fromValue(aidx, ndbl);
    TS_ASSERT_EQUALS(avsh[aidx], SHRT_MIN);

    r_array_var<unsigned short> aush;
    asz = rand() % 150 + 1;
    aush.resize(asz);
    TS_ASSERT_EQUALS(aush.size(), sizeof(short) + asz * sizeof(short));
    aidx = rand() % asz;
    aush.fromValue(aidx, sch_v);
    TS_ASSERT_EQUALS(aush[aidx], 0);
    aidx = rand() % asz;
    aush.fromValue(aidx, ssh_v);
    TS_ASSERT_EQUALS(aush[aidx], 0);
    aidx = rand() % asz;
    aush.fromValue(aidx, sit_v);
    TS_ASSERT_EQUALS(aush[aidx], 0);
    aidx = rand() % asz;
    aush.fromValue(aidx, slg_v);
    TS_ASSERT_EQUALS(aush[aidx], 0);
    aidx = rand() % asz;
    aush.fromValue(aidx, sll_v);
    TS_ASSERT_EQUALS(aush[aidx], 0);
    aidx = rand() % asz;
    aush.fromValue(aidx, nstr);
    TS_ASSERT_EQUALS(aush[aidx], 0);
    aidx = rand() % asz;
    aush.fromValue(aidx, nflt);
    TS_ASSERT_EQUALS(aush[aidx], 0);
    aidx = rand() % asz;
    aush.fromValue(aidx, ndbl);
    TS_ASSERT_EQUALS(aush[aidx], 0);

    r_array_var<int> avit;
    asz = rand() % 150 + 1;
    avit.resize(asz);
    TS_ASSERT_EQUALS(avit.size(), sizeof(short) + asz * sizeof(int));
    aidx = rand() % asz;
    avit.fromValue(aidx, sll_v);
    TS_ASSERT_EQUALS(avit[aidx], INT_MIN);
    aidx = rand() % asz;
    avit.fromValue(aidx, nstr);
    TS_ASSERT_EQUALS(avit[aidx], INT_MIN);
    aidx = rand() % asz;
    avit.fromValue(aidx, nflt);
    TS_ASSERT_EQUALS(avit[aidx], INT_MIN);
    aidx = rand() % asz;
    avit.fromValue(aidx, ndbl);
    TS_ASSERT_EQUALS(avit[aidx], INT_MIN);

    r_array_var<unsigned int> auit;
    asz = rand() % 150 + 1;
    auit.resize(asz);
    TS_ASSERT_EQUALS(auit.size(), sizeof(short) + asz * sizeof(int));
    aidx = rand() % asz;
    auit.fromValue(aidx, sch_v);
    TS_ASSERT_EQUALS(auit[aidx], 0);
    aidx = rand() % asz;
    auit.fromValue(aidx, ssh_v);
    TS_ASSERT_EQUALS(auit[aidx], 0);
    aidx = rand() % asz;
    auit.fromValue(aidx, sit_v);
    TS_ASSERT_EQUALS(auit[aidx], 0);
    aidx = rand() % asz;
    auit.fromValue(aidx, slg_v);
    TS_ASSERT_EQUALS(auit[aidx], 0);
    aidx = rand() % asz;
    auit.fromValue(aidx, sll_v);
    TS_ASSERT_EQUALS(auit[aidx], 0);
    aidx = rand() % asz;
    auit.fromValue(aidx, nstr);
    TS_ASSERT_EQUALS(auit[aidx], 0);
    aidx = rand() % asz;
    auit.fromValue(aidx, nflt);
    TS_ASSERT_EQUALS(auit[aidx], 0);
    aidx = rand() % asz;
    auit.fromValue(aidx, ndbl);
    TS_ASSERT_EQUALS(auit[aidx], 0);

    r_array_var<long> avlg;
    asz = rand() % 150 + 1;
    avlg.resize(asz);
    TS_ASSERT_EQUALS(avlg.size(), sizeof(short) + asz * sizeof(long));
    aidx = rand() % asz;
    avlg.fromValue(aidx, sll_v);
    TS_ASSERT_EQUALS(avlg[aidx], LONG_MIN);
    aidx = rand() % asz;
    avlg.fromValue(aidx, nstr);
    TS_ASSERT_EQUALS(avlg[aidx], LONG_MIN);
    aidx = rand() % asz;
    avlg.fromValue(aidx, nflt);
    TS_ASSERT_EQUALS(avlg[aidx], LONG_MIN);
    aidx = rand() % asz;
    avlg.fromValue(aidx, ndbl);
    TS_ASSERT_EQUALS(avlg[aidx], LONG_MIN);

    r_array_var<unsigned long> aulg;
    asz = rand() % 150 + 1;
    aulg.resize(asz);
    TS_ASSERT_EQUALS(aulg.size(), sizeof(short) + asz * sizeof(long));
    aidx = rand() % asz;
    aulg.fromValue(aidx, sch_v);
    TS_ASSERT_EQUALS(aulg[aidx], 0);
    aidx = rand() % asz;
    aulg.fromValue(aidx, ssh_v);
    TS_ASSERT_EQUALS(aulg[aidx], 0);
    aidx = rand() % asz;
    aulg.fromValue(aidx, sit_v);
    TS_ASSERT_EQUALS(aulg[aidx], 0);
    aidx = rand() % asz;
    aulg.fromValue(aidx, slg_v);
    TS_ASSERT_EQUALS(aulg[aidx], 0);
    aidx = rand() % asz;
    aulg.fromValue(aidx, sll_v);
    TS_ASSERT_EQUALS(aulg[aidx], 0);
    aidx = rand() % asz;
    aulg.fromValue(aidx, nstr);
    TS_ASSERT_EQUALS(aulg[aidx], 0);
    aidx = rand() % asz;
    aulg.fromValue(aidx, nflt);
    TS_ASSERT_EQUALS(aulg[aidx], 0);
    aidx = rand() % asz;
    aulg.fromValue(aidx, ndbl);
    TS_ASSERT_EQUALS(aulg[aidx], 0);

    r_array_var<long long> avll;
    r_array_var<unsigned long long> aull;
    asz = rand() % 150 + 1;
    aull.resize(asz);
    TS_ASSERT_EQUALS(aull.size(), sizeof(short) + asz * sizeof(long long));
    aidx = rand() % asz;
    aull.fromValue(aidx, sch_v);
    TS_ASSERT_EQUALS(aull[aidx], 0);
    aidx = rand() % asz;
    aull.fromValue(aidx, ssh_v);
    TS_ASSERT_EQUALS(aull[aidx], 0);
    aidx = rand() % asz;
    aull.fromValue(aidx, sit_v);
    TS_ASSERT_EQUALS(aull[aidx], 0);
    aidx = rand() % asz;
    aull.fromValue(aidx, slg_v);
    TS_ASSERT_EQUALS(aull[aidx], 0);
    aidx = rand() % asz;
    aull.fromValue(aidx, sll_v);
    TS_ASSERT_EQUALS(aull[aidx], 0);
    aidx = rand() % asz;
    aull.fromValue(aidx, nstr);
    TS_ASSERT_EQUALS(aull[aidx], 0);
    aidx = rand() % asz;
    aull.fromValue(aidx, nflt);
    TS_ASSERT_EQUALS(aull[aidx], 0);
    aidx = rand() % asz;
    aull.fromValue(aidx, ndbl);
    TS_ASSERT_EQUALS(aull[aidx], 0);

    // clip to maximum
    asz = rand() % 150 + 1;
    avch.resize(asz);
    TS_ASSERT_EQUALS(avch.size(), sizeof(short) + asz * sizeof(char));
    aidx = rand() % asz;
    avch.fromValue(aidx, uch_v);
    TS_ASSERT_EQUALS(avch[aidx], CHAR_MAX);
    aidx = rand() % asz;
    avch.fromValue(aidx, -ssh_v);
    TS_ASSERT_EQUALS(avch[aidx], CHAR_MAX);
    aidx = rand() % asz;
    avch.fromValue(aidx, ush_v);
    TS_ASSERT_EQUALS(avch[aidx], CHAR_MAX);
    aidx = rand() % asz;
    avch.fromValue(aidx, -sit_v);
    TS_ASSERT_EQUALS(avch[aidx], CHAR_MAX);
    aidx = rand() % asz;
    avch.fromValue(aidx, uit_v);
    TS_ASSERT_EQUALS(avch[aidx], CHAR_MAX);
    aidx = rand() % asz;
    avch.fromValue(aidx, -slg_v);
    TS_ASSERT_EQUALS(avch[aidx], CHAR_MAX);
    aidx = rand() % asz;
    avch.fromValue(aidx, ulg_v);
    TS_ASSERT_EQUALS(avch[aidx], CHAR_MAX);
    aidx = rand() % asz;
    avch.fromValue(aidx, -sll_v);
    TS_ASSERT_EQUALS(avch[aidx], CHAR_MAX);
    aidx = rand() % asz;
    avch.fromValue(aidx, ull_v);
    TS_ASSERT_EQUALS(avch[aidx], CHAR_MAX);
    aidx = rand() % asz;
    avch.fromValue(aidx, pstr);
    TS_ASSERT_EQUALS(avch[aidx], CHAR_MAX);
    aidx = rand() % asz;
    avch.fromValue(aidx, pflt);
    TS_ASSERT_EQUALS(avch[aidx], CHAR_MAX);
    aidx = rand() % asz;
    avch.fromValue(aidx, pdbl);
    TS_ASSERT_EQUALS(avch[aidx], CHAR_MAX);

    asz = rand() % 150 + 1;
    auch.resize(asz);
    TS_ASSERT_EQUALS(auch.size(), sizeof(short) + asz * sizeof(char));
    aidx = rand() % asz;
    auch.fromValue(aidx, -ssh_v);
    TS_ASSERT_EQUALS(auch[aidx], UCHAR_MAX);
    aidx = rand() % asz;
    auch.fromValue(aidx, ush_v);
    TS_ASSERT_EQUALS(auch[aidx], UCHAR_MAX);
    aidx = rand() % asz;
    auch.fromValue(aidx, -sit_v);
    TS_ASSERT_EQUALS(auch[aidx], UCHAR_MAX);
    aidx = rand() % asz;
    auch.fromValue(aidx, uit_v);
    TS_ASSERT_EQUALS(auch[aidx], UCHAR_MAX);
    aidx = rand() % asz;
    auch.fromValue(aidx, -slg_v);
    TS_ASSERT_EQUALS(auch[aidx], UCHAR_MAX);
    aidx = rand() % asz;
    auch.fromValue(aidx, ulg_v);
    TS_ASSERT_EQUALS(auch[aidx], UCHAR_MAX);
    aidx = rand() % asz;
    auch.fromValue(aidx, -sll_v);
    TS_ASSERT_EQUALS(auch[aidx], UCHAR_MAX);
    aidx = rand() % asz;
    auch.fromValue(aidx, ull_v);
    TS_ASSERT_EQUALS(auch[aidx], UCHAR_MAX);
    aidx = rand() % asz;
    auch.fromValue(aidx, pstr);
    TS_ASSERT_EQUALS(auch[aidx], UCHAR_MAX);
    aidx = rand() % asz;
    auch.fromValue(aidx, pflt);
    TS_ASSERT_EQUALS(auch[aidx], UCHAR_MAX);
    aidx = rand() % asz;
    auch.fromValue(aidx, pdbl);
    TS_ASSERT_EQUALS(auch[aidx], UCHAR_MAX);

    asz = rand() % 150 + 1;
    avsh.resize(asz);
    TS_ASSERT_EQUALS(avsh.size(), sizeof(short) + asz * sizeof(short));
    aidx = rand() % asz;
    avsh.fromValue(aidx, ush_v);
    TS_ASSERT_EQUALS(avsh[aidx], SHRT_MAX);
    aidx = rand() % asz;
    avsh.fromValue(aidx, -sit_v);
    TS_ASSERT_EQUALS(avsh[aidx], SHRT_MAX);
    aidx = rand() % asz;
    avsh.fromValue(aidx, uit_v);
    TS_ASSERT_EQUALS(avsh[aidx], SHRT_MAX);
    aidx = rand() % asz;
    avsh.fromValue(aidx, -slg_v);
    TS_ASSERT_EQUALS(avsh[aidx], SHRT_MAX);
    aidx = rand() % asz;
    avsh.fromValue(aidx, ulg_v);
    TS_ASSERT_EQUALS(avsh[aidx], SHRT_MAX);
    aidx = rand() % asz;
    avsh.fromValue(aidx, -sll_v);
    TS_ASSERT_EQUALS(avsh[aidx], SHRT_MAX);
    aidx = rand() % asz;
    avsh.fromValue(aidx, ull_v);
    TS_ASSERT_EQUALS(avsh[aidx], SHRT_MAX);
    aidx = rand() % asz;
    avsh.fromValue(aidx, pstr);
    TS_ASSERT_EQUALS(avsh[aidx], SHRT_MAX);
    aidx = rand() % asz;
    avsh.fromValue(aidx, pflt);
    TS_ASSERT_EQUALS(avsh[aidx], SHRT_MAX);
    aidx = rand() % asz;
    avsh.fromValue(aidx, pdbl);
    TS_ASSERT_EQUALS(avsh[aidx], SHRT_MAX);

    asz = rand() % 150 + 1;
    aush.resize(asz);
    TS_ASSERT_EQUALS(aush.size(), sizeof(short) + asz * sizeof(short));
    aidx = rand() % asz;
    aush.fromValue(aidx, -sit_v);
    TS_ASSERT_EQUALS(aush[aidx], USHRT_MAX);
    aidx = rand() % asz;
    aush.fromValue(aidx, uit_v);
    TS_ASSERT_EQUALS(aush[aidx], USHRT_MAX);
    aidx = rand() % asz;
    aush.fromValue(aidx, -slg_v);
    TS_ASSERT_EQUALS(aush[aidx], USHRT_MAX);
    aidx = rand() % asz;
    aush.fromValue(aidx, ulg_v);
    TS_ASSERT_EQUALS(aush[aidx], USHRT_MAX);
    aidx = rand() % asz;
    aush.fromValue(aidx, -sll_v);
    TS_ASSERT_EQUALS(aush[aidx], USHRT_MAX);
    aidx = rand() % asz;
    aush.fromValue(aidx, ull_v);
    TS_ASSERT_EQUALS(aush[aidx], USHRT_MAX);
    aidx = rand() % asz;
    aush.fromValue(aidx, pstr);
    TS_ASSERT_EQUALS(aush[aidx], USHRT_MAX);
    aidx = rand() % asz;
    aush.fromValue(aidx, pflt);
    TS_ASSERT_EQUALS(aush[aidx], USHRT_MAX);
    aidx = rand() % asz;
    aush.fromValue(aidx, pdbl);
    TS_ASSERT_EQUALS(aush[aidx], USHRT_MAX);

    asz = rand() % 150 + 1;
    avit.resize(asz);
    TS_ASSERT_EQUALS(avit.size(), sizeof(short) + asz * sizeof(int));
    aidx = rand() % asz;
    avit.fromValue(aidx, uit_v);
    TS_ASSERT_EQUALS(avit[aidx], INT_MAX);
    aidx = rand() % asz;
    avit.fromValue(aidx, ulg_v);
    TS_ASSERT_EQUALS(avit[aidx], INT_MAX);
    aidx = rand() % asz;
    avit.fromValue(aidx, -sll_v);
    TS_ASSERT_EQUALS(avit[aidx], INT_MAX);
    aidx = rand() % asz;
    avit.fromValue(aidx, ull_v);
    TS_ASSERT_EQUALS(avit[aidx], INT_MAX);
    aidx = rand() % asz;
    avit.fromValue(aidx, pstr);
    TS_ASSERT_EQUALS(avit[aidx], INT_MAX);
    aidx = rand() % asz;
    avit.fromValue(aidx, pflt);
    TS_ASSERT_EQUALS(avit[aidx], INT_MAX);
    aidx = rand() % asz;
    avit.fromValue(aidx, pdbl);
    TS_ASSERT_EQUALS(avit[aidx], INT_MAX);

    asz = rand() % 150 + 1;
    auit.resize(asz);
    TS_ASSERT_EQUALS(auit.size(), sizeof(short) + asz * sizeof(int));
    aidx = rand() % asz;
    auit.fromValue(aidx, -sll_v);
    TS_ASSERT_EQUALS(auit[aidx], UINT_MAX);
    aidx = rand() % asz;
    auit.fromValue(aidx, ull_v);
    TS_ASSERT_EQUALS(auit[aidx], UINT_MAX);
    aidx = rand() % asz;
    auit.fromValue(aidx, pstr);
    TS_ASSERT_EQUALS(auit[aidx], UINT_MAX);
    aidx = rand() % asz;
    auit.fromValue(aidx, pflt);
    TS_ASSERT_EQUALS(auit[aidx], UINT_MAX);
    aidx = rand() % asz;
    auit.fromValue(aidx, pdbl);
    TS_ASSERT_EQUALS(auit[aidx], UINT_MAX);

    asz = rand() % 150 + 1;
    avlg.resize(asz);
    TS_ASSERT_EQUALS(avlg.size(), sizeof(short) + asz * sizeof(long));
    aidx = rand() % asz;
    avlg.fromValue(aidx, ulg_v);
    TS_ASSERT_EQUALS(avlg[aidx], LONG_MAX);
    aidx = rand() % asz;
    avlg.fromValue(aidx, ulg_v);
    TS_ASSERT_EQUALS(avlg[aidx], LONG_MAX);
    aidx = rand() % asz;
    avlg.fromValue(aidx, -sll_v);
    TS_ASSERT_EQUALS(avlg[aidx], LONG_MAX);
    aidx = rand() % asz;
    avlg.fromValue(aidx, ull_v);
    TS_ASSERT_EQUALS(avlg[aidx], LONG_MAX);
    aidx = rand() % asz;
    avlg.fromValue(aidx, pstr);
    TS_ASSERT_EQUALS(avlg[aidx], LONG_MAX);
    aidx = rand() % asz;
    avlg.fromValue(aidx, pflt);
    TS_ASSERT_EQUALS(avlg[aidx], LONG_MAX);
    aidx = rand() % asz;
    avlg.fromValue(aidx, pdbl);
    TS_ASSERT_EQUALS(avlg[aidx], LONG_MAX);

    asz = rand() % 150 + 1;
    aulg.resize(asz);
    TS_ASSERT_EQUALS(aulg.size(), sizeof(short) + asz * sizeof(long));
    aidx = rand() % asz;
    aulg.fromValue(aidx, -sll_v);
    TS_ASSERT_EQUALS(aulg[aidx], ULONG_MAX);
    aidx = rand() % asz;
    aulg.fromValue(aidx, ull_v);
    TS_ASSERT_EQUALS(aulg[aidx], ULONG_MAX);
    aidx = rand() % asz;
    aulg.fromValue(aidx, pstr);
    TS_ASSERT_EQUALS(aulg[aidx], ULONG_MAX);
    aidx = rand() % asz;
    aulg.fromValue(aidx, pflt);
    TS_ASSERT_EQUALS(aulg[aidx], ULONG_MAX);
    aidx = rand() % asz;
    aulg.fromValue(aidx, pdbl);
    TS_ASSERT_EQUALS(aulg[aidx], ULONG_MAX);

    asz = rand() % 150 + 1;
    avll.resize(asz);
    TS_ASSERT_EQUALS(avll.size(), sizeof(short) + asz * sizeof(long long));
    aidx = rand() % asz;
    avll.fromValue(aidx, ull_v);
    TS_ASSERT_EQUALS(avll[aidx], LLONG_MAX);
    aidx = rand() % asz;
    avll.fromValue(aidx, pstr);
    TS_ASSERT_EQUALS(avll[aidx], LLONG_MAX);
    aidx = rand() % asz;
    avll.fromValue(aidx, pflt);
    TS_ASSERT_EQUALS(avll[aidx], LLONG_MAX);
    aidx = rand() % asz;
    avll.fromValue(aidx, pdbl);
    TS_ASSERT_EQUALS(avll[aidx], LLONG_MAX);

    //  round down
    float flt1 = 122.3611f;
    double dbl1 = 148. / 3.;  // 49
    float flt2 = -36.286644f;
    double dbl2 = -302. / 7.; // -43

    asz = rand() % 150 + 1;
    avch.resize(asz);
    aidx = rand() % asz;
    avch.fromValue(aidx, flt1);
    TS_ASSERT_EQUALS(avch[aidx], 122);
    aidx = rand() % asz;
    avch.fromValue(aidx, dbl1);
    TS_ASSERT_EQUALS(avch[aidx], 49);
    aidx = rand() % asz;
    avch.fromValue(aidx, flt2);
    TS_ASSERT_EQUALS(avch[aidx], -36);
    aidx = rand() % asz;
    avch.fromValue(aidx, dbl2);
    TS_ASSERT_EQUALS(avch[aidx], -43);

    asz = rand() % 150 + 1;
    auch.resize(asz);
    aidx = rand() % asz;
    auch.fromValue(aidx, flt1);
    TS_ASSERT_EQUALS(auch[aidx], 122);
    aidx = rand() % asz;
    auch.fromValue(aidx, dbl1);
    TS_ASSERT_EQUALS(auch[aidx], 49);

    asz = rand() % 150 + 1;
    avsh.resize(asz);
    aidx = rand() % asz;
    avsh.fromValue(aidx, flt1);
    TS_ASSERT_EQUALS(avsh[aidx], 122);
    aidx = rand() % asz;
    avsh.fromValue(aidx, dbl1);
    TS_ASSERT_EQUALS(avsh[aidx], 49);
    aidx = rand() % asz;
    avsh.fromValue(aidx, flt2);
    TS_ASSERT_EQUALS(avsh[aidx], -36);
    aidx = rand() % asz;
    avsh.fromValue(aidx, dbl2);
    TS_ASSERT_EQUALS(avsh[aidx], -43);

    asz = rand() % 150 + 1;
    aush.resize(asz);
    aidx = rand() % asz;
    aush.fromValue(aidx, flt1);
    TS_ASSERT_EQUALS(aush[aidx], 122);
    aidx = rand() % asz;
    aush.fromValue(aidx, dbl1);
    TS_ASSERT_EQUALS(aush[aidx], 49);

    asz = rand() % 150 + 1;
    avit.resize(asz);
    aidx = rand() % asz;
    avit.fromValue(aidx, flt1);
    TS_ASSERT_EQUALS(avit[aidx], 122);
    aidx = rand() % asz;
    avit.fromValue(aidx, dbl1);
    TS_ASSERT_EQUALS(avit[aidx], 49);
    aidx = rand() % asz;
    avit.fromValue(aidx, flt2);
    TS_ASSERT_EQUALS(avit[aidx], -36);
    aidx = rand() % asz;
    avit.fromValue(aidx, dbl2);
    TS_ASSERT_EQUALS(avit[aidx], -43);

    asz = rand() % 150 + 1;
    auit.resize(asz);
    aidx = rand() % asz;
    auit.fromValue(aidx, flt1);
    TS_ASSERT_EQUALS(auit[aidx], 122);
    aidx = rand() % asz;
    auit.fromValue(aidx, dbl1);
    TS_ASSERT_EQUALS(auit[aidx], 49);

    asz = rand() % 150 + 1;
    avlg.resize(asz);
    aidx = rand() % asz;
    avlg.fromValue(aidx, flt1);
    TS_ASSERT_EQUALS(avlg[aidx], 122);
    aidx = rand() % asz;
    avlg.fromValue(aidx, dbl1);
    TS_ASSERT_EQUALS(avlg[aidx], 49);
    aidx = rand() % asz;
    avlg.fromValue(aidx, flt2);
    TS_ASSERT_EQUALS(avlg[aidx], -36);
    aidx = rand() % asz;
    avlg.fromValue(aidx, dbl2);
    TS_ASSERT_EQUALS(avlg[aidx], -43);

    asz = rand() % 150 + 1;
    aulg.resize(asz);
    aidx = rand() % asz;
    aulg.fromValue(aidx, flt1);
    TS_ASSERT_EQUALS(aulg[aidx], 122);
    aidx = rand() % asz;
    aulg.fromValue(aidx, dbl1);
    TS_ASSERT_EQUALS(aulg[aidx], 49);

    asz = rand() % 150 + 1;
    avll.resize(asz);
    aidx = rand() % asz;
    avll.fromValue(aidx, flt1);
    TS_ASSERT_EQUALS(avll[aidx], 122);
    aidx = rand() % asz;
    avll.fromValue(aidx, dbl1);
    TS_ASSERT_EQUALS(avll[aidx], 49);
    aidx = rand() % asz;
    avll.fromValue(aidx, flt2);
    TS_ASSERT_EQUALS(avll[aidx], -36);
    aidx = rand() % asz;
    avll.fromValue(aidx, dbl2);
    TS_ASSERT_EQUALS(avll[aidx], -43);

    asz = rand() % 150 + 1;
    aull.resize(asz);
    aidx = rand() % asz;
    aull.fromValue(aidx, flt1);
    TS_ASSERT_EQUALS(aull[aidx], 122);
    aidx = rand() % asz;
    aull.fromValue(aidx, dbl1);
    TS_ASSERT_EQUALS(aull[aidx], 49);

    //  round up
    float flt3 = 121.7123f;
    double dbl3 = 146. / 3.;  // 49
    float flt4 = -35.50133f;
    double dbl4 = -300. / 7.; // -43

    asz = rand() % 150 + 1;
    avch.resize(asz);
    aidx = rand() % asz;
    avch.fromValue(aidx, flt3);
    TS_ASSERT_EQUALS(avch[aidx], 122);
    aidx = rand() % asz;
    avch.fromValue(aidx, dbl3);
    TS_ASSERT_EQUALS(avch[aidx], 49);
    aidx = rand() % asz;
    avch.fromValue(aidx, flt4);
    TS_ASSERT_EQUALS(avch[aidx], -36);
    aidx = rand() % asz;
    avch.fromValue(aidx, dbl4);
    TS_ASSERT_EQUALS(avch[aidx], -43);

    asz = rand() % 150 + 1;
    auch.resize(asz);
    aidx = rand() % asz;
    auch.fromValue(aidx, flt3);
    TS_ASSERT_EQUALS(auch[aidx], 122);
    aidx = rand() % asz;
    auch.fromValue(aidx, dbl3);
    TS_ASSERT_EQUALS(auch[aidx], 49);

    asz = rand() % 150 + 1;
    avsh.resize(asz);
    aidx = rand() % asz;
    avsh.fromValue(aidx, flt3);
    TS_ASSERT_EQUALS(avsh[aidx], 122);
    aidx = rand() % asz;
    avsh.fromValue(aidx, dbl3);
    TS_ASSERT_EQUALS(avsh[aidx], 49);
    aidx = rand() % asz;
    avsh.fromValue(aidx, flt4);
    TS_ASSERT_EQUALS(avsh[aidx], -36);
    aidx = rand() % asz;
    avsh.fromValue(aidx, dbl4);
    TS_ASSERT_EQUALS(avsh[aidx], -43);

    asz = rand() % 150 + 1;
    aush.resize(asz);
    aidx = rand() % asz;
    aush.fromValue(aidx, flt3);
    TS_ASSERT_EQUALS(aush[aidx], 122);
    aidx = rand() % asz;
    aush.fromValue(aidx, dbl3);
    TS_ASSERT_EQUALS(aush[aidx], 49);

    asz = rand() % 150 + 1;
    avit.resize(asz);
    aidx = rand() % asz;
    avit.fromValue(aidx, flt3);
    TS_ASSERT_EQUALS(avit[aidx], 122);
    aidx = rand() % asz;
    avit.fromValue(aidx, dbl3);
    TS_ASSERT_EQUALS(avit[aidx], 49);
    aidx = rand() % asz;
    avit.fromValue(aidx, flt4);
    TS_ASSERT_EQUALS(avit[aidx], -36);
    aidx = rand() % asz;
    avit.fromValue(aidx, dbl4);
    TS_ASSERT_EQUALS(avit[aidx], -43);

    asz = rand() % 150 + 1;
    auit.resize(asz);
    aidx = rand() % asz;
    auit.fromValue(aidx, flt3);
    TS_ASSERT_EQUALS(auit[aidx], 122);
    aidx = rand() % asz;
    auit.fromValue(aidx, dbl3);
    TS_ASSERT_EQUALS(auit[aidx], 49);

    asz = rand() % 150 + 1;
    avlg.resize(asz);
    aidx = rand() % asz;
    avlg.fromValue(aidx, flt3);
    TS_ASSERT_EQUALS(avlg[aidx], 122);
    aidx = rand() % asz;
    avlg.fromValue(aidx, dbl3);
    TS_ASSERT_EQUALS(avlg[aidx], 49);
    aidx = rand() % asz;
    avlg.fromValue(aidx, flt4);
    TS_ASSERT_EQUALS(avlg[aidx], -36);
    aidx = rand() % asz;
    avlg.fromValue(aidx, dbl4);
    TS_ASSERT_EQUALS(avlg[aidx], -43);

    asz = rand() % 150 + 1;
    aulg.resize(asz);
    aidx = rand() % asz;
    aulg.fromValue(aidx, flt3);
    TS_ASSERT_EQUALS(aulg[aidx], 122);
    aidx = rand() % asz;
    aulg.fromValue(aidx, dbl3);
    TS_ASSERT_EQUALS(aulg[aidx], 49);

    asz = rand() % 150 + 1;
    avll.resize(asz);
    aidx = rand() % asz;
    avll.fromValue(aidx, flt3);
    TS_ASSERT_EQUALS(avll[aidx], 122);
    aidx = rand() % asz;
    avll.fromValue(aidx, dbl3);
    TS_ASSERT_EQUALS(avll[aidx], 49);
    aidx = rand() % asz;
    avll.fromValue(aidx, flt4);
    TS_ASSERT_EQUALS(avll[aidx], -36);
    aidx = rand() % asz;
    avll.fromValue(aidx, dbl4);
    TS_ASSERT_EQUALS(avll[aidx], -43);

    asz = rand() % 150 + 1;
    aull.resize(asz);
    aidx = rand() % asz;
    aull.fromValue(aidx, flt3);
    TS_ASSERT_EQUALS(aull[aidx], 122);
    aidx = rand() % asz;
    aull.fromValue(aidx, dbl3);
    TS_ASSERT_EQUALS(aull[aidx], 49);
}

template<class C>
const C *const_ptr(const r_array_var<C> &av)
{
    return (const C *)av;
}

void ArrayVarTestSuite::testDirty()
{
    r_array_var<char> avch;
    r_array_var<unsigned char> auch;
    r_array_var<short> avsh;
    r_array_var<unsigned short> aush;
    r_array_var<int> avit;
    r_array_var<unsigned int> auit;
    r_array_var<long> avlg;
    r_array_var<unsigned long> aulg;
    r_array_var<long long> avll;
    r_array_var<unsigned long long> aull;
    r_array_var<float> aflt;
    r_array_var<double> adbl;

    dirty_checker c_avch(&avch);
    dirty_checker c_auch(&auch);
    dirty_checker c_avsh(&avsh);
    dirty_checker c_aush(&auch);
    dirty_checker c_avit(&avit);
    dirty_checker c_auit(&auit);
    dirty_checker c_avlg(&avlg);
    dirty_checker c_aulg(&aulg);
    dirty_checker c_avll(&avll);
    dirty_checker c_aull(&aull);
    dirty_checker c_aflt(&aflt);
    dirty_checker c_adbl(&adbl);

    array_value_var *test[] = {
        &avch,
        &auch,
        &avsh,
        &aush,
        &avit,
        &auit,
        &avlg,
        &aulg,
        &avll,
        &aull,
        &aflt,
        &adbl
    };

    dirty_checker *chck[] = {
        &c_avch,
        &c_auch,
        &c_avsh,
        &c_aush,
        &c_avit,
        &c_auit,
        &c_avlg,
        &c_aulg,
        &c_avll,
        &c_aull,
        &c_aflt,
        &c_adbl
    };

    const int cnt = sizeof(chck) / sizeof(void *);

    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->resize(13);

    // resizing becomes dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 1);

    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->resize(13);

    // ..except when the size is the same, which does not trigger notDirty - not a value
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 0);

    char nval0 = rand() % CHAR_MAX;
    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(0, nval0);

    // a new value makes it dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 1);

    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(0, nval0);

    // but same value makes it not dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 2);

    unsigned char nval1 = rand() % CHAR_MAX;
    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(1, nval1);

    // a new value makes it dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 1);

    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(1, nval1);

    // but same value makes it not dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 2);

    short nval2 = rand() % CHAR_MAX;
    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(2, nval2);

    // a new value makes it dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 1);

    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(2, nval2);

    // but same value makes it not dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 2);

    unsigned short nval3 = rand() % CHAR_MAX;
    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(3, nval3);

    // a new value makes it dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 1);

    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(3, nval3);

    // but same value makes it not dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 2);

    int nval4 = rand() % CHAR_MAX;
    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(4, nval4);

    // a new value makes it dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 1);

    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(4, nval4);

    // but same value makes it not dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 2);

    unsigned int nval5 = rand() % CHAR_MAX;
    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(5, nval5);

    // a new value makes it dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 1);

    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(5, nval5);

    // but same value makes it not dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 2);

    long nval6 = rand() % CHAR_MAX;
    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(6, nval6);

    // a new value makes it dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 1);

    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(6, nval6);

    // but same value makes it not dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 2);

    unsigned long nval7 = rand() % CHAR_MAX;
    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(7, nval7);

    // a new value makes it dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 1);

    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(7, nval7);

    // but same value makes it not dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 2);

    long long nval8 = rand() % CHAR_MAX;
    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(8, nval8);

    // a new value makes it dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 1);

    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(8, nval8);

    // but same value makes it not dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 2);

    unsigned long long nval9 = rand() % CHAR_MAX;
    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(9, nval9);

    // a new value makes it dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 1);

    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(9, nval9);

    // but same value makes it not dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 2);

    float nval10 = rand() % CHAR_MAX;
    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(10, nval10);

    // a new value makes it dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 1);

    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(10, nval10);

    // but same value makes it not dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 2);

    double nval11 = rand() % CHAR_MAX;
    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(11, nval11);

    // a new value makes it dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 1);

    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(11, nval11);

    // but same value makes it not dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 2);

    char nval12[] = "00";
    nval12[0] += rand() % 10;
    nval12[1] += rand() % 10;
    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(12, nval12);

    // a new value makes it dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 1);

    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->fromValue(12, nval12);

    // but same value makes it not dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 2);

    // copy constructor
    r_array_var<char> avch_c(avch);
    r_array_var<unsigned char> auch_c(auch);
    r_array_var<short> avsh_c(avsh);
    r_array_var<unsigned short> aush_c(aush);
    r_array_var<int> avit_c(avit);
    r_array_var<unsigned int> auit_c(auit);
    r_array_var<long> avlg_c(avlg);
    r_array_var<unsigned long> aulg_c(aulg);
    r_array_var<long long> avll_c(avll);
    r_array_var<unsigned long long> aull_c(aull);
    r_array_var<float> aflt_c(aflt);
    r_array_var<double> adbl_c(adbl);

    // dirty is not affected
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 0);

    TS_ASSERT_SAME_DATA(const_ptr<char>(avch), const_ptr<char>(avch_c), avch.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<unsigned char>(auch), const_ptr<unsigned char>(auch_c), auch.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<short>(avsh), const_ptr<short>(avsh_c), avsh.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<unsigned short>(aush), const_ptr<unsigned short>(aush_c), aush.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<int>(avit), const_ptr<int>(avit_c), avit.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<unsigned int>(auit), const_ptr<unsigned int>(auit_c), auit.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<long>(avlg), const_ptr<long>(avlg_c), avlg.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<unsigned long>(aulg), const_ptr<unsigned long>(aulg_c), aulg.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<long long>(avll), const_ptr<long long>(avll_c), avll.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<unsigned long long>(aull), const_ptr<unsigned long long>(aull_c), aull.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<float>(aflt), const_ptr<float>(aflt_c), aflt.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<double>(adbl), const_ptr<double>(adbl_c), adbl.size() - sizeof(short));

    // dirty is not affected
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 0);

    // assignment
    r_array_var<char> avch_m;
    r_array_var<unsigned char> auch_m;
    r_array_var<short> avsh_m;
    r_array_var<unsigned short> aush_m;
    r_array_var<int> avit_m;
    r_array_var<unsigned int> auit_m;
    r_array_var<long> avlg_m;
    r_array_var<unsigned long> aulg_m;
    r_array_var<long long> avll_m;
    r_array_var<unsigned long long> aull_m;
    r_array_var<float> aflt_m;
    r_array_var<double> adbl_m;

    avch_m = avch;
    auch_m = auch;
    avsh_m = avsh;
    aush_m = aush;
    avit_m = avit;
    auit_m = auit;
    avlg_m = avlg;
    aulg_m = aulg;
    avll_m = avll;
    aull_m = aull;
    aflt_m = aflt;
    adbl_m = adbl;

    TS_ASSERT_SAME_DATA(const_ptr<char>(avch), const_ptr<char>(avch_m), avch.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<unsigned char>(auch), const_ptr<unsigned char>(auch_m), auch.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<short>(avsh), const_ptr<short>(avsh_m), avsh.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<unsigned short>(aush), const_ptr<unsigned short>(aush_m), aush.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<int>(avit), const_ptr<int>(avit_m), avit.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<unsigned int>(auit), const_ptr<unsigned int>(auit_m), auit.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<long>(avlg), const_ptr<long>(avlg_m), avlg.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<unsigned long>(aulg), const_ptr<unsigned long>(aulg_m), aulg.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<long long>(avll), const_ptr<long long>(avll_m), avll.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<unsigned long long>(aull), const_ptr<unsigned long long>(aull_m), aull.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<float>(aflt), const_ptr<float>(aflt_m), aflt.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<double>(adbl), const_ptr<double>(adbl_m), adbl.size() - sizeof(short));

    // dirty is not affected
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 0);


    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->resize(5);

    // shrink it to 5 elements makes it dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 1);

    // leaves the remaining values untouched
    TS_ASSERT_EQUALS(avch[0], nval0);
    TS_ASSERT_EQUALS(avch[1], nval1);
    TS_ASSERT_EQUALS(avch[2], nval2);
    TS_ASSERT_EQUALS(avch[3], nval3);
    TS_ASSERT_EQUALS(avch[4], nval4);
    TS_ASSERT_EQUALS(auch[0], nval0);
    TS_ASSERT_EQUALS(auch[1], nval1);
    TS_ASSERT_EQUALS(auch[2], nval2);
    TS_ASSERT_EQUALS(auch[3], nval3);
    TS_ASSERT_EQUALS(auch[4], nval4);
    TS_ASSERT_EQUALS(avsh[0], nval0);
    TS_ASSERT_EQUALS(avsh[1], nval1);
    TS_ASSERT_EQUALS(avsh[2], nval2);
    TS_ASSERT_EQUALS(avsh[3], nval3);
    TS_ASSERT_EQUALS(avsh[4], nval4);
    TS_ASSERT_EQUALS(aush[0], nval0);
    TS_ASSERT_EQUALS(aush[1], nval1);
    TS_ASSERT_EQUALS(aush[2], nval2);
    TS_ASSERT_EQUALS(aush[3], nval3);
    TS_ASSERT_EQUALS(aush[4], nval4);
    TS_ASSERT_EQUALS(avit[0], nval0);
    TS_ASSERT_EQUALS(avit[1], nval1);
    TS_ASSERT_EQUALS(avit[2], nval2);
    TS_ASSERT_EQUALS(avit[3], nval3);
    TS_ASSERT_EQUALS(avit[4], nval4);
    TS_ASSERT_EQUALS(auit[0], nval0);
    TS_ASSERT_EQUALS(auit[1], nval1);
    TS_ASSERT_EQUALS(auit[2], nval2);
    TS_ASSERT_EQUALS(auit[3], nval3);
    TS_ASSERT_EQUALS(auit[4], nval4);
    TS_ASSERT_EQUALS(avlg[0], nval0);
    TS_ASSERT_EQUALS(avlg[1], nval1);
    TS_ASSERT_EQUALS(avlg[2], nval2);
    TS_ASSERT_EQUALS(avlg[3], nval3);
    TS_ASSERT_EQUALS(avlg[4], nval4);
    TS_ASSERT_EQUALS(aulg[0], nval0);
    TS_ASSERT_EQUALS(aulg[1], nval1);
    TS_ASSERT_EQUALS(aulg[2], nval2);
    TS_ASSERT_EQUALS(aulg[3], nval3);
    TS_ASSERT_EQUALS(aulg[4], nval4);
    TS_ASSERT_EQUALS(avll[0], nval0);
    TS_ASSERT_EQUALS(avll[1], nval1);
    TS_ASSERT_EQUALS(avll[2], nval2);
    TS_ASSERT_EQUALS(avll[3], nval3);
    TS_ASSERT_EQUALS(avll[4], nval4);
    TS_ASSERT_EQUALS(aull[0], nval0);
    TS_ASSERT_EQUALS(aull[1], nval1);
    TS_ASSERT_EQUALS(aull[2], nval2);
    TS_ASSERT_EQUALS(aull[3], nval3);
    TS_ASSERT_EQUALS(aull[4], nval4);
    TS_ASSERT_EQUALS(aflt[0], nval0);
    TS_ASSERT_EQUALS(aflt[1], nval1);
    TS_ASSERT_EQUALS(aflt[2], nval2);
    TS_ASSERT_EQUALS(aflt[3], nval3);
    TS_ASSERT_EQUALS(aflt[4], nval4);
    TS_ASSERT_EQUALS(adbl[0], nval0);
    TS_ASSERT_EQUALS(adbl[1], nval1);
    TS_ASSERT_EQUALS(adbl[2], nval2);
    TS_ASSERT_EQUALS(adbl[3], nval3);
    TS_ASSERT_EQUALS(adbl[4], nval4);

    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->resize(30);

    // expanding it makes it dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 1);

    // leaves the old values untouched
    TS_ASSERT_EQUALS(avch[0], nval0);
    TS_ASSERT_EQUALS(avch[1], nval1);
    TS_ASSERT_EQUALS(avch[2], nval2);
    TS_ASSERT_EQUALS(avch[3], nval3);
    TS_ASSERT_EQUALS(avch[4], nval4);
    TS_ASSERT_EQUALS(auch[0], nval0);
    TS_ASSERT_EQUALS(auch[1], nval1);
    TS_ASSERT_EQUALS(auch[2], nval2);
    TS_ASSERT_EQUALS(auch[3], nval3);
    TS_ASSERT_EQUALS(auch[4], nval4);
    TS_ASSERT_EQUALS(avsh[0], nval0);
    TS_ASSERT_EQUALS(avsh[1], nval1);
    TS_ASSERT_EQUALS(avsh[2], nval2);
    TS_ASSERT_EQUALS(avsh[3], nval3);
    TS_ASSERT_EQUALS(avsh[4], nval4);
    TS_ASSERT_EQUALS(aush[0], nval0);
    TS_ASSERT_EQUALS(aush[1], nval1);
    TS_ASSERT_EQUALS(aush[2], nval2);
    TS_ASSERT_EQUALS(aush[3], nval3);
    TS_ASSERT_EQUALS(aush[4], nval4);
    TS_ASSERT_EQUALS(avit[0], nval0);
    TS_ASSERT_EQUALS(avit[1], nval1);
    TS_ASSERT_EQUALS(avit[2], nval2);
    TS_ASSERT_EQUALS(avit[3], nval3);
    TS_ASSERT_EQUALS(avit[4], nval4);
    TS_ASSERT_EQUALS(auit[0], nval0);
    TS_ASSERT_EQUALS(auit[1], nval1);
    TS_ASSERT_EQUALS(auit[2], nval2);
    TS_ASSERT_EQUALS(auit[3], nval3);
    TS_ASSERT_EQUALS(auit[4], nval4);
    TS_ASSERT_EQUALS(avlg[0], nval0);
    TS_ASSERT_EQUALS(avlg[1], nval1);
    TS_ASSERT_EQUALS(avlg[2], nval2);
    TS_ASSERT_EQUALS(avlg[3], nval3);
    TS_ASSERT_EQUALS(avlg[4], nval4);
    TS_ASSERT_EQUALS(aulg[0], nval0);
    TS_ASSERT_EQUALS(aulg[1], nval1);
    TS_ASSERT_EQUALS(aulg[2], nval2);
    TS_ASSERT_EQUALS(aulg[3], nval3);
    TS_ASSERT_EQUALS(aulg[4], nval4);
    TS_ASSERT_EQUALS(avll[0], nval0);
    TS_ASSERT_EQUALS(avll[1], nval1);
    TS_ASSERT_EQUALS(avll[2], nval2);
    TS_ASSERT_EQUALS(avll[3], nval3);
    TS_ASSERT_EQUALS(avll[4], nval4);
    TS_ASSERT_EQUALS(aull[0], nval0);
    TS_ASSERT_EQUALS(aull[1], nval1);
    TS_ASSERT_EQUALS(aull[2], nval2);
    TS_ASSERT_EQUALS(aull[3], nval3);
    TS_ASSERT_EQUALS(aull[4], nval4);
    TS_ASSERT_EQUALS(aflt[0], nval0);
    TS_ASSERT_EQUALS(aflt[1], nval1);
    TS_ASSERT_EQUALS(aflt[2], nval2);
    TS_ASSERT_EQUALS(aflt[3], nval3);
    TS_ASSERT_EQUALS(aflt[4], nval4);
    TS_ASSERT_EQUALS(adbl[0], nval0);
    TS_ASSERT_EQUALS(adbl[1], nval1);
    TS_ASSERT_EQUALS(adbl[2], nval2);
    TS_ASSERT_EQUALS(adbl[3], nval3);
    TS_ASSERT_EQUALS(adbl[4], nval4);

    // the other stuff is initialized to 0xff
    char allones[25 * 8];
    for (int idx = 0; idx < 25 * 8; ++idx)
        allones[idx] = '\xff';

    TS_ASSERT_SAME_DATA(const_ptr<char>(avch) + 5, allones, 25 * sizeof(char));
    TS_ASSERT_SAME_DATA(const_ptr<unsigned char>(auch) + 5, allones, 25 * sizeof(char));
    TS_ASSERT_SAME_DATA(const_ptr<short>(avsh) + 5, allones, 25 * sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<unsigned short>(aush) + 5, allones, 25 * sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<int>(avit) + 5, allones, 25 * sizeof(int));
    TS_ASSERT_SAME_DATA(const_ptr<unsigned int>(auit) + 5, allones, 25 * sizeof(int));
    TS_ASSERT_SAME_DATA(const_ptr<long>(avlg) + 5, allones, 25 * sizeof(long));
    TS_ASSERT_SAME_DATA(const_ptr<unsigned long>(aulg) + 5, allones, 25 * sizeof(long));
    TS_ASSERT_SAME_DATA(const_ptr<long long>(avll) + 5, allones, 25 * sizeof(long long));
    TS_ASSERT_SAME_DATA(const_ptr<unsigned long long>(aull) + 5, allones, 25 * sizeof(long long));
    TS_ASSERT_SAME_DATA(const_ptr<float>(aflt) + 5, allones, 25 * sizeof(float));
    TS_ASSERT_SAME_DATA(const_ptr<double>(adbl) + 5, allones, 25 * sizeof(double));

    // taking const is silent
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 0);

    TS_ASSERT_SAME_DATA((char *)avch + 5, allones, 25 * sizeof(char));
    TS_ASSERT_SAME_DATA((unsigned char *)auch + 5, allones, 25 * sizeof(char));
    TS_ASSERT_SAME_DATA((short *)avsh + 5, allones, 25 * sizeof(short));
    TS_ASSERT_SAME_DATA((unsigned short *)aush + 5, allones, 25 * sizeof(short));
    TS_ASSERT_SAME_DATA((int *)avit + 5, allones, 25 * sizeof(int));
    TS_ASSERT_SAME_DATA((unsigned int *)auit + 5, allones, 25 * sizeof(int));
    TS_ASSERT_SAME_DATA((long *)avlg + 5, allones, 25 * sizeof(long));
    TS_ASSERT_SAME_DATA((unsigned long *)aulg + 5, allones, 25 * sizeof(long));
    TS_ASSERT_SAME_DATA((long long *)avll + 5, allones, 25 * sizeof(long long));
    TS_ASSERT_SAME_DATA((unsigned long long *)aull + 5, allones, 25 * sizeof(long long));
    TS_ASSERT_SAME_DATA((float *)aflt + 5, allones, 25 * sizeof(float));
    TS_ASSERT_SAME_DATA((double *)adbl + 5, allones, 25 * sizeof(double));

    // taking non-const makes it dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 1);

    // nothing bad happens when we try to set an invalid index
    for (int idx = 0; idx < cnt; ++idx) {
        test[idx]->fromValue(60, nval0);
        test[idx]->fromValue(60, nval1);
        test[idx]->fromValue(60, nval2);
        test[idx]->fromValue(60, nval3);
        test[idx]->fromValue(60, nval4);
        test[idx]->fromValue(60, nval5);
        test[idx]->fromValue(60, nval6);
        test[idx]->fromValue(60, nval7);
        test[idx]->fromValue(60, nval8);
        test[idx]->fromValue(60, nval9);
        test[idx]->fromValue(60, nval10);
        test[idx]->fromValue(60, nval11);
        test[idx]->fromValue(60, nval12);
    }

    // should not trigger dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 0);

    // assignment using a base-class reference
    avch_m = *(test[0]);
    auch_m = *(test[1]);
    avsh_m = *(test[2]);
    aush_m = *(test[3]);
    avit_m = *(test[4]);
    auit_m = *(test[5]);
    avlg_m = *(test[6]);
    aulg_m = *(test[7]);
    avll_m = *(test[8]);
    aull_m = *(test[9]);
    aflt_m = *(test[10]);
    adbl_m = *(test[11]);

    TS_ASSERT_SAME_DATA(const_ptr<char>(avch), const_ptr<char>(avch_m), avch.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<unsigned char>(auch), const_ptr<unsigned char>(auch_m), auch.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<short>(avsh), const_ptr<short>(avsh_m), avsh.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<unsigned short>(aush), const_ptr<unsigned short>(aush_m), aush.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<int>(avit), const_ptr<int>(avit_m), avit.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<unsigned int>(auit), const_ptr<unsigned int>(auit_m), auit.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<long>(avlg), const_ptr<long>(avlg_m), avlg.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<unsigned long>(aulg), const_ptr<unsigned long>(aulg_m), aulg.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<long long>(avll), const_ptr<long long>(avll_m), avll.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<unsigned long long>(aull), const_ptr<unsigned long long>(aull_m), aull.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<float>(aflt), const_ptr<float>(aflt_m), aflt.size() - sizeof(short));
    TS_ASSERT_SAME_DATA(const_ptr<double>(adbl), const_ptr<double>(adbl_m), adbl.size() - sizeof(short));

    *(test[0]) = avch_m;
    *(test[1]) = auch_m;
    *(test[2]) = avsh_m;
    *(test[3]) = aush_m;
    *(test[4]) = avit_m;
    *(test[5]) = auit_m;
    *(test[6]) = avlg_m;
    *(test[7]) = aulg_m;
    *(test[8]) = avll_m;
    *(test[9]) = aull_m;
    *(test[10]) = aflt_m;
    *(test[11]) = adbl_m;

    // equating back is not dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 2);

    for (int idx = 0; idx < cnt; ++idx)
        test[idx]->setInvalid();

    // setting invalid makes it dirty
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 1);

    generic_var<double> dummy;
    for (int idx = 0; idx < cnt; ++idx)
        *(test[idx]) = dummy;

    // equating to something that cannot has no effect
    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT_EQUALS(chck[idx]->getStateAndReset(), 0);

    for (int idx = 0; idx < cnt; ++idx)
        TS_ASSERT(!(*(test[idx]) == dummy));

    // copy constructor on an invalid (ie. 0 element)
    r_array_var<char> avch_ci(avch);
    r_array_var<unsigned char> auch_ci(auch);
    r_array_var<short> avsh_ci(avsh);
    r_array_var<unsigned short> aush_ci(aush);
    r_array_var<int> avit_ci(avit);
    r_array_var<unsigned int> auit_ci(auit);
    r_array_var<long> avlg_ci(avlg);
    r_array_var<unsigned long> aulg_ci(aulg);
    r_array_var<long long> avll_ci(avll);
    r_array_var<unsigned long long> aull_ci(aull);
    r_array_var<float> aflt_ci(aflt);
    r_array_var<double> adbl_ci(adbl);

    TS_ASSERT(!avch_ci.isValid());
    TS_ASSERT(!auch_ci.isValid());
    TS_ASSERT(!avsh_ci.isValid());
    TS_ASSERT(!aush_ci.isValid());
    TS_ASSERT(!avit_ci.isValid());
    TS_ASSERT(!auit_ci.isValid());
    TS_ASSERT(!avlg_ci.isValid());
    TS_ASSERT(!aulg_ci.isValid());
    TS_ASSERT(!avll_ci.isValid());
    TS_ASSERT(!aull_ci.isValid());
    TS_ASSERT(!aflt_ci.isValid());
    TS_ASSERT(!adbl_ci.isValid());
}

void ArrayVarTestSuite::testMisc()
{
    r_array_var<char> avch[2];
    r_array_var<unsigned char> auch[2];
    r_array_var<short> avsh[2];
    r_array_var<unsigned short> aush[2];
    r_array_var<int> avit[2];
    r_array_var<unsigned int> auit[2];
    r_array_var<long> avlg[2];
    r_array_var<unsigned long> aulg[2];
    r_array_var<long long> avll[2];
    r_array_var<unsigned long long> aull[2];
    r_array_var<float> aflt[2];
    r_array_var<double> adbl[2];

    TS_ASSERT_EQUALS(avch[0].getNext(), &avch[1]);
    TS_ASSERT_EQUALS(auch[0].getNext(), &auch[1]);
    TS_ASSERT_EQUALS(avsh[0].getNext(), &avsh[1]);
    TS_ASSERT_EQUALS(aush[0].getNext(), &aush[1]);
    TS_ASSERT_EQUALS(avit[0].getNext(), &avit[1]);
    TS_ASSERT_EQUALS(auit[0].getNext(), &auit[1]);
    TS_ASSERT_EQUALS(avlg[0].getNext(), &avlg[1]);
    TS_ASSERT_EQUALS(aulg[0].getNext(), &aulg[1]);
    TS_ASSERT_EQUALS(avll[0].getNext(), &avll[1]);
    TS_ASSERT_EQUALS(aull[0].getNext(), &aull[1]);
    TS_ASSERT_EQUALS(aflt[0].getNext(), &aflt[1]);
    TS_ASSERT_EQUALS(adbl[0].getNext(), &adbl[1]);

    dirty_checker checker(&avch[0]);
    char cbuf = 0;
    avch[0].extract(1, (unsigned char *)cbuf);
    TS_ASSERT(!avch[0].isValid());
    TS_ASSERT_EQUALS(checker.getStateAndReset(), 2);

    r_ext_array_var<double, 0, 0, 0> avend;
    double dbv[] = {
        (double)rand() / (double)rand(),
        (double)rand() * (double)rand(),
        (double)rand() / (double)rand(),
        (double)rand() * (double)rand(),
        (double)rand() / (double)rand(),
        (double)rand() * (double)rand(),
        (double)rand() / (double)rand(),
        (double)rand() * (double)rand()
    };
    avend.extract(sizeof(dbv), reinterpret_cast<unsigned char *>(dbv));
    for (int idx = 0; idx < 8; ++idx)
        TS_ASSERT_EQUALS(avend[idx], rb(dbv[idx]));
}

void ArrayVarTestSuite::testExtract_1()
{
    r_ext_array_var<double,1,0,1> test;
    dirty_checker checker(&test);
    double seed;
    char count;
    char buf[5 * sizeof(double) + sizeof(char)];

    TS_ASSERT(!test.isValid());

    // 1. Test basic extract from a buffer
    count = 3;
    memcpy(buf, &count, sizeof(char));
    seed = rb(150.5);
    memcpy(buf + sizeof(char), &seed, sizeof(double));
    seed = rb(4056.9);
    memcpy(buf + sizeof(char) + sizeof(double), &seed, sizeof(double));
    seed = rb(12345.67);
    memcpy(buf + sizeof(char) + 2 * sizeof(double), &seed, sizeof(double));

    test.extract(3 * sizeof(double) + sizeof(char), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 3 * sizeof(double) + sizeof(char));
    TS_ASSERT(test.cardinal() == 3);
    TS_ASSERT(test[0] == 150.5);
    TS_ASSERT(test[1] == 4056.9);
    TS_ASSERT(test[2] == 12345.67);
    TS_ASSERT(checker.getStateAndReset() == 1);
    TS_ASSERT(test.isValid());

    // 2. Extract again - same cardinal
    seed = rb(-110.5);
    memcpy(buf + sizeof(char), &seed, sizeof(double));
    seed = rb(-8901.4);
    memcpy(buf + sizeof(char) + sizeof(double), &seed, sizeof(double));
    seed = rb(-98765.43);
    memcpy(buf + sizeof(char) + 2 * sizeof(double), &seed, sizeof(double));

    test.extract(3 * sizeof(double) + sizeof(char), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 3 * sizeof(double) + sizeof(char));
    TS_ASSERT(test.cardinal() == 3);
    TS_ASSERT(test[0] == -110.5);
    TS_ASSERT(test[1] == -8901.4);
    TS_ASSERT(test[2] == -98765.43);
    TS_ASSERT(checker.getStateAndReset() == 1);

    // 3. Extract cardinal is too large
    seed = rb(65.5);
    memcpy(buf + sizeof(char), &seed, sizeof(double));
    seed = rb(-128.4);
    memcpy(buf + sizeof(char) + sizeof(double), &seed, sizeof(double));

    test.extract(2 * sizeof(double) + sizeof(char), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 2 * sizeof(double) + sizeof(char));
    TS_ASSERT(test.cardinal() == 2);
    TS_ASSERT(test[0] == 65.5);
    TS_ASSERT(test[1] == -128.4);
    TS_ASSERT(checker.getStateAndReset() == 1);

    // 4. Extract nothing
    count = 0;

    test.extract(sizeof(char), reinterpret_cast<const unsigned char *>(&count));

    TS_ASSERT(test.size() == sizeof(char));
    TS_ASSERT(test.cardinal() == 0);
    TS_ASSERT(test[0] == 0.0);
    TS_ASSERT(test[500] == 0.0);
    TS_ASSERT(checker.getStateAndReset() == 1);
    TS_ASSERT(!test.isValid());

    // 5. Extract nothing because there is no data left
    count = 15;

    test.extract(sizeof(char), reinterpret_cast<const unsigned char *>(&count));

    TS_ASSERT(test.size() == sizeof(char));
    TS_ASSERT(test.cardinal() == 0);
    TS_ASSERT(test[0] == 0.0);
    TS_ASSERT(test[500] == 0.0);
    TS_ASSERT(checker.getStateAndReset() == 2);

    // 6. Extract something else
    count = 5;
    memcpy(buf, &count, sizeof(char));
    seed = rb(7150.5);
    memcpy(buf + sizeof(char), &seed, sizeof(double));
    seed = rb(74056.9);
    memcpy(buf + sizeof(char) + sizeof(double), &seed, sizeof(double));
    seed = rb(712345.67);
    memcpy(buf + sizeof(char) + 2 * sizeof(double), &seed, sizeof(double));
    seed = rb(88235.1);
    memcpy(buf + sizeof(char) + 3 * sizeof(double), &seed, sizeof(double));
    seed = rb(-345.67);
    memcpy(buf + sizeof(char) + 4 * sizeof(double), &seed, sizeof(double));

    test.extract(5 * sizeof(double) + sizeof(char), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 5 * sizeof(double) + sizeof(char));
    TS_ASSERT(test.cardinal() == 5);
    TS_ASSERT(test[0] == 7150.5);
    TS_ASSERT(test[1] == 74056.9);
    TS_ASSERT(test[2] == 712345.67);
    TS_ASSERT(test[3] == 88235.1);
    TS_ASSERT(test[4] == -345.67);
    TS_ASSERT(checker.getStateAndReset() == 1);

    // 7. Exactly the same data
    test.extract(5 * sizeof(double) + sizeof(char), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 5 * sizeof(double) + sizeof(char));
    TS_ASSERT(test.cardinal() == 5);
    TS_ASSERT(test[0] == 7150.5);
    TS_ASSERT(test[1] == 74056.9);
    TS_ASSERT(test[2] == 712345.67);
    TS_ASSERT(test[3] == 88235.1);
    TS_ASSERT(test[4] == -345.67);
    TS_ASSERT(checker.getStateAndReset() == 2);

    // 8. Exactly the same data but truncated
    test.extract(2 * sizeof(double) + sizeof(char), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 2 * sizeof(double) + sizeof(char));
    TS_ASSERT(test.cardinal() == 2);
    TS_ASSERT(test[0] == 7150.5);
    TS_ASSERT(test[1] == 74056.9);
    TS_ASSERT(test[2] == 0.0);
    TS_ASSERT(test[3] == 0.0);
    TS_ASSERT(test[4] == 0.0);
    TS_ASSERT(checker.getStateAndReset() == 1);

    // 9. Same data with correct cardinal this time
    count = 2;
    memcpy(buf, &count, sizeof(char));
    test.extract(2 * sizeof(double) + sizeof(char), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 2 * sizeof(double) + sizeof(char));
    TS_ASSERT(test.cardinal() == 2);
    TS_ASSERT(test[0] == 7150.5);
    TS_ASSERT(test[1] == 74056.9);
    TS_ASSERT(checker.getStateAndReset() == 2);

    test.setInvalid();
    TS_ASSERT(test.size() == sizeof(char));
    TS_ASSERT(test.cardinal() == 0);
    TS_ASSERT(checker.getStateAndReset() == 1);

    test.setInvalid();
    TS_ASSERT(checker.getStateAndReset() == 2);
}

void ArrayVarTestSuite::testExtract_4()
{
    r_ext_array_var<double, -4, 0, 4> test;
    dirty_checker checker(&test);
    double seed;
    int count;
    char buf[5 * sizeof(double) + sizeof(int)];

    TS_ASSERT(!test.isValid());

    // 1. Test basic extract from a buffer
    count = rb(3);
    memcpy(buf, &count, sizeof(int));
    seed = rb(150.5);
    memcpy(buf + sizeof(int), &seed, sizeof(double));
    seed = rb(4056.9);
    memcpy(buf + sizeof(int) + sizeof(double), &seed, sizeof(double));
    seed = rb(12345.67);
    memcpy(buf + sizeof(int) + 2 * sizeof(double), &seed, sizeof(double));

    test.extract(3 * sizeof(double) + sizeof(int), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 3 * sizeof(double) + sizeof(int));
    TS_ASSERT(test.cardinal() == 3);
    TS_ASSERT(test[0] == 150.5);
    TS_ASSERT(test[1] == 4056.9);
    TS_ASSERT(test[2] == 12345.67);
    TS_ASSERT(checker.getStateAndReset() == 1);
    TS_ASSERT(test.isValid());

    // 2. Extract again - same cardinal
    seed = rb(-110.5);
    memcpy(buf + sizeof(int), &seed, sizeof(double));
    seed = rb(-8901.4);
    memcpy(buf + sizeof(int) + sizeof(double), &seed, sizeof(double));
    seed = rb(-98765.43);
    memcpy(buf + sizeof(int) + 2 * sizeof(double), &seed, sizeof(double));

    test.extract(3 * sizeof(double) + sizeof(int), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 3 * sizeof(double) + sizeof(int));
    TS_ASSERT(test.cardinal() == 3);
    TS_ASSERT(test[0] == -110.5);
    TS_ASSERT(test[1] == -8901.4);
    TS_ASSERT(test[2] == -98765.43);
    TS_ASSERT(checker.getStateAndReset() == 1);

    // 3. Extract cardinal is too large
    seed = rb(65.5);
    memcpy(buf + sizeof(int), &seed, sizeof(double));
    seed = rb(-128.4);
    memcpy(buf + sizeof(int) + sizeof(double), &seed, sizeof(double));

    test.extract(2 * sizeof(double) + sizeof(int), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 2 * sizeof(double) + sizeof(int));
    TS_ASSERT(test.cardinal() == 2);
    TS_ASSERT(test[0] == 65.5);
    TS_ASSERT(test[1] == -128.4);
    TS_ASSERT(checker.getStateAndReset() == 1);

    // 4. Extract nothing
    count = 0;

    test.extract(sizeof(int), reinterpret_cast<const unsigned char *>(&count));

    TS_ASSERT(test.size() == sizeof(int));
    TS_ASSERT(test.cardinal() == 0);
    TS_ASSERT(test[0] == 0.0);
    TS_ASSERT(test[500] == 0.0);
    TS_ASSERT(checker.getStateAndReset() == 1);
    TS_ASSERT(!test.isValid());

    // 5. Extract nothing because there is no data left
    count = rb(15);

    test.extract(sizeof(int), reinterpret_cast<const unsigned char *>(&count));

    TS_ASSERT(test.size() == sizeof(int));
    TS_ASSERT(test.cardinal() == 0);
    TS_ASSERT(test[0] == 0.0);
    TS_ASSERT(test[500] == 0.0);
    TS_ASSERT(checker.getStateAndReset() == 2);

    // 6. Extract something else
    count = rb(5);
    memcpy(buf, &count, sizeof(int));
    seed = rb(7150.5);
    memcpy(buf + sizeof(int), &seed, sizeof(double));
    seed = rb(74056.9);
    memcpy(buf + sizeof(int) + sizeof(double), &seed, sizeof(double));
    seed = rb(712345.67);
    memcpy(buf + sizeof(int) + 2 * sizeof(double), &seed, sizeof(double));
    seed = rb(88235.1);
    memcpy(buf + sizeof(int) + 3 * sizeof(double), &seed, sizeof(double));
    seed = rb(-345.67);
    memcpy(buf + sizeof(int) + 4 * sizeof(double), &seed, sizeof(double));

    test.extract(5 * sizeof(double) + sizeof(int), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 5 * sizeof(double) + sizeof(int));
    TS_ASSERT(test.cardinal() == 5);
    TS_ASSERT(test[0] == 7150.5);
    TS_ASSERT(test[1] == 74056.9);
    TS_ASSERT(test[2] == 712345.67);
    TS_ASSERT(test[3] == 88235.1);
    TS_ASSERT(test[4] == -345.67);
    TS_ASSERT(checker.getStateAndReset() == 1);

    // 7. Exactly the same data
    test.extract(5 * sizeof(double) + sizeof(int), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 5 * sizeof(double) + sizeof(int));
    TS_ASSERT(test.cardinal() == 5);
    TS_ASSERT(test[0] == 7150.5);
    TS_ASSERT(test[1] == 74056.9);
    TS_ASSERT(test[2] == 712345.67);
    TS_ASSERT(test[3] == 88235.1);
    TS_ASSERT(test[4] == -345.67);
    TS_ASSERT(checker.getStateAndReset() == 2);

    // 8. Exactly the same data but truncated
    test.extract(2 * sizeof(double) + sizeof(int), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 2 * sizeof(double) + sizeof(int));
    TS_ASSERT(test.cardinal() == 2);
    TS_ASSERT(test[0] == 7150.5);
    TS_ASSERT(test[1] == 74056.9);
    TS_ASSERT(test[2] == 0.0);
    TS_ASSERT(test[3] == 0.0);
    TS_ASSERT(test[4] == 0.0);
    TS_ASSERT(checker.getStateAndReset() == 1);

    // 9. Same data with correct cardinal this time
    count = rb(2);
    memcpy(buf, &count, sizeof(char));
    test.extract(2 * sizeof(double) + sizeof(int), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 2 * sizeof(double) + sizeof(int));
    TS_ASSERT(test.cardinal() == 2);
    TS_ASSERT(test[0] == 7150.5);
    TS_ASSERT(test[1] == 74056.9);
    TS_ASSERT(checker.getStateAndReset() == 2);

    test.setInvalid();
    TS_ASSERT(test.size() == sizeof(int));
    TS_ASSERT(test.cardinal() == 0);
    TS_ASSERT(checker.getStateAndReset() == 1);

    test.setInvalid();
    TS_ASSERT(checker.getStateAndReset() == 2);
}

void ArrayVarTestSuite::testExtract_8()
{
    r_ext_array_var<double, -8, 0, 8> test;
    dirty_checker checker(&test);
    double seed;
    long long count;
    char buf[5 * sizeof(double) + sizeof(long long)];

    TS_ASSERT(!test.isValid());

    // 1. Test basic extract from a buffer
    count = rb(3LL);
    memcpy(buf, &count, sizeof(long long));
    seed = rb(150.5);
    memcpy(buf + sizeof(long long), &seed, sizeof(double));
    seed = rb(4056.9);
    memcpy(buf + sizeof(long long) + sizeof(double), &seed, sizeof(double));
    seed = rb(12345.67);
    memcpy(buf + sizeof(long long) + 2 * sizeof(double), &seed, sizeof(double));

    test.extract(3 * sizeof(double) + sizeof(long long), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 3 * sizeof(double) + sizeof(long long));
    TS_ASSERT(test.cardinal() == 3);
    TS_ASSERT(test[0] == 150.5);
    TS_ASSERT(test[1] == 4056.9);
    TS_ASSERT(test[2] == 12345.67);
    TS_ASSERT(checker.getStateAndReset() == 1);
    TS_ASSERT(test.isValid());

    // 2. Extract again - same cardinal
    seed = rb(-110.5);
    memcpy(buf + sizeof(long long), &seed, sizeof(double));
    seed = rb(-8901.4);
    memcpy(buf + sizeof(long long) + sizeof(double), &seed, sizeof(double));
    seed = rb(-98765.43);
    memcpy(buf + sizeof(long long) + 2 * sizeof(double), &seed, sizeof(double));

    test.extract(3 * sizeof(double) + sizeof(long long), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 3 * sizeof(double) + sizeof(long long));
    TS_ASSERT(test.cardinal() == 3);
    TS_ASSERT(test[0] == -110.5);
    TS_ASSERT(test[1] == -8901.4);
    TS_ASSERT(test[2] == -98765.43);
    TS_ASSERT(checker.getStateAndReset() == 1);

    // 3. Extract cardinal is too large
    seed = rb(65.5);
    memcpy(buf + sizeof(long long), &seed, sizeof(double));
    seed = rb(-128.4);
    memcpy(buf + sizeof(long long) + sizeof(double), &seed, sizeof(double));

    test.extract(2 * sizeof(double) + sizeof(long long), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 2 * sizeof(double) + sizeof(long long));
    TS_ASSERT(test.cardinal() == 2);
    TS_ASSERT(test[0] == 65.5);
    TS_ASSERT(test[1] == -128.4);
    TS_ASSERT(checker.getStateAndReset() == 1);

    // 4. Extract nothing
    count = 0;

    test.extract(sizeof(long long), reinterpret_cast<const unsigned char *>(&count));

    TS_ASSERT(test.size() == sizeof(long long));
    TS_ASSERT(test.cardinal() == 0);
    TS_ASSERT(test[0] == 0.0);
    TS_ASSERT(test[500] == 0.0);
    TS_ASSERT(checker.getStateAndReset() == 1);
    TS_ASSERT(!test.isValid());

    // 5. Extract nothing because there is no data left
    count = rb(15LL);

    test.extract(sizeof(long long), reinterpret_cast<const unsigned char *>(&count));

    TS_ASSERT(test.size() == sizeof(long long));
    TS_ASSERT(test.cardinal() == 0);
    TS_ASSERT(test[0] == 0.0);
    TS_ASSERT(test[500] == 0.0);
    TS_ASSERT(checker.getStateAndReset() == 2);

    // 6. Extract something else
    count = rb(5LL);
    memcpy(buf, &count, sizeof(long long));
    seed = rb(7150.5);
    memcpy(buf + sizeof(long long), &seed, sizeof(double));
    seed = rb(74056.9);
    memcpy(buf + sizeof(long long) + sizeof(double), &seed, sizeof(double));
    seed = rb(712345.67);
    memcpy(buf + sizeof(long long) + 2 * sizeof(double), &seed, sizeof(double));
    seed = rb(88235.1);
    memcpy(buf + sizeof(long long) + 3 * sizeof(double), &seed, sizeof(double));
    seed = rb(-345.67);
    memcpy(buf + sizeof(long long) + 4 * sizeof(double), &seed, sizeof(double));

    test.extract(5 * sizeof(double) + sizeof(long long), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 5 * sizeof(double) + sizeof(long long));
    TS_ASSERT(test.cardinal() == 5);
    TS_ASSERT(test[0] == 7150.5);
    TS_ASSERT(test[1] == 74056.9);
    TS_ASSERT(test[2] == 712345.67);
    TS_ASSERT(test[3] == 88235.1);
    TS_ASSERT(test[4] == -345.67);
    TS_ASSERT(checker.getStateAndReset() == 1);

    // 7. Exactly the same data
    test.extract(5 * sizeof(double) + sizeof(long long), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 5 * sizeof(double) + sizeof(long long));
    TS_ASSERT(test.cardinal() == 5);
    TS_ASSERT(test[0] == 7150.5);
    TS_ASSERT(test[1] == 74056.9);
    TS_ASSERT(test[2] == 712345.67);
    TS_ASSERT(test[3] == 88235.1);
    TS_ASSERT(test[4] == -345.67);
    TS_ASSERT(checker.getStateAndReset() == 2);

    // 8. Exactly the same data but truncated
    test.extract(2 * sizeof(double) + sizeof(long long), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 2 * sizeof(double) + sizeof(long long));
    TS_ASSERT(test.cardinal() == 2);
    TS_ASSERT(test[0] == 7150.5);
    TS_ASSERT(test[1] == 74056.9);
    TS_ASSERT(test[2] == 0.0);
    TS_ASSERT(test[3] == 0.0);
    TS_ASSERT(test[4] == 0.0);
    TS_ASSERT(checker.getStateAndReset() == 1);

    // 9. Same data with correct cardinal this time
    count = rb(2LL);
    memcpy(buf, &count, sizeof(char));
    test.extract(2 * sizeof(double) + sizeof(long long), reinterpret_cast<const unsigned char *>(buf));

    TS_ASSERT(test.size() == 2 * sizeof(double) + sizeof(long long));
    TS_ASSERT(test.cardinal() == 2);
    TS_ASSERT(test[0] == 7150.5);
    TS_ASSERT(test[1] == 74056.9);
    TS_ASSERT(checker.getStateAndReset() == 2);

    test.setInvalid();
    TS_ASSERT(test.size() == sizeof(long long));
    TS_ASSERT(test.cardinal() == 0);
    TS_ASSERT(checker.getStateAndReset() == 1);

    test.setInvalid();
    TS_ASSERT(checker.getStateAndReset() == 2);
}

void ArrayVarTestSuite::testOutput_1()
{
    r_ext_array_var<int,1,0,1> test;
    r_ext_array_var<int,1,0,1> test2;
    dirty_checker checker(&test);
    char buf[4 * sizeof(int) + sizeof(char)];

    test.resize(4);
    test.fromValue(0, 60);
    test.fromValue(1, 70);
    test.fromValue(2, 80);
    test.fromValue(3, 90);
    test.fromValue(4, 100); // nothing happens

    // check assignment
    TS_ASSERT(test.size() == 4 * sizeof(int) + sizeof(char));
    TS_ASSERT(test.cardinal() == 4);
    TS_ASSERT(test[0] == 60);
    TS_ASSERT(test[1] == 70);
    TS_ASSERT(test[2] == 80);
    TS_ASSERT(test[3] == 90);
    TS_ASSERT(test[4] == 0);
    TS_ASSERT(checker.getStateAndReset() == 1);

    // check output
    char counter = 4;
    int data[4] = { rb(60), rb(70), rb(80), rb(90) };
    outbuf ob;
    ob.set(reinterpret_cast<unsigned char *>(buf), 4 * sizeof(int) + sizeof(char));
    test.output(ob);

    TS_ASSERT(memcmp(buf, &counter, sizeof(char)) == 0);
    TS_ASSERT(memcmp(buf + sizeof(char), data, 4 * sizeof(int)) == 0);

    // check assignment of entire array
    TS_ASSERT(test != test2);
    test2 = test;
    ob.set(reinterpret_cast<unsigned char *>(buf), 4 * sizeof(int) + sizeof(char));
    test2.output(ob);
    TS_ASSERT(memcmp(buf, &counter, sizeof(char)) == 0);
    TS_ASSERT(memcmp(buf + sizeof(char), data, 4 * sizeof(int)) == 0);
    TS_ASSERT(test == test2);
}

void ArrayVarTestSuite::testOutput_4()
{
    r_ext_array_var<int,-4,0,4> test;
    r_ext_array_var<int,-4,0,4> test2;
    dirty_checker checker(&test);
    char buf[4 * sizeof(int) + sizeof(int)];

    test.resize(4);
    test.fromValue(0, 60);
    test.fromValue(1, 70);
    test.fromValue(2, 80);
    test.fromValue(3, 90);
    test.fromValue(4, 100); // nothing happens

    // check assignment
    TS_ASSERT(test.size() == 4 * sizeof(int) + sizeof(int));
    TS_ASSERT(test.cardinal() == 4);
    TS_ASSERT(test[0] == 60);
    TS_ASSERT(test[1] == 70);
    TS_ASSERT(test[2] == 80);
    TS_ASSERT(test[3] == 90);
    TS_ASSERT(test[4] == 0);
    TS_ASSERT(checker.getStateAndReset() == 1);

    // check output
    int counter = rb(4);
    int data[4] = { rb(60), rb(70), rb(80), rb(90) };
    outbuf ob;
    ob.set(reinterpret_cast<unsigned char *>(buf), 4 * sizeof(int) + sizeof(int));
    test.output(ob);

    TS_ASSERT_SAME_DATA(buf, &counter, sizeof(int));
    TS_ASSERT_SAME_DATA(buf + sizeof(int), data, 4 * sizeof(int));

    // check assignment of entire array
    TS_ASSERT(test != test2);
    test2 = test;
    ob.set(reinterpret_cast<unsigned char *>(buf), 4 * sizeof(int) + sizeof(int));
    test2.output(ob);
    TS_ASSERT_SAME_DATA(buf, &counter, sizeof(int));
    TS_ASSERT_SAME_DATA(buf + sizeof(int), data, 4 * sizeof(int));
    TS_ASSERT(test == test2);
}

void ArrayVarTestSuite::testOutput_8()
{
    r_ext_array_var<int,-8,0,8> test;
    r_ext_array_var<int,-8,0,8> test2;
    dirty_checker checker(&test);
    char buf[4 * sizeof(int) + sizeof(long long)];

    test.resize(4);
    test.fromValue(0, 60);
    test.fromValue(1, 70);
    test.fromValue(2, 80);
    test.fromValue(3, 90);
    test.fromValue(4, 100); // nothing happens

    // check assignment
    TS_ASSERT(test.size() == 4 * sizeof(int) + sizeof(long long));
    TS_ASSERT(test.cardinal() == 4);
    TS_ASSERT(test[0] == 60);
    TS_ASSERT(test[1] == 70);
    TS_ASSERT(test[2] == 80);
    TS_ASSERT(test[3] == 90);
    TS_ASSERT(test[4] == 0);
    TS_ASSERT(checker.getStateAndReset() == 1);

    // check output
    long long counter = rb(4LL);
    int data[4] = { rb(60), rb(70), rb(80), rb(90) };
    outbuf ob;
    ob.set(reinterpret_cast<unsigned char *>(buf), 4 * sizeof(int) + sizeof(long long));
    test.output(ob);

    TS_ASSERT_SAME_DATA(buf, &counter, sizeof(long long));
    TS_ASSERT_SAME_DATA(buf + sizeof(long long), data, 4 * sizeof(int));

    // check assignment of entire array
    TS_ASSERT(test != test2);
    test2 = test;
    ob.set(reinterpret_cast<unsigned char *>(buf), 4 * sizeof(int) + sizeof(long long));
    test2.output(ob);
    TS_ASSERT_SAME_DATA(buf, &counter, sizeof(long long));
    TS_ASSERT_SAME_DATA(buf + sizeof(long long), data, 4 * sizeof(int));
    TS_ASSERT(test == test2);
}

void ArrayVarTestSuite::testArrayBinaryAssignment()
{
    r_array_var<char> avc1;
    r_array_var<int>  avi1;

    union
    {
        char ch[32];
        int it[8]; /* 4 * 8 = 32 */
    } u, u2;

	for (int idx = 0; idx < 8; ++idx) {
		u.it[idx] = rand() * rand();
		u2.it[idx] = rb(u.it[idx]);
	}

    short count = rb<short>(8);
    char buf[40];
    memcpy(buf, &count, sizeof(count));
    memcpy(buf + sizeof(count), &u2, 32);
    avi1.extract(sizeof(count) + 32, (unsigned char *)buf);

    for (int idx = 0; idx < 8; ++idx)
        TS_ASSERT_EQUALS(avi1[idx], u.it[idx]);

    avc1 = avi1; // binary assignment, the array is the same

    for (int idx = 0; idx < 32; ++idx)
        TS_ASSERT_EQUALS(avc1[idx], u.ch[idx]);

    memset(buf, 0, 40);
    outbuf ob;
    ob.set((unsigned char *)buf, 40);
    avc1.output(ob);

    short ocnt = rb<short>(32);
    TS_ASSERT_SAME_DATA(&ocnt, buf, sizeof(ocnt));
    TS_ASSERT_SAME_DATA(&u, buf + sizeof(ocnt), 32);
}

void ArrayVarTestSuite::testArrayBinaryAssignment_1()
{
    r_ext_array_var<char,1,0,1> avc1;
    r_ext_array_var<int, 1,0,1> avi1;

    union
    {
        char ch[32];
        int it[8]; /* 4 * 8 = 32 */
    } u, u2;

	for (int idx = 0; idx < 8; ++idx) {
		u.it[idx] = rand() * rand();
		u2.it[idx] = rb(u.it[idx]);
	}

    char count = 8;
    char buf[40];
    memcpy(buf, &count, sizeof(count));
    memcpy(buf + sizeof(count), &u2, 32);
    avi1.extract(sizeof(count) + 32, (unsigned char *)buf);

    for (int idx = 0; idx < 8; ++idx)
        TS_ASSERT_EQUALS(avi1[idx], u.it[idx]);

    avc1 = avi1; // binary assignment, the array is the same

    for (int idx = 0; idx < 32; ++idx)
        TS_ASSERT_EQUALS(avc1[idx], u.ch[idx]);

    memset(buf, 0, 40);
    outbuf ob;
    ob.set((unsigned char *)buf, 40);
    avc1.output(ob);

    char ocnt = 32;
    TS_ASSERT_SAME_DATA(&ocnt, buf, sizeof(ocnt));
    TS_ASSERT_SAME_DATA(&u, buf + sizeof(ocnt), 32);
}

void ArrayVarTestSuite::testArrayBinaryAssignment_4()
{
    r_ext_array_var<char, -4, 0, 4> avc1;
    r_ext_array_var<int, -4, 0, 4> avi1;

    union
    {
        char ch[32];
        int it[8]; /* 4 * 8 = 32 */
    } u, u2;

	for (int idx = 0; idx < 8; ++idx) {
		u.it[idx] = rand() * rand();
		u2.it[idx] = rb(u.it[idx]);
	}

    int count = rb(8);
    char buf[40];
    memcpy(buf, &count, sizeof(count));
    memcpy(buf + sizeof(count), &u2, 32);
    avi1.extract(sizeof(count) + 32, (unsigned char *)buf);

    for (int idx = 0; idx < 8; ++idx)
        TS_ASSERT_EQUALS(avi1[idx], u.it[idx]);

    avc1 = avi1; // binary assignment, the array is the same

    for (int idx = 0; idx < 32; ++idx)
        TS_ASSERT_EQUALS(avc1[idx], u.ch[idx]);

    memset(buf, 0, 40);
    outbuf ob;
    ob.set((unsigned char *)buf, 40);
    avc1.output(ob);

    int ocnt = rb(32);
    TS_ASSERT_SAME_DATA(&ocnt, buf, sizeof(ocnt));
    TS_ASSERT_SAME_DATA(&u, buf + sizeof(ocnt), 32);
}

void ArrayVarTestSuite::testArrayBinaryAssignment_8()
{
    r_ext_array_var<char, -8, 0, 8> avc1;
    r_ext_array_var<int, -8, 0, 8> avi1;

    union
    {
        char ch[32];
        int it[8]; /* 4 * 8 = 32 */
    } u, u2;

	for (int idx = 0; idx < 8; ++idx) {
		u.it[idx] = rand() * rand();
		u2.it[idx] = rb(u.it[idx]);
	}

    long long count = rb(8LL);
    char buf[40];
    memcpy(buf, &count, sizeof(count));
    memcpy(buf + sizeof(count), &u2, 32);
    avi1.extract(sizeof(count) + 32, (unsigned char *)buf);

    for (int idx = 0; idx < 8; ++idx)
        TS_ASSERT_EQUALS(avi1[idx], u.it[idx]);

    avc1 = avi1; // binary assignment, the array is the same

    for (int idx = 0; idx < 32; ++idx)
        TS_ASSERT_EQUALS(avc1[idx], u.ch[idx]);

    memset(buf, 0, 40);
    outbuf ob;
    ob.set((unsigned char *)buf, 40);
    avc1.output(ob);

    long long ocnt = rb(32LL);
    TS_ASSERT_SAME_DATA(&ocnt, buf, sizeof(ocnt));
    TS_ASSERT_SAME_DATA(&u, buf + sizeof(ocnt), 32);
}

template<class C>
void format_cardinal(C car, char *ptr, int sz, int off)
{
	C rcar = rb(car);
    memset(ptr, 0, sz);
    memcpy(ptr + off, &rcar, sizeof(C));
}

void ArrayVarTestSuite::testPadding()
{
    r_ext_array_var<char, 1, 5, 8> avc1;
    r_ext_array_var<int, 1, 3, 8> avi1;
    r_ext_array_var<char, 1, 2, 3> avc2;
    r_ext_array_var<int, 1, 1, 3> avi2;
    r_ext_array_var<char, -2, 6, 8> avc3;
    r_ext_array_var<int, -2, 2, 8> avi3;
    r_ext_array_var<char, -2, 1, 3> avc4;
    r_ext_array_var<int, -2, 1, 3> avi4;
    r_ext_array_var<char, -4, 4, 8> avc5;
    r_ext_array_var<int, -4, 1, 5> avi5;
    r_ext_array_var<char, -8, 4, 14> avc6;
    r_ext_array_var<int, -8, 1, 9> avi6;

    /* the data part */
    union
    {
        char ch[32];
        int it[8]; /* 4 * 8 = 32 */
    } u;

    for (int idx = 0; idx < 8; ++idx)
        u.it[0] = rand() * rand();

    /* the cardinal part */
    char avc1_c[8];
    char avi1_c[8];
    char avc2_c[3];
    char avi2_c[3];
    char avc3_c[8];
    char avi3_c[8];
    char avc4_c[3];
    char avi4_c[3];
    char avc5_c[8];
    char avi5_c[5];
    char avc6_c[14];
    char avi6_c[9];

    char avc_1 = 32;
    char avi_1 = 8;
    short avc_2 = 32;
    short avi_2 = 8;
    int avc_4 = 32;
    int avi_4 = 8;
    long long avc_8 = 32;
    long long avi_8 = 8;

    format_cardinal(avc_1, avc1_c, sizeof(avc1_c), 5);
    format_cardinal(avi_1, avi1_c, sizeof(avi1_c), 3);
    format_cardinal(avc_1, avc2_c, sizeof(avc2_c), 2);
    format_cardinal(avi_1, avi2_c, sizeof(avi2_c), 1);
    format_cardinal(avc_2, avc3_c, sizeof(avc3_c), 6);
    format_cardinal(avi_2, avi3_c, sizeof(avi3_c), 2);
    format_cardinal(avc_2, avc4_c, sizeof(avc4_c), 1);
    format_cardinal(avi_2, avi4_c, sizeof(avi4_c), 1);
    format_cardinal(avc_4, avc5_c, sizeof(avc5_c), 4);
    format_cardinal(avi_4, avi5_c, sizeof(avi5_c), 1);
    format_cardinal(avc_8, avc6_c, sizeof(avc6_c), 4);
    format_cardinal(avi_8, avi6_c, sizeof(avi6_c), 1);

    char buf[46];
    outbuf ob;

    memcpy(buf, avc1_c, sizeof(avc1_c));
    memcpy(buf + sizeof(avc1_c), &u, 32);
    avc1.extract(sizeof(avc1_c) + 32, (unsigned char *)buf);
    for (int idx = 0; idx < 32; ++idx)
        TS_ASSERT_EQUALS(avc1[idx], u.ch[idx]);
    TS_ASSERT_EQUALS(avc1.cardinal(), 32);
    memset(buf, 0, 46);
    ob.set((unsigned char *)buf, 46);
    avc1.output(ob);
    TS_ASSERT_SAME_DATA(&avc1_c, buf, sizeof(avc1_c));
    TS_ASSERT_SAME_DATA(&u, buf + sizeof(avc1_c), 32);

    memcpy(buf, avi1_c, sizeof(avi1_c));
    memcpy(buf + sizeof(avi1_c), &u, 32);
    avi1.extract(sizeof(avi1_c) + 32, (unsigned char *)buf);
    memset(buf, 0, 46);
    for (int idx = 0; idx < 8; ++idx)
        TS_ASSERT_EQUALS(avi1[idx],rb(u.it[idx]));
    TS_ASSERT_EQUALS(avi1.cardinal(), 8);
    memset(buf, 0, 46);
    ob.set((unsigned char *)buf, 46);
    avi1.output(ob);
    TS_ASSERT_SAME_DATA(&avi1_c, buf, sizeof(avi1_c));
    TS_ASSERT_SAME_DATA(&u, buf + sizeof(avi1_c), 32);

    memcpy(buf, avc2_c, sizeof(avc2_c));
    memcpy(buf + sizeof(avc2_c), &u, 32);
    avc2.extract(sizeof(avc2_c) + 32, (unsigned char *)buf);
    for (int idx = 0; idx < 32; ++idx)
        TS_ASSERT_EQUALS(avc2[idx], u.ch[idx]);
    TS_ASSERT_EQUALS(avc2.cardinal(), 32);
    memset(buf, 0, 46);
    ob.set((unsigned char *)buf, 46);
    avc2.output(ob);
    TS_ASSERT_SAME_DATA(&avc2_c, buf, sizeof(avc2_c));
    TS_ASSERT_SAME_DATA(&u, buf + sizeof(avc2_c), 32);

    memcpy(buf, avi2_c, sizeof(avi2_c));
    memcpy(buf + sizeof(avi2_c), &u, 32);
    avi2.extract(sizeof(avi2_c) + 32, (unsigned char *)buf);
    memset(buf, 0, 46);
    for (int idx = 0; idx < 8; ++idx)
        TS_ASSERT_EQUALS(avi2[idx], rb(u.it[idx]));
    TS_ASSERT_EQUALS(avi2.cardinal(), 8);
    memset(buf, 0, 46);
    ob.set((unsigned char *)buf, 46);
    avi2.output(ob);
    TS_ASSERT_SAME_DATA(&avi2_c, buf, sizeof(avi2_c));
    TS_ASSERT_SAME_DATA(&u, buf + sizeof(avi2_c), 32);

    memcpy(buf, avc3_c, sizeof(avc3_c));
    memcpy(buf + sizeof(avc3_c), &u, 32);
    avc3.extract(sizeof(avc3_c) + 32, (unsigned char *)buf);
    for (int idx = 0; idx < 32; ++idx)
        TS_ASSERT_EQUALS(avc3[idx], u.ch[idx]);
    TS_ASSERT_EQUALS(avc3.cardinal(), 32);
    memset(buf, 0, 46);
    ob.set((unsigned char *)buf, 46);
    avc3.output(ob);
    TS_ASSERT_SAME_DATA(&avc3_c, buf, sizeof(avc3_c));
    TS_ASSERT_SAME_DATA(&u, buf + sizeof(avc3_c), 32);

    memcpy(buf, avi3_c, sizeof(avi3_c));
    memcpy(buf + sizeof(avi3_c), &u, 32);
    avi3.extract(sizeof(avi3_c) + 32, (unsigned char *)buf);
    memset(buf, 0, 46);
    for (int idx = 0; idx < 8; ++idx)
        TS_ASSERT_EQUALS(avi3[idx], rb(u.it[idx]));
    TS_ASSERT_EQUALS(avi3.cardinal(), 8);
    memset(buf, 0, 46);
    ob.set((unsigned char *)buf, 46);
    avi3.output(ob);
    TS_ASSERT_SAME_DATA(&avi3_c, buf, sizeof(avi3_c));
    TS_ASSERT_SAME_DATA(&u, buf + sizeof(avi3_c), 32);

    memcpy(buf, avc4_c, sizeof(avc4_c));
    memcpy(buf + sizeof(avc4_c), &u, 32);
    avc4.extract(sizeof(avc4_c) + 32, (unsigned char *)buf);
    for (int idx = 0; idx < 32; ++idx)
        TS_ASSERT_EQUALS(avc4[idx], u.ch[idx]);
    TS_ASSERT_EQUALS(avc4.cardinal(), 32);
    memset(buf, 0, 46);
    ob.set((unsigned char *)buf, 46);
    avc4.output(ob);
    TS_ASSERT_SAME_DATA(&avc4_c, buf, sizeof(avc4_c));
    TS_ASSERT_SAME_DATA(&u, buf + sizeof(avc4_c), 32);

    memcpy(buf, avi4_c, sizeof(avi4_c));
    memcpy(buf + sizeof(avi4_c), &u, 32);
    avi4.extract(sizeof(avi4_c) + 32, (unsigned char *)buf);
    memset(buf, 0, 46);
    for (int idx = 0; idx < 8; ++idx)
        TS_ASSERT_EQUALS(avi4[idx], rb(u.it[idx]));
    TS_ASSERT_EQUALS(avi4.cardinal(), 8);
    memset(buf, 0, 46);
    ob.set((unsigned char *)buf, 46);
    avi4.output(ob);
    TS_ASSERT_SAME_DATA(&avi4_c, buf, sizeof(avi4_c));
    TS_ASSERT_SAME_DATA(&u, buf + sizeof(avi4_c), 32);

    memcpy(buf, avc5_c, sizeof(avc5_c));
    memcpy(buf + sizeof(avc5_c), &u, 32);
    avc5.extract(sizeof(avc5_c) + 32, (unsigned char *)buf);
    for (int idx = 0; idx < 32; ++idx)
        TS_ASSERT_EQUALS(avc5[idx], u.ch[idx]);
    TS_ASSERT_EQUALS(avc5.cardinal(), 32);
    memset(buf, 0, 46);
    ob.set((unsigned char *)buf, 46);
    avc5.output(ob);
    TS_ASSERT_SAME_DATA(&avc5_c, buf, sizeof(avc5_c));
    TS_ASSERT_SAME_DATA(&u, buf + sizeof(avc5_c), 32);

    memcpy(buf, avi5_c, sizeof(avi5_c));
    memcpy(buf + sizeof(avi5_c), &u, 32);
    avi5.extract(sizeof(avi5_c) + 32, (unsigned char *)buf);
    memset(buf, 0, 46);
    for (int idx = 0; idx < 8; ++idx)
        TS_ASSERT_EQUALS(avi5[idx], rb(u.it[idx]));
    TS_ASSERT_EQUALS(avi5.cardinal(), 8);
    memset(buf, 0, 46);
    ob.set((unsigned char *)buf, 46);
    avi5.output(ob);
    TS_ASSERT_SAME_DATA(&avi5_c, buf, sizeof(avi5_c));
    TS_ASSERT_SAME_DATA(&u, buf + sizeof(avi5_c), 32);

    memcpy(buf, avc6_c, sizeof(avc6_c));
    memcpy(buf + sizeof(avc6_c), &u, 32);
    avc6.extract(sizeof(avc6_c) + 32, (unsigned char *)buf);
    for (int idx = 0; idx < 32; ++idx)
        TS_ASSERT_EQUALS(avc6[idx], u.ch[idx]);
    TS_ASSERT_EQUALS(avc6.cardinal(), 32);
    memset(buf, 0, 46);
    ob.set((unsigned char *)buf, 46);
    avc6.output(ob);
    TS_ASSERT_SAME_DATA(&avc6_c, buf, sizeof(avc6_c));
    TS_ASSERT_SAME_DATA(&u, buf + sizeof(avc6_c), 32);

    memcpy(buf, avi6_c, sizeof(avi6_c));
    memcpy(buf + sizeof(avi6_c), &u, 32);
    avi6.extract(sizeof(avi6_c) + 32, (unsigned char *)buf);
    memset(buf, 0, 46);
    for (int idx = 0; idx < 8; ++idx)
        TS_ASSERT_EQUALS(avi6[idx], rb(u.it[idx]));
    TS_ASSERT_EQUALS(avi6.cardinal(), 8);
    memset(buf, 0, 46);
    ob.set((unsigned char *)buf, 46);
    avi6.output(ob);
    TS_ASSERT_SAME_DATA(&avi6_c, buf, sizeof(avi6_c));
    TS_ASSERT_SAME_DATA(&u, buf + sizeof(avi6_c), 32);

    TS_TRACE("End");
}

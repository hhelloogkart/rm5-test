/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_rarray_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_ArrayVarTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_rarray_var\test_rarray_var.h"

static ArrayVarTestSuite suite_ArrayVarTestSuite;

static CxxTest::List Tests_ArrayVarTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_ArrayVarTestSuite( "test_rarray_var/test_rarray_var.h", 6, "ArrayVarTestSuite", suite_ArrayVarTestSuite, Tests_ArrayVarTestSuite );

static class TestDescription_suite_ArrayVarTestSuite_testExtract : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ArrayVarTestSuite_testExtract() : CxxTest::RealTestDescription( Tests_ArrayVarTestSuite, suiteDescription_ArrayVarTestSuite, 9, "testExtract" ) {}
 void runTest() { suite_ArrayVarTestSuite.testExtract(); }
} testDescription_suite_ArrayVarTestSuite_testExtract;

static class TestDescription_suite_ArrayVarTestSuite_testOutput : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ArrayVarTestSuite_testOutput() : CxxTest::RealTestDescription( Tests_ArrayVarTestSuite, suiteDescription_ArrayVarTestSuite, 10, "testOutput" ) {}
 void runTest() { suite_ArrayVarTestSuite.testOutput(); }
} testDescription_suite_ArrayVarTestSuite_testOutput;

static class TestDescription_suite_ArrayVarTestSuite_testStream : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ArrayVarTestSuite_testStream() : CxxTest::RealTestDescription( Tests_ArrayVarTestSuite, suiteDescription_ArrayVarTestSuite, 11, "testStream" ) {}
 void runTest() { suite_ArrayVarTestSuite.testStream(); }
} testDescription_suite_ArrayVarTestSuite_testStream;

static class TestDescription_suite_ArrayVarTestSuite_testToValue : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ArrayVarTestSuite_testToValue() : CxxTest::RealTestDescription( Tests_ArrayVarTestSuite, suiteDescription_ArrayVarTestSuite, 12, "testToValue" ) {}
 void runTest() { suite_ArrayVarTestSuite.testToValue(); }
} testDescription_suite_ArrayVarTestSuite_testToValue;

static class TestDescription_suite_ArrayVarTestSuite_testFlexCopy : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ArrayVarTestSuite_testFlexCopy() : CxxTest::RealTestDescription( Tests_ArrayVarTestSuite, suiteDescription_ArrayVarTestSuite, 13, "testFlexCopy" ) {}
 void runTest() { suite_ArrayVarTestSuite.testFlexCopy(); }
} testDescription_suite_ArrayVarTestSuite_testFlexCopy;

static class TestDescription_suite_ArrayVarTestSuite_testRtti : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ArrayVarTestSuite_testRtti() : CxxTest::RealTestDescription( Tests_ArrayVarTestSuite, suiteDescription_ArrayVarTestSuite, 14, "testRtti" ) {}
 void runTest() { suite_ArrayVarTestSuite.testRtti(); }
} testDescription_suite_ArrayVarTestSuite_testRtti;

static class TestDescription_suite_ArrayVarTestSuite_testRoundingAndClipping : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ArrayVarTestSuite_testRoundingAndClipping() : CxxTest::RealTestDescription( Tests_ArrayVarTestSuite, suiteDescription_ArrayVarTestSuite, 15, "testRoundingAndClipping" ) {}
 void runTest() { suite_ArrayVarTestSuite.testRoundingAndClipping(); }
} testDescription_suite_ArrayVarTestSuite_testRoundingAndClipping;

static class TestDescription_suite_ArrayVarTestSuite_testDirty : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ArrayVarTestSuite_testDirty() : CxxTest::RealTestDescription( Tests_ArrayVarTestSuite, suiteDescription_ArrayVarTestSuite, 16, "testDirty" ) {}
 void runTest() { suite_ArrayVarTestSuite.testDirty(); }
} testDescription_suite_ArrayVarTestSuite_testDirty;

static class TestDescription_suite_ArrayVarTestSuite_testMisc : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ArrayVarTestSuite_testMisc() : CxxTest::RealTestDescription( Tests_ArrayVarTestSuite, suiteDescription_ArrayVarTestSuite, 17, "testMisc" ) {}
 void runTest() { suite_ArrayVarTestSuite.testMisc(); }
} testDescription_suite_ArrayVarTestSuite_testMisc;

static class TestDescription_suite_ArrayVarTestSuite_testExtract_1 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ArrayVarTestSuite_testExtract_1() : CxxTest::RealTestDescription( Tests_ArrayVarTestSuite, suiteDescription_ArrayVarTestSuite, 18, "testExtract_1" ) {}
 void runTest() { suite_ArrayVarTestSuite.testExtract_1(); }
} testDescription_suite_ArrayVarTestSuite_testExtract_1;

static class TestDescription_suite_ArrayVarTestSuite_testExtract_4 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ArrayVarTestSuite_testExtract_4() : CxxTest::RealTestDescription( Tests_ArrayVarTestSuite, suiteDescription_ArrayVarTestSuite, 19, "testExtract_4" ) {}
 void runTest() { suite_ArrayVarTestSuite.testExtract_4(); }
} testDescription_suite_ArrayVarTestSuite_testExtract_4;

static class TestDescription_suite_ArrayVarTestSuite_testExtract_8 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ArrayVarTestSuite_testExtract_8() : CxxTest::RealTestDescription( Tests_ArrayVarTestSuite, suiteDescription_ArrayVarTestSuite, 20, "testExtract_8" ) {}
 void runTest() { suite_ArrayVarTestSuite.testExtract_8(); }
} testDescription_suite_ArrayVarTestSuite_testExtract_8;

static class TestDescription_suite_ArrayVarTestSuite_testOutput_1 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ArrayVarTestSuite_testOutput_1() : CxxTest::RealTestDescription( Tests_ArrayVarTestSuite, suiteDescription_ArrayVarTestSuite, 21, "testOutput_1" ) {}
 void runTest() { suite_ArrayVarTestSuite.testOutput_1(); }
} testDescription_suite_ArrayVarTestSuite_testOutput_1;

static class TestDescription_suite_ArrayVarTestSuite_testOutput_4 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ArrayVarTestSuite_testOutput_4() : CxxTest::RealTestDescription( Tests_ArrayVarTestSuite, suiteDescription_ArrayVarTestSuite, 22, "testOutput_4" ) {}
 void runTest() { suite_ArrayVarTestSuite.testOutput_4(); }
} testDescription_suite_ArrayVarTestSuite_testOutput_4;

static class TestDescription_suite_ArrayVarTestSuite_testOutput_8 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ArrayVarTestSuite_testOutput_8() : CxxTest::RealTestDescription( Tests_ArrayVarTestSuite, suiteDescription_ArrayVarTestSuite, 23, "testOutput_8" ) {}
 void runTest() { suite_ArrayVarTestSuite.testOutput_8(); }
} testDescription_suite_ArrayVarTestSuite_testOutput_8;

static class TestDescription_suite_ArrayVarTestSuite_testArrayBinaryAssignment : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ArrayVarTestSuite_testArrayBinaryAssignment() : CxxTest::RealTestDescription( Tests_ArrayVarTestSuite, suiteDescription_ArrayVarTestSuite, 24, "testArrayBinaryAssignment" ) {}
 void runTest() { suite_ArrayVarTestSuite.testArrayBinaryAssignment(); }
} testDescription_suite_ArrayVarTestSuite_testArrayBinaryAssignment;

static class TestDescription_suite_ArrayVarTestSuite_testArrayBinaryAssignment_1 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ArrayVarTestSuite_testArrayBinaryAssignment_1() : CxxTest::RealTestDescription( Tests_ArrayVarTestSuite, suiteDescription_ArrayVarTestSuite, 25, "testArrayBinaryAssignment_1" ) {}
 void runTest() { suite_ArrayVarTestSuite.testArrayBinaryAssignment_1(); }
} testDescription_suite_ArrayVarTestSuite_testArrayBinaryAssignment_1;

static class TestDescription_suite_ArrayVarTestSuite_testArrayBinaryAssignment_4 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ArrayVarTestSuite_testArrayBinaryAssignment_4() : CxxTest::RealTestDescription( Tests_ArrayVarTestSuite, suiteDescription_ArrayVarTestSuite, 26, "testArrayBinaryAssignment_4" ) {}
 void runTest() { suite_ArrayVarTestSuite.testArrayBinaryAssignment_4(); }
} testDescription_suite_ArrayVarTestSuite_testArrayBinaryAssignment_4;

static class TestDescription_suite_ArrayVarTestSuite_testArrayBinaryAssignment_8 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ArrayVarTestSuite_testArrayBinaryAssignment_8() : CxxTest::RealTestDescription( Tests_ArrayVarTestSuite, suiteDescription_ArrayVarTestSuite, 27, "testArrayBinaryAssignment_8" ) {}
 void runTest() { suite_ArrayVarTestSuite.testArrayBinaryAssignment_8(); }
} testDescription_suite_ArrayVarTestSuite_testArrayBinaryAssignment_8;

static class TestDescription_suite_ArrayVarTestSuite_testPadding : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ArrayVarTestSuite_testPadding() : CxxTest::RealTestDescription( Tests_ArrayVarTestSuite, suiteDescription_ArrayVarTestSuite, 28, "testPadding" ) {}
 void runTest() { suite_ArrayVarTestSuite.testPadding(); }
} testDescription_suite_ArrayVarTestSuite_testPadding;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

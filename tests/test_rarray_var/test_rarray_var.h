#ifndef _TEST_ARRAY_VAR_H_DEF_
#define _TEST_ARRAY_VAR_H_DEF_

#include <cxxtest/TestSuite.h>

class ArrayVarTestSuite : public CxxTest::TestSuite
{
public:
    void testExtract(void);
    void testOutput(void);
    void testStream(void);
    void testToValue(void);
    void testFlexCopy();
    void testRtti();
    void testRoundingAndClipping();
    void testDirty();
    void testMisc();
    void testExtract_1();
    void testExtract_4();
    void testExtract_8();
    void testOutput_1();
    void testOutput_4();
    void testOutput_8();
    void testArrayBinaryAssignment();
    void testArrayBinaryAssignment_1();
    void testArrayBinaryAssignment_4();
    void testArrayBinaryAssignment_8();
    void testPadding();
};

#endif
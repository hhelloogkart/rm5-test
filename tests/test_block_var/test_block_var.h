#ifndef _TEST_BLOCK_VAR_H_
#define _TEST_BLOCK_VAR_H_

#include <cxxtest/TestSuite.h>

class BlockVarTestSuite : public CxxTest::TestSuite
{
public:
    void TestSmallFileToFile();
    void TestMediumFileToFile();
    void TestLargeFileToFile();
    void TestGiantFileToFile();
    void TestVarToVar();
    void TestVarToFile();
    void TestFileToVar();
    void TestVarToFunc();
    void TestPullFileToFile();
    void TestPullVarToVar();
    void TestPullFileToVar();
    void TestPullVarToFile();
    void TestPullFuncToVar();
    void TestPullDirToVar();
    void TestPullCmdToVar();
    void TestMultipleTransactions();
    void TestSmallFileToFileWithError();
    void TestMediumFileToFileWithError();
    void TestLargeFileToFileWithError();
    void TestMediumFileToFileWithSevereError();
    void TestMediumFileToFileWithBidirectSevereError();
    void TestAckTimeout();
    void TestRecvTimeout();
    void TestUnmappedRecv();
    void TestUnmappedRecvRejected();
    void TestPullFileNotFound();
    void TestPullRejected();
    void TestPullDirRejected();
    void TestPullCmdRejected();
    void TestRecvWildcard();
    void TestRecvWildcardRejected();
    void TestRecvWildcardFileWildcard();
    void TestPullFileWildcard();
    void TestPullFileWildcardRejected();
    void TestPullFileWildcardFileWildcard();
    void TestMediumFileToFileInitialPacketLost();
    void TestMediumFileToFileHundredTimes();
    void TestTinyFileToFileHundredTimes();
    void TestBlockVarInsideUnionVar();
    void TestBlockVarInsideUnionVarMultiplexTransfer();
    void TestShow();
    void TestAbortSender();
    void TestAbortRecv();

};

#endif
/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_block_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_BlockVarTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_block_var\test_block_var.h"

static BlockVarTestSuite suite_BlockVarTestSuite;

static CxxTest::List Tests_BlockVarTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_BlockVarTestSuite( "test_block_var/test_block_var.h", 6, "BlockVarTestSuite", suite_BlockVarTestSuite, Tests_BlockVarTestSuite );

static class TestDescription_suite_BlockVarTestSuite_TestSmallFileToFile : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestSmallFileToFile() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 9, "TestSmallFileToFile" ) {}
 void runTest() { suite_BlockVarTestSuite.TestSmallFileToFile(); }
} testDescription_suite_BlockVarTestSuite_TestSmallFileToFile;

static class TestDescription_suite_BlockVarTestSuite_TestMediumFileToFile : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestMediumFileToFile() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 10, "TestMediumFileToFile" ) {}
 void runTest() { suite_BlockVarTestSuite.TestMediumFileToFile(); }
} testDescription_suite_BlockVarTestSuite_TestMediumFileToFile;

static class TestDescription_suite_BlockVarTestSuite_TestLargeFileToFile : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestLargeFileToFile() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 11, "TestLargeFileToFile" ) {}
 void runTest() { suite_BlockVarTestSuite.TestLargeFileToFile(); }
} testDescription_suite_BlockVarTestSuite_TestLargeFileToFile;

static class TestDescription_suite_BlockVarTestSuite_TestGiantFileToFile : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestGiantFileToFile() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 12, "TestGiantFileToFile" ) {}
 void runTest() { suite_BlockVarTestSuite.TestGiantFileToFile(); }
} testDescription_suite_BlockVarTestSuite_TestGiantFileToFile;

static class TestDescription_suite_BlockVarTestSuite_TestVarToVar : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestVarToVar() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 13, "TestVarToVar" ) {}
 void runTest() { suite_BlockVarTestSuite.TestVarToVar(); }
} testDescription_suite_BlockVarTestSuite_TestVarToVar;

static class TestDescription_suite_BlockVarTestSuite_TestVarToFile : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestVarToFile() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 14, "TestVarToFile" ) {}
 void runTest() { suite_BlockVarTestSuite.TestVarToFile(); }
} testDescription_suite_BlockVarTestSuite_TestVarToFile;

static class TestDescription_suite_BlockVarTestSuite_TestFileToVar : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestFileToVar() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 15, "TestFileToVar" ) {}
 void runTest() { suite_BlockVarTestSuite.TestFileToVar(); }
} testDescription_suite_BlockVarTestSuite_TestFileToVar;

static class TestDescription_suite_BlockVarTestSuite_TestVarToFunc : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestVarToFunc() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 16, "TestVarToFunc" ) {}
 void runTest() { suite_BlockVarTestSuite.TestVarToFunc(); }
} testDescription_suite_BlockVarTestSuite_TestVarToFunc;

static class TestDescription_suite_BlockVarTestSuite_TestPullFileToFile : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestPullFileToFile() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 17, "TestPullFileToFile" ) {}
 void runTest() { suite_BlockVarTestSuite.TestPullFileToFile(); }
} testDescription_suite_BlockVarTestSuite_TestPullFileToFile;

static class TestDescription_suite_BlockVarTestSuite_TestPullVarToVar : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestPullVarToVar() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 18, "TestPullVarToVar" ) {}
 void runTest() { suite_BlockVarTestSuite.TestPullVarToVar(); }
} testDescription_suite_BlockVarTestSuite_TestPullVarToVar;

static class TestDescription_suite_BlockVarTestSuite_TestPullFileToVar : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestPullFileToVar() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 19, "TestPullFileToVar" ) {}
 void runTest() { suite_BlockVarTestSuite.TestPullFileToVar(); }
} testDescription_suite_BlockVarTestSuite_TestPullFileToVar;

static class TestDescription_suite_BlockVarTestSuite_TestPullVarToFile : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestPullVarToFile() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 20, "TestPullVarToFile" ) {}
 void runTest() { suite_BlockVarTestSuite.TestPullVarToFile(); }
} testDescription_suite_BlockVarTestSuite_TestPullVarToFile;

static class TestDescription_suite_BlockVarTestSuite_TestPullFuncToVar : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestPullFuncToVar() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 21, "TestPullFuncToVar" ) {}
 void runTest() { suite_BlockVarTestSuite.TestPullFuncToVar(); }
} testDescription_suite_BlockVarTestSuite_TestPullFuncToVar;

static class TestDescription_suite_BlockVarTestSuite_TestPullDirToVar : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestPullDirToVar() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 22, "TestPullDirToVar" ) {}
 void runTest() { suite_BlockVarTestSuite.TestPullDirToVar(); }
} testDescription_suite_BlockVarTestSuite_TestPullDirToVar;

static class TestDescription_suite_BlockVarTestSuite_TestPullCmdToVar : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestPullCmdToVar() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 23, "TestPullCmdToVar" ) {}
 void runTest() { suite_BlockVarTestSuite.TestPullCmdToVar(); }
} testDescription_suite_BlockVarTestSuite_TestPullCmdToVar;

static class TestDescription_suite_BlockVarTestSuite_TestMultipleTransactions : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestMultipleTransactions() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 24, "TestMultipleTransactions" ) {}
 void runTest() { suite_BlockVarTestSuite.TestMultipleTransactions(); }
} testDescription_suite_BlockVarTestSuite_TestMultipleTransactions;

static class TestDescription_suite_BlockVarTestSuite_TestSmallFileToFileWithError : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestSmallFileToFileWithError() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 25, "TestSmallFileToFileWithError" ) {}
 void runTest() { suite_BlockVarTestSuite.TestSmallFileToFileWithError(); }
} testDescription_suite_BlockVarTestSuite_TestSmallFileToFileWithError;

static class TestDescription_suite_BlockVarTestSuite_TestMediumFileToFileWithError : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestMediumFileToFileWithError() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 26, "TestMediumFileToFileWithError" ) {}
 void runTest() { suite_BlockVarTestSuite.TestMediumFileToFileWithError(); }
} testDescription_suite_BlockVarTestSuite_TestMediumFileToFileWithError;

static class TestDescription_suite_BlockVarTestSuite_TestLargeFileToFileWithError : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestLargeFileToFileWithError() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 27, "TestLargeFileToFileWithError" ) {}
 void runTest() { suite_BlockVarTestSuite.TestLargeFileToFileWithError(); }
} testDescription_suite_BlockVarTestSuite_TestLargeFileToFileWithError;

static class TestDescription_suite_BlockVarTestSuite_TestMediumFileToFileWithSevereError : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestMediumFileToFileWithSevereError() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 28, "TestMediumFileToFileWithSevereError" ) {}
 void runTest() { suite_BlockVarTestSuite.TestMediumFileToFileWithSevereError(); }
} testDescription_suite_BlockVarTestSuite_TestMediumFileToFileWithSevereError;

static class TestDescription_suite_BlockVarTestSuite_TestMediumFileToFileWithBidirectSevereError : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestMediumFileToFileWithBidirectSevereError() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 29, "TestMediumFileToFileWithBidirectSevereError" ) {}
 void runTest() { suite_BlockVarTestSuite.TestMediumFileToFileWithBidirectSevereError(); }
} testDescription_suite_BlockVarTestSuite_TestMediumFileToFileWithBidirectSevereError;

static class TestDescription_suite_BlockVarTestSuite_TestAckTimeout : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestAckTimeout() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 30, "TestAckTimeout" ) {}
 void runTest() { suite_BlockVarTestSuite.TestAckTimeout(); }
} testDescription_suite_BlockVarTestSuite_TestAckTimeout;

static class TestDescription_suite_BlockVarTestSuite_TestRecvTimeout : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestRecvTimeout() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 31, "TestRecvTimeout" ) {}
 void runTest() { suite_BlockVarTestSuite.TestRecvTimeout(); }
} testDescription_suite_BlockVarTestSuite_TestRecvTimeout;

static class TestDescription_suite_BlockVarTestSuite_TestUnmappedRecv : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestUnmappedRecv() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 32, "TestUnmappedRecv" ) {}
 void runTest() { suite_BlockVarTestSuite.TestUnmappedRecv(); }
} testDescription_suite_BlockVarTestSuite_TestUnmappedRecv;

static class TestDescription_suite_BlockVarTestSuite_TestUnmappedRecvRejected : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestUnmappedRecvRejected() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 33, "TestUnmappedRecvRejected" ) {}
 void runTest() { suite_BlockVarTestSuite.TestUnmappedRecvRejected(); }
} testDescription_suite_BlockVarTestSuite_TestUnmappedRecvRejected;

static class TestDescription_suite_BlockVarTestSuite_TestPullFileNotFound : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestPullFileNotFound() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 34, "TestPullFileNotFound" ) {}
 void runTest() { suite_BlockVarTestSuite.TestPullFileNotFound(); }
} testDescription_suite_BlockVarTestSuite_TestPullFileNotFound;

static class TestDescription_suite_BlockVarTestSuite_TestPullRejected : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestPullRejected() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 35, "TestPullRejected" ) {}
 void runTest() { suite_BlockVarTestSuite.TestPullRejected(); }
} testDescription_suite_BlockVarTestSuite_TestPullRejected;

static class TestDescription_suite_BlockVarTestSuite_TestPullDirRejected : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestPullDirRejected() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 36, "TestPullDirRejected" ) {}
 void runTest() { suite_BlockVarTestSuite.TestPullDirRejected(); }
} testDescription_suite_BlockVarTestSuite_TestPullDirRejected;

static class TestDescription_suite_BlockVarTestSuite_TestPullCmdRejected : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestPullCmdRejected() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 37, "TestPullCmdRejected" ) {}
 void runTest() { suite_BlockVarTestSuite.TestPullCmdRejected(); }
} testDescription_suite_BlockVarTestSuite_TestPullCmdRejected;

static class TestDescription_suite_BlockVarTestSuite_TestRecvWildcard : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestRecvWildcard() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 38, "TestRecvWildcard" ) {}
 void runTest() { suite_BlockVarTestSuite.TestRecvWildcard(); }
} testDescription_suite_BlockVarTestSuite_TestRecvWildcard;

static class TestDescription_suite_BlockVarTestSuite_TestRecvWildcardRejected : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestRecvWildcardRejected() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 39, "TestRecvWildcardRejected" ) {}
 void runTest() { suite_BlockVarTestSuite.TestRecvWildcardRejected(); }
} testDescription_suite_BlockVarTestSuite_TestRecvWildcardRejected;

static class TestDescription_suite_BlockVarTestSuite_TestRecvWildcardFileWildcard : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestRecvWildcardFileWildcard() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 40, "TestRecvWildcardFileWildcard" ) {}
 void runTest() { suite_BlockVarTestSuite.TestRecvWildcardFileWildcard(); }
} testDescription_suite_BlockVarTestSuite_TestRecvWildcardFileWildcard;

static class TestDescription_suite_BlockVarTestSuite_TestPullFileWildcard : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestPullFileWildcard() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 41, "TestPullFileWildcard" ) {}
 void runTest() { suite_BlockVarTestSuite.TestPullFileWildcard(); }
} testDescription_suite_BlockVarTestSuite_TestPullFileWildcard;

static class TestDescription_suite_BlockVarTestSuite_TestPullFileWildcardRejected : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestPullFileWildcardRejected() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 42, "TestPullFileWildcardRejected" ) {}
 void runTest() { suite_BlockVarTestSuite.TestPullFileWildcardRejected(); }
} testDescription_suite_BlockVarTestSuite_TestPullFileWildcardRejected;

static class TestDescription_suite_BlockVarTestSuite_TestPullFileWildcardFileWildcard : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestPullFileWildcardFileWildcard() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 43, "TestPullFileWildcardFileWildcard" ) {}
 void runTest() { suite_BlockVarTestSuite.TestPullFileWildcardFileWildcard(); }
} testDescription_suite_BlockVarTestSuite_TestPullFileWildcardFileWildcard;

static class TestDescription_suite_BlockVarTestSuite_TestMediumFileToFileInitialPacketLost : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestMediumFileToFileInitialPacketLost() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 44, "TestMediumFileToFileInitialPacketLost" ) {}
 void runTest() { suite_BlockVarTestSuite.TestMediumFileToFileInitialPacketLost(); }
} testDescription_suite_BlockVarTestSuite_TestMediumFileToFileInitialPacketLost;

static class TestDescription_suite_BlockVarTestSuite_TestMediumFileToFileHundredTimes : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestMediumFileToFileHundredTimes() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 45, "TestMediumFileToFileHundredTimes" ) {}
 void runTest() { suite_BlockVarTestSuite.TestMediumFileToFileHundredTimes(); }
} testDescription_suite_BlockVarTestSuite_TestMediumFileToFileHundredTimes;

static class TestDescription_suite_BlockVarTestSuite_TestTinyFileToFileHundredTimes : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestTinyFileToFileHundredTimes() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 46, "TestTinyFileToFileHundredTimes" ) {}
 void runTest() { suite_BlockVarTestSuite.TestTinyFileToFileHundredTimes(); }
} testDescription_suite_BlockVarTestSuite_TestTinyFileToFileHundredTimes;

static class TestDescription_suite_BlockVarTestSuite_TestBlockVarInsideUnionVar : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestBlockVarInsideUnionVar() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 47, "TestBlockVarInsideUnionVar" ) {}
 void runTest() { suite_BlockVarTestSuite.TestBlockVarInsideUnionVar(); }
} testDescription_suite_BlockVarTestSuite_TestBlockVarInsideUnionVar;

static class TestDescription_suite_BlockVarTestSuite_TestBlockVarInsideUnionVarMultiplexTransfer : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestBlockVarInsideUnionVarMultiplexTransfer() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 48, "TestBlockVarInsideUnionVarMultiplexTransfer" ) {}
 void runTest() { suite_BlockVarTestSuite.TestBlockVarInsideUnionVarMultiplexTransfer(); }
} testDescription_suite_BlockVarTestSuite_TestBlockVarInsideUnionVarMultiplexTransfer;

static class TestDescription_suite_BlockVarTestSuite_TestShow : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_BlockVarTestSuite_TestShow() : CxxTest::RealTestDescription( Tests_BlockVarTestSuite, suiteDescription_BlockVarTestSuite, 49, "TestShow" ) {}
 void runTest() { suite_BlockVarTestSuite.TestShow(); }
} testDescription_suite_BlockVarTestSuite_TestShow;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

#include "test_block_var.h"
#include <block_var.h>
#include <arry_var.h>
#include <union_var.h>
#include <outbuf.h>
#include <stdio.h>
#include <time.h>

static char *glob_buffer = 0;
static int glob_buffer_len = 0;

char *receiver_callback_function(int &len)
{
    glob_buffer = new char[len];
    glob_buffer_len = len;
    return glob_buffer;
}

char *sender_callback_function(int &len)
{
    const int filesize = 200;
    glob_buffer = new char[filesize];
    glob_buffer_len = filesize;
    len = filesize;

    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        glob_buffer[cnt] = ch;
    return glob_buffer;
}

class block_var_container : public complex_var
{
public:
    COMPLEXCONST(block_var_container)
    block_var data;
    void setRMDirty();
    void setBuddy(block_var_container *cont);
    void setDropFactor(unsigned long df);
    bool transfer();

private:
    bool dirty;
    unsigned long counter;
    unsigned long drop_factor; // eg 2 means drop every other packet
    block_var_container *buddy;
};

void block_var_container::setRMDirty()
{
    dirty = true;
}

void block_var_container::setBuddy(block_var_container *cont)
{
    buddy = cont;
    dirty = false;
    counter = 0;
    drop_factor = 0xFFFFFFFF;
}

void block_var_container::setDropFactor(unsigned long df)
{
    drop_factor = df;
}

bool block_var_container::transfer()
{
    if (!buddy) return false;
    mpt_show::refresh();
    if (!dirty) return false;

    const int sz = size();
    if (sz == 2) return false;

    unsigned char *buf = new unsigned char[sz];
    outbuf ob;
    ob.set(buf, sz);
    output(ob);
    dirty = false;
    if (++counter % drop_factor != 0)
       buddy->extract(ob.size(), buf);
    delete[] buf;
    return true;
}

class TestVar : public complex_var
{
public:
    COMPLEXCONST(TestVar)
    generic_var<char> data[200];
};

class TestVar2 : public complex_var
{
public:
    COMPLEXCONST(TestVar2)
    generic_var<char> dummy;
    generic_var<char> selector;
    generic_var<float> flt[10];
    generic_var<double> dbl[10];
};

class TestUnion : public union_var
{
public:
    UNIONCONST(TestUnion, 1, 1, 0)
    block_var data;
    string_var<30> str;
    TestVar2 cmplx;
};

class block_var_union : public complex_var
{
public:
    COMPLEXCONST(block_var_union)
    TestUnion u;
    bool isRMDirty() const;
    void setRMDirty();
    void setBuddy(block_var_union *cont);
    bool transfer();

private:
    bool dirty;
    block_var_union *buddy;
};

bool block_var_union::isRMDirty() const
{
    return dirty;
}

void block_var_union::setRMDirty()
{
    dirty = true;
}

void block_var_union::setBuddy(block_var_union *cont)
{
    buddy = cont;
    dirty = false;
}

bool block_var_union::transfer()
{
    if (!buddy) return false;
    mpt_show::refresh();
    if (!dirty) return false;

    const int sz = size();
    if (sz == 2) return false;

    unsigned char *buf = new unsigned char[sz];
    outbuf ob;
    ob.set(buf, sz);
    output(ob);
    dirty = false;
    buddy->extract(ob.size(), buf);
    delete[] buf;
    return true;
}

class dirty_checker : public mpt_baseshow
{
public:
    dirty_checker(mpt_var *myvar);
    virtual void setDirty(mpt_var *myvar);
    virtual void notDirty(mpt_var *myvar);
    int getStateAndReset();
protected:
    int state; // 0 - not called, 1 - dirty, 2 - notdirty, 3 - error
    mpt_var *var;
};

dirty_checker::dirty_checker(mpt_var *myvar) : state(0), var(myvar)
{
    myvar->addCallback(this);
}

void dirty_checker::setDirty(mpt_var *myvar)
{
    if (myvar == var) state = 1;
    else state = 3;
}

void dirty_checker::notDirty(mpt_var *myvar)
{
    if (myvar == var) {
        if (state == 0)
            state = 2;
    }
    else state = 3;
}

int dirty_checker::getStateAndReset()
{
    int ret = state;
    state = 0;
    return ret;
}

void BlockVarTestSuite::TestSmallFileToFile()
{
    const int filesize = 200;
    block_var_container send, recv;

    TS_ASSERT_EQUALS(send.data.rtti(), recv.data.rtti());
    TS_ASSERT_DIFFERS(send.data.rtti(), recv.rtti());
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    recv.data.recv_data("test", "recvfile");

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    send.data.push_data("test", "sendfile");

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    f = fopen("recvfile", "rb");
    TS_ASSERT_DIFFERS((void *)f, (void *)0);
    if (f)
    {
        fseek(f, 0, SEEK_END);
        long filesz = ftell(f);
        TS_ASSERT_EQUALS(filesz, filesize);
        fseek(f, 0, SEEK_SET);
        char rbuf[filesize];
        fread(rbuf, 1, filesize, f);
        fclose(f);
        ch = '\0';
        for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
            TS_ASSERT_EQUALS(rbuf[cnt], ch);
        remove("recvfile");
    }
    remove("sendfile");
}

void BlockVarTestSuite::TestMediumFileToFile()
{
    const int filesize = 11725;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    recv.data.recv_data("test", "recvfile");

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    send.data.push_data("test", "sendfile");

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    f = fopen("recvfile", "rb");
    TS_ASSERT_DIFFERS((void *)f, (void *)0);
    if (f)
    {
        fseek(f, 0, SEEK_END);
        long filesz = ftell(f);
        TS_ASSERT_EQUALS(filesz, filesize);
        fseek(f, 0, SEEK_SET);
        char rbuf[filesize];
        fread(rbuf, 1, filesize, f);
        fclose(f);
        ch = '\0';
        for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
            TS_ASSERT_EQUALS(rbuf[cnt], ch);
        remove("recvfile");
    }
    remove("sendfile");
}

void BlockVarTestSuite::TestLargeFileToFile()
{
    const int filesize = 1474660;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    recv.data.recv_data("test", "recvfile");

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    send.data.push_data("test", "sendfile");

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    f = fopen("recvfile", "rb");
    TS_ASSERT_DIFFERS((void *)f, (void *)0);
    if (f)
    {
        fseek(f, 0, SEEK_END);
        long filesz = ftell(f);
        TS_ASSERT_EQUALS(filesz, filesize);
        fseek(f, 0, SEEK_SET);

        const int fileblocksz = 8192;
        ch = '\0';
        for (; filesz > 0; filesz -= fileblocksz)
        {
            char rbuf[fileblocksz];
            size_t len = fread(rbuf, 1, fileblocksz, f);
            for (int cnt = 0; cnt < len; ++cnt, ++ch)
                TS_ASSERT_EQUALS(rbuf[cnt], ch);
        }
        fclose(f);

        remove("recvfile");
    }
    remove("sendfile");
}

void BlockVarTestSuite::TestGiantFileToFile()
{
    const int filesize = 188743715;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    recv.data.recv_data("test", "recvfile");

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    send.data.push_data("test", "sendfile");

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    f = fopen("recvfile", "rb");
    TS_ASSERT_DIFFERS((void *)f, (void *)0);
    if (f)
    {
        fseek(f, 0, SEEK_END);
        long filesz = ftell(f);
        TS_ASSERT_EQUALS(filesz, filesize);
        fseek(f, 0, SEEK_SET);

        const int fileblocksz = 8192;
        ch = '\0';
        for (; filesz > 0; filesz -= fileblocksz)
        {
            char rbuf[fileblocksz];
            size_t len = fread(rbuf, 1, fileblocksz, f);
            for (int cnt = 0; cnt < len; ++cnt, ++ch)
                TS_ASSERT_EQUALS(rbuf[cnt], ch);
        }
        fclose(f);

        remove("recvfile");
    }
    remove("sendfile");
}

void BlockVarTestSuite::TestVarToVar()
{
    const int filesize = 200;
    TestVar sendvar, recvvar;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    recv.data.recv_data("test", &recvvar);

    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        sendvar.data[cnt] = ch;

    send.data.push_data("test", &sendvar);

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        TS_ASSERT_EQUALS(recvvar.data[cnt], ch);
}

void BlockVarTestSuite::TestVarToFile()
{
    const int filesize = 200;
    TestVar sendvar;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    recv.data.recv_data("test", "recvfile");

    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        sendvar.data[cnt] = ch;

    send.data.push_data("test", &sendvar);

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    FILE *f = fopen("recvfile", "rb");
    TS_ASSERT_DIFFERS((void *)f, (void *)0);
    if (f)
    {
        fseek(f, 0, SEEK_END);
        long filesz = ftell(f);
        TS_ASSERT_EQUALS(filesz, filesize);
        fseek(f, 0, SEEK_SET);
        char rbuf[filesize];
        fread(rbuf, 1, filesize, f);
        fclose(f);
        ch = '\0';
        for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
            TS_ASSERT_EQUALS(rbuf[cnt], ch);
        remove("recvfile");
    }
}

void BlockVarTestSuite::TestFileToVar()
{
    const int filesize = 200;
    TestVar recvvar;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    recv.data.recv_data("test", &recvvar);

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    send.data.push_data("test", "sendfile");

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        TS_ASSERT_EQUALS(recvvar.data[cnt], ch);
    remove("sendfile");
}

void BlockVarTestSuite::TestVarToFunc()
{
    const int filesize = 200;
    TestVar sendvar;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    recv.data.recv_data("test", &receiver_callback_function, false);

    TS_ASSERT_EQUALS(glob_buffer, (void *)0);
    TS_ASSERT_EQUALS(glob_buffer_len, 0);

    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        sendvar.data[cnt] = ch;

    send.data.push_data("test", &sendvar);

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    TS_ASSERT_DIFFERS(glob_buffer, (void *)0);
    TS_ASSERT_LESS_THAN_EQUALS(100, glob_buffer_len);
    ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        TS_ASSERT_EQUALS(glob_buffer[cnt], ch);

    delete[] glob_buffer;
    glob_buffer = 0;
    glob_buffer_len = 0;
}

void BlockVarTestSuite::TestPullFileToFile()
{
    const int filesize = 200;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    send.data.push_data_on_pull("test", "sendfile");

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    recv.data.pull_data("test", "recvfile");
    mpt_show::refresh();

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    f = fopen("recvfile", "rb");
    TS_ASSERT_DIFFERS((void *)f, (void *)0);
    if (f)
    {
        fseek(f, 0, SEEK_END);
        long filesz = ftell(f);
        TS_ASSERT_EQUALS(filesz, filesize);
        fseek(f, 0, SEEK_SET);
        char rbuf[filesize];
        fread(rbuf, 1, filesize, f);
        fclose(f);
        ch = '\0';
        for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
            TS_ASSERT_EQUALS(rbuf[cnt], ch);
        remove("recvfile");
    }
    remove("sendfile");
}

void BlockVarTestSuite::TestPullVarToVar()
{
    const int filesize = 200;
    TestVar sendvar, recvvar;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    send.data.push_data_on_pull("test", &sendvar);

    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        sendvar.data[cnt] = ch;

    recv.data.pull_data("test", &recvvar);
    mpt_show::refresh();

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        TS_ASSERT_EQUALS(recvvar.data[cnt], ch);
}

void BlockVarTestSuite::TestPullFileToVar()
{
    const int filesize = 200;
    TestVar recvvar;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    send.data.push_data_on_pull("test", "sendfile");

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    recv.data.pull_data("test", &recvvar);
    mpt_show::refresh();

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        TS_ASSERT_EQUALS(recvvar.data[cnt], ch);
}

void BlockVarTestSuite::TestPullVarToFile()
{
    const int filesize = 200;
    TestVar sendvar;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    send.data.push_data_on_pull("test", &sendvar);

    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        sendvar.data[cnt] = ch;

    recv.data.pull_data("test", "recvfile");
    mpt_show::refresh();

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    FILE *f = fopen("recvfile", "rb");
    TS_ASSERT_DIFFERS((void *)f, (void *)0);
    if (f)
    {
        fseek(f, 0, SEEK_END);
        long filesz = ftell(f);
        TS_ASSERT_EQUALS(filesz, filesize);
        fseek(f, 0, SEEK_SET);
        char rbuf[filesize];
        fread(rbuf, 1, filesize, f);
        fclose(f);
        ch = '\0';
        for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
            TS_ASSERT_EQUALS(rbuf[cnt], ch);
        remove("recvfile");
    }
}

void BlockVarTestSuite::TestPullFuncToVar()
{
    const int filesize = 200;
    TestVar recvvar;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    send.data.push_data_on_pull("test", &sender_callback_function);

    TS_ASSERT_EQUALS(glob_buffer, (void *)0);
    TS_ASSERT_EQUALS(glob_buffer_len, 0);

    recv.data.pull_data("test", &recvvar);
    mpt_show::refresh();

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        TS_ASSERT_EQUALS(recvvar.data[cnt], ch);

    delete[] glob_buffer;
    glob_buffer = 0;
    glob_buffer_len = 0;
}

void BlockVarTestSuite::TestPullDirToVar()
{
    const char dirstr[] = ".";
    array_str_var result;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);

    recv.data.get_dir(dirstr, &result);
    mpt_show::refresh();

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, dirstr);
    TS_ASSERT_EQUALS(send.data.send_name, dirstr);

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    TS_ASSERT(result.isValid());
}

void BlockVarTestSuite::TestPullCmdToVar()
{
    const char cmdstr[] = "C:\\Windows\\system32\\ipconfig.exe";
    array_str_var result;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);

#ifdef _WIN32
    recv.data.exec_cmd(cmdstr, &result);
#else
    recv.data.exec_cmd("/sbin/ifconfig.exe", &result);
#endif
    mpt_show::refresh();

    send.transfer();
    recv.transfer();
    bool once = false;
    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        once |= transfered;
        if (!transfered && once) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, cmdstr);
    TS_ASSERT_EQUALS(send.data.send_name, cmdstr);

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    TS_ASSERT(result.isValid());
}

void BlockVarTestSuite::TestMultipleTransactions()
{
    const int filesize = 200;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    recv.data.recv_data("test", "recvfile");

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    for (int i = 0; i < 10; ++i)
    {
        send.data.push_data("test", "sendfile");
        for (;;)
        {
            bool transfered = send.transfer();
            transfered |= recv.transfer();
            if (!transfered) break;
        }

        f = fopen("recvfile", "rb");
        TS_ASSERT_DIFFERS((void *)f, (void *)0);
        if (f)
        {
            fseek(f, 0, SEEK_END);
            long filesz = ftell(f);
            TS_ASSERT_EQUALS(filesz, filesize);
            fseek(f, 0, SEEK_SET);
            char rbuf[filesize];
            fread(rbuf, 1, filesize, f);
            fclose(f);
            ch = '\0';
            for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
                TS_ASSERT_EQUALS(rbuf[cnt], ch);
            remove("recvfile");
        }
    }
    remove("sendfile");
}

void BlockVarTestSuite::TestSmallFileToFileWithError()
{
    const int filesize = 200;
    block_var_container send, recv;
    recv.setDropFactor(5);

    TS_ASSERT_EQUALS(send.data.rtti(), recv.data.rtti());
    TS_ASSERT_DIFFERS(send.data.rtti(), recv.rtti());
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    recv.data.recv_data("test", "recvfile");

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    send.data.push_data("test", "sendfile");

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    f = fopen("recvfile", "rb");
    TS_ASSERT_DIFFERS((void *)f, (void *)0);
    if (f)
    {
        fseek(f, 0, SEEK_END);
        long filesz = ftell(f);
        TS_ASSERT_EQUALS(filesz, filesize);
        fseek(f, 0, SEEK_SET);
        char rbuf[filesize];
        fread(rbuf, 1, filesize, f);
        fclose(f);
        ch = '\0';
        for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
            TS_ASSERT_EQUALS(rbuf[cnt], ch);
        remove("recvfile");
    }
    remove("sendfile");
}

void BlockVarTestSuite::TestMediumFileToFileWithError()
{
    const int filesize = 14725;
    block_var_container send, recv;
    recv.setDropFactor(5);
    send.setBuddy(&recv);
    recv.setBuddy(&send);

    recv.data.recv_data("test", "recvfile");

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    send.data.push_data("test", "sendfile");

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    f = fopen("recvfile", "rb");
    TS_ASSERT_DIFFERS((void *)f, (void *)0);
    if (f)
    {
        fseek(f, 0, SEEK_END);
        long filesz = ftell(f);
        TS_ASSERT_EQUALS(filesz, filesize);
        fseek(f, 0, SEEK_SET);
        char rbuf[filesize];
        fread(rbuf, 1, filesize, f);
        fclose(f);
        ch = '\0';
        for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
            TS_ASSERT_EQUALS(rbuf[cnt], ch);
        remove("recvfile");
    }
    remove("sendfile");
}

void BlockVarTestSuite::TestLargeFileToFileWithError()
{
    const int filesize = 1614480;
    block_var_container send, recv;
    recv.setDropFactor(5);
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    recv.data.recv_data("test", "recvfile");

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    send.data.push_data("test", "sendfile");

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    f = fopen("recvfile", "rb");
    TS_ASSERT_DIFFERS((void *)f, (void *)0);
    if (f)
    {
        fseek(f, 0, SEEK_END);
        long filesz = ftell(f);
        TS_ASSERT_EQUALS(filesz, filesize);
        fseek(f, 0, SEEK_SET);

        const int fileblocksz = 8192;
        ch = '\0';
        for (; filesz > 0; filesz -= fileblocksz)
        {
            char rbuf[fileblocksz];
            size_t len = fread(rbuf, 1, fileblocksz, f);
            for (int cnt = 0; cnt < len; ++cnt, ++ch)
                TS_ASSERT_EQUALS(rbuf[cnt], ch);
        }
        fclose(f);

        remove("recvfile");
    }
    remove("sendfile");
}

void BlockVarTestSuite::TestMediumFileToFileWithSevereError()
{
    const int filesize = 11300;
    block_var_container send, recv;
    recv.setDropFactor(3);
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    recv.data.recv_data("test", "recvfile");

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    send.data.push_data("test", "sendfile");

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    f = fopen("recvfile", "rb");
    TS_ASSERT_DIFFERS((void *)f, (void *)0);
    if (f)
    {
        fseek(f, 0, SEEK_END);
        long filesz = ftell(f);
        TS_ASSERT_EQUALS(filesz, filesize);
        fseek(f, 0, SEEK_SET);
        char rbuf[filesize];
        fread(rbuf, 1, filesize, f);
        fclose(f);
        ch = '\0';
        for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
            TS_ASSERT_EQUALS(rbuf[cnt], ch);
        remove("recvfile");
    }
    remove("sendfile");
}

void BlockVarTestSuite::TestMediumFileToFileWithBidirectSevereError()
{
    const int filesize = 11300;
    block_var_container send, recv;
    recv.setDropFactor(3);
    send.setDropFactor(3);
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    recv.data.recv_data("test", "recvfile");

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    send.data.push_data("test", "sendfile");

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    f = fopen("recvfile", "rb");
    TS_ASSERT_DIFFERS((void *)f, (void *)0);
    if (f)
    {
        fseek(f, 0, SEEK_END);
        long filesz = ftell(f);
        TS_ASSERT_EQUALS(filesz, filesize);
        fseek(f, 0, SEEK_SET);
        char rbuf[filesize];
        fread(rbuf, 1, filesize, f);
        fclose(f);
        ch = '\0';
        for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
            TS_ASSERT_EQUALS(rbuf[cnt], ch);
        remove("recvfile");
    }
    remove("sendfile");
}

void BlockVarTestSuite::TestAckTimeout()
{
    const int filesize = 38308;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    recv.data.recv_data("test", "recvfile");

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    send.data.push_data("test", "sendfile");

    for (int cnt=0; cnt<5; ++cnt)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }
    recv.setDropFactor(1);
    time_t outage = time(0);
    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }
    TS_ASSERT_DELTA(time(0), outage + 21, 2);
    TS_ASSERT_EQUALS(send.data.send_percent.to_int(), 254);
    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    f = fopen("recvfile", "rb");
    TS_ASSERT_DIFFERS((void *)f, (void *)0);
    if (f)
    {
        fseek(f, 0, SEEK_END);
        long filesz = ftell(f);
        TS_ASSERT_EQUALS(filesz, filesize);
        fseek(f, 0, SEEK_SET);
        char rbuf[filesize];
        fread(rbuf, 1, filesize, f);
        fclose(f);
        ch = '\0';
        for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
            TS_ASSERT_EQUALS(rbuf[cnt], ch);
        remove("recvfile");
    }
    remove("sendfile");
}

void BlockVarTestSuite::TestRecvTimeout()
{
    const int filesize = 38308;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    recv.data.recv_data("test", "recvfile");
    recv.data.set_timeout(9);
    send.data.set_timeout(9);

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    send.data.push_data("test", "sendfile");

    for (int cnt = 0; cnt<5; ++cnt)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }
    send.setDropFactor(1);
    time_t outage = time(0);
    for (; time(0) - outage < 12;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent.to_int(), 254);
    TS_ASSERT_EQUALS(send.data.send_percent.to_int(), 254);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    remove("recvfile");
    remove("sendfile");
}

void BlockVarTestSuite::TestUnmappedRecv()
{
    const int filesize = 12122;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    send.data.push_data("test", "sendfile");

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    f = fopen("test", "rb");
    TS_ASSERT_DIFFERS((void *)f, (void *)0);
    if (f)
    {
        fseek(f, 0, SEEK_END);
        long filesz = ftell(f);
        TS_ASSERT_EQUALS(filesz, filesize);
        fseek(f, 0, SEEK_SET);
        char rbuf[filesize];
        fread(rbuf, 1, filesize, f);
        fclose(f);
        ch = '\0';
        for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
            TS_ASSERT_EQUALS(rbuf[cnt], ch);
        remove("test");
    }
    remove("sendfile");
}

void BlockVarTestSuite::TestUnmappedRecvRejected()
{
    const int filesize = 12122;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    recv.data.set_block_unmapped_recvs(true);

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    send.data.push_data("test", "sendfile");

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 255);
    TS_ASSERT_EQUALS(send.data.send_percent, 254);
    TS_ASSERT(!recv.data.recv_name.isValid());
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    remove("sendfile");
}

void BlockVarTestSuite::TestPullFileNotFound()
{
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);

    recv.data.pull_data("test", "recvfile");
    mpt_show::refresh();

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 254);
    TS_ASSERT_EQUALS(send.data.send_percent, 100); // finished sending error
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT(!send.data.send_name.isValid());

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());
}

void BlockVarTestSuite::TestPullRejected()
{
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    send.data.set_block_unmapped_sends(true);

    recv.data.pull_data("test", "recvfile");
    mpt_show::refresh();

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 254);
    TS_ASSERT_EQUALS(send.data.send_percent, 100); // finished sending error
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT(!send.data.send_name.isValid());

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());
}

void BlockVarTestSuite::TestPullDirRejected()
{
    const char dirstr[] = ".";
    array_str_var result;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    send.data.set_block_dir_pulls(true);

    recv.data.get_dir(dirstr, &result);
    mpt_show::refresh();

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 254);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, dirstr);
    TS_ASSERT(!send.data.send_name.isValid());

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    TS_ASSERT(!result.isValid());
}

void BlockVarTestSuite::TestPullCmdRejected()
{
    const char cmdstr[] = "C:\\Windows\\system32\\ipconfig.exe";
    array_str_var result;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    send.data.set_block_cmd_pulls(true);

#ifdef _WIN32
    recv.data.exec_cmd(cmdstr, &result);
#else
    recv.data.exec_cmd("/sbin/ifconfig.exe", &result);
#endif
    mpt_show::refresh();

    send.transfer();
    recv.transfer();
    bool once = false;
    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        once |= transfered;
        if (!transfered && once) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 254);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, cmdstr);
    TS_ASSERT(!send.data.send_name.isValid());

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    TS_ASSERT(!result.isValid());
}

void BlockVarTestSuite::TestRecvWildcard()
{
    const int filesize = 16984;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    recv.data.set_block_unmapped_recvs(true);
    recv.data.recv_data("test_*", "recvfile");

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    send.data.push_data("test_543211", "sendfile");

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test_543211");
    TS_ASSERT_EQUALS(send.data.send_name, "test_543211");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    f = fopen("recvfile", "rb");
    TS_ASSERT_DIFFERS((void *)f, (void *)0);
    if (f)
    {
        fseek(f, 0, SEEK_END);
        long filesz = ftell(f);
        TS_ASSERT_EQUALS(filesz, filesize);
        fseek(f, 0, SEEK_SET);
        char rbuf[filesize];
        fread(rbuf, 1, filesize, f);
        fclose(f);
        ch = '\0';
        for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
            TS_ASSERT_EQUALS(rbuf[cnt], ch);
        remove("recvfile");
    }
    remove("sendfile");
}

void BlockVarTestSuite::TestRecvWildcardRejected()
{
    const int filesize = 6533;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    recv.data.set_block_unmapped_recvs(true);
    recv.data.recv_data("test_*", "recvfile");

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    send.data.push_data("test", "sendfile");

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 255);
    TS_ASSERT_EQUALS(send.data.send_percent, 254);
    TS_ASSERT(!recv.data.recv_name.isValid());
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    remove("sendfile");
}

void BlockVarTestSuite::TestRecvWildcardFileWildcard()
{
    const int filesize = 19997;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    recv.data.set_block_unmapped_recvs(true);
    recv.data.recv_data("test_*", "recvfile__*");

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    send.data.push_data("test_A431", "sendfile");

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test_A431");
    TS_ASSERT_EQUALS(send.data.send_name, "test_A431");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    f = fopen("recvfile__A431", "rb");
    TS_ASSERT_DIFFERS((void *)f, (void *)0);
    if (f)
    {
        fseek(f, 0, SEEK_END);
        long filesz = ftell(f);
        TS_ASSERT_EQUALS(filesz, filesize);
        fseek(f, 0, SEEK_SET);
        char rbuf[filesize];
        fread(rbuf, 1, filesize, f);
        fclose(f);
        ch = '\0';
        for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
            TS_ASSERT_EQUALS(rbuf[cnt], ch);
        remove("recvfile__A431");
    }
    remove("sendfile");
}

void BlockVarTestSuite::TestPullFileWildcard()
{
    const int filesize = 200195;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    send.data.push_data_on_pull("test_*", "sendfile");
    send.data.set_block_unmapped_sends(true);

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    recv.data.pull_data("test_ARC420", "recvfile");
    mpt_show::refresh();

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test_ARC420");
    TS_ASSERT_EQUALS(send.data.send_name, "test_ARC420");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    f = fopen("recvfile", "rb");
    TS_ASSERT_DIFFERS((void *)f, (void *)0);
    if (f)
    {
        fseek(f, 0, SEEK_END);
        long filesz = ftell(f);
        TS_ASSERT_EQUALS(filesz, filesize);
        fseek(f, 0, SEEK_SET);
        char rbuf[filesize];
        fread(rbuf, 1, filesize, f);
        fclose(f);
        ch = '\0';
        for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
            TS_ASSERT_EQUALS(rbuf[cnt], ch);
        remove("recvfile");
    }
    remove("sendfile");
}

void BlockVarTestSuite::TestPullFileWildcardRejected()
{
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    send.data.push_data_on_pull("test_*", "sendfile");
    send.data.set_block_unmapped_sends(true);

    recv.data.pull_data("test", "recvfile");
    mpt_show::refresh();

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 254);
    TS_ASSERT_EQUALS(send.data.send_percent, 100); // finished sending error
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT(!send.data.send_name.isValid());

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());
}

void BlockVarTestSuite::TestPullFileWildcardFileWildcard()
{
    const int filesize = 211195;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    send.data.push_data_on_pull("test_*", "sendfile__*");
    send.data.set_block_unmapped_sends(true);

    FILE *f = fopen("sendfile__123_567", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    recv.data.pull_data("test_123_567", "recvfile");
    mpt_show::refresh();

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test_123_567");
    TS_ASSERT_EQUALS(send.data.send_name, "test_123_567");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    f = fopen("recvfile", "rb");
    TS_ASSERT_DIFFERS((void *)f, (void *)0);
    if (f)
    {
        fseek(f, 0, SEEK_END);
        long filesz = ftell(f);
        TS_ASSERT_EQUALS(filesz, filesize);
        fseek(f, 0, SEEK_SET);
        char rbuf[filesize];
        fread(rbuf, 1, filesize, f);
        fclose(f);
        ch = '\0';
        for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
            TS_ASSERT_EQUALS(rbuf[cnt], ch);
        remove("recvfile");
    }
    remove("sendfile__123_567");
}

void BlockVarTestSuite::TestMediumFileToFileInitialPacketLost()
{
    const int filesize = 71703;
    block_var_container send, recv;
    send.setDropFactor(1);
    recv.setDropFactor(8);
    send.setBuddy(&recv);
    recv.setBuddy(&send);

    recv.data.recv_data("test", "recvfile");

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    send.data.push_data("test", "sendfile");

    for (int cnt = 0; cnt<5; ++cnt)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }
    send.setDropFactor(8);

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    f = fopen("recvfile", "rb");
    TS_ASSERT_DIFFERS((void *)f, (void *)0);
    if (f)
    {
        fseek(f, 0, SEEK_END);
        long filesz = ftell(f);
        TS_ASSERT_EQUALS(filesz, filesize);
        fseek(f, 0, SEEK_SET);
        char rbuf[filesize];
        fread(rbuf, 1, filesize, f);
        fclose(f);
        ch = '\0';
        for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
            TS_ASSERT_EQUALS(rbuf[cnt], ch);
        remove("recvfile");
    }
    remove("sendfile");
}

void BlockVarTestSuite::TestMediumFileToFileHundredTimes()
{
    block_var_container send, recv;
    send.setDropFactor(8);
    recv.setDropFactor(8);
    send.setBuddy(&recv);
    recv.setBuddy(&send);

    recv.data.recv_data("test*", "recvfile*");

    for (int loopcnt = 0; loopcnt < 100; ++loopcnt)
    {
        const int filesize = 5000 + rand();
        char sendfilename[11];
        char recvfilename[11];
        char trfxname[7];

        sprintf(sendfilename, "sendfile%02d", loopcnt);
        sprintf(recvfilename, "recvfile%02d", loopcnt);
        sprintf(trfxname, "test%02d", loopcnt);

        FILE *f = fopen(sendfilename, "wb");
        char ch = '\0';
        for (int cnt = 0; cnt < filesize; ++cnt, ch += loopcnt)
            fwrite(&ch, 1, 1, f);
        fclose(f);

        send.data.push_data(trfxname, sendfilename);

        for (;;)
        {
            bool transfered = send.transfer();
            transfered |= recv.transfer();
            if (!transfered) break;
        }

        TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
        TS_ASSERT_EQUALS(send.data.send_percent, 100);
        TS_ASSERT_EQUALS(recv.data.recv_name, trfxname);
        TS_ASSERT_EQUALS(send.data.send_name, trfxname);

        TS_ASSERT_EQUALS(recv.data.send_percent, 255);
        TS_ASSERT_EQUALS(send.data.recv_percent, 255);
        TS_ASSERT(!recv.data.send_name.isValid());
        TS_ASSERT(!send.data.recv_name.isValid());

        f = fopen(recvfilename, "rb");
        TS_ASSERT_DIFFERS((void *)f, (void *)0);
        if (f)
        {
            fseek(f, 0, SEEK_END);
            long filesz = ftell(f);
            TS_ASSERT_EQUALS(filesz, filesize);
            fseek(f, 0, SEEK_SET);
            char *rbuf = new char[filesize];
            fread(rbuf, 1, filesize, f);
            fclose(f);
            ch = '\0';
            for (int cnt = 0; cnt < filesize; ++cnt, ch += loopcnt)
                TS_ASSERT_EQUALS(rbuf[cnt], ch);
            remove(recvfilename);
            delete[] rbuf;
        }
        remove(sendfilename);
    }
}

void BlockVarTestSuite::TestTinyFileToFileHundredTimes()
{
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);

    recv.data.recv_data("test*", "recvfile*");

    for (int loopcnt = 0; loopcnt < 100; ++loopcnt)
    {
        const int filesize = 20 + (rand() % 20);
        char sendfilename[11];
        char recvfilename[11];
        char trfxname[7];

        sprintf(sendfilename, "sendfile%02d", loopcnt);
        sprintf(recvfilename, "recvfile%02d", loopcnt);
        sprintf(trfxname, "test%02d", loopcnt);

        FILE *f = fopen(sendfilename, "wb");
        char ch = '\0';
        for (int cnt = 0; cnt < filesize; ++cnt, ch += loopcnt)
            fwrite(&ch, 1, 1, f);
        fclose(f);

        send.data.push_data(trfxname, sendfilename);

        for (;;)
        {
            bool transfered = send.transfer();
            transfered |= recv.transfer();
            if (!transfered) break;
        }

        TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
        TS_ASSERT_EQUALS(send.data.send_percent, 100);
        TS_ASSERT_EQUALS(recv.data.recv_name, trfxname);
        TS_ASSERT_EQUALS(send.data.send_name, trfxname);

        TS_ASSERT_EQUALS(recv.data.send_percent, 255);
        TS_ASSERT_EQUALS(send.data.recv_percent, 255);
        TS_ASSERT(!recv.data.send_name.isValid());
        TS_ASSERT(!send.data.recv_name.isValid());

        f = fopen(recvfilename, "rb");
        TS_ASSERT_DIFFERS((void *)f, (void *)0);
        if (f)
        {
            fseek(f, 0, SEEK_END);
            long filesz = ftell(f);
            TS_ASSERT_EQUALS(filesz, filesize);
            fseek(f, 0, SEEK_SET);
            char *rbuf = new char[filesize];
            fread(rbuf, 1, filesize, f);
            fclose(f);
            ch = '\0';
            for (int cnt = 0; cnt < filesize; ++cnt, ch += loopcnt)
                TS_ASSERT_EQUALS(rbuf[cnt], ch);
            remove(recvfilename);
            delete[] rbuf;
        }
        remove(sendfilename);
    }
}

void BlockVarTestSuite::TestBlockVarInsideUnionVar()
{
    const char teststr[] = "\xFF\1\x2c\x3d\x4f\x50\x61\x72\x83\x94\xa5\xb6\xc7\xd8\xe9";
    const int filesize = 7725;
    block_var_union send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    recv.u.data.recv_data("test", "recvfile");
    send.u.data.set_timeout(3600);
    recv.u.data.set_timeout(3600);
    send.u.data.regulate_sending_by_count(1);
    recv.u.data.regulate_sending_by_count(1);

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    send.u.data.push_data("test", "sendfile");
    // 2nd byte needs to be 1
    send.u.str = teststr;
    send.u.cmplx.dummy = 16;
    // needs to be 2
    send.u.cmplx.selector = 2;
    send.u.cmplx.flt[0] = 1.f;
    send.u.cmplx.flt[1] = 2.f;
    send.u.cmplx.flt[2] = 3.f;
    send.u.cmplx.flt[3] = 4.f;
    send.u.cmplx.flt[4] = 5.f;
    send.u.cmplx.flt[5] = 6.f;
    send.u.cmplx.flt[6] = 7.f;
    send.u.cmplx.flt[7] = 8.f;
    send.u.cmplx.flt[8] = 9.f;
    send.u.cmplx.flt[9] = 10.f;
    send.u.cmplx.dbl[0] = 10.;
    send.u.cmplx.dbl[1] = 11.;
    send.u.cmplx.dbl[2] = 12.;
    send.u.cmplx.dbl[3] = 13.;
    send.u.cmplx.dbl[4] = 14.;
    send.u.cmplx.dbl[5] = 15.;
    send.u.cmplx.dbl[6] = 16.;
    send.u.cmplx.dbl[7] = 17.;
    send.u.cmplx.dbl[8] = 18.;
    send.u.cmplx.dbl[9] = 19.;

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.u.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.u.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.u.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.u.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.u.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.u.data.recv_percent, 255);
    TS_ASSERT(!recv.u.data.send_name.isValid());
    TS_ASSERT(!send.u.data.recv_name.isValid());

    f = fopen("recvfile", "rb");
    TS_ASSERT_DIFFERS((void *)f, (void *)0);
    if (f)
    {
        fseek(f, 0, SEEK_END);
        long filesz = ftell(f);
        TS_ASSERT_EQUALS(filesz, filesize);
        fseek(f, 0, SEEK_SET);
        char rbuf[filesize];
        fread(rbuf, 1, filesize, f);
        fclose(f);
        ch = '\0';
        for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
            TS_ASSERT_EQUALS(rbuf[cnt], ch);
        remove("recvfile");
    }
    remove("sendfile");

    TS_ASSERT_EQUALS(recv.u.str, teststr);
    TS_ASSERT_EQUALS(recv.u.cmplx.dummy, 16);
    TS_ASSERT_EQUALS(recv.u.cmplx.selector, 2);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[0], 1.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[1], 2.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[2], 3.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[3], 4.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[4], 5.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[5], 6.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[6], 7.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[7], 8.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[8], 9.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[9], 10.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[0], 10.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[1], 11.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[2], 12.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[3], 13.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[4], 14.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[5], 15.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[6], 16.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[7], 17.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[8], 18.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[9], 19.);
}

void BlockVarTestSuite::TestBlockVarInsideUnionVarMultiplexTransfer()
{
    char teststr[] = "\xFF\1\x2c\x3d\x4f\x50\x61\x72\x83\x94\xa5\xb6\xc7\xd8\xe9";
    const int filesize = 284953;
    block_var_union send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    recv.u.data.recv_data("test", "recvfile");
    send.u.data.set_timeout(3600);
    recv.u.data.set_timeout(3600);
    send.u.data.regulate_sending_by_count(1);
    recv.u.data.regulate_sending_by_count(1);

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    send.u.data.push_data("test", "sendfile");

    for (int cnt=0; cnt < 20; ++cnt)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    // 2nd byte needs to be 1
    send.u.str = teststr;
    send.u.cmplx.dummy = 16;
    // needs to be 2
    send.u.cmplx.selector = 2;
    send.u.cmplx.flt[0] = 1.f;
    send.u.cmplx.flt[1] = 2.f;
    send.u.cmplx.flt[2] = 3.f;
    send.u.cmplx.flt[3] = 4.f;
    send.u.cmplx.flt[4] = 5.f;
    send.u.cmplx.flt[5] = 6.f;
    send.u.cmplx.flt[6] = 7.f;
    send.u.cmplx.flt[7] = 8.f;
    send.u.cmplx.flt[8] = 9.f;
    send.u.cmplx.flt[9] = 10.f;
    send.u.cmplx.dbl[0] = 10.;
    send.u.cmplx.dbl[1] = 11.;
    send.u.cmplx.dbl[2] = 12.;
    send.u.cmplx.dbl[3] = 13.;
    send.u.cmplx.dbl[4] = 14.;
    send.u.cmplx.dbl[5] = 15.;
    send.u.cmplx.dbl[6] = 16.;
    send.u.cmplx.dbl[7] = 17.;
    send.u.cmplx.dbl[8] = 18.;
    send.u.cmplx.dbl[9] = 19.;

    for (int cnt = 0; cnt < 10; ++cnt)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.u.str, teststr);
    TS_ASSERT_EQUALS(recv.u.cmplx.dummy, 16);
    TS_ASSERT_EQUALS(recv.u.cmplx.selector, 2);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[0], 1.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[1], 2.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[2], 3.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[3], 4.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[4], 5.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[5], 6.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[6], 7.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[7], 8.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[8], 9.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[9], 10.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[0], 10.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[1], 11.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[2], 12.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[3], 13.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[4], 14.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[5], 15.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[6], 16.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[7], 17.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[8], 18.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[9], 19.);

    for (int cnt = 0; cnt < 10; ++cnt)
        teststr[2 + cnt] = static_cast<char>(rand() % 256);
    send.u.str = teststr;
    send.u.cmplx.flt[0] = 11.f;
    send.u.cmplx.flt[1] = 12.f;
    send.u.cmplx.flt[2] = 13.f;
    send.u.cmplx.flt[3] = 14.f;
    send.u.cmplx.flt[4] = 15.f;
    send.u.cmplx.flt[5] = 16.f;
    send.u.cmplx.flt[6] = 17.f;
    send.u.cmplx.flt[7] = 18.f;
    send.u.cmplx.flt[8] = 19.f;
    send.u.cmplx.flt[9] = 20.f;
    send.u.cmplx.dbl[0] = 20.;
    send.u.cmplx.dbl[1] = 21.;
    send.u.cmplx.dbl[2] = 22.;
    send.u.cmplx.dbl[3] = 23.;
    send.u.cmplx.dbl[4] = 24.;
    send.u.cmplx.dbl[5] = 25.;
    send.u.cmplx.dbl[6] = 26.;
    send.u.cmplx.dbl[7] = 27.;
    send.u.cmplx.dbl[8] = 28.;
    send.u.cmplx.dbl[9] = 29.;

    for (int cnt = 0; cnt < 10; ++cnt)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.u.str, teststr);
    TS_ASSERT_EQUALS(recv.u.cmplx.dummy, 16);
    TS_ASSERT_EQUALS(recv.u.cmplx.selector, 2);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[0], 11.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[1], 12.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[2], 13.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[3], 14.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[4], 15.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[5], 16.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[6], 17.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[7], 18.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[8], 19.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[9], 20.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[0], 20.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[1], 21.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[2], 22.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[3], 23.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[4], 24.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[5], 25.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[6], 26.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[7], 27.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[8], 28.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[9], 29.);

    for (int cnt = 0; cnt < 10; ++cnt)
        teststr[2 + cnt] = static_cast<char>(rand() % 256);
    send.u.str = teststr;
    send.u.cmplx.flt[0] = 31.f;
    send.u.cmplx.flt[1] = 32.f;
    send.u.cmplx.flt[2] = 33.f;
    send.u.cmplx.flt[3] = 34.f;
    send.u.cmplx.flt[4] = 35.f;
    send.u.cmplx.flt[5] = 36.f;
    send.u.cmplx.flt[6] = 37.f;
    send.u.cmplx.flt[7] = 38.f;
    send.u.cmplx.flt[8] = 39.f;
    send.u.cmplx.flt[9] = 40.f;
    send.u.cmplx.dbl[0] = 40.;
    send.u.cmplx.dbl[1] = 41.;
    send.u.cmplx.dbl[2] = 42.;
    send.u.cmplx.dbl[3] = 43.;
    send.u.cmplx.dbl[4] = 44.;
    send.u.cmplx.dbl[5] = 45.;
    send.u.cmplx.dbl[6] = 46.;
    send.u.cmplx.dbl[7] = 47.;
    send.u.cmplx.dbl[8] = 48.;
    send.u.cmplx.dbl[9] = 49.;

    for (int cnt = 0; cnt < 10; ++cnt)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.u.str, teststr);
    TS_ASSERT_EQUALS(recv.u.cmplx.dummy, 16);
    TS_ASSERT_EQUALS(recv.u.cmplx.selector, 2);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[0], 31.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[1], 32.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[2], 33.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[3], 34.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[4], 35.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[5], 36.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[6], 37.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[7], 38.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[8], 39.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.flt[9], 40.f);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[0], 40.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[1], 41.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[2], 42.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[3], 43.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[4], 44.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[5], 45.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[6], 46.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[7], 47.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[8], 48.);
    TS_ASSERT_EQUALS(recv.u.cmplx.dbl[9], 49.);

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.u.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.u.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.u.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.u.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.u.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.u.data.recv_percent, 255);
    TS_ASSERT(!recv.u.data.send_name.isValid());
    TS_ASSERT(!send.u.data.recv_name.isValid());

    f = fopen("recvfile", "rb");
    TS_ASSERT_DIFFERS((void *)f, (void *)0);
    if (f)
    {
        fseek(f, 0, SEEK_END);
        long filesz = ftell(f);
        TS_ASSERT_EQUALS(filesz, filesize);
        fseek(f, 0, SEEK_SET);
        char rbuf[filesize];
        fread(rbuf, 1, filesize, f);
        fclose(f);
        ch = '\0';
        for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
            TS_ASSERT_EQUALS(rbuf[cnt], ch);
        remove("recvfile");
    }
    remove("sendfile");
}

void BlockVarTestSuite::TestShow()
{
    const int filesize = 97220;
    block_var_container send, recv;
    recv.setDropFactor(5);
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    dirty_checker s_checker(&send.data.send_percent);
    dirty_checker r_checker(&recv.data.recv_percent);
    dirty_checker s_checker1(&send.data);
    dirty_checker r_checker1(&recv.data);
    unsigned char s_last_val = 0;
    unsigned char r_last_val = 255;

    recv.data.recv_data("test", "recvfile");

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    TS_ASSERT_EQUALS(recv.data.recv_percent, 255);
    TS_ASSERT_EQUALS(send.data.send_percent, 255);
    TS_ASSERT(!recv.data.recv_name.isValid());
    TS_ASSERT(!send.data.send_name.isValid());
    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    send.data.push_data("test", "sendfile");

    TS_ASSERT_EQUALS(send.data.send_percent, 0);
    TS_ASSERT_EQUALS(send.data.send_name, "test");
    TS_ASSERT_EQUALS(s_checker.getStateAndReset(), 1);

    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();

        const unsigned char s = send.data.send_percent;
        const unsigned char r = recv.data.recv_percent;
        if (s == s_last_val)
        {
            const int ret = s_checker.getStateAndReset();
            TS_ASSERT(ret == 2 || ret == 0);
        }
        else
        {
            TS_ASSERT_EQUALS(s_checker.getStateAndReset(), 1);
            s_last_val = s;
        }

        if (r == r_last_val)
        {
            const int ret = r_checker.getStateAndReset();
            TS_ASSERT(ret == 2 || ret == 0);
        }
        else
        {
            TS_ASSERT_EQUALS(r_checker.getStateAndReset(), 1);
            r_last_val = r;
        }

        if (!transfered) break;

        TS_ASSERT_EQUALS(s_checker1.getStateAndReset(), 1);
        TS_ASSERT_EQUALS(r_checker1.getStateAndReset(), 1);
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent, 100);
    TS_ASSERT_EQUALS(send.data.send_percent, 100);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT_EQUALS(send.data.send_name, "test");

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    f = fopen("recvfile", "rb");
    TS_ASSERT_DIFFERS((void *)f, (void *)0);
    if (f)
    {
        fseek(f, 0, SEEK_END);
        long filesz = ftell(f);
        TS_ASSERT_EQUALS(filesz, filesize);
        fseek(f, 0, SEEK_SET);
        char rbuf[filesize];
        fread(rbuf, 1, filesize, f);
        fclose(f);
        ch = '\0';
        for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
            TS_ASSERT_EQUALS(rbuf[cnt], ch);
        remove("recvfile");
    }
    remove("sendfile");
}

void BlockVarTestSuite::TestAbortSender()
{
    const int filesize = 13305;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    recv.data.recv_data("test", "recvfile");
    recv.data.set_timeout(5);
    send.data.set_timeout(5);

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    send.data.push_data("test", "sendfile");

    for (int cnt = 0; cnt<5; ++cnt)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }
    send.setInvalid();
    time_t outage = time(0);
    for (; time(0) - outage < 8;)
    {
        send.transfer();
        recv.transfer();
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent.to_int(), 254);
    TS_ASSERT_EQUALS(send.data.send_percent.to_int(), 255);
    TS_ASSERT_EQUALS(recv.data.recv_name, "test");
    TS_ASSERT(!send.data.send_name.isValid());

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    remove("recvfile");
    remove("sendfile");
}

void BlockVarTestSuite::TestAbortRecv()
{
    const int filesize = 6201;
    block_var_container send, recv;
    send.setBuddy(&recv);
    recv.setBuddy(&send);
    recv.data.recv_data("test", "recvfile");
    recv.data.set_timeout(5);
    send.data.set_timeout(5);

    FILE *f = fopen("sendfile", "wb");
    char ch = '\0';
    for (int cnt = 0; cnt < filesize; ++cnt, ++ch)
        fwrite(&ch, 1, 1, f);
    fclose(f);

    send.data.push_data("test", "sendfile");

    for (int cnt = 0; cnt<5; ++cnt)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }
    recv.setInvalid();
    for (;;)
    {
        bool transfered = send.transfer();
        transfered |= recv.transfer();
        if (!transfered) break;
    }

    TS_ASSERT_EQUALS(recv.data.recv_percent.to_int(), 255);
    TS_ASSERT_EQUALS(send.data.send_percent.to_int(), 254);
    TS_ASSERT_EQUALS(send.data.send_name, "test");
    TS_ASSERT(!recv.data.recv_name.isValid());

    TS_ASSERT_EQUALS(recv.data.send_percent, 255);
    TS_ASSERT_EQUALS(send.data.recv_percent, 255);
    TS_ASSERT(!recv.data.send_name.isValid());
    TS_ASSERT(!send.data.recv_name.isValid());

    remove("recvfile");
    remove("sendfile");
}

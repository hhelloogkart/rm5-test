/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_frame_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_FrameVarTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_frame_var\test_frame_var.h"

static FrameVarTestSuite suite_FrameVarTestSuite;

static CxxTest::List Tests_FrameVarTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_FrameVarTestSuite( "test_frame_var/test_frame_var.h", 6, "FrameVarTestSuite", suite_FrameVarTestSuite, Tests_FrameVarTestSuite );

static class TestDescription_suite_FrameVarTestSuite_testWrite : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FrameVarTestSuite_testWrite() : CxxTest::RealTestDescription( Tests_FrameVarTestSuite, suiteDescription_FrameVarTestSuite, 9, "testWrite" ) {}
 void runTest() { suite_FrameVarTestSuite.testWrite(); }
} testDescription_suite_FrameVarTestSuite_testWrite;

static class TestDescription_suite_FrameVarTestSuite_testStream : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FrameVarTestSuite_testStream() : CxxTest::RealTestDescription( Tests_FrameVarTestSuite, suiteDescription_FrameVarTestSuite, 10, "testStream" ) {}
 void runTest() { suite_FrameVarTestSuite.testStream(); }
} testDescription_suite_FrameVarTestSuite_testStream;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

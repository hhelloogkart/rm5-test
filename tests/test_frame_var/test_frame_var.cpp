#include <sstream>
#include <stdio.h>
#include <defutil.h>
#include <rm_var.h>
#include <rminterf.h>
#include <gen_var.h>
#include <frame_var.h>
#include <time/timecore.h>

#include "test_frame_var.h"


struct f1_typ : public frame_var
{
    FRAMECONST( f1_typ, 6 )
    //1 sub frame, data size == multiple of frame size
    generic_var<int> a;
};

struct f2_typ : public frame_var
{
    FRAMECONST( f2_typ, 6 )

    //1 sub frame, data size != multiple of frame size
    generic_var<unsigned short> a;
};

struct f3_typ : public frame_var
{
    FRAMECONST( f3_typ, 6 )

    //2 sub frame, data size != multiple of frame size
    generic_var<int> a;
    generic_var<unsigned short> b;
};

struct f4_typ : public frame_var
{
    FRAMECONST( f4_typ, 6 )

    //255 sub frame, data size == multiple of frame size
    generic_var<int> a[255];
};

struct f5_typ : public frame_var
{
    FRAMECONST( f5_typ, 6 )

    //255 sub frame, data size != multiple of frame size
    generic_var<int> a[254];
    generic_var<unsigned short> b;
};

struct test_wr1_typ : public rm_var
{
    RMCONST( test_wr1_typ, "test_wr1", false );
    f1_typ f1;
};

struct test_wr2_typ : public rm_var
{
    RMCONST( test_wr2_typ, "test_wr2", false );
    f2_typ f2;
};

struct test_wr3_typ : public rm_var
{
    RMCONST( test_wr3_typ, "test_wr3", false );
    f3_typ f3;
};

struct test_wr4_typ : public rm_var
{
    RMCONST( test_wr4_typ, "test_wr4", false );
    f4_typ f4;
};

struct test_wr5_typ : public rm_var
{
    RMCONST( test_wr5_typ, "test_wr5", false );
    f5_typ f5;
};

struct interface_typ : public rm_interface
{
    RMINTERFACECONST(interface_typ)
    test_wr1_typ test_wr1_var;
    test_wr2_typ test_wr2_var;
    test_wr3_typ test_wr3_var;
    test_wr4_typ test_wr4_var;
    test_wr5_typ test_wr5_var;
};
interface_typ interfaces[2];

void FrameVarTestSuite::testWrite(void)
{
    interfaces[0].test_wr1_var.setRegister(false);
    interfaces[0].test_wr2_var.setRegister(false);
    interfaces[0].test_wr3_var.setRegister(false);
    interfaces[0].test_wr4_var.setRegister(false);
    interfaces[0].test_wr5_var.setRegister(false);
    interfaces[1].test_wr1_var.setRegister(true);
    interfaces[1].test_wr2_var.setRegister(true);
    interfaces[1].test_wr3_var.setRegister(true);
    interfaces[1].test_wr4_var.setRegister(true);
    interfaces[1].test_wr5_var.setRegister(true);
    clearData();

    //connect to RM
    const char settings[] =
        "client = RMSCRIPT\n"
        "host = RM5\n"
        "port = 0\n"
        "access = SM\n"
        "flow = 0\n";
    rmclient_init init(settings);

    setData();

    //send data to RM
    while(interfaces[1].test_wr5_var.f5.b != 5U)
    {
        init.incoming();
        mpt_show::refresh();
        init.outgoing();
    }

    checkData();
}

void FrameVarTestSuite::testStream(void)
{
    setData();
    std::ostringstream os1;
    std::ostringstream os2;
    std::ostringstream os3;
    std::ostringstream os4;
    std::ostringstream os5;

    os1 << interfaces[0].test_wr1_var << std::ends;
    os2 << interfaces[0].test_wr2_var << std::ends;
    os3 << interfaces[0].test_wr3_var << std::ends;
    os4 << interfaces[0].test_wr4_var << std::ends;
    os5 << interfaces[0].test_wr5_var << std::ends;

    clearData();
    std::istringstream is1(os1.str());
    std::istringstream is2(os2.str());
    std::istringstream is3(os3.str());
    std::istringstream is4(os4.str());
    std::istringstream is5(os5.str());

    is1 >> interfaces[1].test_wr1_var;
    is2 >> interfaces[1].test_wr2_var;
    is3 >> interfaces[1].test_wr3_var;
    is4 >> interfaces[1].test_wr4_var;
    is5 >> interfaces[1].test_wr5_var;
    checkData();
}

void FrameVarTestSuite::setData(void)
{
    int i;

    //set data
    interfaces[0].test_wr1_var.f1.a = 1;

    interfaces[0].test_wr2_var.f2.a = 2U;

    interfaces[0].test_wr3_var.f3.a = 3;
    interfaces[0].test_wr3_var.f3.b = 4U;

    for(i = 1; i <= 255; ++i)
        interfaces[0].test_wr4_var.f4.a[i-1] = i;

    for(i = 1; i <= 254; ++i)
        interfaces[0].test_wr5_var.f5.a[i-1] = i;
    interfaces[0].test_wr5_var.f5.b = 5U;
}

void FrameVarTestSuite::clearData(void)
{
    interfaces[1].test_wr1_var.setInvalid();
    interfaces[1].test_wr2_var.setInvalid();
    interfaces[1].test_wr3_var.setInvalid();
    interfaces[1].test_wr4_var.setInvalid();
    interfaces[1].test_wr5_var.setInvalid();
}

void FrameVarTestSuite::checkData(void)
{
    int i;

    //test data rx correctly
    TS_ASSERT(interfaces[1].test_wr1_var.f1.a == 1);
    TS_ASSERT(interfaces[1].test_wr2_var.f2.a == (unsigned short)2U);
    TS_ASSERT(interfaces[1].test_wr3_var.f3.a == 3);
    TS_ASSERT(interfaces[1].test_wr3_var.f3.b == (unsigned short)4U);

    for(i = 1; i <= 255; ++i)
    {
        TS_ASSERT(interfaces[1].test_wr4_var.f4.a[i-1] == i);
    }

    for(i = 1; i <= 254; ++i)
    {
        TS_ASSERT(interfaces[1].test_wr5_var.f5.a[i-1] == i);
    }
    TS_ASSERT(interfaces[1].test_wr5_var.f5.b == (unsigned short)5U);
}

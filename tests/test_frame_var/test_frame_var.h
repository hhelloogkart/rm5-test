#ifndef _TEST_FRAME_VAR_H_DEF_
#define _TEST_FRAME_VAR_H_DEF_

#include <cxxtest/TestSuite.h>

class FrameVarTestSuite : public CxxTest::TestSuite 
{
public:
    void testWrite(void);
    void testStream(void);

private:
    void setData();
    void clearData();
    void checkData(void);
};

#endif


#ifndef _TEST_MSG_VAR_H_DEF_
#define _TEST_MSG_VAR_H_DEF_

#include <cxxtest/TestSuite.h>
#include <cxxtest/GlobalFixture.h>

class rmclient_init;

class MsgVarTestSuiteFixture : public CxxTest::GlobalFixture
{
public:
    bool setUpWorld();
    bool tearDownWorld();
};

class MsgVarTestSuite : public CxxTest::TestSuite
{
public:
    void testRecv();
    void testSend();
};

#endif

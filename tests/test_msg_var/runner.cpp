/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_msg_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_MsgVarTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_msg_var\test_msg_var.h"

static MsgVarTestSuite suite_MsgVarTestSuite;

static CxxTest::List Tests_MsgVarTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_MsgVarTestSuite( "test_msg_var/test_msg_var.h", 16, "MsgVarTestSuite", suite_MsgVarTestSuite, Tests_MsgVarTestSuite );

static class TestDescription_suite_MsgVarTestSuite_testRecv : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_MsgVarTestSuite_testRecv() : CxxTest::RealTestDescription( Tests_MsgVarTestSuite, suiteDescription_MsgVarTestSuite, 19, "testRecv" ) {}
 void runTest() { suite_MsgVarTestSuite.testRecv(); }
} testDescription_suite_MsgVarTestSuite_testRecv;

static class TestDescription_suite_MsgVarTestSuite_testSend : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_MsgVarTestSuite_testSend() : CxxTest::RealTestDescription( Tests_MsgVarTestSuite, suiteDescription_MsgVarTestSuite, 20, "testSend" ) {}
 void runTest() { suite_MsgVarTestSuite.testSend(); }
} testDescription_suite_MsgVarTestSuite_testSend;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_decode_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_DecodeVarTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_decode_var\test_decode_var.h"

static DecodeVarTestSuite suite_DecodeVarTestSuite;

static CxxTest::List Tests_DecodeVarTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_DecodeVarTestSuite( "test_decode_var/test_decode_var.h", 6, "DecodeVarTestSuite", suite_DecodeVarTestSuite, Tests_DecodeVarTestSuite );

static class TestDescription_suite_DecodeVarTestSuite_testAssign : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_DecodeVarTestSuite_testAssign() : CxxTest::RealTestDescription( Tests_DecodeVarTestSuite, suiteDescription_DecodeVarTestSuite, 10, "testAssign" ) {}
 void runTest() { suite_DecodeVarTestSuite.testAssign(); }
} testDescription_suite_DecodeVarTestSuite_testAssign;

static class TestDescription_suite_DecodeVarTestSuite_testStream : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_DecodeVarTestSuite_testStream() : CxxTest::RealTestDescription( Tests_DecodeVarTestSuite, suiteDescription_DecodeVarTestSuite, 11, "testStream" ) {}
 void runTest() { suite_DecodeVarTestSuite.testStream(); }
} testDescription_suite_DecodeVarTestSuite_testStream;

static class TestDescription_suite_DecodeVarTestSuite_testExtract : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_DecodeVarTestSuite_testExtract() : CxxTest::RealTestDescription( Tests_DecodeVarTestSuite, suiteDescription_DecodeVarTestSuite, 12, "testExtract" ) {}
 void runTest() { suite_DecodeVarTestSuite.testExtract(); }
} testDescription_suite_DecodeVarTestSuite_testExtract;

static class TestDescription_suite_DecodeVarTestSuite_testExtract2 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_DecodeVarTestSuite_testExtract2() : CxxTest::RealTestDescription( Tests_DecodeVarTestSuite, suiteDescription_DecodeVarTestSuite, 13, "testExtract2" ) {}
 void runTest() { suite_DecodeVarTestSuite.testExtract2(); }
} testDescription_suite_DecodeVarTestSuite_testExtract2;

static class TestDescription_suite_DecodeVarTestSuite_testExtractWithTemplatedDecodeVars : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_DecodeVarTestSuite_testExtractWithTemplatedDecodeVars() : CxxTest::RealTestDescription( Tests_DecodeVarTestSuite, suiteDescription_DecodeVarTestSuite, 14, "testExtractWithTemplatedDecodeVars" ) {}
 void runTest() { suite_DecodeVarTestSuite.testExtractWithTemplatedDecodeVars(); }
} testDescription_suite_DecodeVarTestSuite_testExtractWithTemplatedDecodeVars;

static class TestDescription_suite_DecodeVarTestSuite_testBitFieldHandling : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_DecodeVarTestSuite_testBitFieldHandling() : CxxTest::RealTestDescription( Tests_DecodeVarTestSuite, suiteDescription_DecodeVarTestSuite, 15, "testBitFieldHandling" ) {}
 void runTest() { suite_DecodeVarTestSuite.testBitFieldHandling(); }
} testDescription_suite_DecodeVarTestSuite_testBitFieldHandling;

static class TestDescription_suite_DecodeVarTestSuite_testMisc : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_DecodeVarTestSuite_testMisc() : CxxTest::RealTestDescription( Tests_DecodeVarTestSuite, suiteDescription_DecodeVarTestSuite, 16, "testMisc" ) {}
 void runTest() { suite_DecodeVarTestSuite.testMisc(); }
} testDescription_suite_DecodeVarTestSuite_testMisc;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

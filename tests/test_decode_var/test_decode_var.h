#ifndef _TEST_DECODE_VAR_H_DEF_
#define _TEST_DECODE_VAR_H_DEF_

#include <cxxtest/TestSuite.h>

class DecodeVarTestSuite : public CxxTest::TestSuite
{
public:
    DecodeVarTestSuite();
    void testAssign(void);
    void testStream(void);
    void testExtract(void);
    void testExtract2(void);
    void testExtractWithTemplatedDecodeVars();
    void testBitFieldHandling();
    void testMisc();
};

#endif


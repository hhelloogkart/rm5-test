#include <cplx_var.h>
#include <decode_var.h>
#include <rdecode_var.h>
#include <decode_bit_var.h>
#include <rdecode_bit_var.h>
#include <decode_complement_var.h>
#include <prefiximpl.h>
#include <bit_var.h>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>
#include <limits.h>
#include "test_decode_var.h"

#define NELEM(a) (sizeof(a)/sizeof(a[0]))

#define MIN_VAL	-180
#define MAX_VAL	180
#define INC_VAL	0.7
#define STREAM_ERR 0.01

#define DEFINE_TEMPL_DECODE_VAR(t,sz,rg,os) \
struct t##_s \
{ \
    static const int SIZE = sz; \
    static const int SCALE_OFFSET = 1; \
    static double RANGE; \
    static double OFFSET; \
}; \
double t##_s::RANGE = rg; \
double t##_s::OFFSET = os; \
templ_decode_var<t##_s> t[2]; \
templ_rdecode_var<t##_s> t##_r[2]; \
struct t##_s2 \
{ \
    static const int SIZE = sz; \
    static const int SCALE_OFFSET = 1; \
    static double RANGE; \
    static double OFFSET; \
}; \
double t##_s2::RANGE = 360.0; \
double t##_s2::OFFSET = 0; \
templ_decode_complement_var<t##_s2> t##_c[2];

DEFINE_TEMPL_DECODE_VAR(i8,-1,360.0,0)
DEFINE_TEMPL_DECODE_VAR(u8,1,360.0,-180.)
DEFINE_TEMPL_DECODE_VAR(i16,-2,360.0,0)
DEFINE_TEMPL_DECODE_VAR(u16,2,360.0,-180.)
DEFINE_TEMPL_DECODE_VAR(i24,-3,360.0,0)
DEFINE_TEMPL_DECODE_VAR(u24,3,360.0,-180.)
DEFINE_TEMPL_DECODE_VAR(i32,-4,360.0,0)
DEFINE_TEMPL_DECODE_VAR(u32,4,360.0,-180.)

#define DEFINE_TEMPL_DECODE_BIT_VAR(t,sz,ws,rg,os) \
struct t##_s \
{ \
    static const int SIZE = sz; \
    static const int WORD_SIZE = ws; \
    static const int SCALE_OFFSET = 1; \
    static double RANGE; \
    static double OFFSET; \
}; \
double t##_s::RANGE = rg; \
double t##_s::OFFSET = os; \
templ_decode_bit_var<t##_s> t##dbv[2]; \
templ_rdecode_bit_var<t##_s> t##rbv[2]; \
templw_decode_bit_var<t##_s> t##wdbv[2]; \
templw_rdecode_bit_var<t##_s> t##wrbv[2];

DEFINE_TEMPL_DECODE_BIT_VAR(sbit1,-1,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit1,1,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit2,-2,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit2,2,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit3,-3,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit3,3,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit4,-4,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit4,4,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit5,-5,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit5,5,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit6,-6,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit6,6,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit7,-7,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit7,7,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit8,-8,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit8,8,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit9,-9,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit9,9,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit10,-10,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit10,10,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit11,-11,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit11,11,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit12,-12,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit12,12,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit13,-13,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit13,13,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit14,-14,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit14,14,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit15,-15,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit15,15,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit16,-16,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit16,16,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit17,-17,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit17,17,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit18,-18,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit18,18,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit19,-19,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit19,19,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit20,-20,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit20,20,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit21,-21,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit21,21,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit22,-22,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit22,22,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit23,-23,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit23,23,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit24,-24,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit24,24,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit25,-25,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit25,25,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit26,-26,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit26,26,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit27,-27,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit27,27,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit28,-28,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit28,28,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit29,-29,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit29,29,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit30,-30,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit30,30,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit31,-31,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit31,31,6,360.0,-180.)
DEFINE_TEMPL_DECODE_BIT_VAR(sbit32,-32,6,360.0,0)
DEFINE_TEMPL_DECODE_BIT_VAR(ubit32,32,6,360.0,-180.)

vdecode_var *var[] =
{
    &i8[0],
    &u8[0],
    &i16[0],
    &u16[0],
    &i24[0],
    &u24[0],
    &i32[0],
    &u32[0],
    &i8_r[0],
    &u8_r[0],
    &i16_r[0],
    &u16_r[0],
    &i24_r[0],
    &u24_r[0],
    &i32_r[0],
    &u32_r[0],
    &i8_c[0],
    &i16_c[0],
    &i24_c[0],
    &i32_c[0],
    &sbit1dbv[0],
    &sbit2dbv[0],
    &sbit3dbv[0],
    &sbit4dbv[0],
    &sbit5dbv[0],
    &sbit6dbv[0],
    &sbit7dbv[0],
    &sbit8dbv[0],
    &sbit9dbv[0],
    &sbit10dbv[0],
    &sbit11dbv[0],
    &sbit12dbv[0],
    &sbit13dbv[0],
    &sbit14dbv[0],
    &sbit15dbv[0],
    &sbit16dbv[0],
    &sbit17dbv[0],
    &sbit18dbv[0],
    &sbit19dbv[0],
    &sbit20dbv[0],
    &sbit21dbv[0],
    &sbit22dbv[0],
    &sbit23dbv[0],
    &sbit24dbv[0],
    &sbit25dbv[0],
    &sbit26dbv[0],
    &sbit27dbv[0],
    &sbit28dbv[0],
    &sbit29dbv[0],
    &sbit30dbv[0],
    &sbit31dbv[0],
    &sbit32dbv[0],
    &sbit1rbv[0],
    &sbit2rbv[0],
    &sbit3rbv[0],
    &sbit4rbv[0],
    &sbit5rbv[0],
    &sbit6rbv[0],
    &sbit7rbv[0],
    &sbit8rbv[0],
    &sbit9rbv[0],
    &sbit10rbv[0],
    &sbit11rbv[0],
    &sbit12rbv[0],
    &sbit13rbv[0],
    &sbit14rbv[0],
    &sbit15rbv[0],
    &sbit16rbv[0],
    &sbit17rbv[0],
    &sbit18rbv[0],
    &sbit19rbv[0],
    &sbit20rbv[0],
    &sbit21rbv[0],
    &sbit22rbv[0],
    &sbit23rbv[0],
    &sbit24rbv[0],
    &sbit25rbv[0],
    &sbit26rbv[0],
    &sbit27rbv[0],
    &sbit28rbv[0],
    &sbit29rbv[0],
    &sbit30rbv[0],
    &sbit31rbv[0],
    &sbit32rbv[0],
    &sbit1wdbv[0],
    &sbit2wdbv[0],
    &sbit3wdbv[0],
    &sbit4wdbv[0],
    &sbit5wdbv[0],
    &sbit6wdbv[0],
    &sbit7wdbv[0],
    &sbit8wdbv[0],
    &sbit9wdbv[0],
    &sbit10wdbv[0],
    &sbit11wdbv[0],
    &sbit12wdbv[0],
    &sbit13wdbv[0],
    &sbit14wdbv[0],
    &sbit15wdbv[0],
    &sbit16wdbv[0],
    &sbit17wdbv[0],
    &sbit18wdbv[0],
    &sbit19wdbv[0],
    &sbit20wdbv[0],
    &sbit21wdbv[0],
    &sbit22wdbv[0],
    &sbit23wdbv[0],
    &sbit24wdbv[0],
    &sbit25wdbv[0],
    &sbit26wdbv[0],
    &sbit27wdbv[0],
    &sbit28wdbv[0],
    &sbit29wdbv[0],
    &sbit30wdbv[0],
    &sbit31wdbv[0],
    &sbit32wdbv[0],
    &sbit1wrbv[0],
    &sbit2wrbv[0],
    &sbit3wrbv[0],
    &sbit4wrbv[0],
    &sbit5wrbv[0],
    &sbit6wrbv[0],
    &sbit7wrbv[0],
    &sbit8wrbv[0],
    &sbit9wrbv[0],
    &sbit10wrbv[0],
    &sbit11wrbv[0],
    &sbit12wrbv[0],
    &sbit13wrbv[0],
    &sbit14wrbv[0],
    &sbit15wrbv[0],
    &sbit16wrbv[0],
    &sbit17wrbv[0],
    &sbit18wrbv[0],
    &sbit19wrbv[0],
    &sbit20wrbv[0],
    &sbit21wrbv[0],
    &sbit22wrbv[0],
    &sbit23wrbv[0],
    &sbit24wrbv[0],
    &sbit25wrbv[0],
    &sbit26wrbv[0],
    &sbit27wrbv[0],
    &sbit28wrbv[0],
    &sbit29wrbv[0],
    &sbit30wrbv[0],
    &sbit31wrbv[0],
    &sbit32wrbv[0],
    &ubit1dbv[0],
    &ubit2dbv[0],
    &ubit3dbv[0],
    &ubit4dbv[0],
    &ubit5dbv[0],
    &ubit6dbv[0],
    &ubit7dbv[0],
    &ubit8dbv[0],
    &ubit9dbv[0],
    &ubit10dbv[0],
    &ubit11dbv[0],
    &ubit12dbv[0],
    &ubit13dbv[0],
    &ubit14dbv[0],
    &ubit15dbv[0],
    &ubit16dbv[0],
    &ubit17dbv[0],
    &ubit18dbv[0],
    &ubit19dbv[0],
    &ubit20dbv[0],
    &ubit21dbv[0],
    &ubit22dbv[0],
    &ubit23dbv[0],
    &ubit24dbv[0],
    &ubit25dbv[0],
    &ubit26dbv[0],
    &ubit27dbv[0],
    &ubit28dbv[0],
    &ubit29dbv[0],
    &ubit30dbv[0],
    &ubit31dbv[0],
    &ubit32dbv[0],
    &ubit1rbv[0],
    &ubit2rbv[0],
    &ubit3rbv[0],
    &ubit4rbv[0],
    &ubit5rbv[0],
    &ubit6rbv[0],
    &ubit7rbv[0],
    &ubit8rbv[0],
    &ubit9rbv[0],
    &ubit10rbv[0],
    &ubit11rbv[0],
    &ubit12rbv[0],
    &ubit13rbv[0],
    &ubit14rbv[0],
    &ubit15rbv[0],
    &ubit16rbv[0],
    &ubit17rbv[0],
    &ubit18rbv[0],
    &ubit19rbv[0],
    &ubit20rbv[0],
    &ubit21rbv[0],
    &ubit22rbv[0],
    &ubit23rbv[0],
    &ubit24rbv[0],
    &ubit25rbv[0],
    &ubit26rbv[0],
    &ubit27rbv[0],
    &ubit28rbv[0],
    &ubit29rbv[0],
    &ubit30rbv[0],
    &ubit31rbv[0],
    &ubit32rbv[0],
    &ubit1wdbv[0],
    &ubit2wdbv[0],
    &ubit3wdbv[0],
    &ubit4wdbv[0],
    &ubit5wdbv[0],
    &ubit6wdbv[0],
    &ubit7wdbv[0],
    &ubit8wdbv[0],
    &ubit9wdbv[0],
    &ubit10wdbv[0],
    &ubit11wdbv[0],
    &ubit12wdbv[0],
    &ubit13wdbv[0],
    &ubit14wdbv[0],
    &ubit15wdbv[0],
    &ubit16wdbv[0],
    &ubit17wdbv[0],
    &ubit18wdbv[0],
    &ubit19wdbv[0],
    &ubit20wdbv[0],
    &ubit21wdbv[0],
    &ubit22wdbv[0],
    &ubit23wdbv[0],
    &ubit24wdbv[0],
    &ubit25wdbv[0],
    &ubit26wdbv[0],
    &ubit27wdbv[0],
    &ubit28wdbv[0],
    &ubit29wdbv[0],
    &ubit30wdbv[0],
    &ubit31wdbv[0],
    &ubit32wdbv[0],
    &ubit1wrbv[0],
    &ubit2wrbv[0],
    &ubit3wrbv[0],
    &ubit4wrbv[0],
    &ubit5wrbv[0],
    &ubit6wrbv[0],
    &ubit7wrbv[0],
    &ubit8wrbv[0],
    &ubit9wrbv[0],
    &ubit10wrbv[0],
    &ubit11wrbv[0],
    &ubit12wrbv[0],
    &ubit13wrbv[0],
    &ubit14wrbv[0],
    &ubit15wrbv[0],
    &ubit16wrbv[0],
    &ubit17wrbv[0],
    &ubit18wrbv[0],
    &ubit19wrbv[0],
    &ubit20wrbv[0],
    &ubit21wrbv[0],
    &ubit22wrbv[0],
    &ubit23wrbv[0],
    &ubit24wrbv[0],
    &ubit25wrbv[0],
    &ubit26wrbv[0],
    &ubit27wrbv[0],
    &ubit28wrbv[0],
    &ubit29wrbv[0],
    &ubit30wrbv[0],
    &ubit31wrbv[0],
    &ubit32wrbv[0]
};

vdecode_var *mirror[] =
{
    &i8[1],
    &u8[1],
    &i16[1],
    &u16[1],
    &i24[1],
    &u24[1],
    &i32[1],
    &u32[1],
    &i8_r[1],
    &u8_r[1],
    &i16_r[1],
    &u16_r[1],
    &i24_r[1],
    &u24_r[1],
    &i32_r[1],
    &u32_r[1],
    &i8_c[1],
    &i16_c[1],
    &i24_c[1],
    &i32_c[1],
    &sbit1dbv[1],
    &sbit2dbv[1],
    &sbit3dbv[1],
    &sbit4dbv[1],
    &sbit5dbv[1],
    &sbit6dbv[1],
    &sbit7dbv[1],
    &sbit8dbv[1],
    &sbit9dbv[1],
    &sbit10dbv[1],
    &sbit11dbv[1],
    &sbit12dbv[1],
    &sbit13dbv[1],
    &sbit14dbv[1],
    &sbit15dbv[1],
    &sbit16dbv[1],
    &sbit17dbv[1],
    &sbit18dbv[1],
    &sbit19dbv[1],
    &sbit20dbv[1],
    &sbit21dbv[1],
    &sbit22dbv[1],
    &sbit23dbv[1],
    &sbit24dbv[1],
    &sbit25dbv[1],
    &sbit26dbv[1],
    &sbit27dbv[1],
    &sbit28dbv[1],
    &sbit29dbv[1],
    &sbit30dbv[1],
    &sbit31dbv[1],
    &sbit32dbv[1],
    &sbit1rbv[1],
    &sbit2rbv[1],
    &sbit3rbv[1],
    &sbit4rbv[1],
    &sbit5rbv[1],
    &sbit6rbv[1],
    &sbit7rbv[1],
    &sbit8rbv[1],
    &sbit9rbv[1],
    &sbit10rbv[1],
    &sbit11rbv[1],
    &sbit12rbv[1],
    &sbit13rbv[1],
    &sbit14rbv[1],
    &sbit15rbv[1],
    &sbit16rbv[1],
    &sbit17rbv[1],
    &sbit18rbv[1],
    &sbit19rbv[1],
    &sbit20rbv[1],
    &sbit21rbv[1],
    &sbit22rbv[1],
    &sbit23rbv[1],
    &sbit24rbv[1],
    &sbit25rbv[1],
    &sbit26rbv[1],
    &sbit27rbv[1],
    &sbit28rbv[1],
    &sbit29rbv[1],
    &sbit30rbv[1],
    &sbit31rbv[1],
    &sbit32rbv[1],
    &sbit1wdbv[1],
    &sbit2wdbv[1],
    &sbit3wdbv[1],
    &sbit4wdbv[1],
    &sbit5wdbv[1],
    &sbit6wdbv[1],
    &sbit7wdbv[1],
    &sbit8wdbv[1],
    &sbit9wdbv[1],
    &sbit10wdbv[1],
    &sbit11wdbv[1],
    &sbit12wdbv[1],
    &sbit13wdbv[1],
    &sbit14wdbv[1],
    &sbit15wdbv[1],
    &sbit16wdbv[1],
    &sbit17wdbv[1],
    &sbit18wdbv[1],
    &sbit19wdbv[1],
    &sbit20wdbv[1],
    &sbit21wdbv[1],
    &sbit22wdbv[1],
    &sbit23wdbv[1],
    &sbit24wdbv[1],
    &sbit25wdbv[1],
    &sbit26wdbv[1],
    &sbit27wdbv[1],
    &sbit28wdbv[1],
    &sbit29wdbv[1],
    &sbit30wdbv[1],
    &sbit31wdbv[1],
    &sbit32wdbv[1],
    &sbit1wrbv[1],
    &sbit2wrbv[1],
    &sbit3wrbv[1],
    &sbit4wrbv[1],
    &sbit5wrbv[1],
    &sbit6wrbv[1],
    &sbit7wrbv[1],
    &sbit8wrbv[1],
    &sbit9wrbv[1],
    &sbit10wrbv[1],
    &sbit11wrbv[1],
    &sbit12wrbv[1],
    &sbit13wrbv[1],
    &sbit14wrbv[1],
    &sbit15wrbv[1],
    &sbit16wrbv[1],
    &sbit17wrbv[1],
    &sbit18wrbv[1],
    &sbit19wrbv[1],
    &sbit20wrbv[1],
    &sbit21wrbv[1],
    &sbit22wrbv[1],
    &sbit23wrbv[1],
    &sbit24wrbv[1],
    &sbit25wrbv[1],
    &sbit26wrbv[1],
    &sbit27wrbv[1],
    &sbit28wrbv[1],
    &sbit29wrbv[1],
    &sbit30wrbv[1],
    &sbit31wrbv[1],
    &sbit32wrbv[1],
    &ubit1dbv[1],
    &ubit2dbv[1],
    &ubit3dbv[1],
    &ubit4dbv[1],
    &ubit5dbv[1],
    &ubit6dbv[1],
    &ubit7dbv[1],
    &ubit8dbv[1],
    &ubit9dbv[1],
    &ubit10dbv[1],
    &ubit11dbv[1],
    &ubit12dbv[1],
    &ubit13dbv[1],
    &ubit14dbv[1],
    &ubit15dbv[1],
    &ubit16dbv[1],
    &ubit17dbv[1],
    &ubit18dbv[1],
    &ubit19dbv[1],
    &ubit20dbv[1],
    &ubit21dbv[1],
    &ubit22dbv[1],
    &ubit23dbv[1],
    &ubit24dbv[1],
    &ubit25dbv[1],
    &ubit26dbv[1],
    &ubit27dbv[1],
    &ubit28dbv[1],
    &ubit29dbv[1],
    &ubit30dbv[1],
    &ubit31dbv[1],
    &ubit32dbv[1],
    &ubit1rbv[1],
    &ubit2rbv[1],
    &ubit3rbv[1],
    &ubit4rbv[1],
    &ubit5rbv[1],
    &ubit6rbv[1],
    &ubit7rbv[1],
    &ubit8rbv[1],
    &ubit9rbv[1],
    &ubit10rbv[1],
    &ubit11rbv[1],
    &ubit12rbv[1],
    &ubit13rbv[1],
    &ubit14rbv[1],
    &ubit15rbv[1],
    &ubit16rbv[1],
    &ubit17rbv[1],
    &ubit18rbv[1],
    &ubit19rbv[1],
    &ubit20rbv[1],
    &ubit21rbv[1],
    &ubit22rbv[1],
    &ubit23rbv[1],
    &ubit24rbv[1],
    &ubit25rbv[1],
    &ubit26rbv[1],
    &ubit27rbv[1],
    &ubit28rbv[1],
    &ubit29rbv[1],
    &ubit30rbv[1],
    &ubit31rbv[1],
    &ubit32rbv[1],
    &ubit1wdbv[1],
    &ubit2wdbv[1],
    &ubit3wdbv[1],
    &ubit4wdbv[1],
    &ubit5wdbv[1],
    &ubit6wdbv[1],
    &ubit7wdbv[1],
    &ubit8wdbv[1],
    &ubit9wdbv[1],
    &ubit10wdbv[1],
    &ubit11wdbv[1],
    &ubit12wdbv[1],
    &ubit13wdbv[1],
    &ubit14wdbv[1],
    &ubit15wdbv[1],
    &ubit16wdbv[1],
    &ubit17wdbv[1],
    &ubit18wdbv[1],
    &ubit19wdbv[1],
    &ubit20wdbv[1],
    &ubit21wdbv[1],
    &ubit22wdbv[1],
    &ubit23wdbv[1],
    &ubit24wdbv[1],
    &ubit25wdbv[1],
    &ubit26wdbv[1],
    &ubit27wdbv[1],
    &ubit28wdbv[1],
    &ubit29wdbv[1],
    &ubit30wdbv[1],
    &ubit31wdbv[1],
    &ubit32wdbv[1],
    &ubit1wrbv[1],
    &ubit2wrbv[1],
    &ubit3wrbv[1],
    &ubit4wrbv[1],
    &ubit5wrbv[1],
    &ubit6wrbv[1],
    &ubit7wrbv[1],
    &ubit8wrbv[1],
    &ubit9wrbv[1],
    &ubit10wrbv[1],
    &ubit11wrbv[1],
    &ubit12wrbv[1],
    &ubit13wrbv[1],
    &ubit14wrbv[1],
    &ubit15wrbv[1],
    &ubit16wrbv[1],
    &ubit17wrbv[1],
    &ubit18wrbv[1],
    &ubit19wrbv[1],
    &ubit20wrbv[1],
    &ubit21wrbv[1],
    &ubit22wrbv[1],
    &ubit23wrbv[1],
    &ubit24wrbv[1],
    &ubit25wrbv[1],
    &ubit26wrbv[1],
    &ubit27wrbv[1],
    &ubit28wrbv[1],
    &ubit29wrbv[1],
    &ubit30wrbv[1],
    &ubit31wrbv[1],
    &ubit32wrbv[1]
};

DEFINE_DECODE_VAR(i8_typ, -1, 360.0, 0, 1);
DEFINE_DECODE_VAR(u8_typ, 1, 360.0, -180, 1);
DEFINE_DECODE_VAR(i16_typ, -2, 360.0, 0, 1);
DEFINE_DECODE_VAR(u16_typ, 2, 360.0, -180, 1);
DEFINE_DECODE_VAR(i24_typ, -3, 360.0, 0, 1);
DEFINE_DECODE_VAR(u24_typ, 3, 360.0, -180, 1);
DEFINE_DECODE_VAR(i32_typ, -4, 360.0, 0, 1);
DEFINE_DECODE_VAR(u32_typ, 4, 360.0, -180, 1);

DEFINE_RDECODE_VAR(ri8_typ, -1, 360.0, 0, 1);
DEFINE_RDECODE_VAR(ru8_typ, 1, 360.0, -180, 1);
DEFINE_RDECODE_VAR(ri16_typ, -2, 360.0, 0, 1);
DEFINE_RDECODE_VAR(ru16_typ, 2, 360.0, -180, 1);
DEFINE_RDECODE_VAR(ri24_typ, -3, 360.0, 0, 1);
DEFINE_RDECODE_VAR(ru24_typ, 3, 360.0, -180, 1);
DEFINE_RDECODE_VAR(ri32_typ, -4, 360.0, 0, 1);
DEFINE_RDECODE_VAR(ru32_typ, 4, 360.0, -180, 1);

DEFINE_DECODE_COMPLEMENT_VAR(ci8_typ, -1, 360.0, 0, 1);
DEFINE_DECODE_COMPLEMENT_VAR(cu8_typ, 1, 360.0, 0, 1);
DEFINE_DECODE_COMPLEMENT_VAR(ci16_typ, -2, 360.0, 0, 1);
DEFINE_DECODE_COMPLEMENT_VAR(cu16_typ, 2, 360.0, 0, 1);
DEFINE_DECODE_COMPLEMENT_VAR(ci24_typ, -3, 360.0, 0, 1);
DEFINE_DECODE_COMPLEMENT_VAR(cu24_typ, 3, 360.0, 0, 1);
DEFINE_DECODE_COMPLEMENT_VAR(ci32_typ, -4, 360.0, 0, 1);
DEFINE_DECODE_COMPLEMENT_VAR(cu32_typ, 4, 360.0, 0, 1);

DEFINE_DECODE_BIT_VAR(sdbv1_typ,-1,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv1_typ,1,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv2_typ,-2,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv2_typ,2,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv3_typ,-3,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv3_typ,3,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv4_typ,-4,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv4_typ,4,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv5_typ,-5,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv5_typ,5,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv6_typ,-6,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv6_typ,6,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv7_typ,-7,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv7_typ,7,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv8_typ,-8,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv8_typ,8,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv9_typ,-9,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv9_typ,9,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv10_typ,-10,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv10_typ,10,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv11_typ,-11,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv11_typ,11,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv12_typ,-12,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv12_typ,12,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv13_typ,-13,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv13_typ,13,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv14_typ,-14,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv14_typ,14,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv15_typ,-15,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv15_typ,15,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv16_typ,-16,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv16_typ,16,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv17_typ,-17,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv17_typ,17,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv18_typ,-18,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv18_typ,18,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv19_typ,-19,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv19_typ,19,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv20_typ,-20,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv20_typ,20,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv21_typ,-21,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv21_typ,21,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv22_typ,-22,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv22_typ,22,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv23_typ,-23,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv23_typ,23,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv24_typ,-24,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv24_typ,24,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv25_typ,-25,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv25_typ,25,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv26_typ,-26,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv26_typ,26,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv27_typ,-27,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv27_typ,27,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv28_typ,-28,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv28_typ,28,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv29_typ,-29,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv29_typ,29,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv30_typ,-30,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv30_typ,30,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv31_typ,-31,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv31_typ,31,360.0,-180.0,1);
DEFINE_DECODE_BIT_VAR(sdbv32_typ,-32,360.0,0,1);
DEFINE_DECODE_BIT_VAR(udbv32_typ,32,360.0,-180.0,1);

DEFINE_RDECODE_BIT_VAR(srvb1_typ, -1, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb1_typ, 1, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb2_typ, -2, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb2_typ, 2, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb3_typ, -3, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb3_typ, 3, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb4_typ, -4, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb4_typ, 4, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb5_typ, -5, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb5_typ, 5, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb6_typ, -6, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb6_typ, 6, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb7_typ, -7, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb7_typ, 7, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb8_typ, -8, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb8_typ, 8, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb9_typ, -9, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb9_typ, 9, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb10_typ, -10, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb10_typ, 10, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb11_typ, -11, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb11_typ, 11, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb12_typ, -12, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb12_typ, 12, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb13_typ, -13, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb13_typ, 13, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb14_typ, -14, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb14_typ, 14, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb15_typ, -15, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb15_typ, 15, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb16_typ, -16, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb16_typ, 16, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb17_typ, -17, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb17_typ, 17, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb18_typ, -18, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb18_typ, 18, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb19_typ, -19, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb19_typ, 19, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb20_typ, -20, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb20_typ, 20, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb21_typ, -21, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb21_typ, 21, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb22_typ, -22, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb22_typ, 22, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb23_typ, -23, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb23_typ, 23, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb24_typ, -24, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb24_typ, 24, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb25_typ, -25, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb25_typ, 25, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb26_typ, -26, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb26_typ, 26, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb27_typ, -27, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb27_typ, 27, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb28_typ, -28, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb28_typ, 28, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb29_typ, -29, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb29_typ, 29, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb30_typ, -30, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb30_typ, 30, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb31_typ, -31, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb31_typ, 31, 360.0, -180.0, 1);
DEFINE_RDECODE_BIT_VAR(srvb32_typ, -32, 360.0, 0, 1);
DEFINE_RDECODE_BIT_VAR(urvb32_typ, 32, 360.0, -180.0, 1);

struct c_typ : public complex_var
{
    COMPLEXCONST( c_typ )
    i8_typ i8;
    u8_typ u8;
    i16_typ i16;
    u16_typ u16;
    i24_typ i24;
    u24_typ u24;
    i32_typ i32;
    u32_typ u32;
    ri8_typ ri8;
    ru8_typ ru8;
    ri16_typ ri16;
    ru16_typ ru16;
    ri24_typ ri24;
    ru24_typ ru24;
    ri32_typ ri32;
    ru32_typ ru32;
    ci8_typ ci8;
    cu8_typ cu8;
    ci16_typ ci16;
    cu16_typ cu16;
    ci24_typ ci24;
    cu24_typ cu24;
    ci32_typ ci32;
    cu32_typ cu32;
    sdbv1_typ sdbv1;
    sdbv2_typ sdbv2;
    sdbv3_typ sdbv3;
    sdbv4_typ sdbv4;
    sdbv5_typ sdbv5;
    sdbv6_typ sdbv6;
    sdbv7_typ sdbv7;
    sdbv8_typ sdbv8;
    sdbv9_typ sdbv9;
    sdbv10_typ sdbv10;
    sdbv11_typ sdbv11;
    sdbv12_typ sdbv12;
    sdbv13_typ sdbv13;
    sdbv14_typ sdbv14;
    sdbv15_typ sdbv15;
    sdbv16_typ sdbv16;
    sdbv17_typ sdbv17;
    sdbv18_typ sdbv18;
    sdbv19_typ sdbv19;
    sdbv20_typ sdbv20;
    sdbv21_typ sdbv21;
    sdbv22_typ sdbv22;
    sdbv23_typ sdbv23;
    sdbv24_typ sdbv24;
    sdbv25_typ sdbv25;
    sdbv26_typ sdbv26;
    sdbv27_typ sdbv27;
    sdbv28_typ sdbv28;
    sdbv29_typ sdbv29;
    sdbv30_typ sdbv30;
    sdbv31_typ sdbv31;
    sdbv32_typ sdbv32;
    udbv1_typ udbv1;
    udbv2_typ udbv2;
    udbv3_typ udbv3;
    udbv4_typ udbv4;
    udbv5_typ udbv5;
    udbv6_typ udbv6;
    udbv7_typ udbv7;
    udbv8_typ udbv8;
    udbv9_typ udbv9;
    udbv10_typ udbv10;
    udbv11_typ udbv11;
    udbv12_typ udbv12;
    udbv13_typ udbv13;
    udbv14_typ udbv14;
    udbv15_typ udbv15;
    udbv16_typ udbv16;
    udbv17_typ udbv17;
    udbv18_typ udbv18;
    udbv19_typ udbv19;
    udbv20_typ udbv20;
    udbv21_typ udbv21;
    udbv22_typ udbv22;
    udbv23_typ udbv23;
    udbv24_typ udbv24;
    udbv25_typ udbv25;
    udbv26_typ udbv26;
    udbv27_typ udbv27;
    udbv28_typ udbv28;
    udbv29_typ udbv29;
    udbv30_typ udbv30;
    udbv31_typ udbv31;
    udbv32_typ udbv32; // unfortunately this ends on the byte boundary
    sdbv1_typ last8;
    udbv2_typ last7;
    sdbv3_typ last6;
    udbv1_typ last5; // 7th bit, leave 1 blank to test
    srvb1_typ srvb1; // start at byte boundary here
    srvb2_typ srvb2;
    srvb3_typ srvb3;
    srvb4_typ srvb4;
    srvb5_typ srvb5;
    srvb6_typ srvb6;
    srvb7_typ srvb7;
    srvb8_typ srvb8;
    srvb9_typ srvb9;
    srvb10_typ srvb10;
    srvb11_typ srvb11;
    srvb12_typ srvb12;
    srvb13_typ srvb13;
    srvb14_typ srvb14;
    srvb15_typ srvb15;
    srvb16_typ srvb16;
    srvb17_typ srvb17;
    srvb18_typ srvb18;
    srvb19_typ srvb19;
    srvb20_typ srvb20;
    srvb21_typ srvb21;
    srvb22_typ srvb22;
    srvb23_typ srvb23;
    srvb24_typ srvb24;
    srvb25_typ srvb25;
    srvb26_typ srvb26;
    srvb27_typ srvb27;
    srvb28_typ srvb28;
    srvb29_typ srvb29;
    srvb30_typ srvb30;
    srvb31_typ srvb31;
    srvb32_typ srvb32;
    urvb1_typ urvb1;
    urvb2_typ urvb2;
    urvb3_typ urvb3;
    urvb4_typ urvb4;
    urvb5_typ urvb5;
    urvb6_typ urvb6;
    urvb7_typ urvb7;
    urvb8_typ urvb8;
    urvb9_typ urvb9;
    urvb10_typ urvb10;
    urvb11_typ urvb11;
    urvb12_typ urvb12;
    urvb13_typ urvb13;
    urvb14_typ urvb14;
    urvb15_typ urvb15;
    urvb16_typ urvb16;
    urvb17_typ urvb17;
    urvb18_typ urvb18;
    urvb19_typ urvb19;
    urvb20_typ urvb20;
    urvb21_typ urvb21;
    urvb22_typ urvb22;
    urvb23_typ urvb23;
    urvb24_typ urvb24;
    urvb25_typ urvb25;
    urvb26_typ urvb26;
    urvb27_typ urvb27;
    urvb28_typ urvb28;
    urvb29_typ urvb29;
    urvb30_typ urvb30;
    urvb31_typ urvb31;
    urvb32_typ urvb32; // unfortunately this ends on the byte boundary
    srvb1_typ last4;
    urvb2_typ last3;
    srvb3_typ last2;
    urvb1_typ last1; // 7th bit, leave 1 blank to test
};

struct d_typ : public complex_var
{

    RUNTIME_TYPE(d_typ)
    mpt_var* getNext()
    {
        return this + 1;
    }
    d_typ();
    d_typ(const d_typ &right);
    srvb11_typ b1;
    r_bit_var<6> b2;
    sdbv10_typ b3;
    bit_var<11> b4;
};

d_typ::d_typ()
{
    pop();
}

d_typ::d_typ(const d_typ &right) :
    b2(right.b2),
    b4(right.b4)
{
    pop();
    b1 = right.b1;
    b3 = right.b3;
}

template<class C>
C round_double(double d)
{
    double val = round(d);
    if (val > std::numeric_limits<C>::max()) return std::numeric_limits<C>::max();
    if (val < std::numeric_limits<C>::min()) return std::numeric_limits<C>::min();
    return val;
}

DecodeVarTestSuite::DecodeVarTestSuite()
{
    srand((unsigned int)time(NULL));
    value_var::set_float_format("%.18g");
}

void DecodeVarTestSuite::testAssign(void)
{
    double d = MIN_VAL + INC_VAL;
    double e;
    generic_var<double> g(d + 11.0);

    for(int i = 0; i < NELEM(var); ++i)
    {
        d = MIN_VAL + INC_VAL;

        //assignment and comparison with double
        *var[i] = d;
        TS_ASSERT(*var[i] == d);
        TS_ASSERT(*var[i] != -d);
        TS_ASSERT(-*var[i] == -d);
        //assignment and comparison with decode_var
        decode_var *dv = dynamic_cast<decode_var *>(var[i]);
        if (dv)
        {
            decode_var *dm = dynamic_cast<decode_var *>(mirror[i]);
            *dm = *dv;
            TS_ASSERT(*dv == *dm);
            TS_ASSERT(!(*dv != *dm));
            TS_ASSERT(*dv <= *dm);
            TS_ASSERT(*dv >= *dm);
            *dv = d - 1.0;
            TS_ASSERT(*dv < *dm);
            TS_ASSERT(*dv <= *dm);
            *dv = d + 1.0;
            TS_ASSERT(*dv > *dm);
            TS_ASSERT(*dv >= *dm);
        }
        else
        {
            rdecode_var *rv = dynamic_cast<rdecode_var *>(var[i]);
            if (rv)
            {
                rdecode_var *rm = dynamic_cast<rdecode_var *>(mirror[i]);
                *rm = *rv;
                TS_ASSERT(*rv == *rm);
                TS_ASSERT(!(*rv != *rm));
                TS_ASSERT(*rv <= *rm);
                TS_ASSERT(*rv >= *rm);
                *rv = d - 1.0;
                TS_ASSERT(*rv < *rm);
                TS_ASSERT(*rv <= *rm);
                *rv = d + 1.0;
                TS_ASSERT(*rv > *rm);
                TS_ASSERT(*rv >= *rm);
            }
            else
            {
                decode_bit_var *dbv = dynamic_cast<decode_bit_var *>(var[i]);
                if (dbv)
                {
                    decode_bit_var *dbm = dynamic_cast<decode_bit_var *>(mirror[i]);
                    *dbm = *dbv;
                    TS_ASSERT(*dbv == *dbm);
                    TS_ASSERT(!(*dbv != *dbm));
                    TS_ASSERT(*dbv <= *dbm);
                    TS_ASSERT(*dbv >= *dbm);
                    *dbv = d - 1.0;
                    TS_ASSERT(*dbv < *dbm);
                    TS_ASSERT(*dbv <= *dbm);
                    *dbv = d + 1.0;
                    TS_ASSERT(*dbv > *dbm);
                    TS_ASSERT(*dbv >= *dbm);
                }
                else
                {
                    rdecode_bit_var *rbv = dynamic_cast<rdecode_bit_var *>(var[i]);
                    if (rbv)
                    {
                        rdecode_bit_var *rbm = dynamic_cast<rdecode_bit_var *>(mirror[i]);
                        *rbm = *rbv;
                        TS_ASSERT(*rbv == *rbm);
                        TS_ASSERT(!(*rbv != *rbm));
                        TS_ASSERT(*rbv <= *rbm);
                        TS_ASSERT(*rbv >= *rbm);
                        *rbv = d - 1.0;
                        TS_ASSERT(*rbv < *rbm);
                        TS_ASSERT(*rbv <= *rbm);
                        *rbv = d + 1.0;
                        TS_ASSERT(*rbv > *rbm);
                        TS_ASSERT(*rbv >= *rbm);
                    }
                    else
                    {
                        decode_complement_var *dcv = dynamic_cast<decode_complement_var *>(var[i]);
                        if (dcv)
                        {
                            decode_complement_var *dcm = dynamic_cast<decode_complement_var *>(mirror[i]);
                            *dcm = *dcv;
                            TS_ASSERT(*dcv == *dcm);
                            TS_ASSERT(!(*dcv != *dcm));
                            TS_ASSERT(*dcv <= *dcm);
                            TS_ASSERT(*dcv >= *dcm);
                            *dcv = d - 1.0;
                            TS_ASSERT(*dcv < *dcm);
                            TS_ASSERT(*dcv <= *dcm);
                            *dcv = d + 1.0;
                            TS_ASSERT(*dcv > *dcm);
                            TS_ASSERT(*dcv >= *dcm);
                        }
                    }
                }
            }
        }

        //function operator
        *var[i] = d;
        TS_ASSERT((*var[i])() == d);

        //to_xxx function
        TS_ASSERT(var[i]->to_double() == d);
        TS_ASSERT_EQUALS(var[i]->to_int(), round_double<int>(d));
        TS_ASSERT_EQUALS(var[i]->to_unsigned_int(), round_double<unsigned int>(d));
        TS_ASSERT_EQUALS(var[i]->to_short(), round_double<short>(d));
        TS_ASSERT_EQUALS(var[i]->to_unsigned_short(), round_double<unsigned short>(d));
        TS_ASSERT_EQUALS(var[i]->to_char(), round_double<char>(d));
        TS_ASSERT_EQUALS(var[i]->to_unsigned_char(), round_double<unsigned char>(d));

        //from_xxx function
        var[i]->from_char(-55);
        TS_ASSERT(*var[i] == (double)-55);
        var[i]->from_unsigned_char(55);
        TS_ASSERT(*var[i] == (double)55);
        var[i]->from_short(-55);
        TS_ASSERT(*var[i] == (double)-55);
        var[i]->from_unsigned_short(55);
        TS_ASSERT(*var[i] == (double)55);
        var[i]->from_int(-55);
        TS_ASSERT(*var[i] == (double)-55);
        var[i]->from_unsigned_int(55);
        TS_ASSERT(*var[i] == (double)55);
        var[i]->from_long(-55);
        TS_ASSERT(*var[i] == (double)-55);
        var[i]->from_unsigned_long(55);
        TS_ASSERT(*var[i] == (double)55);
        var[i]->from_char_ptr("-55");
        TS_ASSERT(*var[i] == (double)-55);
        var[i]->from_char_ptr("55");
        TS_ASSERT(*var[i] == (double)55);

        //comparison with double
        *var[i] = d;
        TS_ASSERT(!(*var[i] < d));
        TS_ASSERT(!(*var[i] > d));
        TS_ASSERT(*var[i] <= d);
        TS_ASSERT(*var[i] >= d);
        *var[i] += 1.0;
        TS_ASSERT(!(*var[i] < d));
        TS_ASSERT(*var[i] > d);
        *var[i] -= 2.0;
        TS_ASSERT(*var[i] < d);
        TS_ASSERT(!(*var[i] > d));

        //assignment and comparison with generic_var
        *var[i] = g;
        TS_ASSERT(*var[i] == g);
        TS_ASSERT(!(*var[i] != g));
        TS_ASSERT(!(*var[i] < g));
        TS_ASSERT(!(*var[i] > g));
        TS_ASSERT(*var[i] <= g);
        TS_ASSERT(*var[i] >= g);
        *var[i] += 1.0;
        *var[i] += g;
        TS_ASSERT(!(*var[i] < (g + g)));
        TS_ASSERT(*var[i] > (g + g));
        *var[i] -= g;
        TS_ASSERT(*var[i] > g);
        TS_ASSERT(!(*var[i] < g));

        //+ - * / with double
        *var[i] = d;
        e = d + *var[i];
        TS_ASSERT(e == (d + d));
        e = *var[i] + d;
        TS_ASSERT(e == (d + d));
        e = d - *var[i];
        TS_ASSERT(e == (d - d));
        e = *var[i] - d;
        TS_ASSERT(e == (d - d));
        e = d * (double)(*var[i]);
        TS_ASSERT_DELTA(e, (d * d), DBL_EPSILON);
        e = *var[i] * d;
        TS_ASSERT_DELTA(e, (d * d), DBL_EPSILON);
        e = d / *var[i];
        TS_ASSERT(e == (d / d));
        e = *var[i] / d;
        TS_ASSERT(e == (d / d));

        //+ - * / with generic
        *var[i] = g;
        e = g + *var[i];
        TS_ASSERT(e == (g + g));
        e = *var[i] + g;
        TS_ASSERT(e == (g + g));
        e = g - *var[i];
        TS_ASSERT(e == (g - g));
        e = *var[i] - g;
        TS_ASSERT(e == (g - g));
        e = g * *var[i];
        TS_ASSERT_DELTA(e, (g * g), DBL_EPSILON);
        e = *var[i] * g;
        TS_ASSERT_DELTA(e, (g * g), DBL_EPSILON);
        e = g / *var[i];
        TS_ASSERT(e == (g / g));
        e = *var[i] / g;
        TS_ASSERT(e == (g / g));
    }
}

void DecodeVarTestSuite::testStream(void)
{
    char msg[100];
    outbuf ob;

    for(int i = 0; i < NELEM(var); ++i)
    {
        for(double d = MIN_VAL; d <= MAX_VAL; d += INC_VAL)
        {
            *var[i] = d;
            sprintf(msg, "(var[%d] after assign %.2f)", i, d);
            TSM_ASSERT(msg, *var[i] == d);
            std::ostringstream os;
            os << *var[i];

            *var[i] = -d + 0.2;
            sprintf(msg, "(var[%d] after reset %.2f)", i, d);
            TSM_ASSERT(msg, *var[i] != d);
            std::istringstream is(os.str());
            is >> *var[i];
            sprintf(msg, "(var[%d] after stream in %.2f)", i, d);
            TSM_ASSERT(msg, (double)*var[i] >= (d - STREAM_ERR) && *var[i] <= (d + STREAM_ERR));
        }
    }
}

void DecodeVarTestSuite::testExtract(void)
{
    unsigned char buf[100];
    char msg[300];
    outbuf ob;

    for(int i = 0; i < NELEM(var); ++i)
    {
        for(double d = MIN_VAL; d <= MAX_VAL; d += INC_VAL)
        {
            *var[i] = d;
            sprintf(msg, "(var[%d] after assign %.2f)", i, d);
            TSM_ASSERT_DELTA(msg, (double)*var[i], d, DBL_EPSILON);
            memset(buf, 0, sizeof(buf));
            ob.set(buf, sizeof(buf));
            var[i]->output(ob);
            sprintf(msg, "(var[%d] after output %.2f)", i, d);
            TSM_ASSERT(msg, ob.size() == var[i]->size());

            *var[i] = -d + 1.2;
            sprintf(msg, "(var[%d] after reset %.2f)", i, d);
            TSM_ASSERT(msg, (double)*var[i] != d);
            var[i]->extract(ob.size(), ob.get());
            sprintf(msg, "(var[%d] after extract %.2f), %.15f %.15f %.15f %s)", i, d,
                (double)*var[i],
                d - 1.5 * var[i]->getScaling(), d + 1.5 * var[i]->getScaling(),
                ((double)*var[i] >= d - 1.5 * var[i]->getScaling()) &&
                ((double)*var[i] <= d + 1.5 * var[i]->getScaling())? "true" : "false");
            TSM_ASSERT_DELTA(msg, (double)*var[i], d, 1.5 * var[i]->getScaling());
        }
    }
}

void DecodeVarTestSuite::testExtract2(void)
{
    unsigned char buf[500];
    char msg[300];
    outbuf ob;
    c_typ c;

    for(double d = MIN_VAL; d <= MAX_VAL - (c.cardinal() * INC_VAL); d += (c.cardinal() * INC_VAL))
    {
        memset(buf, 0, sizeof(buf));
        ob.set(buf, sizeof(buf));

        //set value
        for(int i = 0; i < c.cardinal(); ++i)
        {
            double dd = d + i * INC_VAL;
            dynamic_cast<value_var*>(c[i])->fromValue(dd);
            sprintf(msg, "(c[%d] after assign %.2f)", i, dd);
            TSM_ASSERT_DELTA(msg, dynamic_cast<value_var*>(c[i])->to_double(), dd, DBL_EPSILON);
        }

        //output
        c.output(ob);

        //reset value
        for(int i = 0; i < c.cardinal(); ++i)
        {
            double dd = d + i * INC_VAL;
            dynamic_cast<value_var*>(c[i])->fromValue(-d + 1.1);
            TS_ASSERT(dynamic_cast<value_var*>(c[i])->to_double() != dd);
        }

        //extract
        TS_ASSERT(c.extract(ob.size(), ob.get()) == 0);

        //test
        for(int i = 0; i < c.cardinal(); ++i)
        {
            double dd = d + i * INC_VAL;
            vdecode_var *dv = dynamic_cast<vdecode_var*>(c[i]);
            double scaling = dv->getScaling();

            sprintf(msg, "(nested.var[%d] after extract %.2f), %.15f %.15f %.15f %s)", i, dd,
                dynamic_cast<value_var*>(c[i])->to_double(),
                dd - 1.5 * scaling, dd + 1.5 * scaling,
                (dynamic_cast<value_var*>(c[i])->to_double() >= dd - 1.5 * scaling) &&
                (dynamic_cast<value_var*>(c[i])->to_double() <= dd + 1.5 * scaling) ? "true" : "false");
            TSM_ASSERT_DELTA(msg,
                dynamic_cast<value_var*>(c[i])->to_double(), dd,
                1.5 * scaling);
        }
    }

    // equivalence checks
    c_typ stfill;
    for (int k = 0; k < stfill.cardinal(); ++k)
    {
        generic_var<double> *gd = dynamic_cast<generic_var<double> *>(stfill[k]);
        if (gd)
        {
            double rv = static_cast<double>(rand()) / static_cast<double>(rand());
            while (rv > 180.0) rv /= 2.;
            if (rand() % 2 == 0) rv = -rv;
            gd->from_double(rv);
        }
        else TSM_ASSERT("Dynamic cast failed", false);
    }

    // a. stream
    std::ostringstream os;
    os << stfill;

    c.setInvalid();
    TS_ASSERT_DIFFERS(stfill, c);

    std::istringstream is(os.str());
    is >> c;
    TS_ASSERT_EQUALS(stfill, c);

    // b. assignment
    c.setInvalid();
    TS_ASSERT_DIFFERS(stfill, c);
    c = stfill;
    TS_ASSERT_EQUALS(stfill, c);

    // c. copy constructor
    c_typ stcpy(stfill);
    TS_ASSERT_EQUALS(stfill, stcpy);
}

struct X1
{
    static const int SIZE = 4;
    static const double RANGE;
    static const double OFFSET;
    static const int SCALE_OFFSET = 1;
};
const double X1::RANGE = 360.;
const double X1::OFFSET = -180.;

bool withinRange(double a1, double a2)
{
    return (a1 > a2 - 1.e-6) && (a1 < a2 + 1.e-6);
}

void DecodeVarTestSuite::testExtractWithTemplatedDecodeVars()
{
    templ_decode_var<X1> dv;
    unsigned int buf = 0;
    dv.extract(4, (const unsigned char *)&buf);
    TS_ASSERT_EQUALS(dv.to_double(), X1::OFFSET);

    buf = UINT_MAX;
    dv.extract(4, (const unsigned char *)&buf);
    TS_ASSERT_EQUALS(dv.to_double(), X1::OFFSET+X1::RANGE);

    buf = UINT_MAX/2;
    dv.extract(4, (const unsigned char *)&buf);
    TS_ASSERT(withinRange(dv.to_double(), 0.));

    templ_rdecode_var<X1> rv;
    buf = 0;
    rv.extract(4, (const unsigned char *)&buf);
    TS_ASSERT_EQUALS(rv.to_double(), X1::OFFSET);

    buf = UINT_MAX;
    rv.extract(4, (const unsigned char *)&buf);
    TS_ASSERT_EQUALS(rv.to_double(), X1::OFFSET + X1::RANGE);

    buf = 0xFFFFFF7F;
    rv.extract(4, (const unsigned char *)&buf);
    TS_ASSERT(withinRange(rv.to_double(), 0.));
}

/* Decode-bit-decode, span across byte boundary, end before boundary */
struct bf1_typ : public complex_var
{
    COMPLEXCONST(bf1_typ)
    sdbv1_typ bv1;   // --- byte boundary --
    bit_var<2> bv2;
    udbv7_typ bv3;   // --- cross boundary --
    bit_var<4> bv4;
};

/* Bit-decode-bit, span across byte boundary, end after boundary */
struct bf2_typ : public complex_var
{
    COMPLEXCONST(bf2_typ)
    bit_var<3> bv1;   // --- byte boundary --
    sdbv3_typ bv2;
    bit_var<9> bv3;   // --- cross boundary --
};

/* Decode-bit-decode, stay within byte boundary, end before boundary */
struct bf3_typ : public complex_var
{
    COMPLEXCONST(bf3_typ)
    udbv4_typ bv1;   // --- byte boundary --
    bit_var<4> bv2;
    sdbv3_typ bv3;   // --- byte boundary --
    bit_var<4> bv4;
};

/* Bit-decode-bit, stay within byte boundary, end on boundary */
struct bf4_typ : public complex_var
{
    COMPLEXCONST(bf4_typ)
    bit_var<5> bv1;    // --- byte boundary --
    sdbv3_typ bv2;
    bit_var<4> bv3;  // --- byte boundary --
    udbv4_typ bv4;
};

/* Decode-bit-decode, span across byte boundary, end before boundary */
struct bf5_typ : public complex_var
{
    COMPLEXCONST(bf5_typ)
    srvb1_typ bv1;   // --- byte boundary --
    r_bit_var<2> bv2;
    urvb7_typ bv3;   // --- cross boundary --
    r_bit_var<4> bv4;
};

/* Bit-decode-bit, span across byte boundary, end after boundary */
struct bf6_typ : public complex_var
{
    COMPLEXCONST(bf6_typ)
    r_bit_var<3> bv1;   // --- byte boundary --
    srvb3_typ bv2;
    r_bit_var<9> bv3;   // --- cross boundary --
};

/* Decode-bit-decode, stay within byte boundary, end before boundary */
struct bf7_typ : public complex_var
{
    COMPLEXCONST(bf7_typ)
    urvb4_typ bv1;   // --- byte boundary --
    r_bit_var<4> bv2;
    srvb3_typ bv3;   // --- byte boundary --
    r_bit_var<4> bv4;
};

/* Bit-decode-bit, stay within byte boundary, end on boundary */
struct bf8_typ : public complex_var
{
    COMPLEXCONST(bf8_typ)
    r_bit_var<5> bv1;    // --- byte boundary --
    srvb3_typ bv2;
    r_bit_var<4> bv3;  // --- byte boundary --
    urvb4_typ bv4;
};

/* Normal and R versions will automatically create a byte boundary */
struct bf9_typ : public complex_var
{
    COMPLEXCONST(bf9_typ)
    sdbv4_typ bv1;
    r_bit_var<4> bv2; // -- this will start on another byte
};

struct bf10_typ : public complex_var
{
    COMPLEXCONST(bf10_typ)
    r_bit_var<4> bv1;
    sdbv4_typ bv2; // -- this will start on another byte
};

struct bf11_typ : public complex_var
{
    COMPLEXCONST(bf11_typ)
    srvb4_typ bv1;
    bit_var<4> bv2; // -- this will start on another byte
};

struct bf12_typ : public complex_var
{
    COMPLEXCONST(bf12_typ)
    bit_var<4> bv1;
    srvb4_typ bv2; // -- this will start on another byte
};

struct bf13_typ : public complex_var
{
    COMPLEXCONST(bf13_typ)
    udbv4_typ bv1;
    r_bit_var<4> bv2; // -- this will start on another byte
};

struct bf14_typ : public complex_var
{
    COMPLEXCONST(bf14_typ)
    r_bit_var<4> bv1;
    udbv4_typ bv2; // -- this will start on another byte
};

struct bf15_typ : public complex_var
{
    COMPLEXCONST(bf15_typ)
    urvb4_typ bv1;
    bit_var<4> bv2; // -- this will start on another byte
};

struct bf16_typ : public complex_var
{
    COMPLEXCONST(bf16_typ)
    bit_var<4> bv1;
    urvb4_typ bv2; // -- this will start on another byte
};

// Normal byte, Normal bit series
struct bf17_typ : public complex_var
{
    COMPLEXCONST(bf17_typ)
    u8_typ bv1;
    bit_var<4> bv2;
    sdbv2_typ bv3;
};

struct bf18_typ : public complex_var
{
    COMPLEXCONST(bf18_typ)
    u8_typ bv1;
    udbv2_typ bv2;
    bit_var<4> bv3;
};

struct bf19_typ : public complex_var
{
    COMPLEXCONST(bf19_typ)
    bit_var<4> bv1;
    udbv2_typ bv2;
    u8_typ bv3;
};

// Reverse byte, Reverse bit series
struct bf20_typ : public complex_var
{
    COMPLEXCONST(bf20_typ)
    ru8_typ bv1;
    r_bit_var<4> bv2;
    srvb2_typ bv3;
};

struct bf21_typ : public complex_var
{
    COMPLEXCONST(bf21_typ)
    ru8_typ bv1;
    urvb2_typ bv2;
    r_bit_var<4> bv3;
};

struct bf22_typ : public complex_var
{
    COMPLEXCONST(bf22_typ)
    r_bit_var<4> bv1;
    urvb2_typ bv2;
    ru8_typ bv3;
};

// Reverse byte, Normal bit series
struct bf23_typ : public complex_var
{
    COMPLEXCONST(bf23_typ)
    ru8_typ bv1;
    bit_var<4> bv2;
    sdbv2_typ bv3;
};

struct bf24_typ : public complex_var
{
    COMPLEXCONST(bf24_typ)
    ru8_typ bv1;
    udbv2_typ bv2;
    bit_var<4> bv3;
};

struct bf25_typ : public complex_var
{
    COMPLEXCONST(bf25_typ)
    bit_var<4> bv1;
    udbv2_typ bv2;
    ru8_typ bv3;
};

// Normal byte, Reverse bit series
struct bf26_typ : public complex_var
{
    COMPLEXCONST(bf26_typ)
    u8_typ bv1;
    r_bit_var<4> bv2;
    srvb2_typ bv3;
};

struct bf27_typ : public complex_var
{
    COMPLEXCONST(bf27_typ)
    u8_typ bv1;
    urvb2_typ bv2;
    r_bit_var<4> bv3;
};

struct bf28_typ : public complex_var
{
    COMPLEXCONST(bf28_typ)
    r_bit_var<4> bv1;
    urvb2_typ bv2;
    u8_typ bv3;
};

// Sign - unsigned mixing
struct bf29_typ : public complex_var
{
    COMPLEXCONST(bf29_typ)
    urvb3_typ bv1;
    srvb3_typ bv2;
    urvb3_typ bv3;
    srvb3_typ bv4;
};

struct bf30_typ : public complex_var
{
    COMPLEXCONST(bf30_typ)
    udbv3_typ bv1;
    sdbv3_typ bv2;
    udbv3_typ bv3;
    sdbv3_typ bv4;
};

// Big to small
struct bf31_typ : public complex_var
{
    COMPLEXCONST(bf31_typ)
    udbv12_typ bv1;
    sdbv4_typ bv2;
};

struct bf32_typ : public complex_var
{
    COMPLEXCONST(bf32_typ)
    sdbv12_typ bv1;
    udbv4_typ bv2;
};

struct bf33_typ : public complex_var
{
    COMPLEXCONST(bf33_typ)
    urvb12_typ bv1;
    srvb4_typ bv2;
};

struct bf34_typ : public complex_var
{
    COMPLEXCONST(bf34_typ)
    srvb12_typ bv1;
    urvb4_typ bv2;
};

void DecodeVarTestSuite::testBitFieldHandling()
{
    complex_var *tbfh[] = {
        new bf1_typ,
        new bf2_typ,
        new bf3_typ,
        new bf4_typ,
        new bf5_typ,
        new bf6_typ,
        new bf7_typ,
        new bf8_typ,
        new bf9_typ,
        new bf10_typ,
        new bf11_typ,
        new bf12_typ,
        new bf13_typ,
        new bf14_typ,
        new bf15_typ,
        new bf16_typ,
        new bf17_typ,
        new bf18_typ,
        new bf19_typ,
        new bf20_typ,
        new bf21_typ,
        new bf22_typ,
        new bf23_typ,
        new bf24_typ,
        new bf25_typ,
        new bf26_typ,
        new bf27_typ,
        new bf28_typ,
        new bf29_typ,
        new bf30_typ,
        new bf31_typ,
        new bf32_typ,
        new bf33_typ,
        new bf34_typ };
    complex_var *mirr[] = {
        new bf1_typ,
        new bf2_typ,
        new bf3_typ,
        new bf4_typ,
        new bf5_typ,
        new bf6_typ,
        new bf7_typ,
        new bf8_typ,
        new bf9_typ,
        new bf10_typ,
        new bf11_typ,
        new bf12_typ,
        new bf13_typ,
        new bf14_typ,
        new bf15_typ,
        new bf16_typ,
        new bf17_typ,
        new bf18_typ,
        new bf19_typ,
        new bf20_typ,
        new bf21_typ,
        new bf22_typ,
        new bf23_typ,
        new bf24_typ,
        new bf25_typ,
        new bf26_typ,
        new bf27_typ,
        new bf28_typ,
        new bf29_typ,
        new bf30_typ,
        new bf31_typ,
        new bf32_typ,
        new bf33_typ,
        new bf34_typ };

    unsigned char buf[100];
    char msg[300];
    outbuf ob;

    TS_ASSERT(mpt_var::check_stack());
    for (int i = 0; i < NELEM(tbfh); ++i)
    {
        sprintf(msg, "testBitFieldHandling var[%d]", i);
        for (int j = 0; j < 10; ++j)
        {
            for (int k = 0; k < tbfh[i]->cardinal(); ++k)
            {
                generic_var<unsigned int> *gv = dynamic_cast<generic_var<unsigned int> *>((*tbfh[i])[k]);
                if (gv) gv->from_unsigned_int(rand() % 256);
                else
                {
                    generic_var<double> *gd = dynamic_cast<generic_var<double> *>((*tbfh[i])[k]);
                    if (gd)
                    {
                        double rv = static_cast<double>(rand()) / static_cast<double>(rand());
                        while (rv > 180.0) rv /= 2.;
                        if (rand() % 2 == 0) rv = -rv;
                        gd->from_double(rv);
                    }
                }
            }
            TSM_ASSERT(msg, *tbfh[i] != *mirr[i]);
            memset(buf, 0, sizeof(buf));
            ob.set(buf, sizeof(buf));
            tbfh[i]->output(ob);
            TSM_ASSERT_EQUALS(msg, ob.size(), 2);
            mirr[i]->extract(ob.size(), buf);
            for (int k = 0; k < tbfh[i]->cardinal(); ++k)
            {
                vbit_var *gv = dynamic_cast<vbit_var *>((*tbfh[i])[k]);
                if (gv)
                {
                    vbit_var *gm = dynamic_cast<vbit_var *>((*mirr[i])[k]);
                    int bitsz = gv->get_bit_size();
                    unsigned int mask = 1;
                    for (int p = 1; p < bitsz; ++p) mask = (mask << 1) + 1;
                    unsigned int vv = gv->to_unsigned_int() & mask;
                    unsigned int mv = gm->to_unsigned_int() & mask;
                    TS_ASSERT_EQUALS(vv, mv);
                }
                else
                {
                    vdecode_var *dv = dynamic_cast<vdecode_var *>((*tbfh[i])[k]);
                    if (dv)
                    {
                        double scaling = dv->getScaling();
                        double dd = dv->to_double();
                        double dm = dynamic_cast<vdecode_var *>((*mirr[i])[k])->to_double();

                        sprintf(msg, "(nested.var[%d] after extract %.2f), %.15f %.15f %.15f %s)", i, dd,
                            dm, dd - 1.5 * scaling, dd + 1.5 * scaling,
                            ((dm >= dd - 1.5 * scaling) && (dm <= dd + 1.5 * scaling)) ? "true" : "false");
                        TSM_ASSERT_DELTA(msg, dm, dd, 1.5 * scaling);
                    }
                }
            }
        }
        delete tbfh[i];
        delete mirr[i];
    }
}

void DecodeVarTestSuite::testMisc()
{
    TS_ASSERT_EQUALS(u32[0].getOffset(), -180.);
    for (int i = 0; i < NELEM(var); ++i)
    {
        TS_ASSERT_EQUALS(var[i]->getNext(), mirror[i]);
    }

    d_typ d1;
    d1.b1 = 90.0;
    d1.b2 = 17;
    d1.b3 = -90.0;
    d1.b4 = 22;
    d_typ d2(d1);
    TS_ASSERT_EQUALS(d1, d2);

    char pfbuf[9];
    int pfsz;
    int pfex;

    unsigned int uit[2];
    uit[0] = rand();
    pfsz = prefix_encode(pfbuf, uit[0]);
    TS_ASSERT_LESS_THAN_EQUALS(pfsz, 5);
    pfex = prefix_decode(pfbuf, pfsz, uit[1]);
    TS_ASSERT_EQUALS(pfex, pfsz);
    TS_ASSERT_EQUALS(uit[0], uit[1]);

    TS_ASSERT(true);
}

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
* Copyright DSO National Laboratories 2013. All Rights Reserved.   *
*                                                                  *
* This file may not be used without permission from DSO.           *
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

/** \file rmPrvtShow.cpp
	\brief rmPrvtShow.cpp implementation file.

	This contains the class definition to read the updated RM data into the CGF CSCI.
*/


#include "writeshow.h"
#include "data.h"
#include <mpt_show.h>
#include <muxshow.h>
#include <iostream>

  
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


testWriteShow::testWriteShow()
{
	addCallback(&multiDataWrite.multData);	
}

testWriteShow::~testWriteShow()
{
	removeCallback(&multiDataWrite.multData);	
}


void testWriteShow::compute(int id)
{
	complex_var *var = multiDataWrite.multData.item(id); //operator[](id)
	if (var == 0) {
		// no child, the child was deleted

	} else {
		int type = multiDataWrite.multData.typeOf(id);
		switch (type) { //user do something with the child updated object
			case 0:
				{
					ooi_typ *obj = static_cast<ooi_typ *>(var);
					break;
				}
			case 1:
				{
					eei_typ *obj = static_cast<eei_typ *>(var);
					break;
				}
			case 2:
				{
					flightres_polygon_typ *obj = static_cast<flightres_polygon_typ *>(var);
					break;
				}
			case 3:
				{
					flightres_circle_typ *obj = static_cast<flightres_circle_typ *>(var);
					break;
				}
			case 4:
				{
					poi_typ *obj = static_cast<poi_typ *>(var);
					break;
				}
			default:
				break;
		}
	}
	
}
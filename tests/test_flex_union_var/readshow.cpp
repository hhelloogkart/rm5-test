/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
* Copyright DSO National Laboratories 2013. All Rights Reserved.   *
*                                                                  *
* This file may not be used without permission from DSO.           *
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

/** \file rmShow.cpp
	\brief rmShow.cpp implementation file.

	This contains the class definition to read the updated RM data into the CGF CSCI.
*/

#include <defutil.h>
#include <flun_var.h>
#include "readshow.h"
#include "data.h"
//#include "dbgout.h"
#include <mpt_show.h>
#include <muxshow.h>
#include <iostream>
#include <list>
#ifdef _WIN32
#include <windows.h>
#endif

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

testReadShow::testReadShow()
{
	addCallback(&multiDataRead.multData);	
}

testReadShow::~testReadShow()
{
	removeCallback(&multiDataRead.multData);	
}

//only update the logic of the element id
//user is notify with child being deleted or child updated content
void testReadShow::compute(int id)
{
	//std::cout << std::endl << "compute id: " << id << std::endl;
	static int iCnt = 0;
	
	complex_var *var = multiDataRead.multData.item(id); //operator[](id)
	if (var == 0) {
		// no child, the child was deleted

		//cout << "id deleted: " << id << endl;
		//this->delteIDDirty(id);
		//getchar();
		

	} else {
		int type = multiDataRead.multData.typeOf(id);
		//cout << "reading type " << type << endl;
		switch (type) { //user do something with the child updated object
			case 0:
				{					
					ooi_typ *obj = static_cast<ooi_typ *>(var);
					printOoiType(obj);					
					break;
				}
			case 1:
				{
					eei_typ *obj = static_cast<eei_typ *>(var);
					printEeiType(obj);
					break;
				}
			case 2:
				{
					flightres_polygon_typ *obj = static_cast<flightres_polygon_typ *>(var);
					printFlightRes_polygonType(obj);
					break;
				}
			case 3:
				{
					iCnt++;
					flightres_circle_typ *obj = static_cast<flightres_circle_typ *>(var);
					printFlightRes_circleType(obj);
					break;
				}
			case 4:
				{
					poi_typ *obj = static_cast<poi_typ *>(var);
					break;
				}
			default:
				break;
		}
	}	
}


/*
class ooi_typ : public complex_var
{
public:
COMPLEXCONST(ooi_typ)
generic_var<int> idx;	// Positional index of object
generic_var<int> valid;	// 0: Invalid OOI 1:Valid OOI 
generic_var<unsigned int> created;	// Date and time created 
generic_var<unsigned int> last_updated;	// Date and time last updated
flex_var<property_typ> ppty_data;	// Property values of OOI
cir_typ data;	// Details of OOI
flex_var<image_typ> img_data;	// List of tagged images
};

class cir_typ : public complex_var
{
public:
	COMPLEXCONST(cir_typ)
	string_var<16> label;	// Identifier of the circle. If invalid, then the object should be ignored 
	position_typ ctr_point;	// Center point of circle 
	generic_var<double> radius;	// Radius of circle in metres 
	generic_var<double> inner_radius;	// Radius of inner circle. Tentatively used for Sector only. 
	generic_var<double> arc_angle;	// Arc angle of sector. Tentatively used for Sector only. 
	generic_var<double> arc_length;	// Angle length. Tentatively used for Sector only. 
};

class position_typ : public complex_var
{
public:
	COMPLEXCONST(position_typ)
	generic_var<double> lat;	// Latitude
	generic_var<double> lon;	// Longitude 
	generic_var<double> alt;	// Altitude 
};

*/

void testReadShow::printOoiType(ooi_typ* obj)
{
	/*
	int cnt;
	
	cout << endl << endl;
	cout << "ooi_typ idx " << obj->idx << endl;
	cout << "valid " << obj->valid << endl;
	cout << "created " << obj->created << endl;
	cout << "last_updated " << obj->last_updated << endl;
	cout << "label " << obj->data.label << endl;
	cout << "lat " << obj->data.ctr_point.lat << endl;
	cout << "lon " << obj->data.ctr_point.lon << endl;
	cout << "alt " << obj->data.ctr_point.alt << endl;
	cout << "radius " << obj->data.radius << endl;
	cout << "inner_radius " << obj->data.inner_radius << endl;
	cout << "arc_angle " << obj->data.arc_angle << endl;
	cout << "arc_length " << obj->data.arc_length << endl;
	

	for (cnt = 0; cnt < obj->ppty_data.cardinal(); ++cnt)
	{
		cout << "ppty_data " << cnt << ":" << *(obj->ppty_data[cnt]) << endl;		
	}

	for (cnt = 0; cnt < obj->img_data.cardinal(); ++cnt)
	{
		cout << "img_data " << cnt << ":" << *(obj->img_data[cnt]) << endl;		
	}
	*/

}


/*
class eei_typ : public complex_var
{
public:
	COMPLEXCONST(eei_typ)
	generic_var<int> idx;	// Positional index of object
	generic_var<int> valid;	// 0: Invalid EEI 1:Valid EEI
	generic_var<unsigned int> created;	// Date and time created
	generic_var<unsigned int> last_updated;	// Date and time last updated
	flex_var<property_typ> ppty_data;	// Property values of item
	polygon_typ data;	// Details of EEI
};

class polygon_typ : public complex_var
{
public:
	COMPLEXCONST(polygon_typ)
	string_var<16> label;	// Identifier of the polygon. If invalid, then the object should be ignored
	flex_var<position_typ> points;	// Points of the polygon
};

class position_typ : public complex_var
{
public:
	COMPLEXCONST(position_typ)
	generic_var<double> lat;	// Latitude
	generic_var<double> lon;	// Longitude
	generic_var<double> alt;	// Altitude
};

*/

void testReadShow::printEeiType(eei_typ* obj)
{
	/*
	int cnt;
	cout << endl << endl;
	cout << "eei_typ idx " << obj->idx << endl;
	cout << "valid " << obj->valid << endl;
	cout << "created " << obj->created << endl;
	cout << "last_updated " << obj->last_updated << endl;
	
	//cout << "label " << obj->data.label << endl;
	for (cnt = 0; cnt < obj->data.points.cardinal(); ++cnt)
	{
		position_typ* pPoints = obj->data.points[cnt];
		cout << "lat " << pPoints->lat << endl;
		cout << "lon " << pPoints->lon << endl;
		cout << "alt " << pPoints->alt << endl;
	}
	
	for (cnt = 0; cnt < obj->ppty_data.cardinal(); ++cnt)
	{
		//cout << "ppty_data " << cnt << ":" << *(obj->ppty_data[cnt]) << endl;		
	}
	*/
}

void testReadShow::printFlightRes_polygonType(flightres_polygon_typ* obj)
{
	/*
	int cnt;
	cout << endl << endl;
	cout << "flightRes_polygon_typ idx " << obj->idx << endl;
	cout << "valid " << obj->valid << endl;
	cout << "created " << obj->created << endl;
	cout << "last_updated " << obj->last_updated << endl;
	
	cout << "label " << obj->data.label << endl;
	for (cnt = 0; cnt < obj->data.points.cardinal(); ++cnt)
	{
		position_typ* pPoints = obj->data.points[cnt];
		cout << "lat " << pPoints->lat << endl;
		cout << "lon " << pPoints->lon << endl;
		cout << "alt " << pPoints->alt << endl;
	}
	
	for (cnt = 0; cnt < obj->ppty_data.cardinal(); ++cnt)
	{
		cout << "ppty_data " << cnt << ":" << *(obj->ppty_data[cnt]) << endl;		
	}	
	*/
}

void testReadShow::printFlightRes_circleType(flightres_circle_typ* obj)
{
	/*
	int cnt;
	cout << endl << endl;
	cout << "flightRes_circle_typ idx " << obj->idx << endl;
	
	cout << "valid " << obj->valid << endl;
	cout << "created " << obj->created << endl;
	cout << "last_updated " << obj->last_updated << endl;
	
	cout << "label " << obj->data.label << endl;
	cout << "lat " << obj->data.ctr_point.lat << endl;
	cout << "lon " << obj->data.ctr_point.lon << endl;
	cout << "alt " << obj->data.ctr_point.alt << endl;
	cout << "radius " << obj->data.radius << endl;
	cout << "inner_radius " << obj->data.inner_radius << endl;
	cout << "arc_angle " << obj->data.arc_angle << endl;
	cout << "arc_length " << obj->data.arc_length << endl;
	
	for (cnt = 0; cnt < obj->ppty_data.cardinal(); ++cnt)
	{
		cout << "ppty_data " << cnt << ":" << *(obj->ppty_data[cnt]) << endl;		
	}

	for (cnt = 0; cnt < obj->img_data.cardinal(); ++cnt)
	{
		cout << "img_data " << cnt << ":" << *(obj->img_data[cnt]) << endl;		
	}
	*/
}

#ifndef _TEST_FLUN_VAR_H
#define _TEST_FLUN_VAR_H

#include <cxxtest/TestSuite.h>
#include "data.h"

class FlunVarTestSuite : public CxxTest::TestSuite 
{
public:
	void testCreateData(void);
	void testReadData(void); //read created data, include change
	void testDeleteData(void);
	void testReadData1(void);//read created data with 1 element deleted
	void testDeleteReadData(void); //delete all read data
	void testReadNoChange(void); //no change in multiDataWrite content
	void testMaxElement(void); //impose MAX_ELEMENT per send, excluding deleted children
	
private:

	ooi_typ ooiData; //type 0
	eei_typ eeiData; //type 1
	flightres_polygon_typ flightResPolyData; //type 2
	flightres_circle_typ flightResCirData; //type 3

	void writeRM(void);
	void readRM(void);
	void fillData(void);
	void assignData(void);
	void createData(void);
	void destroyData(int iTypeDestroy);
	void destroyData(void);
	void fillOoiData(ooi_typ* pOoiData);
	void fillEeiData(eei_typ* pEeiData);
	void fillFlightres_polygon_typData(flightres_polygon_typ* pFlightres_polygonData);
	void fillFlightres_circle_typData(flightres_circle_typ* pFlightres_circleData);

};

#endif

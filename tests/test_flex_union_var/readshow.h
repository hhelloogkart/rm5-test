/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
* Copyright DSO National Laboratories 2013. All Rights Reserved.   *
*                                                                  *
* This file may not be used without permission from DSO.           *
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

/** \file rmshow.h
	\brief rmshow.cpp header file.

  This contains the class declaration to read RM data into CGF CSCI.
  The RM data are for interfaces between CGF CSCI with UAV (6DOF) CSCI.
*/

#ifndef _RM_SHOW_H
#define _RM_SHOW_H

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
INCLUDES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

#include "math.h"
#include "data.h"
#include <flun_show.h>
#include <string>
#include <list>

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DEFINITIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

const double radtodeg = 57.295779513082320876798154814105;
const double degtorad = 0.017453292519943295769236907684886;
const double fttom = 0.3048;
const double mtoft = 1.0/0.3048;

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
FORWARD DECLARATIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
using namespace std;

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
CLASS DOCUMENTATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

class testReadShow : public flex_union_show
{
public:
	testReadShow();
	~testReadShow();
	virtual void compute(int id); //overload compute of flex_union_show

protected:
	void printOoiType(ooi_typ* obj);
	void printEeiType(eei_typ* obj);	
	void printFlightRes_polygonType(flightres_polygon_typ* obj);
	void printFlightRes_circleType(flightres_circle_typ* obj);
};

#endif /* _RM_SHOW_H */
#include <sstream>
#include <stdio.h>
#include <defutil.h>
#include <gen_var.h>
#include "test_flex_union_var.h"
#include <util/dbgout.h>
#include <defutil.h>
#include <util/utilcore.h>
#include <iostream>
#ifdef _WIN32
#include <windows.h>
#endif
#include "readshow.h"
#include "writeshow.h"


//to verify the number of elements and types created
void FlunVarTestSuite::testCreateData(void)
{
	createData();

	int iElement = multiDataWrite.multData.item_cardinal();
	int iItemID = multiDataWrite.multData.item_first_id();

	int iMatchType = 0;
	for(int iIndex=0; iIndex<iElement && iItemID!=-1; iIndex++)
	{
		//int id = it->first;
		complex_var *var = multiDataWrite.multData.item(iItemID);
		if(var!=0)
		{
			int type = multiDataWrite.multData.typeOf(iItemID);
			switch (type)
			{ //user do something with the child updated object
				case 0:
				{
					iMatchType++;
					break;
				}
				case 1:
				{
					iMatchType++;
					break;
				}
				case 2:
				{
					iMatchType++;
					break;
				}
				case 3:
				{
					iMatchType++;
					break;
				}
				case 4:
				{
					iMatchType++;
					break;
				}
				default:
					break;
			}//switch (type)

		}//if(var!=0)

		//std::cout << "testCreateData iItemID " << iItemID << std::endl;
		iItemID = multiDataWrite.multData.item_next_id(iItemID);
		//std::cout << "testCreateData next iItemID " << iItemID << std::endl;
	}

	TS_ASSERT_EQUALS(iElement, 3);
	TS_ASSERT_EQUALS(iMatchType, 3);


	writeRM();
	readRM();

}

void FlunVarTestSuite::testReadData(void)
{

	ooi_typ *ooi_typ_objRead, *ooi_typ_objWrite;
	eei_typ *eei_typ_objRead, *eei_typ_objWrite;
	flightres_polygon_typ *flightres_poltyp_objRead, *flightres_poltyp_objWrite;

	int iElement = multiDataRead.multData.item_cardinal();
	int iItemID = multiDataRead.multData.item_first_id();

	int iMatchType = 0;

	for(int iIndex=0; iIndex<iElement && iItemID!=-1; iIndex++)
	{
		complex_var *var = multiDataRead.multData.item(iItemID);
		if(var!=0)
		{
			int type = multiDataRead.multData.typeOf(iItemID);
			switch (type)
			{	//user do something with the child updated object
				case 0:
				{
					ooi_typ_objRead = static_cast<ooi_typ *>(var);
					iMatchType++;
					break;
				}
				case 1:
				{
					eei_typ_objRead = static_cast<eei_typ *>(var);
					iMatchType++;
					break;
				}
				case 2:
				{
					flightres_poltyp_objRead = static_cast<flightres_polygon_typ *>(var);
					iMatchType++;
					break;
				}
				case 3:
				{
					iMatchType++;
					break;
				}
				case 4:
				{
					iMatchType++;
					break;
				}
				default:
					break;
			}//switch (type)
		}//if(var!=0)



		//std::cout << "testReadData read iItemID " << iItemID << std::endl;
		iItemID = multiDataRead.multData.item_next_id(iItemID);
		//std::cout << "testReadData read next iItemID " << iItemID << std::endl;
	}

	TS_ASSERT_EQUALS(iElement, 3);
	TS_ASSERT_EQUALS(iMatchType, 3);

	iElement = multiDataWrite.multData.item_cardinal();
	iItemID = multiDataWrite.multData.item_first_id();
	for(int iIndex=0; iIndex<iElement && iItemID!=-1; iIndex++)
	{
		complex_var *var = multiDataWrite.multData.item(iItemID);
		if(var!=0)
		{
			int type = multiDataWrite.multData.typeOf(iItemID);
			switch (type)
			{ //user do something with the child updated object
				case 0:
				{
					ooi_typ_objWrite = static_cast<ooi_typ *>(var);
					break;
				}
				case 1:
				{
					eei_typ_objWrite = static_cast<eei_typ *>(var);
					break;
				}
				case 2:
				{
					flightres_poltyp_objWrite = static_cast<flightres_polygon_typ *>(var);
					break;
				}
				case 3:
				{
					break;
				}
				case 4:
				{
					break;
				}
				default:
					break;
			}//switch (type)

		}//if(var!=0)

		//std::cout << "testReadData write iItemID " << iItemID << std::endl;
		iItemID = multiDataWrite.multData.item_next_id(iItemID);
		//std::cout << "testReadData write next iItemID " << iItemID << std::endl;
	}

	//ooi_typ
	TS_ASSERT_EQUALS(ooi_typ_objRead->idx, ooi_typ_objWrite->idx);
	TS_ASSERT_EQUALS(ooi_typ_objRead->valid, ooi_typ_objWrite->valid);
	TS_ASSERT_EQUALS(ooi_typ_objRead->created, ooi_typ_objWrite->created);
	TS_ASSERT_EQUALS(ooi_typ_objRead->last_updated, ooi_typ_objWrite->last_updated);

	TS_ASSERT_EQUALS(ooi_typ_objRead->ppty_data.cardinal(), ooi_typ_objWrite->ppty_data.cardinal());
	for (int cnt = 0; cnt < ooi_typ_objRead->ppty_data.cardinal(); ++cnt)
	{
		TS_ASSERT_EQUALS(*(ooi_typ_objRead->ppty_data[cnt]), *(ooi_typ_objWrite->ppty_data[cnt]));
		//std::cout << "ooi_typ_objRead->ppty_data " << cnt << ":" << *(ooi_typ_objRead->ppty_data[cnt]) << endl;
		//std::cout << "ooi_typ_objWrite->ppty_data " << cnt << ":" << *(ooi_typ_objWrite->ppty_data[cnt]) << endl;
	}

	TS_ASSERT_EQUALS(ooi_typ_objRead->data.label, ooi_typ_objWrite->data.label);
	//std::cout << "ooi_typ_objRead->data.label " << ooi_typ_objRead->data.label << std::endl;
	//std::cout << "ooi_typ_objWrite->data.label " << ooi_typ_objWrite->data.label << std::endl;

	TS_ASSERT_EQUALS(ooi_typ_objRead->data.ctr_point.lat, ooi_typ_objWrite->data.ctr_point.lat);
	TS_ASSERT_EQUALS(ooi_typ_objRead->data.ctr_point.lon, ooi_typ_objWrite->data.ctr_point.lon);
	TS_ASSERT_EQUALS(ooi_typ_objRead->data.ctr_point.alt, ooi_typ_objWrite->data.ctr_point.alt);

	TS_ASSERT_EQUALS(ooi_typ_objRead->data.radius, ooi_typ_objWrite->data.radius);
	TS_ASSERT_EQUALS(ooi_typ_objRead->data.inner_radius, ooi_typ_objWrite->data.inner_radius);
	TS_ASSERT_EQUALS(ooi_typ_objRead->data.arc_angle, ooi_typ_objWrite->data.arc_angle);
	TS_ASSERT_EQUALS(ooi_typ_objRead->data.arc_length, ooi_typ_objWrite->data.arc_length);

	TS_ASSERT_EQUALS(ooi_typ_objRead->img_data.cardinal(), ooi_typ_objWrite->img_data.cardinal());
	for (int cnt = 0; cnt < ooi_typ_objRead->img_data.cardinal(); ++cnt)
	{
		TS_ASSERT_EQUALS(*(ooi_typ_objRead->img_data[cnt]), *(ooi_typ_objWrite->img_data[cnt]));
		//std::cout << "ooi_typ_objRead->img_data " << cnt << ":" << *(ooi_typ_objRead->img_data[cnt]) << endl;
		//std::cout << "ooi_typ_objWrite->img_data " << cnt << ":" << *(ooi_typ_objWrite->img_data[cnt]) << endl;
	}

	//eei_typ
	TS_ASSERT_EQUALS(eei_typ_objRead->idx, eei_typ_objWrite->idx);
	TS_ASSERT_EQUALS(eei_typ_objRead->valid, eei_typ_objWrite->valid);
	TS_ASSERT_EQUALS(eei_typ_objRead->created, eei_typ_objWrite->created);
	TS_ASSERT_EQUALS(eei_typ_objRead->last_updated, eei_typ_objWrite->last_updated);

	TS_ASSERT_EQUALS(eei_typ_objRead->ppty_data.cardinal(), eei_typ_objWrite->ppty_data.cardinal());
	for (int cnt = 0; cnt < eei_typ_objRead->ppty_data.cardinal(); ++cnt)
	{
		TS_ASSERT_EQUALS(*(eei_typ_objRead->ppty_data[cnt]), *(eei_typ_objWrite->ppty_data[cnt]));
		//std::cout << "eei_typ_objRead->ppty_data " << cnt << ":" << *(eei_typ_objRead->ppty_data[cnt]) << endl;
		//std::cout << "eei_typ_objWrite->ppty_data " << cnt << ":" << *(eei_typ_objWrite->ppty_data[cnt]) << endl;
	}

	TS_ASSERT_EQUALS(eei_typ_objRead->data.label, eei_typ_objWrite->data.label);
	//std::cout << "eei_typ_objRead->data.label " << eei_typ_objRead->data.label << std::endl;
	//std::cout << "eei_typ_objWrite->data.label " << eei_typ_objWrite->data.label << std::endl;

	TS_ASSERT_EQUALS(eei_typ_objRead->data.points.cardinal(), eei_typ_objWrite->data.points.cardinal());
	for (int cnt = 0; cnt < eei_typ_objRead->data.points.cardinal(); ++cnt)
	{
		position_typ posRead = *(eei_typ_objRead->data.points[cnt]);
		position_typ posWrite = *(eei_typ_objWrite->data.points[cnt]);
		TS_ASSERT_EQUALS(posRead.lat, posWrite.lat);
		TS_ASSERT_EQUALS(posRead.lon, posWrite.lon);
		TS_ASSERT_EQUALS(posRead.alt, posWrite.alt);
	}

	//flightres_polygon_typ
	TS_ASSERT_EQUALS(flightres_poltyp_objRead->idx, flightres_poltyp_objWrite->idx);
	TS_ASSERT_EQUALS(flightres_poltyp_objRead->valid, flightres_poltyp_objWrite->valid);
	TS_ASSERT_EQUALS(flightres_poltyp_objRead->created, flightres_poltyp_objWrite->created);
	TS_ASSERT_EQUALS(flightres_poltyp_objRead->last_updated, flightres_poltyp_objWrite->last_updated);

	TS_ASSERT_EQUALS(flightres_poltyp_objRead->ppty_data.cardinal(), flightres_poltyp_objWrite->ppty_data.cardinal());
	for (int cnt = 0; cnt < flightres_poltyp_objRead->ppty_data.cardinal(); ++cnt)
	{
		TS_ASSERT_EQUALS(*(flightres_poltyp_objRead->ppty_data[cnt]), *(flightres_poltyp_objWrite->ppty_data[cnt]));
		//std::cout << "flightres_poltyp_objRead->ppty_data " << cnt << ":" << *(flightres_poltyp_objRead->ppty_data[cnt]) << endl;
		//std::cout << "flightres_poltyp_objWrite->ppty_data " << cnt << ":" << *(flightres_poltyp_objWrite->ppty_data[cnt]) << endl;
	}

	TS_ASSERT_EQUALS(flightres_poltyp_objRead->data.points.cardinal(), flightres_poltyp_objWrite->data.points.cardinal());
	for (int cnt = 0; cnt < eei_typ_objRead->data.points.cardinal(); ++cnt)
	{
		position_typ posRead = *(flightres_poltyp_objRead->data.points[cnt]);
		position_typ posWrite = *(flightres_poltyp_objWrite->data.points[cnt]);
		TS_ASSERT_EQUALS(posRead.lat, posWrite.lat);
		TS_ASSERT_EQUALS(posRead.lon, posWrite.lon);
		TS_ASSERT_EQUALS(posRead.alt, posWrite.alt);
	}
}

void FlunVarTestSuite::testDeleteData(void)
{
	int iTypeDestroy = 1; //eei_typ
	destroyData(iTypeDestroy);

	writeRM();
	readRM();
}

void FlunVarTestSuite::testDeleteReadData(void)
{
	while(multiDataRead.multData.item_cardinal()>0)
	{
		int iItemID = multiDataRead.multData.item_first_id();
		//std::cout << "testDeleteReadData iItemID " << iItemID << std::endl;
		multiDataRead.multData.destroy(iItemID);
	}

	int iElement = multiDataRead.multData.item_cardinal();
	TS_ASSERT_EQUALS(iElement, 0);
}

void FlunVarTestSuite::testReadData1(void)
{
	int iElement = multiDataRead.multData.item_cardinal();
	int iItemID = multiDataRead.multData.item_first_id();

	int iMatchType = 0;
	for(int iIndex=0; iIndex<iElement && iItemID!=-1; iIndex++)
	{
		complex_var *var = multiDataRead.multData.item(iItemID);
		if(var!=0)
		{
			int type = multiDataRead.multData.typeOf(iItemID);
			switch (type)
			{ //user do something with the child updated object
				case 0:
				{
					iMatchType++;
					break;
				}
				case 1:
				{
					//iMatchType++;
					break;
				}
				case 2:
				{
					iMatchType++;
					break;
				}
				case 3:
				{

					break;
				}
				case 4:
				{

					break;
				}
				default:
					break;
			}//switch (type)

		}//if(var!=0)

		//std::cout << "testReadData1 iItemID " << iItemID << std::endl;
		iItemID = multiDataRead.multData.item_next_id(iItemID);
		//std::cout << "testReadData1 next iItemID " << iItemID << std::endl;
	}

	TS_ASSERT_EQUALS(iElement, 2);
	TS_ASSERT_EQUALS(iMatchType, 2);

}


//no change in multiDataWrite content
void FlunVarTestSuite::testReadNoChange(void)
{
	writeRM();
	readRM();

	int iElement = multiDataRead.multData.item_cardinal();
	int iItemID = multiDataRead.multData.item_first_id();

	int iMatchType = 0;
	for(int iIndex=0; iIndex<iElement && iItemID!=-1; iIndex++)
	{
		complex_var *var = multiDataRead.multData.item(iItemID);
		if(var!=0)
		{
			int type = multiDataRead.multData.typeOf(iItemID);
			switch (type)
			{ //user do something with the child updated object
				case 0:
				{
					iMatchType++;
					break;
				}
				case 1:
				{
					//iMatchType++;
					break;
				}
				case 2:
				{
					iMatchType++;
					break;
				}
				case 3:
				{
					break;
				}
				case 4:
				{
					break;
				}
				default:
					break;
			}//switch (type)

		}//if(var!=0)

		//std::cout << "testReadNoChange iItemID " << iItemID << std::endl;
		iItemID = multiDataRead.multData.item_next_id(iItemID);
		//std::cout << "testReadNoChange next iItemID " << iItemID << std::endl;
	}

	TS_ASSERT_EQUALS(iElement, 0);
	TS_ASSERT_EQUALS(iMatchType, 0);

}

void FlunVarTestSuite::testMaxElement(void)
{
	//int iElement = multiDataWrite.multData.item_cardinal();
	destroyData();
	int iElement = multiDataWrite.multData.item_cardinal();
	//std::cout << "iElementd " << iElement << std::endl;
	TS_ASSERT_EQUALS(iElement, 0);

	//create 500 elements of flightres_polygon_typ
	int id = -1;
	int iCreateNum = 0;
	while(iCreateNum<500)
	{
		iCreateNum++;
		int iType = 2;
		multiDataWrite.multData.create(iType, &id);
	}

	assignData();

	multiDataWrite.multData.setRMDirty();
	writeRM();
	readRM();
	iElement = multiDataRead.multData.item_cardinal();
	TS_ASSERT_EQUALS(iElement, 200);

	multiDataWrite.multData.setRMDirty();
	writeRM();
	readRM();
	iElement = multiDataRead.multData.item_cardinal();
	TS_ASSERT_EQUALS(iElement, 400);

	multiDataWrite.multData.setRMDirty();
	writeRM();
	readRM();
	iElement = multiDataRead.multData.item_cardinal();
	TS_ASSERT_EQUALS(iElement, 500);
}

void FlunVarTestSuite::writeRM(void)
{
	multiDataWrite.setRegister(false);

	//connect to RM
	const char settings[] =
		"client = RMSCRIPT\n"
		"host = RM5\n"
		"port = 0\n"
		"access = SM\n"
		"flow = 0\n";
	rmclient_init init(settings);

	init.incoming();
	mpt_show::refresh();
	init.outgoing();
}

void FlunVarTestSuite::readRM(void)
{
	//switch to input (register variable)
	multiDataRead.setRegister(true);

	//connect to RM
	const char settings[] =
		"client = RMSCRIPT\n"
		"host = RM5\n"
		"port = 0\n"
		"access = SM\n"
		"flow = 0\n";
	rmclient_init init(settings);

	init.incoming();
	mpt_show::refresh();
	init.outgoing();

	//int iElement = multiDataRead.multData.item_cardinal();
	//std::cout << std::endl;
	//std::cout << "iElement: " << iElement << std::endl;
}


void FlunVarTestSuite::destroyData(int iTypeDestroy)
{
	int iElement = multiDataWrite.multData.item_cardinal();
	int iItemID = multiDataWrite.multData.item_first_id();
	for(int iIndex=0; iIndex<iElement && iItemID!=-1; iIndex++)
	{
		int iType = multiDataWrite.multData.typeOf(iItemID);
		if(iType == iTypeDestroy)
		{
			multiDataWrite.multData.destroy(iItemID);
			//multiDataWrite.multData.setRMDirty(); //specially hardcore, esle will not work
		}
		iItemID = multiDataWrite.multData.item_next_id(iItemID);
	}
}

void FlunVarTestSuite::destroyData(void)
{
	while(multiDataWrite.multData.item_cardinal()>0)
	{
		int iItemID = multiDataWrite.multData.item_first_id();
		multiDataWrite.multData.destroy(iItemID);
	}
}

void FlunVarTestSuite::createData(void)
{
	fillData();

	int iType = -1;
	int id = -1;

	iType = 0;
	multiDataWrite.multData.create(iType, &id);
	//std::cout << "create type0 id: " << id << std::endl;
	iType = 1;
	multiDataWrite.multData.create(iType, &id);
	//std::cout << "create type1 id: " << id << std::endl;
	iType = 2;
	multiDataWrite.multData.create(iType, &id);
	//std::cout << "create type2 id: " << id << std::endl;
	//iType = 3;
	//multiDataWrite.multData.create(iType, &id);

	assignData();
}


void FlunVarTestSuite::fillData(void)
{
	fillOoiData(&ooiData);
	fillEeiData(&eeiData);
	fillFlightres_polygon_typData(&flightResPolyData);
	fillFlightres_circle_typData(&flightResCirData);
}

void FlunVarTestSuite::assignData(void)
{
	int iElement = multiDataWrite.multData.item_cardinal();
	int iItemID = multiDataWrite.multData.item_first_id();

	for(int iIndex=0; iIndex<iElement && iItemID!=-1; iIndex++)
	{
		complex_var *var = multiDataWrite.multData.item(iItemID);
		if(var!=0)
		{
			int type = multiDataWrite.multData.typeOf(iItemID);
			switch (type)
			{ //user do something with the child updated object
				case 0:
				{
					ooi_typ *obj = static_cast<ooi_typ *>(var);
					ooiData.idx = iItemID;
					*obj = ooiData;
					break;
				}
				case 1:
				{
					eei_typ *obj = static_cast<eei_typ *>(var);
					eeiData.idx = iItemID;
					*obj = eeiData;
					break;
				}
				case 2:
				{
					flightres_polygon_typ *obj = static_cast<flightres_polygon_typ *>(var);
					flightResPolyData.idx = iItemID;
					*obj = flightResPolyData;
					break;
				}
				case 3:
				{
					//flightres_circle_typ *obj = static_cast<flightres_circle_typ *>(var);
					//flightResCirData.idx = iItemID;
					//*obj = flightResCirData;
					break;
				}
				case 4:
				{
					//poi_typ *obj = static_cast<poi_typ *>(var); //not used
					break;
				}
				default:
					break;
			}//switch (type)

		}//if(var!=0)

		iItemID = multiDataWrite.multData.item_next_id(iItemID);
	}//for(int iIndex=0; iIndex<iElement && iItemID!=-1; iIndex++)
}

void FlunVarTestSuite::fillOoiData(ooi_typ* pOoiData)
{
	cir_typ cirData;
	cirData.label = "cirLabel";
	cirData.ctr_point.lat = 1.01;
	cirData.ctr_point.lon = 1.02;
	cirData.ctr_point.alt = 1.03;
	cirData.radius = 1.4;
	cirData.inner_radius = 1.5;
	cirData.arc_angle = 1.6;
	cirData.arc_length = 1.7;

	char cPptyDataArr[30];
	property_typ ppty_data;
	for(int i=0; i<5; i++)
	{
		sprintf(cPptyDataArr, "ooiData_%d", i);
		ppty_data.value = cPptyDataArr;
		strcpy(ppty_data.value, cPptyDataArr);
		pOoiData->ppty_data.add(ppty_data);
	}


	char cImageDataArr[30];
	image_typ imageData;
	for(int i=0; i<3; i++)
	{
		sprintf(cImageDataArr, "imageType_%d", i);
		imageData.img_name = cImageDataArr;
		strcpy(imageData.img_name, cImageDataArr);
		pOoiData->img_data.add(imageData);
	}


	pOoiData->idx = -1;
	pOoiData->valid = 1;
	pOoiData->created = 1;
	pOoiData->last_updated = 1;
	pOoiData->data = cirData;

}

void FlunVarTestSuite::fillEeiData(eei_typ* pEeiData)
{
	polygon_typ polyData;
	position_typ pos;

	polyData.label = "polyLabel";

	for(int i=0; i<2; i++)
	{
		pos.lat = 2.00 + i * 0.01;
		pos.lon = 2.00 + i * 0.02;
		pos.alt = 2.00 + i * 0.03;
		polyData.points.add(pos);
	}


	char cPptyDataArr[30];
	property_typ ppty_data;
	for(int i=0; i<3; i++)
	{
		sprintf(cPptyDataArr, "eeiData_%d", i);
		ppty_data.value = cPptyDataArr;
		strcpy(ppty_data.value, cPptyDataArr);
		pEeiData->ppty_data.add(ppty_data);
	}

	pEeiData->idx = -1;
	pEeiData->valid = 2;
	pEeiData->created = 2;
	pEeiData->last_updated = 2;
	pEeiData->data = polyData;

}

void FlunVarTestSuite::fillFlightres_polygon_typData(flightres_polygon_typ* pFlightres_polygonData)
{
	polygon_typ polyData;
	position_typ pos;

	polyData.label = "flightResPolyLabel";

	for(int i=0; i<4; i++)
	{
		pos.lat = 3.00 + i * 0.01;
		pos.lon = 3.00 + i * 0.02;
		pos.alt = 3.00 + i * 0.03;
		polyData.points.add(pos);
	}

	char cPptyDataArr[30];
	property_typ ppty_data;
	for(int i=0; i<5; i++)
	{
		sprintf(cPptyDataArr, "flightResPolyData_%d", i);
		ppty_data.value = cPptyDataArr;
		strcpy(ppty_data.value, cPptyDataArr);
		pFlightres_polygonData->ppty_data.add(ppty_data);
	}

	pFlightres_polygonData->idx = -1;
	pFlightres_polygonData->valid = 3;
	pFlightres_polygonData->created = 3;
	pFlightres_polygonData->last_updated = 3;
	pFlightres_polygonData->data = polyData;

}

void FlunVarTestSuite::fillFlightres_circle_typData(flightres_circle_typ* pFlightres_circleData)
{
	cir_typ cirData;
	cirData.label = "fightResCirLabel";
	cirData.ctr_point.lat = 4.01;
	cirData.ctr_point.lon = 4.02;
	cirData.ctr_point.alt = 4.03;
	cirData.radius = 4.4;
	cirData.inner_radius = 4.5;
	cirData.arc_angle = 4.6;
	cirData.arc_length = 4.7;

	char cPptyDataArr[30];
	property_typ ppty_data;
	for(int i=0; i<6; i++)
	{
		sprintf(cPptyDataArr, "flightResCircleData_%d", i);
		ppty_data.value = cPptyDataArr;
		strcpy(ppty_data.value, cPptyDataArr);
		pFlightres_circleData->ppty_data.add(ppty_data);
	}

	char cImageDataArr[30];
	image_typ imageData;
	for(int i=0; i<6; i++)
	{
		sprintf(cImageDataArr, "flightResCicleimageType_%d", i);
		imageData.img_name = cImageDataArr;
		strcpy(imageData.img_name, cImageDataArr);
		pFlightres_circleData->img_data.add(imageData);
	}

	pFlightres_circleData->idx = -1;
	pFlightres_circleData->valid = 4;
	pFlightres_circleData->created = 4;
	pFlightres_circleData->last_updated = 4;
	pFlightres_circleData->data = cirData;

}

/*
void FlunVarTestSuite::testWrite(void)
{
	u3_selector = 1;

	writeRM();
	readRM();
}

void FlunVarTestSuite::setData(void)
{
	//set data
	test_wr_var.u1.c1.a = 1;
	test_wr_var.u1.c1.b = 2;

	test_wr_var.u2.c1.a = 11;
	test_wr_var.u2.c1.b = 22;

	test_wr_var.u3.c1.a = 100;
	test_wr_var.u3.c2.a = 200;
	test_wr_var.u3.c3.a = 300;
	test_wr_var.u3.c4.a = 400;

	test_wr_var.u4.c1.a = 11;
	test_wr_var.u4.c2.a = 22;
	test_wr_var.u4.c3.a = 33;

	test_wr_var.a = 0xAAAAAAAA;
	test_wr_var.b = 0xBBBBBBBB;
	test_wr_var.c = 0xCCCCCCCC;
	test_wr_var.d = 0xDDDDDDDD;
}

void FlunVarTestSuite::clearData(void)
{
}

void FlunVarTestSuite::checkData(void)
{
	//test data rx correctly
	TS_ASSERT(test_wr_var.u1.c1.a == 1);
	TS_ASSERT(test_wr_var.u1.c1.b == (unsigned short)2);
	TS_ASSERT(test_wr_var.u2.c1.a == 11);
	TS_ASSERT(test_wr_var.u2.c1.b == (unsigned short)22);
	TS_ASSERT(test_wr_var.u4.c3.a == 33);

	switch(u3_selector)
	{
	case 0:
		TS_ASSERT(test_wr_var.u3.c1.a == 100);
		break;
	case 1:
		TS_ASSERT(test_wr_var.u3.c2.a == 200);
		break;
	case 2:
		TS_ASSERT(test_wr_var.u3.c3.a == 300);
		break;
	case 3:
		TS_ASSERT(test_wr_var.u3.c4.a == 400);
		break;
	}

	//test data before and after union are correctly decoded
	TS_ASSERT(test_wr_var.a == 0xAAAAAAAA);
	TS_ASSERT(test_wr_var.b == 0xBBBBBBBB);
	TS_ASSERT(test_wr_var.c == 0xCCCCCCCC);
	TS_ASSERT(test_wr_var.d == 0xDDDDDDDD);

}
*/

#ifndef _DATA_H_
#define _DATA_H_

/* %%RM_VAR_DEF_INCLUDE_BEGIN%% */
#include <rm_var.h>
#include <gen_var.h>
#include <cplx_var.h>
#include <arry_var.h>
#include <flex_var.h>
#include <str_var.h>
#include <flun_var.h>
#include <map>
#include <list>

/* %%RM_VAR_DEF_INCLUDE_END%% */

/* %%RM_VAR_DEFINITIONS_BEGIN%% */
/*!
	Latitude and Longitude coordinates
*/
class position_typ : public complex_var
{
public:
	COMPLEXCONST(position_typ)
	generic_var<double> lat;	/*!< Latitude */
	generic_var<double> lon;	/*!< Longitude */
	generic_var<double> alt;	/*!< Altitude  */
};
/*!
	Property Values for each object
*/
class property_typ : public complex_var
{
public:
	COMPLEXCONST(property_typ)
	array_str_var value;	/*!< Value of each property */
};
/*!
	Description of a circle
*/
class cir_typ : public complex_var
{
public:
	COMPLEXCONST(cir_typ)
	string_var<16> label;	/*!< Identifier of the circle. If invalid, then the object should be ignored */
	position_typ ctr_point;	/*!< Center point of circle */
	generic_var<double> radius;	/*!< Radius of circle in metres */
	generic_var<double> inner_radius;	/*!< Radius of inner circle. Tentatively used for Sector only. */
	generic_var<double> arc_angle;	/*!< Arc angle of sector. Tentatively used for Sector only. */
	generic_var<double> arc_length;	/*!< Angle length. Tentatively used for Sector only. */
};
/*!
	Tagged image data
*/
class image_typ : public complex_var
{
public:
	COMPLEXCONST(image_typ)
	array_str_var img_name;	/*!< Name of tagged image */
};
/*!
	OOI
*/
class ooi_typ : public complex_var
{
public:
	COMPLEXCONST(ooi_typ)
	generic_var<int> idx;	/*!< Positional index of object */
	generic_var<int> valid;	/*!< 0: Invalid OOI 1:Valid OOI */
	generic_var<unsigned int> created;	/*!< Date and time created */
	generic_var<unsigned int> last_updated;	/*!< Date and time last updated */
	flex_var<property_typ> ppty_data;	/*!< Property values of OOI */
	cir_typ data;	/*!< Details of OOI */
	flex_var<image_typ> img_data;	/*!< List of tagged images */
};
/*!
	Description of a polygon
*/
class polygon_typ : public complex_var
{
public:
	COMPLEXCONST(polygon_typ)
	string_var<16> label;	/*!< Identifier of the polygon. If invalid, then the object should be ignored */
	flex_var<position_typ> points;	/*!< Points of the polygon */
};
/*!
	EEI
*/
class eei_typ : public complex_var
{
public:
	COMPLEXCONST(eei_typ)
	generic_var<int> idx;	/*!< Positional index of object */
	generic_var<int> valid;	/*!< 0: Invalid EEI 1:Valid EEI */
	generic_var<unsigned int> created;	/*!< Date and time created */
	generic_var<unsigned int> last_updated;	/*!< Date and time last updated */
	flex_var<property_typ> ppty_data;	/*!< Property values of item */
	polygon_typ data;	/*!< Details of EEI */
};
/*!
	Restricted Polygons
*/
class flightres_polygon_typ : public complex_var
{
public:
	COMPLEXCONST(flightres_polygon_typ)
	generic_var<int> idx;	/*!< Positional index of object */
	generic_var<int> valid;	/*!< 0: Invalid restricted polygon 1:Valid restricted polygon */
	generic_var<unsigned int> created;	/*!< Date and time created */
	generic_var<unsigned int> last_updated;	/*!< Date and time last updated */
	flex_var<property_typ> ppty_data;	/*!< Property values of item */
	polygon_typ data;	/*!< Details of restricted polygon */
};
/*!
	Restricted Circles
*/
class flightres_circle_typ : public complex_var
{
public:
	COMPLEXCONST(flightres_circle_typ)
	generic_var<int> idx;	/*!< Positional index of object */
	generic_var<int> valid;	/*!< 0: Invalid restricted circle 1:Valid restricted circle */
	generic_var<unsigned int> created;	/*!< Date and time created */
	generic_var<unsigned int> last_updated;	/*!< Date and time last updated */
	flex_var<property_typ> ppty_data;	/*!< Property values of retricted circles */
	cir_typ data;	/*!< Details of restricted circle */
	flex_var<image_typ> img_data;	/*!< List of tagged images */
};
/*!
	POI
*/
class poi_typ : public complex_var
{
public:
	COMPLEXCONST(poi_typ)
	generic_var<int> idx;	/*!< Positional index of object */
	generic_var<int> valid;	/*!< 0: Invalid POI 1:Valid POI */
	generic_var<unsigned int> created;	/*!< Date and time created */
	generic_var<unsigned int> last_updated;	/*!< Date and time last updated */
	flex_var<property_typ> ppty_data;	/*!< Property values of POI */
	cir_typ data;	/*!< Details of POI */
	flex_var<image_typ> img_data;	/*!< List of tagged images */
};
/*!
     This flex union var will contain a list of
     either ooi_typ, eei_typ, flightres_polygon_typ, flightres_circle_typ, or poi_typ

     It is a variable length list just like flex_var but
     each element can be one of the types
*/
class multi_typ : public flex_union_var
{
public:
	
    FLEXUNIONCONST(multi_typ) //store complex_factor_base to prvt

private:
    complex_factory<ooi_typ,0> f1; 
    complex_factory<eei_typ,1> f2;
    complex_factory<flightres_polygon_typ,2> f3;
    complex_factory<flightres_circle_typ,3> f4;
    complex_factory<poi_typ,4> f5; //not used
};


class multiTypeTest : public rm_var
{
public:
	RMCONST(multiTypeTest,"MULTITYPE_TEST",1)
	multi_typ multData;
	generic_var<int> iData;	
};

/* %%RM_VAR_DEFINITIONS_END%% */

/* %%RM_VAR_EXTERNS_BEGIN%% */
extern multiTypeTest multiDataWrite;
extern multiTypeTest multiDataRead;
/* %%RM_VAR_EXTERNS_END%% */

#endif /* _DATA_H_ */
/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_flex_union_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_FlunVarTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_flex_union_var\test_flex_union_var.h"

static FlunVarTestSuite suite_FlunVarTestSuite;

static CxxTest::List Tests_FlunVarTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_FlunVarTestSuite( "test_flex_union_var/test_flex_union_var.h", 7, "FlunVarTestSuite", suite_FlunVarTestSuite, Tests_FlunVarTestSuite );

static class TestDescription_suite_FlunVarTestSuite_testCreateData : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FlunVarTestSuite_testCreateData() : CxxTest::RealTestDescription( Tests_FlunVarTestSuite, suiteDescription_FlunVarTestSuite, 10, "testCreateData" ) {}
 void runTest() { suite_FlunVarTestSuite.testCreateData(); }
} testDescription_suite_FlunVarTestSuite_testCreateData;

static class TestDescription_suite_FlunVarTestSuite_testReadData : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FlunVarTestSuite_testReadData() : CxxTest::RealTestDescription( Tests_FlunVarTestSuite, suiteDescription_FlunVarTestSuite, 11, "testReadData" ) {}
 void runTest() { suite_FlunVarTestSuite.testReadData(); }
} testDescription_suite_FlunVarTestSuite_testReadData;

static class TestDescription_suite_FlunVarTestSuite_testDeleteData : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FlunVarTestSuite_testDeleteData() : CxxTest::RealTestDescription( Tests_FlunVarTestSuite, suiteDescription_FlunVarTestSuite, 12, "testDeleteData" ) {}
 void runTest() { suite_FlunVarTestSuite.testDeleteData(); }
} testDescription_suite_FlunVarTestSuite_testDeleteData;

static class TestDescription_suite_FlunVarTestSuite_testReadData1 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FlunVarTestSuite_testReadData1() : CxxTest::RealTestDescription( Tests_FlunVarTestSuite, suiteDescription_FlunVarTestSuite, 13, "testReadData1" ) {}
 void runTest() { suite_FlunVarTestSuite.testReadData1(); }
} testDescription_suite_FlunVarTestSuite_testReadData1;

static class TestDescription_suite_FlunVarTestSuite_testDeleteReadData : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FlunVarTestSuite_testDeleteReadData() : CxxTest::RealTestDescription( Tests_FlunVarTestSuite, suiteDescription_FlunVarTestSuite, 14, "testDeleteReadData" ) {}
 void runTest() { suite_FlunVarTestSuite.testDeleteReadData(); }
} testDescription_suite_FlunVarTestSuite_testDeleteReadData;

static class TestDescription_suite_FlunVarTestSuite_testReadNoChange : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FlunVarTestSuite_testReadNoChange() : CxxTest::RealTestDescription( Tests_FlunVarTestSuite, suiteDescription_FlunVarTestSuite, 15, "testReadNoChange" ) {}
 void runTest() { suite_FlunVarTestSuite.testReadNoChange(); }
} testDescription_suite_FlunVarTestSuite_testReadNoChange;

static class TestDescription_suite_FlunVarTestSuite_testMaxElement : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FlunVarTestSuite_testMaxElement() : CxxTest::RealTestDescription( Tests_FlunVarTestSuite, suiteDescription_FlunVarTestSuite, 16, "testMaxElement" ) {}
 void runTest() { suite_FlunVarTestSuite.testMaxElement(); }
} testDescription_suite_FlunVarTestSuite_testMaxElement;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

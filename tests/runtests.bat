@ECHO OFF
setlocal enabledelayedexpansion enableextensions
DEL /F /S /Q coverage
DEL /F /S /Q xunit
MKDIR coverage
MKDIR xunit
SET _r1=%~dp0
SET _r2=%_r1:~0,-6%
SET _r3=%_r2:~0,-9%oscore\

SET PATH=%_r2%lib;%_r3%lib;C:\Qt\Qt5.8.0\5.8\msvc2015_64\bin;%PATH%
REM ===OpenCppCoverage===
SET PATH=%PATH%;"C:\Program Files\OpenCppCoverage"
REM ===Inspector=========
SET PATH=%PATH%;"C:\Program Files (x86)\Intel\oneAPI\inspector\2021.2.0\bin64"

ECHO sources=%_r2% > opencppcoverage.cfg
ECHO sources=%_r3% >> opencppcoverage.cfg
ECHO modules=%_r2% >> opencppcoverage.cfg
ECHO modules=%_r3% >> opencppcoverage.cfg
FOR /D %%G IN ("test_*") DO (
ECHO %%G
  IF EXIST %%G\run.ini (
    runutil\debug\runutil.exe %%G\run.ini xunit\%%G_%%1.xml
  ) ELSE (
    opencppcoverage.exe --modules %_r2% --modules %_r3% --sources %_r2% --modules %_r3% --working_dir %%G --export_type binary:coverage\%%G.cov -- %%G\debug\%%G.exe > xunit\%%G.xml
    inspxe-cl.exe -collect mi2 -app-working-dir %CD%\%%G -suppression-file %CD%\inspxe.sup -result-dir %CD%\inspxe\%%G -search-dir all:r=%_r2% -- debug\%%G.exe
	inspxe-cl.exe -report problems -report-all -result-dir %CD%\inspxe\%%G -report-output %CD%\inspxe\%%G\%%G_report.txt
  )
)
del opencppcoverage.cfg
set LIST=
for %%x in (coverage\*.cov) do set LIST=!LIST! --input_coverage %%x
set LIST=%LIST:~1%
opencppcoverage.exe %LIST% --modules %_r2% --modules %_r3% --sources %_r2% --modules %_r3% --export_type html:coverage\html --export_type cobertura:coverage\xml\coverage.xml
extentreports-dotnet-cli\ExtentReportsDotNetCLI\ExtentReportsDotNetCLI\bin\Debug\extent.exe -d xunit\ -o xunit\html\ --merge
#ifndef _CDATA_H_
#define _CDATA_H_

/* %%RM_VAR_STRUCT_DEFINITIONS_BEGIN%% */
typedef struct
{
	unsigned char uch;
	char ch;
	unsigned short ush;
	short sh;
	unsigned int uin;
	int in;
	unsigned long ulg;
	long lg;
	unsigned long long ulglg;
	long long lglg;
	float flt;
	double dbl;
} gen_test_typ;
typedef struct
{
	unsigned char uch[3];
	char ch[3];
	unsigned short ush[3];
	short sh[3];
	unsigned int uin[3];
	int in[3];
	unsigned long ulg[3];
	long lg[3];
	unsigned long long ulglg[3];
	long long lglg[3];
	float flt[3];
	double dbl[3];
} arry_test_typ;
typedef struct
{
	int bv_1a;
	int bv_2a;
	int bv_6a;
	int bv_1b;
	int bv_8b;
	int bv_16b;
	int bv_24b;
	int bv_32b;
	int bv_7b;
	int bv_1c;
	int bv_15c;
	int bv_1d;
	int bv_23d;
	int bv_1e;
	int bv_31e;
	int bv_1f;
	int rbv_1f;
	int rbv_2f;
	int rbv_6f;
	int rbv_8f;
	int rbv_9f;
	int rbv_16f;
	int rbv_17f;
	int rbv_24f;
	int rbv_25f;
	int rbv_32f;
	int in;
	int rbv_3g;
	int bv_3g;
	int rbv_4g;
	int bv_2g[3];
	int rbv_2g[3];
	int rbv_5g;
	int bv_10g[3];
	int rbv_10g[3];
	int bv_7g;
} bit_test_typ;
typedef struct
{
	double dv_char;
	double dv_short;
	double dv_int;
	double rdv_char;
	double rdv_short;
	double rdv_int;
	double adv_char[3];
	double adv_int[3];
	double radv_short[3];
} decode_test_typ;
typedef struct
{
	double dbv_4;
	double dbv_12;
	int bv_3a;
	double dbv_20;
	double dbv_28;
	double rdbv_4;
	double rdbv_12;
	int bv_3b;
	double rdbv_20;
	double rdbv_28;
	int rbv_3c;
	double rdbv_5;
} decode_bit_test_typ;
/* %%RM_VAR_STRUCT_DEFINITIONS_END%% */

#ifdef __cplusplus
extern "C" {
#endif
/* %%RM_VAR_STRUCT_EXTERN_BEGIN%% */
extern gen_test_typ g1;
extern arry_test_typ a1;
extern bit_test_typ b1;
extern decode_test_typ d1;
extern decode_bit_test_typ db1;
/* %%RM_VAR_STRUCT_EXTERN_END%% */
#ifdef __cplusplus
}
#endif

#endif /* _CDATA_H_ */

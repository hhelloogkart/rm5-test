#ifndef _TEST_STRUCT_VAR_H_DEF_
#define _TEST_STRUCT_VAR_H_DEF_

#include <cxxtest/TestSuite.h>

class StructVarTestSuite : public CxxTest::TestSuite
{
public:
    void testExtractInit(void);
    void testExtractGenericVars(void);
    void testExtractGenericVarArrays(void);
    void testExtractBitVars(void);
    void testExtractDecodeVars(void);
    void testExtractDecodeBitVars(void);
    void testPackOutput(void);
    void testStream(void);
    void testCopyAndAssignment1();
    void testCopyAndAssignment2();
};

#endif

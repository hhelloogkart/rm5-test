#include "cdata.h"
#include <rmcdata.h>

/* %%RM_VAR_STRUCT_INST_BEGIN%% */
gen_test_typ g1;
void * gen_test_typ_g1_ptr = &g1;
arry_test_typ a1;
void *const arry_test_typ_a1_ptr = &a1;
bit_test_typ b1;
void *const bit_test_typ_b1_ptr = &b1;
decode_test_typ d1;
void *const decode_test_typ_d1_ptr = &d1;
decode_bit_test_typ db1;
void *const decode_bit_test_typ_db1_ptr = &db1;
/* %%RM_VAR_STRUCT_INST_END%% */

/* %%RM_VAR_STRUCT_EXTR_BEGIN%% */
unsigned int gen_test_typ_extract(gen_test_typ *dest, int num, const unsigned char *buf, int *len)
{
	int beginLen = *len;
	int count[2];

	for (count[0]=0; count[0]<num; ++count[0])
	{
		buf+=unsigned_char_extract(&(dest->uch),1,buf,len);
		buf+=char_extract(&(dest->ch),1,buf,len);
		buf+=unsigned_short_extract(&(dest->ush),1,buf,len);
		buf+=short_extract(&(dest->sh),1,buf,len);
		buf+=unsigned_int_extract(&(dest->uin),1,buf,len);
		buf+=int_extract(&(dest->in),1,buf,len);
		buf+=unsigned_long_extract(&(dest->ulg),1,buf,len);
		buf+=long_extract(&(dest->lg),1,buf,len);
		buf+=unsigned_long_long_extract(&(dest->ulglg),1,buf,len);
		buf+=long_long_extract(&(dest->lglg),1,buf,len);
		buf+=float_extract(&(dest->flt),1,buf,len);
		buf+=double_extract(&(dest->dbl),1,buf,len);
		++dest;
	}
	return (beginLen - *len);
}
unsigned int arry_test_typ_extract(arry_test_typ *dest, int num, const unsigned char *buf, int *len)
{
	int beginLen = *len;
	int count[2];

	for (count[0]=0; count[0]<num; ++count[0])
	{
		buf+=unsigned_char_extract(&(dest->uch[0]),3,buf,len);
		buf+=char_extract(&(dest->ch[0]),3,buf,len);
		buf+=unsigned_short_extract(&(dest->ush[0]),3,buf,len);
		buf+=short_extract(&(dest->sh[0]),3,buf,len);
		buf+=unsigned_int_extract(&(dest->uin[0]),3,buf,len);
		buf+=int_extract(&(dest->in[0]),3,buf,len);
		buf+=unsigned_long_extract(&(dest->ulg[0]),3,buf,len);
		buf+=long_extract(&(dest->lg[0]),3,buf,len);
		buf+=unsigned_long_long_extract(&(dest->ulglg[0]),3,buf,len);
		buf+=long_long_extract(&(dest->lglg[0]),3,buf,len);
		buf+=float_extract(&(dest->flt[0]),3,buf,len);
		buf+=double_extract(&(dest->dbl[0]),3,buf,len);
		++dest;
	}
	return (beginLen - *len);
}
unsigned int bit_test_typ_extract(bit_test_typ *dest, int num, const unsigned char *buf, int *len)
{
	int beginLen = *len;
	int count[2];

	for (count[0]=0; count[0]<num; ++count[0])
	{
		if (*len == 0) return beginLen;
		buf+=bit_extract(&(dest->bv_1a),1,1,0,buf,len);
		buf+=bit_extract(&(dest->bv_2a),1,2,(1)%8,buf,len);
		buf+=bit_extract(&(dest->bv_6a),1,5,(2+1)%8,buf,len);
		buf+=bit_extract(&(dest->bv_1b),1,1,(5+2+1)%8,buf,len);
		buf+=bit_extract(&(dest->bv_8b),1,8,(1+5+2+1)%8,buf,len);
		buf+=bit_extract(&(dest->bv_16b),1,16,(8+1+5+2+1)%8,buf,len);
		buf+=bit_extract(&(dest->bv_24b),1,24,(16+8+1+5+2+1)%8,buf,len);
		buf+=bit_extract(&(dest->bv_32b),1,32,(24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_extract(&(dest->bv_7b),1,7,(32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_extract(&(dest->bv_1c),1,1,(7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_extract(&(dest->bv_15c),1,15,(1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_extract(&(dest->bv_1d),1,1,(15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_extract(&(dest->bv_23d),1,23,(1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_extract(&(dest->bv_1e),1,1,(23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_extract(&(dest->bv_31e),1,31,(1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_extract(&(dest->bv_1f),1,1,(31+1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_r_extract(&(dest->rbv_1f),1,1,(1+31+1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_r_extract(&(dest->rbv_2f),1,2,(1+1+31+1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_r_extract(&(dest->rbv_6f),1,6,(2+1+1+31+1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_r_extract(&(dest->rbv_8f),1,8,(6+2+1+1+31+1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_r_extract(&(dest->rbv_9f),1,9,(8+6+2+1+1+31+1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_r_extract(&(dest->rbv_16f),1,16,(9+8+6+2+1+1+31+1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_r_extract(&(dest->rbv_17f),1,17,(16+9+8+6+2+1+1+31+1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_r_extract(&(dest->rbv_24f),1,24,(17+16+9+8+6+2+1+1+31+1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_r_extract(&(dest->rbv_25f),1,25,(24+17+16+9+8+6+2+1+1+31+1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_r_extract(&(dest->rbv_32f),1,32,(25+24+17+16+9+8+6+2+1+1+31+1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=int_extract(&(dest->in),1,buf,len);
		if (*len == 0) return beginLen;
		buf+=bit_r_extract(&(dest->rbv_3g),1,3,0,buf,len);
		buf+=bit_extract(&(dest->bv_3g),1,3,(3)%8,buf,len);
		buf+=bit_r_extract(&(dest->rbv_4g),1,4,(3+3)%8,buf,len);
		buf+=bit_extract(&(dest->bv_2g[0]),3,2,(4+3+3)%8,buf,len);
		buf+=bit_r_extract(&(dest->rbv_2g[0]),3,2,((3*2)+4+3+3)%8,buf,len);
		buf+=bit_r_extract(&(dest->rbv_5g),1,5,((3*2)+(3*2)+4+3+3)%8,buf,len);
		buf+=bit_extract(&(dest->bv_10g[0]),3,10,(5+(3*2)+(3*2)+4+3+3)%8,buf,len);
		buf+=bit_r_extract(&(dest->rbv_10g[0]),3,10,((3*10)+5+(3*2)+(3*2)+4+3+3)%8,buf,len);
		buf+=bit_extract(&(dest->bv_7g),1,7,((3*10)+(3*10)+5+(3*2)+(3*2)+4+3+3)%8,buf,len);
		++dest;
	}
	return (beginLen - *len);
}
unsigned int decode_test_typ_extract(decode_test_typ *dest, int num, const unsigned char *buf, int *len)
{
	int beginLen = *len;
	int count[2];

	for (count[0]=0; count[0]<num; ++count[0])
	{
		buf+=decode_extract(&(dest->dv_char),1,1,60,0,1,buf,len);
		buf+=decode_extract(&(dest->dv_short),1,2,90,-90,1,buf,len);
		buf+=decode_extract(&(dest->dv_int),1,4,-180,180,1,buf,len);
		buf+=decode_r_extract(&(dest->rdv_char),1,1,60,0,1,buf,len);
		buf+=decode_r_extract(&(dest->rdv_short),1,2,90,-90,1,buf,len);
		buf+=decode_r_extract(&(dest->rdv_int),1,4,-180,180,1,buf,len);
		buf+=decode_extract(&(dest->adv_char[0]),3,1,100,-50,1,buf,len);
		buf+=decode_extract(&(dest->adv_int[0]),3,4,500,-250,1,buf,len);
		buf+=decode_r_extract(&(dest->radv_short[0]),3,2,200,-100,1,buf,len);
		++dest;
	}
	return (beginLen - *len);
}
unsigned int decode_bit_test_typ_extract(decode_bit_test_typ *dest, int num, const unsigned char *buf, int *len)
{
	int beginLen = *len;
	int count[2];

	for (count[0]=0; count[0]<num; ++count[0])
	{
		if (*len == 0) return beginLen;
		buf+=decode_bit_extract(&(dest->dbv_4),1,4,0,7.5,-2.5,1,buf,len);
		buf+=decode_bit_extract(&(dest->dbv_12),1,12,(4)%8,90,-90,1,buf,len);
		buf+=bit_extract(&(dest->bv_3a),1,3,(12+4)%8,buf,len);
		buf+=decode_bit_extract(&(dest->dbv_20),1,20,(3+12+4)%8,180,-180,1,buf,len);
		buf+=decode_bit_extract(&(dest->dbv_28),1,28,(20+3+12+4)%8,180,-180,1,buf,len);
		buf+=decode_bit_r_extract(&(dest->rdbv_4),1,4,(28+20+3+12+4)%8,7.5,-2.5,1,buf,len);
		buf+=decode_bit_r_extract(&(dest->rdbv_12),1,12,(4+28+20+3+12+4)%8,90,-90,1,buf,len);
		buf+=bit_extract(&(dest->bv_3b),1,3,(12+4+28+20+3+12+4)%8,buf,len);
		buf+=decode_bit_r_extract(&(dest->rdbv_20),1,20,(3+12+4+28+20+3+12+4)%8,180,-180,1,buf,len);
		buf+=decode_bit_r_extract(&(dest->rdbv_28),1,28,(20+3+12+4+28+20+3+12+4)%8,180,-180,1,buf,len);
		buf+=bit_r_extract(&(dest->rbv_3c),1,3,(28+20+3+12+4+28+20+3+12+4)%8,buf,len);
		buf+=decode_bit_r_extract(&(dest->rdbv_5),1,5,(3+28+20+3+12+4+28+20+3+12+4)%8,7.5,-2.5,1,buf,len);
		++dest;
	}
	return (beginLen - *len);
}
/* %%RM_VAR_STRUCT_EXTR_END%% */

/* %%RM_VAR_STRUCT_PACKER_BEGIN%% */
unsigned int gen_test_typ_pack(gen_test_typ *dest, int num, unsigned char *buf, int *len)
{
	int beginLen = *len;
	int count[2];

	for (count[0]=0; count[0]<num; ++count[0])
	{
		buf+=unsigned_char_pack(&(dest->uch),1,buf,len);
		buf+=char_pack(&(dest->ch),1,buf,len);
		buf+=unsigned_short_pack(&(dest->ush),1,buf,len);
		buf+=short_pack(&(dest->sh),1,buf,len);
		buf+=unsigned_int_pack(&(dest->uin),1,buf,len);
		buf+=int_pack(&(dest->in),1,buf,len);
		buf+=unsigned_long_pack(&(dest->ulg),1,buf,len);
		buf+=long_pack(&(dest->lg),1,buf,len);
		buf+=unsigned_long_long_pack(&(dest->ulglg),1,buf,len);
		buf+=long_long_pack(&(dest->lglg),1,buf,len);
		buf+=float_pack(&(dest->flt),1,buf,len);
		buf+=double_pack(&(dest->dbl),1,buf,len);
		++dest;
	}
	return (beginLen - *len);
}
unsigned int arry_test_typ_pack(arry_test_typ *dest, int num, unsigned char *buf, int *len)
{
	int beginLen = *len;
	int count[2];

	for (count[0]=0; count[0]<num; ++count[0])
	{
		buf+=unsigned_char_pack(&(dest->uch[0]),3,buf,len);
		buf+=char_pack(&(dest->ch[0]),3,buf,len);
		buf+=unsigned_short_pack(&(dest->ush[0]),3,buf,len);
		buf+=short_pack(&(dest->sh[0]),3,buf,len);
		buf+=unsigned_int_pack(&(dest->uin[0]),3,buf,len);
		buf+=int_pack(&(dest->in[0]),3,buf,len);
		buf+=unsigned_long_pack(&(dest->ulg[0]),3,buf,len);
		buf+=long_pack(&(dest->lg[0]),3,buf,len);
		buf+=unsigned_long_long_pack(&(dest->ulglg[0]),3,buf,len);
		buf+=long_long_pack(&(dest->lglg[0]),3,buf,len);
		buf+=float_pack(&(dest->flt[0]),3,buf,len);
		buf+=double_pack(&(dest->dbl[0]),3,buf,len);
		++dest;
	}
	return (beginLen - *len);
}
unsigned int bit_test_typ_pack(bit_test_typ *dest, int num, unsigned char *buf, int *len)
{
	int beginLen = *len;
	int count[2];

	for (count[0]=0; count[0]<num; ++count[0])
	{
		if (*len == 0) return beginLen;
		buf+=bit_pack(&(dest->bv_1a),1,1,0,buf,len);
		buf+=bit_pack(&(dest->bv_2a),1,2,(1)%8,buf,len);
		buf+=bit_pack(&(dest->bv_6a),1,5,(2+1)%8,buf,len);
		buf+=bit_pack(&(dest->bv_1b),1,1,(5+2+1)%8,buf,len);
		buf+=bit_pack(&(dest->bv_8b),1,8,(1+5+2+1)%8,buf,len);
		buf+=bit_pack(&(dest->bv_16b),1,16,(8+1+5+2+1)%8,buf,len);
		buf+=bit_pack(&(dest->bv_24b),1,24,(16+8+1+5+2+1)%8,buf,len);
		buf+=bit_pack(&(dest->bv_32b),1,32,(24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_pack(&(dest->bv_7b),1,7,(32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_pack(&(dest->bv_1c),1,1,(7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_pack(&(dest->bv_15c),1,15,(1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_pack(&(dest->bv_1d),1,1,(15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_pack(&(dest->bv_23d),1,23,(1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_pack(&(dest->bv_1e),1,1,(23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_pack(&(dest->bv_31e),1,31,(1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_pack(&(dest->bv_1f),1,1,(31+1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_r_pack(&(dest->rbv_1f),1,1,(1+31+1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_r_pack(&(dest->rbv_2f),1,2,(1+1+31+1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_r_pack(&(dest->rbv_6f),1,6,(2+1+1+31+1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_r_pack(&(dest->rbv_8f),1,8,(6+2+1+1+31+1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_r_pack(&(dest->rbv_9f),1,9,(8+6+2+1+1+31+1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_r_pack(&(dest->rbv_16f),1,16,(9+8+6+2+1+1+31+1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_r_pack(&(dest->rbv_17f),1,17,(16+9+8+6+2+1+1+31+1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_r_pack(&(dest->rbv_24f),1,24,(17+16+9+8+6+2+1+1+31+1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_r_pack(&(dest->rbv_25f),1,25,(24+17+16+9+8+6+2+1+1+31+1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=bit_r_pack(&(dest->rbv_32f),1,32,(25+24+17+16+9+8+6+2+1+1+31+1+23+1+15+1+7+32+24+16+8+1+5+2+1)%8,buf,len);
		buf+=int_pack(&(dest->in),1,buf,len);
		if (*len == 0) return beginLen;
		buf+=bit_r_pack(&(dest->rbv_3g),1,3,0,buf,len);
		buf+=bit_pack(&(dest->bv_3g),1,3,(3)%8,buf,len);
		buf+=bit_r_pack(&(dest->rbv_4g),1,4,(3+3)%8,buf,len);
		buf+=bit_pack(&(dest->bv_2g[0]),3,2,(4+3+3)%8,buf,len);
		buf+=bit_r_pack(&(dest->rbv_2g[0]),3,2,((3*2)+4+3+3)%8,buf,len);
		buf+=bit_r_pack(&(dest->rbv_5g),1,5,((3*2)+(3*2)+4+3+3)%8,buf,len);
		buf+=bit_pack(&(dest->bv_10g[0]),3,10,(5+(3*2)+(3*2)+4+3+3)%8,buf,len);
		buf+=bit_r_pack(&(dest->rbv_10g[0]),3,10,((3*10)+5+(3*2)+(3*2)+4+3+3)%8,buf,len);
		buf+=bit_pack(&(dest->bv_7g),1,7,((3*10)+(3*10)+5+(3*2)+(3*2)+4+3+3)%8,buf,len);
		++dest;
	}
	return (beginLen - *len);
}
unsigned int decode_test_typ_pack(decode_test_typ *dest, int num, unsigned char *buf, int *len)
{
	int beginLen = *len;
	int count[2];

	for (count[0]=0; count[0]<num; ++count[0])
	{
		buf+=decode_pack(&(dest->dv_char),1,1,60,0,1,buf,len);
		buf+=decode_pack(&(dest->dv_short),1,2,90,-90,1,buf,len);
		buf+=decode_pack(&(dest->dv_int),1,4,-180,180,1,buf,len);
		buf+=decode_r_pack(&(dest->rdv_char),1,1,60,0,1,buf,len);
		buf+=decode_r_pack(&(dest->rdv_short),1,2,90,-90,1,buf,len);
		buf+=decode_r_pack(&(dest->rdv_int),1,4,-180,180,1,buf,len);
		buf+=decode_pack(&(dest->adv_char[0]),3,1,100,-50,1,buf,len);
		buf+=decode_pack(&(dest->adv_int[0]),3,4,500,-250,1,buf,len);
		buf+=decode_r_pack(&(dest->radv_short[0]),3,2,200,-100,1,buf,len);
		++dest;
	}
	return (beginLen - *len);
}
unsigned int decode_bit_test_typ_pack(decode_bit_test_typ *dest, int num, unsigned char *buf, int *len)
{
	int beginLen = *len;
	int count[2];

	for (count[0]=0; count[0]<num; ++count[0])
	{
		if (*len == 0) return beginLen;
		buf+=decode_bit_pack(&(dest->dbv_4),1,4,0,7.5,-2.5,1,buf,len);
		buf+=decode_bit_pack(&(dest->dbv_12),1,12,(4)%8,90,-90,1,buf,len);
		buf+=bit_pack(&(dest->bv_3a),1,3,(12+4)%8,buf,len);
		buf+=decode_bit_pack(&(dest->dbv_20),1,20,(3+12+4)%8,180,-180,1,buf,len);
		buf+=decode_bit_pack(&(dest->dbv_28),1,28,(20+3+12+4)%8,180,-180,1,buf,len);
		buf+=decode_bit_r_pack(&(dest->rdbv_4),1,4,(28+20+3+12+4)%8,7.5,-2.5,1,buf,len);
		buf+=decode_bit_r_pack(&(dest->rdbv_12),1,12,(4+28+20+3+12+4)%8,90,-90,1,buf,len);
		buf+=bit_pack(&(dest->bv_3b),1,3,(12+4+28+20+3+12+4)%8,buf,len);
		buf+=decode_bit_r_pack(&(dest->rdbv_20),1,20,(3+12+4+28+20+3+12+4)%8,180,-180,1,buf,len);
		buf+=decode_bit_r_pack(&(dest->rdbv_28),1,28,(20+3+12+4+28+20+3+12+4)%8,180,-180,1,buf,len);
		buf+=bit_r_pack(&(dest->rbv_3c),1,3,(28+20+3+12+4+28+20+3+12+4)%8,buf,len);
		buf+=decode_bit_r_pack(&(dest->rdbv_5),1,5,(3+28+20+3+12+4+28+20+3+12+4)%8,7.5,-2.5,1,buf,len);
		++dest;
	}
	return (beginLen - *len);
}
/* %%RM_VAR_STRUCT_PACKER_END%% */

/* %%RM_VAR_STRUCT_SIZE_BEGIN%% */
int gen_test_typ_size(gen_test_typ *var)
{
	return sizeof(unsigned char)+sizeof(char)+sizeof(unsigned short)+sizeof(short)+sizeof(unsigned int)+sizeof(int)+sizeof(unsigned long)+sizeof(long)+sizeof(unsigned long long)+sizeof(long long)+sizeof(float)+sizeof(double);
}
int arry_test_typ_size(arry_test_typ *var)
{
	return sizeof(unsigned char)*3+sizeof(char)*3+sizeof(unsigned short)*3+sizeof(short)*3+sizeof(unsigned int)*3+sizeof(int)*3+sizeof(unsigned long)*3+sizeof(long)*3+sizeof(unsigned long long)*3+sizeof(long long)*3+sizeof(float)*3+sizeof(double)*3;
}
int bit_test_typ_size(bit_test_typ *var)
{
	return (1+2+5+1+8+16+24+32+7+1+15+1+23+1+31+1+1+2+6+8+9+16+17+24+25+32+7)/8+sizeof(int)+(3+3+4+2*3+2*3+5+10*3+10*3+7+7)/8;
}
int decode_test_typ_size(decode_test_typ *var)
{
	return 1+2+4+1+2+4+1*3+4*3+2*3;
}
int decode_bit_test_typ_size(decode_bit_test_typ *var)
{
	return (4+12+3+20+28+4+12+3+20+28+3+5+7)/8;
}
/* %%RM_VAR_STRUCT_SIZE_END%% */

/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_struct_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_StructVarTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_struct_var\test_struct_var.h"

static StructVarTestSuite suite_StructVarTestSuite;

static CxxTest::List Tests_StructVarTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_StructVarTestSuite( "test_struct_var/test_struct_var.h", 6, "StructVarTestSuite", suite_StructVarTestSuite, Tests_StructVarTestSuite );

static class TestDescription_suite_StructVarTestSuite_testExtractInit : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_StructVarTestSuite_testExtractInit() : CxxTest::RealTestDescription( Tests_StructVarTestSuite, suiteDescription_StructVarTestSuite, 9, "testExtractInit" ) {}
 void runTest() { suite_StructVarTestSuite.testExtractInit(); }
} testDescription_suite_StructVarTestSuite_testExtractInit;

static class TestDescription_suite_StructVarTestSuite_testExtractGenericVars : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_StructVarTestSuite_testExtractGenericVars() : CxxTest::RealTestDescription( Tests_StructVarTestSuite, suiteDescription_StructVarTestSuite, 10, "testExtractGenericVars" ) {}
 void runTest() { suite_StructVarTestSuite.testExtractGenericVars(); }
} testDescription_suite_StructVarTestSuite_testExtractGenericVars;

static class TestDescription_suite_StructVarTestSuite_testExtractGenericVarArrays : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_StructVarTestSuite_testExtractGenericVarArrays() : CxxTest::RealTestDescription( Tests_StructVarTestSuite, suiteDescription_StructVarTestSuite, 11, "testExtractGenericVarArrays" ) {}
 void runTest() { suite_StructVarTestSuite.testExtractGenericVarArrays(); }
} testDescription_suite_StructVarTestSuite_testExtractGenericVarArrays;

static class TestDescription_suite_StructVarTestSuite_testExtractBitVars : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_StructVarTestSuite_testExtractBitVars() : CxxTest::RealTestDescription( Tests_StructVarTestSuite, suiteDescription_StructVarTestSuite, 12, "testExtractBitVars" ) {}
 void runTest() { suite_StructVarTestSuite.testExtractBitVars(); }
} testDescription_suite_StructVarTestSuite_testExtractBitVars;

static class TestDescription_suite_StructVarTestSuite_testExtractDecodeVars : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_StructVarTestSuite_testExtractDecodeVars() : CxxTest::RealTestDescription( Tests_StructVarTestSuite, suiteDescription_StructVarTestSuite, 13, "testExtractDecodeVars" ) {}
 void runTest() { suite_StructVarTestSuite.testExtractDecodeVars(); }
} testDescription_suite_StructVarTestSuite_testExtractDecodeVars;

static class TestDescription_suite_StructVarTestSuite_testExtractDecodeBitVars : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_StructVarTestSuite_testExtractDecodeBitVars() : CxxTest::RealTestDescription( Tests_StructVarTestSuite, suiteDescription_StructVarTestSuite, 14, "testExtractDecodeBitVars" ) {}
 void runTest() { suite_StructVarTestSuite.testExtractDecodeBitVars(); }
} testDescription_suite_StructVarTestSuite_testExtractDecodeBitVars;

static class TestDescription_suite_StructVarTestSuite_testPackOutput : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_StructVarTestSuite_testPackOutput() : CxxTest::RealTestDescription( Tests_StructVarTestSuite, suiteDescription_StructVarTestSuite, 15, "testPackOutput" ) {}
 void runTest() { suite_StructVarTestSuite.testPackOutput(); }
} testDescription_suite_StructVarTestSuite_testPackOutput;

static class TestDescription_suite_StructVarTestSuite_testStream : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_StructVarTestSuite_testStream() : CxxTest::RealTestDescription( Tests_StructVarTestSuite, suiteDescription_StructVarTestSuite, 16, "testStream" ) {}
 void runTest() { suite_StructVarTestSuite.testStream(); }
} testDescription_suite_StructVarTestSuite_testStream;

static class TestDescription_suite_StructVarTestSuite_testCopyAndAssignment1 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_StructVarTestSuite_testCopyAndAssignment1() : CxxTest::RealTestDescription( Tests_StructVarTestSuite, suiteDescription_StructVarTestSuite, 17, "testCopyAndAssignment1" ) {}
 void runTest() { suite_StructVarTestSuite.testCopyAndAssignment1(); }
} testDescription_suite_StructVarTestSuite_testCopyAndAssignment1;

static class TestDescription_suite_StructVarTestSuite_testCopyAndAssignment2 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_StructVarTestSuite_testCopyAndAssignment2() : CxxTest::RealTestDescription( Tests_StructVarTestSuite, suiteDescription_StructVarTestSuite, 18, "testCopyAndAssignment2" ) {}
 void runTest() { suite_StructVarTestSuite.testCopyAndAssignment2(); }
} testDescription_suite_StructVarTestSuite_testCopyAndAssignment2;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

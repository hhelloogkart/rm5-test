TARGET = test_struct_var
HEADERS += test_struct_var.h
SOURCES += test_struct_var.cpp test_struct_var2.cpp data.cpp cdata.c

include (../tests.pri)

HEADERS += data.h cdata.h

unix:LIBS += -lrmc
win32{
 win32-msvc2013 | win32-msvc2015 {
  LIBS += ../../lib/rmc$${LIB_SUFFIX}1.lib
 } else:win32-msvc2008 {
  LIBS += ../../lib/rmc1.lib
 } else {
  LIBS += ../../lib/rmc10.lib
 }
}
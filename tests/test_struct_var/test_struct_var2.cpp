#include "test_struct_var.h"
#include "data.h"
#include <link_var.h>
#include <stdio.h>
#include <string.h>
#include <outbuf.h>

const char dataset[] = {
        /* gen test */
        '\xA',  /* unsigned char */
        '\xFB', /* char ch */
        '\3',  /* unsigned short ush */
        '\xA',
        '\x77', /* short sh */
        '\x93',
        '\x5A', /* unsigned int */
        '\6',
        '\6',
        '\x4A',
        '\x4C', /* int */
        '\6',
        '\6',
        '\xAC',
        '\x5E', /* unsigned long */
        '\6',
        '\6',
        '\x4A',
        '\x33', /* long */
        '\7',
        '\7',
        '\xB2',
        '\x29', /* unsigned long long */
        '\6',
        '\6',
        '\6',
        '\6',
        '\6',
        '\6',
        '\x4A',
        '\xFB', /* long long */
        '\xFF',
        '\xFE',
        '\xFF',
        '\xFE',
        '\xFF',
        '\xFE',
        '\x4A',
        '\0', /* float */
        '\0',
        '\x98',
        '\x40',
        '\0', /* double */
        '\0',
        '\0',
        '\0',
        '\0',
        '\0',
        '\x15',
        '\x40',
        /* arry test */
        '\x77',  /* unsigned char */
        '\x2C',  /* unsigned char */
        '\x9B',  /* unsigned char */
        '\xAA',  /* char ch */
        '\xB',   /* char ch */
        '\x3D',  /* char ch */
        '\xFC',  /* unsigned short ush */
        '\x9A',
        '\x22',  /* unsigned short ush */
        '\xA',
        '\x3B',  /* unsigned short ush */
        '\xBA',
        '\x71',  /* short sh */
        '\xB3',
        '\x7F',  /* short sh */
        '\x72',
        '\xFF',  /* short sh */
        '\x80',
        '\x12',  /* unsigned int */
        '\6',
        '\6',
        '\xCB',
        '\x50',  /* unsigned int */
        '\x44',
        '\x96',
        '\x9A',
        '\xE',  /* unsigned int */
        '\6',
        '\6',
        '\xD',
        '\x36', /* int */
        '\0',
        '\0',
        '\xE3',
        '\xAB', /* int */
        '\0',
        '\0',
        '\0',
        '\0',   /* int */
        '\x5E',
        '\0',
        '\x99',
        '\x82', /* unsigned long */
        '\0',
        '\3',
        '\x12',
        '\x78', /* unsigned long */
        '\6',
        '\2',
        '\xC0',
        '\xAA', /* unsigned long */
        '\x33',
        '\x76',
        '\x13',
        '\x33', /* long */
        '\7',
        '\7',
        '\xB2',
        '\xA0', /* long */
        '\2',
        '\0',
        '\0',
        '\0',   /* long */
        '\x31',
        '\x21',
        '\x78',
        '\0', /* unsigned long long */
        '\6',
        '\6',
        '\6',
        '\6',
        '\6',
        '\6',
        '\x9C',
        '\xE', /* unsigned long long */
        '\0',
        '\0',
        '\0',
        '\0',
        '\0',
        '\0',
        '\0',
        '\x10', /* unsigned long long */
        '\xFE',
        '\xDD',
        '\x8',
        '\6',
        '\4',
        '\2',
        '\xD0',
        '\x18', /* long long */
        '\xFF',
        '\xFE',
        '\xFF',
        '\xFE',
        '\xFF',
        '\xFE',
        '\x91',
        '\x32', /* long long */
        '\x44',
        '\x12',
        '\0',
        '\0',
        '\0',
        '\0',
        '\x8',
        '\x53', /* long long */
        '\0',
        '\4',
        '\x8B',
        '\x63',
        '\xFA',
        '\x1F',
        '\x60',
        '\0', /* float */
        '\xC0',
        '\xD7',
        '\x43',
        '\0', /* float */
        '\x40',
        '\x23',
        '\xC3',
        '\xFC', /* float */
        '\xD0',
        '\xFA',
        '\x4C',
        '\0', /* double */
        '\0',
        '\0',
        '\0',
        '\xA0',
        '\x88',
        '\xB3',
        '\x40',
        '\0', /* double */
        '\0',
        '\0',
        '\0',
        '\x20',
        '\x74',
        '\xB0',
        '\xC0',
        '\0', /* double */
        '\0',
        '\x40',
        '\xF1',
        '\x13',
        '\x73',
        '\x35',
        '\xC2',
        /* bit test */
        '\x5d', /* bit */
        '\xf1',
        '\x28',
        '\x29',
        '\xd1',
        '\xdd',
        '\x06',
        '\x80',
        '\x7b',
        '\xe1',
        '\xff',
        '\xa5',
        '\xe1',
        '\x2e',
        '\xe0',
        '\x5e',
        '\xf8',
        '\x41',
        '\xf3',
        '\xad',
        '\x00',
        '\x25',
        '\xe2',
        '\xe1',
        '\x84',
        '\xa9',
        '\x03',
        '\xad',
        '\x08',
        '\x10',
        '\xa4',
        '\x00',
        '\x05',
        '\x14',
        '\x00',
        '\xd8',
        '\xdc',
        '\xff',
        '\x1f',
        '\xAB', /* int */
        '\x10',
        '\x95',
        '\x02',
        '\xee',
        '\xd2',
        '\x76',
        '\x0c',
        '\x90',
        '\x7f',
        '\x77',
        '\xbb',
        '\x07',
        '\x70',
        '\x27',
        '\x2c',
        /* decode test */
        '\x80', // dv_char
        '\0',   // dv_short
        '\x80',
        '\0',   // dv_int
        '\0',
        '\0',
        '\x80',
        '\x40', // rdv_char
        '\x40', // rdv_short
        '\0',
        '\x40', // rdv_int
        '\0',
        '\0',
        '\0',
        '\x20', // adv_char[0]
        '\x10', // adv_char[1]
        '\x08', // adv_char[2]
        '\0',   // adv_int[0]
        '\0',
        '\0',
        '\x20',
        '\0',   // adv_int[1]
        '\0',
        '\x80',
        '\0',
        '\0',   // adv_int[2]
        '\x80',
        '\0',
        '\0',
        '\x20', // rdv_short[0]
        '\0',
        '\4',   // rdv_short[1]
        '\0',
        '\0',   // rdv_short[2]
        /* decode bit tests */
        '\x80',
        '\x7c',
        '\x3e',
        '\x83',
        '\x4f',
        '\x12',
        '\x80',
        '\x84',
        '\x1e',
        '\x48',
        '\x58',
        '\x32',
        '\x12',
        '\x7a',
        '\x80',
        '\x8d',
        '\x5b',
        '\x40',
        '\x39'
};

/* This set initializes the structs with the right values */

class dirty_checker : public mpt_baseshow
{
public:
    dirty_checker(mpt_var *myvar);
    ~dirty_checker();
    virtual void setDirty(mpt_var *myvar);
    virtual void notDirty(mpt_var *myvar);
    int getStateAndReset();
protected:
    int state; // 0 - not called, 1 - dirty, 2 - notdirty, 3 - error
    mpt_var *var;
};

dirty_checker::dirty_checker(mpt_var *myvar) : state(0), var(myvar)
{
    myvar->addCallback(this);
}

dirty_checker::~dirty_checker()
{
    var->removeCallback(this);
}

void dirty_checker::setDirty(mpt_var *myvar)
{
    if (myvar == var) state |= 1;
    else state |= 4;
}

void dirty_checker::notDirty(mpt_var *myvar)
{
    if (myvar == var) state |= 2;
    else state |= 4;
}

int dirty_checker::getStateAndReset()
{
    int ret = state;
    state = 0;
    return ret;
}

void StructVarTestSuite::testExtractInit(void)
{
    dirty_checker chk(&t4);
    t4.setInvalid();
    TS_ASSERT_EQUALS(chk.getStateAndReset(), 1);
    TS_ASSERT(!t4.isValid());
    t4.setInvalid();
    TS_ASSERT_EQUALS(chk.getStateAndReset(), 2);
    t4.extract(sizeof(dataset), (const unsigned char *)dataset);
    TS_ASSERT(t4.isValid());
    TS_ASSERT_EQUALS(chk.getStateAndReset(), 1);
}

/* This set checks if the data is the same */

void StructVarTestSuite::testPackOutput(void)
{
    char buf[sizeof(dataset)];
    outbuf obuf;
    obuf.set((unsigned char *)buf, sizeof(dataset));
    t4.output(obuf);
    TS_ASSERT(memcmp(buf, dataset, sizeof(dataset)) == 0);
}

void StructVarTestSuite::testStream(void)
{
    std::ostringstream os;
    os << t4 << std::ends;

    dirty_checker chk(&t4);
    t4.setInvalid();
    TS_ASSERT_EQUALS(chk.getStateAndReset(), 1);
    TS_ASSERT(!t4.isValid());

    std::istringstream is(os.str());
    is >> t4;
    TS_ASSERT_EQUALS(chk.getStateAndReset(), 1);
    testExtractGenericVars();
    testExtractGenericVarArrays();
    testExtractBitVars();
    testExtractDecodeVars();
    testExtractDecodeBitVars();
}

void StructVarTestSuite::testCopyAndAssignment2()
{
    gen_test_typ_marshaller mg[2];
    link_var lv;
    lv = mg[0];
    mg[0].setInvalid();
    TS_ASSERT_DIFFERS(mg[0], t4.gen);
    mg[0] = t4.gen;
    TS_ASSERT_EQUALS(mg[0], t4.gen);
    TS_ASSERT_EQUALS(t4.gen, lv);
    mg[0].setInvalid();
    TS_ASSERT_DIFFERS(mg[0], t4.gen);
    TS_ASSERT(!mg[0].isValid());
    TS_ASSERT_EQUALS(mg[0].getNext(), &mg[1]);

    TS_TRACE("End");
}
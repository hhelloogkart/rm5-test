#ifndef _DATA_H_
#define _DATA_H_

/* %%RM_VAR_DEF_INCLUDE_BEGIN%% */
#include <rm_var.h>
#include <cplx_var.h>
#include <gen_var.h>
#include <bit_var.h>
#include <struct_var.h>
/* %%RM_VAR_DEF_INCLUDE_END%% */

/* %%RM_VAR_DEFINITIONS_BEGIN%% */
struct DECODE_PROP_1
{
	static const int SIZE = 1;
	static const int SCALE_OFFSET = 1;
	static const double RANGE;
	static const double OFFSET;
};
#ifdef INIT_DECODE_PARAM
const double DECODE_PROP_1::RANGE = 60.;
const double DECODE_PROP_1::OFFSET = 0.;
#endif /* INIT_DECODE_PARAM */
struct DECODE_PROP_2
{
	static const int SIZE = 2;
	static const int SCALE_OFFSET = 1;
	static const double RANGE;
	static const double OFFSET;
};
#ifdef INIT_DECODE_PARAM
const double DECODE_PROP_2::RANGE = 90.;
const double DECODE_PROP_2::OFFSET = -90.;
#endif /* INIT_DECODE_PARAM */
struct DECODE_PROP_3
{
	static const int SIZE = 4;
	static const int SCALE_OFFSET = 1;
	static const double RANGE;
	static const double OFFSET;
};
#ifdef INIT_DECODE_PARAM
const double DECODE_PROP_3::RANGE = -180.;
const double DECODE_PROP_3::OFFSET = 180.;
#endif /* INIT_DECODE_PARAM */
struct DECODE_PROP_7
{
	static const int SIZE = 1;
	static const int SCALE_OFFSET = 1;
	static const double RANGE;
	static const double OFFSET;
};
#ifdef INIT_DECODE_PARAM
const double DECODE_PROP_7::RANGE = 100.;
const double DECODE_PROP_7::OFFSET = -50.;
#endif /* INIT_DECODE_PARAM */
struct DECODE_PROP_8
{
	static const int SIZE = 4;
	static const int SCALE_OFFSET = 1;
	static const double RANGE;
	static const double OFFSET;
};
#ifdef INIT_DECODE_PARAM
const double DECODE_PROP_8::RANGE = 500.;
const double DECODE_PROP_8::OFFSET = -250.;
#endif /* INIT_DECODE_PARAM */
struct DECODE_PROP_9
{
	static const int SIZE = 2;
	static const int SCALE_OFFSET = 1;
	static const double RANGE;
	static const double OFFSET;
};
#ifdef INIT_DECODE_PARAM
const double DECODE_PROP_9::RANGE = 200.;
const double DECODE_PROP_9::OFFSET = -100.;
#endif /* INIT_DECODE_PARAM */
struct DECODE_PROP_10
{
	static const int SIZE = 4;
	static const int SCALE_OFFSET = 1;
	static const double RANGE;
	static const double OFFSET;
};
#ifdef INIT_DECODE_PARAM
const double DECODE_PROP_10::RANGE = 7.5;
const double DECODE_PROP_10::OFFSET = -2.5;
#endif /* INIT_DECODE_PARAM */
struct DECODE_PROP_11
{
	static const int SIZE = 12;
	static const int SCALE_OFFSET = 1;
	static const double RANGE;
	static const double OFFSET;
};
#ifdef INIT_DECODE_PARAM
const double DECODE_PROP_11::RANGE = 90.;
const double DECODE_PROP_11::OFFSET = -90.;
#endif /* INIT_DECODE_PARAM */
struct DECODE_PROP_12
{
	static const int SIZE = 20;
	static const int SCALE_OFFSET = 1;
	static const double RANGE;
	static const double OFFSET;
};
#ifdef INIT_DECODE_PARAM
const double DECODE_PROP_12::RANGE = 180.;
const double DECODE_PROP_12::OFFSET = -180.;
#endif /* INIT_DECODE_PARAM */
struct DECODE_PROP_13
{
	static const int SIZE = 28;
	static const int SCALE_OFFSET = 1;
	static const double RANGE;
	static const double OFFSET;
};
#ifdef INIT_DECODE_PARAM
const double DECODE_PROP_13::RANGE = 180.;
const double DECODE_PROP_13::OFFSET = -180.;
#endif /* INIT_DECODE_PARAM */
struct DECODE_PROP_18
{
	static const int SIZE = 5;
	static const int SCALE_OFFSET = 1;
	static const double RANGE;
	static const double OFFSET;
};
#ifdef INIT_DECODE_PARAM
const double DECODE_PROP_18::RANGE = 7.5;
const double DECODE_PROP_18::OFFSET = -2.5;
#endif /* INIT_DECODE_PARAM */
extern "C" void * gen_test_typ_g1_ptr;
struct gen_test_typ;
extern "C" unsigned int gen_test_typ_extract(gen_test_typ *dest, int num, const unsigned char *buf, int *len);
extern "C" unsigned int gen_test_typ_pack(gen_test_typ *dest, int num, unsigned char *buf, int *len);
extern "C" int gen_test_typ_size(gen_test_typ *var);
/*!
	Marshaller class for gen_test_typ
*/
class gen_test_typ_marshaller : public struct_var
{
public:
	gen_test_typ_marshaller() : struct_var(gen_test_typ_g1_ptr) {}
	int extract(int len, const unsigned char *buf)
	{
		gen_test_typ_extract((gen_test_typ *)my_struct, 1, buf, &len);
		setDirty();
		return len;
	}
	int size() const
	{
		return gen_test_typ_size((gen_test_typ *)my_struct);
	}
	void output(outbuf &strm)
	{
		int sz=strm.maxsize()-strm.size();
		gen_test_typ_pack((gen_test_typ *)my_struct, 1, strm.getcur(),&sz);
		strm.setsize(strm.maxsize()-sz);
	}
};
extern "C" void * const arry_test_typ_a1_ptr;
struct arry_test_typ;
extern "C" unsigned int arry_test_typ_extract(arry_test_typ *dest, int num, const unsigned char *buf, int *len);
extern "C" unsigned int arry_test_typ_pack(arry_test_typ *dest, int num, unsigned char *buf, int *len);
extern "C" int arry_test_typ_size(arry_test_typ *var);
/*!
	Marshaller class for arry_test_typ
*/
class arry_test_typ_marshaller : public struct_var
{
public:
	arry_test_typ_marshaller() : struct_var(arry_test_typ_a1_ptr) {}
	int extract(int len, const unsigned char *buf)
	{
		arry_test_typ_extract((arry_test_typ *)my_struct, 1, buf, &len);
		setDirty();
		return len;
	}
	int size() const
	{
		return arry_test_typ_size((arry_test_typ *)my_struct);
	}
	void output(outbuf &strm)
	{
		int sz=strm.maxsize()-strm.size();
		arry_test_typ_pack((arry_test_typ *)my_struct, 1, strm.getcur(),&sz);
		strm.setsize(strm.maxsize()-sz);
	}
};
extern "C" void * const bit_test_typ_b1_ptr;
struct bit_test_typ;
extern "C" unsigned int bit_test_typ_extract(bit_test_typ *dest, int num, const unsigned char *buf, int *len);
extern "C" unsigned int bit_test_typ_pack(bit_test_typ *dest, int num, unsigned char *buf, int *len);
extern "C" int bit_test_typ_size(bit_test_typ *var);
/*!
	Marshaller class for bit_test_typ
*/
class bit_test_typ_marshaller : public struct_var
{
public:
	bit_test_typ_marshaller() : struct_var(bit_test_typ_b1_ptr) {}
	int extract(int len, const unsigned char *buf)
	{
		bit_test_typ_extract((bit_test_typ *)my_struct, 1, buf, &len);
		setDirty();
		return len;
	}
	int size() const
	{
		return bit_test_typ_size((bit_test_typ *)my_struct);
	}
	void output(outbuf &strm)
	{
		int sz=strm.maxsize()-strm.size();
		bit_test_typ_pack((bit_test_typ *)my_struct, 1, strm.getcur(),&sz);
		strm.setsize(strm.maxsize()-sz);
	}
};
extern "C" void * const decode_test_typ_d1_ptr;
struct decode_test_typ;
extern "C" unsigned int decode_test_typ_extract(decode_test_typ *dest, int num, const unsigned char *buf, int *len);
extern "C" unsigned int decode_test_typ_pack(decode_test_typ *dest, int num, unsigned char *buf, int *len);
extern "C" int decode_test_typ_size(decode_test_typ *var);
/*!
	Marshaller class for decode_test_typ
*/
class decode_test_typ_marshaller : public struct_var
{
public:
	decode_test_typ_marshaller() : struct_var(decode_test_typ_d1_ptr) {}
	int extract(int len, const unsigned char *buf)
	{
		decode_test_typ_extract((decode_test_typ *)my_struct, 1, buf, &len);
		setDirty();
		return len;
	}
	int size() const
	{
		return decode_test_typ_size((decode_test_typ *)my_struct);
	}
	void output(outbuf &strm)
	{
		int sz=strm.maxsize()-strm.size();
		decode_test_typ_pack((decode_test_typ *)my_struct, 1, strm.getcur(),&sz);
		strm.setsize(strm.maxsize()-sz);
	}
};
extern "C" void * const decode_bit_test_typ_db1_ptr;
struct decode_bit_test_typ;
extern "C" unsigned int decode_bit_test_typ_extract(decode_bit_test_typ *dest, int num, const unsigned char *buf, int *len);
extern "C" unsigned int decode_bit_test_typ_pack(decode_bit_test_typ *dest, int num, unsigned char *buf, int *len);
extern "C" int decode_bit_test_typ_size(decode_bit_test_typ *var);
/*!
	Marshaller class for decode_bit_test_typ
*/
class decode_bit_test_typ_marshaller : public struct_var
{
public:
	decode_bit_test_typ_marshaller() : struct_var(decode_bit_test_typ_db1_ptr) {}
	int extract(int len, const unsigned char *buf)
	{
		decode_bit_test_typ_extract((decode_bit_test_typ *)my_struct, 1, buf, &len);
		setDirty();
		return len;
	}
	int size() const
	{
		return decode_bit_test_typ_size((decode_bit_test_typ *)my_struct);
	}
	void output(outbuf &strm)
	{
		int sz=strm.maxsize()-strm.size();
		decode_bit_test_typ_pack((decode_bit_test_typ *)my_struct, 1, strm.getcur(),&sz);
		strm.setsize(strm.maxsize()-sz);
	}
};
class var_test4 : public rm_var
{
public:
	RMCONST(var_test4,"TEST4",1)
	gen_test_typ_marshaller gen;
	arry_test_typ_marshaller arry;
	bit_test_typ_marshaller bits;
	decode_test_typ_marshaller dec;
	decode_bit_test_typ_marshaller dbits;
};
/* %%RM_VAR_DEFINITIONS_END%% */

/* %%RM_VAR_EXTERNS_BEGIN%% */
extern var_test4 t4;
/* %%RM_VAR_EXTERNS_END%% */

#endif /* _DATA_H_ */

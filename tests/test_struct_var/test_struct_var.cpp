#include "test_struct_var.h"
#include "cdata.h"

/* This set checks if the data is OK */

void StructVarTestSuite::testExtractGenericVars(void)
{
    TS_ASSERT_EQUALS(g1.uch,10);
    TS_ASSERT_EQUALS(g1.ch,-5);
    TS_ASSERT_EQUALS(g1.ush,2563);
    TS_ASSERT_EQUALS(g1.sh,-27785);
    TS_ASSERT_EQUALS(g1.uin,1241908826);
    TS_ASSERT_EQUALS(g1.in,-1408891316);
    TS_ASSERT_EQUALS(g1.ulg,1241908830);
    TS_ASSERT_EQUALS(g1.lg, -1308162253);
    TS_ASSERT_EQUALS(g1.ulglg, 5333957431607559721);
    TS_ASSERT_EQUALS(g1.lglg, 5404038073572851707);
    TS_ASSERT_EQUALS(g1.flt, 4.75f);
    TS_ASSERT_EQUALS(g1.dbl, 5.25);
}

void StructVarTestSuite::testExtractGenericVarArrays(void)
{
    TS_ASSERT_EQUALS(a1.uch[0], 119);
    TS_ASSERT_EQUALS(a1.uch[1], 44);
    TS_ASSERT_EQUALS(a1.uch[2], 155);
    TS_ASSERT_EQUALS(a1.ch[0], -86);
    TS_ASSERT_EQUALS(a1.ch[1], 11);
    TS_ASSERT_EQUALS(a1.ch[2], 61);
    TS_ASSERT_EQUALS(a1.ush[0], 39676);
    TS_ASSERT_EQUALS(a1.ush[1], 2594);
    TS_ASSERT_EQUALS(a1.ush[2], 47675);
    TS_ASSERT_EQUALS(a1.sh[0], -19599);
    TS_ASSERT_EQUALS(a1.sh[1], 29311);
    TS_ASSERT_EQUALS(a1.sh[2], -32513);
    TS_ASSERT_EQUALS(a1.uin[0], 3406169618);
    TS_ASSERT_EQUALS(a1.uin[1], 2593539152);
    TS_ASSERT_EQUALS(a1.uin[2], 218498574);
    TS_ASSERT_EQUALS(a1.in[0], -486539210);
    TS_ASSERT_EQUALS(a1.in[1], 171);
    TS_ASSERT_EQUALS(a1.in[2], 2566938112);
    TS_ASSERT_EQUALS(a1.ulg[0], 302186626);
    TS_ASSERT_EQUALS(a1.ulg[1], 3221358200);
    TS_ASSERT_EQUALS(a1.ulg[2], 326513578);
    TS_ASSERT_EQUALS(a1.lg[0], -1308162253);
    TS_ASSERT_EQUALS(a1.lg[1], 672);
    TS_ASSERT_EQUALS(a1.lg[2], 2015441152);
    TS_ASSERT_EQUALS(a1.ulglg[0], 11242680142717650432);
    TS_ASSERT_EQUALS(a1.ulglg[1], 14);
    TS_ASSERT_EQUALS(a1.ulglg[2], 14988546933807513104);
    TS_ASSERT_EQUALS(a1.lglg[0], -7926616823443816680);
    TS_ASSERT_EQUALS(a1.lglg[1], 576460752304620594);
    TS_ASSERT_EQUALS(a1.lglg[2], 6926530057360113747);
    TS_ASSERT_EQUALS(a1.flt[0], 431.5f);
    TS_ASSERT_EQUALS(a1.flt[1], -163.25f);
    TS_ASSERT_EQUALS(a1.flt[2], 1.315e8f);
    TS_ASSERT_EQUALS(a1.dbl[0], 5000.625);
    TS_ASSERT_EQUALS(a1.dbl[1], -4212.125);
    TS_ASSERT_EQUALS(a1.dbl[2], -9.2125e10);
}

void StructVarTestSuite::testExtractBitVars(void)
{
	TS_ASSERT_EQUALS(b1.bv_1a, 1);
	TS_ASSERT_EQUALS(b1.bv_2a, 2);
	TS_ASSERT_EQUALS(b1.bv_6a, 11);
	TS_ASSERT_EQUALS(b1.bv_1b, 1);
	TS_ASSERT_EQUALS(b1.bv_8b, 120);
	TS_ASSERT_EQUALS(b1.bv_16b, 38036);
	TS_ASSERT_EQUALS(b1.bv_24b, 225000);
	TS_ASSERT_EQUALS(b1.bv_32b, -1000000);
	TS_ASSERT_EQUALS(b1.bv_7b, 82);
	TS_ASSERT_EQUALS(b1.bv_1c, 1);
	TS_ASSERT_EQUALS(b1.bv_15c, 6000);
	TS_ASSERT_EQUALS(b1.bv_1d, 0);
	TS_ASSERT_EQUALS(b1.bv_23d, 8138608);
	TS_ASSERT_EQUALS(b1.bv_1e, 1);
	TS_ASSERT_EQUALS(b1.bv_31e, 5700000);
	TS_ASSERT_EQUALS(b1.bv_1f, 1);
	TS_ASSERT_EQUALS(b1.rbv_1f, 0);
	TS_ASSERT_EQUALS(b1.rbv_2f, 2);
	TS_ASSERT_EQUALS(b1.rbv_6f, 17);
	TS_ASSERT_EQUALS(b1.rbv_8f, 30);
	TS_ASSERT_EQUALS(b1.rbv_9f, 57);
	TS_ASSERT_EQUALS(b1.rbv_16f, 3246);
	TS_ASSERT_EQUALS(b1.rbv_17f, 2897);
	TS_ASSERT_EQUALS(b1.rbv_24f, 33360);
	TS_ASSERT_EQUALS(b1.rbv_25f, 1312000);
	TS_ASSERT_EQUALS(b1.rbv_32f, 57114623);
    TS_ASSERT_EQUALS(b1.in, 43323563);
    TS_ASSERT_EQUALS(b1.rbv_3g, 3);
    TS_ASSERT_EQUALS(b1.bv_3g, 5);
    TS_ASSERT_EQUALS(b1.rbv_4g, 13);
    TS_ASSERT_EQUALS(b1.bv_2g[0], 0);
    TS_ASSERT_EQUALS(b1.bv_2g[1], 1);
    TS_ASSERT_EQUALS(b1.bv_2g[2], 3);
    TS_ASSERT_EQUALS(b1.rbv_2g[0], 1);
    TS_ASSERT_EQUALS(b1.rbv_2g[1], 2);
    TS_ASSERT_EQUALS(b1.rbv_2g[2], 3);
    TS_ASSERT_EQUALS(b1.rbv_5g, 17);
    TS_ASSERT_EQUALS(b1.bv_10g[0], 513);
    TS_ASSERT_EQUALS(b1.bv_10g[1], 1020);
    TS_ASSERT_EQUALS(b1.bv_10g[2], 750);
    TS_ASSERT_EQUALS(b1.rbv_10g[0], 751);
    TS_ASSERT_EQUALS(b1.rbv_10g[1], 1);
    TS_ASSERT_EQUALS(b1.rbv_10g[2], 882);
    TS_ASSERT_EQUALS(b1.bv_7g, 88);
}

void StructVarTestSuite::testExtractDecodeVars(void)
{
    TS_ASSERT_DELTA(d1.dv_char, 30., 60./256.);
    TS_ASSERT_DELTA(d1.dv_short, -45., 90./65536.);
    TS_ASSERT_DELTA(d1.dv_int, 90., 1.e-4);
    TS_ASSERT_DELTA(d1.rdv_char, 15., 60./256.);
    TS_ASSERT_DELTA(d1.rdv_short, -67.5, 90./65536.);
    TS_ASSERT_DELTA(d1.rdv_int, 135., 1e-4);
    TS_ASSERT_DELTA(d1.adv_char[0], -37.45, 100./256.);
    TS_ASSERT_DELTA(d1.adv_char[1], -43.75, 100./256.);
    TS_ASSERT_DELTA(d1.adv_char[2], -46.86, 100./256.);
    TS_ASSERT_DELTA(d1.adv_int[0], -187.5, 1e-4);
    TS_ASSERT_DELTA(d1.adv_int[1], -249.0234, 1e-4);
    TS_ASSERT_DELTA(d1.adv_int[2], -249.9961853, 1e-7);
    TS_ASSERT_DELTA(d1.radv_short[0], -75., 200./65536);
    TS_ASSERT_DELTA(d1.radv_short[1], -96.875, 200./65536);
    TS_ASSERT_DELTA(d1.radv_short[2], -99.60955, 200./65536);
}

void StructVarTestSuite::testExtractDecodeBitVars(void)
{
    TS_ASSERT_EQUALS(db1.bv_3a, 3);
    TS_ASSERT_EQUALS(db1.bv_3b, 6);
    TS_ASSERT_EQUALS(db1.rbv_3c, 5);
    TS_ASSERT_DELTA(db1.dbv_4, 3.5, 1e-1);
    TS_ASSERT_DELTA(db1.dbv_12, -68.043956, 1e-6);
    TS_ASSERT_DELTA(db1.dbv_20, -154.250769, 1e-6);
    TS_ASSERT_DELTA(db1.dbv_28, -177.317791, 1e-6);
    TS_ASSERT_DELTA(db1.rdbv_4, 2, 1e-1);
    TS_ASSERT_DELTA(db1.rdbv_12, -85.384615, 1e-6);
    TS_ASSERT_DELTA(db1.rdbv_20, -167.279193, 1e-6);
    TS_ASSERT_DELTA(db1.rdbv_28, -175.233564, 1e-6);
    TS_ASSERT_DELTA(db1.rdbv_5, -0.806452, 1e-6);
}

extern "C" void * gen_test_typ_g1_ptr;
gen_test_typ g2;

void StructVarTestSuite::testCopyAndAssignment1()
{
    gen_test_typ_g1_ptr = &g2;
    memset(gen_test_typ_g1_ptr, 255, 8);
}
/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_rgen_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_RGenVarTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_rgen_var\test_rgen_var.h"

static RGenVarTestSuite suite_RGenVarTestSuite;

static CxxTest::List Tests_RGenVarTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_RGenVarTestSuite( "test_rgen_var/test_rgen_var.h", 6, "RGenVarTestSuite", suite_RGenVarTestSuite, Tests_RGenVarTestSuite );

static class TestDescription_suite_RGenVarTestSuite_testChar : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_RGenVarTestSuite_testChar() : CxxTest::RealTestDescription( Tests_RGenVarTestSuite, suiteDescription_RGenVarTestSuite, 9, "testChar" ) {}
 void runTest() { suite_RGenVarTestSuite.testChar(); }
} testDescription_suite_RGenVarTestSuite_testChar;

static class TestDescription_suite_RGenVarTestSuite_testShort : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_RGenVarTestSuite_testShort() : CxxTest::RealTestDescription( Tests_RGenVarTestSuite, suiteDescription_RGenVarTestSuite, 10, "testShort" ) {}
 void runTest() { suite_RGenVarTestSuite.testShort(); }
} testDescription_suite_RGenVarTestSuite_testShort;

static class TestDescription_suite_RGenVarTestSuite_testInt : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_RGenVarTestSuite_testInt() : CxxTest::RealTestDescription( Tests_RGenVarTestSuite, suiteDescription_RGenVarTestSuite, 11, "testInt" ) {}
 void runTest() { suite_RGenVarTestSuite.testInt(); }
} testDescription_suite_RGenVarTestSuite_testInt;

static class TestDescription_suite_RGenVarTestSuite_testLong : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_RGenVarTestSuite_testLong() : CxxTest::RealTestDescription( Tests_RGenVarTestSuite, suiteDescription_RGenVarTestSuite, 12, "testLong" ) {}
 void runTest() { suite_RGenVarTestSuite.testLong(); }
} testDescription_suite_RGenVarTestSuite_testLong;

static class TestDescription_suite_RGenVarTestSuite_testLongLong : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_RGenVarTestSuite_testLongLong() : CxxTest::RealTestDescription( Tests_RGenVarTestSuite, suiteDescription_RGenVarTestSuite, 13, "testLongLong" ) {}
 void runTest() { suite_RGenVarTestSuite.testLongLong(); }
} testDescription_suite_RGenVarTestSuite_testLongLong;

static class TestDescription_suite_RGenVarTestSuite_testUnsignedChar : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_RGenVarTestSuite_testUnsignedChar() : CxxTest::RealTestDescription( Tests_RGenVarTestSuite, suiteDescription_RGenVarTestSuite, 14, "testUnsignedChar" ) {}
 void runTest() { suite_RGenVarTestSuite.testUnsignedChar(); }
} testDescription_suite_RGenVarTestSuite_testUnsignedChar;

static class TestDescription_suite_RGenVarTestSuite_testUnsignedShort : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_RGenVarTestSuite_testUnsignedShort() : CxxTest::RealTestDescription( Tests_RGenVarTestSuite, suiteDescription_RGenVarTestSuite, 15, "testUnsignedShort" ) {}
 void runTest() { suite_RGenVarTestSuite.testUnsignedShort(); }
} testDescription_suite_RGenVarTestSuite_testUnsignedShort;

static class TestDescription_suite_RGenVarTestSuite_testUnsignedInt : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_RGenVarTestSuite_testUnsignedInt() : CxxTest::RealTestDescription( Tests_RGenVarTestSuite, suiteDescription_RGenVarTestSuite, 16, "testUnsignedInt" ) {}
 void runTest() { suite_RGenVarTestSuite.testUnsignedInt(); }
} testDescription_suite_RGenVarTestSuite_testUnsignedInt;

static class TestDescription_suite_RGenVarTestSuite_testUnsignedLong : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_RGenVarTestSuite_testUnsignedLong() : CxxTest::RealTestDescription( Tests_RGenVarTestSuite, suiteDescription_RGenVarTestSuite, 17, "testUnsignedLong" ) {}
 void runTest() { suite_RGenVarTestSuite.testUnsignedLong(); }
} testDescription_suite_RGenVarTestSuite_testUnsignedLong;

static class TestDescription_suite_RGenVarTestSuite_testUnsignedLongLong : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_RGenVarTestSuite_testUnsignedLongLong() : CxxTest::RealTestDescription( Tests_RGenVarTestSuite, suiteDescription_RGenVarTestSuite, 18, "testUnsignedLongLong" ) {}
 void runTest() { suite_RGenVarTestSuite.testUnsignedLongLong(); }
} testDescription_suite_RGenVarTestSuite_testUnsignedLongLong;

static class TestDescription_suite_RGenVarTestSuite_testFloat : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_RGenVarTestSuite_testFloat() : CxxTest::RealTestDescription( Tests_RGenVarTestSuite, suiteDescription_RGenVarTestSuite, 19, "testFloat" ) {}
 void runTest() { suite_RGenVarTestSuite.testFloat(); }
} testDescription_suite_RGenVarTestSuite_testFloat;

static class TestDescription_suite_RGenVarTestSuite_testDouble : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_RGenVarTestSuite_testDouble() : CxxTest::RealTestDescription( Tests_RGenVarTestSuite, suiteDescription_RGenVarTestSuite, 20, "testDouble" ) {}
 void runTest() { suite_RGenVarTestSuite.testDouble(); }
} testDescription_suite_RGenVarTestSuite_testDouble;

static class TestDescription_suite_RGenVarTestSuite_testExtract : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_RGenVarTestSuite_testExtract() : CxxTest::RealTestDescription( Tests_RGenVarTestSuite, suiteDescription_RGenVarTestSuite, 21, "testExtract" ) {}
 void runTest() { suite_RGenVarTestSuite.testExtract(); }
} testDescription_suite_RGenVarTestSuite_testExtract;

static class TestDescription_suite_RGenVarTestSuite_testOutput : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_RGenVarTestSuite_testOutput() : CxxTest::RealTestDescription( Tests_RGenVarTestSuite, suiteDescription_RGenVarTestSuite, 22, "testOutput" ) {}
 void runTest() { suite_RGenVarTestSuite.testOutput(); }
} testDescription_suite_RGenVarTestSuite_testOutput;

static class TestDescription_suite_RGenVarTestSuite_testStream : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_RGenVarTestSuite_testStream() : CxxTest::RealTestDescription( Tests_RGenVarTestSuite, suiteDescription_RGenVarTestSuite, 23, "testStream" ) {}
 void runTest() { suite_RGenVarTestSuite.testStream(); }
} testDescription_suite_RGenVarTestSuite_testStream;

static class TestDescription_suite_RGenVarTestSuite_testToValue : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_RGenVarTestSuite_testToValue() : CxxTest::RealTestDescription( Tests_RGenVarTestSuite, suiteDescription_RGenVarTestSuite, 24, "testToValue" ) {}
 void runTest() { suite_RGenVarTestSuite.testToValue(); }
} testDescription_suite_RGenVarTestSuite_testToValue;

static class TestDescription_suite_RGenVarTestSuite_testFromValue : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_RGenVarTestSuite_testFromValue() : CxxTest::RealTestDescription( Tests_RGenVarTestSuite, suiteDescription_RGenVarTestSuite, 25, "testFromValue" ) {}
 void runTest() { suite_RGenVarTestSuite.testFromValue(); }
} testDescription_suite_RGenVarTestSuite_testFromValue;

static class TestDescription_suite_RGenVarTestSuite_testCopyConstructor : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_RGenVarTestSuite_testCopyConstructor() : CxxTest::RealTestDescription( Tests_RGenVarTestSuite, suiteDescription_RGenVarTestSuite, 26, "testCopyConstructor" ) {}
 void runTest() { suite_RGenVarTestSuite.testCopyConstructor(); }
} testDescription_suite_RGenVarTestSuite_testCopyConstructor;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

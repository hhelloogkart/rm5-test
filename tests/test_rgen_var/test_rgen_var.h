#ifndef _TEST_RGEN_VAR_H_DEF_
#define _TEST_RGEN_VAR_H_DEF_

#include <cxxtest/TestSuite.h>

class RGenVarTestSuite : public CxxTest::TestSuite
{
public:
    void testChar(void);
    void testShort(void);
    void testInt(void);
    void testLong(void);
    void testLongLong(void);
    void testUnsignedChar(void);
    void testUnsignedShort(void);
    void testUnsignedInt(void);
    void testUnsignedLong(void);
    void testUnsignedLongLong(void);
    void testFloat(void);
    void testDouble(void);
    void testExtract(void);
    void testOutput(void);
    void testStream(void);
    void testToValue(void);
    void testFromValue(void);
    void testCopyConstructor(void);
};

#endif


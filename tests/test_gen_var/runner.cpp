/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_gen_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_GenVarTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_gen_var\test_gen_var.h"

static GenVarTestSuite suite_GenVarTestSuite;

static CxxTest::List Tests_GenVarTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_GenVarTestSuite( "test_gen_var/test_gen_var.h", 6, "GenVarTestSuite", suite_GenVarTestSuite, Tests_GenVarTestSuite );

static class TestDescription_suite_GenVarTestSuite_testChar : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_GenVarTestSuite_testChar() : CxxTest::RealTestDescription( Tests_GenVarTestSuite, suiteDescription_GenVarTestSuite, 9, "testChar" ) {}
 void runTest() { suite_GenVarTestSuite.testChar(); }
} testDescription_suite_GenVarTestSuite_testChar;

static class TestDescription_suite_GenVarTestSuite_testShort : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_GenVarTestSuite_testShort() : CxxTest::RealTestDescription( Tests_GenVarTestSuite, suiteDescription_GenVarTestSuite, 10, "testShort" ) {}
 void runTest() { suite_GenVarTestSuite.testShort(); }
} testDescription_suite_GenVarTestSuite_testShort;

static class TestDescription_suite_GenVarTestSuite_testInt : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_GenVarTestSuite_testInt() : CxxTest::RealTestDescription( Tests_GenVarTestSuite, suiteDescription_GenVarTestSuite, 11, "testInt" ) {}
 void runTest() { suite_GenVarTestSuite.testInt(); }
} testDescription_suite_GenVarTestSuite_testInt;

static class TestDescription_suite_GenVarTestSuite_testLong : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_GenVarTestSuite_testLong() : CxxTest::RealTestDescription( Tests_GenVarTestSuite, suiteDescription_GenVarTestSuite, 12, "testLong" ) {}
 void runTest() { suite_GenVarTestSuite.testLong(); }
} testDescription_suite_GenVarTestSuite_testLong;

static class TestDescription_suite_GenVarTestSuite_testLongLong : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_GenVarTestSuite_testLongLong() : CxxTest::RealTestDescription( Tests_GenVarTestSuite, suiteDescription_GenVarTestSuite, 13, "testLongLong" ) {}
 void runTest() { suite_GenVarTestSuite.testLongLong(); }
} testDescription_suite_GenVarTestSuite_testLongLong;

static class TestDescription_suite_GenVarTestSuite_testUnsignedChar : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_GenVarTestSuite_testUnsignedChar() : CxxTest::RealTestDescription( Tests_GenVarTestSuite, suiteDescription_GenVarTestSuite, 14, "testUnsignedChar" ) {}
 void runTest() { suite_GenVarTestSuite.testUnsignedChar(); }
} testDescription_suite_GenVarTestSuite_testUnsignedChar;

static class TestDescription_suite_GenVarTestSuite_testUnsignedShort : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_GenVarTestSuite_testUnsignedShort() : CxxTest::RealTestDescription( Tests_GenVarTestSuite, suiteDescription_GenVarTestSuite, 15, "testUnsignedShort" ) {}
 void runTest() { suite_GenVarTestSuite.testUnsignedShort(); }
} testDescription_suite_GenVarTestSuite_testUnsignedShort;

static class TestDescription_suite_GenVarTestSuite_testUnsignedInt : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_GenVarTestSuite_testUnsignedInt() : CxxTest::RealTestDescription( Tests_GenVarTestSuite, suiteDescription_GenVarTestSuite, 16, "testUnsignedInt" ) {}
 void runTest() { suite_GenVarTestSuite.testUnsignedInt(); }
} testDescription_suite_GenVarTestSuite_testUnsignedInt;

static class TestDescription_suite_GenVarTestSuite_testUnsignedLong : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_GenVarTestSuite_testUnsignedLong() : CxxTest::RealTestDescription( Tests_GenVarTestSuite, suiteDescription_GenVarTestSuite, 17, "testUnsignedLong" ) {}
 void runTest() { suite_GenVarTestSuite.testUnsignedLong(); }
} testDescription_suite_GenVarTestSuite_testUnsignedLong;

static class TestDescription_suite_GenVarTestSuite_testUnsignedLongLong : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_GenVarTestSuite_testUnsignedLongLong() : CxxTest::RealTestDescription( Tests_GenVarTestSuite, suiteDescription_GenVarTestSuite, 18, "testUnsignedLongLong" ) {}
 void runTest() { suite_GenVarTestSuite.testUnsignedLongLong(); }
} testDescription_suite_GenVarTestSuite_testUnsignedLongLong;

static class TestDescription_suite_GenVarTestSuite_testFloat : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_GenVarTestSuite_testFloat() : CxxTest::RealTestDescription( Tests_GenVarTestSuite, suiteDescription_GenVarTestSuite, 19, "testFloat" ) {}
 void runTest() { suite_GenVarTestSuite.testFloat(); }
} testDescription_suite_GenVarTestSuite_testFloat;

static class TestDescription_suite_GenVarTestSuite_testDouble : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_GenVarTestSuite_testDouble() : CxxTest::RealTestDescription( Tests_GenVarTestSuite, suiteDescription_GenVarTestSuite, 20, "testDouble" ) {}
 void runTest() { suite_GenVarTestSuite.testDouble(); }
} testDescription_suite_GenVarTestSuite_testDouble;

static class TestDescription_suite_GenVarTestSuite_testExtract : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_GenVarTestSuite_testExtract() : CxxTest::RealTestDescription( Tests_GenVarTestSuite, suiteDescription_GenVarTestSuite, 21, "testExtract" ) {}
 void runTest() { suite_GenVarTestSuite.testExtract(); }
} testDescription_suite_GenVarTestSuite_testExtract;

static class TestDescription_suite_GenVarTestSuite_testOutput : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_GenVarTestSuite_testOutput() : CxxTest::RealTestDescription( Tests_GenVarTestSuite, suiteDescription_GenVarTestSuite, 22, "testOutput" ) {}
 void runTest() { suite_GenVarTestSuite.testOutput(); }
} testDescription_suite_GenVarTestSuite_testOutput;

static class TestDescription_suite_GenVarTestSuite_testStream : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_GenVarTestSuite_testStream() : CxxTest::RealTestDescription( Tests_GenVarTestSuite, suiteDescription_GenVarTestSuite, 23, "testStream" ) {}
 void runTest() { suite_GenVarTestSuite.testStream(); }
} testDescription_suite_GenVarTestSuite_testStream;

static class TestDescription_suite_GenVarTestSuite_testToValue : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_GenVarTestSuite_testToValue() : CxxTest::RealTestDescription( Tests_GenVarTestSuite, suiteDescription_GenVarTestSuite, 24, "testToValue" ) {}
 void runTest() { suite_GenVarTestSuite.testToValue(); }
} testDescription_suite_GenVarTestSuite_testToValue;

static class TestDescription_suite_GenVarTestSuite_testFromValue : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_GenVarTestSuite_testFromValue() : CxxTest::RealTestDescription( Tests_GenVarTestSuite, suiteDescription_GenVarTestSuite, 25, "testFromValue" ) {}
 void runTest() { suite_GenVarTestSuite.testFromValue(); }
} testDescription_suite_GenVarTestSuite_testFromValue;

static class TestDescription_suite_GenVarTestSuite_testCopyConstructor : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_GenVarTestSuite_testCopyConstructor() : CxxTest::RealTestDescription( Tests_GenVarTestSuite, suiteDescription_GenVarTestSuite, 26, "testCopyConstructor" ) {}
 void runTest() { suite_GenVarTestSuite.testCopyConstructor(); }
} testDescription_suite_GenVarTestSuite_testCopyConstructor;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

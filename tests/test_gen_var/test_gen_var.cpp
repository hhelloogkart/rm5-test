#include "test_gen_var.h"
#include <gen_var.h>
#include <rgen_var.h>
#include <cplx_var.h>
#include <mpt_show.h>
#include <outbuf.h>
#include <util/utilcore.h>
#include <sstream>
#include <string.h>
#include <limits.h>

#define TS_ASSERT_NOT(x) TS_ASSERT(!(x))

class test_typ : public complex_var
{
public:
    COMPLEXCONST(test_typ)
    generic_var<char> vch;
    generic_var<short> vsh;
    generic_var<int> vit;
    generic_var<long> vlg;
    generic_var<long long> vll;
    generic_var<unsigned char> uch;
    generic_var<unsigned short> ush;
    generic_var<unsigned int> uit;
    generic_var<unsigned long> ulg;
    generic_var<unsigned long long> ull;
    generic_var<float> flt;
    generic_var<double> dbl;
};

class rtest_typ : public complex_var
{
public:
    COMPLEXCONST(rtest_typ)
    r_generic_var<char> vch;
    r_generic_var<short> vsh;
    r_generic_var<int> vit;
    r_generic_var<long> vlg;
    r_generic_var<long long> vll;
    r_generic_var<unsigned char> uch;
    r_generic_var<unsigned short> ush;
    r_generic_var<unsigned int> uit;
    r_generic_var<unsigned long> ulg;
    r_generic_var<unsigned long long> ull;
    r_generic_var<float> flt;
    r_generic_var<double> dbl;
};

template<class C>
unsigned char *tmemcpy(const C &v, unsigned char *buf)
{
    memcpy(buf, &v, sizeof(C));
    return buf + sizeof(C);
}

template<class C>
unsigned char *rmemcpy(const C&v, unsigned char *buf)
{
    const unsigned char *pbuf = reinterpret_cast<const unsigned char *>(&v) + sizeof(C);
    unsigned char * const ebuf = buf + sizeof(C);
    do
    {
        --pbuf;
        *buf = *pbuf;
        ++buf;
    } while (buf != ebuf);
    return ebuf;
}

class dirty_checker : public mpt_baseshow
{
public:
    dirty_checker(mpt_var *myvar);
    virtual void setDirty(mpt_var *myvar);
    virtual void notDirty(mpt_var *myvar);
    int getStateAndReset();
protected:
    int state; // 0 - not called, 1 - dirty, 2 - notdirty, 3 - error
    mpt_var *var;
};

dirty_checker::dirty_checker(mpt_var *myvar) : state(0), var(myvar)
{
    myvar->addCallback(this);
}

void dirty_checker::setDirty(mpt_var *myvar)
{
    if (myvar == var) state = 1;
    else state = 3;
}

void dirty_checker::notDirty(mpt_var *myvar)
{
    if (myvar == var) state = 2;
    else state = 3;
}

int dirty_checker::getStateAndReset()
{
    int ret = state;
    state = 0;
    return ret;
}

void GenVarTestSuite::testChar(void)
{
    const char const_1 = 1;
    const char const_2 = 2;
    const char const_3 = 3;
    generic_var<char> gv_1(const_1);
    generic_var<char> gv_2(const_2);
    generic_var<char> gv_3a(const_3);
    generic_var<char> gv_3b(gv_3a);
    generic_var<char> gv_5((char)'\5');
    generic_var<char> gv_invalid;
    generic_var<int>  gv_int1(1);
    r_generic_var<char> rgv_1(const_1);
    r_generic_var<char> rgv_2(const_2);

    value_var::set_uint_format("%lu");
    value_var::set_int_format("%ld");

    // Equal test (allows other value_var types)
    TS_ASSERT(gv_1 == const_1);
    TS_ASSERT(gv_1 == (char)'\1');
    TS_ASSERT(const_1 == gv_1);
    TS_ASSERT(gv_1 == gv_int1);
    TS_ASSERT(gv_3a == gv_3b);
    TS_ASSERT(gv_3a == 3);
    TS_ASSERT(gv_invalid == -1);
    TS_ASSERT(gv_1 == rgv_1);
    TS_ASSERT(rgv_1 == gv_1);

    TS_ASSERT_NOT(gv_1 == const_2);
    TS_ASSERT_NOT(gv_1 == (char)'\2');
    TS_ASSERT_NOT(gv_1 == gv_2);
    TS_ASSERT_NOT(const_2 == gv_1);
    TS_ASSERT_NOT(gv_2 == gv_int1);
    TS_ASSERT_NOT(gv_2 == 1);
    TS_ASSERT_NOT(gv_2 == rgv_1);
    TS_ASSERT_NOT(rgv_1 == gv_2);

    // Not equal test (allows other value_var types)
    TS_ASSERT_NOT(gv_1 != const_1);
    TS_ASSERT_NOT(gv_1 != (char)'\1');
    TS_ASSERT_NOT(const_1 != gv_1);
    TS_ASSERT_NOT(gv_1 != gv_int1);
    TS_ASSERT_NOT(gv_3a != gv_3b);
    TS_ASSERT_NOT(gv_3a != 3);
    TS_ASSERT_NOT(gv_invalid != -1);
    TS_ASSERT_NOT(gv_1 != rgv_1);
    TS_ASSERT_NOT(rgv_1 != gv_1);

    TS_ASSERT(gv_1 != const_2);
    TS_ASSERT(gv_1 != (char)'\2');
    TS_ASSERT(gv_1 != gv_2);
    TS_ASSERT(const_2 != gv_1);
    TS_ASSERT(gv_2 != gv_int1);
    TS_ASSERT(gv_2 != 1);
    TS_ASSERT(gv_2 != rgv_1);
    TS_ASSERT(rgv_1 != gv_2);

    // Less than test (same type only)
    TS_ASSERT_NOT(gv_1 < const_1);
    TS_ASSERT_NOT(gv_1 < (char)'\1');
    TS_ASSERT_NOT(const_1 < gv_1);
    TS_ASSERT_NOT(gv_3a < gv_3b);
    TS_ASSERT_NOT(gv_1 < rgv_1);
    TS_ASSERT_NOT(rgv_1 < gv_1);

    TS_ASSERT(gv_1 < const_2);
    TS_ASSERT(gv_1 < gv_2);
    TS_ASSERT(const_1 < gv_2);
    TS_ASSERT(rgv_1 < gv_2);
    TS_ASSERT(gv_1 < rgv_2);

    TS_ASSERT_NOT(gv_2 < const_1);
    TS_ASSERT_NOT(gv_2 < gv_1);
    TS_ASSERT_NOT(const_2 < gv_1);
    TS_ASSERT_NOT(rgv_2 < gv_1);
    TS_ASSERT_NOT(gv_2 < rgv_1);

    // Less than or equal test (same type only)
    TS_ASSERT(gv_1 <= const_1);
    TS_ASSERT(gv_1 <= (char)'\1');
    TS_ASSERT(const_1 <= gv_1);
    TS_ASSERT(gv_3a <= gv_3b);
    TS_ASSERT(gv_1 <= rgv_1);
    TS_ASSERT(rgv_1 <= gv_1);

    TS_ASSERT(gv_1 <= const_2);
    TS_ASSERT(gv_1 <= gv_2);
    TS_ASSERT(const_1 <= gv_2);
    TS_ASSERT(rgv_1 <= gv_2);
    TS_ASSERT(gv_1 <= rgv_2);

    TS_ASSERT_NOT(gv_2 <= const_1);
    TS_ASSERT_NOT(gv_2 <= gv_1);
    TS_ASSERT_NOT(const_2 <= gv_1);
    TS_ASSERT_NOT(rgv_2 <= gv_1);
    TS_ASSERT_NOT(gv_2 <= rgv_1);

    // More than test (same type only)
    TS_ASSERT_NOT(gv_1 > const_1);
    TS_ASSERT_NOT(gv_1 > (char)'\1');
    TS_ASSERT_NOT(const_1 > gv_1);
    TS_ASSERT_NOT(gv_3a > gv_3b);
    TS_ASSERT_NOT(gv_1 > rgv_1);
    TS_ASSERT_NOT(rgv_1 > gv_1);

    TS_ASSERT_NOT(gv_1 > const_2);
    TS_ASSERT_NOT(gv_1 > gv_2);
    TS_ASSERT_NOT(const_1 > gv_2);
    TS_ASSERT_NOT(rgv_1 > gv_2);
    TS_ASSERT_NOT(gv_1 > rgv_2);

    TS_ASSERT(gv_2 > const_1);
    TS_ASSERT(gv_2 > gv_1);
    TS_ASSERT(const_2 > gv_1);
    TS_ASSERT(rgv_2 > gv_1);
    TS_ASSERT(gv_2 > rgv_1);

    // More than and equal test (same type only)
    TS_ASSERT(gv_1 >= const_1);
    TS_ASSERT(gv_1 >= (char)'\1');
    TS_ASSERT(const_1 >= gv_1);
    TS_ASSERT(gv_3a >= gv_3b);
    TS_ASSERT(gv_1 >= rgv_1);
    TS_ASSERT(rgv_1 >= gv_1);

    TS_ASSERT_NOT(gv_1 >= const_2);
    TS_ASSERT_NOT(gv_1 >= gv_2);
    TS_ASSERT_NOT(const_1 >= gv_2);
    TS_ASSERT_NOT(rgv_1 >= gv_2);
    TS_ASSERT_NOT(gv_1 >= rgv_2);

    TS_ASSERT(gv_2 >= const_1);
    TS_ASSERT(gv_2 >= gv_1);
    TS_ASSERT(const_2 >= gv_1);
    TS_ASSERT(rgv_2 >= gv_1);
    TS_ASSERT(gv_2 >= rgv_1);

    // Plus (same type only)
    TS_ASSERT(gv_1 + const_2 == const_3);
    TS_ASSERT(const_1 + gv_2 == const_3);
    TS_ASSERT(gv_1 + const_2 == gv_3a);
    TS_ASSERT(const_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + gv_2 == gv_3a);
    TS_ASSERT(rgv_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + rgv_2 == gv_3a);

    // Minus (same type only)
    TS_ASSERT(gv_2 - const_1 == const_1);
    TS_ASSERT(const_2 - gv_1 == const_1);
    TS_ASSERT(gv_2 - const_1 == gv_1);
    TS_ASSERT(const_2 - gv_1 == gv_1);
    TS_ASSERT(gv_3a - gv_1 == gv_2);
    TS_ASSERT(gv_2 - rgv_1 == gv_1);
    TS_ASSERT(rgv_2 - gv_1 == rgv_1);

    // Multiply (same type only)
    TS_ASSERT(gv_2 * const_1 == const_2);
    TS_ASSERT(const_2 * gv_1 == const_2);
    TS_ASSERT(gv_2 * const_1 == gv_2);
    TS_ASSERT(const_2 * gv_1 == gv_2);
    TS_ASSERT(gv_2 * gv_1 == gv_2);
    TS_ASSERT(rgv_1 * gv_2 == gv_2);
    TS_ASSERT(gv_1 * rgv_2 == rgv_2);

    // Divide (same type only)
    TS_ASSERT(gv_3a / const_2 == const_1);
    TS_ASSERT(const_3 / gv_2 == const_1);
    TS_ASSERT(gv_3a / const_2 == gv_1);
    TS_ASSERT(const_3 / gv_2 == gv_1);
    TS_ASSERT(gv_3a / gv_2 == gv_1);
    TS_ASSERT(gv_2 / rgv_2 == gv_1);
    TS_ASSERT(rgv_2 / gv_2 == rgv_1);

    // Assign (allows other value_var types)
    dirty_checker checker1(&gv_1);
    dirty_checker checker2(&gv_3a);
    dirty_checker checker3(&rgv_1);
    gv_1 = const_1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    gv_3a = gv_3b;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 2);
    gv_3a = (char)'\3';
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 2);
    gv_1 = gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    gv_1 = rgv_1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    rgv_1 = gv_1;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 2);
    gv_1 = const_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = (char)'\4';
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = gv_3a;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 0);
    gv_1 = 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    rgv_1 = gv_2;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 1);
    gv_1 = gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    rgv_1 = gv_int1;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 1);

    // Plus-Assign (allows other value_var types)
    gv_1 += gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (char)'\2');
    gv_1 += gv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (char)'\4');
    gv_1 += const_3;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (char)'\7');
    gv_1 += (char)'\4';
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (char)'\xb');
    gv_1 += 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (char)'\x10');
    gv_1 += rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (short)0x12);

    // Minus-Assign (allows other value_var types)
    gv_1 -= rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (short)0x10);
    gv_1 -= 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (char)'\xb');
    gv_1 -= (char)'\4';
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (char)'\7');
    gv_1 -= const_3;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (char)'\4');
    gv_1 -= gv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (char)'\2');
    gv_1 -= gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (char)'\1');

    // Multiply-Assign (allows other value_var types)
    gv_3a *= gv_int1;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (char)'\3');
    gv_3a *= gv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (char)'\6');
    gv_3a *= const_3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (char)'\x12');
    gv_3a *= (char)'\2';
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (char)'\x24');
    gv_3a *= 2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (char)'\x48');

    // Divide-Assign (allows other value_var types)
    gv_3a /= 2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (char)'\x24');
    gv_3a /= (char)'\2';
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (char)'\x12');
    gv_3a /= const_3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (char)'\6');
    gv_3a /= gv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (char)'\3');
    gv_3a /= gv_int1;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (char)'\3');

    // Repeat Multiply and Divide Assign
    gv_3a *= rgv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (char)6);
    gv_3a /= rgv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (char)3);

    // Negate
    const char ng = -gv_3a;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 0);
    TS_ASSERT_EQUALS(ng, (char)-3);
    TS_ASSERT_EQUALS(gv_3a(), (char)3);

    // Illegal compare
    test_typ mytest;
    TS_ASSERT(gv_1 != mytest);

    // Next
    generic_var<char> arry[2];
    TS_ASSERT_EQUALS(&arry[1], arry[0].getNext());
}

void GenVarTestSuite::testShort(void)
{
    const short const_1 = 1;
    const short const_2 = 2;
    const short const_3 = 3;
    generic_var<short> gv_1(const_1);
    generic_var<short> gv_2(const_2);
    generic_var<short> gv_3a(const_3);
    generic_var<short> gv_3b(gv_3a);
    generic_var<short> gv_5((short)5);
    generic_var<short> gv_invalid;
    generic_var<int>  gv_int1(1);
    r_generic_var<short> rgv_1(const_1);
    r_generic_var<short> rgv_2(const_2);

    // Equal test (allows other value_var types)
    TS_ASSERT(gv_1 == const_1);
    TS_ASSERT(gv_1 == (short)1);
    TS_ASSERT(const_1 == gv_1);
    TS_ASSERT(gv_1 == gv_int1);
    TS_ASSERT(gv_3a == gv_3b);
    TS_ASSERT(gv_3a == 3);
    TS_ASSERT(gv_invalid == -1);
    TS_ASSERT(gv_1 == rgv_1);
    TS_ASSERT(rgv_1 == gv_1);

    TS_ASSERT_NOT(gv_1 == const_2);
    TS_ASSERT_NOT(gv_1 == (short)2);
    TS_ASSERT_NOT(gv_1 == gv_2);
    TS_ASSERT_NOT(const_2 == gv_1);
    TS_ASSERT_NOT(gv_2 == gv_int1);
    TS_ASSERT_NOT(gv_2 == 1);
    TS_ASSERT_NOT(gv_2 == rgv_1);
    TS_ASSERT_NOT(rgv_1 == gv_2);

    // Not equal test (allows other value_var types)
    TS_ASSERT_NOT(gv_1 != const_1);
    TS_ASSERT_NOT(gv_1 != (short)1);
    TS_ASSERT_NOT(const_1 != gv_1);
    TS_ASSERT_NOT(gv_1 != gv_int1);
    TS_ASSERT_NOT(gv_3a != gv_3b);
    TS_ASSERT_NOT(gv_3a != 3);
    TS_ASSERT_NOT(gv_invalid != -1);
    TS_ASSERT_NOT(gv_1 != rgv_1);
    TS_ASSERT_NOT(rgv_1 != gv_1);

    TS_ASSERT(gv_1 != const_2);
    TS_ASSERT(gv_1 != (short)2);
    TS_ASSERT(gv_1 != gv_2);
    TS_ASSERT(const_2 != gv_1);
    TS_ASSERT(gv_2 != gv_int1);
    TS_ASSERT(gv_2 != 1);
    TS_ASSERT(gv_2 != rgv_1);
    TS_ASSERT(rgv_1 != gv_2);

    // Less than test (same type only)
    TS_ASSERT_NOT(gv_1 < const_1);
    TS_ASSERT_NOT(gv_1 < (short)1);
    TS_ASSERT_NOT(const_1 < gv_1);
    TS_ASSERT_NOT(gv_3a < gv_3b);
    TS_ASSERT_NOT(gv_1 < rgv_1);
    TS_ASSERT_NOT(rgv_1 < gv_1);

    TS_ASSERT(gv_1 < const_2);
    TS_ASSERT(gv_1 < gv_2);
    TS_ASSERT(const_1 < gv_2);
    TS_ASSERT(rgv_1 < gv_2);
    TS_ASSERT(gv_1 < rgv_2);

    TS_ASSERT_NOT(gv_2 < const_1);
    TS_ASSERT_NOT(gv_2 < gv_1);
    TS_ASSERT_NOT(const_2 < gv_1);
    TS_ASSERT_NOT(rgv_2 < gv_1);
    TS_ASSERT_NOT(gv_2 < rgv_1);

    // Less than or equal test (same type only)
    TS_ASSERT(gv_1 <= const_1);
    TS_ASSERT(gv_1 <= (short)1);
    TS_ASSERT(const_1 <= gv_1);
    TS_ASSERT(gv_3a <= gv_3b);
    TS_ASSERT(gv_1 <= rgv_1);
    TS_ASSERT(rgv_1 <= gv_1);

    TS_ASSERT(gv_1 <= const_2);
    TS_ASSERT(gv_1 <= gv_2);
    TS_ASSERT(const_1 <= gv_2);
    TS_ASSERT(rgv_1 <= gv_2);
    TS_ASSERT(gv_1 <= rgv_2);

    TS_ASSERT_NOT(gv_2 <= const_1);
    TS_ASSERT_NOT(gv_2 <= gv_1);
    TS_ASSERT_NOT(const_2 <= gv_1);
    TS_ASSERT_NOT(rgv_2 <= gv_1);
    TS_ASSERT_NOT(gv_2 <= rgv_1);

    // More than test (same type only)
    TS_ASSERT_NOT(gv_1 > const_1);
    TS_ASSERT_NOT(gv_1 > (short)1);
    TS_ASSERT_NOT(const_1 > gv_1);
    TS_ASSERT_NOT(gv_3a > gv_3b);
    TS_ASSERT_NOT(gv_1 > rgv_1);
    TS_ASSERT_NOT(rgv_1 > gv_1);

    TS_ASSERT_NOT(gv_1 > const_2);
    TS_ASSERT_NOT(gv_1 > gv_2);
    TS_ASSERT_NOT(const_1 > gv_2);
    TS_ASSERT_NOT(rgv_1 > gv_2);
    TS_ASSERT_NOT(gv_1 > rgv_2);

    TS_ASSERT(gv_2 > const_1);
    TS_ASSERT(gv_2 > gv_1);
    TS_ASSERT(const_2 > gv_1);
    TS_ASSERT(rgv_2 > gv_1);
    TS_ASSERT(gv_2 > rgv_1);

    // More than and equal test (same type only)
    TS_ASSERT(gv_1 >= const_1);
    TS_ASSERT(gv_1 >= (short)1);
    TS_ASSERT(const_1 >= gv_1);
    TS_ASSERT(gv_3a >= gv_3b);
    TS_ASSERT(gv_1 >= rgv_1);
    TS_ASSERT(rgv_1 >= gv_1);

    TS_ASSERT_NOT(gv_1 >= const_2);
    TS_ASSERT_NOT(gv_1 >= gv_2);
    TS_ASSERT_NOT(const_1 >= gv_2);
    TS_ASSERT_NOT(rgv_1 >= gv_2);
    TS_ASSERT_NOT(gv_1 >= rgv_2);

    TS_ASSERT(gv_2 >= const_1);
    TS_ASSERT(gv_2 >= gv_1);
    TS_ASSERT(const_2 >= gv_1);
    TS_ASSERT(rgv_2 >= gv_1);
    TS_ASSERT(gv_2 >= rgv_1);

    // Plus (same type only)
    TS_ASSERT(gv_1 + const_2 == const_3);
    TS_ASSERT(const_1 + gv_2 == const_3);
    TS_ASSERT(gv_1 + const_2 == gv_3a);
    TS_ASSERT(const_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + gv_2 == gv_3a);
    TS_ASSERT(rgv_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + rgv_2 == gv_3a);

    // Minus (same type only)
    TS_ASSERT(gv_2 - const_1 == const_1);
    TS_ASSERT(const_2 - gv_1 == const_1);
    TS_ASSERT(gv_2 - const_1 == gv_1);
    TS_ASSERT(const_2 - gv_1 == gv_1);
    TS_ASSERT(gv_3a - gv_1 == gv_2);
    TS_ASSERT(gv_2 - rgv_1 == gv_1);
    TS_ASSERT(rgv_2 - gv_1 == rgv_1);

    // Multiply (same type only)
    TS_ASSERT(gv_2 * const_1 == const_2);
    TS_ASSERT(const_2 * gv_1 == const_2);
    TS_ASSERT(gv_2 * const_1 == gv_2);
    TS_ASSERT(const_2 * gv_1 == gv_2);
    TS_ASSERT(gv_2 * gv_1 == gv_2);
    TS_ASSERT(rgv_1 * gv_2 == gv_2);
    TS_ASSERT(gv_1 * rgv_2 == rgv_2);

    // Divide (same type only)
    TS_ASSERT(gv_3a / const_2 == const_1);
    TS_ASSERT(const_3 / gv_2 == const_1);
    TS_ASSERT(gv_3a / const_2 == gv_1);
    TS_ASSERT(const_3 / gv_2 == gv_1);
    TS_ASSERT(gv_3a / gv_2 == gv_1);
    TS_ASSERT(gv_2 / rgv_2 == gv_1);
    TS_ASSERT(rgv_2 / gv_2 == rgv_1);

    // Assign (allows other value_var types)
    dirty_checker checker1(&gv_1);
    dirty_checker checker2(&gv_3a);
    dirty_checker checker3(&rgv_1);
    gv_1 = const_1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    gv_3a = gv_3b;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 2);
    gv_3a = (short)3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 2);
    gv_1 = gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    gv_1 = rgv_1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    rgv_1 = gv_1;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 2);
    gv_1 = const_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = (short)4;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = gv_3a;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 0);
    gv_1 = 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    rgv_1 = gv_2;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 1);
    gv_1 = gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    rgv_1 = gv_int1;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 1);

    // Plus-Assign (allows other value_var types)
    gv_1 += gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (short)2);
    gv_1 += gv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (short)4);
    gv_1 += const_3;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (short)7);
    gv_1 += (short)'\4';
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (short)0xb);
    gv_1 += 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (short)0x10);
    gv_1 += rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (short)0x12);

    // Minus-Assign (allows other value_var types)
    gv_1 -= rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (short)0x10);
    gv_1 -= 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (short)0xb);
    gv_1 -= (short)4;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (short)7);
    gv_1 -= const_3;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (short)4);
    gv_1 -= gv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (short)2);
    gv_1 -= gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (short)1);

    // Multiply-Assign (allows other value_var types)
    gv_3a *= gv_int1;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (short)3);
    gv_3a *= gv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (short)6);
    gv_3a *= const_3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (short)0x12);
    gv_3a *= (short)'\2';
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (short)0x24);
    gv_3a *= 2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (short)0x48);

    // Divide-Assign (allows other value_var types)
    gv_3a /= 2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (short)0x24);
    gv_3a /= (short)2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (short)0x12);
    gv_3a /= const_3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (short)6);
    gv_3a /= gv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (short)3);
    gv_3a /= gv_int1;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (short)3);

    // Repeat Multiply and Divide Assign
    gv_3a *= rgv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (short)6);
    gv_3a /= rgv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (short)3);

    // Negate
    const short ng = -gv_3a;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 0);
    TS_ASSERT_EQUALS(ng, (short)-3);
    TS_ASSERT_EQUALS(gv_3a(), (short)3);

    // Illegal compare
    test_typ mytest;
    TS_ASSERT(gv_1 != mytest);

    // Next
    generic_var<short> arry[2];
    TS_ASSERT_EQUALS(&arry[1], arry[0].getNext());
}

void GenVarTestSuite::testInt(void)
{
    const int const_1 = 1;
    const int const_2 = 2;
    const int const_3 = 3;
    generic_var<int> gv_1(const_1);
    generic_var<int> gv_2(const_2);
    generic_var<int> gv_3a(const_3);
    generic_var<int> gv_3b(gv_3a);
    generic_var<int> gv_5((int)5);
    generic_var<int> gv_invalid;
    generic_var<long long>  gv_int1(1);
    r_generic_var<int> rgv_1(const_1);
    r_generic_var<int> rgv_2(const_2);

    // Equal test (allows other value_var types)
    TS_ASSERT(gv_1 == const_1);
    TS_ASSERT(gv_1 == (int)1);
    TS_ASSERT(const_1 == gv_1);
    TS_ASSERT(gv_1 == gv_int1);
    TS_ASSERT(gv_3a == gv_3b);
    TS_ASSERT(gv_3a == 3);
    TS_ASSERT(gv_invalid == -1);
    TS_ASSERT(gv_1 == rgv_1);
    TS_ASSERT(rgv_1 == gv_1);

    TS_ASSERT_NOT(gv_1 == const_2);
    TS_ASSERT_NOT(gv_1 == (int)2);
    TS_ASSERT_NOT(gv_1 == gv_2);
    TS_ASSERT_NOT(const_2 == gv_1);
    TS_ASSERT_NOT(gv_2 == gv_int1);
    TS_ASSERT_NOT(gv_2 == 1);
    TS_ASSERT_NOT(gv_2 == rgv_1);
    TS_ASSERT_NOT(rgv_1 == gv_2);

    // Not equal test (allows other value_var types)
    TS_ASSERT_NOT(gv_1 != const_1);
    TS_ASSERT_NOT(gv_1 != (int)1);
    TS_ASSERT_NOT(const_1 != gv_1);
    TS_ASSERT_NOT(gv_1 != gv_int1);
    TS_ASSERT_NOT(gv_3a != gv_3b);
    TS_ASSERT_NOT(gv_3a != 3);
    TS_ASSERT_NOT(gv_invalid != -1);
    TS_ASSERT_NOT(gv_1 != rgv_1);
    TS_ASSERT_NOT(rgv_1 != gv_1);

    TS_ASSERT(gv_1 != const_2);
    TS_ASSERT(gv_1 != (int)2);
    TS_ASSERT(gv_1 != gv_2);
    TS_ASSERT(const_2 != gv_1);
    TS_ASSERT(gv_2 != gv_int1);
    TS_ASSERT(gv_2 != 1);
    TS_ASSERT(gv_2 != rgv_1);
    TS_ASSERT(rgv_1 != gv_2);

    // Less than test (same type only)
    TS_ASSERT_NOT(gv_1 < const_1);
    TS_ASSERT_NOT(gv_1 < (int)1);
    TS_ASSERT_NOT(const_1 < gv_1);
    TS_ASSERT_NOT(gv_3a < gv_3b);
    TS_ASSERT_NOT(gv_1 < rgv_1);
    TS_ASSERT_NOT(rgv_1 < gv_1);

    TS_ASSERT(gv_1 < const_2);
    TS_ASSERT(gv_1 < gv_2);
    TS_ASSERT(const_1 < gv_2);
    TS_ASSERT(rgv_1 < gv_2);
    TS_ASSERT(gv_1 < rgv_2);

    TS_ASSERT_NOT(gv_2 < const_1);
    TS_ASSERT_NOT(gv_2 < gv_1);
    TS_ASSERT_NOT(const_2 < gv_1);
    TS_ASSERT_NOT(rgv_2 < gv_1);
    TS_ASSERT_NOT(gv_2 < rgv_1);

    // Less than or equal test (same type only)
    TS_ASSERT(gv_1 <= const_1);
    TS_ASSERT(gv_1 <= (int)1);
    TS_ASSERT(const_1 <= gv_1);
    TS_ASSERT(gv_3a <= gv_3b);
    TS_ASSERT(gv_1 <= rgv_1);
    TS_ASSERT(rgv_1 <= gv_1);

    TS_ASSERT(gv_1 <= const_2);
    TS_ASSERT(gv_1 <= gv_2);
    TS_ASSERT(const_1 <= gv_2);
    TS_ASSERT(rgv_1 <= gv_2);
    TS_ASSERT(gv_1 <= rgv_2);

    TS_ASSERT_NOT(gv_2 <= const_1);
    TS_ASSERT_NOT(gv_2 <= gv_1);
    TS_ASSERT_NOT(const_2 <= gv_1);
    TS_ASSERT_NOT(rgv_2 <= gv_1);
    TS_ASSERT_NOT(gv_2 <= rgv_1);

    // More than test (same type only)
    TS_ASSERT_NOT(gv_1 > const_1);
    TS_ASSERT_NOT(gv_1 > (int)1);
    TS_ASSERT_NOT(const_1 > gv_1);
    TS_ASSERT_NOT(gv_3a > gv_3b);
    TS_ASSERT_NOT(gv_1 > rgv_1);
    TS_ASSERT_NOT(rgv_1 > gv_1);

    TS_ASSERT_NOT(gv_1 > const_2);
    TS_ASSERT_NOT(gv_1 > gv_2);
    TS_ASSERT_NOT(const_1 > gv_2);
    TS_ASSERT_NOT(rgv_1 > gv_2);
    TS_ASSERT_NOT(gv_1 > rgv_2);

    TS_ASSERT(gv_2 > const_1);
    TS_ASSERT(gv_2 > gv_1);
    TS_ASSERT(const_2 > gv_1);
    TS_ASSERT(rgv_2 > gv_1);
    TS_ASSERT(gv_2 > rgv_1);

    // More than and equal test (same type only)
    TS_ASSERT(gv_1 >= const_1);
    TS_ASSERT(gv_1 >= (int)1);
    TS_ASSERT(const_1 >= gv_1);
    TS_ASSERT(gv_3a >= gv_3b);
    TS_ASSERT(gv_1 >= rgv_1);
    TS_ASSERT(rgv_1 >= gv_1);

    TS_ASSERT_NOT(gv_1 >= const_2);
    TS_ASSERT_NOT(gv_1 >= gv_2);
    TS_ASSERT_NOT(const_1 >= gv_2);
    TS_ASSERT_NOT(rgv_1 >= gv_2);
    TS_ASSERT_NOT(gv_1 >= rgv_2);

    TS_ASSERT(gv_2 >= const_1);
    TS_ASSERT(gv_2 >= gv_1);
    TS_ASSERT(const_2 >= gv_1);
    TS_ASSERT(rgv_2 >= gv_1);
    TS_ASSERT(gv_2 >= rgv_1);

    // Plus (same type only)
    TS_ASSERT(gv_1 + const_2 == const_3);
    TS_ASSERT(const_1 + gv_2 == const_3);
    TS_ASSERT(gv_1 + const_2 == gv_3a);
    TS_ASSERT(const_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + gv_2 == gv_3a);
    TS_ASSERT(rgv_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + rgv_2 == gv_3a);

    // Minus (same type only)
    TS_ASSERT(gv_2 - const_1 == const_1);
    TS_ASSERT(const_2 - gv_1 == const_1);
    TS_ASSERT(gv_2 - const_1 == gv_1);
    TS_ASSERT(const_2 - gv_1 == gv_1);
    TS_ASSERT(gv_3a - gv_1 == gv_2);
    TS_ASSERT(gv_2 - rgv_1 == gv_1);
    TS_ASSERT(rgv_2 - gv_1 == rgv_1);

    // Multiply (same type only)
    TS_ASSERT(gv_2 * const_1 == const_2);
    TS_ASSERT(const_2 * gv_1 == const_2);
    TS_ASSERT(gv_2 * const_1 == gv_2);
    TS_ASSERT(const_2 * gv_1 == gv_2);
    TS_ASSERT(gv_2 * gv_1 == gv_2);
    TS_ASSERT(rgv_1 * gv_2 == gv_2);
    TS_ASSERT(gv_1 * rgv_2 == rgv_2);

    // Divide (same type only)
    TS_ASSERT(gv_3a / const_2 == const_1);
    TS_ASSERT(const_3 / gv_2 == const_1);
    TS_ASSERT(gv_3a / const_2 == gv_1);
    TS_ASSERT(const_3 / gv_2 == gv_1);
    TS_ASSERT(gv_3a / gv_2 == gv_1);
    TS_ASSERT(gv_2 / rgv_2 == gv_1);
    TS_ASSERT(rgv_2 / gv_2 == rgv_1);

    // Assign (allows other value_var types)
    dirty_checker checker1(&gv_1);
    dirty_checker checker2(&gv_3a);
    dirty_checker checker3(&rgv_1);
    gv_1 = const_1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    gv_3a = gv_3b;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 2);
    gv_3a = (int)3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 2);
    gv_1 = gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    gv_1 = rgv_1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    rgv_1 = gv_1;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 2);
    gv_1 = const_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = (int)4;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = gv_3a;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 0);
    gv_1 = 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    rgv_1 = gv_2;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 1);
    gv_1 = gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    rgv_1 = gv_int1;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 1);

    // Plus-Assign (allows other value_var types)
    gv_1 += gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (int)2);
    gv_1 += gv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (int)4);
    gv_1 += const_3;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (int)7);
    gv_1 += (int)'\4';
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (int)0xb);
    gv_1 += 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (int)0x10);
    gv_1 += rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (int)0x12);

    // Minus-Assign (allows other value_var types)
    gv_1 -= rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (int)0x10);
    gv_1 -= 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (int)0xb);
    gv_1 -= (int)4;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (int)7);
    gv_1 -= const_3;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (int)4);
    gv_1 -= gv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (int)2);
    gv_1 -= gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (int)1);

    // Multiply-Assign (allows other value_var types)
    gv_3a *= gv_int1;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (int)3);
    gv_3a *= gv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (int)6);
    gv_3a *= const_3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (int)0x12);
    gv_3a *= (int)'\2';
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (int)0x24);
    gv_3a *= 2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (int)0x48);

    // Divide-Assign (allows other value_var types)
    gv_3a /= 2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (int)0x24);
    gv_3a /= (int)2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (int)0x12);
    gv_3a /= const_3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (int)6);
    gv_3a /= gv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (int)3);
    gv_3a /= gv_int1;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (int)3);

    // Repeat Multiply and Divide Assign
    gv_3a *= rgv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (int)6);
    gv_3a /= rgv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (int)3);

    // Negate
    const int ng = -gv_3a;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 0);
    TS_ASSERT_EQUALS(ng, -3);
    TS_ASSERT_EQUALS(gv_3a(), 3);

    // Illegal compare
    test_typ mytest;
    TS_ASSERT(gv_1 != mytest);

    // Next
    generic_var<int> arry[2];
    TS_ASSERT_EQUALS(&arry[1], arry[0].getNext());
}

void GenVarTestSuite::testLong(void)
{
    const long const_1 = 1;
    const long const_2 = 2;
    const long const_3 = 3;
    generic_var<long> gv_1(const_1);
    generic_var<long> gv_2(const_2);
    generic_var<long> gv_3a(const_3);
    generic_var<long> gv_3b(gv_3a);
    generic_var<long> gv_5((long)5);
    generic_var<long> gv_invalid;
    generic_var<long long>  gv_long1(1);
    r_generic_var<long> rgv_1(const_1);
    r_generic_var<long> rgv_2(const_2);

    // Equal test (allows other value_var types)
    TS_ASSERT(gv_1 == const_1);
    TS_ASSERT(gv_1 == (long)1);
    TS_ASSERT(const_1 == gv_1);
    TS_ASSERT(gv_1 == gv_long1);
    TS_ASSERT(gv_3a == gv_3b);
    TS_ASSERT(gv_3a == 3);
    TS_ASSERT(gv_invalid == -1);
    TS_ASSERT(gv_1 == rgv_1);
    TS_ASSERT(rgv_1 == gv_1);

    TS_ASSERT_NOT(gv_1 == const_2);
    TS_ASSERT_NOT(gv_1 == (long)2);
    TS_ASSERT_NOT(gv_1 == gv_2);
    TS_ASSERT_NOT(const_2 == gv_1);
    TS_ASSERT_NOT(gv_2 == gv_long1);
    TS_ASSERT_NOT(gv_2 == 1);
    TS_ASSERT_NOT(gv_2 == rgv_1);
    TS_ASSERT_NOT(rgv_1 == gv_2);

    // Not equal test (allows other value_var types)
    TS_ASSERT_NOT(gv_1 != const_1);
    TS_ASSERT_NOT(gv_1 != (long)1);
    TS_ASSERT_NOT(const_1 != gv_1);
    TS_ASSERT_NOT(gv_1 != gv_long1);
    TS_ASSERT_NOT(gv_3a != gv_3b);
    TS_ASSERT_NOT(gv_3a != 3);
    TS_ASSERT_NOT(gv_invalid != -1);
    TS_ASSERT_NOT(gv_1 != rgv_1);
    TS_ASSERT_NOT(rgv_1 != gv_1);

    TS_ASSERT(gv_1 != const_2);
    TS_ASSERT(gv_1 != (long)2);
    TS_ASSERT(gv_1 != gv_2);
    TS_ASSERT(const_2 != gv_1);
    TS_ASSERT(gv_2 != gv_long1);
    TS_ASSERT(gv_2 != 1);
    TS_ASSERT(gv_2 != rgv_1);
    TS_ASSERT(rgv_1 != gv_2);

    // Less than test (same type only)
    TS_ASSERT_NOT(gv_1 < const_1);
    TS_ASSERT_NOT(gv_1 < (long)1);
    TS_ASSERT_NOT(const_1 < gv_1);
    TS_ASSERT_NOT(gv_3a < gv_3b);
    TS_ASSERT_NOT(gv_1 < rgv_1);
    TS_ASSERT_NOT(rgv_1 < gv_1);

    TS_ASSERT(gv_1 < const_2);
    TS_ASSERT(gv_1 < gv_2);
    TS_ASSERT(const_1 < gv_2);
    TS_ASSERT(rgv_1 < gv_2);
    TS_ASSERT(gv_1 < rgv_2);

    TS_ASSERT_NOT(gv_2 < const_1);
    TS_ASSERT_NOT(gv_2 < gv_1);
    TS_ASSERT_NOT(const_2 < gv_1);
    TS_ASSERT_NOT(rgv_2 < gv_1);
    TS_ASSERT_NOT(gv_2 < rgv_1);

    // Less than or equal test (same type only)
    TS_ASSERT(gv_1 <= const_1);
    TS_ASSERT(gv_1 <= (long)1);
    TS_ASSERT(const_1 <= gv_1);
    TS_ASSERT(gv_3a <= gv_3b);
    TS_ASSERT(gv_1 <= rgv_1);
    TS_ASSERT(rgv_1 <= gv_1);

    TS_ASSERT(gv_1 <= const_2);
    TS_ASSERT(gv_1 <= gv_2);
    TS_ASSERT(const_1 <= gv_2);
    TS_ASSERT(rgv_1 <= gv_2);
    TS_ASSERT(gv_1 <= rgv_2);

    TS_ASSERT_NOT(gv_2 <= const_1);
    TS_ASSERT_NOT(gv_2 <= gv_1);
    TS_ASSERT_NOT(const_2 <= gv_1);
    TS_ASSERT_NOT(rgv_2 <= gv_1);
    TS_ASSERT_NOT(gv_2 <= rgv_1);

    // More than test (same type only)
    TS_ASSERT_NOT(gv_1 > const_1);
    TS_ASSERT_NOT(gv_1 > (long)1);
    TS_ASSERT_NOT(const_1 > gv_1);
    TS_ASSERT_NOT(gv_3a > gv_3b);
    TS_ASSERT_NOT(gv_1 > rgv_1);
    TS_ASSERT_NOT(rgv_1 > gv_1);

    TS_ASSERT_NOT(gv_1 > const_2);
    TS_ASSERT_NOT(gv_1 > gv_2);
    TS_ASSERT_NOT(const_1 > gv_2);
    TS_ASSERT_NOT(rgv_1 > gv_2);
    TS_ASSERT_NOT(gv_1 > rgv_2);

    TS_ASSERT(gv_2 > const_1);
    TS_ASSERT(gv_2 > gv_1);
    TS_ASSERT(const_2 > gv_1);
    TS_ASSERT(rgv_2 > gv_1);
    TS_ASSERT(gv_2 > rgv_1);

    // More than and equal test (same type only)
    TS_ASSERT(gv_1 >= const_1);
    TS_ASSERT(gv_1 >= (long)1);
    TS_ASSERT(const_1 >= gv_1);
    TS_ASSERT(gv_3a >= gv_3b);
    TS_ASSERT(gv_1 >= rgv_1);
    TS_ASSERT(rgv_1 >= gv_1);

    TS_ASSERT_NOT(gv_1 >= const_2);
    TS_ASSERT_NOT(gv_1 >= gv_2);
    TS_ASSERT_NOT(const_1 >= gv_2);
    TS_ASSERT_NOT(rgv_1 >= gv_2);
    TS_ASSERT_NOT(gv_1 >= rgv_2);

    TS_ASSERT(gv_2 >= const_1);
    TS_ASSERT(gv_2 >= gv_1);
    TS_ASSERT(const_2 >= gv_1);
    TS_ASSERT(rgv_2 >= gv_1);
    TS_ASSERT(gv_2 >= rgv_1);

    // Plus (same type only)
    TS_ASSERT(gv_1 + const_2 == const_3);
    TS_ASSERT(const_1 + gv_2 == const_3);
    TS_ASSERT(gv_1 + const_2 == gv_3a);
    TS_ASSERT(const_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + gv_2 == gv_3a);
    TS_ASSERT(rgv_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + rgv_2 == gv_3a);

    // Minus (same type only)
    TS_ASSERT(gv_2 - const_1 == const_1);
    TS_ASSERT(const_2 - gv_1 == const_1);
    TS_ASSERT(gv_2 - const_1 == gv_1);
    TS_ASSERT(const_2 - gv_1 == gv_1);
    TS_ASSERT(gv_3a - gv_1 == gv_2);
    TS_ASSERT(gv_2 - rgv_1 == gv_1);
    TS_ASSERT(rgv_2 - gv_1 == rgv_1);

    // Multiply (same type only)
    TS_ASSERT(gv_2 * const_1 == const_2);
    TS_ASSERT(const_2 * gv_1 == const_2);
    TS_ASSERT(gv_2 * const_1 == gv_2);
    TS_ASSERT(const_2 * gv_1 == gv_2);
    TS_ASSERT(gv_2 * gv_1 == gv_2);
    TS_ASSERT(rgv_1 * gv_2 == gv_2);
    TS_ASSERT(gv_1 * rgv_2 == rgv_2);

    // Divide (same type only)
    TS_ASSERT(gv_3a / const_2 == const_1);
    TS_ASSERT(const_3 / gv_2 == const_1);
    TS_ASSERT(gv_3a / const_2 == gv_1);
    TS_ASSERT(const_3 / gv_2 == gv_1);
    TS_ASSERT(gv_3a / gv_2 == gv_1);
    TS_ASSERT(gv_2 / rgv_2 == gv_1);
    TS_ASSERT(rgv_2 / gv_2 == rgv_1);

    // Assign (allows other value_var types)
    dirty_checker checker1(&gv_1);
    dirty_checker checker2(&gv_3a);
    dirty_checker checker3(&rgv_1);
    gv_1 = const_1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    gv_3a = gv_3b;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 2);
    gv_3a = (long)3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 2);
    gv_1 = gv_long1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    gv_1 = rgv_1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    rgv_1 = gv_1;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 2);
    gv_1 = const_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = (long)4;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = gv_3a;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 0);
    gv_1 = 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    rgv_1 = gv_2;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 1);
    gv_1 = gv_long1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    rgv_1 = gv_long1;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 1);

    // Plus-Assign (allows other value_var types)
    gv_1 += gv_long1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long)2);
    gv_1 += gv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long)4);
    gv_1 += const_3;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long)7);
    gv_1 += (long)'\4';
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long)0xb);
    gv_1 += 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long)0x10);
    gv_1 += rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long)0x12);

    // Minus-Assign (allows other value_var types)
    gv_1 -= rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long)0x10);
    gv_1 -= 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long)0xb);
    gv_1 -= (long)4;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long)7);
    gv_1 -= const_3;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long)4);
    gv_1 -= gv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long)2);
    gv_1 -= gv_long1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long)1);

    // Multiply-Assign (allows other value_var types)
    gv_3a *= gv_long1;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long)3);
    gv_3a *= gv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long)6);
    gv_3a *= const_3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long)0x12);
    gv_3a *= (long)'\2';
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long)0x24);
    gv_3a *= 2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long)0x48);

    // Divide-Assign (allows other value_var types)
    gv_3a /= 2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long)0x24);
    gv_3a /= (long)2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long)0x12);
    gv_3a /= const_3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long)6);
    gv_3a /= gv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long)3);
    gv_3a /= gv_long1;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long)3);

    // Repeat Multiply and Divide Assign
    gv_3a *= rgv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long)6);
    gv_3a /= rgv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long)3);

    // Negate
    const long ng = -gv_3a;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 0);
    TS_ASSERT_EQUALS(ng, -3L);
    TS_ASSERT_EQUALS(gv_3a(), 3L);

    // Illegal compare
    test_typ mytest;
    TS_ASSERT(gv_1 != mytest);

    // Next
    generic_var<long> arry[2];
    TS_ASSERT_EQUALS(&arry[1], arry[0].getNext());
}

void GenVarTestSuite::testLongLong(void)
{
    const long long const_1 = 1;
    const long long const_2 = 2;
    const long long const_3 = 3;
    generic_var<long long> gv_1(const_1);
    generic_var<long long> gv_2(const_2);
    generic_var<long long> gv_3a(const_3);
    generic_var<long long> gv_3b(gv_3a);
    generic_var<long long> gv_5((long long)5);
    generic_var<long long> gv_invalid;
    generic_var<long>  gv_long1(1);
    r_generic_var<long long> rgv_1(const_1);
    r_generic_var<long long> rgv_2(const_2);

    // Equal test (allows other value_var types)
    TS_ASSERT(gv_1 == const_1);
    TS_ASSERT(gv_1 == (long long)1);
    TS_ASSERT(const_1 == gv_1);
    TS_ASSERT(gv_1 == gv_long1);
    TS_ASSERT(gv_3a == gv_3b);
    TS_ASSERT(gv_3a == 3);
    TS_ASSERT(gv_invalid == -1);
    TS_ASSERT(gv_1 == rgv_1);
    TS_ASSERT(rgv_1 == gv_1);

    TS_ASSERT_NOT(gv_1 == const_2);
    TS_ASSERT_NOT(gv_1 == (long long)2);
    TS_ASSERT_NOT(gv_1 == gv_2);
    TS_ASSERT_NOT(const_2 == gv_1);
    TS_ASSERT_NOT(gv_2 == gv_long1);
    TS_ASSERT_NOT(gv_2 == 1);
    TS_ASSERT_NOT(gv_2 == rgv_1);
    TS_ASSERT_NOT(rgv_1 == gv_2);

    // Not equal test (allows other value_var types)
    TS_ASSERT_NOT(gv_1 != const_1);
    TS_ASSERT_NOT(gv_1 != (long long)1);
    TS_ASSERT_NOT(const_1 != gv_1);
    TS_ASSERT_NOT(gv_1 != gv_long1);
    TS_ASSERT_NOT(gv_3a != gv_3b);
    TS_ASSERT_NOT(gv_3a != 3);
    TS_ASSERT_NOT(gv_invalid != -1);
    TS_ASSERT_NOT(gv_1 != rgv_1);
    TS_ASSERT_NOT(rgv_1 != gv_1);

    TS_ASSERT(gv_1 != const_2);
    TS_ASSERT(gv_1 != (long long)2);
    TS_ASSERT(gv_1 != gv_2);
    TS_ASSERT(const_2 != gv_1);
    TS_ASSERT(gv_2 != gv_long1);
    TS_ASSERT(gv_2 != 1);
    TS_ASSERT(gv_2 != rgv_1);
    TS_ASSERT(rgv_1 != gv_2);

    // Less than test (same type only)
    TS_ASSERT_NOT(gv_1 < const_1);
    TS_ASSERT_NOT(gv_1 < (long long)1);
    TS_ASSERT_NOT(const_1 < gv_1);
    TS_ASSERT_NOT(gv_3a < gv_3b);
    TS_ASSERT_NOT(gv_1 < rgv_1);
    TS_ASSERT_NOT(rgv_1 < gv_1);

    TS_ASSERT(gv_1 < const_2);
    TS_ASSERT(gv_1 < gv_2);
    TS_ASSERT(const_1 < gv_2);
    TS_ASSERT(rgv_1 < gv_2);
    TS_ASSERT(gv_1 < rgv_2);

    TS_ASSERT_NOT(gv_2 < const_1);
    TS_ASSERT_NOT(gv_2 < gv_1);
    TS_ASSERT_NOT(const_2 < gv_1);
    TS_ASSERT_NOT(rgv_2 < gv_1);
    TS_ASSERT_NOT(gv_2 < rgv_1);

    // Less than or equal test (same type only)
    TS_ASSERT(gv_1 <= const_1);
    TS_ASSERT(gv_1 <= (long long)1);
    TS_ASSERT(const_1 <= gv_1);
    TS_ASSERT(gv_3a <= gv_3b);
    TS_ASSERT(gv_1 <= rgv_1);
    TS_ASSERT(rgv_1 <= gv_1);

    TS_ASSERT(gv_1 <= const_2);
    TS_ASSERT(gv_1 <= gv_2);
    TS_ASSERT(const_1 <= gv_2);
    TS_ASSERT(rgv_1 <= gv_2);
    TS_ASSERT(gv_1 <= rgv_2);

    TS_ASSERT_NOT(gv_2 <= const_1);
    TS_ASSERT_NOT(gv_2 <= gv_1);
    TS_ASSERT_NOT(const_2 <= gv_1);
    TS_ASSERT_NOT(rgv_2 <= gv_1);
    TS_ASSERT_NOT(gv_2 <= rgv_1);

    // More than test (same type only)
    TS_ASSERT_NOT(gv_1 > const_1);
    TS_ASSERT_NOT(gv_1 > (long long)1);
    TS_ASSERT_NOT(const_1 > gv_1);
    TS_ASSERT_NOT(gv_3a > gv_3b);
    TS_ASSERT_NOT(gv_1 > rgv_1);
    TS_ASSERT_NOT(rgv_1 > gv_1);

    TS_ASSERT_NOT(gv_1 > const_2);
    TS_ASSERT_NOT(gv_1 > gv_2);
    TS_ASSERT_NOT(const_1 > gv_2);
    TS_ASSERT_NOT(rgv_1 > gv_2);
    TS_ASSERT_NOT(gv_1 > rgv_2);

    TS_ASSERT(gv_2 > const_1);
    TS_ASSERT(gv_2 > gv_1);
    TS_ASSERT(const_2 > gv_1);
    TS_ASSERT(rgv_2 > gv_1);
    TS_ASSERT(gv_2 > rgv_1);

    // More than and equal test (same type only)
    TS_ASSERT(gv_1 >= const_1);
    TS_ASSERT(gv_1 >= (long long)1);
    TS_ASSERT(const_1 >= gv_1);
    TS_ASSERT(gv_3a >= gv_3b);
    TS_ASSERT(gv_1 >= rgv_1);
    TS_ASSERT(rgv_1 >= gv_1);

    TS_ASSERT_NOT(gv_1 >= const_2);
    TS_ASSERT_NOT(gv_1 >= gv_2);
    TS_ASSERT_NOT(const_1 >= gv_2);
    TS_ASSERT_NOT(rgv_1 >= gv_2);
    TS_ASSERT_NOT(gv_1 >= rgv_2);

    TS_ASSERT(gv_2 >= const_1);
    TS_ASSERT(gv_2 >= gv_1);
    TS_ASSERT(const_2 >= gv_1);
    TS_ASSERT(rgv_2 >= gv_1);
    TS_ASSERT(gv_2 >= rgv_1);

    // Plus (same type only)
    TS_ASSERT(gv_1 + const_2 == const_3);
    TS_ASSERT(const_1 + gv_2 == const_3);
    TS_ASSERT(gv_1 + const_2 == gv_3a);
    TS_ASSERT(const_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + gv_2 == gv_3a);
    TS_ASSERT(rgv_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + rgv_2 == gv_3a);

    // Minus (same type only)
    TS_ASSERT(gv_2 - const_1 == const_1);
    TS_ASSERT(const_2 - gv_1 == const_1);
    TS_ASSERT(gv_2 - const_1 == gv_1);
    TS_ASSERT(const_2 - gv_1 == gv_1);
    TS_ASSERT(gv_3a - gv_1 == gv_2);
    TS_ASSERT(gv_2 - rgv_1 == gv_1);
    TS_ASSERT(rgv_2 - gv_1 == rgv_1);

    // Multiply (same type only)
    TS_ASSERT(gv_2 * const_1 == const_2);
    TS_ASSERT(const_2 * gv_1 == const_2);
    TS_ASSERT(gv_2 * const_1 == gv_2);
    TS_ASSERT(const_2 * gv_1 == gv_2);
    TS_ASSERT(gv_2 * gv_1 == gv_2);
    TS_ASSERT(rgv_1 * gv_2 == gv_2);
    TS_ASSERT(gv_1 * rgv_2 == rgv_2);

    // Divide (same type only)
    TS_ASSERT(gv_3a / const_2 == const_1);
    TS_ASSERT(const_3 / gv_2 == const_1);
    TS_ASSERT(gv_3a / const_2 == gv_1);
    TS_ASSERT(const_3 / gv_2 == gv_1);
    TS_ASSERT(gv_3a / gv_2 == gv_1);
    TS_ASSERT(gv_2 / rgv_2 == gv_1);
    TS_ASSERT(rgv_2 / gv_2 == rgv_1);

    // Assign (allows other value_var types)
    dirty_checker checker1(&gv_1);
    dirty_checker checker2(&gv_3a);
    dirty_checker checker3(&rgv_1);
    gv_1 = const_1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    gv_3a = gv_3b;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 2);
    gv_3a = (long long)3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 2);
    gv_1 = gv_long1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    gv_1 = rgv_1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    rgv_1 = gv_1;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 2);
    gv_1 = const_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = (long long)4;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = gv_3a;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 0);
    gv_1 = 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    rgv_1 = gv_2;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 1);
    gv_1 = gv_long1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    rgv_1 = gv_long1;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 1);

    // Plus-Assign (allows other value_var types)
    gv_1 += gv_long1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long long)2);
    gv_1 += gv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long long)4);
    gv_1 += const_3;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long long)7);
    gv_1 += (long long)'\4';
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long long)0xb);
    gv_1 += 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long long)0x10);
    gv_1 += rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long long)0x12);

    // Minus-Assign (allows other value_var types)
    gv_1 -= rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long long)0x10);
    gv_1 -= 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long long)0xb);
    gv_1 -= (long long)4;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long long)7);
    gv_1 -= const_3;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long long)4);
    gv_1 -= gv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long long)2);
    gv_1 -= gv_long1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (long long)1);

    // Multiply-Assign (allows other value_var types)
    gv_3a *= gv_long1;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long long)3);
    gv_3a *= gv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long long)6);
    gv_3a *= const_3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long long)0x12);
    gv_3a *= (long long)'\2';
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long long)0x24);
    gv_3a *= 2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long long)0x48);

    // Divide-Assign (allows other value_var types)
    gv_3a /= 2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long long)0x24);
    gv_3a /= (long long)2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long long)0x12);
    gv_3a /= const_3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long long)6);
    gv_3a /= gv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long long)3);
    gv_3a /= gv_long1;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long long)3);

    // Repeat Multiply and Divide Assign
    gv_3a *= rgv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long long)6);
    gv_3a /= rgv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (long long)3);

    // Negate
    const long long ng = -gv_3a;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 0);
    TS_ASSERT_EQUALS(ng, -3LL);
    TS_ASSERT_EQUALS(gv_3a(), 3LL);

    // Illegal compare
    test_typ mytest;
    TS_ASSERT(gv_1 != mytest);

    // Next
    generic_var<long long> arry[2];
    TS_ASSERT_EQUALS(&arry[1], arry[0].getNext());
}

void GenVarTestSuite::testUnsignedChar(void)
{
    const unsigned char const_1 = 1;
    const unsigned char const_2 = 2;
    const unsigned char const_3 = 3;
    generic_var<unsigned char> gv_1(const_1);
    generic_var<unsigned char> gv_2(const_2);
    generic_var<unsigned char> gv_3a(const_3);
    generic_var<unsigned char> gv_3b(gv_3a);
    generic_var<unsigned char> gv_5((unsigned char)'\5');
    generic_var<unsigned char> gv_invalid;
    generic_var<unsigned int>  gv_int1(1);
    r_generic_var<unsigned char> rgv_1(const_1);
    r_generic_var<unsigned char> rgv_2(const_2);

    // Equal test (allows other value_var types)
    TS_ASSERT(gv_1 == const_1);
    TS_ASSERT(gv_1 == (unsigned char)'\1');
    TS_ASSERT(const_1 == gv_1);
    TS_ASSERT(gv_1 == gv_int1);
    TS_ASSERT(gv_3a == gv_3b);
    TS_ASSERT(gv_3a == 3);
    TS_ASSERT(gv_invalid == 255);
    TS_ASSERT(gv_1 == rgv_1);
    TS_ASSERT(rgv_1 == gv_1);

    TS_ASSERT_NOT(gv_1 == const_2);
    TS_ASSERT_NOT(gv_1 == (unsigned char)'\2');
    TS_ASSERT_NOT(gv_1 == gv_2);
    TS_ASSERT_NOT(const_2 == gv_1);
    TS_ASSERT_NOT(gv_2 == gv_int1);
    TS_ASSERT_NOT(gv_2 == 1);
    TS_ASSERT_NOT(gv_2 == rgv_1);
    TS_ASSERT_NOT(rgv_1 == gv_2);

    // Not equal test (allows other value_var types)
    TS_ASSERT_NOT(gv_1 != const_1);
    TS_ASSERT_NOT(gv_1 != (unsigned char)'\1');
    TS_ASSERT_NOT(const_1 != gv_1);
    TS_ASSERT_NOT(gv_1 != gv_int1);
    TS_ASSERT_NOT(gv_3a != gv_3b);
    TS_ASSERT_NOT(gv_3a != 3);
    TS_ASSERT_NOT(gv_invalid != 0xff);
    TS_ASSERT_NOT(gv_1 != rgv_1);
    TS_ASSERT_NOT(rgv_1 != gv_1);

    TS_ASSERT(gv_1 != const_2);
    TS_ASSERT(gv_1 != (unsigned char)'\2');
    TS_ASSERT(gv_1 != gv_2);
    TS_ASSERT(const_2 != gv_1);
    TS_ASSERT(gv_2 != gv_int1);
    TS_ASSERT(gv_2 != 1);
    TS_ASSERT(gv_2 != rgv_1);
    TS_ASSERT(rgv_1 != gv_2);

    // Less than test (same type only)
    TS_ASSERT_NOT(gv_1 < const_1);
    TS_ASSERT_NOT(gv_1 < (unsigned char)'\1');
    TS_ASSERT_NOT(const_1 < gv_1);
    TS_ASSERT_NOT(gv_3a < gv_3b);
    TS_ASSERT_NOT(gv_1 < rgv_1);
    TS_ASSERT_NOT(rgv_1 < gv_1);

    TS_ASSERT(gv_1 < const_2);
    TS_ASSERT(gv_1 < gv_2);
    TS_ASSERT(const_1 < gv_2);
    TS_ASSERT(rgv_1 < gv_2);
    TS_ASSERT(gv_1 < rgv_2);

    TS_ASSERT_NOT(gv_2 < const_1);
    TS_ASSERT_NOT(gv_2 < gv_1);
    TS_ASSERT_NOT(const_2 < gv_1);
    TS_ASSERT_NOT(rgv_2 < gv_1);
    TS_ASSERT_NOT(gv_2 < rgv_1);

    // Less than or equal test (same type only)
    TS_ASSERT(gv_1 <= const_1);
    TS_ASSERT(gv_1 <= (unsigned char)'\1');
    TS_ASSERT(const_1 <= gv_1);
    TS_ASSERT(gv_3a <= gv_3b);
    TS_ASSERT(gv_1 <= rgv_1);
    TS_ASSERT(rgv_1 <= gv_1);

    TS_ASSERT(gv_1 <= const_2);
    TS_ASSERT(gv_1 <= gv_2);
    TS_ASSERT(const_1 <= gv_2);
    TS_ASSERT(rgv_1 <= gv_2);
    TS_ASSERT(gv_1 <= rgv_2);

    TS_ASSERT_NOT(gv_2 <= const_1);
    TS_ASSERT_NOT(gv_2 <= gv_1);
    TS_ASSERT_NOT(const_2 <= gv_1);
    TS_ASSERT_NOT(rgv_2 <= gv_1);
    TS_ASSERT_NOT(gv_2 <= rgv_1);

    // More than test (same type only)
    TS_ASSERT_NOT(gv_1 > const_1);
    TS_ASSERT_NOT(gv_1 > (unsigned char)'\1');
    TS_ASSERT_NOT(const_1 > gv_1);
    TS_ASSERT_NOT(gv_3a > gv_3b);
    TS_ASSERT_NOT(gv_1 > rgv_1);
    TS_ASSERT_NOT(rgv_1 > gv_1);

    TS_ASSERT_NOT(gv_1 > const_2);
    TS_ASSERT_NOT(gv_1 > gv_2);
    TS_ASSERT_NOT(const_1 > gv_2);
    TS_ASSERT_NOT(rgv_1 > gv_2);
    TS_ASSERT_NOT(gv_1 > rgv_2);

    TS_ASSERT(gv_2 > const_1);
    TS_ASSERT(gv_2 > gv_1);
    TS_ASSERT(const_2 > gv_1);
    TS_ASSERT(rgv_2 > gv_1);
    TS_ASSERT(gv_2 > rgv_1);

    // More than and equal test (same type only)
    TS_ASSERT(gv_1 >= const_1);
    TS_ASSERT(gv_1 >= (unsigned char)'\1');
    TS_ASSERT(const_1 >= gv_1);
    TS_ASSERT(gv_3a >= gv_3b);
    TS_ASSERT(gv_1 >= rgv_1);
    TS_ASSERT(rgv_1 >= gv_1);

    TS_ASSERT_NOT(gv_1 >= const_2);
    TS_ASSERT_NOT(gv_1 >= gv_2);
    TS_ASSERT_NOT(const_1 >= gv_2);
    TS_ASSERT_NOT(rgv_1 >= gv_2);
    TS_ASSERT_NOT(gv_1 >= rgv_2);

    TS_ASSERT(gv_2 >= const_1);
    TS_ASSERT(gv_2 >= gv_1);
    TS_ASSERT(const_2 >= gv_1);
    TS_ASSERT(rgv_2 >= gv_1);
    TS_ASSERT(gv_2 >= rgv_1);

    // Plus (same type only)
    TS_ASSERT(gv_1 + const_2 == const_3);
    TS_ASSERT(const_1 + gv_2 == const_3);
    TS_ASSERT(gv_1 + const_2 == gv_3a);
    TS_ASSERT(const_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + gv_2 == gv_3a);
    TS_ASSERT(rgv_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + rgv_2 == gv_3a);

    // Minus (same type only)
    TS_ASSERT(gv_2 - const_1 == const_1);
    TS_ASSERT(const_2 - gv_1 == const_1);
    TS_ASSERT(gv_2 - const_1 == gv_1);
    TS_ASSERT(const_2 - gv_1 == gv_1);
    TS_ASSERT(gv_3a - gv_1 == gv_2);
    TS_ASSERT(gv_2 - rgv_1 == gv_1);
    TS_ASSERT(rgv_2 - gv_1 == rgv_1);

    // Multiply (same type only)
    TS_ASSERT(gv_2 * const_1 == const_2);
    TS_ASSERT(const_2 * gv_1 == const_2);
    TS_ASSERT(gv_2 * const_1 == gv_2);
    TS_ASSERT(const_2 * gv_1 == gv_2);
    TS_ASSERT(gv_2 * gv_1 == gv_2);
    TS_ASSERT(rgv_1 * gv_2 == gv_2);
    TS_ASSERT(gv_1 * rgv_2 == rgv_2);

    // Divide (same type only)
    TS_ASSERT(gv_3a / const_2 == const_1);
    TS_ASSERT(const_3 / gv_2 == const_1);
    TS_ASSERT(gv_3a / const_2 == gv_1);
    TS_ASSERT(const_3 / gv_2 == gv_1);
    TS_ASSERT(gv_3a / gv_2 == gv_1);
    TS_ASSERT(gv_2 / rgv_2 == gv_1);
    TS_ASSERT(rgv_2 / gv_2 == rgv_1);

    // Assign (allows other value_var types)
    dirty_checker checker1(&gv_1);
    dirty_checker checker2(&gv_3a);
    dirty_checker checker3(&rgv_1);
    gv_1 = const_1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    gv_3a = gv_3b;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 2);
    gv_3a = (unsigned char)'\3';
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 2);
    gv_1 = gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    gv_1 = rgv_1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    rgv_1 = gv_1;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 2);
    gv_1 = const_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = (unsigned char)'\4';
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = gv_3a;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 0);
    gv_1 = 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    rgv_1 = gv_2;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 1);
    gv_1 = gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    rgv_1 = gv_int1;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 1);

    // Plus-Assign (allows other value_var types)
    gv_1 += gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned char)'\2');
    gv_1 += gv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned char)'\4');
    gv_1 += const_3;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned char)'\7');
    gv_1 += (unsigned char)'\4';
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned char)'\xb');
    gv_1 += 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned char)'\x10');
    gv_1 += rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (short)0x12);

    // Minus-Assign (allows other value_var types)
    gv_1 -= rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (short)0x10);
    gv_1 -= 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned char)'\xb');
    gv_1 -= (unsigned char)'\4';
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned char)'\7');
    gv_1 -= const_3;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned char)'\4');
    gv_1 -= gv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned char)'\2');
    gv_1 -= gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned char)'\1');

    // Multiply-Assign (allows other value_var types)
    gv_3a *= gv_int1;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned char)'\3');
    gv_3a *= gv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned char)'\6');
    gv_3a *= const_3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned char)'\x12');
    gv_3a *= (unsigned char)'\2';
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned char)'\x24');
    gv_3a *= 2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned char)'\x48');

    // Divide-Assign (allows other value_var types)
    gv_3a /= 2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned char)'\x24');
    gv_3a /= (unsigned char)'\2';
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned char)'\x12');
    gv_3a /= const_3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned char)'\6');
    gv_3a /= gv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned char)'\3');
    gv_3a /= gv_int1;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned char)'\3');

    // Repeat Multiply and Divide Assign
    gv_3a *= rgv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned char)6);
    gv_3a /= rgv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned char)3);

    // Negate - no effect on unsigned
    const unsigned char ng = -gv_3a;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 0);
    TS_ASSERT_EQUALS(ng, (unsigned char)3U);
    TS_ASSERT_EQUALS(gv_3a(), (unsigned char)3U);

    // Illegal compare
    test_typ mytest;
    TS_ASSERT(gv_1 != mytest);

    // Next
    generic_var<unsigned char> arry[2];
    TS_ASSERT_EQUALS(&arry[1], arry[0].getNext());
}

void GenVarTestSuite::testUnsignedShort(void)
{
    const unsigned short const_1 = 1;
    const unsigned short const_2 = 2;
    const unsigned short const_3 = 3;
    generic_var<unsigned short> gv_1(const_1);
    generic_var<unsigned short> gv_2(const_2);
    generic_var<unsigned short> gv_3a(const_3);
    generic_var<unsigned short> gv_3b(gv_3a);
    generic_var<unsigned short> gv_5((unsigned short)5);
    generic_var<unsigned short> gv_invalid;
    generic_var<unsigned int>  gv_int1(1);
    r_generic_var<unsigned short> rgv_1(const_1);
    r_generic_var<unsigned short> rgv_2(const_2);

    // Equal test (allows other value_var types)
    TS_ASSERT(gv_1 == const_1);
    TS_ASSERT(gv_1 == (unsigned short)1);
    TS_ASSERT(const_1 == gv_1);
    TS_ASSERT(gv_1 == gv_int1);
    TS_ASSERT(gv_3a == gv_3b);
    TS_ASSERT(gv_3a == 3);
    TS_ASSERT(gv_invalid == 65535);
    TS_ASSERT(gv_1 == rgv_1);
    TS_ASSERT(rgv_1 == gv_1);

    TS_ASSERT_NOT(gv_1 == const_2);
    TS_ASSERT_NOT(gv_1 == (unsigned short)2);
    TS_ASSERT_NOT(gv_1 == gv_2);
    TS_ASSERT_NOT(const_2 == gv_1);
    TS_ASSERT_NOT(gv_2 == gv_int1);
    TS_ASSERT_NOT(gv_2 == 1);
    TS_ASSERT_NOT(gv_2 == rgv_1);
    TS_ASSERT_NOT(rgv_1 == gv_2);

    // Not equal test (allows other value_var types)
    TS_ASSERT_NOT(gv_1 != const_1);
    TS_ASSERT_NOT(gv_1 != (unsigned short)1);
    TS_ASSERT_NOT(const_1 != gv_1);
    TS_ASSERT_NOT(gv_1 != gv_int1);
    TS_ASSERT_NOT(gv_3a != gv_3b);
    TS_ASSERT_NOT(gv_3a != 3);
    TS_ASSERT_NOT(gv_invalid != 0xffff);
    TS_ASSERT_NOT(gv_1 != rgv_1);
    TS_ASSERT_NOT(rgv_1 != gv_1);

    TS_ASSERT(gv_1 != const_2);
    TS_ASSERT(gv_1 != (unsigned short)2);
    TS_ASSERT(gv_1 != gv_2);
    TS_ASSERT(const_2 != gv_1);
    TS_ASSERT(gv_2 != gv_int1);
    TS_ASSERT(gv_2 != 1);
    TS_ASSERT(gv_2 != rgv_1);
    TS_ASSERT(rgv_1 != gv_2);

    // Less than test (same type only)
    TS_ASSERT_NOT(gv_1 < const_1);
    TS_ASSERT_NOT(gv_1 < (unsigned short)1);
    TS_ASSERT_NOT(const_1 < gv_1);
    TS_ASSERT_NOT(gv_3a < gv_3b);
    TS_ASSERT_NOT(gv_1 < rgv_1);
    TS_ASSERT_NOT(rgv_1 < gv_1);

    TS_ASSERT(gv_1 < const_2);
    TS_ASSERT(gv_1 < gv_2);
    TS_ASSERT(const_1 < gv_2);
    TS_ASSERT(rgv_1 < gv_2);
    TS_ASSERT(gv_1 < rgv_2);

    TS_ASSERT_NOT(gv_2 < const_1);
    TS_ASSERT_NOT(gv_2 < gv_1);
    TS_ASSERT_NOT(const_2 < gv_1);
    TS_ASSERT_NOT(rgv_2 < gv_1);
    TS_ASSERT_NOT(gv_2 < rgv_1);

    // Less than or equal test (same type only)
    TS_ASSERT(gv_1 <= const_1);
    TS_ASSERT(gv_1 <= (unsigned short)1);
    TS_ASSERT(const_1 <= gv_1);
    TS_ASSERT(gv_3a <= gv_3b);
    TS_ASSERT(gv_1 <= rgv_1);
    TS_ASSERT(rgv_1 <= gv_1);

    TS_ASSERT(gv_1 <= const_2);
    TS_ASSERT(gv_1 <= gv_2);
    TS_ASSERT(const_1 <= gv_2);
    TS_ASSERT(rgv_1 <= gv_2);
    TS_ASSERT(gv_1 <= rgv_2);

    TS_ASSERT_NOT(gv_2 <= const_1);
    TS_ASSERT_NOT(gv_2 <= gv_1);
    TS_ASSERT_NOT(const_2 <= gv_1);
    TS_ASSERT_NOT(rgv_2 <= gv_1);
    TS_ASSERT_NOT(gv_2 <= rgv_1);

    // More than test (same type only)
    TS_ASSERT_NOT(gv_1 > const_1);
    TS_ASSERT_NOT(gv_1 > (unsigned short)1);
    TS_ASSERT_NOT(const_1 > gv_1);
    TS_ASSERT_NOT(gv_3a > gv_3b);
    TS_ASSERT_NOT(gv_1 > rgv_1);
    TS_ASSERT_NOT(rgv_1 > gv_1);

    TS_ASSERT_NOT(gv_1 > const_2);
    TS_ASSERT_NOT(gv_1 > gv_2);
    TS_ASSERT_NOT(const_1 > gv_2);
    TS_ASSERT_NOT(rgv_1 > gv_2);
    TS_ASSERT_NOT(gv_1 > rgv_2);

    TS_ASSERT(gv_2 > const_1);
    TS_ASSERT(gv_2 > gv_1);
    TS_ASSERT(const_2 > gv_1);
    TS_ASSERT(rgv_2 > gv_1);
    TS_ASSERT(gv_2 > rgv_1);

    // More than and equal test (same type only)
    TS_ASSERT(gv_1 >= const_1);
    TS_ASSERT(gv_1 >= (unsigned short)1);
    TS_ASSERT(const_1 >= gv_1);
    TS_ASSERT(gv_3a >= gv_3b);
    TS_ASSERT(gv_1 >= rgv_1);
    TS_ASSERT(rgv_1 >= gv_1);

    TS_ASSERT_NOT(gv_1 >= const_2);
    TS_ASSERT_NOT(gv_1 >= gv_2);
    TS_ASSERT_NOT(const_1 >= gv_2);
    TS_ASSERT_NOT(rgv_1 >= gv_2);
    TS_ASSERT_NOT(gv_1 >= rgv_2);

    TS_ASSERT(gv_2 >= const_1);
    TS_ASSERT(gv_2 >= gv_1);
    TS_ASSERT(const_2 >= gv_1);
    TS_ASSERT(rgv_2 >= gv_1);
    TS_ASSERT(gv_2 >= rgv_1);

    // Plus (same type only)
    TS_ASSERT(gv_1 + const_2 == const_3);
    TS_ASSERT(const_1 + gv_2 == const_3);
    TS_ASSERT(gv_1 + const_2 == gv_3a);
    TS_ASSERT(const_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + gv_2 == gv_3a);
    TS_ASSERT(rgv_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + rgv_2 == gv_3a);

    // Minus (same type only)
    TS_ASSERT(gv_2 - const_1 == const_1);
    TS_ASSERT(const_2 - gv_1 == const_1);
    TS_ASSERT(gv_2 - const_1 == gv_1);
    TS_ASSERT(const_2 - gv_1 == gv_1);
    TS_ASSERT(gv_3a - gv_1 == gv_2);
    TS_ASSERT(gv_2 - rgv_1 == gv_1);
    TS_ASSERT(rgv_2 - gv_1 == rgv_1);

    // Multiply (same type only)
    TS_ASSERT(gv_2 * const_1 == const_2);
    TS_ASSERT(const_2 * gv_1 == const_2);
    TS_ASSERT(gv_2 * const_1 == gv_2);
    TS_ASSERT(const_2 * gv_1 == gv_2);
    TS_ASSERT(gv_2 * gv_1 == gv_2);
    TS_ASSERT(rgv_1 * gv_2 == gv_2);
    TS_ASSERT(gv_1 * rgv_2 == rgv_2);

    // Divide (same type only)
    TS_ASSERT(gv_3a / const_2 == const_1);
    TS_ASSERT(const_3 / gv_2 == const_1);
    TS_ASSERT(gv_3a / const_2 == gv_1);
    TS_ASSERT(const_3 / gv_2 == gv_1);
    TS_ASSERT(gv_3a / gv_2 == gv_1);
    TS_ASSERT(gv_2 / rgv_2 == gv_1);
    TS_ASSERT(rgv_2 / gv_2 == rgv_1);

    // Assign (allows other value_var types)
    dirty_checker checker1(&gv_1);
    dirty_checker checker2(&gv_3a);
    dirty_checker checker3(&rgv_1);
    gv_1 = const_1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    gv_3a = gv_3b;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 2);
    gv_3a = (unsigned short)3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 2);
    gv_1 = gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    gv_1 = rgv_1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    rgv_1 = gv_1;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 2);
    gv_1 = const_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = (unsigned short)4;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = gv_3a;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 0);
    gv_1 = 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    rgv_1 = gv_2;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 1);
    gv_1 = gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    rgv_1 = gv_int1;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 1);

    // Plus-Assign (allows other value_var types)
    gv_1 += gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned short)2);
    gv_1 += gv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned short)4);
    gv_1 += const_3;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned short)7);
    gv_1 += (unsigned short)'\4';
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned short)0xb);
    gv_1 += 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned short)0x10);
    gv_1 += rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned short)0x12);

    // Minus-Assign (allows other value_var types)
    gv_1 -= rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned short)0x10);
    gv_1 -= 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned short)0xb);
    gv_1 -= (unsigned short)4;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned short)7);
    gv_1 -= const_3;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned short)4);
    gv_1 -= gv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned short)2);
    gv_1 -= gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned short)1);

    // Multiply-Assign (allows other value_var types)
    gv_3a *= gv_int1;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned short)3);
    gv_3a *= gv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned short)6);
    gv_3a *= const_3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned short)0x12);
    gv_3a *= (unsigned short)'\2';
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned short)0x24);
    gv_3a *= 2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned short)0x48);

    // Divide-Assign (allows other value_var types)
    gv_3a /= 2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned short)0x24);
    gv_3a /= (unsigned short)2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned short)0x12);
    gv_3a /= const_3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned short)6);
    gv_3a /= gv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned short)3);
    gv_3a /= gv_int1;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned short)3);

    // Repeat Multiply and Divide Assign
    gv_3a *= rgv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned short)6);
    gv_3a /= rgv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned short)3);

    // Negate - no effect on unsigned
    const unsigned short ng = -gv_3a;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 0);
    TS_ASSERT_EQUALS(ng, (unsigned short)3U);
    TS_ASSERT_EQUALS(gv_3a(), (unsigned short)3U);

    // Illegal compare
    test_typ mytest;
    TS_ASSERT(gv_1 != mytest);

    // Next
    generic_var<unsigned short> arry[2];
    TS_ASSERT_EQUALS(&arry[1], arry[0].getNext());
}

void GenVarTestSuite::testUnsignedInt(void)
{
    const unsigned int const_1 = 1;
    const unsigned int const_2 = 2;
    const unsigned int const_3 = 3;
    generic_var<unsigned int> gv_1(const_1);
    generic_var<unsigned int> gv_2(const_2);
    generic_var<unsigned int> gv_3a(const_3);
    generic_var<unsigned int> gv_3b(gv_3a);
    generic_var<unsigned int> gv_5((unsigned int)5);
    generic_var<unsigned int> gv_invalid;
    generic_var<long long>  gv_int1(1);
    r_generic_var<unsigned int> rgv_1(const_1);
    r_generic_var<unsigned int> rgv_2(const_2);

    // Equal test (allows other value_var types)
    TS_ASSERT(gv_1 == const_1);
    TS_ASSERT(gv_1 == (unsigned int)1);
    TS_ASSERT(const_1 == gv_1);
    TS_ASSERT(gv_1 == gv_int1);
    TS_ASSERT(gv_3a == gv_3b);
    TS_ASSERT(gv_3a == 3);
    TS_ASSERT(gv_invalid == 0xFFFFFFFF);
    TS_ASSERT(gv_1 == rgv_1);
    TS_ASSERT(rgv_1 == gv_1);

    TS_ASSERT_NOT(gv_1 == const_2);
    TS_ASSERT_NOT(gv_1 == (unsigned int)2);
    TS_ASSERT_NOT(gv_1 == gv_2);
    TS_ASSERT_NOT(const_2 == gv_1);
    TS_ASSERT_NOT(gv_2 == gv_int1);
    TS_ASSERT_NOT(gv_2 == 1);
    TS_ASSERT_NOT(gv_2 == rgv_1);
    TS_ASSERT_NOT(rgv_1 == gv_2);

    // Not equal test (allows other value_var types)
    TS_ASSERT_NOT(gv_1 != const_1);
    TS_ASSERT_NOT(gv_1 != (unsigned int)1);
    TS_ASSERT_NOT(const_1 != gv_1);
    TS_ASSERT_NOT(gv_1 != gv_int1);
    TS_ASSERT_NOT(gv_3a != gv_3b);
    TS_ASSERT_NOT(gv_3a != 3);
    TS_ASSERT_NOT(gv_invalid != -1);
    TS_ASSERT_NOT(gv_1 != rgv_1);
    TS_ASSERT_NOT(rgv_1 != gv_1);

    TS_ASSERT(gv_1 != const_2);
    TS_ASSERT(gv_1 != (unsigned int)2);
    TS_ASSERT(gv_1 != gv_2);
    TS_ASSERT(const_2 != gv_1);
    TS_ASSERT(gv_2 != gv_int1);
    TS_ASSERT(gv_2 != 1);
    TS_ASSERT(gv_2 != rgv_1);
    TS_ASSERT(rgv_1 != gv_2);

    // Less than test (same type only)
    TS_ASSERT_NOT(gv_1 < const_1);
    TS_ASSERT_NOT(gv_1 < (unsigned int)1);
    TS_ASSERT_NOT(const_1 < gv_1);
    TS_ASSERT_NOT(gv_3a < gv_3b);
    TS_ASSERT_NOT(gv_1 < rgv_1);
    TS_ASSERT_NOT(rgv_1 < gv_1);

    TS_ASSERT(gv_1 < const_2);
    TS_ASSERT(gv_1 < gv_2);
    TS_ASSERT(const_1 < gv_2);
    TS_ASSERT(rgv_1 < gv_2);
    TS_ASSERT(gv_1 < rgv_2);

    TS_ASSERT_NOT(gv_2 < const_1);
    TS_ASSERT_NOT(gv_2 < gv_1);
    TS_ASSERT_NOT(const_2 < gv_1);
    TS_ASSERT_NOT(rgv_2 < gv_1);
    TS_ASSERT_NOT(gv_2 < rgv_1);

    // Less than or equal test (same type only)
    TS_ASSERT(gv_1 <= const_1);
    TS_ASSERT(gv_1 <= (unsigned int)1);
    TS_ASSERT(const_1 <= gv_1);
    TS_ASSERT(gv_3a <= gv_3b);
    TS_ASSERT(gv_1 <= rgv_1);
    TS_ASSERT(rgv_1 <= gv_1);

    TS_ASSERT(gv_1 <= const_2);
    TS_ASSERT(gv_1 <= gv_2);
    TS_ASSERT(const_1 <= gv_2);
    TS_ASSERT(rgv_1 <= gv_2);
    TS_ASSERT(gv_1 <= rgv_2);

    TS_ASSERT_NOT(gv_2 <= const_1);
    TS_ASSERT_NOT(gv_2 <= gv_1);
    TS_ASSERT_NOT(const_2 <= gv_1);
    TS_ASSERT_NOT(rgv_2 <= gv_1);
    TS_ASSERT_NOT(gv_2 <= rgv_1);

    // More than test (same type only)
    TS_ASSERT_NOT(gv_1 > const_1);
    TS_ASSERT_NOT(gv_1 > (unsigned int)1);
    TS_ASSERT_NOT(const_1 > gv_1);
    TS_ASSERT_NOT(gv_3a > gv_3b);
    TS_ASSERT_NOT(gv_1 > rgv_1);
    TS_ASSERT_NOT(rgv_1 > gv_1);

    TS_ASSERT_NOT(gv_1 > const_2);
    TS_ASSERT_NOT(gv_1 > gv_2);
    TS_ASSERT_NOT(const_1 > gv_2);
    TS_ASSERT_NOT(rgv_1 > gv_2);
    TS_ASSERT_NOT(gv_1 > rgv_2);

    TS_ASSERT(gv_2 > const_1);
    TS_ASSERT(gv_2 > gv_1);
    TS_ASSERT(const_2 > gv_1);
    TS_ASSERT(rgv_2 > gv_1);
    TS_ASSERT(gv_2 > rgv_1);

    // More than and equal test (same type only)
    TS_ASSERT(gv_1 >= const_1);
    TS_ASSERT(gv_1 >= (unsigned int)1);
    TS_ASSERT(const_1 >= gv_1);
    TS_ASSERT(gv_3a >= gv_3b);
    TS_ASSERT(gv_1 >= rgv_1);
    TS_ASSERT(rgv_1 >= gv_1);

    TS_ASSERT_NOT(gv_1 >= const_2);
    TS_ASSERT_NOT(gv_1 >= gv_2);
    TS_ASSERT_NOT(const_1 >= gv_2);
    TS_ASSERT_NOT(rgv_1 >= gv_2);
    TS_ASSERT_NOT(gv_1 >= rgv_2);

    TS_ASSERT(gv_2 >= const_1);
    TS_ASSERT(gv_2 >= gv_1);
    TS_ASSERT(const_2 >= gv_1);
    TS_ASSERT(rgv_2 >= gv_1);
    TS_ASSERT(gv_2 >= rgv_1);

    // Plus (same type only)
    TS_ASSERT(gv_1 + const_2 == const_3);
    TS_ASSERT(const_1 + gv_2 == const_3);
    TS_ASSERT(gv_1 + const_2 == gv_3a);
    TS_ASSERT(const_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + gv_2 == gv_3a);
    TS_ASSERT(rgv_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + rgv_2 == gv_3a);

    // Minus (same type only)
    TS_ASSERT(gv_2 - const_1 == const_1);
    TS_ASSERT(const_2 - gv_1 == const_1);
    TS_ASSERT(gv_2 - const_1 == gv_1);
    TS_ASSERT(const_2 - gv_1 == gv_1);
    TS_ASSERT(gv_3a - gv_1 == gv_2);
    TS_ASSERT(gv_2 - rgv_1 == gv_1);
    TS_ASSERT(rgv_2 - gv_1 == rgv_1);

    // Multiply (same type only)
    TS_ASSERT(gv_2 * const_1 == const_2);
    TS_ASSERT(const_2 * gv_1 == const_2);
    TS_ASSERT(gv_2 * const_1 == gv_2);
    TS_ASSERT(const_2 * gv_1 == gv_2);
    TS_ASSERT(gv_2 * gv_1 == gv_2);
    TS_ASSERT(rgv_1 * gv_2 == gv_2);
    TS_ASSERT(gv_1 * rgv_2 == rgv_2);

    // Divide (same type only)
    TS_ASSERT(gv_3a / const_2 == const_1);
    TS_ASSERT(const_3 / gv_2 == const_1);
    TS_ASSERT(gv_3a / const_2 == gv_1);
    TS_ASSERT(const_3 / gv_2 == gv_1);
    TS_ASSERT(gv_3a / gv_2 == gv_1);
    TS_ASSERT(gv_2 / rgv_2 == gv_1);
    TS_ASSERT(rgv_2 / gv_2 == rgv_1);

    // Assign (allows other value_var types)
    dirty_checker checker1(&gv_1);
    dirty_checker checker2(&gv_3a);
    dirty_checker checker3(&rgv_1);
    gv_1 = const_1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    gv_3a = gv_3b;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 2);
    gv_3a = (unsigned int)3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 2);
    gv_1 = gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    gv_1 = rgv_1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    rgv_1 = gv_1;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 2);
    gv_1 = const_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = (unsigned int)4;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = gv_3a;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 0);
    gv_1 = 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    rgv_1 = gv_2;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 1);
    gv_1 = gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    rgv_1 = gv_int1;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 1);

    // Plus-Assign (allows other value_var types)
    gv_1 += gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned int)2);
    gv_1 += gv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned int)4);
    gv_1 += const_3;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned int)7);
    gv_1 += (unsigned int)'\4';
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned int)0xb);
    gv_1 += 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned int)0x10);
    gv_1 += rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned int)0x12);

    // Minus-Assign (allows other value_var types)
    gv_1 -= rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned int)0x10);
    gv_1 -= 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned int)0xb);
    gv_1 -= (unsigned int)4;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned int)7);
    gv_1 -= const_3;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned int)4);
    gv_1 -= gv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned int)2);
    gv_1 -= gv_int1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned int)1);

    // Multiply-Assign (allows other value_var types)
    gv_3a *= gv_int1;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned int)3);
    gv_3a *= gv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned int)6);
    gv_3a *= const_3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned int)0x12);
    gv_3a *= (unsigned int)'\2';
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned int)0x24);
    gv_3a *= 2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned int)0x48);

    // Divide-Assign (allows other value_var types)
    gv_3a /= 2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned int)0x24);
    gv_3a /= (unsigned int)2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned int)0x12);
    gv_3a /= const_3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned int)6);
    gv_3a /= gv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned int)3);
    gv_3a /= gv_int1;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned int)3);

    // Repeat Multiply and Divide Assign
    gv_3a *= rgv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned int)6);
    gv_3a /= rgv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned int)3);

    // Negate - no effect on unsigned
    const unsigned int ng = -gv_3a;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 0);
    TS_ASSERT_EQUALS(ng, 3U);
    TS_ASSERT_EQUALS(gv_3a(), 3U);

    // Illegal compare
    test_typ mytest;
    TS_ASSERT(gv_1 != mytest);

    // Next
    generic_var<unsigned int> arry[2];
    TS_ASSERT_EQUALS(&arry[1], arry[0].getNext());
}

void GenVarTestSuite::testUnsignedLong(void)
{
    const unsigned long const_1 = 1;
    const unsigned long const_2 = 2;
    const unsigned long const_3 = 3;
    generic_var<unsigned long> gv_1(const_1);
    generic_var<unsigned long> gv_2(const_2);
    generic_var<unsigned long> gv_3a(const_3);
    generic_var<unsigned long> gv_3b(gv_3a);
    generic_var<unsigned long> gv_5((unsigned long)5);
    generic_var<unsigned long> gv_invalid;
    generic_var<unsigned long long>  gv_long1(1);
    r_generic_var<unsigned long> rgv_1(const_1);
    r_generic_var<unsigned long> rgv_2(const_2);

    // Equal test (allows other value_var types)
    TS_ASSERT(gv_1 == const_1);
    TS_ASSERT(gv_1 == (unsigned long)1);
    TS_ASSERT(const_1 == gv_1);
    TS_ASSERT(gv_1 == gv_long1);
    TS_ASSERT(gv_3a == gv_3b);
    TS_ASSERT(gv_3a == 3);
    TS_ASSERT(gv_invalid == 0xFFFFFFFF);
    TS_ASSERT(gv_1 == rgv_1);
    TS_ASSERT(rgv_1 == gv_1);

    TS_ASSERT_NOT(gv_1 == const_2);
    TS_ASSERT_NOT(gv_1 == (unsigned long)2);
    TS_ASSERT_NOT(gv_1 == gv_2);
    TS_ASSERT_NOT(const_2 == gv_1);
    TS_ASSERT_NOT(gv_2 == gv_long1);
    TS_ASSERT_NOT(gv_2 == 1);
    TS_ASSERT_NOT(gv_2 == rgv_1);
    TS_ASSERT_NOT(rgv_1 == gv_2);

    // Not equal test (allows other value_var types)
    TS_ASSERT_NOT(gv_1 != const_1);
    TS_ASSERT_NOT(gv_1 != (unsigned long)1);
    TS_ASSERT_NOT(const_1 != gv_1);
    TS_ASSERT_NOT(gv_1 != gv_long1);
    TS_ASSERT_NOT(gv_3a != gv_3b);
    TS_ASSERT_NOT(gv_3a != 3);
    TS_ASSERT_NOT(gv_invalid != -1);
    TS_ASSERT_NOT(gv_1 != rgv_1);
    TS_ASSERT_NOT(rgv_1 != gv_1);

    TS_ASSERT(gv_1 != const_2);
    TS_ASSERT(gv_1 != (unsigned long)2);
    TS_ASSERT(gv_1 != gv_2);
    TS_ASSERT(const_2 != gv_1);
    TS_ASSERT(gv_2 != gv_long1);
    TS_ASSERT(gv_2 != 1);
    TS_ASSERT(gv_2 != rgv_1);
    TS_ASSERT(rgv_1 != gv_2);

    // Less than test (same type only)
    TS_ASSERT_NOT(gv_1 < const_1);
    TS_ASSERT_NOT(gv_1 < (unsigned long)1);
    TS_ASSERT_NOT(const_1 < gv_1);
    TS_ASSERT_NOT(gv_3a < gv_3b);
    TS_ASSERT_NOT(gv_1 < rgv_1);
    TS_ASSERT_NOT(rgv_1 < gv_1);

    TS_ASSERT(gv_1 < const_2);
    TS_ASSERT(gv_1 < gv_2);
    TS_ASSERT(const_1 < gv_2);
    TS_ASSERT(rgv_1 < gv_2);
    TS_ASSERT(gv_1 < rgv_2);

    TS_ASSERT_NOT(gv_2 < const_1);
    TS_ASSERT_NOT(gv_2 < gv_1);
    TS_ASSERT_NOT(const_2 < gv_1);
    TS_ASSERT_NOT(rgv_2 < gv_1);
    TS_ASSERT_NOT(gv_2 < rgv_1);

    // Less than or equal test (same type only)
    TS_ASSERT(gv_1 <= const_1);
    TS_ASSERT(gv_1 <= (unsigned long)1);
    TS_ASSERT(const_1 <= gv_1);
    TS_ASSERT(gv_3a <= gv_3b);
    TS_ASSERT(gv_1 <= rgv_1);
    TS_ASSERT(rgv_1 <= gv_1);

    TS_ASSERT(gv_1 <= const_2);
    TS_ASSERT(gv_1 <= gv_2);
    TS_ASSERT(const_1 <= gv_2);
    TS_ASSERT(rgv_1 <= gv_2);
    TS_ASSERT(gv_1 <= rgv_2);

    TS_ASSERT_NOT(gv_2 <= const_1);
    TS_ASSERT_NOT(gv_2 <= gv_1);
    TS_ASSERT_NOT(const_2 <= gv_1);
    TS_ASSERT_NOT(rgv_2 <= gv_1);
    TS_ASSERT_NOT(gv_2 <= rgv_1);

    // More than test (same type only)
    TS_ASSERT_NOT(gv_1 > const_1);
    TS_ASSERT_NOT(gv_1 > (unsigned long)1);
    TS_ASSERT_NOT(const_1 > gv_1);
    TS_ASSERT_NOT(gv_3a > gv_3b);
    TS_ASSERT_NOT(gv_1 > rgv_1);
    TS_ASSERT_NOT(rgv_1 > gv_1);

    TS_ASSERT_NOT(gv_1 > const_2);
    TS_ASSERT_NOT(gv_1 > gv_2);
    TS_ASSERT_NOT(const_1 > gv_2);
    TS_ASSERT_NOT(rgv_1 > gv_2);
    TS_ASSERT_NOT(gv_1 > rgv_2);

    TS_ASSERT(gv_2 > const_1);
    TS_ASSERT(gv_2 > gv_1);
    TS_ASSERT(const_2 > gv_1);
    TS_ASSERT(rgv_2 > gv_1);
    TS_ASSERT(gv_2 > rgv_1);

    // More than and equal test (same type only)
    TS_ASSERT(gv_1 >= const_1);
    TS_ASSERT(gv_1 >= (unsigned long)1);
    TS_ASSERT(const_1 >= gv_1);
    TS_ASSERT(gv_3a >= gv_3b);
    TS_ASSERT(gv_1 >= rgv_1);
    TS_ASSERT(rgv_1 >= gv_1);

    TS_ASSERT_NOT(gv_1 >= const_2);
    TS_ASSERT_NOT(gv_1 >= gv_2);
    TS_ASSERT_NOT(const_1 >= gv_2);
    TS_ASSERT_NOT(rgv_1 >= gv_2);
    TS_ASSERT_NOT(gv_1 >= rgv_2);

    TS_ASSERT(gv_2 >= const_1);
    TS_ASSERT(gv_2 >= gv_1);
    TS_ASSERT(const_2 >= gv_1);
    TS_ASSERT(rgv_2 >= gv_1);
    TS_ASSERT(gv_2 >= rgv_1);

    // Plus (same type only)
    TS_ASSERT(gv_1 + const_2 == const_3);
    TS_ASSERT(const_1 + gv_2 == const_3);
    TS_ASSERT(gv_1 + const_2 == gv_3a);
    TS_ASSERT(const_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + gv_2 == gv_3a);
    TS_ASSERT(rgv_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + rgv_2 == gv_3a);

    // Minus (same type only)
    TS_ASSERT(gv_2 - const_1 == const_1);
    TS_ASSERT(const_2 - gv_1 == const_1);
    TS_ASSERT(gv_2 - const_1 == gv_1);
    TS_ASSERT(const_2 - gv_1 == gv_1);
    TS_ASSERT(gv_3a - gv_1 == gv_2);
    TS_ASSERT(gv_2 - rgv_1 == gv_1);
    TS_ASSERT(rgv_2 - gv_1 == rgv_1);

    // Multiply (same type only)
    TS_ASSERT(gv_2 * const_1 == const_2);
    TS_ASSERT(const_2 * gv_1 == const_2);
    TS_ASSERT(gv_2 * const_1 == gv_2);
    TS_ASSERT(const_2 * gv_1 == gv_2);
    TS_ASSERT(gv_2 * gv_1 == gv_2);
    TS_ASSERT(rgv_1 * gv_2 == gv_2);
    TS_ASSERT(gv_1 * rgv_2 == rgv_2);

    // Divide (same type only)
    TS_ASSERT(gv_3a / const_2 == const_1);
    TS_ASSERT(const_3 / gv_2 == const_1);
    TS_ASSERT(gv_3a / const_2 == gv_1);
    TS_ASSERT(const_3 / gv_2 == gv_1);
    TS_ASSERT(gv_3a / gv_2 == gv_1);
    TS_ASSERT(gv_2 / rgv_2 == gv_1);
    TS_ASSERT(rgv_2 / gv_2 == rgv_1);

    // Assign (allows other value_var types)
    dirty_checker checker1(&gv_1);
    dirty_checker checker2(&gv_3a);
    dirty_checker checker3(&rgv_1);
    gv_1 = const_1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    gv_3a = gv_3b;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 2);
    gv_3a = (unsigned long)3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 2);
    gv_1 = gv_long1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    gv_1 = rgv_1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    rgv_1 = gv_1;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 2);
    gv_1 = const_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = (unsigned long)4;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = gv_3a;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 0);
    gv_1 = 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    rgv_1 = gv_2;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 1);
    gv_1 = gv_long1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    rgv_1 = gv_long1;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 1);

    // Plus-Assign (allows other value_var types)
    gv_1 += gv_long1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long)2);
    gv_1 += gv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long)4);
    gv_1 += const_3;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long)7);
    gv_1 += (unsigned long)'\4';
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long)0xb);
    gv_1 += 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long)0x10);
    gv_1 += rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long)0x12);

    // Minus-Assign (allows other value_var types)
    gv_1 -= rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long)0x10);
    gv_1 -= 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long)0xb);
    gv_1 -= (unsigned long)4;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long)7);
    gv_1 -= const_3;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long)4);
    gv_1 -= gv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long)2);
    gv_1 -= gv_long1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long)1);

    // Multiply-Assign (allows other value_var types)
    gv_3a *= gv_long1;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long)3);
    gv_3a *= gv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long)6);
    gv_3a *= const_3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long)0x12);
    gv_3a *= (unsigned long)'\2';
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long)0x24);
    gv_3a *= 2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long)0x48);

    // Divide-Assign (allows other value_var types)
    gv_3a /= 2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long)0x24);
    gv_3a /= (unsigned long)2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long)0x12);
    gv_3a /= const_3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long)6);
    gv_3a /= gv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long)3);
    gv_3a /= gv_long1;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long)3);

    // Repeat Multiply and Divide Assign
    gv_3a *= rgv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long)6);
    gv_3a /= rgv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long)3);

    // Negate - no effect on unsigned
    const unsigned long ng = -gv_3a;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 0);
    TS_ASSERT_EQUALS(ng, 3UL);
    TS_ASSERT_EQUALS(gv_3a(), 3UL);

    // Illegal compare
    test_typ mytest;
    TS_ASSERT(gv_1 != mytest);

    // Next
    generic_var<unsigned long> arry[2];
    TS_ASSERT_EQUALS(&arry[1], arry[0].getNext());
}

void GenVarTestSuite::testUnsignedLongLong(void)
{
    const unsigned long long const_1 = 1;
    const unsigned long long const_2 = 2;
    const unsigned long long const_3 = 3;
    generic_var<unsigned long long> gv_1(const_1);
    generic_var<unsigned long long> gv_2(const_2);
    generic_var<unsigned long long> gv_3a(const_3);
    generic_var<unsigned long long> gv_3b(gv_3a);
    generic_var<unsigned long long> gv_5((unsigned long long)5);
    generic_var<unsigned long long> gv_invalid;
    generic_var<long>  gv_long1(1);
    r_generic_var<unsigned long long> rgv_1(const_1);
    r_generic_var<unsigned long long> rgv_2(const_2);

    // Equal test (allows other value_var types)
    TS_ASSERT(gv_1 == const_1);
    TS_ASSERT(gv_1 == (unsigned long long)1);
    TS_ASSERT(const_1 == gv_1);
    TS_ASSERT(gv_1 == gv_long1);
    TS_ASSERT(gv_3a == gv_3b);
    TS_ASSERT(gv_3a == 3);
    TS_ASSERT(gv_invalid == 0xffffffffffffffffULL);
    TS_ASSERT(gv_1 == rgv_1);
    TS_ASSERT(rgv_1 == gv_1);

    TS_ASSERT_NOT(gv_1 == const_2);
    TS_ASSERT_NOT(gv_1 == (unsigned long long)2);
    TS_ASSERT_NOT(gv_1 == gv_2);
    TS_ASSERT_NOT(const_2 == gv_1);
    TS_ASSERT_NOT(gv_2 == gv_long1);
    TS_ASSERT_NOT(gv_2 == 1);
    TS_ASSERT_NOT(gv_2 == rgv_1);
    TS_ASSERT_NOT(rgv_1 == gv_2);

    // Not equal test (allows other value_var types)
    TS_ASSERT_NOT(gv_1 != const_1);
    TS_ASSERT_NOT(gv_1 != (unsigned long long)1);
    TS_ASSERT_NOT(const_1 != gv_1);
    TS_ASSERT_NOT(gv_1 != gv_long1);
    TS_ASSERT_NOT(gv_3a != gv_3b);
    TS_ASSERT_NOT(gv_3a != 3);
    TS_ASSERT_NOT(gv_invalid != -1);
    TS_ASSERT_NOT(gv_1 != rgv_1);
    TS_ASSERT_NOT(rgv_1 != gv_1);

    TS_ASSERT(gv_1 != const_2);
    TS_ASSERT(gv_1 != (unsigned long long)2);
    TS_ASSERT(gv_1 != gv_2);
    TS_ASSERT(const_2 != gv_1);
    TS_ASSERT(gv_2 != gv_long1);
    TS_ASSERT(gv_2 != 1);
    TS_ASSERT(gv_2 != rgv_1);
    TS_ASSERT(rgv_1 != gv_2);

    // Less than test (same type only)
    TS_ASSERT_NOT(gv_1 < const_1);
    TS_ASSERT_NOT(gv_1 < (unsigned long long)1);
    TS_ASSERT_NOT(const_1 < gv_1);
    TS_ASSERT_NOT(gv_3a < gv_3b);
    TS_ASSERT_NOT(gv_1 < rgv_1);
    TS_ASSERT_NOT(rgv_1 < gv_1);

    TS_ASSERT(gv_1 < const_2);
    TS_ASSERT(gv_1 < gv_2);
    TS_ASSERT(const_1 < gv_2);
    TS_ASSERT(rgv_1 < gv_2);
    TS_ASSERT(gv_1 < rgv_2);

    TS_ASSERT_NOT(gv_2 < const_1);
    TS_ASSERT_NOT(gv_2 < gv_1);
    TS_ASSERT_NOT(const_2 < gv_1);
    TS_ASSERT_NOT(rgv_2 < gv_1);
    TS_ASSERT_NOT(gv_2 < rgv_1);

    // Less than or equal test (same type only)
    TS_ASSERT(gv_1 <= const_1);
    TS_ASSERT(gv_1 <= (unsigned long long)1);
    TS_ASSERT(const_1 <= gv_1);
    TS_ASSERT(gv_3a <= gv_3b);
    TS_ASSERT(gv_1 <= rgv_1);
    TS_ASSERT(rgv_1 <= gv_1);

    TS_ASSERT(gv_1 <= const_2);
    TS_ASSERT(gv_1 <= gv_2);
    TS_ASSERT(const_1 <= gv_2);
    TS_ASSERT(rgv_1 <= gv_2);
    TS_ASSERT(gv_1 <= rgv_2);

    TS_ASSERT_NOT(gv_2 <= const_1);
    TS_ASSERT_NOT(gv_2 <= gv_1);
    TS_ASSERT_NOT(const_2 <= gv_1);
    TS_ASSERT_NOT(rgv_2 <= gv_1);
    TS_ASSERT_NOT(gv_2 <= rgv_1);

    // More than test (same type only)
    TS_ASSERT_NOT(gv_1 > const_1);
    TS_ASSERT_NOT(gv_1 > (unsigned long long)1);
    TS_ASSERT_NOT(const_1 > gv_1);
    TS_ASSERT_NOT(gv_3a > gv_3b);
    TS_ASSERT_NOT(gv_1 > rgv_1);
    TS_ASSERT_NOT(rgv_1 > gv_1);

    TS_ASSERT_NOT(gv_1 > const_2);
    TS_ASSERT_NOT(gv_1 > gv_2);
    TS_ASSERT_NOT(const_1 > gv_2);
    TS_ASSERT_NOT(rgv_1 > gv_2);
    TS_ASSERT_NOT(gv_1 > rgv_2);

    TS_ASSERT(gv_2 > const_1);
    TS_ASSERT(gv_2 > gv_1);
    TS_ASSERT(const_2 > gv_1);
    TS_ASSERT(rgv_2 > gv_1);
    TS_ASSERT(gv_2 > rgv_1);

    // More than and equal test (same type only)
    TS_ASSERT(gv_1 >= const_1);
    TS_ASSERT(gv_1 >= (unsigned long long)1);
    TS_ASSERT(const_1 >= gv_1);
    TS_ASSERT(gv_3a >= gv_3b);
    TS_ASSERT(gv_1 >= rgv_1);
    TS_ASSERT(rgv_1 >= gv_1);

    TS_ASSERT_NOT(gv_1 >= const_2);
    TS_ASSERT_NOT(gv_1 >= gv_2);
    TS_ASSERT_NOT(const_1 >= gv_2);
    TS_ASSERT_NOT(rgv_1 >= gv_2);
    TS_ASSERT_NOT(gv_1 >= rgv_2);

    TS_ASSERT(gv_2 >= const_1);
    TS_ASSERT(gv_2 >= gv_1);
    TS_ASSERT(const_2 >= gv_1);
    TS_ASSERT(rgv_2 >= gv_1);
    TS_ASSERT(gv_2 >= rgv_1);

    // Plus (same type only)
    TS_ASSERT(gv_1 + const_2 == const_3);
    TS_ASSERT(const_1 + gv_2 == const_3);
    TS_ASSERT(gv_1 + const_2 == gv_3a);
    TS_ASSERT(const_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + gv_2 == gv_3a);
    TS_ASSERT(rgv_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + rgv_2 == gv_3a);

    // Minus (same type only)
    TS_ASSERT(gv_2 - const_1 == const_1);
    TS_ASSERT(const_2 - gv_1 == const_1);
    TS_ASSERT(gv_2 - const_1 == gv_1);
    TS_ASSERT(const_2 - gv_1 == gv_1);
    TS_ASSERT(gv_3a - gv_1 == gv_2);
    TS_ASSERT(gv_2 - rgv_1 == gv_1);
    TS_ASSERT(rgv_2 - gv_1 == rgv_1);

    // Multiply (same type only)
    TS_ASSERT(gv_2 * const_1 == const_2);
    TS_ASSERT(const_2 * gv_1 == const_2);
    TS_ASSERT(gv_2 * const_1 == gv_2);
    TS_ASSERT(const_2 * gv_1 == gv_2);
    TS_ASSERT(gv_2 * gv_1 == gv_2);
    TS_ASSERT(rgv_1 * gv_2 == gv_2);
    TS_ASSERT(gv_1 * rgv_2 == rgv_2);

    // Divide (same type only)
    TS_ASSERT(gv_3a / const_2 == const_1);
    TS_ASSERT(const_3 / gv_2 == const_1);
    TS_ASSERT(gv_3a / const_2 == gv_1);
    TS_ASSERT(const_3 / gv_2 == gv_1);
    TS_ASSERT(gv_3a / gv_2 == gv_1);
    TS_ASSERT(gv_2 / rgv_2 == gv_1);
    TS_ASSERT(rgv_2 / gv_2 == rgv_1);

    // Assign (allows other value_var types)
    dirty_checker checker1(&gv_1);
    dirty_checker checker2(&gv_3a);
    dirty_checker checker3(&rgv_1);
    gv_1 = const_1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    gv_3a = gv_3b;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 2);
    gv_3a = (unsigned long long)3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 2);
    gv_1 = gv_long1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    gv_1 = rgv_1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    rgv_1 = gv_1;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 2);
    gv_1 = const_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = (unsigned long long)4;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = gv_3a;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 0);
    gv_1 = 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    gv_1 = rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    rgv_1 = gv_2;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 1);
    gv_1 = gv_long1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    rgv_1 = gv_long1;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 1);

    // Plus-Assign (allows other value_var types)
    gv_1 += gv_long1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long long)2);
    gv_1 += gv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long long)4);
    gv_1 += const_3;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long long)7);
    gv_1 += (unsigned long long)'\4';
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long long)0xb);
    gv_1 += 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long long)0x10);
    gv_1 += rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long long)0x12);

    // Minus-Assign (allows other value_var types)
    gv_1 -= rgv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long long)0x10);
    gv_1 -= 5;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long long)0xb);
    gv_1 -= (unsigned long long)4;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long long)7);
    gv_1 -= const_3;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long long)4);
    gv_1 -= gv_2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long long)2);
    gv_1 -= gv_long1;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_1, (unsigned long long)1);

    // Multiply-Assign (allows other value_var types)
    gv_3a *= gv_long1;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long long)3);
    gv_3a *= gv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long long)6);
    gv_3a *= const_3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long long)0x12);
    gv_3a *= (unsigned long long)'\2';
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long long)0x24);
    gv_3a *= 2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long long)0x48);

    // Divide-Assign (allows other value_var types)
    gv_3a /= 2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long long)0x24);
    gv_3a /= (unsigned long long)2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long long)0x12);
    gv_3a /= const_3;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long long)6);
    gv_3a /= gv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long long)3);
    gv_3a /= gv_long1;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long long)3);

    // Repeat Multiply and Divide Assign
    gv_3a *= rgv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long long)6);
    gv_3a /= rgv_2;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(gv_3a, (unsigned long long)3);

    // Negate - no effect on unsigned
    const unsigned long long ng = -gv_3a;
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 0);
    TS_ASSERT_EQUALS(ng, 3ULL);
    TS_ASSERT_EQUALS(gv_3a(), 3ULL);

    // Illegal compare
    test_typ mytest;
    TS_ASSERT(gv_1 != mytest);

    // Next
    generic_var<unsigned long long> arry[2];
    TS_ASSERT_EQUALS(&arry[1], arry[0].getNext());
}

void GenVarTestSuite::testFloat(void)
{
    generic_var<float> flt;
    dirty_checker checker(&flt);
    TS_ASSERT(!flt.setInvalid());
    TS_ASSERT_EQUALS(checker.getStateAndReset(), 2);

    const float const_1 = (float)rand() / (float)rand();
    const float const_2 = (float)rand() * (float)rand();
    const float const_3 = const_1 + const_2;
    const float const_4 = const_1 * const_2;
    const float const_5 = const_1 / const_2;
    generic_var<float> gv_1(const_1);
    generic_var<float> gv_2(const_2);
    generic_var<float> gv_3a(const_3);
    generic_var<float> gv_3b(gv_3a);
    generic_var<float> gv_4(const_4);
    generic_var<float> gv_5(const_5);
    generic_var<int>  gv_int1(1);
    r_generic_var<float> rgv_1(const_1);
    r_generic_var<float> rgv_2(const_2);

    // Equal test (allows other value_var types)
    TS_ASSERT(gv_1 == const_1);
    TS_ASSERT(const_1 == gv_1);
    TS_ASSERT(gv_3a == gv_3b);
    TS_ASSERT(gv_1 == rgv_1);
    TS_ASSERT(rgv_1 == gv_1);

    TS_ASSERT_NOT(gv_1 == const_2);
    TS_ASSERT_NOT(gv_1 == gv_2);
    TS_ASSERT_NOT(const_2 == gv_1);
    TS_ASSERT_NOT(gv_2 == rgv_1);
    TS_ASSERT_NOT(rgv_1 == gv_2);

    // Not equal test (allows other value_var types)
    TS_ASSERT_NOT(gv_1 != const_1);
    TS_ASSERT_NOT(const_1 != gv_1);
    TS_ASSERT_NOT(gv_3a != gv_3b);
    TS_ASSERT_NOT(gv_1 != rgv_1);
    TS_ASSERT_NOT(rgv_1 != gv_1);

    TS_ASSERT(gv_1 != const_2);
    TS_ASSERT(gv_1 != gv_2);
    TS_ASSERT(const_2 != gv_1);
    TS_ASSERT(gv_2 != gv_int1);
    TS_ASSERT(gv_2 != 1);
    TS_ASSERT(gv_2 != rgv_1);
    TS_ASSERT(rgv_1 != gv_2);

    // Less than test (same type only)
    TS_ASSERT_NOT(gv_1 < const_1);
    TS_ASSERT_NOT(const_1 < gv_1);
    TS_ASSERT_NOT(gv_3a < gv_3b);
    TS_ASSERT_NOT(gv_1 < rgv_1);
    TS_ASSERT_NOT(rgv_1 < gv_1);

    TS_ASSERT(gv_1 < const_2);
    TS_ASSERT(gv_1 < gv_2);
    TS_ASSERT(const_1 < gv_2);
    TS_ASSERT(rgv_1 < gv_2);
    TS_ASSERT(gv_1 < rgv_2);

    TS_ASSERT_NOT(gv_2 < const_1);
    TS_ASSERT_NOT(gv_2 < gv_1);
    TS_ASSERT_NOT(const_2 < gv_1);
    TS_ASSERT_NOT(rgv_2 < gv_1);
    TS_ASSERT_NOT(gv_2 < rgv_1);

    // Less than or equal test (same type only)
    TS_ASSERT(gv_1 <= const_1);
    TS_ASSERT(const_1 <= gv_1);
    TS_ASSERT(gv_3a <= gv_3b);
    TS_ASSERT(gv_1 <= rgv_1);
    TS_ASSERT(rgv_1 <= gv_1);

    TS_ASSERT(gv_1 <= const_2);
    TS_ASSERT(gv_1 <= gv_2);
    TS_ASSERT(const_1 <= gv_2);
    TS_ASSERT(rgv_1 <= gv_2);
    TS_ASSERT(gv_1 <= rgv_2);

    TS_ASSERT_NOT(gv_2 <= const_1);
    TS_ASSERT_NOT(gv_2 <= gv_1);
    TS_ASSERT_NOT(const_2 <= gv_1);
    TS_ASSERT_NOT(rgv_2 <= gv_1);
    TS_ASSERT_NOT(gv_2 <= rgv_1);

    // More than test (same type only)
    TS_ASSERT_NOT(gv_1 > const_1);
    TS_ASSERT_NOT(const_1 > gv_1);
    TS_ASSERT_NOT(gv_3a > gv_3b);
    TS_ASSERT_NOT(gv_1 > rgv_1);
    TS_ASSERT_NOT(rgv_1 > gv_1);

    TS_ASSERT_NOT(gv_1 > const_2);
    TS_ASSERT_NOT(gv_1 > gv_2);
    TS_ASSERT_NOT(const_1 > gv_2);
    TS_ASSERT_NOT(rgv_1 > gv_2);
    TS_ASSERT_NOT(gv_1 > rgv_2);

    TS_ASSERT(gv_2 > const_1);
    TS_ASSERT(gv_2 > gv_1);
    TS_ASSERT(const_2 > gv_1);
    TS_ASSERT(rgv_2 > gv_1);
    TS_ASSERT(gv_2 > rgv_1);

    // More than and equal test (same type only)
    TS_ASSERT(gv_1 >= const_1);
    TS_ASSERT(const_1 >= gv_1);
    TS_ASSERT(gv_3a >= gv_3b);
    TS_ASSERT(gv_1 >= rgv_1);
    TS_ASSERT(rgv_1 >= gv_1);

    TS_ASSERT_NOT(gv_1 >= const_2);
    TS_ASSERT_NOT(gv_1 >= gv_2);
    TS_ASSERT_NOT(const_1 >= gv_2);
    TS_ASSERT_NOT(rgv_1 >= gv_2);
    TS_ASSERT_NOT(gv_1 >= rgv_2);

    TS_ASSERT(gv_2 >= const_1);
    TS_ASSERT(gv_2 >= gv_1);
    TS_ASSERT(const_2 >= gv_1);
    TS_ASSERT(rgv_2 >= gv_1);
    TS_ASSERT(gv_2 >= rgv_1);

    // Plus (same type only)
    TS_ASSERT(gv_1 + const_2 == const_1 + const_2);
    TS_ASSERT(const_1 + gv_2 == const_1 + const_2);
    TS_ASSERT(gv_1 + const_2 == gv_3a);
    TS_ASSERT(const_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + gv_2 == gv_3a);
    TS_ASSERT(rgv_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + rgv_2 == gv_3a);

    // Minus (same type only)
    TS_ASSERT_DELTA(gv_3a - const_2, const_1, 1.e-2);
    TS_ASSERT_DELTA(const_3 - gv_2, const_1, 1.e-2);
    TS_ASSERT_DELTA(gv_3a - const_2, gv_1, 1.e-2);
    TS_ASSERT_DELTA(const_3 - gv_2, gv_1, 1.e-2);
    TS_ASSERT_DELTA(gv_3a - gv_1, gv_2, 1.e-2);
    TS_ASSERT_DELTA(gv_3b - rgv_2, gv_1, 1.e-2);

    // Multiply (same type only)
    TS_ASSERT(gv_2 * const_1 == const_4);
    TS_ASSERT(const_2 * gv_1 == const_4);
    TS_ASSERT(gv_2 * const_1 == gv_4);
    TS_ASSERT(const_2 * gv_1 == gv_4);
    TS_ASSERT(gv_2 * gv_1 == gv_4);
    TS_ASSERT(rgv_1 * gv_2 == gv_4);
    TS_ASSERT(gv_1 * rgv_2 == gv_4);

    // Divide (same type only)
    TS_ASSERT(gv_1 / const_2 == const_5);
    TS_ASSERT(const_1 / gv_2 == const_5);
    TS_ASSERT(gv_4 / const_2 == gv_1);
    TS_ASSERT(const_4 / gv_2 == gv_1);
    TS_ASSERT(gv_4 / gv_2 == gv_1);
    TS_ASSERT(gv_4 / rgv_2 == gv_1);
    TS_ASSERT(gv_4 / gv_2 == rgv_1);

    // Illegal compare
    test_typ mytest;
    TS_ASSERT(gv_1 != mytest);

    // Next
    generic_var<float> arry[2];
    TS_ASSERT_EQUALS(&arry[1], arry[0].getNext());
}

void GenVarTestSuite::testDouble(void)
{
    generic_var<float> dbl;
    dirty_checker checker(&dbl);
    TS_ASSERT(!dbl.setInvalid());
    TS_ASSERT_EQUALS(checker.getStateAndReset(), 2);

    const double const_1 = (double)rand() / (double)rand() / (double)rand();
    const double const_2 = ((double)rand() * (double)rand()) / (double)rand();
    const double const_3 = const_1 + const_2;
    const double const_4 = const_1 * const_2;
    const double const_5 = const_1 / const_2;
    generic_var<double> gv_1(const_1);
    generic_var<double> gv_2(const_2);
    generic_var<double> gv_3a(const_3);
    generic_var<double> gv_3b(gv_3a);
    generic_var<double> gv_4(const_4);
    generic_var<double> gv_5(const_5);
    generic_var<int>  gv_int1(1);
    r_generic_var<double> rgv_1(const_1);
    r_generic_var<double> rgv_2(const_2);

    // Equal test (allows other value_var types)
    TS_ASSERT(gv_1 == const_1);
    TS_ASSERT(const_1 == gv_1);
    TS_ASSERT(gv_3a == gv_3b);
    TS_ASSERT(gv_1 == rgv_1);
    TS_ASSERT(rgv_1 == gv_1);

    TS_ASSERT_NOT(gv_1 == const_2);
    TS_ASSERT_NOT(gv_1 == gv_2);
    TS_ASSERT_NOT(const_2 == gv_1);
    TS_ASSERT_NOT(gv_2 == rgv_1);
    TS_ASSERT_NOT(rgv_1 == gv_2);

    // Not equal test (allows other value_var types)
    TS_ASSERT_NOT(gv_1 != const_1);
    TS_ASSERT_NOT(const_1 != gv_1);
    TS_ASSERT_NOT(gv_3a != gv_3b);
    TS_ASSERT_NOT(gv_1 != rgv_1);
    TS_ASSERT_NOT(rgv_1 != gv_1);

    TS_ASSERT(gv_1 != const_2);
    TS_ASSERT(gv_1 != gv_2);
    TS_ASSERT(const_2 != gv_1);
    TS_ASSERT(gv_2 != gv_int1);
    TS_ASSERT(gv_2 != 1);
    TS_ASSERT(gv_2 != rgv_1);
    TS_ASSERT(rgv_1 != gv_2);

    // Less than test (same type only)
    TS_ASSERT_NOT(gv_1 < const_1);
    TS_ASSERT_NOT(const_1 < gv_1);
    TS_ASSERT_NOT(gv_3a < gv_3b);
    TS_ASSERT_NOT(gv_1 < rgv_1);
    TS_ASSERT_NOT(rgv_1 < gv_1);

    TS_ASSERT(gv_1 < const_2);
    TS_ASSERT(gv_1 < gv_2);
    TS_ASSERT(const_1 < gv_2);
    TS_ASSERT(rgv_1 < gv_2);
    TS_ASSERT(gv_1 < rgv_2);

    TS_ASSERT_NOT(gv_2 < const_1);
    TS_ASSERT_NOT(gv_2 < gv_1);
    TS_ASSERT_NOT(const_2 < gv_1);
    TS_ASSERT_NOT(rgv_2 < gv_1);
    TS_ASSERT_NOT(gv_2 < rgv_1);

    // Less than or equal test (same type only)
    TS_ASSERT(gv_1 <= const_1);
    TS_ASSERT(const_1 <= gv_1);
    TS_ASSERT(gv_3a <= gv_3b);
    TS_ASSERT(gv_1 <= rgv_1);
    TS_ASSERT(rgv_1 <= gv_1);

    TS_ASSERT(gv_1 <= const_2);
    TS_ASSERT(gv_1 <= gv_2);
    TS_ASSERT(const_1 <= gv_2);
    TS_ASSERT(rgv_1 <= gv_2);
    TS_ASSERT(gv_1 <= rgv_2);

    TS_ASSERT_NOT(gv_2 <= const_1);
    TS_ASSERT_NOT(gv_2 <= gv_1);
    TS_ASSERT_NOT(const_2 <= gv_1);
    TS_ASSERT_NOT(rgv_2 <= gv_1);
    TS_ASSERT_NOT(gv_2 <= rgv_1);

    // More than test (same type only)
    TS_ASSERT_NOT(gv_1 > const_1);
    TS_ASSERT_NOT(const_1 > gv_1);
    TS_ASSERT_NOT(gv_3a > gv_3b);
    TS_ASSERT_NOT(gv_1 > rgv_1);
    TS_ASSERT_NOT(rgv_1 > gv_1);

    TS_ASSERT_NOT(gv_1 > const_2);
    TS_ASSERT_NOT(gv_1 > gv_2);
    TS_ASSERT_NOT(const_1 > gv_2);
    TS_ASSERT_NOT(rgv_1 > gv_2);
    TS_ASSERT_NOT(gv_1 > rgv_2);

    TS_ASSERT(gv_2 > const_1);
    TS_ASSERT(gv_2 > gv_1);
    TS_ASSERT(const_2 > gv_1);
    TS_ASSERT(rgv_2 > gv_1);
    TS_ASSERT(gv_2 > rgv_1);

    // More than and equal test (same type only)
    TS_ASSERT(gv_1 >= const_1);
    TS_ASSERT(const_1 >= gv_1);
    TS_ASSERT(gv_3a >= gv_3b);
    TS_ASSERT(gv_1 >= rgv_1);
    TS_ASSERT(rgv_1 >= gv_1);

    TS_ASSERT_NOT(gv_1 >= const_2);
    TS_ASSERT_NOT(gv_1 >= gv_2);
    TS_ASSERT_NOT(const_1 >= gv_2);
    TS_ASSERT_NOT(rgv_1 >= gv_2);
    TS_ASSERT_NOT(gv_1 >= rgv_2);

    TS_ASSERT(gv_2 >= const_1);
    TS_ASSERT(gv_2 >= gv_1);
    TS_ASSERT(const_2 >= gv_1);
    TS_ASSERT(rgv_2 >= gv_1);
    TS_ASSERT(gv_2 >= rgv_1);

    // Plus (same type only)
    TS_ASSERT(gv_1 + const_2 == const_1 + const_2);
    TS_ASSERT(const_1 + gv_2 == const_1 + const_2);
    TS_ASSERT(gv_1 + const_2 == gv_3a);
    TS_ASSERT(const_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + gv_2 == gv_3a);
    TS_ASSERT(rgv_1 + gv_2 == gv_3a);
    TS_ASSERT(gv_1 + rgv_2 == gv_3a);

    // Minus (same type only)
    TS_ASSERT_DELTA(gv_3a - const_2, const_1, 1.e-6);
    TS_ASSERT_DELTA(const_3 - gv_2, const_1, 1.e-6);
    TS_ASSERT_DELTA(gv_3a - const_2, gv_1, 1.e-6);
    TS_ASSERT_DELTA(const_3 - gv_2, gv_1, 1.e-6);
    TS_ASSERT_DELTA(gv_3a - gv_1, gv_2, 1.e-6);
    TS_ASSERT_DELTA(gv_3b - rgv_2, gv_1, 1.e-6);

    // Multiply (same type only)
    TS_ASSERT(gv_2 * const_1 == const_4);
    TS_ASSERT(const_2 * gv_1 == const_4);
    TS_ASSERT(gv_2 * const_1 == gv_4);
    TS_ASSERT(const_2 * gv_1 == gv_4);
    TS_ASSERT(gv_2 * gv_1 == gv_4);
    TS_ASSERT(rgv_1 * gv_2 == gv_4);
    TS_ASSERT(gv_1 * rgv_2 == gv_4);

    // Divide (same type only)
    TS_ASSERT(gv_1 / const_2 == const_5);
    TS_ASSERT(const_1 / gv_2 == const_5);
    TS_ASSERT(gv_4 / const_2 == gv_1);
    TS_ASSERT(const_4 / gv_2 == gv_1);
    TS_ASSERT(gv_4 / gv_2 == gv_1);
    TS_ASSERT(gv_4 / rgv_2 == gv_1);
    TS_ASSERT(gv_4 / gv_2 == rgv_1);

    // Illegal compare
    test_typ mytest;
    TS_ASSERT(gv_1 != mytest);

    // Next
    generic_var<double> arry[2];
    TS_ASSERT_EQUALS(&arry[1], arry[0].getNext());
}

void GenVarTestSuite::testExtract(void)
{
    char  vch[] = { -128, -1, 0, 1, 127 };
    short vsh[] = { -32768, -1, 0, 1, 32767 };
    int   vit[] = { -2147483647, -1, 0, 1, 2147483647 };
    long  vlg[] = { -2147483647, -1, 0, 1, 2147483647 };
    long long vll[] = { -9223372036854775807LL, -1, 0, 1, 9223372036854775807LL };
    unsigned char  uch[] = { 0, 1, 127, 128, 255 };
    unsigned short ush[] = { 0, 1, 32767, 32768, 65535 };
    unsigned int   uit[] = { 0, 1, 2147483647, 2147483648, 4294967295 };
    unsigned long  ulg[] = { 0, 1, 2147483647, 2147483648, 4294967295 };
    unsigned long long ull[] = { 0, 1, 9223372036854775807LL, 9223372036854775808UL, 18446744073709551615UL };
    float  flt[] = { -3.4028e19F, -1.1755e-19F, 0, 1.1755e-19F, 3.4028e19F };
    double dbl[] = { -1.797693134e308, -2.22507e-308, 0, 2.22507e-308, 1.797693134e308 };
    const int bufsz = (sizeof(char) + sizeof(short) + sizeof(int) + sizeof(long) + sizeof(long long)) * 2 + sizeof(float) + sizeof(double);

    test_typ test;
    TS_ASSERT_EQUALS(bufsz, test.size());

    dirty_checker checker(&test);

    unsigned char buf[bufsz];
    for (int idx = 0; idx < 5; ++idx)
    {
        unsigned char *pbuf = buf;
        pbuf = tmemcpy(vch[idx], pbuf);
        pbuf = tmemcpy(vsh[idx], pbuf);
        pbuf = tmemcpy(vit[idx], pbuf);
        pbuf = tmemcpy(vlg[idx], pbuf);
        pbuf = tmemcpy(vll[idx], pbuf);
        pbuf = tmemcpy(uch[idx], pbuf);
        pbuf = tmemcpy(ush[idx], pbuf);
        pbuf = tmemcpy(uit[idx], pbuf);
        pbuf = tmemcpy(ulg[idx], pbuf);
        pbuf = tmemcpy(ull[idx], pbuf);
        pbuf = tmemcpy(flt[idx], pbuf);
        pbuf = tmemcpy(dbl[idx], pbuf);
        test.extract(sizeof(buf), buf);
        TS_ASSERT(vch[idx] == test.vch);
        TS_ASSERT(vsh[idx] == test.vsh);
        TS_ASSERT(vit[idx] == test.vit);
        TS_ASSERT(vlg[idx] == test.vlg);
        TS_ASSERT(vll[idx] == test.vll);
        TS_ASSERT(uch[idx] == test.uch);
        TS_ASSERT(ush[idx] == test.ush);
        TS_ASSERT(uit[idx] == test.uit);
        TS_ASSERT(ulg[idx] == test.ulg);
        TS_ASSERT(ull[idx] == test.ull);
        TS_ASSERT(flt[idx] == test.flt);
        TS_ASSERT(dbl[idx] == test.dbl);
        TS_ASSERT_EQUALS(checker.getStateAndReset(), 1);
    }
    test.extract(sizeof(buf), buf);
    TS_ASSERT_EQUALS(checker.getStateAndReset(), 2);

    rtest_typ rtest;
    TS_ASSERT_EQUALS(bufsz, rtest.size());

    dirty_checker rchecker(&rtest);
    for (int idx = 0; idx < 5; ++idx)
    {
        unsigned char *pbuf = buf;
        pbuf = rmemcpy(vch[idx], pbuf);
        pbuf = rmemcpy(vsh[idx], pbuf);
        pbuf = rmemcpy(vit[idx], pbuf);
        pbuf = rmemcpy(vlg[idx], pbuf);
        pbuf = rmemcpy(vll[idx], pbuf);
        pbuf = rmemcpy(uch[idx], pbuf);
        pbuf = rmemcpy(ush[idx], pbuf);
        pbuf = rmemcpy(uit[idx], pbuf);
        pbuf = rmemcpy(ulg[idx], pbuf);
        pbuf = rmemcpy(ull[idx], pbuf);
        pbuf = rmemcpy(flt[idx], pbuf);
        pbuf = rmemcpy(dbl[idx], pbuf);
        rtest.extract(sizeof(buf), buf);
        TS_ASSERT(vch[idx] == rtest.vch);
        TS_ASSERT(vsh[idx] == rtest.vsh);
        TS_ASSERT(vit[idx] == rtest.vit);
        TS_ASSERT(vlg[idx] == rtest.vlg);
        TS_ASSERT(vll[idx] == rtest.vll);
        TS_ASSERT(uch[idx] == rtest.uch);
        TS_ASSERT(ush[idx] == rtest.ush);
        TS_ASSERT(uit[idx] == rtest.uit);
        TS_ASSERT(ulg[idx] == rtest.ulg);
        TS_ASSERT(ull[idx] == rtest.ull);
        TS_ASSERT(flt[idx] == rtest.flt);
        TS_ASSERT(dbl[idx] == rtest.dbl);
        TS_ASSERT_EQUALS(rchecker.getStateAndReset(), 1);
    }
    rtest.extract(sizeof(buf), buf);
    TS_ASSERT_EQUALS(rchecker.getStateAndReset(), 2);
}

void GenVarTestSuite::testOutput(void)
{
    char  vch[] = { -128, -1, 0, 1, 127 };
    short vsh[] = { -32768, -1, 0, 1, 32767 };
    int   vit[] = { -2147483647, -1, 0, 1, 2147483647 };
    long  vlg[] = { -2147483647, -1, 0, 1, 2147483647 };
    long long vll[] = { -9223372036854775807LL, -1, 0, 1, 9223372036854775807LL };
    unsigned char  uch[] = { 0, 1, 127, 128, 255 };
    unsigned short ush[] = { 0, 1, 32767, 32768, 65535 };
    unsigned int   uit[] = { 0, 1, 2147483647, 2147483648, 4294967295 };
    unsigned long  ulg[] = { 0, 1, 2147483647, 2147483648, 4294967295 };
    unsigned long long ull[] = { 0, 1, 9223372036854775807LL, 9223372036854775808UL, 18446744073709551615UL };
    float  flt[] = { -3.4028e19F, -1.1755e-19F, 0, 1.1755e-19F, 3.4028e19F };
    double dbl[] = { -1.797693134e308, -2.22507e-308, 0, 2.22507e-308, 1.797693134e308 };
    const int bufsz = (sizeof(char) + sizeof(short) + sizeof(int) + sizeof(long) + sizeof(long long)) * 2 + sizeof(float) + sizeof(double);

    test_typ test;
    TS_ASSERT_EQUALS(bufsz, test.size());

    unsigned char buf1[bufsz], buf2[bufsz];
    for (int idx = 0; idx < 5; ++idx)
    {
        unsigned char *pbuf = buf1;
        pbuf = tmemcpy(vch[idx], pbuf);
        pbuf = tmemcpy(vsh[idx], pbuf);
        pbuf = tmemcpy(vit[idx], pbuf);
        pbuf = tmemcpy(vlg[idx], pbuf);
        pbuf = tmemcpy(vll[idx], pbuf);
        pbuf = tmemcpy(uch[idx], pbuf);
        pbuf = tmemcpy(ush[idx], pbuf);
        pbuf = tmemcpy(uit[idx], pbuf);
        pbuf = tmemcpy(ulg[idx], pbuf);
        pbuf = tmemcpy(ull[idx], pbuf);
        pbuf = tmemcpy(flt[idx], pbuf);
        pbuf = tmemcpy(dbl[idx], pbuf);
        test.vch = vch[idx];
        test.vsh = vsh[idx];
        test.vit = vit[idx];
        test.vlg = vlg[idx];
        test.vll = vll[idx];
        test.uch = uch[idx];
        test.ush = ush[idx];
        test.uit = uit[idx];
        test.ulg = ulg[idx];
        test.ull = ull[idx];
        test.flt = flt[idx];
        test.dbl = dbl[idx];
        outbuf ob;
        ob.set(buf2, bufsz);
        test.output(ob);
        TS_ASSERT_SAME_DATA(buf1, buf2, bufsz);
    }

    rtest_typ rtest;
    TS_ASSERT_EQUALS(bufsz, rtest.size());

    for (int idx = 0; idx < 5; ++idx)
    {
        unsigned char *pbuf = buf1;
        pbuf = rmemcpy(vch[idx], pbuf);
        pbuf = rmemcpy(vsh[idx], pbuf);
        pbuf = rmemcpy(vit[idx], pbuf);
        pbuf = rmemcpy(vlg[idx], pbuf);
        pbuf = rmemcpy(vll[idx], pbuf);
        pbuf = rmemcpy(uch[idx], pbuf);
        pbuf = rmemcpy(ush[idx], pbuf);
        pbuf = rmemcpy(uit[idx], pbuf);
        pbuf = rmemcpy(ulg[idx], pbuf);
        pbuf = rmemcpy(ull[idx], pbuf);
        pbuf = rmemcpy(flt[idx], pbuf);
        pbuf = rmemcpy(dbl[idx], pbuf);
        rtest.vch = vch[idx];
        rtest.vsh = vsh[idx];
        rtest.vit = vit[idx];
        rtest.vlg = vlg[idx];
        rtest.vll = vll[idx];
        rtest.uch = uch[idx];
        rtest.ush = ush[idx];
        rtest.uit = uit[idx];
        rtest.ulg = ulg[idx];
        rtest.ull = ull[idx];
        rtest.flt = flt[idx];
        rtest.dbl = dbl[idx];
        outbuf ob;
        ob.set(buf2, bufsz);
        rtest.output(ob);
        TS_ASSERT_SAME_DATA(buf1, buf2, bufsz);
    }
}

void GenVarTestSuite::testStream(void)
{
    test_typ test, test2;

    // simple value within range, note \x20 is a space which can be nasty
    test.vch = '\x20';
    test.vsh = 32;
    test.vit = 32;
    test.vlg = 32L;
    test.vll = 32LL;
    test.uch = '\x20';
    test.ush = 32U;
    test.uit = 32U;
    test.ulg = 32UL;
    test.ull = 32ULL;
    test.flt = 32.f;
    test.dbl = 32.;

    std::ostringstream oss;
    oss << test;
    std::string data = oss.str();
    TS_ASSERT_SAME_DATA(data.data(), "32 32 32 32 32 32 32 32 32 32 32 32", data.size());

    {
        std::istringstream iss(data);
        iss >> test2;
    }
    TS_ASSERT(test == test2);

    // simple negative values (for those that can)
    test.vch = -32;
    test.vsh = -32;
    test.vit = -32;
    test.vlg = -32L;
    test.vll = -32LL;
    test.uch = '\x0';
    test.ush = 0;
    test.uit = 0;
    test.ulg = 0;
    test.ull = 0;
    test.flt = -32.f;
    test.dbl = -32.;

    oss.str(std::string());
    oss << test;
    data = oss.str();
    TS_ASSERT_SAME_DATA(data.data(), "-32 -32 -32 -32 -32 0 0 0 0 0 -32 -32", data.size());

    {
        std::istringstream iss(data);
        iss >> test2;
    }
    TS_ASSERT(test == test2);

    // near limits
    test.vch = '\x7E';
    test.vsh = 32766;
    test.vit = 2147483646;
    test.vlg = 2147483646L;
    test.vll = 9223372036854775806LL;
    test.uch = '\xFE';
    test.ush = 65534U;
    test.uit = 4294967294U;
    test.ulg = 4294967294UL;
    test.ull = 18446744073709551614ULL;
    test.flt = 3.4028e19F;
    test.dbl = 1.797693134e308;

    oss.str(std::string());
    value_var::set_float_format("%.8g");
    oss << test;
    data = oss.str();
    TS_ASSERT_SAME_DATA(data.data(), "126 32766 2147483646 2147483646 9223372036854775806 254 65534 4294967294 4294967294 18446744073709551614 3.4028e+19 1.7976931e+308", data.size());

    {
        std::istringstream iss(data);
        iss >> test2;
    }
    data = "1.797693134e+308";
    {
        std::istringstream iss(data);
        iss >> test2.dbl;
    }
    TS_ASSERT(test == test2);

    // near lower bound limits
    test.vch = -126;
    test.vsh = -32766;
    test.vit = -2147483646;
    test.vlg = -2147483646L;
    test.vll = -9223372036854775806LL;
    test.uch = 1U;
    test.ush = 1U;
    test.uit = 1U;
    test.ulg = 1UL;
    test.ull = 1ULL;
    test.flt = -3.4028e19F;
    test.dbl = -1.797693134e308;

    oss.str(std::string());
    oss << test;
    data = oss.str();
    TS_ASSERT_SAME_DATA(data.data(), "-126 -32766 -2147483646 -2147483646 -9223372036854775806 1 1 1 1 1 -3.4028e+19 -1.7976931e+308", data.size());

    {
        std::istringstream iss(data);
        iss >> test2;
    }
    data = "-1.797693134e+308";
    {
        std::istringstream iss(data);
        iss >> test2.dbl;
    }
    TS_ASSERT(test == test2);

    // simple fractions
    test.flt = 0.5f;
    test.dbl = 0.5;

    oss.str(std::string());
    oss << test.flt << ' ' << test.dbl;
    data = oss.str();
    TS_ASSERT_SAME_DATA(data.data(), "0.5 0.5", data.size());
    {
        std::istringstream iss(data);
        iss >> test2.flt >> test2.dbl;
    }
    TS_ASSERT(test == test2);

    test.flt = -0.1f;
    test.dbl = -0.1;

    oss.str(std::string());
    oss << test.flt << ' ' << test.dbl;
    data = oss.str();
    TS_ASSERT_SAME_DATA(data.data(), "-0.1 -0.1", data.size());
    {
        std::istringstream iss(data);
        iss >> test2.flt >> test2.dbl;
    }
    TS_ASSERT(test == test2);

    // really small numbers
    test.flt = 1.1755e-19F;
    test.dbl = 2.22507e-308;
    oss.str(std::string());
    oss << test.flt << ' ' << test.dbl;
    data = oss.str();
    TS_ASSERT_SAME_DATA(data.data(), "1.1755e-19 2.22507e-308", data.size());
    {
        std::istringstream iss(data);
        iss >> test2.flt >> test2.dbl;
    }
    TS_ASSERT(test == test2);

    // invalid
    test.setInvalid();

    oss.str(std::string());
    oss << test;
    data = oss.str();
    TS_ASSERT_SAME_DATA(data.data(), "-1 -1 -1 -1 -1 255 65535 4294967295 4294967295 18446744073709551615 N N", data.size());

    {
        std::istringstream iss(data);
        iss >> test2;
    }
    TS_ASSERT(!test2.flt.isValid());
    TS_ASSERT(!test2.dbl.isValid());
    TS_ASSERT(!test2.isValid());
}

void GenVarTestSuite::testToValue(void)
{
    // 1. From float/double to ...

    // Integers with rounding
    generic_var<double> dbl;
    dbl = 3.5;
    TS_ASSERT_EQUALS(dbl.to_unsigned_char(), '\4');
    TS_ASSERT_EQUALS(dbl.to_char(), '\4');
    TS_ASSERT_EQUALS(dbl.to_unsigned_int(), 4U);
    TS_ASSERT_EQUALS(dbl.to_int(), 4);
    TS_ASSERT_EQUALS(dbl.to_unsigned_long(), 4UL);
    TS_ASSERT_EQUALS(dbl.to_long(), 4L);
    TS_ASSERT_SAME_DATA(dbl.to_const_char_ptr(), "3.5", 4);
    TS_ASSERT_SAME_DATA(dbl.to_char_ptr(), "3.5", 4);
    dbl = 3.4;
    TS_ASSERT_EQUALS(dbl.to_unsigned_char(), '\3');
    TS_ASSERT_EQUALS(dbl.to_char(), '\3');
    TS_ASSERT_EQUALS(dbl.to_unsigned_int(), 3U);
    TS_ASSERT_EQUALS(dbl.to_int(), 3);
    TS_ASSERT_EQUALS(dbl.to_unsigned_long(), 3UL);
    TS_ASSERT_EQUALS(dbl.to_long(), 3L);
    TS_ASSERT_SAME_DATA(dbl.to_const_char_ptr(), "3.4", 4);
    TS_ASSERT_SAME_DATA(dbl.to_char_ptr(), "3.4", 4);
    dbl = -3.5;
    TS_ASSERT_EQUALS(dbl.to_char(), -4);
    TS_ASSERT_EQUALS(dbl.to_int(), -4);
    TS_ASSERT_EQUALS(dbl.to_long(), -4L);
    TS_ASSERT_SAME_DATA(dbl.to_const_char_ptr(), "-3.5", 5);
    TS_ASSERT_SAME_DATA(dbl.to_char_ptr(), "-3.5", 5);
    dbl = -3.4;
    TS_ASSERT_EQUALS(dbl.to_char(), -3);
    TS_ASSERT_EQUALS(dbl.to_int(), -3);
    TS_ASSERT_EQUALS(dbl.to_long(), -3L);
    TS_ASSERT_SAME_DATA(dbl.to_const_char_ptr(), "-3.4", 5);
    TS_ASSERT_SAME_DATA(dbl.to_char_ptr(), "-3.4", 5);

    generic_var<float> flt;
    flt = 3.5f;
    TS_ASSERT_EQUALS(flt.to_unsigned_char(), '\4');
    TS_ASSERT_EQUALS(flt.to_char(), '\4');
    TS_ASSERT_EQUALS(flt.to_unsigned_int(), 4U);
    TS_ASSERT_EQUALS(flt.to_int(), 4);
    TS_ASSERT_EQUALS(flt.to_unsigned_long(), 4UL);
    TS_ASSERT_EQUALS(flt.to_long(), 4L);
    TS_ASSERT_SAME_DATA(flt.to_const_char_ptr(), "3.5", 4);
    TS_ASSERT_SAME_DATA(flt.to_char_ptr(), "3.5", 4);
    flt = 3.4f;
    TS_ASSERT_EQUALS(flt.to_unsigned_char(), '\3');
    TS_ASSERT_EQUALS(flt.to_char(), '\3');
    TS_ASSERT_EQUALS(flt.to_unsigned_int(), 3U);
    TS_ASSERT_EQUALS(flt.to_int(), 3);
    TS_ASSERT_EQUALS(flt.to_unsigned_long(), 3UL);
    TS_ASSERT_EQUALS(flt.to_long(), 3L);
    TS_ASSERT_SAME_DATA(flt.to_const_char_ptr(), "3.4000001", 10);
    TS_ASSERT_SAME_DATA(flt.to_char_ptr(), "3.4000001", 10);
    flt = -3.5f;
    TS_ASSERT_EQUALS(flt.to_char(), -4);
    TS_ASSERT_EQUALS(flt.to_int(), -4);
    TS_ASSERT_EQUALS(flt.to_long(), -4L);
    TS_ASSERT_SAME_DATA(flt.to_const_char_ptr(), "-3.5", 5);
    TS_ASSERT_SAME_DATA(flt.to_char_ptr(), "-3.5", 5);
    flt = -3.4f;
    TS_ASSERT_EQUALS(flt.to_char(), -3);
    TS_ASSERT_EQUALS(flt.to_int(), -3);
    TS_ASSERT_EQUALS(flt.to_long(), -3L);
    TS_ASSERT_SAME_DATA(flt.to_const_char_ptr(), "-3.4000001", 11); // %.8g
    TS_ASSERT_SAME_DATA(flt.to_char_ptr(), "-3.4000001", 11);

    // Negative to unsigned integers
    TS_ASSERT_EQUALS(dbl.to_unsigned_char(), 0);
    TS_ASSERT_EQUALS(dbl.to_unsigned_short(), 0);
    TS_ASSERT_EQUALS(dbl.to_unsigned_int(), 0);
    TS_ASSERT_EQUALS(dbl.to_unsigned_long(), 0);
    TS_ASSERT_EQUALS(dbl.to_unsigned_long_long(), 0);

    TS_ASSERT_EQUALS(flt.to_unsigned_char(), 0);
    TS_ASSERT_EQUALS(flt.to_unsigned_short(), 0);
    TS_ASSERT_EQUALS(flt.to_unsigned_int(), 0);
    TS_ASSERT_EQUALS(flt.to_unsigned_long(), 0);
    TS_ASSERT_EQUALS(flt.to_unsigned_long_long(), 0);

    // Integers with overflow
    dbl = 2e19;
    TS_ASSERT_EQUALS(dbl.to_char(), CHAR_MAX);
    TS_ASSERT_EQUALS(dbl.to_unsigned_char(), UCHAR_MAX);
    TS_ASSERT_EQUALS(dbl.to_short(), SHRT_MAX);
    TS_ASSERT_EQUALS(dbl.to_unsigned_short(), USHRT_MAX);
    TS_ASSERT_EQUALS(dbl.to_int(), LONG_MAX);
    TS_ASSERT_EQUALS(dbl.to_unsigned_int(), ULONG_MAX);
    TS_ASSERT_EQUALS(dbl.to_long(), LONG_MAX);
    TS_ASSERT_EQUALS(dbl.to_unsigned_long(), ULONG_MAX);
    TS_ASSERT_EQUALS(dbl.to_long_long(), LLONG_MAX);
    TS_ASSERT_EQUALS(dbl.to_unsigned_long_long(), ULLONG_MAX);
    TS_ASSERT_SAME_DATA(dbl.to_const_char_ptr(), "2e+19", 6);
    TS_ASSERT_SAME_DATA(dbl.to_char_ptr(), "2e+19", 6);

    flt = 2e19f;
    TS_ASSERT_EQUALS(flt.to_char(), CHAR_MAX);
    TS_ASSERT_EQUALS(flt.to_unsigned_char(), UCHAR_MAX);
    TS_ASSERT_EQUALS(flt.to_short(), SHRT_MAX);
    TS_ASSERT_EQUALS(flt.to_unsigned_short(), USHRT_MAX);
    TS_ASSERT_EQUALS(flt.to_int(), LONG_MAX);
    TS_ASSERT_EQUALS(flt.to_unsigned_int(), ULONG_MAX);
    TS_ASSERT_EQUALS(flt.to_long(), LONG_MAX);
    TS_ASSERT_EQUALS(flt.to_unsigned_long(), ULONG_MAX);
    TS_ASSERT_EQUALS(flt.to_long_long(), LLONG_MAX);
    TS_ASSERT_EQUALS(flt.to_unsigned_long_long(), ULLONG_MAX);
    TS_ASSERT_SAME_DATA(flt.to_const_char_ptr(), "2e+19", 6);
    TS_ASSERT_SAME_DATA(flt.to_char_ptr(), "2e+19", 6);

    dbl = 128.;
    TS_ASSERT_EQUALS(dbl.to_char(), CHAR_MAX);
    TS_ASSERT_EQUALS(dbl.to_unsigned_char(), 128U);
    dbl = 35000.;
    TS_ASSERT_EQUALS(dbl.to_short(), SHRT_MAX);
    TS_ASSERT_EQUALS(dbl.to_unsigned_short(), 35000U);
    dbl = 2200000000.;
    TS_ASSERT_EQUALS(dbl.to_int(), LONG_MAX);
    TS_ASSERT_EQUALS(dbl.to_unsigned_int(), 2200000000U);

    // Integers with underflow
    dbl = -2e19;
    TS_ASSERT_EQUALS(dbl.to_char(), CHAR_MIN);
    TS_ASSERT_EQUALS(dbl.to_unsigned_char(), 0);
    TS_ASSERT_EQUALS(dbl.to_short(), SHRT_MIN);
    TS_ASSERT_EQUALS(dbl.to_unsigned_short(), 0);
    TS_ASSERT_EQUALS(dbl.to_int(), LONG_MIN);
    TS_ASSERT_EQUALS(dbl.to_unsigned_int(), 0);
    TS_ASSERT_EQUALS(dbl.to_long(), LONG_MIN);
    TS_ASSERT_EQUALS(dbl.to_unsigned_long(), 0);
    TS_ASSERT_EQUALS(dbl.to_long_long(), LLONG_MIN);
    TS_ASSERT_EQUALS(dbl.to_unsigned_long_long(), 0);

    flt = -2e19f;
    TS_ASSERT_EQUALS(flt.to_char(), CHAR_MIN);
    TS_ASSERT_EQUALS(flt.to_unsigned_char(), 0);
    TS_ASSERT_EQUALS(flt.to_short(), SHRT_MIN);
    TS_ASSERT_EQUALS(flt.to_unsigned_short(), 0);
    TS_ASSERT_EQUALS(flt.to_int(), LONG_MIN);
    TS_ASSERT_EQUALS(flt.to_unsigned_int(), 0);
    TS_ASSERT_EQUALS(flt.to_long(), LONG_MIN);
    TS_ASSERT_EQUALS(flt.to_unsigned_long(), 0);
    TS_ASSERT_EQUALS(flt.to_long_long(), LLONG_MIN);
    TS_ASSERT_EQUALS(flt.to_unsigned_long_long(), 0);

    // Double/float
    TS_ASSERT_EQUALS(dbl.to_double(), -2e19);
    TS_ASSERT_EQUALS(dbl.to_float(), -2e19f);
    TS_ASSERT_EQUALS(flt.to_double(), -2e19f); // there will be some imprecision
    TS_ASSERT_EQUALS(flt.to_float(), -2e19f);

    // Double to float with overflow and underflow
    dbl = -3.8928e39;
    TS_ASSERT_EQUALS(dbl.to_float(), -FLT_MAX);
    dbl = 3.8928e39;
    TS_ASSERT_EQUALS(dbl.to_float(), FLT_MAX);

    // 2. From unsigned integers to ...
    generic_var<unsigned char> uch;
    generic_var<unsigned short> ush;
    generic_var<unsigned int> uit;
    generic_var<unsigned long> ulg;
    generic_var<unsigned long long> ull;

    // Smaller size integers (within range)
    uch = 120U;
    ush = 120U;
    uit = 120U;
    ulg = 120UL;
    ull = 120ULL;
    TS_ASSERT_EQUALS(uch.to_char(), 120);
    TS_ASSERT_EQUALS(ush.to_char(), 120);
    TS_ASSERT_EQUALS(ush.to_unsigned_char(), 120U);
    TS_ASSERT_EQUALS(ush.to_short(), 120);
    TS_ASSERT_EQUALS(uit.to_char(), 120);
    TS_ASSERT_EQUALS(uit.to_unsigned_char(), 120U);
    TS_ASSERT_EQUALS(uit.to_short(), 120);
    TS_ASSERT_EQUALS(uit.to_unsigned_short(), 120U);
    TS_ASSERT_EQUALS(uit.to_int(), 120);
    TS_ASSERT_EQUALS(uit.to_long(), 120);
    TS_ASSERT_EQUALS(uit.to_unsigned_long(), 120U);
    TS_ASSERT_EQUALS(ulg.to_char(), 120);
    TS_ASSERT_EQUALS(ulg.to_unsigned_char(), 120U);
    TS_ASSERT_EQUALS(ulg.to_short(), 120);
    TS_ASSERT_EQUALS(ulg.to_unsigned_short(), 120U);
    TS_ASSERT_EQUALS(ulg.to_int(), 120);
    TS_ASSERT_EQUALS(ulg.to_unsigned_int(), 120U);
    TS_ASSERT_EQUALS(ulg.to_long(), 120);
    TS_ASSERT_EQUALS(ull.to_char(), 120);
    TS_ASSERT_EQUALS(ull.to_unsigned_char(), 120U);
    TS_ASSERT_EQUALS(ull.to_short(), 120);
    TS_ASSERT_EQUALS(ull.to_unsigned_short(), 120U);
    TS_ASSERT_EQUALS(ull.to_int(), 120);
    TS_ASSERT_EQUALS(ull.to_unsigned_int(), 120U);
    TS_ASSERT_EQUALS(ull.to_long(), 120);
    TS_ASSERT_EQUALS(ull.to_unsigned_long(), 120U);
    TS_ASSERT_EQUALS(ull.to_long_long(), 120);

    TS_ASSERT_SAME_DATA(uch.to_const_char_ptr(), "120", 4);
    TS_ASSERT_SAME_DATA(uch.to_char_ptr(), "120", 4);
    TS_ASSERT_SAME_DATA(ush.to_const_char_ptr(), "120", 4);
    TS_ASSERT_SAME_DATA(ush.to_char_ptr(), "120", 4);
    TS_ASSERT_SAME_DATA(uit.to_const_char_ptr(), "120", 4);
    TS_ASSERT_SAME_DATA(uit.to_char_ptr(), "120", 4);
    TS_ASSERT_SAME_DATA(ulg.to_const_char_ptr(), "120", 4);
    TS_ASSERT_SAME_DATA(ulg.to_char_ptr(), "120", 4);
    TS_ASSERT_SAME_DATA(ull.to_const_char_ptr(), "120", 4);
    TS_ASSERT_SAME_DATA(ull.to_char_ptr(), "120", 4);

    // Smaller size integers (overflow)
    uch = 200U;
    ush = 33000U;
    uit = 2200000000U;
    ulg = 2200000000UL;
    ull = 9300000000000000000ULL;
    TS_ASSERT_EQUALS(uch.to_char(), CHAR_MAX);

    TS_ASSERT_EQUALS(ush.to_char(), CHAR_MAX);
    TS_ASSERT_EQUALS(ush.to_unsigned_char(), UCHAR_MAX);
    TS_ASSERT_EQUALS(ush.to_short(), SHRT_MAX);

    TS_ASSERT_EQUALS(uit.to_char(), CHAR_MAX);
    TS_ASSERT_EQUALS(uit.to_unsigned_char(), UCHAR_MAX);
    TS_ASSERT_EQUALS(uit.to_short(), SHRT_MAX);
    TS_ASSERT_EQUALS(uit.to_unsigned_short(), USHRT_MAX);
    TS_ASSERT_EQUALS(uit.to_int(), LONG_MAX);
    TS_ASSERT_EQUALS(uit.to_long(), LONG_MAX);

    TS_ASSERT_EQUALS(ulg.to_char(), CHAR_MAX);
    TS_ASSERT_EQUALS(ulg.to_unsigned_char(), UCHAR_MAX);
    TS_ASSERT_EQUALS(ulg.to_short(), SHRT_MAX);
    TS_ASSERT_EQUALS(ulg.to_unsigned_short(), USHRT_MAX);
    TS_ASSERT_EQUALS(ulg.to_int(), LONG_MAX);
    TS_ASSERT_EQUALS(ulg.to_long(), LONG_MAX);

    TS_ASSERT_EQUALS(ull.to_char(), CHAR_MAX);
    TS_ASSERT_EQUALS(ull.to_unsigned_char(), UCHAR_MAX);
    TS_ASSERT_EQUALS(ull.to_short(), SHRT_MAX);
    TS_ASSERT_EQUALS(ull.to_unsigned_short(), USHRT_MAX);
    TS_ASSERT_EQUALS(ull.to_int(), LONG_MAX);
    TS_ASSERT_EQUALS(ull.to_unsigned_int(), ULONG_MAX);
    TS_ASSERT_EQUALS(ull.to_long(), LONG_MAX);
    TS_ASSERT_EQUALS(ull.to_unsigned_long(), ULONG_MAX);
    TS_ASSERT_EQUALS(ull.to_long_long(), LLONG_MAX);

    // There is no overflow because min value is 0

    // Same type
    TS_ASSERT_EQUALS(uch.to_unsigned_char(), 200U);
    TS_ASSERT_EQUALS(ush.to_unsigned_short(), 33000U);
    TS_ASSERT_EQUALS(uit.to_unsigned_int(), 2200000000U);
    TS_ASSERT_EQUALS(ulg.to_unsigned_long(), 2200000000UL);
    TS_ASSERT_EQUALS(ull.to_unsigned_long_long(), 9300000000000000000ULL);

    // Larger size integers
    TS_ASSERT_EQUALS(uch.to_short(), 200);
    TS_ASSERT_EQUALS(uch.to_unsigned_short(), 200U);
    TS_ASSERT_EQUALS(uch.to_int(), 200);
    TS_ASSERT_EQUALS(uch.to_unsigned_int(), 200U);
    TS_ASSERT_EQUALS(uch.to_long(), 200);
    TS_ASSERT_EQUALS(uch.to_unsigned_long(), 200U);
    TS_ASSERT_EQUALS(uch.to_long_long(), 200);
    TS_ASSERT_EQUALS(uch.to_unsigned_long_long(), 200U);

    TS_ASSERT_EQUALS(ush.to_int(), 33000);
    TS_ASSERT_EQUALS(ush.to_unsigned_int(), 33000U);
    TS_ASSERT_EQUALS(ush.to_long(), 33000);
    TS_ASSERT_EQUALS(ush.to_unsigned_long(), 33000U);
    TS_ASSERT_EQUALS(ush.to_long_long(), 33000);
    TS_ASSERT_EQUALS(ush.to_unsigned_long_long(), 33000U);

    TS_ASSERT_EQUALS(uit.to_long_long(), 2200000000);
    TS_ASSERT_EQUALS(uit.to_unsigned_long_long(), 2200000000U);

    TS_ASSERT_EQUALS(ulg.to_long_long(), 2200000000);
    TS_ASSERT_EQUALS(ulg.to_unsigned_long_long(), 2200000000U);

    // Floats
    TS_ASSERT_EQUALS(uch.to_float(), 200.);
    TS_ASSERT_EQUALS(uch.to_double(), 200.);
    TS_ASSERT_EQUALS(ush.to_float(), 33000.);
    TS_ASSERT_EQUALS(ush.to_double(), 33000.);
    TS_ASSERT_EQUALS(uit.to_float(), 2200000000.);
    TS_ASSERT_EQUALS(uit.to_double(), 2200000000.);
    TS_ASSERT_EQUALS(ulg.to_float(), 2200000000.);
    TS_ASSERT_EQUALS(ulg.to_double(), 2200000000.);
    TS_ASSERT_DELTA(ull.to_float(), 9300000000000000000.f, 1.e+12f);
    TS_ASSERT_EQUALS(ull.to_double(), 9300000000000000000.);

    // 3. From signed integers
    generic_var<char> sch;
    generic_var<short> ssh;
    generic_var<int> sit;
    generic_var<long> slg;
    generic_var<long long> sll;

    // Smaller size integers (within range)
    sch = 125;
    ssh = 125;
    sit = 125;
    slg = 125;
    sll = 125;
    TS_ASSERT_EQUALS(sch.to_char(), 125);
    TS_ASSERT_EQUALS(ssh.to_char(), 125);
    TS_ASSERT_EQUALS(ssh.to_unsigned_char(), 125U);
    TS_ASSERT_EQUALS(ssh.to_short(), 125);
    TS_ASSERT_EQUALS(sit.to_char(), 125);
    TS_ASSERT_EQUALS(sit.to_unsigned_char(), 125U);
    TS_ASSERT_EQUALS(sit.to_short(), 125);
    TS_ASSERT_EQUALS(sit.to_unsigned_short(), 125U);
    TS_ASSERT_EQUALS(sit.to_int(), 125);
    TS_ASSERT_EQUALS(sit.to_long(), 125);
    TS_ASSERT_EQUALS(sit.to_unsigned_long(), 125U);
    TS_ASSERT_EQUALS(slg.to_char(), 125);
    TS_ASSERT_EQUALS(slg.to_unsigned_char(), 125U);
    TS_ASSERT_EQUALS(slg.to_short(), 125);
    TS_ASSERT_EQUALS(slg.to_unsigned_short(), 125U);
    TS_ASSERT_EQUALS(slg.to_int(), 125);
    TS_ASSERT_EQUALS(slg.to_unsigned_int(), 125U);
    TS_ASSERT_EQUALS(slg.to_long(), 125);
    TS_ASSERT_EQUALS(sll.to_char(), 125);
    TS_ASSERT_EQUALS(sll.to_unsigned_char(), 125U);
    TS_ASSERT_EQUALS(sll.to_short(), 125);
    TS_ASSERT_EQUALS(sll.to_unsigned_short(), 125U);
    TS_ASSERT_EQUALS(sll.to_int(), 125);
    TS_ASSERT_EQUALS(sll.to_unsigned_int(), 125U);
    TS_ASSERT_EQUALS(sll.to_long(), 125);
    TS_ASSERT_EQUALS(sll.to_unsigned_long(), 125U);
    TS_ASSERT_EQUALS(sll.to_long_long(), 125);

    TS_ASSERT_SAME_DATA(sch.to_const_char_ptr(), "125", 4);
    TS_ASSERT_SAME_DATA(sch.to_char_ptr(), "125", 4);
    TS_ASSERT_SAME_DATA(ssh.to_const_char_ptr(), "125", 4);
    TS_ASSERT_SAME_DATA(ssh.to_char_ptr(), "125", 4);
    TS_ASSERT_SAME_DATA(sit.to_const_char_ptr(), "125", 4);
    TS_ASSERT_SAME_DATA(sit.to_char_ptr(), "125", 4);
    TS_ASSERT_SAME_DATA(slg.to_const_char_ptr(), "125", 4);
    TS_ASSERT_SAME_DATA(slg.to_char_ptr(), "125", 4);
    TS_ASSERT_SAME_DATA(sll.to_const_char_ptr(), "125", 4);
    TS_ASSERT_SAME_DATA(sll.to_char_ptr(), "125", 4);

    // Smaller size integers (overflow)
    sch = 126;
    ssh = 32000;
    sit = 2100000000;
    slg = 2100000000L;
    sll = 9200000000000000000LL;
    TS_ASSERT_EQUALS(ssh.to_char(), CHAR_MAX);
    TS_ASSERT_EQUALS(ssh.to_unsigned_char(), UCHAR_MAX);

    TS_ASSERT_EQUALS(sit.to_char(), CHAR_MAX);
    TS_ASSERT_EQUALS(sit.to_unsigned_char(), UCHAR_MAX);
    TS_ASSERT_EQUALS(sit.to_short(), SHRT_MAX);
    TS_ASSERT_EQUALS(sit.to_unsigned_short(), USHRT_MAX);

    TS_ASSERT_EQUALS(slg.to_char(), CHAR_MAX);
    TS_ASSERT_EQUALS(slg.to_unsigned_char(), UCHAR_MAX);
    TS_ASSERT_EQUALS(slg.to_short(), SHRT_MAX);
    TS_ASSERT_EQUALS(slg.to_unsigned_short(), USHRT_MAX);

    TS_ASSERT_EQUALS(sll.to_char(), CHAR_MAX);
    TS_ASSERT_EQUALS(sll.to_unsigned_char(), UCHAR_MAX);
    TS_ASSERT_EQUALS(sll.to_short(), SHRT_MAX);
    TS_ASSERT_EQUALS(sll.to_unsigned_short(), USHRT_MAX);
    TS_ASSERT_EQUALS(sll.to_int(), LONG_MAX);
    TS_ASSERT_EQUALS(sll.to_unsigned_int(), ULONG_MAX);
    TS_ASSERT_EQUALS(sll.to_long(), LONG_MAX);
    TS_ASSERT_EQUALS(sll.to_unsigned_long(), ULONG_MAX);

    // Unsigned integers (underflow)
    sch = -126;
    ssh = -32000;
    sit = -2100000000;
    slg = -2100000000;
    sll = -9200000000000000000LL;
    TS_ASSERT_EQUALS(sch.to_unsigned_char(), 0);
    TS_ASSERT_EQUALS(sch.to_unsigned_short(), 0);
    TS_ASSERT_EQUALS(sch.to_unsigned_int(), 0);
    TS_ASSERT_EQUALS(sch.to_unsigned_long(), 0);
    TS_ASSERT_EQUALS(sch.to_unsigned_long_long(), 0);

    TS_ASSERT_EQUALS(ssh.to_unsigned_char(), 0);
    TS_ASSERT_EQUALS(ssh.to_unsigned_short(), 0);
    TS_ASSERT_EQUALS(ssh.to_unsigned_int(), 0);
    TS_ASSERT_EQUALS(ssh.to_unsigned_long(), 0);
    TS_ASSERT_EQUALS(ssh.to_unsigned_long_long(), 0);

    TS_ASSERT_EQUALS(sit.to_unsigned_char(), 0);
    TS_ASSERT_EQUALS(sit.to_unsigned_short(), 0);
    TS_ASSERT_EQUALS(sit.to_unsigned_int(), 0);
    TS_ASSERT_EQUALS(sit.to_unsigned_long(), 0);
    TS_ASSERT_EQUALS(sit.to_unsigned_long_long(), 0);

    TS_ASSERT_EQUALS(slg.to_unsigned_char(), 0);
    TS_ASSERT_EQUALS(slg.to_unsigned_short(), 0);
    TS_ASSERT_EQUALS(slg.to_unsigned_int(), 0);
    TS_ASSERT_EQUALS(slg.to_unsigned_long(), 0);
    TS_ASSERT_EQUALS(slg.to_unsigned_long_long(), 0);

    TS_ASSERT_EQUALS(sll.to_unsigned_char(), 0);
    TS_ASSERT_EQUALS(sll.to_unsigned_short(), 0);
    TS_ASSERT_EQUALS(sll.to_unsigned_int(), 0);
    TS_ASSERT_EQUALS(sll.to_unsigned_long(), 0);
    TS_ASSERT_EQUALS(sll.to_unsigned_long_long(), 0);

    // Smaller size signed integers (underflow)
    TS_ASSERT_EQUALS(ssh.to_char(), CHAR_MIN);
    TS_ASSERT_EQUALS(sit.to_char(), CHAR_MIN);
    TS_ASSERT_EQUALS(sit.to_short(), SHRT_MIN);
    TS_ASSERT_EQUALS(slg.to_char(), CHAR_MIN);
    TS_ASSERT_EQUALS(slg.to_short(), SHRT_MIN);
    TS_ASSERT_EQUALS(sll.to_char(), CHAR_MIN);
    TS_ASSERT_EQUALS(sll.to_short(), SHRT_MIN);
    TS_ASSERT_EQUALS(sll.to_int(), LONG_MIN);
    TS_ASSERT_EQUALS(sll.to_long(), LONG_MIN);

    // Same type
    TS_ASSERT_EQUALS(sch.to_char(), -126);
    TS_ASSERT_EQUALS(ssh.to_short(), -32000);
    TS_ASSERT_EQUALS(sit.to_int(), -2100000000);
    TS_ASSERT_EQUALS(slg.to_long(), -2100000000);
    TS_ASSERT_EQUALS(sll.to_long_long(), -9200000000000000000LL);

    // Larger size integers (signed for negative value)
    TS_ASSERT_EQUALS(sch.to_short(), -126);
    TS_ASSERT_EQUALS(sch.to_int(), -126);
    TS_ASSERT_EQUALS(sch.to_long(), -126);
    TS_ASSERT_EQUALS(sch.to_long_long(), -126);

    TS_ASSERT_EQUALS(ssh.to_int(), -32000);
    TS_ASSERT_EQUALS(ssh.to_long(), -32000);
    TS_ASSERT_EQUALS(ssh.to_long_long(), -32000);

    TS_ASSERT_EQUALS(sit.to_long_long(), -2100000000);

    TS_ASSERT_EQUALS(slg.to_long_long(), -2100000000);

    // Larger size integers (positive value)
    sch = 126;
    ssh = 32000;
    sit = 2100000000;
    slg = 2100000000L;
    sll = 9200000000000000000LL;

    TS_ASSERT_EQUALS(sch.to_unsigned_char(), 126U);
    TS_ASSERT_EQUALS(sch.to_unsigned_short(), 126U);
    TS_ASSERT_EQUALS(sch.to_unsigned_int(), 126U);
    TS_ASSERT_EQUALS(sch.to_unsigned_long(), 126U);
    TS_ASSERT_EQUALS(sch.to_unsigned_long_long(), 126U);
    TS_ASSERT_EQUALS(sch.to_short(), 126);
    TS_ASSERT_EQUALS(sch.to_int(), 126);
    TS_ASSERT_EQUALS(sch.to_long(), 126);
    TS_ASSERT_EQUALS(sch.to_long_long(), 126);

    TS_ASSERT_EQUALS(ssh.to_unsigned_short(), 32000U);
    TS_ASSERT_EQUALS(ssh.to_unsigned_int(), 32000U);
    TS_ASSERT_EQUALS(ssh.to_unsigned_long(), 32000U);
    TS_ASSERT_EQUALS(ssh.to_unsigned_long_long(), 32000U);
    TS_ASSERT_EQUALS(ssh.to_int(), 32000);
    TS_ASSERT_EQUALS(ssh.to_long(), 32000);
    TS_ASSERT_EQUALS(ssh.to_long_long(), 32000);

    TS_ASSERT_EQUALS(sit.to_unsigned_int(), 2100000000U);
    TS_ASSERT_EQUALS(sit.to_unsigned_long(), 2100000000U);
    TS_ASSERT_EQUALS(sit.to_unsigned_long_long(), 2100000000U);
    TS_ASSERT_EQUALS(sit.to_long_long(), 2100000000);

    TS_ASSERT_EQUALS(slg.to_unsigned_int(), 2100000000U);
    TS_ASSERT_EQUALS(slg.to_unsigned_long(), 2100000000U);
    TS_ASSERT_EQUALS(slg.to_unsigned_long_long(), 2100000000U);
    TS_ASSERT_EQUALS(slg.to_long_long(), 2100000000);

    TS_ASSERT_EQUALS(sll.to_unsigned_long_long(), 9200000000000000000LL);

    // Floats - positive values
    TS_ASSERT_EQUALS(sch.to_float(), 126.f);
    TS_ASSERT_EQUALS(sch.to_double(), 126.);
    TS_ASSERT_EQUALS(ssh.to_float(), 32000.f);
    TS_ASSERT_EQUALS(ssh.to_double(), 32000.);
    TS_ASSERT_EQUALS(sit.to_float(), 2100000000.f);
    TS_ASSERT_EQUALS(sit.to_double(), 2100000000.);
    TS_ASSERT_EQUALS(slg.to_float(), 2100000000.f);
    TS_ASSERT_EQUALS(slg.to_double(), 2100000000.);
    TS_ASSERT_EQUALS(sll.to_float(), 9200000000000000000.f);
    TS_ASSERT_EQUALS(sll.to_double(), 9200000000000000000.);

    // Floats - negative values
    sch = -126;
    ssh = -32000;
    sit = -2100000000;
    slg = -2100000000;
    sll = -9200000000000000000LL;
    TS_ASSERT_EQUALS(sch.to_float(), -126.f);
    TS_ASSERT_EQUALS(sch.to_double(), -126.);
    TS_ASSERT_EQUALS(ssh.to_float(), -32000.f);
    TS_ASSERT_EQUALS(ssh.to_double(), -32000.);
    TS_ASSERT_EQUALS(sit.to_float(), -2100000000.f);
    TS_ASSERT_EQUALS(sit.to_double(), -2100000000.);
    TS_ASSERT_EQUALS(slg.to_float(), -2100000000.f);
    TS_ASSERT_EQUALS(slg.to_double(), -2100000000.);
    TS_ASSERT_EQUALS(sll.to_float(), -9200000000000000000.f);
    TS_ASSERT_EQUALS(sll.to_double(), -9200000000000000000.);
}

void GenVarTestSuite::testFromValue(void)
{
    // 1. From float/double to integer types

    generic_var<unsigned char> uch;
    generic_var<char> sch;
    generic_var<unsigned short> ush;
    generic_var<short> ssh;
    generic_var<unsigned int> uit;
    generic_var<int> sit;
    generic_var<unsigned long> ulg;
    generic_var<long> slg;
    generic_var<unsigned long long> ull;
    generic_var<long long> sll;

    char sch_v = 9;
    unsigned char uch_v = 7;
    short ssh_v = 5;
    unsigned short ush_v = 3;
    int sit_v = 1;
    unsigned uit_v = 11;
    long slg_v = 13;
    unsigned long ulg_v = 15;
    long long sll_v = 17;
    unsigned long long ull_v = 19;

    // Regular
    uch.fromValue(sch_v);
    TS_ASSERT_EQUALS(uch, '\11');
    uch.fromValue(uch_v);
    TS_ASSERT_EQUALS(uch, '\7');
    uch.fromValue(ssh_v);
    TS_ASSERT_EQUALS(uch, '\5');
    uch.fromValue(ush_v);
    TS_ASSERT_EQUALS(uch, '\3');
    uch.fromValue(sit_v);
    TS_ASSERT_EQUALS(uch, '\1');
    uch.fromValue(uit_v);
    TS_ASSERT_EQUALS(uch, '\13');
    uch.fromValue(slg_v);
    TS_ASSERT_EQUALS(uch, '\15');
    uch.fromValue(ulg_v);
    TS_ASSERT_EQUALS(uch, '\17');
    uch.fromValue(sll_v);
    TS_ASSERT_EQUALS(uch, '\21');
    uch.fromValue(ull_v);
    TS_ASSERT_EQUALS(uch, '\23');
    uch.fromValue("21");
    TS_ASSERT_EQUALS(uch, '\25');
    uch.fromValue("");
    TS_ASSERT_EQUALS(uch, 0);
    uch.from_char_ptr("     ");
    TS_ASSERT_EQUALS(uch, 0);
    sch.fromValue(sch_v);
    TS_ASSERT_EQUALS(sch, '\11');
    sch.fromValue(uch_v);
    TS_ASSERT_EQUALS(sch, '\7');
    sch.fromValue(ssh_v);
    TS_ASSERT_EQUALS(sch, '\5');
    sch.fromValue(ush_v);
    TS_ASSERT_EQUALS(sch, '\3');
    sch.fromValue(sit_v);
    TS_ASSERT_EQUALS(sch, '\1');
    sch.fromValue(uit_v);
    TS_ASSERT_EQUALS(sch, '\13');
    sch.fromValue(slg_v);
    TS_ASSERT_EQUALS(sch, '\15');
    sch.fromValue(ulg_v);
    TS_ASSERT_EQUALS(sch, '\17');
    sch.fromValue(sll_v);
    TS_ASSERT_EQUALS(sch, '\21');
    sch.fromValue(ull_v);
    TS_ASSERT_EQUALS(sch, '\23');
    sch.fromValue("21");
    TS_ASSERT_EQUALS(sch, '\25');
    sch.fromValue("");
    TS_ASSERT_EQUALS(sch, 0);
    ush.fromValue(sch_v);
    TS_ASSERT_EQUALS(ush, (unsigned short)9U);
    ush.fromValue(uch_v);
    TS_ASSERT_EQUALS(ush, (unsigned short)7U);
    ush.fromValue(ssh_v);
    TS_ASSERT_EQUALS(ush, (unsigned short)5U);
    ush.fromValue(ush_v);
    TS_ASSERT_EQUALS(ush, (unsigned short)3U);
    ush.fromValue(sit_v);
    TS_ASSERT_EQUALS(ush, (unsigned short)1U);
    ush.fromValue(uit_v);
    TS_ASSERT_EQUALS(ush, (unsigned short)11U);
    ush.fromValue(slg_v);
    TS_ASSERT_EQUALS(ush, (unsigned short)13U);
    ush.fromValue(ulg_v);
    TS_ASSERT_EQUALS(ush, (unsigned short)15U);
    ush.fromValue(sll_v);
    TS_ASSERT_EQUALS(ush, (unsigned short)17U);
    ush.fromValue(ull_v);
    TS_ASSERT_EQUALS(ush, (unsigned short)19U);
    ush.fromValue("21");
    TS_ASSERT_EQUALS(ush, (unsigned short)21U);
    ush.fromValue("");
    TS_ASSERT_EQUALS(ush, 0);
    ush.from_char_ptr("     ");
    TS_ASSERT_EQUALS(ush, 0);
    ssh.fromValue(sch_v);
    TS_ASSERT_EQUALS(ssh, (short)9);
    ssh.fromValue(uch_v);
    TS_ASSERT_EQUALS(ssh, (short)7);
    ssh.fromValue(ssh_v);
    TS_ASSERT_EQUALS(ssh, (short)5);
    ssh.fromValue(ush_v);
    TS_ASSERT_EQUALS(ssh, (short)3);
    ssh.fromValue(sit_v);
    TS_ASSERT_EQUALS(ssh, (short)1);
    ssh.fromValue(uit_v);
    TS_ASSERT_EQUALS(ssh, (short)11);
    ssh.fromValue(slg_v);
    TS_ASSERT_EQUALS(ssh, (short)13);
    ssh.fromValue(ulg_v);
    TS_ASSERT_EQUALS(ssh, (short)15);
    ssh.fromValue(sll_v);
    TS_ASSERT_EQUALS(ssh, (short)17);
    ssh.fromValue(ull_v);
    TS_ASSERT_EQUALS(ssh, (short)19);
    ssh.fromValue("21");
    TS_ASSERT_EQUALS(ssh, (short)21);
    ssh.fromValue("");
    TS_ASSERT_EQUALS(ssh, 0);
    uit.fromValue(sch_v);
    TS_ASSERT_EQUALS(uit, 9U);
    uit.fromValue(uch_v);
    TS_ASSERT_EQUALS(uit, 7U);
    uit.fromValue(ssh_v);
    TS_ASSERT_EQUALS(uit, 5U);
    uit.fromValue(ush_v);
    TS_ASSERT_EQUALS(uit, 3U);
    uit.fromValue(sit_v);
    TS_ASSERT_EQUALS(uit, 1U);
    uit.fromValue(uit_v);
    TS_ASSERT_EQUALS(uit, 11U);
    uit.fromValue(slg_v);
    TS_ASSERT_EQUALS(uit, 13U);
    uit.fromValue(ulg_v);
    TS_ASSERT_EQUALS(uit, 15U);
    uit.fromValue(sll_v);
    TS_ASSERT_EQUALS(uit, 17U);
    uit.fromValue(ull_v);
    TS_ASSERT_EQUALS(uit, 19U);
    uit.fromValue("21");
    TS_ASSERT_EQUALS(uit, 21U);
    uit.fromValue("");
    TS_ASSERT_EQUALS(uit, 0);
    uit.from_char_ptr("     ");
    TS_ASSERT_EQUALS(uit, 0);
    sit.fromValue(sch_v);
    TS_ASSERT_EQUALS(sit, 9);
    sit.fromValue(uch_v);
    TS_ASSERT_EQUALS(sit, 7);
    sit.fromValue(ssh_v);
    TS_ASSERT_EQUALS(sit, 5);
    sit.fromValue(ush_v);
    TS_ASSERT_EQUALS(sit, 3);
    sit.fromValue(sit_v);
    TS_ASSERT_EQUALS(sit, 1);
    sit.fromValue(uit_v);
    TS_ASSERT_EQUALS(sit, 11);
    sit.fromValue(slg_v);
    TS_ASSERT_EQUALS(sit, 13);
    sit.fromValue(ulg_v);
    TS_ASSERT_EQUALS(sit, 15);
    sit.fromValue(sll_v);
    TS_ASSERT_EQUALS(sit, 17);
    sit.fromValue(ull_v);
    TS_ASSERT_EQUALS(sit, 19);
    sit.fromValue("21");
    TS_ASSERT_EQUALS(sit, 21);
    sit.fromValue("");
    TS_ASSERT_EQUALS(sit, 0);
    ulg.fromValue(sch_v);
    TS_ASSERT_EQUALS(ulg, 9UL);
    ulg.fromValue(uch_v);
    TS_ASSERT_EQUALS(ulg, 7UL);
    ulg.fromValue(ssh_v);
    TS_ASSERT_EQUALS(ulg, 5UL);
    ulg.fromValue(ush_v);
    TS_ASSERT_EQUALS(ulg, 3UL);
    ulg.fromValue(sit_v);
    TS_ASSERT_EQUALS(ulg, 1UL);
    ulg.fromValue(uit_v);
    TS_ASSERT_EQUALS(ulg, 11UL);
    ulg.fromValue(slg_v);
    TS_ASSERT_EQUALS(ulg, 13UL);
    ulg.fromValue(ulg_v);
    TS_ASSERT_EQUALS(ulg, 15UL);
    ulg.fromValue(sll_v);
    TS_ASSERT_EQUALS(ulg, 17UL);
    ulg.fromValue(ull_v);
    TS_ASSERT_EQUALS(ulg, 19UL);
    ulg.fromValue("21");
    TS_ASSERT_EQUALS(ulg, 21UL);
    ulg.fromValue("");
    TS_ASSERT_EQUALS(ulg, 0);
    ulg.from_char_ptr("     ");
    TS_ASSERT_EQUALS(ulg, 0);
    slg.fromValue(sch_v);
    TS_ASSERT_EQUALS(slg, 9L);
    slg.fromValue(uch_v);
    TS_ASSERT_EQUALS(slg, 7L);
    slg.fromValue(ssh_v);
    TS_ASSERT_EQUALS(slg, 5L);
    slg.fromValue(ush_v);
    TS_ASSERT_EQUALS(slg, 3L);
    slg.fromValue(sit_v);
    TS_ASSERT_EQUALS(slg, 1L);
    slg.fromValue(uit_v);
    TS_ASSERT_EQUALS(slg, 11L);
    slg.fromValue(slg_v);
    TS_ASSERT_EQUALS(slg, 13L);
    slg.fromValue(ulg_v);
    TS_ASSERT_EQUALS(slg, 15L);
    slg.fromValue(sll_v);
    TS_ASSERT_EQUALS(slg, 17L);
    slg.fromValue(ull_v);
    TS_ASSERT_EQUALS(slg, 19L);
    slg.fromValue("21");
    TS_ASSERT_EQUALS(slg, 21L);
    slg.fromValue("");
    TS_ASSERT_EQUALS(slg, 0);
    ull.fromValue(sch_v);
    TS_ASSERT_EQUALS(ull, 9ULL);
    ull.fromValue(uch_v);
    TS_ASSERT_EQUALS(ull, 7ULL);
    ull.fromValue(ssh_v);
    TS_ASSERT_EQUALS(ull, 5ULL);
    ull.fromValue(ush_v);
    TS_ASSERT_EQUALS(ull, 3ULL);
    ull.fromValue(sit_v);
    TS_ASSERT_EQUALS(ull, 1ULL);
    ull.fromValue(uit_v);
    TS_ASSERT_EQUALS(ull, 11ULL);
    ull.fromValue(slg_v);
    TS_ASSERT_EQUALS(ull, 13ULL);
    ull.fromValue(ulg_v);
    TS_ASSERT_EQUALS(ull, 15ULL);
    ull.fromValue(sll_v);
    TS_ASSERT_EQUALS(ull, 17ULL);
    ull.fromValue(ull_v);
    TS_ASSERT_EQUALS(ull, 19ULL);
    ull.fromValue("21");
    TS_ASSERT_EQUALS(ull, 21ULL);
    ull.fromValue("");
    TS_ASSERT_EQUALS(ull, 0);
    ull.from_char_ptr("     ");
    TS_ASSERT_EQUALS(ull, 0);
    sll.fromValue(sch_v);
    TS_ASSERT_EQUALS(sll, 9LL);
    sll.fromValue(uch_v);
    TS_ASSERT_EQUALS(sll, 7LL);
    sll.fromValue(ssh_v);
    TS_ASSERT_EQUALS(sll, 5LL);
    sll.fromValue(ush_v);
    TS_ASSERT_EQUALS(sll, 3LL);
    sll.fromValue(sit_v);
    TS_ASSERT_EQUALS(sll, 1LL);
    sll.fromValue(uit_v);
    TS_ASSERT_EQUALS(sll, 11LL);
    sll.fromValue(slg_v);
    TS_ASSERT_EQUALS(sll, 13LL);
    sll.fromValue(ulg_v);
    TS_ASSERT_EQUALS(sll, 15LL);
    sll.fromValue(sll_v);
    TS_ASSERT_EQUALS(sll, 17LL);
    sll.fromValue(ull_v);
    TS_ASSERT_EQUALS(sll, 19LL);
    sll.fromValue("21");
    TS_ASSERT_EQUALS(sll, 21LL);
    sll.fromValue("");
    TS_ASSERT_EQUALS(sll, 0);

    // Rounding
    uch.fromValue(100.5f);
    TS_ASSERT_EQUALS(uch, '\145');
    uch.fromValue(104.3f);
    TS_ASSERT_EQUALS(uch, '\150');
    uch.fromValue(108.5);
    TS_ASSERT_EQUALS(uch, '\155');
    uch.fromValue(112.499);
    TS_ASSERT_EQUALS(uch, '\160');
    ush.fromValue(100.5f);
    TS_ASSERT_EQUALS(ush, (unsigned short)101U);
    ush.fromValue(104.3f);
    TS_ASSERT_EQUALS(ush, (unsigned short)104U);
    ush.fromValue(108.5);
    TS_ASSERT_EQUALS(ush, (unsigned short)109U);
    ush.fromValue(112.499);
    TS_ASSERT_EQUALS(ush, (unsigned short)112U);
    uit.fromValue(100.5f);
    TS_ASSERT_EQUALS(uit, 101U);
    uit.fromValue(104.3f);
    TS_ASSERT_EQUALS(uit, 104U);
    uit.fromValue(108.5);
    TS_ASSERT_EQUALS(uit, 109U);
    uit.fromValue(112.499);
    TS_ASSERT_EQUALS(uit, 112U);
    ulg.fromValue(100.5f);
    TS_ASSERT_EQUALS(ulg, 101UL);
    ulg.fromValue(104.3f);
    TS_ASSERT_EQUALS(ulg, 104UL);
    ulg.fromValue(108.5);
    TS_ASSERT_EQUALS(ulg, 109UL);
    ulg.fromValue(112.499);
    TS_ASSERT_EQUALS(ulg, 112UL);
    ull.fromValue(100.5f);
    TS_ASSERT_EQUALS(ull, 101ULL);
    ull.fromValue(104.3f);
    TS_ASSERT_EQUALS(ull, 104ULL);
    ull.fromValue(108.5);
    TS_ASSERT_EQUALS(ull, 109ULL);
    ull.fromValue(112.499);
    TS_ASSERT_EQUALS(ull, 112ULL);
    sch.fromValue(100.5f);
    TS_ASSERT_EQUALS(sch, '\145');
    sch.fromValue(104.3f);
    TS_ASSERT_EQUALS(sch, '\150');
    sch.fromValue(-96.5f);
    TS_ASSERT_EQUALS(sch, (signed char)-97);
    sch.fromValue(-92.3f);
    TS_ASSERT_EQUALS(sch, (signed char)-92);
    sch.fromValue(108.5);
    TS_ASSERT_EQUALS(sch, '\155');
    sch.fromValue(112.499);
    TS_ASSERT_EQUALS(sch, '\160');
    sch.fromValue(-88.5241);
    TS_ASSERT_EQUALS(sch, (signed char)-89);
    sch.fromValue(-84.3967);
    TS_ASSERT_EQUALS(sch, (signed char)-84);
    ssh.fromValue(100.5f);
    TS_ASSERT_EQUALS(ssh, (short)101);
    ssh.fromValue(104.3f);
    TS_ASSERT_EQUALS(ssh, (short)104);
    ssh.fromValue(-96.5f);
    TS_ASSERT_EQUALS(ssh, (short)-97);
    ssh.fromValue(-92.3f);
    TS_ASSERT_EQUALS(ssh, (short)-92);
    ssh.fromValue(108.5);
    TS_ASSERT_EQUALS(ssh, (short)109);
    ssh.fromValue(112.499);
    TS_ASSERT_EQUALS(ssh, (short)112);
    ssh.fromValue(-88.5241);
    TS_ASSERT_EQUALS(ssh, (short)-89);
    ssh.fromValue(-84.3967);
    TS_ASSERT_EQUALS(ssh, (short)-84);
    sit.fromValue(100.5f);
    TS_ASSERT_EQUALS(sit, 101);
    sit.fromValue(104.3f);
    TS_ASSERT_EQUALS(sit, 104);
    sit.fromValue(-96.5f);
    TS_ASSERT_EQUALS(sit, -97);
    sit.fromValue(-92.3f);
    TS_ASSERT_EQUALS(sit, -92);
    sit.fromValue(108.5);
    TS_ASSERT_EQUALS(sit, 109);
    sit.fromValue(112.499);
    TS_ASSERT_EQUALS(sit, 112);
    sit.fromValue(-88.5241);
    TS_ASSERT_EQUALS(sit, -89);
    sit.fromValue(-84.3967);
    TS_ASSERT_EQUALS(sit, -84);
    slg.fromValue(100.5f);
    TS_ASSERT_EQUALS(slg, 101L);
    slg.fromValue(104.3f);
    TS_ASSERT_EQUALS(slg, 104L);
    slg.fromValue(-96.5f);
    TS_ASSERT_EQUALS(slg, -97L);
    slg.fromValue(-92.3f);
    TS_ASSERT_EQUALS(slg, -92L);
    slg.fromValue(108.5);
    TS_ASSERT_EQUALS(slg, 109L);
    slg.fromValue(112.499);
    TS_ASSERT_EQUALS(slg, 112L);
    slg.fromValue(-88.5241);
    TS_ASSERT_EQUALS(slg, -89L);
    slg.fromValue(-84.3967);
    TS_ASSERT_EQUALS(slg, -84L);
    sll.fromValue(100.5f);
    TS_ASSERT_EQUALS(sll, 101LL);
    sll.fromValue(104.3f);
    TS_ASSERT_EQUALS(sll, 104LL);
    sll.fromValue(-96.5f);
    TS_ASSERT_EQUALS(sll, -97LL);
    sll.fromValue(-92.3f);
    TS_ASSERT_EQUALS(sll, -92LL);
    sll.fromValue(108.5);
    TS_ASSERT_EQUALS(sll, 109LL);
    sll.fromValue(112.499);
    TS_ASSERT_EQUALS(sll, 112LL);
    sll.fromValue(-88.5241);
    TS_ASSERT_EQUALS(sll, -89LL);
    sll.fromValue(-84.3967);
    TS_ASSERT_EQUALS(sll, -84LL);

    // Clip to max
    uch.from_short((short)400);
    TS_ASSERT_EQUALS(uch, (unsigned char)255);
    uch.from_unsigned_short((unsigned short)34000);
    TS_ASSERT_EQUALS(uch, (unsigned char)255);
    uch.from_int(1073741824);
    TS_ASSERT_EQUALS(uch, (unsigned char)255);
    uch.from_unsigned_int(2147539648U);
    TS_ASSERT_EQUALS(uch, (unsigned char)255);
    uch.from_long(1065721824L);
    TS_ASSERT_EQUALS(uch, (unsigned char)255);
    uch.from_unsigned_long(2147532443UL);
    TS_ASSERT_EQUALS(uch, (unsigned char)255);
    uch.from_long_long(4611686018436950218LL);
    TS_ASSERT_EQUALS(uch, (unsigned char)255);
    uch.from_unsigned_long_long(9223372096732559023ULL);
    TS_ASSERT_EQUALS(uch, (unsigned char)255);
    sch.from_unsigned_char('\301');
    TS_ASSERT_EQUALS(sch, '\177');
    sch.from_short((short)300);
    TS_ASSERT_EQUALS(sch, '\177');
    sch.from_unsigned_short((unsigned short)34000);
    TS_ASSERT_EQUALS(sch, '\177');
    sch.from_int(1073741824);
    TS_ASSERT_EQUALS(sch, '\177');
    sch.from_unsigned_int(2147539648U);
    TS_ASSERT_EQUALS(sch, '\177');
    sch.from_long(1065721824L);
    TS_ASSERT_EQUALS(sch, '\177');
    sch.from_unsigned_long(2147532443UL);
    TS_ASSERT_EQUALS(sch, '\177');
    sch.from_long_long(4611686018436950218LL);
    TS_ASSERT_EQUALS(sch, '\177');
    sch.from_unsigned_long_long(9223372096732559023ULL);
    TS_ASSERT_EQUALS(sch, '\177');
    ush.from_int(1073741824);
    TS_ASSERT_EQUALS(ush, (unsigned short)65535);
    ush.from_unsigned_int(2147539648U);
    TS_ASSERT_EQUALS(ush, (unsigned short)65535);
    ush.from_long(1065721824L);
    TS_ASSERT_EQUALS(ush, (unsigned short)65535);
    ush.from_unsigned_long(2147532443UL);
    TS_ASSERT_EQUALS(ush, (unsigned short)65535);
    ush.from_long_long(4611686018436950218LL);
    TS_ASSERT_EQUALS(ush, (unsigned short)65535);
    ush.from_unsigned_long_long(9223372096732559023ULL);
    TS_ASSERT_EQUALS(ush, (unsigned short)65535);
    ssh.from_unsigned_short((unsigned short)34000);
    TS_ASSERT_EQUALS(ssh, (short)32767);
    ssh.from_int(1073741824);
    TS_ASSERT_EQUALS(ssh, (short)32767);
    ssh.from_unsigned_int(2147539648U);
    TS_ASSERT_EQUALS(ssh, (short)32767);
    ssh.from_long(1065721824L);
    TS_ASSERT_EQUALS(ssh, (short)32767);
    ssh.from_unsigned_long(2147532443UL);
    TS_ASSERT_EQUALS(ssh, (short)32767);
    ssh.from_long_long(4611686018436950218LL);
    TS_ASSERT_EQUALS(ssh, (short)32767);
    ssh.from_unsigned_long_long(9223372096732559023ULL);
    TS_ASSERT_EQUALS(ssh, (short)32767);
    uit.from_long_long(4611686018436950218LL);
    TS_ASSERT_EQUALS(uit, 4294967295U);
    uit.from_unsigned_long_long(9223372096732559023ULL);
    TS_ASSERT_EQUALS(uit, 4294967295U);
    sit.from_unsigned_int(2147532443U);
    TS_ASSERT_EQUALS(sit, 2147483647);
    sit.from_unsigned_long(2147532443UL);
    TS_ASSERT_EQUALS(sit, 2147483647);
    sit.from_long_long(4611686018436950218LL);
    TS_ASSERT_EQUALS(sit, 2147483647);
    sit.from_unsigned_long_long(9223372096732559023ULL);
    TS_ASSERT_EQUALS(sit, 2147483647);
    ulg.from_long_long(4611686018436950218LL);
    TS_ASSERT_EQUALS(ulg, 4294967295UL);
    ulg.from_unsigned_long_long(9223372096732559023ULL);
    TS_ASSERT_EQUALS(ulg, 4294967295UL);
    slg.from_unsigned_long(2147532443UL);
    TS_ASSERT_EQUALS(slg, 2147483647L);
    slg.from_long_long(4611686018436950218LL);
    TS_ASSERT_EQUALS(slg, 2147483647L);
    slg.from_unsigned_long_long(9223372096732559023ULL);
    TS_ASSERT_EQUALS(slg, 2147483647L);
    sll.from_unsigned_long_long(9223372096732559023ULL);
    TS_ASSERT_EQUALS(sll, 9223372036854775807LL);

    // Clip to min
    uch.from_char((signed char)-116);
    TS_ASSERT_EQUALS(uch, 0);
    uch.from_short((short)-32762);
    TS_ASSERT_EQUALS(uch, 0);
    uch.from_int(-2147419395);
    TS_ASSERT_EQUALS(uch, 0);
    uch.from_long(-1743429375);
    TS_ASSERT_EQUALS(uch, 0);
    uch.from_long_long(-9223372036853921539LL);
    TS_ASSERT_EQUALS(uch, 0);
    sch.from_short((short)-32762);
    TS_ASSERT_EQUALS(sch, (signed char)-128);
    sch.from_int(-2147419395);
    TS_ASSERT_EQUALS(sch, (signed char)-128);
    sch.from_long(-1743429375);
    TS_ASSERT_EQUALS(sch, (signed char)-128);
    sch.from_long_long(-9223372036853921539LL);
    TS_ASSERT_EQUALS(sch, (signed char)-128);
    ush.from_short((short)-32762);
    TS_ASSERT_EQUALS(ush, 0);
    ush.from_int(-2147419395);
    TS_ASSERT_EQUALS(ush, 0);
    ush.from_long(-1743429375);
    TS_ASSERT_EQUALS(ush, 0);
    ush.from_long_long(-9223372036853921539LL);
    TS_ASSERT_EQUALS(ush, 0);
    ssh.from_int(-2147419395);
    TS_ASSERT_EQUALS(ssh, (short)-32768);
    ssh.from_long(-1743429375);
    TS_ASSERT_EQUALS(ssh, (short)-32768);
    ssh.from_long_long(-9223372036853921539LL);
    TS_ASSERT_EQUALS(ssh, (short)-32768);
    uit.from_int(-2147419395);
    TS_ASSERT_EQUALS(uit, 0);
    uit.from_long(-1743429375);
    TS_ASSERT_EQUALS(uit, 0);
    uit.from_long_long(-9223372036853921539LL);
    TS_ASSERT_EQUALS(uit, 0);
    sit.from_long_long(-9223372036853921539LL);
    TS_ASSERT_EQUALS(sit, (int)-2147483648);
    ulg.from_int(-2147419395);
    TS_ASSERT_EQUALS(ulg, 0);
    ulg.from_long(-1743429375);
    TS_ASSERT_EQUALS(ulg, 0);
    ulg.from_long_long(-9223372036853921539LL);
    TS_ASSERT_EQUALS(ulg, 0);
    slg.from_long_long(-9223372036853921539LL);
    TS_ASSERT_EQUALS(slg, (long)-2147483648);
    ull.from_long_long(-9223372036853921539LL);
    TS_ASSERT_EQUALS(ull, 0);

    // 2. From integer/string/float types to float/double
    generic_var<float> flt;
    generic_var<double> dbl;

    flt.fromValue(sch_v);
    TS_ASSERT_EQUALS(flt, 9.f);
    flt.fromValue(uch_v);
    TS_ASSERT_EQUALS(flt, 7.f);
    flt.fromValue(ssh_v);
    TS_ASSERT_EQUALS(flt, 5.f);
    flt.fromValue(ush_v);
    TS_ASSERT_EQUALS(flt, 3.f);
    flt.fromValue(sit_v);
    TS_ASSERT_EQUALS(flt, 1.f);
    flt.fromValue(uit_v);
    TS_ASSERT_EQUALS(flt, 11.f);
    flt.fromValue(slg_v);
    TS_ASSERT_EQUALS(flt, 13.f);
    flt.fromValue(ulg_v);
    TS_ASSERT_EQUALS(flt, 15.f);
    flt.fromValue(sll_v);
    TS_ASSERT_EQUALS(flt, 17.f);
    flt.fromValue(ull_v);
    TS_ASSERT_EQUALS(flt, 19.f);
    flt.fromValue("21");
    TS_ASSERT_EQUALS(flt, 21.f);
    dbl.fromValue(sch_v);
    TS_ASSERT_EQUALS(dbl, 9.);
    dbl.fromValue(uch_v);
    TS_ASSERT_EQUALS(dbl, 7.);
    dbl.fromValue(ssh_v);
    TS_ASSERT_EQUALS(dbl, 5.);
    dbl.fromValue(ush_v);
    TS_ASSERT_EQUALS(dbl, 3.);
    dbl.fromValue(sit_v);
    TS_ASSERT_EQUALS(dbl, 1.);
    dbl.fromValue(uit_v);
    TS_ASSERT_EQUALS(dbl, 11.);
    dbl.fromValue(slg_v);
    TS_ASSERT_EQUALS(dbl, 13.);
    dbl.fromValue(ulg_v);
    TS_ASSERT_EQUALS(dbl, 15.);
    dbl.fromValue(sll_v);
    TS_ASSERT_EQUALS(dbl, 17.);
    dbl.fromValue(ull_v);
    TS_ASSERT_EQUALS(dbl, 19.);
    dbl.fromValue("21");
    TS_ASSERT_EQUALS(dbl, 21.);

    flt.from_char((signed char)-86);
    TS_ASSERT_EQUALS(flt, -86.f);
    flt.from_short((short)-6453);
    TS_ASSERT_EQUALS(flt, -6453.f);
    flt.from_int(-63513);
    TS_ASSERT_EQUALS(flt, -63513.f);
    flt.from_long(-43523L);
    TS_ASSERT_EQUALS(flt, -43523.f);
    flt.from_long_long(-34241LL);
    TS_ASSERT_EQUALS(flt, -34241.f);
    flt.from_char_ptr("-42421");
    TS_ASSERT_EQUALS(flt, -42421.f);
    flt.from_char_ptr("");
    TS_ASSERT_EQUALS(flt, 0);
    dbl.from_char((signed char)-86);
    TS_ASSERT_EQUALS(dbl, -86.);
    dbl.from_short((short)-6453);
    TS_ASSERT_EQUALS(dbl, -6453.);
    dbl.from_int(-63513);
    TS_ASSERT_EQUALS(dbl, -63513.);
    dbl.from_long(-43523L);
    TS_ASSERT_EQUALS(dbl, -43523.);
    dbl.from_long_long(-34241LL);
    TS_ASSERT_EQUALS(dbl, -34241.);
    dbl.from_char_ptr("-42421");
    TS_ASSERT_EQUALS(dbl, -42421.);
    dbl.from_char_ptr("");
    TS_ASSERT_EQUALS(dbl, 0);
    dbl.from_char_ptr("     ");
    TS_ASSERT_EQUALS(dbl, 0);
}

void GenVarTestSuite::testCopyConstructor(void)
{
    // Integers to integers
    generic_var<char> sch('\104'); // 68
    generic_var<char> sch_n((char)-54);
    generic_var<unsigned char> uch('\364'); // 244
    generic_var<unsigned char> uch_n('\104'); // 68
    generic_var<short> ssh((short)31943);
    generic_var<short> ssh_n((short)-32593);
    generic_var<unsigned short> ush((unsigned short)54001);
    generic_var<unsigned short> ush_n((unsigned short)31943U);
    generic_var<int> sit(268494041);
    generic_var<int> sit_n(-341124346);
    generic_var<unsigned int> uit(3467483669U);
    generic_var<unsigned int> uit_n(268494041U);
    generic_var<long> slg(428496782L);
    generic_var<long> slg_n(-198832255L);
    generic_var<unsigned long> ulg(4269310731UL);
    generic_var<unsigned long> ulg_n(428496782UL);
    generic_var<long long> sll(18014398967378304LL);
    generic_var<long long> sll_n(-576460761068855588LL);
    generic_var<unsigned long long> ull(18446744073703655295ULL);
    generic_var<unsigned long long> ull_n(8223372036306943697ULL);
    generic_var<short> ssh_1((short)68);
    generic_var<short> ssh_2((short)-54);
    generic_var<unsigned short> ush_1((unsigned short)68U);
    generic_var<int> sit_1(68);
    generic_var<int> sit_2(-54);
    generic_var<unsigned int> uit_1(68U);
    generic_var<long> slg_1(68);
    generic_var<long> slg_2(-54);
    generic_var<unsigned long> ulg_1(68U);
    generic_var<long long> sll_1(68);
    generic_var<long long> sll_2(-54);
    generic_var<unsigned long long> ull_1(68U);

    const value_var &sch_r = sch;
    const value_var &sch_n_r = sch_n;
    const value_var &uch_r = uch;
    const value_var &uch_n_r = uch_n;
    const value_var &ssh_r = ssh;
    const value_var &ssh_n_r = ssh_n;
    const value_var &ush_r = ush;
    const value_var &ush_n_r = ush_n;
    const value_var &sit_r = sit;
    const value_var &sit_n_r = sit_n;
    const value_var &uit_r = uit;
    const value_var &uit_n_r = uit_n;
    const value_var &slg_r = slg;
    const value_var &slg_n_r = slg_n;
    const value_var &ulg_r = ulg;
    const value_var &ulg_n_r = ulg_n;
    const value_var &sll_r = sll;
    const value_var &sll_n_r = sll_n;
    const value_var &ull_r = ull;
    const value_var &ull_n_r = ull_n;
    const value_var &ssh_1_r = ssh_1;
    const value_var &ssh_2_r = ssh_2;
    const value_var &ush_1_r = ush_1;
    const value_var &sit_1_r = sit_1;
    const value_var &sit_2_r = sit_2;
    const value_var &uit_1_r = uit_1;
    const value_var &slg_1_r = slg_1;
    const value_var &slg_2_r = slg_2;
    const value_var &ulg_1_r = ulg_1;
    const value_var &sll_1_r = sll_1;
    const value_var &sll_2_r = sll_2;
    const value_var &ull_1_r = ull_1;

    {
        generic_var<char> val(sch_r);
        TS_ASSERT_EQUALS(val, '\104');
    }
    {
        generic_var<char> val(sch_n_r);
        TS_ASSERT_EQUALS(val, (char)-54);
    }
    {
        generic_var<char> val(uch_n_r);
        TS_ASSERT_EQUALS(val, '\104');
    }
    {
        generic_var<char> val(ssh_1_r);
        TS_ASSERT_EQUALS(val, '\104');
    }
    {
        generic_var<char> val(ssh_2_r);
        TS_ASSERT_EQUALS(val, (char)-54);
    }
    {
        generic_var<char> val(ush_1_r);
        TS_ASSERT_EQUALS(val, '\104');
    }
    {
        generic_var<char> val(sit_1_r);
        TS_ASSERT_EQUALS(val, '\104');
    }
    {
        generic_var<char> val(sit_2_r);
        TS_ASSERT_EQUALS(val, (char)-54);
    }
    {
        generic_var<char> val(uit_1_r);
        TS_ASSERT_EQUALS(val, '\104');
    }
    {
        generic_var<char> val(slg_1_r);
        TS_ASSERT_EQUALS(val, '\104');
    }
    {
        generic_var<char> val(slg_2_r);
        TS_ASSERT_EQUALS(val, (char)-54);
    }
    {
        generic_var<char> val(ulg_1_r);
        TS_ASSERT_EQUALS(val, '\104');
    }
    {
        generic_var<char> val(sll_1_r);
        TS_ASSERT_EQUALS(val, '\104');
    }
    {
        generic_var<char> val(sll_2_r);
        TS_ASSERT_EQUALS(val, (char)-54);
    }
    {
        generic_var<char> val(ull_1_r);
        TS_ASSERT_EQUALS(val, '\104');
    }

    {
        generic_var<unsigned char> val(sch_r);
        TS_ASSERT_EQUALS(val, '\104');
    }
    {
        generic_var<unsigned char> val(uch_r);
        TS_ASSERT_EQUALS(val, (unsigned char)'\364');
    }
    {
        generic_var<unsigned char> val(ssh_1_r);
        TS_ASSERT_EQUALS(val, '\104');
    }
    {
        generic_var<unsigned char> val(ush_1_r);
        TS_ASSERT_EQUALS(val, '\104');
    }
    {
        generic_var<unsigned char> val(sit_1_r);
        TS_ASSERT_EQUALS(val, '\104');
    }
    {
        generic_var<unsigned char> val(uit_1_r);
        TS_ASSERT_EQUALS(val, '\104');
    }
    {
        generic_var<unsigned char> val(slg_1_r);
        TS_ASSERT_EQUALS(val, '\104');
    }
    {
        generic_var<unsigned char> val(ulg_1_r);
        TS_ASSERT_EQUALS(val, '\104');
    }
    {
        generic_var<unsigned char> val(sll_1_r);
        TS_ASSERT_EQUALS(val, '\104');
    }
    {
        generic_var<unsigned char> val(ull_1_r);
        TS_ASSERT_EQUALS(val, '\104');
    }
    {
        generic_var<short> val(sch_r);
        TS_ASSERT_EQUALS(val, (short)68);
    }
    {
        generic_var<short> val(sch_n_r);
        TS_ASSERT_EQUALS(val, (short)-54);
    }
    {
        generic_var<short> val(uch_r);
        TS_ASSERT_EQUALS(val, (short)244);
    }
    {
        generic_var<short> val(ssh_r);
        TS_ASSERT_EQUALS(val, (short)31943);
    }
    {
        generic_var<short> val(ssh_n_r);
        TS_ASSERT_EQUALS(val, (short)-32593);
    }
    {
        generic_var<short> val(ush_n_r);
        TS_ASSERT_EQUALS(val, (short)31943);
    }
    {
        generic_var<short> val(sit_1_r);
        TS_ASSERT_EQUALS(val, (short)68);
    }
    {
        generic_var<short> val(sit_2_r);
        TS_ASSERT_EQUALS(val, (short)-54);
    }
    {
        generic_var<short> val(uit_1_r);
        TS_ASSERT_EQUALS(val, (short)68);
    }
    {
        generic_var<short> val(slg_1_r);
        TS_ASSERT_EQUALS(val, (short)68);
    }
    {
        generic_var<short> val(slg_2_r);
        TS_ASSERT_EQUALS(val, (short)-54);
    }
    {
        generic_var<short> val(ulg_1_r);
        TS_ASSERT_EQUALS(val, (short)68);
    }
    {
        generic_var<short> val(sll_1_r);
        TS_ASSERT_EQUALS(val, (short)68);
    }
    {
        generic_var<short> val(sll_2_r);
        TS_ASSERT_EQUALS(val, (short)-54);
    }
    {
        generic_var<short> val(ull_1_r);
        TS_ASSERT_EQUALS(val, (short)68);
    }
    {
        generic_var<unsigned short> val(sch_r);
        TS_ASSERT_EQUALS(val, (unsigned short)68U);
    }
    {
        generic_var<unsigned short> val(uch_r);
        TS_ASSERT_EQUALS(val, (unsigned short)244U);
    }
    {
        generic_var<unsigned short> val(ssh_r);
        TS_ASSERT_EQUALS(val, (unsigned short)31943U);
    }
    {
        generic_var<unsigned short> val(ush_r);
        TS_ASSERT_EQUALS(val, (unsigned short)54001U);
    }
    {
        generic_var<unsigned short> val(sit_1_r);
        TS_ASSERT_EQUALS(val, (unsigned short)68U);
    }
    {
        generic_var<unsigned short> val(uit_1_r);
        TS_ASSERT_EQUALS(val, (unsigned short)68U);
    }
    {
        generic_var<unsigned short> val(slg_1_r);
        TS_ASSERT_EQUALS(val, (unsigned short)68U);
    }
    {
        generic_var<unsigned short> val(ulg_1_r);
        TS_ASSERT_EQUALS(val, (unsigned short)68U);
    }
    {
        generic_var<unsigned short> val(sll_1_r);
        TS_ASSERT_EQUALS(val, (unsigned short)68U);
    }
    {
        generic_var<unsigned short> val(ull_1_r);
        TS_ASSERT_EQUALS(val, (unsigned short)68U);
    }
    {
        generic_var<int> val(sch_r);
        TS_ASSERT_EQUALS(val, 68);
    }
    {
        generic_var<int> val(sch_n_r);
        TS_ASSERT_EQUALS(val, -54);
    }
    {
        generic_var<int> val(uch_r);
        TS_ASSERT_EQUALS(val, 244);
    }
    {
        generic_var<int> val(ssh_r);
        TS_ASSERT_EQUALS(val, 31943);
    }
    {
        generic_var<int> val(ssh_n_r);
        TS_ASSERT_EQUALS(val, -32593);
    }
    {
        generic_var<int> val(ush_r);
        TS_ASSERT_EQUALS(val, 54001);
    }
    {
        generic_var<int> val(sit_r);
        TS_ASSERT_EQUALS(val, 268494041);
    }
    {
        generic_var<int> val(sit_n_r);
        TS_ASSERT_EQUALS(val, -341124346);
    }
    {
        generic_var<int> val(uit_n_r);
        TS_ASSERT_EQUALS(val, 268494041);
    }
    {
        generic_var<int> val(slg_r);
        TS_ASSERT_EQUALS(val, 428496782);
    }
    {
        generic_var<int> val(slg_n_r);
        TS_ASSERT_EQUALS(val, -198832255);
    }
    {
        generic_var<int> val(ulg_n_r);
        TS_ASSERT_EQUALS(val, 428496782);
    }
    {
        generic_var<int> val(sll_1_r);
        TS_ASSERT_EQUALS(val, 68);
    }
    {
        generic_var<int> val(sll_2_r);
        TS_ASSERT_EQUALS(val, -54);
    }
    {
        generic_var<int> val(ull_1_r);
        TS_ASSERT_EQUALS(val, 68);
    }
    {
        generic_var<unsigned int> val(sch_r);
        TS_ASSERT_EQUALS(val, 68U);
    }
    {
        generic_var<unsigned int> val(uch_r);
        TS_ASSERT_EQUALS(val, 244U);
    }
    {
        generic_var<unsigned int> val(ssh_r);
        TS_ASSERT_EQUALS(val, 31943U);
    }
    {
        generic_var<unsigned int> val(ush_r);
        TS_ASSERT_EQUALS(val, 54001U);
    }
    {
        generic_var<unsigned int> val(sit_r);
        TS_ASSERT_EQUALS(val, 268494041U);
    }
    {
        generic_var<unsigned int> val(uit_r);
        TS_ASSERT_EQUALS(val, 3467483669U);
    }
    {
        generic_var<unsigned int> val(slg_r);
        TS_ASSERT_EQUALS(val, 428496782U);
    }
    {
        generic_var<unsigned int> val(ulg_r);
        TS_ASSERT_EQUALS(val, 4269310731U);
    }
    {
        generic_var<unsigned int> val(sll_1_r);
        TS_ASSERT_EQUALS(val, 68U);
    }
    {
        generic_var<unsigned int> val(ull_1_r);
        TS_ASSERT_EQUALS(val, 68U);
    }
    {
        generic_var<long> val(sch_r);
        TS_ASSERT_EQUALS(val, 68L);
    }
    {
        generic_var<long> val(sch_n_r);
        TS_ASSERT_EQUALS(val, -54L);
    }
    {
        generic_var<long> val(uch_r);
        TS_ASSERT_EQUALS(val, 244L);
    }
    {
        generic_var<long> val(ssh_r);
        TS_ASSERT_EQUALS(val, 31943L);
    }
    {
        generic_var<long> val(ssh_n_r);
        TS_ASSERT_EQUALS(val, -32593L);
    }
    {
        generic_var<long> val(ush_r);
        TS_ASSERT_EQUALS(val, 54001L);
    }
    {
        generic_var<long> val(sit_r);
        TS_ASSERT_EQUALS(val, 268494041L);
    }
    {
        generic_var<long> val(sit_n_r);
        TS_ASSERT_EQUALS(val, -341124346L);
    }
    {
        generic_var<long> val(uit_n_r);
        TS_ASSERT_EQUALS(val, 268494041L);
    }
    {
        generic_var<long> val(slg_r);
        TS_ASSERT_EQUALS(val, 428496782L);
    }
    {
        generic_var<long> val(slg_n_r);
        TS_ASSERT_EQUALS(val, -198832255L);
    }
    {
        generic_var<long> val(ulg_n_r);
        TS_ASSERT_EQUALS(val, 428496782L);
    }
    {
        generic_var<long> val(sll_1_r);
        TS_ASSERT_EQUALS(val, 68L);
    }
    {
        generic_var<long> val(sll_2_r);
        TS_ASSERT_EQUALS(val, -54L);
    }
    {
        generic_var<long> val(ull_1_r);
        TS_ASSERT_EQUALS(val, 68L);
    }
    {
        generic_var<unsigned long> val(sch_r);
        TS_ASSERT_EQUALS(val, 68UL);
    }
    {
        generic_var<unsigned long> val(uch_r);
        TS_ASSERT_EQUALS(val, 244UL);
    }
    {
        generic_var<unsigned long> val(ssh_r);
        TS_ASSERT_EQUALS(val, 31943UL);
    }
    {
        generic_var<unsigned long> val(ush_r);
        TS_ASSERT_EQUALS(val, 54001UL);
    }
    {
        generic_var<unsigned long> val(sit_r);
        TS_ASSERT_EQUALS(val, 268494041UL);
    }
    {
        generic_var<unsigned long> val(uit_r);
        TS_ASSERT_EQUALS(val, 3467483669UL);
    }
    {
        generic_var<unsigned long> val(slg_r);
        TS_ASSERT_EQUALS(val, 428496782UL);
    }
    {
        generic_var<unsigned long> val(ulg_r);
        TS_ASSERT_EQUALS(val, 4269310731UL);
    }
    {
        generic_var<unsigned long> val(sll_1_r);
        TS_ASSERT_EQUALS(val, 68UL);
    }
    {
        generic_var<unsigned long> val(ull_1_r);
        TS_ASSERT_EQUALS(val, 68UL);
    }
    {
        generic_var<long long> val(sch_r);
        TS_ASSERT_EQUALS(val, 68LL);
    }
    {
        generic_var<long long> val(sch_n_r);
        TS_ASSERT_EQUALS(val, -54LL);
    }
    {
        generic_var<long long> val(uch_r);
        TS_ASSERT_EQUALS(val, 244LL);
    }
    {
        generic_var<long long> val(ssh_r);
        TS_ASSERT_EQUALS(val, 31943LL);
    }
    {
        generic_var<long long> val(ssh_n_r);
        TS_ASSERT_EQUALS(val, -32593LL);
    }
    {
        generic_var<long long> val(ush_r);
        TS_ASSERT_EQUALS(val, 54001LL);
    }
    {
        generic_var<long long> val(sit_r);
        TS_ASSERT_EQUALS(val, 268494041LL);
    }
    {
        generic_var<long long> val(sit_n_r);
        TS_ASSERT_EQUALS(val, -341124346LL);
    }
    {
        generic_var<long long> val(uit_n_r);
        TS_ASSERT_EQUALS(val, 268494041LL);
    }
    {
        generic_var<long long> val(slg_r);
        TS_ASSERT_EQUALS(val, 428496782LL);
    }
    {
        generic_var<long long> val(slg_n_r);
        TS_ASSERT_EQUALS(val, -198832255LL);
    }
    {
        generic_var<long long> val(ulg_n_r);
        TS_ASSERT_EQUALS(val, 428496782LL);
    }
    {
        generic_var<long long> val(sll_r);
        TS_ASSERT_EQUALS(val, 18014398967378304LL);
    }
    {
        generic_var<long long> val(sll_n_r);
        TS_ASSERT_EQUALS(val, -576460761068855588LL);
    }
    {
        generic_var<long long> val(ull_n_r);
        TS_ASSERT_EQUALS(val, 8223372036306943697LL);
    }
    {
        generic_var<unsigned long long> val(sch_r);
        TS_ASSERT_EQUALS(val, 68ULL);
    }
    {
        generic_var<unsigned long long> val(uch_r);
        TS_ASSERT_EQUALS(val, 244ULL);
    }
    {
        generic_var<unsigned long long> val(ssh_r);
        TS_ASSERT_EQUALS(val, 31943ULL);
    }
    {
        generic_var<unsigned long long> val(ush_r);
        TS_ASSERT_EQUALS(val, 54001ULL);
    }
    {
        generic_var<unsigned long long> val(sit_r);
        TS_ASSERT_EQUALS(val, 268494041ULL);
    }
    {
        generic_var<unsigned long long> val(uit_r);
        TS_ASSERT_EQUALS(val, 3467483669ULL);
    }
    {
        generic_var<unsigned long long> val(slg_r);
        TS_ASSERT_EQUALS(val, 428496782ULL);
    }
    {
        generic_var<unsigned long long> val(ulg_r);
        TS_ASSERT_EQUALS(val, 4269310731ULL);
    }
    {
        generic_var<unsigned long long> val(sll_r);
        TS_ASSERT_EQUALS(val, 18014398967378304ULL);
    }
    {
        generic_var<unsigned long long> val(ull_r);
        TS_ASSERT_EQUALS(val, 18446744073703655295ULL);
    }

    // Integers to integers (clip at min value)
    {
        generic_var<char> val(ssh_n_r);
        TS_ASSERT_EQUALS(val, CHAR_MIN);
    }
    {
        generic_var<char> val(sit_n_r);
        TS_ASSERT_EQUALS(val, CHAR_MIN);
    }
    {
        generic_var<char> val(slg_n_r);
        TS_ASSERT_EQUALS(val, CHAR_MIN);
    }
    {
        generic_var<char> val(sll_n_r);
        TS_ASSERT_EQUALS(val, CHAR_MIN);
    }
    {
        generic_var<unsigned char> val(sch_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<unsigned char> val(ssh_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<unsigned char> val(sit_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<unsigned char> val(slg_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<unsigned char> val(sll_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<short> val(sit_n_r);
        TS_ASSERT_EQUALS(val, SHRT_MIN);
    }
    {
        generic_var<short> val(slg_n_r);
        TS_ASSERT_EQUALS(val, SHRT_MIN);
    }
    {
        generic_var<short> val(sll_n_r);
        TS_ASSERT_EQUALS(val, SHRT_MIN);
    }
    {
        generic_var<unsigned short> val(sch_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<unsigned short> val(ssh_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<unsigned short> val(sit_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<unsigned short> val(slg_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<unsigned short> val(sll_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<int> val(sll_n_r);
        TS_ASSERT_EQUALS(val, LONG_MIN);
    }
    {
        generic_var<unsigned int> val(sch_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<unsigned int> val(ssh_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<unsigned int> val(sit_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<unsigned int> val(slg_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<unsigned int> val(sll_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<long> val(sll_n_r);
        TS_ASSERT_EQUALS(val, LONG_MIN);
    }
    {
        generic_var<unsigned long> val(sch_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<unsigned long> val(ssh_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<unsigned long> val(sit_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<unsigned long> val(slg_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<unsigned long> val(sll_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<unsigned long long> val(sch_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<unsigned long long> val(ssh_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<unsigned long long> val(sit_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<unsigned long long> val(slg_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<unsigned long long> val(sll_n_r);
        TS_ASSERT_EQUALS(val, 0);
    }

    // Integers to integers (clip at max value)
    {
        generic_var<char> val(uch_r);
        TS_ASSERT_EQUALS(val, CHAR_MAX);
    }
    {
        generic_var<char> val(ssh_r);
        TS_ASSERT_EQUALS(val, CHAR_MAX);
    }
    {
        generic_var<char> val(ush_r);
        TS_ASSERT_EQUALS(val, CHAR_MAX);
    }
    {
        generic_var<char> val(sit_r);
        TS_ASSERT_EQUALS(val, CHAR_MAX);
    }
    {
        generic_var<char> val(uit_r);
        TS_ASSERT_EQUALS(val, CHAR_MAX);
    }
    {
        generic_var<char> val(slg_r);
        TS_ASSERT_EQUALS(val, CHAR_MAX);
    }
    {
        generic_var<char> val(ulg_r);
        TS_ASSERT_EQUALS(val, CHAR_MAX);
    }
    {
        generic_var<char> val(sll_r);
        TS_ASSERT_EQUALS(val, CHAR_MAX);
    }
    {
        generic_var<char> val(ull_r);
        TS_ASSERT_EQUALS(val, CHAR_MAX);
    }
    {
        generic_var<unsigned char> val(ssh_r);
        TS_ASSERT_EQUALS(val, UCHAR_MAX);
    }
    {
        generic_var<unsigned char> val(ush_r);
        TS_ASSERT_EQUALS(val, UCHAR_MAX);
    }
    {
        generic_var<unsigned char> val(sit_r);
        TS_ASSERT_EQUALS(val, UCHAR_MAX);
    }
    {
        generic_var<unsigned char> val(uit_r);
        TS_ASSERT_EQUALS(val, UCHAR_MAX);
    }
    {
        generic_var<unsigned char> val(slg_r);
        TS_ASSERT_EQUALS(val, UCHAR_MAX);
    }
    {
        generic_var<unsigned char> val(ulg_r);
        TS_ASSERT_EQUALS(val, UCHAR_MAX);
    }
    {
        generic_var<unsigned char> val(sll_r);
        TS_ASSERT_EQUALS(val, UCHAR_MAX);
    }
    {
        generic_var<unsigned char> val(ull_r);
        TS_ASSERT_EQUALS(val, UCHAR_MAX);
    }
    {
        generic_var<short> val(ush_r);
        TS_ASSERT_EQUALS(val, SHRT_MAX);
    }
    {
        generic_var<short> val(sit_r);
        TS_ASSERT_EQUALS(val, SHRT_MAX);
    }
    {
        generic_var<short> val(uit_r);
        TS_ASSERT_EQUALS(val, SHRT_MAX);
    }
    {
        generic_var<short> val(slg_r);
        TS_ASSERT_EQUALS(val, SHRT_MAX);
    }
    {
        generic_var<short> val(ulg_r);
        TS_ASSERT_EQUALS(val, SHRT_MAX);
    }
    {
        generic_var<short> val(sll_r);
        TS_ASSERT_EQUALS(val, SHRT_MAX);
    }
    {
        generic_var<short> val(ull_r);
        TS_ASSERT_EQUALS(val, SHRT_MAX);
    }
    {
        generic_var<unsigned short> val(sit_r);
        TS_ASSERT_EQUALS(val, USHRT_MAX);
    }
    {
        generic_var<unsigned short> val(uit_r);
        TS_ASSERT_EQUALS(val, USHRT_MAX);
    }
    {
        generic_var<unsigned short> val(slg_r);
        TS_ASSERT_EQUALS(val, USHRT_MAX);
    }
    {
        generic_var<unsigned short> val(ulg_r);
        TS_ASSERT_EQUALS(val, USHRT_MAX);
    }
    {
        generic_var<unsigned short> val(sll_r);
        TS_ASSERT_EQUALS(val, USHRT_MAX);
    }
    {
        generic_var<unsigned short> val(ull_r);
        TS_ASSERT_EQUALS(val, USHRT_MAX);
    }
    {
        generic_var<int> val(uit_r);
        TS_ASSERT_EQUALS(val, LONG_MAX);
    }
    {
        generic_var<int> val(ulg_r);
        TS_ASSERT_EQUALS(val, LONG_MAX);
    }
    {
        generic_var<int> val(sll_r);
        TS_ASSERT_EQUALS(val, LONG_MAX);
    }
    {
        generic_var<int> val(ull_r);
        TS_ASSERT_EQUALS(val, LONG_MAX);
    }
    {
        generic_var<unsigned int> val(sll_r);
        TS_ASSERT_EQUALS(val, ULONG_MAX);
    }
    {
        generic_var<unsigned int> val(ull_r);
        TS_ASSERT_EQUALS(val, ULONG_MAX);
    }
    {
        generic_var<long> val(uit_r);
        TS_ASSERT_EQUALS(val, LONG_MAX);
    }
    {
        generic_var<long> val(ulg_r);
        TS_ASSERT_EQUALS(val, LONG_MAX);
    }
    {
        generic_var<long> val(sll_r);
        TS_ASSERT_EQUALS(val, LONG_MAX);
    }
    {
        generic_var<long> val(ull_r);
        TS_ASSERT_EQUALS(val, LONG_MAX);
    }
    {
        generic_var<unsigned long> val(sll_r);
        TS_ASSERT_EQUALS(val, ULONG_MAX);
    }
    {
        generic_var<unsigned long> val(ull_r);
        TS_ASSERT_EQUALS(val, ULONG_MAX);
    }
    {
        generic_var<long long> val(ull_r);
        TS_ASSERT_EQUALS(val, LLONG_MAX);
    }

    // Floats to integers (round down)
    generic_var<float> flt_1(118.42f);
    generic_var<double> dbl_1(84.392);
    const value_var &flt_1_r = flt_1;
    const value_var &dbl_1_r = dbl_1;
    {
        generic_var<char> val(flt_1_r);
        TS_ASSERT_EQUALS(val, 118);
    }
    {
        generic_var<unsigned char> val(flt_1_r);
        TS_ASSERT_EQUALS(val, 118);
    }
    {
        generic_var<short> val(flt_1_r);
        TS_ASSERT_EQUALS(val, 118);
    }
    {
        generic_var<unsigned short> val(flt_1_r);
        TS_ASSERT_EQUALS(val, 118);
    }
    {
        generic_var<int> val(flt_1_r);
        TS_ASSERT_EQUALS(val, 118);
    }
    {
        generic_var<unsigned int> val(flt_1_r);
        TS_ASSERT_EQUALS(val, 118);
    }
    {
        generic_var<long> val(flt_1_r);
        TS_ASSERT_EQUALS(val, 118);
    }
    {
        generic_var<unsigned long> val(flt_1_r);
        TS_ASSERT_EQUALS(val, 118);
    }
    {
        generic_var<long long> val(flt_1_r);
        TS_ASSERT_EQUALS(val, 118);
    }
    {
        generic_var<unsigned long long> val(flt_1_r);
        TS_ASSERT_EQUALS(val, 118);
    }
    {
        generic_var<char> val(dbl_1_r);
        TS_ASSERT_EQUALS(val, 84);
    }
    {
        generic_var<unsigned char> val(dbl_1_r);
        TS_ASSERT_EQUALS(val, 84);
    }
    {
        generic_var<short> val(dbl_1_r);
        TS_ASSERT_EQUALS(val, 84);
    }
    {
        generic_var<unsigned short> val(dbl_1_r);
        TS_ASSERT_EQUALS(val, 84);
    }
    {
        generic_var<int> val(dbl_1_r);
        TS_ASSERT_EQUALS(val, 84);
    }
    {
        generic_var<unsigned int> val(dbl_1_r);
        TS_ASSERT_EQUALS(val, 84);
    }
    {
        generic_var<long> val(dbl_1_r);
        TS_ASSERT_EQUALS(val, 84);
    }
    {
        generic_var<unsigned long> val(dbl_1_r);
        TS_ASSERT_EQUALS(val, 84);
    }
    {
        generic_var<long long> val(dbl_1_r);
        TS_ASSERT_EQUALS(val, 84);
    }
    {
        generic_var<unsigned long long> val(dbl_1_r);
        TS_ASSERT_EQUALS(val, 84);
    }

    // Floats to integers (round up)
    generic_var<float> flt_2(117.844f);
    generic_var<double> dbl_2(83.721);
    const value_var &flt_2_r = flt_2;
    const value_var &dbl_2_r = dbl_2;
    {
        generic_var<char> val(flt_2_r);
        TS_ASSERT_EQUALS(val, 118);
    }
    {
        generic_var<unsigned char> val(flt_2_r);
        TS_ASSERT_EQUALS(val, 118);
    }
    {
        generic_var<short> val(flt_2_r);
        TS_ASSERT_EQUALS(val, 118);
    }
    {
        generic_var<unsigned short> val(flt_2_r);
        TS_ASSERT_EQUALS(val, 118);
    }
    {
        generic_var<int> val(flt_2_r);
        TS_ASSERT_EQUALS(val, 118);
    }
    {
        generic_var<unsigned int> val(flt_2_r);
        TS_ASSERT_EQUALS(val, 118);
    }
    {
        generic_var<long> val(flt_2_r);
        TS_ASSERT_EQUALS(val, 118);
    }
    {
        generic_var<unsigned long> val(flt_2_r);
        TS_ASSERT_EQUALS(val, 118);
    }
    {
        generic_var<long long> val(flt_2_r);
        TS_ASSERT_EQUALS(val, 118);
    }
    {
        generic_var<unsigned long long> val(flt_2_r);
        TS_ASSERT_EQUALS(val, 118);
    }
    {
        generic_var<char> val(dbl_2_r);
        TS_ASSERT_EQUALS(val, 84);
    }
    {
        generic_var<unsigned char> val(dbl_2_r);
        TS_ASSERT_EQUALS(val, 84);
    }
    {
        generic_var<short> val(dbl_2_r);
        TS_ASSERT_EQUALS(val, 84);
    }
    {
        generic_var<unsigned short> val(dbl_2_r);
        TS_ASSERT_EQUALS(val, 84);
    }
    {
        generic_var<int> val(dbl_2_r);
        TS_ASSERT_EQUALS(val, 84);
    }
    {
        generic_var<unsigned int> val(dbl_2_r);
        TS_ASSERT_EQUALS(val, 84);
    }
    {
        generic_var<long> val(dbl_2_r);
        TS_ASSERT_EQUALS(val, 84);
    }
    {
        generic_var<unsigned long> val(dbl_2_r);
        TS_ASSERT_EQUALS(val, 84);
    }
    {
        generic_var<long long> val(dbl_2_r);
        TS_ASSERT_EQUALS(val, 84);
    }
    {
        generic_var<unsigned long long> val(dbl_2_r);
        TS_ASSERT_EQUALS(val, 84);
    }

    // Floats to integers (overflow)
    generic_var<float> flt_3(1.e21f);
    generic_var<double> dbl_3(1.e21);
    const value_var &flt_3_r = flt_3;
    const value_var &dbl_3_r = dbl_3;

    {
        generic_var<char> val(flt_3_r);
        TS_ASSERT_EQUALS(val, CHAR_MAX);
    }
    {
        generic_var<unsigned char> val(flt_3_r);
        TS_ASSERT_EQUALS(val, UCHAR_MAX);
    }
    {
        generic_var<short> val(flt_3_r);
        TS_ASSERT_EQUALS(val, SHRT_MAX);
    }
    {
        generic_var<unsigned short> val(flt_3_r);
        TS_ASSERT_EQUALS(val, USHRT_MAX);
    }
    {
        generic_var<int> val(flt_3_r);
        TS_ASSERT_EQUALS(val, INT_MAX);
    }
    {
        generic_var<unsigned int> val(flt_3_r);
        TS_ASSERT_EQUALS(val, UINT_MAX);
    }
    {
        generic_var<long> val(flt_3_r);
        TS_ASSERT_EQUALS(val, LONG_MAX);
    }
    {
        generic_var<unsigned long> val(flt_3_r);
        TS_ASSERT_EQUALS(val, ULONG_MAX);
    }
    {
        generic_var<long long> val(flt_3_r);
        TS_ASSERT_EQUALS(val, LLONG_MAX);
    }
    {
        generic_var<unsigned long long> val(flt_3_r);
        TS_ASSERT_EQUALS(val, ULLONG_MAX);
    }
    {
        generic_var<char> val(dbl_3_r);
        TS_ASSERT_EQUALS(val, CHAR_MAX);
    }
    {
        generic_var<unsigned char> val(dbl_3_r);
        TS_ASSERT_EQUALS(val, UCHAR_MAX);
    }
    {
        generic_var<short> val(dbl_3_r);
        TS_ASSERT_EQUALS(val, SHRT_MAX);
    }
    {
        generic_var<unsigned short> val(dbl_3_r);
        TS_ASSERT_EQUALS(val, USHRT_MAX);
    }
    {
        generic_var<int> val(dbl_3_r);
        TS_ASSERT_EQUALS(val, INT_MAX);
    }
    {
        generic_var<unsigned int> val(dbl_3_r);
        TS_ASSERT_EQUALS(val, UINT_MAX);
    }
    {
        generic_var<long> val(dbl_3_r);
        TS_ASSERT_EQUALS(val, LONG_MAX);
    }
    {
        generic_var<unsigned long> val(dbl_3_r);
        TS_ASSERT_EQUALS(val, ULONG_MAX);
    }
    {
        generic_var<long long> val(dbl_3_r);
        TS_ASSERT_EQUALS(val, LLONG_MAX);
    }
    {
        generic_var<unsigned long long> val(dbl_3_r);
        TS_ASSERT_EQUALS(val, ULLONG_MAX);
    }

    // Floats to integers (underflow)
    generic_var<float> flt_4(-1.e21f);
    generic_var<double> dbl_4(-1.e21);
    const value_var &flt_4_r = flt_4;
    const value_var &dbl_4_r = dbl_4;

    {
        generic_var<char> val(flt_4_r);
        TS_ASSERT_EQUALS(val, CHAR_MIN);
    }
    {
        generic_var<unsigned char> val(flt_4_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<short> val(flt_4_r);
        TS_ASSERT_EQUALS(val, SHRT_MIN);
    }
    {
        generic_var<unsigned short> val(flt_4_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<int> val(flt_4_r);
        TS_ASSERT_EQUALS(val, INT_MIN);
    }
    {
        generic_var<unsigned int> val(flt_4_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<long> val(flt_4_r);
        TS_ASSERT_EQUALS(val, LONG_MIN);
    }
    {
        generic_var<unsigned long> val(flt_4_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<long long> val(flt_4_r);
        TS_ASSERT_EQUALS(val, LLONG_MIN);
    }
    {
        generic_var<unsigned long long> val(flt_4_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<char> val(dbl_4_r);
        TS_ASSERT_EQUALS(val, CHAR_MIN);
    }
    {
        generic_var<unsigned char> val(dbl_4_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<short> val(dbl_4_r);
        TS_ASSERT_EQUALS(val, SHRT_MIN);
    }
    {
        generic_var<unsigned short> val(dbl_4_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<int> val(dbl_4_r);
        TS_ASSERT_EQUALS(val, INT_MIN);
    }
    {
        generic_var<unsigned int> val(dbl_4_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<long> val(dbl_4_r);
        TS_ASSERT_EQUALS(val, LONG_MIN);
    }
    {
        generic_var<unsigned long> val(dbl_4_r);
        TS_ASSERT_EQUALS(val, 0);
    }
    {
        generic_var<long long> val(dbl_4_r);
        TS_ASSERT_EQUALS(val, LLONG_MIN);
    }
    {
        generic_var<unsigned long long> val(dbl_4_r);
        TS_ASSERT_EQUALS(val, 0);
    }

    // Integers to float
    {
        generic_var<float> val(sch_r);
        TS_ASSERT_EQUALS(val, 68.f);
    }
    {
        generic_var<float> val(sch_n_r);
        TS_ASSERT_EQUALS(val, -54.f);
    }
    {
        generic_var<float> val(uch);
        TS_ASSERT_EQUALS(val, 244.f);
    }
    {
        generic_var<float> val(uch_n_r);
        TS_ASSERT_EQUALS(val, 68.f);
    }
    {
        generic_var<float> val(ssh_r);
        TS_ASSERT_EQUALS(val, 31943.f);
    }
    {
        generic_var<float> val(ssh_n_r);
        TS_ASSERT_EQUALS(val, -32593.f);
    }
    {
        generic_var<float> val(ush_r);
        TS_ASSERT_EQUALS(val, 54001.f);
    }
    {
        generic_var<float> val(ush_n_r);
        TS_ASSERT_EQUALS(val, 31943.f);
    }
    {
        generic_var<float> val(sit_r);
        TS_ASSERT_EQUALS(val, 268494041.f);
    }
    {
        generic_var<float> val(sit_n_r);
        TS_ASSERT_EQUALS(val, -341124346.f);
    }
    {
        generic_var<float> val(uit_r);
        TS_ASSERT_EQUALS(val, 3467483669.f);
    }
    {
        generic_var<float> val(uit_n_r);
        TS_ASSERT_EQUALS(val, 268494041.f);
    }
    {
        generic_var<float> val(slg_r);
        TS_ASSERT_EQUALS(val, 428496782.f);
    }
    {
        generic_var<float> val(slg_n_r);
        TS_ASSERT_EQUALS(val, -198832255.f);
    }
    {
        generic_var<float> val(ulg_r);
        TS_ASSERT_EQUALS(val, 4269310731.f);
    }
    {
        generic_var<float> val(ulg_n_r);
        TS_ASSERT_EQUALS(val, 428496782.f);
    }
    {
        generic_var<float> val(sll_r);
        TS_ASSERT_EQUALS(val, 18014398967378304.f);
    }
    {
        generic_var<float> val(sll_n_r);
        TS_ASSERT_EQUALS(val, -576460761068855588.f);
    }
    {
        generic_var<float> val(ull_r);
        TS_ASSERT_EQUALS(val, 18446744073703655295.f);
    }
    {
        generic_var<float> val(ull_n_r);
        TS_ASSERT_EQUALS(val, 8223372036306943697.f);
    }

    {
        generic_var<double> val(sch_r);
        TS_ASSERT_EQUALS(val, 68.);
    }
    {
        generic_var<double> val(sch_n_r);
        TS_ASSERT_EQUALS(val, -54.);
    }
    {
        generic_var<double> val(uch);
        TS_ASSERT_EQUALS(val, 244.);
    }
    {
        generic_var<double> val(uch_n_r);
        TS_ASSERT_EQUALS(val, 68.);
    }
    {
        generic_var<double> val(ssh_r);
        TS_ASSERT_EQUALS(val, 31943.);
    }
    {
        generic_var<double> val(ssh_n_r);
        TS_ASSERT_EQUALS(val, -32593.);
    }
    {
        generic_var<double> val(ush_r);
        TS_ASSERT_EQUALS(val, 54001.);
    }
    {
        generic_var<double> val(ush_n_r);
        TS_ASSERT_EQUALS(val, 31943.);
    }
    {
        generic_var<double> val(sit_r);
        TS_ASSERT_EQUALS(val, 268494041.);
    }
    {
        generic_var<double> val(sit_n_r);
        TS_ASSERT_EQUALS(val, -341124346.);
    }
    {
        generic_var<double> val(uit_r);
        TS_ASSERT_EQUALS(val, 3467483669.);
    }
    {
        generic_var<double> val(uit_n_r);
        TS_ASSERT_EQUALS(val, 268494041.);
    }
    {
        generic_var<double> val(slg_r);
        TS_ASSERT_EQUALS(val, 428496782.);
    }
    {
        generic_var<double> val(slg_n_r);
        TS_ASSERT_EQUALS(val, -198832255.);
    }
    {
        generic_var<double> val(ulg_r);
        TS_ASSERT_EQUALS(val, 4269310731.);
    }
    {
        generic_var<double> val(ulg_n_r);
        TS_ASSERT_EQUALS(val, 428496782.);
    }
    {
        generic_var<double> val(sll_r);
        TS_ASSERT_EQUALS(val, 18014398967378304.);
    }
    {
        generic_var<double> val(sll_n_r);
        TS_ASSERT_EQUALS(val, -576460761068855588.);
    }
    {
        generic_var<double> val(ull_r);
        TS_ASSERT_EQUALS(val, 18446744073703655295.);
    }
    {
        generic_var<double> val(ull_n_r);
        TS_ASSERT_EQUALS(val, 8223372036306943697.);
    }

    TS_ASSERT_EQUALS(sch.rtti(), sch_n.rtti());
    TS_ASSERT_EQUALS(uch.rtti(), uch_n.rtti());
    TS_ASSERT_EQUALS(ssh.rtti(), ssh_n.rtti());
    TS_ASSERT_EQUALS(ush.rtti(), ush_n.rtti());
    TS_ASSERT_EQUALS(sit.rtti(), sit_n.rtti());
    TS_ASSERT_EQUALS(uit.rtti(), uit_n.rtti());
    TS_ASSERT_EQUALS(slg.rtti(), slg_n.rtti());
    TS_ASSERT_EQUALS(ulg.rtti(), ulg_n.rtti());
    TS_ASSERT_EQUALS(sll.rtti(), sll_n.rtti());
    TS_ASSERT_EQUALS(ull.rtti(), ull_n.rtti());
    TS_ASSERT_DIFFERS(sch.rtti(), uch_n.rtti());
    TS_ASSERT_DIFFERS(sch.rtti(), ssh_n.rtti());
    TS_ASSERT_DIFFERS(sch.rtti(), ush_n.rtti());
    TS_ASSERT_DIFFERS(sch.rtti(), sit_n.rtti());
    TS_ASSERT_DIFFERS(sch.rtti(), uit_n.rtti());
    TS_ASSERT_DIFFERS(sch.rtti(), slg_n.rtti());
    TS_ASSERT_DIFFERS(sch.rtti(), ulg_n.rtti());
    TS_ASSERT_DIFFERS(sch.rtti(), sll_n.rtti());
    TS_ASSERT_DIFFERS(sch.rtti(), ull_n.rtti());
    TS_ASSERT_DIFFERS(uch.rtti(), ssh_n.rtti());
    TS_ASSERT_DIFFERS(uch.rtti(), ush_n.rtti());
    TS_ASSERT_DIFFERS(uch.rtti(), sit_n.rtti());
    TS_ASSERT_DIFFERS(uch.rtti(), uit_n.rtti());
    TS_ASSERT_DIFFERS(uch.rtti(), slg_n.rtti());
    TS_ASSERT_DIFFERS(uch.rtti(), ulg_n.rtti());
    TS_ASSERT_DIFFERS(uch.rtti(), sll_n.rtti());
    TS_ASSERT_DIFFERS(uch.rtti(), ull_n.rtti());
    TS_ASSERT_DIFFERS(ssh.rtti(), ush_n.rtti());
    TS_ASSERT_DIFFERS(ssh.rtti(), sit_n.rtti());
    TS_ASSERT_DIFFERS(ssh.rtti(), uit_n.rtti());
    TS_ASSERT_DIFFERS(ssh.rtti(), slg_n.rtti());
    TS_ASSERT_DIFFERS(ssh.rtti(), ulg_n.rtti());
    TS_ASSERT_DIFFERS(ssh.rtti(), sll_n.rtti());
    TS_ASSERT_DIFFERS(ssh.rtti(), ull_n.rtti());
    TS_ASSERT_DIFFERS(ush.rtti(), sit_n.rtti());
    TS_ASSERT_DIFFERS(ush.rtti(), uit_n.rtti());
    TS_ASSERT_DIFFERS(ush.rtti(), slg_n.rtti());
    TS_ASSERT_DIFFERS(ush.rtti(), ulg_n.rtti());
    TS_ASSERT_DIFFERS(ush.rtti(), sll_n.rtti());
    TS_ASSERT_DIFFERS(ush.rtti(), ull_n.rtti());
    TS_ASSERT_DIFFERS(sit.rtti(), uit_n.rtti());
    TS_ASSERT_DIFFERS(sit.rtti(), slg_n.rtti());
    TS_ASSERT_DIFFERS(sit.rtti(), ulg_n.rtti());
    TS_ASSERT_DIFFERS(sit.rtti(), sll_n.rtti());
    TS_ASSERT_DIFFERS(sit.rtti(), ull_n.rtti());
    TS_ASSERT_DIFFERS(uit.rtti(), slg_n.rtti());
    TS_ASSERT_DIFFERS(uit.rtti(), ulg_n.rtti());
    TS_ASSERT_DIFFERS(uit.rtti(), sll_n.rtti());
    TS_ASSERT_DIFFERS(uit.rtti(), ull_n.rtti());
    TS_ASSERT_DIFFERS(slg.rtti(), ulg_n.rtti());
    TS_ASSERT_DIFFERS(slg.rtti(), sll_n.rtti());
    TS_ASSERT_DIFFERS(slg.rtti(), ull_n.rtti());
    TS_ASSERT_DIFFERS(ulg.rtti(), sll_n.rtti());
    TS_ASSERT_DIFFERS(ulg.rtti(), ull_n.rtti());
    TS_ASSERT_DIFFERS(sll.rtti(), ull_n.rtti());

    TS_TRACE("End");
}

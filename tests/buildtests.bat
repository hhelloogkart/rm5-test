@ECHO OFF
:: set QTDIR
:: set PATH for python, qmake, VC++ compilation environment
setlocal enabledelayedexpansion enableextensions
FOR /D %%G IN ("test_*") DO (
ECHO %%G
python.exe cxxtest\bin\cxxtestgen --runner=XmlPrinter --world=%%G -o %%G\runner.cpp %%G\%%G.h
cd %%G
qmake.exe %%G.pro
nmake -f Makefile.Debug
cd ..
)
cd runutil
qmake runutil.pro
nmake -f Makefile.Debug
cd ..\..
qmake -o rm5tcp_static.mak rm5tcp_static.pro
nmake -f rm5tcp_static.mak.Debug
cd tests
nuget install "extentreports-dotnet-cli\ExtentReportsDotNetCLI\ExtentReportsDotNetCLI\packages.config" -OutputDirectory "extentreports-dotnet-cli\ExtentReportsDotNetCLI\packages"
devenv "extentreports-dotnet-cli\ExtentReportsDotNetCLI\ExtentReportsDotNetCLI.sln" /build Debug

TARGET = test_protobuf_lite

DEFINES += PROTOBUF_USE_DLLS
GP=../test_protobuf/google/protobuf

HEADERS += \
  $${GP}/test_util_lite.h

SOURCES +=  \
  $${GP}/test_util_lite.cc                            \
  $${GP}/lite_unittest.cc                             \
  $${GP}/unittest_lite.pb.cc                          \
  $${GP}/unittest_import_lite.pb.cc                   \
  $${GP}/unittest_import_public_lite.pb.cc

INCLUDEPATH += ../test_protobuf ../../include ../../src

include (../tests.pri)
SOURCES -= runner.cpp
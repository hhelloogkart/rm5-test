TEMPLATE = app
CONFIG += debug warn_on console
CONFIG -= qt

CONFIG(debug, debug|release) {
    LIB_SUFFIX=d
}

DEPENDPATH += ../../include ../../../oscore/include
INCLUDEPATH += . ../../include ../../../oscore/include $$PWD/cxxtest

OBJECTS_DIR = .objs
unix:LIBS += -L../../lib -lrm -L../../../oscore/lib -loscore
win32{
 win32-msvc2013 | win32-msvc2015 {
  LIBS += ../../lib/rm$${LIB_SUFFIX}5.lib ../../../oscore/lib/oscore$${LIB_SUFFIX}1.lib
 } else:win32-msvc2008 {
  LIBS += ../../lib/rm5.lib ../../../oscore/lib/oscore1.lib
  QMAKE_CXXFLAGS -= -Zc:wchar_t-
  QMAKE_CXXFLAGS *= /Zc:wchar_t
 } else {
  LIBS += ../../lib/rm50.lib ../../../oscore/lib/oscore10.lib
 }
}

# Platform Defines
win32:DEFINES -= _MBCS UNICODE
win32-msvc:QMAKE_CFLAGS += /MDd /Zi
win32-msvc:QMAKE_CXXFLAGS += /GX /GR /MDd /Zi
win32-msvc:QMAKE_CXXFLAGS -= /GZ /ZI
win32-msvc*:DEFINES += _CRT_SECURE_NO_WARNINGS
linux-icc:QMAKE_LFLAGS += -cxxlib-icc
linux-icc:QMAKE_CXXFLAGS += -cxxlib-icc
linux-icc:QMAKE_CFLAGS += -cxxlib-icc
linux-g++:QMAKE_LFLAGS += -Wl,-rpath,$(QTDIR)/lib
linux-icc:QMAKE_LFLAGS += -Qoption,ld,-rpath,$(QTDIR)/lib
hpux-acc:DEFINES += _HPUX_SOURCE
irix-cc:QMAKE_CXXFLAGS += -LANG:std
tru64-cxx:DEFINES += __SMALL_ENDIAN_ __USE_STD_IOSTREAM
linux-icc {
  CONFIG -= debug
  CONFIG += release
}

cxxtarget.target = runner.cpp
cxxtarget.depends = $${HEADERS}
win32 {
  cxxtarget.commands = perl -w $$(QTDIR)\..\cxxtest\cxxtestgen.pl --error-printer -o runner.cpp $${HEADERS}
} else {
  cxxtarget.commands = perl -w $$(QTDIR)/../cxxtest/cxxtestgen.pl --error-printer -o runner.cpp $${HEADERS}
}

QMAKE_EXTRA_TARGETS += cxxtarget

SOURCES += runner.cpp
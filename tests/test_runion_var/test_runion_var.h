#ifndef _TEST_UNION_VAR_H_DEF_
#define _TEST_UNION_VAR_H_DEF_

#include <cxxtest/TestSuite.h>

class UnionVarTestSuite : public CxxTest::TestSuite
{
public:
    void testWrite(void);
    void testWriteShow(void);
    void testSelector(void);
    void testStream(void);
    void testAssignAndCopy();
    void testInvalidSelector();

private:
    void writeRM(int);
    void readRM(int);
    void setData();
    void clearData();
    void checkData(int);
};

#endif


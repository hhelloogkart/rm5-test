#include <sstream>
#include <stdio.h>
#include <outbuf.h>
#include <gen_var.h>
#include <union_var.h>
#include <link_var.h>
#include "test_runion_var.h"

const int TRANSFER_BUFFER_DEPTH = 10;

class dirty_checker : public mpt_baseshow
{
public:
    dirty_checker(mpt_var *myvar);
    virtual void setDirty(mpt_var *myvar);
    virtual void notDirty(mpt_var *myvar);
    int getStateAndReset();
protected:
    int state; // 0 - not called, 1 - dirty, 2 - notdirty, 3 - error
    mpt_var *var;
};

dirty_checker::dirty_checker(mpt_var *myvar) : state(0), var(myvar)
{
    myvar->addCallback(this);
}

void dirty_checker::setDirty(mpt_var *myvar)
{
    if (myvar == var) state = 1;
    else state = 3;
}

void dirty_checker::notDirty(mpt_var *myvar)
{
    if (myvar == var) state = 2;
    else state = 3;
}

int dirty_checker::getStateAndReset()
{
    int ret = state;
    state = 0;
    return ret;
}

struct u1_typ : public r_union_var
{
    RUNIONCONST( u1_typ, 4, sizeof(char), NULL )

    struct c1_typ : public complex_var
    {
        COMPLEXCONST( c1_typ );
        generic_var<int> a;
        generic_var<unsigned char> selector;
        generic_var<unsigned short> b;
    } c1;
    struct c2_typ : public complex_var
    {
        COMPLEXCONST( c2_typ );
        generic_var<unsigned short> a;
        generic_var<unsigned short> b;
        generic_var<unsigned char> selector;
        generic_var<int> c;
    } c2;
    struct c3_typ : public complex_var
    {
        COMPLEXCONST( c3_typ );
        generic_var<float> a;
        generic_var<unsigned char> selector;
        generic_var<short> b;
    } c3;
};

struct u2_typ : public r_union_var
{
    RUNIONCONST( u2_typ, 4, sizeof(int), "2, 1, 0" )

    struct c1_typ : public complex_var
    {
        COMPLEXCONST( c1_typ );
        generic_var<int> a;
        generic_var<int> selector;
        generic_var<unsigned short> b;
    } c1;
    struct c2_typ : public complex_var
    {
        COMPLEXCONST( c2_typ );
        generic_var<unsigned short> a;
        generic_var<unsigned short> b;
        generic_var<int> selector;
        generic_var<int> c;
    } c2;
    struct c3_typ : public complex_var
    {
        COMPLEXCONST( c3_typ );
        generic_var<float> a;
        generic_var<int> selector;
        generic_var<short> b;
    } c3;
};

struct u4_typ : public r_union_var
{
    RUNIONCONST( u4_typ, 0, sizeof(int), "2, 3" )

    struct c1_typ : public complex_var
    {
        COMPLEXCONST( c1_typ );
        generic_var<int> selector;
        generic_var<int> a;
    } c1;
    struct c2_typ : public complex_var
    {
        COMPLEXCONST( c2_typ );
        generic_var<int> selector;
        generic_var<int> a;
    } c2;
    struct c3_typ : public complex_var
    {
        COMPLEXCONST( c3_typ );
        generic_var<int> selector;
        generic_var<int> a;
    } c3;
};

struct test_wr_typ : public complex_var
{
public:
    COMPLEXCONST( test_wr_typ );

    void read();
    void write();
    void setRMDirty();
    bool isRMDirty() const;

    generic_var<unsigned int> a;
    struct u1_typ u1;
    generic_var<unsigned int> b;
    struct u2_typ u2;
    generic_var<unsigned int> c;
    struct u4_typ u4;

private:
    bool dirty;

    static unsigned char *transfer_buffer[TRANSFER_BUFFER_DEPTH];
    static int transfer_buffer_size[TRANSFER_BUFFER_DEPTH];

};

void test_wr_typ::setRMDirty()
{
    dirty = true;
}

bool test_wr_typ::isRMDirty() const
{
    return dirty;
}

unsigned char *test_wr_typ::transfer_buffer[TRANSFER_BUFFER_DEPTH] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
int test_wr_typ::transfer_buffer_size[TRANSFER_BUFFER_DEPTH] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

void test_wr_typ::read()
{
    for (int i = 0; i < TRANSFER_BUFFER_DEPTH; ++i)
        if (transfer_buffer_size[i] && transfer_buffer[i])
        {
            extract(transfer_buffer_size[i], transfer_buffer[i]);
            delete [] transfer_buffer[i];
            transfer_buffer[i] = 0;
            transfer_buffer_size[i] = 0;
            break;
        }
}

void test_wr_typ::write()
{
    if (dirty)
    {
        dirty = false;
        for (int i = 0; i < TRANSFER_BUFFER_DEPTH; ++i)
            if (!(transfer_buffer_size[i] && transfer_buffer[i]))
            {
                const int len = size();
                transfer_buffer[i] = new unsigned char[len];
                outbuf ob;
                ob.set(transfer_buffer[i], len);
                output(ob);
                transfer_buffer_size[i] = ob.size();
                break;
            }
    }
}

struct test_wr_typ test_wr_var;

void filldata(complex_var &c)
{
    for (int idx = 0; idx < c.cardinal(); ++idx)
        static_cast<value_var *>(c[idx])->fromValue(rand());
}

template<class C>
void appendbuffer(outbuf &ob, C value)
{
    unsigned char *b = reinterpret_cast<unsigned char *>(&value);
    for (int idx = 0; idx < sizeof(C); ++idx, ++b)
        ob += *b;
}

void UnionVarTestSuite::testWrite(void)
{
    writeRM(10);
    readRM(10);
}

void UnionVarTestSuite::testWriteShow(void)
{
    /* the union packet transfer progression */
    for (int i=9; i>0; --i)
    {
        writeRM(i);
        readRM(i);
    }
}

void UnionVarTestSuite::testSelector(void)
{
    //set selector
    test_wr_var.u1.setSelector(0, 0x1);
    test_wr_var.u1.setSelector(1, 0x2);
    test_wr_var.u1.setSelector(2, 0x3);
    TS_ASSERT_EQUALS(test_wr_var.u1.getSelector(0), 1);
    TS_ASSERT_EQUALS(test_wr_var.u1.getSelector(1), 2);
    TS_ASSERT_EQUALS(test_wr_var.u1.getSelector(2), 3);

    test_wr_var.u2.setSelector(0, 0x11111111);
    test_wr_var.u2.setSelector(1, 0x22222222);
    test_wr_var.u2.setSelector(2, 0x33333333);
    TS_ASSERT_EQUALS(test_wr_var.u2.getSelector(0), 0x11111111);
    TS_ASSERT_EQUALS(test_wr_var.u2.getSelector(1), 0x22222222);
    TS_ASSERT_EQUALS(test_wr_var.u2.getSelector(2), 0x33333333);

    test_wr_var.setInvalid();

    writeRM(10);
    readRM(10);

    test_wr_var.u1.setSelectors("  45  , 30  ,  35    "); // invalid spaces no problem
    TS_ASSERT_EQUALS(test_wr_var.u1.getSelector(0), 45);
    TS_ASSERT_EQUALS(test_wr_var.u1.getSelector(1), 30);
    TS_ASSERT_EQUALS(test_wr_var.u1.getSelector(2), 35);

    test_wr_var.u2.setSelectors("654321,123456,987654XYZ"); // rubbish behind no problem
    TS_ASSERT_EQUALS(test_wr_var.u2.getSelector(0), 654321);
    TS_ASSERT_EQUALS(test_wr_var.u2.getSelector(1), 123456);
    TS_ASSERT_EQUALS(test_wr_var.u2.getSelector(2), 987654);

    test_wr_var.setInvalid();
    writeRM(10);
    readRM(10);
}

void UnionVarTestSuite::testStream(void)
{
    setData();
    std::ostringstream os;
    os << test_wr_var << std::ends;;

    clearData();
    std::istringstream is(os.str());
    is >> test_wr_var;
    checkData(0);
}

void UnionVarTestSuite::testAssignAndCopy()
{
    u1_typ u1;
    u2_typ u3;
    filldata(u1.c1);
    filldata(u1.c2);
    filldata(u1.c3);
    u1.c1.selector = 0;
    u1.c2.selector = 1;
    u1.c3.selector = 2;

    u1_typ u2(u1);
    TS_ASSERT_EQUALS(u1, u2);

    u2.setInvalid();
    TS_ASSERT_DIFFERS(u1, u2);

    u2 = u1;
    TS_ASSERT_EQUALS(u1, u2);

    mpt_var &m1 = u1;
    u2.setInvalid();
    u2 = m1;

    TS_ASSERT_EQUALS(u1, m1);
    TS_ASSERT_DIFFERS(u1, u3);

    link_var lv;
    TS_ASSERT(!lv.isValid());
    lv = u2;
    TS_ASSERT(lv.isValid());
    TS_ASSERT_EQUALS(u1, lv);
    TS_ASSERT_EQUALS(lv, u1);
}

void UnionVarTestSuite::testInvalidSelector()
{
    u1_typ u1;
    dirty_checker topchk(&u1);
    dirty_checker chk1(&u1.c1);
    dirty_checker chk2(&u1.c2);
    dirty_checker chk3(&u1.c3);
    unsigned char buf[12];
    outbuf ob;
    ob.set(buf, sizeof(buf));
    appendbuffer(ob, 3.125f);
    appendbuffer<unsigned char>(ob, 2); // valid
    appendbuffer<short>(ob, -540);
    u1.extract(ob.size(), buf);
    TS_ASSERT_EQUALS(topchk.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(chk1.getStateAndReset(), 0);
    TS_ASSERT_EQUALS(chk2.getStateAndReset(), 0);
    TS_ASSERT_EQUALS(chk3.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(u1.c3.a, 3.125f);
    TS_ASSERT_EQUALS(u1.c3.b, (short)-540);

    ob.set(buf, sizeof(buf));
    appendbuffer(ob, 5);
    appendbuffer<unsigned char>(ob, 3); // invalid
    appendbuffer(ob, 6);
    u1.extract(ob.size(), buf);
    TS_ASSERT_EQUALS(topchk.getStateAndReset(), 2);
    TS_ASSERT_EQUALS(chk1.getStateAndReset(), 0);
    TS_ASSERT_EQUALS(chk2.getStateAndReset(), 0);
    TS_ASSERT_EQUALS(chk3.getStateAndReset(), 0);

    ob.set(buf, sizeof(buf));
    appendbuffer(ob, 5);
    appendbuffer<unsigned char>(ob, 255); // invalid
    appendbuffer(ob, 6);
    u1.extract(ob.size(), buf);
    TS_ASSERT_EQUALS(topchk.getStateAndReset(), 2);
    TS_ASSERT_EQUALS(chk1.getStateAndReset(), 0);
    TS_ASSERT_EQUALS(chk2.getStateAndReset(), 0);
    TS_ASSERT_EQUALS(chk3.getStateAndReset(), 0);

    ob.set(buf, sizeof(buf));
    appendbuffer<unsigned short>(ob, 500);
    appendbuffer<unsigned short>(ob, 33000);
    appendbuffer<unsigned char>(ob, 1); // valid but packet is short
    u1.extract(ob.size(), buf);
    TS_ASSERT_EQUALS(topchk.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(chk1.getStateAndReset(), 0);
    TS_ASSERT_EQUALS(chk2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(chk3.getStateAndReset(), 0);
    TS_ASSERT_EQUALS(u1.c2.a, (unsigned short)500);
    TS_ASSERT_EQUALS(u1.c2.b, (unsigned short)33000);
    TS_ASSERT_EQUALS(u1.c2.c, -1);

    ob.set(buf, sizeof(buf));
    appendbuffer<unsigned short>(ob, 600);
    appendbuffer<unsigned short>(ob, 34000); // packet not big enough for selector
    u1.extract(ob.size(), buf);
    TS_ASSERT_EQUALS(topchk.getStateAndReset(), 2);
    TS_ASSERT_EQUALS(chk1.getStateAndReset(), 0);
    TS_ASSERT_EQUALS(chk2.getStateAndReset(), 0);
    TS_ASSERT_EQUALS(chk3.getStateAndReset(), 0);

    u1.c2.c = -12000; // makes it dirty
    TS_ASSERT_EQUALS(topchk.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(chk1.getStateAndReset(), 0);
    TS_ASSERT_EQUALS(chk2.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(chk3.getStateAndReset(), 0);

    ob.set(buf, 4); // not enough buffer for selector
    TS_ASSERT_EQUALS(ob.size(), 0);
    u1.output(ob);
    TS_ASSERT_EQUALS(ob.size(), 4);


    TS_TRACE("End");
}

void UnionVarTestSuite::writeRM(int n)
{
    clearData();
    setData();
    for (int i=0; i<n; ++i) {
        mpt_show::refresh();
        test_wr_var.write();
    }
}

void UnionVarTestSuite::readRM(int n)
{
    clearData();
    for (int i=0; i<n; ++i)
        test_wr_var.read();
    checkData(n);
}

void UnionVarTestSuite::setData(void)
{
    //set data
    test_wr_var.u1.c1.a = 1;
    test_wr_var.u1.c1.b = 2;
    test_wr_var.u1.c2.a = 3;
    test_wr_var.u1.c2.b = 4;
    test_wr_var.u1.c3.a = 5;
    test_wr_var.u1.c3.b = 6;

    test_wr_var.u2.c1.a = 11;
    test_wr_var.u2.c1.b = 22;
    test_wr_var.u2.c2.a = 33;
    test_wr_var.u2.c2.b = 44;
    test_wr_var.u2.c3.a = 55;
    test_wr_var.u2.c3.b = 66;

    test_wr_var.u4.c1.a = 11;
    test_wr_var.u4.c2.a = 22;
    test_wr_var.u4.c3.a = 33;

    test_wr_var.a = 0xAAAAAAAA;
    test_wr_var.b = 0xBBBBBBBB;
    test_wr_var.c = 0xCCCCCCCC;
}

void UnionVarTestSuite::clearData(void)
{
    test_wr_var.setInvalid();
}

void UnionVarTestSuite::checkData(int n)
{
    /* union will be updated progressively when there is increasing number
       of packet exchanges, n */
    switch (n)
    {
    case 10:
        TS_ASSERT_EQUALS(test_wr_var.u4.c3.a.to_int(), 33);
    case 9:
        TS_ASSERT_EQUALS(test_wr_var.u4.c2.a.to_int(), 22);
    case 8: /* u3.c3 */
    case 7: /* u3.c2 */
    case 6: /* u3.c1 */
    case 5:
        TS_ASSERT_EQUALS(test_wr_var.u2.c3.a.to_int(), 55);
        TS_ASSERT_EQUALS(test_wr_var.u2.c3.b.to_unsigned_short(), (unsigned short)66);
    case 4:
        TS_ASSERT_EQUALS(test_wr_var.u2.c2.a.to_int(), 33);
        TS_ASSERT_EQUALS(test_wr_var.u2.c2.b.to_unsigned_short(), (unsigned short)44);
    case 3:
        TS_ASSERT_EQUALS(test_wr_var.u1.c3.a.to_int(), 5);
        TS_ASSERT_EQUALS(test_wr_var.u1.c3.b.to_unsigned_short(), (unsigned short)6);
    case 2:
        TS_ASSERT_EQUALS(test_wr_var.u1.c2.a.to_int(), 3);
        TS_ASSERT_EQUALS(test_wr_var.u1.c2.b.to_unsigned_short(), (unsigned short)4);
    case 1:
        //test data rx correctly
        TS_ASSERT_EQUALS(test_wr_var.u1.c1.a.to_int(), 1);
        TS_ASSERT_EQUALS(test_wr_var.u1.c1.b.to_unsigned_short(), (unsigned short)2);
        TS_ASSERT_EQUALS(test_wr_var.u2.c1.a.to_int(), 11);
        TS_ASSERT_EQUALS(test_wr_var.u2.c1.b.to_unsigned_short(), (unsigned short)22);
        TS_ASSERT_EQUALS(test_wr_var.u4.c1.a.to_int(), 11);
        //test data before and after union are correctly decoded
        TS_ASSERT_EQUALS(test_wr_var.a.to_unsigned_int(), 0xAAAAAAAA);
        TS_ASSERT_EQUALS(test_wr_var.b.to_unsigned_int(), 0xBBBBBBBB);
        TS_ASSERT_EQUALS(test_wr_var.c.to_unsigned_int(), 0xCCCCCCCC);
    }
}

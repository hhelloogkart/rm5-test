/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_runion_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_UnionVarTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_runion_var\test_runion_var.h"

static UnionVarTestSuite suite_UnionVarTestSuite;

static CxxTest::List Tests_UnionVarTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_UnionVarTestSuite( "test_runion_var/test_runion_var.h", 6, "UnionVarTestSuite", suite_UnionVarTestSuite, Tests_UnionVarTestSuite );

static class TestDescription_suite_UnionVarTestSuite_testWrite : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_UnionVarTestSuite_testWrite() : CxxTest::RealTestDescription( Tests_UnionVarTestSuite, suiteDescription_UnionVarTestSuite, 9, "testWrite" ) {}
 void runTest() { suite_UnionVarTestSuite.testWrite(); }
} testDescription_suite_UnionVarTestSuite_testWrite;

static class TestDescription_suite_UnionVarTestSuite_testWriteShow : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_UnionVarTestSuite_testWriteShow() : CxxTest::RealTestDescription( Tests_UnionVarTestSuite, suiteDescription_UnionVarTestSuite, 10, "testWriteShow" ) {}
 void runTest() { suite_UnionVarTestSuite.testWriteShow(); }
} testDescription_suite_UnionVarTestSuite_testWriteShow;

static class TestDescription_suite_UnionVarTestSuite_testSelector : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_UnionVarTestSuite_testSelector() : CxxTest::RealTestDescription( Tests_UnionVarTestSuite, suiteDescription_UnionVarTestSuite, 11, "testSelector" ) {}
 void runTest() { suite_UnionVarTestSuite.testSelector(); }
} testDescription_suite_UnionVarTestSuite_testSelector;

static class TestDescription_suite_UnionVarTestSuite_testStream : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_UnionVarTestSuite_testStream() : CxxTest::RealTestDescription( Tests_UnionVarTestSuite, suiteDescription_UnionVarTestSuite, 12, "testStream" ) {}
 void runTest() { suite_UnionVarTestSuite.testStream(); }
} testDescription_suite_UnionVarTestSuite_testStream;

static class TestDescription_suite_UnionVarTestSuite_testAssignAndCopy : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_UnionVarTestSuite_testAssignAndCopy() : CxxTest::RealTestDescription( Tests_UnionVarTestSuite, suiteDescription_UnionVarTestSuite, 13, "testAssignAndCopy" ) {}
 void runTest() { suite_UnionVarTestSuite.testAssignAndCopy(); }
} testDescription_suite_UnionVarTestSuite_testAssignAndCopy;

static class TestDescription_suite_UnionVarTestSuite_testInvalidSelector : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_UnionVarTestSuite_testInvalidSelector() : CxxTest::RealTestDescription( Tests_UnionVarTestSuite, suiteDescription_UnionVarTestSuite, 14, "testInvalidSelector" ) {}
 void runTest() { suite_UnionVarTestSuite.testInvalidSelector(); }
} testDescription_suite_UnionVarTestSuite_testInvalidSelector;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

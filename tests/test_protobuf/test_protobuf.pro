TARGET = test_protobuf

DEFINES += PROTOBUF_USE_DLLS
GP=google/protobuf

HEADERS += \
  $${GP}/test_util.h                                  \
  $${GP}/testing/googletest.h                         \
  $${GP}/testing/file.h

SOURCES += gtest_main.cc \
  $${GP}/test_util.cc                                 \
  $${GP}/testing/googletest.cc                        \
  $${GP}/testing/file.cc                              \
  $${GP}/stubs/common_unittest.cc                     \
  $${GP}/stubs/once_unittest.cc                       \
  $${GP}/stubs/strutil_unittest.cc                    \
  $${GP}/stubs/structurally_valid_unittest.cc         \
  $${GP}/stubs/stringprintf_unittest.cc               \
  $${GP}/stubs/template_util_unittest.cc              \
  $${GP}/stubs/type_traits_unittest.cc                \
  $${GP}/descriptor_database_unittest.cc              \
  $${GP}/descriptor_unittest.cc                       \
  $${GP}/dynamic_message_unittest.cc                  \
  $${GP}/extension_set_unittest.cc                    \
  $${GP}/generated_message_reflection_unittest.cc     \
  $${GP}/message_unittest.cc                          \
  $${GP}/reflection_ops_unittest.cc                   \
  $${GP}/repeated_field_unittest.cc                   \
  $${GP}/repeated_field_reflection_unittest.cc        \
  $${GP}/text_format_unittest.cc                      \
  $${GP}/unknown_field_set_unittest.cc                \
  $${GP}/wire_format_unittest.cc                      \
  $${GP}/io/coded_stream_unittest.cc                  \
  $${GP}/io/printer_unittest.cc                       \
  $${GP}/io/tokenizer_unittest.cc                     \
  $${GP}/io/zero_copy_stream_unittest.cc              \
  $${GP}/compiler/parser_unittest.cc                  \
  $${GP}/unittest.pb.cc                               \
  $${GP}/unittest_custom_options.pb.cc                \
  $${GP}/unittest_import.pb.cc                        \
  $${GP}/unittest_import_public.pb.cc                 \
  $${GP}/unittest_mset.pb.cc

INCLUDEPATH += . ../../include ../../src

include (../tests.pri)
SOURCES -= runner.cpp
LIBS += lib/gtest$${LIB_SUFFIX}.lib
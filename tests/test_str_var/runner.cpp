/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_str_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_StrVarTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_str_var\test_str_var.h"

static StrVarTestSuite suite_StrVarTestSuite;

static CxxTest::List Tests_StrVarTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_StrVarTestSuite( "test_str_var/test_str_var.h", 6, "StrVarTestSuite", suite_StrVarTestSuite, Tests_StrVarTestSuite );

static class TestDescription_suite_StrVarTestSuite_testString : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_StrVarTestSuite_testString() : CxxTest::RealTestDescription( Tests_StrVarTestSuite, suiteDescription_StrVarTestSuite, 9, "testString" ) {}
 void runTest() { suite_StrVarTestSuite.testString(); }
} testDescription_suite_StrVarTestSuite_testString;

static class TestDescription_suite_StrVarTestSuite_testExtract : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_StrVarTestSuite_testExtract() : CxxTest::RealTestDescription( Tests_StrVarTestSuite, suiteDescription_StrVarTestSuite, 10, "testExtract" ) {}
 void runTest() { suite_StrVarTestSuite.testExtract(); }
} testDescription_suite_StrVarTestSuite_testExtract;

static class TestDescription_suite_StrVarTestSuite_testOutput : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_StrVarTestSuite_testOutput() : CxxTest::RealTestDescription( Tests_StrVarTestSuite, suiteDescription_StrVarTestSuite, 11, "testOutput" ) {}
 void runTest() { suite_StrVarTestSuite.testOutput(); }
} testDescription_suite_StrVarTestSuite_testOutput;

static class TestDescription_suite_StrVarTestSuite_testStream : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_StrVarTestSuite_testStream() : CxxTest::RealTestDescription( Tests_StrVarTestSuite, suiteDescription_StrVarTestSuite, 12, "testStream" ) {}
 void runTest() { suite_StrVarTestSuite.testStream(); }
} testDescription_suite_StrVarTestSuite_testStream;

static class TestDescription_suite_StrVarTestSuite_testToValue : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_StrVarTestSuite_testToValue() : CxxTest::RealTestDescription( Tests_StrVarTestSuite, suiteDescription_StrVarTestSuite, 13, "testToValue" ) {}
 void runTest() { suite_StrVarTestSuite.testToValue(); }
} testDescription_suite_StrVarTestSuite_testToValue;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

#ifndef _TEST_STR_VAR_H_DEF_
#define _TEST_STR_VAR_H_DEF_

#include <cxxtest/TestSuite.h>

class StrVarTestSuite : public CxxTest::TestSuite
{
public:
    void testString(void);
    void testExtract(void);
    void testOutput(void);
    void testStream(void);
    void testToValue(void);
};

#endif


#include "test_str_var.h"
#include <str_var.h>
#include <arry_var.h>
#include <gen_var.h>
#include <rgen_var.h>
#include <cplx_var.h>
#include <mpt_show.h>
#include <outbuf.h>
#include <util/utilcore.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>

#define TS_ASSERT_NOT(x) TS_ASSERT(!(x))

template<int sz>
void messup(string_var<sz> &str)
{
    for (int idx = 0; idx < sz; ++idx)
        str[idx] = static_cast<char>(rand() & 0xff);
}

class dirty_checker : public mpt_baseshow
{
public:
    dirty_checker(mpt_var *myvar);
    virtual void setDirty(mpt_var *myvar);
    virtual void notDirty(mpt_var *myvar);
    int getStateAndReset();
protected:
    int state; // 0 - not called, 1 - dirty, 2 - notdirty, 3 - error
    mpt_var *var;
};

dirty_checker::dirty_checker(mpt_var *myvar) : state(0), var(myvar)
{
    myvar->addCallback(this);
}

void dirty_checker::setDirty(mpt_var *myvar)
{
    if (myvar == var) state = 1;
    else state = 3;
}

void dirty_checker::notDirty(mpt_var *myvar)
{
    if (myvar == var) state = 2;
    else state = 3;
}

int dirty_checker::getStateAndReset()
{
    int ret = state;
    state = 0;
    return ret;
}

struct StrVarTest : public complex_var
{
public:
    COMPLEXCONST(StrVarTest)
    string_var<3> abc;
    string_var<11> d2l;
    string_var<3> mno;
};

struct StrVarTest3 : public complex_var
{
public:
    COMPLEXCONST(StrVarTest3)
    string_var<2> abc;
    string_var<5> d2l;
    string_var<2> mno;
};

void StrVarTestSuite::testString(void)
{
    const char shstr[] = "ABCDE";
    const char lgstr[] = "FGHIJKLMNOPQRSTUVWXYZ\0ABCDEFGHIJKLMNOPQRSTUVW"; // everything C and after will not be assigned
    string_var<8> str1, str2(shstr), str2a(str2);
    string_var<24> str3, str4(lgstr), str4a(str4);
    const string_var<24> &str4r = str4;
    string_var<12> str5(shstr), str5a(str2);
    generic_var<int> i_5(567890123);
    generic_var<short> s_1(12345);
    r_generic_var<int> ri_5(567890123);
    r_generic_var<short> rs_1(12345);
    string_var<5> str6(ri_5), str7(rs_1);
    string_var<5> str6a(i_5), str7a(s_1);
    string_var<12> str8(ri_5), str9(rs_1);
    string_var<12> str8a(i_5), str9a(s_1);
    const char i_5str[] = "567890123";
    const char s_1str[] = "12345";
    char strbuf[25];

    // RTTI
    TS_ASSERT_EQUALS(str1.rtti(), str2.rtti());
    TS_ASSERT_EQUALS(str1.rtti(), str3.rtti());
    TS_ASSERT_EQUALS(str5.rtti(), str3.rtti());

    // Use of pointers
    sprintf(strbuf, "%s", static_cast<char *>(str4));
    TS_ASSERT_EQUALS(strlen(strbuf), 21);
    TS_ASSERT_EQUALS(strcmp(strbuf, "FGHIJKLMNOPQRSTUVWXYZ"), 0);
    memset(strbuf, 0, sizeof(strbuf));
    TS_ASSERT_EQUALS(strlen(strbuf), 0);
    sprintf(strbuf, "%s", static_cast<const char *>(str4r));
    TS_ASSERT_EQUALS(strlen(strbuf), 21);
    TS_ASSERT_EQUALS(strcmp(strbuf, "FGHIJKLMNOPQRSTUVWXYZ"), 0);
    memset(strbuf, 0, sizeof(strbuf));
    TS_ASSERT_EQUALS(strlen(strbuf), 0);
    sprintf(strbuf, str4r);
    TS_ASSERT_EQUALS(strlen(strbuf), 21);
    TS_ASSERT_EQUALS(strcmp(strbuf, "FGHIJKLMNOPQRSTUVWXYZ"), 0);

    // Size
    TS_ASSERT_EQUALS(str1.size(), 8);
    TS_ASSERT_EQUALS(str3.size(), 24);
    TS_ASSERT_EQUALS(str6.size(), 5);
    TS_ASSERT_EQUALS(str8.size(), 12);

    // Test of constructors
    TS_ASSERT_NOT(str1.isValid());
    TS_ASSERT(str2.isValid());
    TS_ASSERT(str2a.isValid());
    TS_ASSERT_NOT(str3.isValid());
    TS_ASSERT(str4.isValid());
    TS_ASSERT(str4a.isValid());
    TS_ASSERT_EQUALS(memcmp(static_cast<const char *>(str2), shstr, 6), 0);
    TS_ASSERT_EQUALS(*(static_cast<const char *>(str2) + 8), '\0');
    TS_ASSERT_EQUALS(memcmp(static_cast<const char *>(str4), lgstr, 21), 0); // only assign until first null
    TS_ASSERT_EQUALS(*(static_cast<const char *>(str4) + 24), '\0');
    TS_ASSERT_EQUALS(memcmp(static_cast<const char *>(str5), shstr, 6), 0);
    TS_ASSERT_EQUALS(*(static_cast<const char *>(str5) + 12), '\0');
    TS_ASSERT_EQUALS(memcmp(static_cast<const char *>(str2a), shstr, 6), 0);
    TS_ASSERT_EQUALS(*(static_cast<const char *>(str2a) + 8), '\0');
    TS_ASSERT_EQUALS(memcmp(static_cast<const char *>(str4a), lgstr, 21), 0);
    TS_ASSERT_EQUALS(*(static_cast<const char *>(str4a) + 24), '\0');
    TS_ASSERT_EQUALS(memcmp(static_cast<const char *>(str6), i_5str, 5), 0);
    TS_ASSERT_EQUALS(*(static_cast<const char *>(str6) + 5), '\0');
    TS_ASSERT_EQUALS(memcmp(static_cast<const char *>(str6a), i_5str, 5), 0);
    TS_ASSERT_EQUALS(*(static_cast<const char *>(str6a) + 5), '\0');
    TS_ASSERT_EQUALS(memcmp(static_cast<const char *>(str7), s_1str, 6), 0);
    TS_ASSERT_EQUALS(*(static_cast<const char *>(str7) + 5), '\0');
    TS_ASSERT_EQUALS(memcmp(static_cast<const char *>(str7a), s_1str, 6), 0);
    TS_ASSERT_EQUALS(*(static_cast<const char *>(str7a) + 5), '\0');
    TS_ASSERT_EQUALS(memcmp(static_cast<const char *>(str8), i_5str, 10), 0);
    TS_ASSERT_EQUALS(*(static_cast<const char *>(str8a) + 12), '\0');
    TS_ASSERT_EQUALS(memcmp(static_cast<const char *>(str8), i_5str, 10), 0);
    TS_ASSERT_EQUALS(*(static_cast<const char *>(str8a) + 12), '\0');
    TS_ASSERT_EQUALS(memcmp(static_cast<const char *>(str9), s_1str, 6), 0);
    TS_ASSERT_EQUALS(*(static_cast<const char *>(str9) + 12), '\0');
    TS_ASSERT_EQUALS(memcmp(static_cast<const char *>(str9a), s_1str, 6), 0);
    TS_ASSERT_EQUALS(*(static_cast<const char *>(str9a) + 12), '\0');

    // Test of equal
    // -- same class (must be the same for all bytes)
    TS_ASSERT_NOT(str1 == str2);
    TS_ASSERT(str2 == str2a);
    TS_ASSERT_NOT(str3 == str4);
    TS_ASSERT(str4 == str4a);
    TS_ASSERT(str6 == str6a);
    TS_ASSERT(str7 == str7a);
    //TS_ASSERT(str8 == str8a); Cannot do this because does not fill up entire string
    //TS_ASSERT(str9 == str9a);
    //TS_ASSERT(str10 == str10a);
    // -- string_var but not same size (compare up to null)
    TS_ASSERT(str7 == str9);
    TS_ASSERT(str9 == str7);
    TS_ASSERT_NOT(str6 == str8);
    TS_ASSERT_NOT(str8 == str6);
    TS_ASSERT(str5 == str2);
    TS_ASSERT(str2 == str5);
    TS_ASSERT(str2 == str5a);
    TS_ASSERT(str5a == str2);
    // -- gen_var (compare up to null)
    TS_ASSERT(str7 == rs_1);
    TS_ASSERT(str7 == s_1);
    TS_ASSERT(str7a == rs_1);
    TS_ASSERT(str7a == s_1);
    TS_ASSERT_NOT(str6 == ri_5);
    TS_ASSERT_NOT(str6a == i_5);
    TS_ASSERT(str9 == rs_1);
    TS_ASSERT(str9 == s_1);
    TS_ASSERT(str9a == rs_1);
    TS_ASSERT(str9a == s_1);
    TS_ASSERT(str8 == ri_5);
    TS_ASSERT(str8 == i_5);
    TS_ASSERT(str8a == ri_5);
    TS_ASSERT(str8a == i_5);
    // -- const char * (compare up to null)
    TS_ASSERT_NOT(str1 == shstr);
    TS_ASSERT(str2 == shstr);
    TS_ASSERT(str2a == shstr);
    TS_ASSERT_NOT(str3 == lgstr);
    TS_ASSERT(str4 == lgstr);
    TS_ASSERT(str4a == lgstr);

    // Test of assignment
    dirty_checker checker1(&str1);
    dirty_checker checker2(&str2);
    dirty_checker checker4(&str4);
    // -- same class
    str1 = str2; // assign to same class
    TS_ASSERT(str1 == str2);
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    str1 = shstr; // assign to const char
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    str1[7] = 'X'; // after the null
    TS_ASSERT(str1 == shstr); // so still equal
    TS_ASSERT(str1 == str5);
    TS_ASSERT_NOT(str1 == str2); // but no longer equal
    str1 = str5; // assign to other class
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    str1 = str2; // back to same class
    TS_ASSERT(str1 == str2); // now equal
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1); // change after null detected
    str1 = s_1; // assign to other class, no overflow
    TS_ASSERT(str1 == str7a);
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    str1 = i_5; // assign to other class, overflow
    TS_ASSERT(str1 == "56789012");
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_NOT(str1 == str6a); // 7a is too short
    TS_ASSERT_NOT(str1 == str9a); // 9a is too long
    str1 = str4;
    str2 = str4a;
    str4 = str4a;
    TS_ASSERT(str1 == str2);
    TS_ASSERT(str1 == "FGHIJKLM");
    TS_ASSERT_NOT(str1 == lgstr);
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT(str1.setInvalid());
    TS_ASSERT_NOT(str1.isValid());
    TS_ASSERT_NOT(str1 == str2);
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_NOT(str1.setInvalid());
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);
    TS_ASSERT_EQUALS(checker4.getStateAndReset(), 2);

    str1 = lgstr;
    TS_ASSERT(str1 == str2);
    TS_ASSERT_NOT(str1 == lgstr);
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    // Test assignment - string_var overload detection
    messup<8>(str1);
    messup<8>(str2);
    TS_ASSERT_NOT(str1 == str2);
    str1 = shstr;
    str2 = shstr;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT_EQUALS(checker2.getStateAndReset(), 1);
    TS_ASSERT_NOT(str1 == str2);
    const mpt_var & rstr2 = str2;
    str1 = rstr2;
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    TS_ASSERT(str1 == str2);

    // Misc copy constructor
    char misc1[] = "MISC1OVERFLOW";
    string_var<5> misc2(misc1);
    string_var<5> misc3("MISC1OVERFLOW");
    TS_ASSERT(misc2 == misc3);
    TS_ASSERT_NOT(misc2 == (const char *)misc1);

    // Misc assignment
    dirty_checker checker3(&misc3);
    misc3 = misc1;
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 2);
    misc3 = "MISC1OVERFLOW";
    TS_ASSERT_EQUALS(checker3.getStateAndReset(), 2);
}

void StrVarTestSuite::testExtract(void)
{
    //                    0---------0---------0
    const char lgstr[] = "FGHIJKLMNOPQRSTUVWXYZ\0ABCDEFGHIJKLMNOPQRSTUVW"; // everything C and after will not be assigned
    const char otstr[] = "012345678901234";
    const unsigned char *lgstrp = reinterpret_cast<const unsigned char *>(lgstr);
    string_var<24> str1;
    dirty_checker checker1(&str1);
    TS_ASSERT_NOT(str1.isValid());
    // underflow case
    str1.extract(8, lgstrp);
    for (int idx = 0; idx < 8; ++idx)
        TS_ASSERT(str1[idx] == lgstr[idx]);
    TS_ASSERT_NOT(str1[8] == lgstr[8]);
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    // overflow case
    str1.extract(45, lgstrp);
    for (int idx = 0; idx < 24; ++idx)
        TS_ASSERT(str1[idx] == lgstr[idx]);
    TS_ASSERT_NOT(str1[24] == lgstr[24]);
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);

    char str3[25];
    strcpy(str3, str1);
    TS_ASSERT(str1 == (const char *)str3); // comparison with a char array is null terminated based (string assumed)
    string_var<24> str2(str3);
    TS_ASSERT_NOT(str1 == str2); // comparison with another string_var is over the entire size

    str1.extract(45, lgstrp); // exactly the same
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);

    str1.extract(38, lgstrp); // same data, less overflow
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);

    str1.extract(24, lgstrp); // although exact size, but data is the same
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);

    str1.extract(23, lgstrp); // same data but now smaller than str1 size
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);

    str1.extract(23, lgstrp); // now no change
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);

    str1.extract(15, lgstrp); // shrink again
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);

    str1.extract(15, (unsigned char *)otstr); // other string
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
}

void StrVarTestSuite::testOutput(void)
{
    const char lgstr[] = "FGHIJKLMNOPQRSTUVWXYZ\0AB";
    unsigned char buf[24];
    string_var<24> str1;
    for (int idx = 0; idx < 24; ++idx)
        str1[idx] = lgstr[idx];
    outbuf ob;
    ob.set(buf, 24);
    str1.output(ob);
    TS_ASSERT_SAME_DATA(buf, lgstr, 24);
}

void StrVarTestSuite::testStream(void)
{
    std::ostringstream oss;
    const char lgstr[] = "FGHIJKLMNOPQRSTUVWXYZ\0AB";
    string_var<24> str1;
    for (int idx = 0; idx < 24; ++idx)
        str1[idx] = lgstr[idx];
    oss << str1;
    TS_ASSERT_EQUALS(oss.str(), lgstr);
    // Note the above is not comprehensive because it only tests
    // till the null. But currently streaming will only handle till
    // null anyway. Hope to improve in the future.
    string_var<26> str2;
    {
        std::istringstream iss(oss.str());
        iss >> str2;
    }
    TS_ASSERT(str1 == str2);


    // with spaces
    str1 = "The red\1\2\3 fox jumped";
    TS_ASSERT_NOT(str1 == str2);
    oss.str(std::string());
    oss << str1;
    {
        std::istringstream iss(oss.str());
        iss >> str2;
    }
    TS_ASSERT(str1 == str2);

    StrVarTest cplx1;
    cplx1.abc = "abc";
    cplx1.d2l = "def ghi jkl";
    cplx1.mno = "mno";
    oss.str(std::string());
    oss << cplx1;

    StrVarTest cplx2;
    cplx2.abc;
    cplx2.d2l;
    cplx2.mno;
    {
        std::istringstream iss(oss.str());
        iss >> cplx2;
    }
    TS_ASSERT(cplx1 == cplx2);

    // input overflow
    StrVarTest3 cplx3;
    cplx3.abc;
    cplx3.d2l;
    cplx3.mno;
    {
        std::istringstream iss(oss.str());
        iss >> cplx3;
    }
    TS_ASSERT(cplx3.abc == "ab");
    TS_ASSERT(cplx3.d2l == "def g");
    TS_ASSERT(cplx3.mno == "mn");

    string_var<32> complicated[2];
    complicated[0] = "abc\tdef\rijk\nlmn\vopq\frst\\";
    complicated[1] = "uvw\txyz\rABC\nDEF\vGHI\fJKL\\";
    oss.str(std::string());
    oss << complicated[0] << ' ' << complicated[1];
    {
        std::istringstream iss(oss.str());
        iss >> complicated[1] >> complicated[0]; // reverse
    }
    TS_ASSERT(complicated[0] == "uvw\txyz\rABC\nDEF\vGHI\fJKL\\");
    TS_ASSERT(complicated[1] == "abc\tdef\rijk\nlmn\vopq\frst\\");
    TS_ASSERT_EQUALS(complicated[0].getNext(), &complicated[1]);
    oss.str(std::string());
    {
        std::istringstream iss(oss.str());
        iss >> complicated[1] >> complicated[0]; // reverse
    }
    TS_ASSERT(!complicated[0].isValid());
    TS_ASSERT(!complicated[1].isValid());
    oss << complicated[0] << ' ' << complicated[1];
    TS_ASSERT_EQUALS(oss.str(), "\"\" \"\"");
    complicated[0] = "X";
    complicated[1] = "Y";
    oss.str(std::string("\\c")); // wierd invalid string
    {
        std::istringstream iss(oss.str());
        iss >> complicated[1] >> complicated[0]; // reverse
    }
    TS_ASSERT(complicated[1] == "\\c");
    TS_ASSERT(!complicated[0].isValid());
    complicated[0] = "X";
    complicated[1] = "Y";
    oss.str(std::string("\"\\c\"")); // wierd invalid string
    {
        std::istringstream iss(oss.str());
        iss >> complicated[1] >> complicated[0]; // reverse
    }
    TS_ASSERT(complicated[1] == "\\c");
    TS_ASSERT(!complicated[0].isValid());
}
void StrVarTestSuite::testToValue(void)
{
    string_var<19> value;
    // from literals
    value.fromValue(5);
    TS_ASSERT(value == "5");
    value.fromValue(-5);
    TS_ASSERT(value == "-5");
    value.fromValue(5.);
    TS_ASSERT(value == "5");
    value.fromValue(-5.);
    TS_ASSERT(value == "-5");
    // to value
    const char *cstr = value.to_const_char_ptr();
    TS_ASSERT_EQUALS(strcmp(cstr, "-5"), 0);
    char *str = value.to_char_ptr();
    TS_ASSERT_EQUALS(strcmp(str, "-5"), 0);
    char vch = value.to_char();
    TS_ASSERT_EQUALS(vch, -5);
    short vsh = value.to_short();
    TS_ASSERT_EQUALS(vsh, -5);
    int vit = value.to_int();
    TS_ASSERT_EQUALS(vit, -5);
    long vlg = value.to_long();
    TS_ASSERT_EQUALS(vlg, -5);
    long long vll = value.to_long_long();
    TS_ASSERT_EQUALS(vll, -5);
    // from signed variables
    vsh += 112;
    value.fromValue(vsh);
    TS_ASSERT(value == "107");
    vit += 1112;
    value.fromValue(vit);
    TS_ASSERT(value == "1107");
    vlg += 11112;
    value.fromValue(vlg);
    TS_ASSERT(value == "11107");
    vll += 111112;
    value.fromValue(vll);
    TS_ASSERT(value == "111107");
    vch += 12;
    value.fromValue(vch);
    TS_ASSERT(value == "7");
    // to value
    unsigned char xch = value.to_unsigned_char();
    TS_ASSERT_EQUALS(xch, 7);
    unsigned short xsh = value.to_unsigned_short();
    TS_ASSERT_EQUALS(xsh, 7);
    unsigned int xit = value.to_unsigned_int();
    TS_ASSERT_EQUALS(xit, 7);
    unsigned long xlg = value.to_unsigned_long();
    TS_ASSERT_EQUALS(xlg, 7);
    unsigned long long xll = value.to_unsigned_long_long();
    TS_ASSERT_EQUALS(xll, 7);
    // from unsigned variables
    xch = 130;
    value.fromValue(xch);
    TS_ASSERT(value == "130");
    xsh = 33210;
    value.fromValue(xsh);
    TS_ASSERT(value == "33210");
    xit = 2415919104;
    value.fromValue(xit);
    TS_ASSERT(value == "2415919104");
    xlg = 3240314215;
    value.fromValue(xlg);
    TS_ASSERT(value == "3240314215");
    xll = 9223372036867121486;
    value.fromValue(xll);
    TS_ASSERT(value == "9223372036867121486");

    // Should do a better job with string_var
    // which currently depends on user to set
    // the format string
    value.set_float_format("%.7g");
    value.fromValue(35005.45);
    TS_ASSERT(value == "35005.45");
    TS_ASSERT(!(value == "350"));
    TS_ASSERT(!(value == "350005.459532"));
    TS_ASSERT(value.to_double() == 35005.45);
    TS_ASSERT(value.to_float() == 35005.45f);
    value.fromValue(-35005.45);
    TS_ASSERT(value == "-35005.45");
    TS_ASSERT(value.to_double() == -35005.45);
    TS_ASSERT(value.to_float() == -35005.45f);


    value.fromValue(6.78322e11f);
    TS_ASSERT(value == "6.78322e+11");
    TS_ASSERT(!(value == "6.78"));
    TS_ASSERT(!(value == "6.78322e+101"));
    TS_ASSERT(value.to_double() == 6.78322e+11);
    TS_ASSERT(value.to_float() == 6.78322e+11f);
    value.fromValue(-1.34885e-8f);
    TS_ASSERT(value == "-1.34885e-08");
    TS_ASSERT(value.to_double() == -1.34885e-8);
    TS_ASSERT(value.to_float() == -1.34885e-8f);

    value.from_char_ptr("ABCDE\0FGHIJ");
    TS_ASSERT(value == "ABCDE");
    value.from_char_ptr("0123456789012345678901");
    TS_ASSERT(value == "0123456789012345678");


    TS_TRACE("End");
}

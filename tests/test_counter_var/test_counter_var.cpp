#include "test_counter_var.h"
#include <str_var.h>
#include <arry_var.h>
#include <gen_var.h>
#include <mpt_show.h>
#include <counter_var.h>
#include <link_var.h>
#include <sstream>

#define TS_ASSERT_NOT(x) TS_ASSERT(!(x))

class SimplePacket : public counter_var
{
public:
    COUNTERCONST(SimplePacket)
    SimplePacket(unsigned int counter_id);
    generic_var<int> a[10];
    array_var<int> b;
    generic_var<short> c;
    string_var<5> d;

    void setRMDirty();
    void setBuddy(SimplePacket *frnd);
    bool transfer(bool drop = false);

private:
    bool dirty;
    SimplePacket *buddy;
};

SimplePacket::SimplePacket(unsigned int counter_id) :
    counter_var(counter_id)
{
    pop();
}

void SimplePacket::setRMDirty()
{
    dirty = true;
    counter_var::setRMDirty();
}

void SimplePacket::setBuddy(SimplePacket *frnd)
{
    buddy = frnd;
    dirty = false;
}

bool SimplePacket::transfer(bool drop)
{
    if (!buddy) return false;
    mpt_show::refresh();
    if (!dirty) return false;

    const int sz = size();
    if (sz == checksum_size()) return false;

    unsigned char *buf = new unsigned char[sz];
    outbuf ob;
    ob.set(buf, sz);
    output(ob);
    dirty = false;
    if (!drop) buddy->extract(ob.size(), buf);
    delete[] buf;
    return true;
}

void fillPacket(SimplePacket &packet, int v)
{
    for (int idx = 0; idx < 10; ++idx)
        packet.a[idx] = v;
    packet.b.resize(v+1);
    int *b = packet.b;
    for (int idx = 0; idx <= v; ++idx)
        b[idx] = v;
    packet.c = (short)v;
    const char ch = '0' + (char)v;
    packet.d[0] = ch;
    packet.d[1] = ch;
    packet.d[2] = ch;
    packet.d[3] = ch;
    packet.d[4] = ch;
}

void chkPacket(const SimplePacket &packet, int v)
{
    for (int idx = 0; idx < 10; ++idx)
        TS_ASSERT_EQUALS(packet.a[idx], v);
    const int bsz = packet.b.cardinal();
    TS_ASSERT_EQUALS(bsz, v+1);
    const int *b = packet.b;
    for (int idx = 0; idx < bsz; ++idx)
        TS_ASSERT_EQUALS(b[idx], v);
    TS_ASSERT_EQUALS(packet.c.to_short(), (short)v);
    const char ch = '0' + (char)v;
    TS_ASSERT_EQUALS(packet.d[0], ch);
    TS_ASSERT_EQUALS(packet.d[1], ch);
    TS_ASSERT_EQUALS(packet.d[2], ch);
    TS_ASSERT_EQUALS(packet.d[3], ch);
    TS_ASSERT_EQUALS(packet.d[4], ch);
}

class dirty_checker : public mpt_baseshow
{
public:
    dirty_checker(mpt_var *myvar);
    virtual void setDirty(mpt_var *myvar);
    virtual void notDirty(mpt_var *myvar);
    virtual void operator()();
    void getStateAndReset(int &d, int &n);
    int getRefreshStateAndReset();

protected:
    int dstate; // 0 - not called, 1 or more - count of dirtiness, -1 - error
    int nstate; // 0 - not called, 1 or more - count of not dirtiness, -1 = error
    int ostate;
    mpt_var *var;
};

dirty_checker::dirty_checker(mpt_var *myvar) : dstate(0), nstate(0), ostate(0), var(myvar)
{
    myvar->addCallback(this);
}

void dirty_checker::setDirty(mpt_var *myvar)
{
    if (myvar == var && dstate >= 0) ++dstate;
    else dstate = -1;
}

void dirty_checker::notDirty(mpt_var *myvar)
{
    if (myvar == var && nstate >= 0) ++nstate;
    else nstate = -1;
}

void dirty_checker::operator()()
{
    ++ostate;
}

void dirty_checker::getStateAndReset(int &d, int &n)
{
    d = dstate;
    n = nstate;
    dstate = nstate = 0;
}

int dirty_checker::getRefreshStateAndReset()
{
    int ret = ostate;
    ostate = 0;
    return ret;
}

void CounterVarTestSuite::testDataTransfer(void)
{
    SimplePacket send, recv(1);
    dirty_checker chker(&recv);
    int dstate, nstate;
    send.setBuddy(&recv);
    recv.setBuddy(&send);

    /* Test until the counter loops around at 65535
        As chk_var will generate a notDirty on check
        failure (and nothing) on success, we use this
        to test if the counter checks passed. However,
        it is fragile because it may be confused with
        triggers from its children also.
    */
    for (int idx = 0; idx < 66000; ++idx)
    {
        fillPacket(send, idx % 10);
        send.transfer();
        chker.getStateAndReset(dstate, nstate);
        TS_ASSERT_LESS_THAN(0, dstate);
        TS_ASSERT_EQUALS(nstate, 0);
        chkPacket(recv, idx % 10);
    }

    TS_ASSERT(send == recv);
    mpt_var &recvr = recv;
    TS_ASSERT(send == recvr);

    SimplePacket cpy(send);
    chkPacket(cpy, 9);
    TS_ASSERT(send == cpy);

    fillPacket(recv, 4);
    TS_ASSERT(!(send == recv));
    recv = send;
    chkPacket(recv, 9);
    TS_ASSERT(send == recvr);

    fillPacket(cpy, 5);
    TS_ASSERT(!(send == cpy));
    cpy = recvr;
    TS_ASSERT(send == cpy);
}

void CounterVarTestSuite::testDroppedPackets(void)
{
    SimplePacket send(1), recv;
    dirty_checker chker(&recv);
    int dstate, nstate;
    send.setBuddy(&recv);
    recv.setBuddy(&send);

    /* Test until the counter loops around at 65535 */
    for (int idx = 0; idx < 66001; ++idx)
    {
        fillPacket(send, idx % 10);
        send.transfer(idx % 2);
        chker.getStateAndReset(dstate, nstate);
        if (idx % 2)
        {
            TS_ASSERT_EQUALS(dstate, 0);
            TS_ASSERT_EQUALS(nstate, 0);
        }
        else
        {
            TS_ASSERT_LESS_THAN(0, dstate);
            TS_ASSERT_EQUALS(nstate, 0);
            chkPacket(recv, idx % 10);
        }
    }
    TS_ASSERT(send == recv);
    mpt_var &recvr = recv;
    TS_ASSERT(send == recvr);
}

void CounterVarTestSuite::testManyDroppedPackets(void)
{
    SimplePacket send, recv(1);
    dirty_checker chker(&recv);
    int dstate, nstate;
    send.setBuddy(&recv);
    recv.setBuddy(&send);

    /* Test until the counter loops around at 65535 */
    /* Note that the tests are a bit fragile because
        if the loss is an exact multiple of 10 (fillPacket
        repetition) then when the packet is extracted,
        it will produce many notDirty because the data
        is the same
    */
    for (int idx = 0; idx < 66010; ++idx)
    {
        bool drop = (idx % 1000) > 505;
        fillPacket(send, idx % 10);
        send.transfer(drop);
        chker.getStateAndReset(dstate, nstate);
        if (drop)
        {
            TS_ASSERT_EQUALS(dstate, 0);
            TS_ASSERT_EQUALS(nstate, 0);
        }
        else
        {
            TS_ASSERT_LESS_THAN(0, dstate);
            TS_ASSERT_EQUALS(nstate, 0);
            chkPacket(recv, idx % 10);
        }
    }
    TS_ASSERT(send == recv);
    mpt_var &recvr = recv;
    TS_ASSERT(send == recvr);
}

void CounterVarTestSuite::testVeryOldData(void)
{
    SimplePacket send, replay(1), recv(2);
    int dstate, nstate;
    send.setBuddy(&recv);
    replay.setBuddy(&recv);
    send.setInvalid();
    replay.setInvalid();
    recv.setInvalid();
    dirty_checker chker(&recv);

    for (int idy = 0; idy < 450; ++idy)
    {
        for (int idx = 0; idx < 80; ++idx)
        {
            fillPacket(send, idx % 10);
            send.transfer();
            chker.getStateAndReset(dstate, nstate);
            TS_ASSERT_LESS_THAN(0, dstate);
            TS_ASSERT_EQUALS(nstate, 0);
            chkPacket(recv, idx % 10);
        }

        for (int idx = 0; idx < 80; ++idx)
        {
            fillPacket(replay, idx % 10);
            replay.transfer();
            chker.getStateAndReset(dstate, nstate);
            TS_ASSERT_EQUALS(dstate, 0);
            TS_ASSERT_EQUALS(nstate, 1);
        }

        for (int idx = 0; idx < 80; ++idx)
        {
            fillPacket(replay, idx % 10);
            replay.transfer();
            chker.getStateAndReset(dstate, nstate);
            TS_ASSERT_LESS_THAN(0, dstate);
            TS_ASSERT_EQUALS(nstate, 0);
            chkPacket(recv, idx % 10);
        }

        for (int idx = 0; idx < 80; ++idx)
        {
            fillPacket(send, idx % 10);
            send.transfer();
            chker.getStateAndReset(dstate, nstate);
            TS_ASSERT_EQUALS(dstate, 0);
            TS_ASSERT_EQUALS(nstate, 1);
        }
    }

    TS_ASSERT(replay == recv);
    TS_ASSERT(send == recv);

    mpt_var &recvr = recv;
    TS_ASSERT(replay == recvr);
    TS_ASSERT(send == recvr);
}

class Packet : public counter_var
{
public:
    COUNTERCONST(Packet)
    Packet(unsigned int counter_id);
    generic_var<int> a[10];
    generic_var<double> b[10];
};

Packet::Packet(unsigned int counter_id) :
    counter_var(counter_id)
{
    pop();
}

class InterfaceBase : public complex_var
{
public:
    RUNTIME_TYPE(InterfaceBase)
    InterfaceBase() : complex_var() {}
    void setRMDirty();
    void setBuddy(InterfaceBase *frnd);
    bool transfer(bool drop = false);
    mpt_var *getNext();

private:
    bool dirty;
    InterfaceBase *buddy;
};

void InterfaceBase::setRMDirty()
{
    dirty = true;

}

void InterfaceBase::setBuddy(InterfaceBase *frnd)
{
    buddy = frnd;
    dirty = false;
}

bool InterfaceBase::transfer(bool drop)
{
    if (!buddy) return false;
    mpt_show::refresh();
    if (!dirty) return false;

    const int sz = size();
    if (sz == 2) return false;

    unsigned char *buf = new unsigned char[sz];
    outbuf ob;
    ob.set(buf, sz);
    output(ob);
    dirty = false;
    if (!drop) buddy->extract(ob.size(), buf);
    delete[] buf;
    return true;
}

mpt_var * InterfaceBase::getNext()
{
    return this + 1;
}

class PacketInterface1 : public InterfaceBase
{
public:
    PacketInterface1(unsigned int counter_id = 0);
    Packet data;
};

PacketInterface1::PacketInterface1(unsigned int counter_id) : data(counter_id)
{
    pop();
}

class PacketInterface2 : public InterfaceBase
{
public:
    PacketInterface2();
    link_var data;
};

PacketInterface2::PacketInterface2()
{
    pop();
}

class PacketInterface3 : public InterfaceBase
{
public:
    PacketInterface3();
    generic_var<unsigned short> counter;
    link_var data[20];
};

PacketInterface3::PacketInterface3()
{
    pop();
}

template<class C>
void fillGenArray(generic_var<C> arry[], C start, C step = 1)
{
    for (int idx = 0; idx < 10; ++idx, start += step)
        arry[idx] = start;
}

template<class C>
void checkGenArray(const generic_var<C> arry[], C start, C step = 1)
{
    for (int idx = 0; idx < 10; ++idx, start += step)
        TS_ASSERT_EQUALS(arry[idx], start);
}

void CounterVarTestSuite::testRedundantTransfer(void)
{
    int dstate, nstate;
    PacketInterface1 send1a, send2a, send3a;
    PacketInterface2 send1b, send2b, send3b;
    PacketInterface1 recv1a(1), recv2a(1), recv3a(1);
    PacketInterface2 recv1b, recv2b, recv3b;

    send1b.data = send1a.data;
    send2b.data = send2a.data;
    send3b.data = send3a.data;
    recv1b.data = recv1a.data;
    recv2b.data = recv2a.data;
    recv3b.data = recv3a.data;

    send1a.setBuddy(&recv1a);
    send1b.setBuddy(&recv1b);
    send2a.setBuddy(&recv2a);
    send2b.setBuddy(&recv2b);
    send3a.setBuddy(&recv3a);
    send3b.setBuddy(&recv3b);

    send1a.setInvalid();
    recv1a.setInvalid();

    dirty_checker chker1a(&recv1a);
    dirty_checker chker1b(&recv1b);
    dirty_checker chker2a(&recv2a);
    dirty_checker chker2b(&recv2b);
    dirty_checker chker3a(&recv3a);
    dirty_checker chker3b(&recv3b);

    for (int idx = 0; idx < 70000; ++idx)
    {
        /**Block Transfer***********************************************************/

        // fill the 3 data, which should propagate to both a and b redundant interfaces
        fillGenArray(send1a.data.a, 0, 1);
        fillGenArray(send2a.data.a, 10, 1);
        fillGenArray(send3a.data.a, 20, 1);
        fillGenArray(send1a.data.b, 0.0, 1.1*(idx + 1));
        fillGenArray(send2a.data.b, 10.0, 1.1*(idx + 1));
        fillGenArray(send3a.data.b, 20.0, 1.1*(idx + 1));

        // multiple transfers on interface A (packets accepted)
        send1a.transfer();
        send2a.transfer();
        send3a.transfer();

        chker1a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker1b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker2a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker2b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker3a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker3b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        // multiple transfers on interface B (duplicate packet rejected)
        send1b.transfer();
        send2b.transfer();
        send3b.transfer();

        chker1a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        chker1b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        chker2a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        chker2b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        chker3a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        chker3b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        checkGenArray(recv1a.data.a, 0, 1);
        checkGenArray(recv2a.data.a, 10, 1);
        checkGenArray(recv3a.data.a, 20, 1);
        checkGenArray(recv1a.data.b, 0.0, 1.1*(idx + 1));
        checkGenArray(recv2a.data.b, 10.0, 1.1*(idx + 1));
        checkGenArray(recv3a.data.b, 20.0, 1.1*(idx + 1));

        /**Interleaved Transfer***********************************************************/

        // fill the 3 data, which should propagate to both a and b redundant interfaces
        fillGenArray(send1a.data.a, 5, 3);
        fillGenArray(send2a.data.a, 15, 3);
        fillGenArray(send3a.data.a, 25, 3);
        fillGenArray(send1a.data.b, 5.0, 3.1*(idx+1));
        fillGenArray(send2a.data.b, 15.0, 3.1*(idx+1));
        fillGenArray(send3a.data.b, 25.0, 3.1*(idx+1));

        // transfer on interface A (accepted)
        send1a.transfer();

        chker1a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker1b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        // transfer on interface B (interleaved, duplicate rejected)
        send1b.transfer();

        chker1a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        chker1b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        checkGenArray(recv1a.data.a, 5, 3);
        checkGenArray(recv1a.data.b, 5.0, 3.1*(idx + 1));

        // transfer on interface A (2nd msg) (accepted)
        send2a.transfer();

        chker2a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker2b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        // transfer on interface B (2nd msg)(interleaved, duplicate rejected)
        send2b.transfer();

        chker2a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        chker2b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        checkGenArray(recv2a.data.a, 15, 3);
        checkGenArray(recv2a.data.b, 15.0, 3.1*(idx + 1));

        // transfer on interface A (3rd msg) (accepted)
        send3a.transfer();

        chker3a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker3b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        // transfer on interface B (3rd msg)(interleaved, duplicate rejected)
        send3b.transfer();

        chker3a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        chker3b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        checkGenArray(recv3a.data.a, 25, 3);
        checkGenArray(recv3a.data.b, 25.0, 3.1*(idx + 1));

        /**Block Transfer(B interface first)************************************************/

        // fill the 3 data, which should propagate to both a and b redundant interfaces
        fillGenArray(send1a.data.a, 0, 1);
        fillGenArray(send2a.data.a, 10, 1);
        fillGenArray(send3a.data.a, 20, 1);
        fillGenArray(send1a.data.b, 0.0, 1.1*(idx+1));
        fillGenArray(send2a.data.b, 10.0, 1.1*(idx + 1));
        fillGenArray(send3a.data.b, 20.0, 1.1*(idx + 1));

        // multiple transfers on interface B (packets accepted)
        send1b.transfer();
        send2b.transfer();
        send3b.transfer();

        chker1a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker1b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker2a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker2b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker3a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker3b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        // multiple transfers on interface A (duplicate packet rejected)
        send1a.transfer();
        send2a.transfer();
        send3a.transfer();

        chker1a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        chker1b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        chker2a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        chker2b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        chker3a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        chker3b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        checkGenArray(recv1a.data.a, 0, 1);
        checkGenArray(recv2a.data.a, 10, 1);
        checkGenArray(recv3a.data.a, 20, 1);
        checkGenArray(recv1a.data.b, 0.0, 1.1*(idx + 1));
        checkGenArray(recv2a.data.b, 10.0, 1.1*(idx + 1));
        checkGenArray(recv3a.data.b, 20.0, 1.1*(idx + 1));

        /**Interleaved Transfer(B interface first)*************************************************/

        // fill the 3 data, which should propagate to both a and b redundant interfaces
        fillGenArray(send1a.data.a, 5, 3);
        fillGenArray(send2a.data.a, 15, 3);
        fillGenArray(send3a.data.a, 25, 3);
        fillGenArray(send1a.data.b, 5.0, 3.1*(idx + 1));
        fillGenArray(send2a.data.b, 15.0, 3.1*(idx + 1));
        fillGenArray(send3a.data.b, 25.0, 3.1*(idx + 1));

        // transfer on interface B (accepted)
        send1b.transfer();

        chker1a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker1b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        // transfer on interface A (interleaved, duplicate rejected)
        send1a.transfer();

        chker1a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        chker1b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        checkGenArray(recv1a.data.a, 5, 3);
        checkGenArray(recv1a.data.b, 5.0, 3.1*(idx + 1));

        // transfer on interface B (2nd msg) (accepted)
        send2b.transfer();

        chker2a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker2b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        // transfer on interface A (2nd msg)(interleaved, duplicate rejected)
        send2a.transfer();

        chker2a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        chker2b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        checkGenArray(recv2a.data.a, 15, 3);
        checkGenArray(recv2a.data.b, 15.0, 3.1*(idx + 1));

        // transfer on interface B (3rd msg) (accepted)
        send3b.transfer();

        chker3a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker3b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        // transfer on interface A (3rd msg)(interleaved, duplicate rejected)
        send3a.transfer();

        chker3a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        chker3b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 0);
        TS_ASSERT_EQUALS(nstate, 1);  // failed check on interface b

        checkGenArray(recv3a.data.a, 25, 3);
        checkGenArray(recv3a.data.b, 25.0, 3.1*(idx + 1));

        /**Block Transfer(redundant link packet drop)***************************************/

        // fill the 3 data, which should propagate to both a and b redundant interfaces
        fillGenArray(send1a.data.a, 500, 10);
        fillGenArray(send2a.data.a, 600, 10);
        fillGenArray(send3a.data.a, 700, 10);
        fillGenArray(send1a.data.b, 500.0, 10.1*(idx + 1));
        fillGenArray(send2a.data.b, 600.0, 10.1*(idx + 1));
        fillGenArray(send3a.data.b, 700.0, 10.1*(idx + 1));

        // multiple transfers on interface A (packets accepted)
        send3a.transfer();
        send2a.transfer();
        send1a.transfer();

        // multiple transfers on interface B (packet dropped)
        send3b.transfer(true);
        send2b.transfer(true);
        send1b.transfer(true);

        chker1a.getStateAndReset(dstate, nstate);
        if (dstate != 20)
        {
            TS_TRACE("here");
        }
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker1b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker2a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker2b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker3a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker3b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        checkGenArray(recv1a.data.a, 500, 10);
        checkGenArray(recv2a.data.a, 600, 10);
        checkGenArray(recv3a.data.a, 700, 10);
        checkGenArray(recv1a.data.b, 500.0, 10.1*(idx + 1));
        checkGenArray(recv2a.data.b, 600.0, 10.1*(idx + 1));
        checkGenArray(recv3a.data.b, 700.0, 10.1*(idx + 1));

        // fill the 3 data, which should propagate to both a and b redundant interfaces
        fillGenArray(send1a.data.a, 550, 11);
        fillGenArray(send2a.data.a, 650, 11);
        fillGenArray(send3a.data.a, 750, 11);
        fillGenArray(send1a.data.b, -2550.0, 11.3*(idx + 1));
        fillGenArray(send2a.data.b, -3650.0, 11.3*(idx + 1));
        fillGenArray(send3a.data.b, -4750.0, 11.3*(idx + 1));

        // multiple transfers on interface A (packets accepted)
        send3a.transfer(true);
        send2a.transfer(true);
        send1a.transfer(true);

        // multiple transfers on interface B (packet dropped)
        send3b.transfer();
        send2b.transfer();
        send1b.transfer();

        chker1a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker1b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker2a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker2b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker3a.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        chker3b.getStateAndReset(dstate, nstate);
        TS_ASSERT_EQUALS(dstate, 20); // array of 20
        TS_ASSERT_EQUALS(nstate, 0);

        checkGenArray(recv1a.data.a, 550, 11);
        checkGenArray(recv2a.data.a, 650, 11);
        checkGenArray(recv3a.data.a, 750, 11);
        checkGenArray(recv1a.data.b, -2550.0, 11.3*(idx + 1));
        checkGenArray(recv2a.data.b, -3650.0, 11.3*(idx + 1));
        checkGenArray(recv3a.data.b, -4750.0, 11.3*(idx + 1));
    }

    TS_ASSERT(send1a == recv1b);
    TS_ASSERT(send2a == recv2b);
    TS_ASSERT(send3a == recv3b);
    TS_ASSERT(send1b == recv1a);
    TS_ASSERT(send2b == recv2a);
    TS_ASSERT(send3b == recv3a);
    TS_ASSERT(send1b.data == recv1b.data);
    TS_ASSERT(send2b.data == recv2b.data);
    TS_ASSERT(send3b.data == recv3b.data);

    TS_ASSERT(recv1a.isValid());
    TS_ASSERT(recv2a.isValid());
    TS_ASSERT(recv3a.isValid());
    TS_ASSERT(recv1b.isValid());
    TS_ASSERT(recv2b.isValid());
    TS_ASSERT(recv3b.isValid());

    recv1b.setInvalid();
    recv2a.setInvalid();
    recv3b.setInvalid();

    TS_ASSERT(!recv1a.isValid());
    TS_ASSERT(!recv2a.isValid());
    TS_ASSERT(!recv3a.isValid());
    TS_ASSERT(!recv1b.isValid());
    TS_ASSERT(!recv2b.isValid());
    TS_ASSERT(!recv3b.isValid());

    TS_ASSERT(send1a != recv1b);
    TS_ASSERT(send2a != recv2b);
    TS_ASSERT(send3a != recv3b);
    TS_ASSERT(send1b != recv1a);
    TS_ASSERT(send2b != recv2a);
    TS_ASSERT(send3b != recv3a);
    TS_ASSERT(send1b.data != recv1b.data);
    TS_ASSERT(send2b.data != recv2b.data);
    TS_ASSERT(send3b.data != recv3b.data);

    value_var::set_float_format("%.11g");
    std::ostringstream oss;
    oss << send1a;
    std::ostringstream oss2;
    oss2 << send1b;
    TS_ASSERT_EQUALS(oss.str(), oss2.str());
    std::istringstream iss(oss.str());
    iss >> recv1b;
    TS_ASSERT_EQUALS(send1b, recv1b);
    TS_ASSERT_EQUALS(send1a, recv1a);
    TS_ASSERT(send1a == recv1b);

    dirty_checker chk_1(&(recv1a.data));
    dirty_checker chk_2(&(recv2a.data));
    dirty_checker chk_3(&(recv3a.data));
    recv1a.refresh();
    recv2a.refresh();
    recv3a.refresh();
    TS_ASSERT_EQUALS(chk_1.getRefreshStateAndReset(), 1);
    TS_ASSERT_EQUALS(chk_2.getRefreshStateAndReset(), 1);
    TS_ASSERT_EQUALS(chk_3.getRefreshStateAndReset(), 1);
    recv1b.refresh();
    recv2b.refresh();
    recv3b.refresh();
    TS_ASSERT_EQUALS(chk_1.getRefreshStateAndReset(), 1);
    TS_ASSERT_EQUALS(chk_2.getRefreshStateAndReset(), 1);
    TS_ASSERT_EQUALS(chk_3.getRefreshStateAndReset(), 1);

    // Test link to indivdual generic vars
    PacketInterface1 send4a(2), recv4a(3);
    PacketInterface3 send4b, recv4b;
    for (int idx = 0; idx < 10; ++idx)
    {
        send4b.data[idx] = send4a.data.a[idx];
        recv4b.data[idx] = recv4a.data.a[idx];
    }
    for (int idx = 0; idx < 10; ++idx)
    {
        send4b.data[idx + 10] = send4a.data.b[idx];
        recv4b.data[idx + 10] = recv4a.data.b[idx];
    }
    send4a.setBuddy(&recv4b); // Invert the relationship
    send4b.setBuddy(&recv4a);

    dirty_checker chker4a(&recv4a);
    dirty_checker chker4b(&recv4b);
    fillGenArray(send4a.data.a, rand(), rand());
    fillGenArray<double>(send4a.data.b, (double)rand(), 100. + rand() / rand());

    send4a.transfer();
    chker4a.getStateAndReset(dstate, nstate);
    TS_ASSERT_EQUALS(dstate, 20); // array of 20
    TS_ASSERT_EQUALS(nstate, 0);

    chker4b.getStateAndReset(dstate, nstate);
    TS_ASSERT_EQUALS(dstate, 21); // array of 20 + counter
    TS_ASSERT_EQUALS(nstate, 0);

    send4b.counter = 0;

    TS_ASSERT_EQUALS(send4a, recv4a);
    TS_ASSERT_EQUALS(send4b, recv4b);

    fillGenArray(send4a.data.a, rand(), rand());
    fillGenArray<double>(send4a.data.b, rand(), 100. + (double)rand() / rand());
    send4b.transfer();
    chker4a.getStateAndReset(dstate, nstate);
    TS_ASSERT_EQUALS(dstate, 20); // array of 20
    TS_ASSERT_EQUALS(nstate, 0);

    chker4b.getStateAndReset(dstate, nstate);
    TS_ASSERT_EQUALS(dstate, 20); // array of 20 + counter
    TS_ASSERT_EQUALS(nstate, 0);

    TS_ASSERT_EQUALS(send4a, recv4a);
    TS_ASSERT_EQUALS(send4b, recv4b);
    TS_ASSERT_EQUALS(send4b.data[11], recv4a.data.b[1]);
    TS_ASSERT_EQUALS(send4a.data.b[2], recv4b.data[12]);
    TS_ASSERT_EQUALS(send4b.data[13], recv4b.data[13]);

    TS_ASSERT_EQUALS(recv4b.data[0].getNext(), &(recv4a.data.a[1]));
}

class ExtendedPacket1 : public counter_var
{
public:
    EXTCOUNTERCONST(ExtendedPacket1, 0, 1, 2, 0, 0)
    generic_var<int> a[10];
    array_var<int> b;
    generic_var<short> c;
    string_var<5> d;

    void setRMDirty();
    void setBuddy(ExtendedPacket1 *frnd);
    bool transfer(bool drop = false);

private:
    bool dirty;
    ExtendedPacket1 *buddy;
};

void ExtendedPacket1::setRMDirty()
{
    dirty = true;
    counter_var::setRMDirty();
}

void ExtendedPacket1::setBuddy(ExtendedPacket1 *frnd)
{
    buddy = frnd;
    if (frnd->id == 0)
        frnd->id = 1;
    else
        id = 2;
    dirty = false;
}

bool ExtendedPacket1::transfer(bool drop)
{
    if (!buddy) return false;
    mpt_show::refresh();
    if (!dirty) return false;

    const int sz = size();
    if (sz == checksum_size()) return false;

    unsigned char *buf = new unsigned char[sz];
    outbuf ob;
    ob.set(buf, sz);
    output(ob);
    dirty = false;
    if (!drop) buddy->extract(ob.size(), buf);
    delete[] buf;
    return true;
}

void fillPacket1(ExtendedPacket1 &packet, int v)
{
    for (int idx = 0; idx < 10; ++idx)
        packet.a[idx] = v;
    packet.b.resize(v + 1);
    int *b = packet.b;
    for (int idx = 0; idx <= v; ++idx)
        b[idx] = v;
    packet.c = (short)v;
    const char ch = '0' + (char)v;
    packet.d[0] = ch;
    packet.d[1] = ch;
    packet.d[2] = ch;
    packet.d[3] = ch;
    packet.d[4] = ch;
}

void chkPacket1(const ExtendedPacket1 &packet, int v)
{
    for (int idx = 0; idx < 10; ++idx)
        TS_ASSERT_EQUALS(packet.a[idx], v);
    const int bsz = packet.b.cardinal();
    TS_ASSERT_EQUALS(bsz, v + 1);
    const int *b = packet.b;
    for (int idx = 0; idx < bsz; ++idx)
        TS_ASSERT_EQUALS(b[idx], v);
    TS_ASSERT_EQUALS(packet.c.to_short(), (short)v);
    const char ch = '0' + (char)v;
    TS_ASSERT_EQUALS(packet.d[0], ch);
    TS_ASSERT_EQUALS(packet.d[1], ch);
    TS_ASSERT_EQUALS(packet.d[2], ch);
    TS_ASSERT_EQUALS(packet.d[3], ch);
    TS_ASSERT_EQUALS(packet.d[4], ch);
}

void CounterVarTestSuite::testDataTransfer1(void)
{
    ExtendedPacket1 send, recv;
    dirty_checker chker(&recv);
    int dstate, nstate;
    send.setBuddy(&recv);
    recv.setBuddy(&send);

    /* Test until the counter loops around at 65535
    As chk_var will generate a notDirty on check
    failure (and nothing) on success, we use this
    to test if the counter checks passed. However,
    it is fragile because it may be confused with
    triggers from its children also.
    */
    for (int idx = 0; idx < 500; ++idx)
    {
        fillPacket1(send, idx % 10);
        send.transfer();
        chker.getStateAndReset(dstate, nstate);
        TS_ASSERT_LESS_THAN(0, dstate);
        TS_ASSERT_EQUALS(nstate, 0);
        chkPacket1(recv, idx % 10);
    }

    TS_ASSERT(send == recv);
    mpt_var &recvr = recv;
    TS_ASSERT(send == recvr);

    ExtendedPacket1 cpy(send);
    chkPacket1(cpy, 9);
    TS_ASSERT(send == cpy);

    fillPacket1(recv, 4);
    TS_ASSERT(!(send == recv));
    recv = send;
    chkPacket1(recv, 9);
    TS_ASSERT(send == recvr);

    fillPacket1(cpy, 5);
    TS_ASSERT(!(send == cpy));
    cpy = recvr;
    TS_ASSERT(send == cpy);
}

void CounterVarTestSuite::testDroppedPackets1(void)
{
    ExtendedPacket1 send, recv;
    dirty_checker chker(&recv);
    int dstate, nstate;
    send.setBuddy(&recv);
    recv.setBuddy(&send);

    /* Test for a bit */
    for (int idx = 0; idx < 501; ++idx)
    {
        fillPacket1(send, idx % 10);
        send.transfer(idx % 2);
        chker.getStateAndReset(dstate, nstate);
        if (idx % 2)
        {
            TS_ASSERT_EQUALS(dstate, 0);
            TS_ASSERT_EQUALS(nstate, 0);
        }
        else
        {
            TS_ASSERT_LESS_THAN(0, dstate);
            TS_ASSERT_EQUALS(nstate, 0);
            chkPacket1(recv, idx % 10);
        }
    }
    TS_ASSERT(send == recv);
    mpt_var &recvr = recv;
    TS_ASSERT(send == recvr);
}

void CounterVarTestSuite::testManyDroppedPackets1(void)
{
    ExtendedPacket1 send, recv;
    dirty_checker chker(&recv);
    int dstate, nstate;
    send.setBuddy(&recv);
    recv.setBuddy(&send);

    /* Test until the counter loops around at 65535 */
    /* Note that the tests are a bit fragile because
    if the loss is an exact multiple of 10 (fillPacket
    repetition) then when the packet is extracted,
    it will produce many notDirty because the data
    is the same
    */
    for (int idx = 0; idx < 520; ++idx)
    {
        bool drop = (idx % 100) > 55;
        fillPacket1(send, idx % 10);
        send.transfer(drop);
        chker.getStateAndReset(dstate, nstate);
        if (drop)
        {
            TS_ASSERT_EQUALS(dstate, 0);
            TS_ASSERT_EQUALS(nstate, 0);
        }
        else
        {
            TS_ASSERT_LESS_THAN(0, dstate);
            TS_ASSERT_EQUALS(nstate, 0);
            chkPacket1(recv, idx % 10);
        }
    }
    TS_ASSERT(send == recv);
    mpt_var &recvr = recv;
    TS_ASSERT(send == recvr);
}

void CounterVarTestSuite::testVeryOldData1(void)
{
    ExtendedPacket1 send, replay, recv;
    int dstate, nstate;
    send.setBuddy(&recv);
    replay.setBuddy(&recv);
    send.setInvalid();
    replay.setInvalid();
    recv.setInvalid();
    dirty_checker chker(&recv);

    for (int idy = 0; idy < 450; ++idy)
    {
        for (int idx = 0; idx < 80; ++idx)
        {
            fillPacket1(send, idx % 10);
            send.transfer();
            chker.getStateAndReset(dstate, nstate);
            TS_ASSERT_LESS_THAN(0, dstate);
            TS_ASSERT_EQUALS(nstate, 0);
            chkPacket1(recv, idx % 10);
        }

        for (int idx = 0; idx < 80; ++idx)
        {
            fillPacket1(replay, idx % 10);
            replay.transfer();
            chker.getStateAndReset(dstate, nstate);
            TS_ASSERT_EQUALS(dstate, 0);
            TS_ASSERT_EQUALS(nstate, 1);
        }

        for (int idx = 0; idx < 80; ++idx)
        {
            fillPacket1(replay, idx % 10);
            replay.transfer();
            chker.getStateAndReset(dstate, nstate);
            TS_ASSERT_LESS_THAN(0, dstate);
            TS_ASSERT_EQUALS(nstate, 0);
            chkPacket1(recv, idx % 10);
        }

        for (int idx = 0; idx < 80; ++idx)
        {
            fillPacket1(send, idx % 10);
            send.transfer();
            chker.getStateAndReset(dstate, nstate);
            TS_ASSERT_EQUALS(dstate, 0);
            TS_ASSERT_EQUALS(nstate, 1);
        }
    }

    TS_ASSERT(replay == recv);
    TS_ASSERT(send == recv);

    mpt_var &recvr = recv;
    TS_ASSERT(replay == recvr);
    TS_ASSERT(send == recvr);
}

class ExtendedPacket4 : public counter_var
{
public:
    EXTCOUNTERCONST(ExtendedPacket4, 0, 4, 2, 0, 0)
    generic_var<int> a[10];
    array_var<int> b;
    generic_var<short> c;
    string_var<5> d;

    void setRMDirty();
    void setBuddy(ExtendedPacket4 *frnd);
    bool transfer(bool drop = false);

private:
    bool dirty;
    ExtendedPacket4 *buddy;
};

void ExtendedPacket4::setRMDirty()
{
    dirty = true;
    counter_var::setRMDirty();
}

void ExtendedPacket4::setBuddy(ExtendedPacket4 *frnd)
{
    buddy = frnd;
    if (frnd->id == 0)
        frnd->id = 1;
    else
        id = 2;
    dirty = false;
}

bool ExtendedPacket4::transfer(bool drop)
{
    if (!buddy) return false;
    mpt_show::refresh();
    if (!dirty) return false;

    const int sz = size();
    if (sz == checksum_size()) return false;

    unsigned char *buf = new unsigned char[sz];
    outbuf ob;
    ob.set(buf, sz);
    output(ob);
    dirty = false;
    if (!drop) buddy->extract(ob.size(), buf);
    delete[] buf;
    return true;
}

void fillPacket4(ExtendedPacket4 &packet, int v)
{
    for (int idx = 0; idx < 10; ++idx)
        packet.a[idx] = v;
    packet.b.resize(v + 1);
    int *b = packet.b;
    for (int idx = 0; idx <= v; ++idx)
        b[idx] = v;
    packet.c = (short)v;
    const char ch = '0' + (char)v;
    packet.d[0] = ch;
    packet.d[1] = ch;
    packet.d[2] = ch;
    packet.d[3] = ch;
    packet.d[4] = ch;
}

void chkPacket4(const ExtendedPacket4 &packet, int v)
{
    for (int idx = 0; idx < 10; ++idx)
        TS_ASSERT_EQUALS(packet.a[idx], v);
    const int bsz = packet.b.cardinal();
    TS_ASSERT_EQUALS(bsz, v + 1);
    const int *b = packet.b;
    for (int idx = 0; idx < bsz; ++idx)
        TS_ASSERT_EQUALS(b[idx], v);
    TS_ASSERT_EQUALS(packet.c.to_short(), (short)v);
    const char ch = '0' + (char)v;
    TS_ASSERT_EQUALS(packet.d[0], ch);
    TS_ASSERT_EQUALS(packet.d[1], ch);
    TS_ASSERT_EQUALS(packet.d[2], ch);
    TS_ASSERT_EQUALS(packet.d[3], ch);
    TS_ASSERT_EQUALS(packet.d[4], ch);
}

void CounterVarTestSuite::testDataTransfer4(void)
{
    ExtendedPacket4 send, recv;
    dirty_checker chker(&recv);
    int dstate, nstate;
    send.setBuddy(&recv);
    recv.setBuddy(&send);

    /* Test until the counter loops around at 65535
    As chk_var will generate a notDirty on check
    failure (and nothing) on success, we use this
    to test if the counter checks passed. However,
    it is fragile because it may be confused with
    triggers from its children also.
    */
    for (int idx = 0; idx < 500; ++idx)
    {
        fillPacket4(send, idx % 10);
        send.transfer();
        chker.getStateAndReset(dstate, nstate);
        TS_ASSERT_LESS_THAN(0, dstate);
        TS_ASSERT_EQUALS(nstate, 0);
        chkPacket4(recv, idx % 10);
    }

    TS_ASSERT(send == recv);
    mpt_var &recvr = recv;
    TS_ASSERT(send == recvr);

    ExtendedPacket4 cpy(send);
    chkPacket4(cpy, 9);
    TS_ASSERT(send == cpy);

    fillPacket4(recv, 4);
    TS_ASSERT(!(send == recv));
    recv = send;
    chkPacket4(recv, 9);
    TS_ASSERT(send == recvr);

    fillPacket4(cpy, 5);
    TS_ASSERT(!(send == cpy));
    cpy = recvr;
    TS_ASSERT(send == cpy);
}

void CounterVarTestSuite::testDroppedPackets4(void)
{
    ExtendedPacket4 send, recv;
    dirty_checker chker(&recv);
    int dstate, nstate;
    send.setBuddy(&recv);
    recv.setBuddy(&send);

    /* Test for a bit */
    for (int idx = 0; idx < 501; ++idx)
    {
        fillPacket4(send, idx % 10);
        send.transfer(idx % 2);
        chker.getStateAndReset(dstate, nstate);
        if (idx % 2)
        {
            TS_ASSERT_EQUALS(dstate, 0);
            TS_ASSERT_EQUALS(nstate, 0);
        }
        else
        {
            TS_ASSERT_LESS_THAN(0, dstate);
            TS_ASSERT_EQUALS(nstate, 0);
            chkPacket4(recv, idx % 10);
        }
    }
    TS_ASSERT(send == recv);
    mpt_var &recvr = recv;
    TS_ASSERT(send == recvr);
}

void CounterVarTestSuite::testManyDroppedPackets4(void)
{
    ExtendedPacket4 send, recv;
    dirty_checker chker(&recv);
    int dstate, nstate;
    send.setBuddy(&recv);
    recv.setBuddy(&send);

    /* Test until the counter loops around at 65535 */
    /* Note that the tests are a bit fragile because
    if the loss is an exact multiple of 10 (fillPacket
    repetition) then when the packet is extracted,
    it will produce many notDirty because the data
    is the same
    */
    for (long long idx = 0; idx < 66020; ++idx)
    {
        bool drop = (idx % 1000) > 505;
        fillPacket4(send, idx % 10);
        send.transfer(drop);
        chker.getStateAndReset(dstate, nstate);
        if (drop)
        {
            TS_ASSERT_EQUALS(dstate, 0);
            TS_ASSERT_EQUALS(nstate, 0);
        }
        else
        {
            TS_ASSERT_LESS_THAN(0, dstate);
            TS_ASSERT_EQUALS(nstate, 0);
            chkPacket4(recv, idx % 10);
        }
    }
    TS_ASSERT(send == recv);
    mpt_var &recvr = recv;
    TS_ASSERT(send == recvr);
}

void CounterVarTestSuite::testVeryOldData4(void)
{
    ExtendedPacket4 send, replay, recv;
    int dstate, nstate;
    send.setBuddy(&recv);
    replay.setBuddy(&recv);
    send.setInvalid();
    replay.setInvalid();
    recv.setInvalid();
    dirty_checker chker(&recv);

    for (int idy = 0; idy < 450; ++idy)
    {
        for (int idx = 0; idx < 80; ++idx)
        {
            fillPacket4(send, idx % 10);
            send.transfer();
            chker.getStateAndReset(dstate, nstate);
            TS_ASSERT_LESS_THAN(0, dstate);
            TS_ASSERT_EQUALS(nstate, 0);
            chkPacket4(recv, idx % 10);
        }

        for (int idx = 0; idx < 80; ++idx)
        {
            fillPacket4(replay, idx % 10);
            replay.transfer();
            chker.getStateAndReset(dstate, nstate);
            TS_ASSERT_EQUALS(dstate, 0);
            TS_ASSERT_EQUALS(nstate, 1);
        }

        for (int idx = 0; idx < 80; ++idx)
        {
            fillPacket4(replay, idx % 10);
            replay.transfer();
            chker.getStateAndReset(dstate, nstate);
            TS_ASSERT_LESS_THAN(0, dstate);
            TS_ASSERT_EQUALS(nstate, 0);
            chkPacket4(recv, idx % 10);
        }

        for (int idx = 0; idx < 80; ++idx)
        {
            fillPacket4(send, idx % 10);
            send.transfer();
            chker.getStateAndReset(dstate, nstate);
            TS_ASSERT_EQUALS(dstate, 0);
            TS_ASSERT_EQUALS(nstate, 1);
        }
    }

    TS_ASSERT(replay == recv);
    TS_ASSERT(send == recv);

    mpt_var &recvr = recv;
    TS_ASSERT(replay == recvr);
    TS_ASSERT(send == recvr);

    TS_TRACE("End");
}

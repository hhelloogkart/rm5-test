#ifndef _TEST_COUNTER_VAR_H_DEF_
#define _TEST_COUNTER_VAR_H_DEF_

#include <cxxtest/TestSuite.h>

class CounterVarTestSuite : public CxxTest::TestSuite
{
public:
    void testDataTransfer(void);
    void testRedundantTransfer(void);
    void testDroppedPackets(void);
    void testManyDroppedPackets(void);
    void testVeryOldData(void);
    void testDataTransfer1(void);
    void testDroppedPackets1(void);
    void testManyDroppedPackets1(void);
    void testVeryOldData1(void);
    void testDataTransfer4(void);
    void testDroppedPackets4(void);
    void testManyDroppedPackets4(void);
    void testVeryOldData4(void);
};

#endif

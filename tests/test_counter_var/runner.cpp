/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_counter_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_CounterVarTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_counter_var\test_counter_var.h"

static CounterVarTestSuite suite_CounterVarTestSuite;

static CxxTest::List Tests_CounterVarTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_CounterVarTestSuite( "test_counter_var/test_counter_var.h", 6, "CounterVarTestSuite", suite_CounterVarTestSuite, Tests_CounterVarTestSuite );

static class TestDescription_suite_CounterVarTestSuite_testDataTransfer : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_CounterVarTestSuite_testDataTransfer() : CxxTest::RealTestDescription( Tests_CounterVarTestSuite, suiteDescription_CounterVarTestSuite, 9, "testDataTransfer" ) {}
 void runTest() { suite_CounterVarTestSuite.testDataTransfer(); }
} testDescription_suite_CounterVarTestSuite_testDataTransfer;

static class TestDescription_suite_CounterVarTestSuite_testRedundantTransfer : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_CounterVarTestSuite_testRedundantTransfer() : CxxTest::RealTestDescription( Tests_CounterVarTestSuite, suiteDescription_CounterVarTestSuite, 10, "testRedundantTransfer" ) {}
 void runTest() { suite_CounterVarTestSuite.testRedundantTransfer(); }
} testDescription_suite_CounterVarTestSuite_testRedundantTransfer;

static class TestDescription_suite_CounterVarTestSuite_testDroppedPackets : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_CounterVarTestSuite_testDroppedPackets() : CxxTest::RealTestDescription( Tests_CounterVarTestSuite, suiteDescription_CounterVarTestSuite, 11, "testDroppedPackets" ) {}
 void runTest() { suite_CounterVarTestSuite.testDroppedPackets(); }
} testDescription_suite_CounterVarTestSuite_testDroppedPackets;

static class TestDescription_suite_CounterVarTestSuite_testManyDroppedPackets : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_CounterVarTestSuite_testManyDroppedPackets() : CxxTest::RealTestDescription( Tests_CounterVarTestSuite, suiteDescription_CounterVarTestSuite, 12, "testManyDroppedPackets" ) {}
 void runTest() { suite_CounterVarTestSuite.testManyDroppedPackets(); }
} testDescription_suite_CounterVarTestSuite_testManyDroppedPackets;

static class TestDescription_suite_CounterVarTestSuite_testVeryOldData : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_CounterVarTestSuite_testVeryOldData() : CxxTest::RealTestDescription( Tests_CounterVarTestSuite, suiteDescription_CounterVarTestSuite, 13, "testVeryOldData" ) {}
 void runTest() { suite_CounterVarTestSuite.testVeryOldData(); }
} testDescription_suite_CounterVarTestSuite_testVeryOldData;

static class TestDescription_suite_CounterVarTestSuite_testDataTransfer1 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_CounterVarTestSuite_testDataTransfer1() : CxxTest::RealTestDescription( Tests_CounterVarTestSuite, suiteDescription_CounterVarTestSuite, 14, "testDataTransfer1" ) {}
 void runTest() { suite_CounterVarTestSuite.testDataTransfer1(); }
} testDescription_suite_CounterVarTestSuite_testDataTransfer1;

static class TestDescription_suite_CounterVarTestSuite_testDroppedPackets1 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_CounterVarTestSuite_testDroppedPackets1() : CxxTest::RealTestDescription( Tests_CounterVarTestSuite, suiteDescription_CounterVarTestSuite, 15, "testDroppedPackets1" ) {}
 void runTest() { suite_CounterVarTestSuite.testDroppedPackets1(); }
} testDescription_suite_CounterVarTestSuite_testDroppedPackets1;

static class TestDescription_suite_CounterVarTestSuite_testManyDroppedPackets1 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_CounterVarTestSuite_testManyDroppedPackets1() : CxxTest::RealTestDescription( Tests_CounterVarTestSuite, suiteDescription_CounterVarTestSuite, 16, "testManyDroppedPackets1" ) {}
 void runTest() { suite_CounterVarTestSuite.testManyDroppedPackets1(); }
} testDescription_suite_CounterVarTestSuite_testManyDroppedPackets1;

static class TestDescription_suite_CounterVarTestSuite_testVeryOldData1 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_CounterVarTestSuite_testVeryOldData1() : CxxTest::RealTestDescription( Tests_CounterVarTestSuite, suiteDescription_CounterVarTestSuite, 17, "testVeryOldData1" ) {}
 void runTest() { suite_CounterVarTestSuite.testVeryOldData1(); }
} testDescription_suite_CounterVarTestSuite_testVeryOldData1;

static class TestDescription_suite_CounterVarTestSuite_testDataTransfer4 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_CounterVarTestSuite_testDataTransfer4() : CxxTest::RealTestDescription( Tests_CounterVarTestSuite, suiteDescription_CounterVarTestSuite, 18, "testDataTransfer4" ) {}
 void runTest() { suite_CounterVarTestSuite.testDataTransfer4(); }
} testDescription_suite_CounterVarTestSuite_testDataTransfer4;

static class TestDescription_suite_CounterVarTestSuite_testDroppedPackets4 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_CounterVarTestSuite_testDroppedPackets4() : CxxTest::RealTestDescription( Tests_CounterVarTestSuite, suiteDescription_CounterVarTestSuite, 19, "testDroppedPackets4" ) {}
 void runTest() { suite_CounterVarTestSuite.testDroppedPackets4(); }
} testDescription_suite_CounterVarTestSuite_testDroppedPackets4;

static class TestDescription_suite_CounterVarTestSuite_testManyDroppedPackets4 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_CounterVarTestSuite_testManyDroppedPackets4() : CxxTest::RealTestDescription( Tests_CounterVarTestSuite, suiteDescription_CounterVarTestSuite, 20, "testManyDroppedPackets4" ) {}
 void runTest() { suite_CounterVarTestSuite.testManyDroppedPackets4(); }
} testDescription_suite_CounterVarTestSuite_testManyDroppedPackets4;

static class TestDescription_suite_CounterVarTestSuite_testVeryOldData4 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_CounterVarTestSuite_testVeryOldData4() : CxxTest::RealTestDescription( Tests_CounterVarTestSuite, suiteDescription_CounterVarTestSuite, 21, "testVeryOldData4" ) {}
 void runTest() { suite_CounterVarTestSuite.testVeryOldData4(); }
} testDescription_suite_CounterVarTestSuite_testVeryOldData4;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

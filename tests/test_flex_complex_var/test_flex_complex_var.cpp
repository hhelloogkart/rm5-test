#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>
#include <gen_var.h>
#include <cplx_var.h>
#include <flex_var.h>
#include <fcpl_var.h>
#include <iostream>
#include <sstream>
#include "test_flex_complex_var.h"
using namespace std;

template<class C>
C rb(C in)
{
    C out = in;
    char *left = reinterpret_cast<char *>(&out);
    char *right = left + sizeof(C) - 1;
    while (left < right)
    {
        const char tmp = *left;
        *left = *right;
        *right = tmp;
        ++left;
        --right;
    }
    return out;
}

class dirty_checker : public mpt_baseshow
{
public:
    dirty_checker(mpt_var *myvar);
    virtual void setDirty(mpt_var *myvar);
    virtual void notDirty(mpt_var *myvar);
    int getStateAndReset();
protected:
    int state; // 0 - not called, 1 - dirty, 2 - notdirty, 3 - error
    mpt_var *var;
};

dirty_checker::dirty_checker(mpt_var *myvar) : state(0), var(myvar)
{
    myvar->addCallback(this);
}

void dirty_checker::setDirty(mpt_var *myvar)
{
    if (myvar == var) state = 1;
    else state = 3;
}

void dirty_checker::notDirty(mpt_var *myvar)
{
    if (myvar == var) state = 2;
    else state = 3;
}

int dirty_checker::getStateAndReset()
{
    int ret = state;
    state = 0;
    return ret;
}

class my_complex_var : public complex_var
{
public:
    COMPLEXCONST( my_complex_var )
    generic_var<float> flt1;
    generic_var<float> flt2;
    generic_var<double> dbl;
    generic_var<char>  iv1;
    generic_var<short> iv2;
    generic_var<int>   iv3;
    generic_var<long long>   iv4;
    generic_var<unsigned char>  iv5;
    generic_var<unsigned short> iv6;
    generic_var<unsigned int>   iv7;
    generic_var<unsigned long long>   iv8;
};

class my_complex_var1 : public complex_var
{
public:
    COMPLEXCONST( my_complex_var1 )
    generic_var<double> dbl1;
    generic_var<double> dbl2;
    generic_var<int>   iv1;
};

class my_complex_var2 : public complex_var
{
public:
    COMPLEXCONST( my_complex_var2 )
    generic_var<int>   iv1;
    generic_var<int>   iv2;
    generic_var<double> dbl1;
    generic_var<unsigned long>   iv3;
    generic_var<double> dbl2;
};

void FlexComplexVarTestSuite::testComplex(void)
{
    my_complex_var *test = new my_complex_var();
    my_complex_var1 *test1 = new my_complex_var1();

    TS_ASSERT(!test->isValid());
    TS_TRACE("1. Test my_complex_var structure\n");
    test->flt1 = 12.23f;
    test->flt2 = -23.345f;
    test->dbl = 6543.2121;
    test->iv1 = -5;
    test->iv2 = -300;
    test->iv3 = -765;
    test->iv4 = -87654;
    test->iv5 = 5;
    test->iv6 = 300;
    test->iv7 = 765;
    test->iv8 = 87654;
    TS_ASSERT(test->size() == 2*sizeof(float)+sizeof(double)+sizeof(char)+sizeof(short)+sizeof(int)
        +sizeof(long long)+sizeof(unsigned char)+sizeof(unsigned short)+sizeof(unsigned int)
        +sizeof(unsigned long long));
    TS_ASSERT(test->cardinal() == 11);
    TS_ASSERT_DELTA(test->flt1.to_float(), 12.23f, 0.001);
    TS_ASSERT_DELTA(test->flt2.to_float(), -23.345f, 0.001);
    TS_ASSERT_DELTA(test->dbl.to_double(), 6543.2121, 0.0001);
    TS_ASSERT_EQUALS(test->iv1.to_char(), -5);
    TS_ASSERT_EQUALS(test->iv2.to_short(), -300);
    TS_ASSERT_EQUALS(test->iv3.to_int(), -765);
    TS_ASSERT_EQUALS(test->iv4.to_long(), -87654);
    TS_ASSERT_EQUALS(test->iv5.to_unsigned_char(), 5);
    TS_ASSERT_EQUALS(test->iv6.to_unsigned_short(), 300);
    TS_ASSERT_EQUALS(test->iv7.to_unsigned_int(), 765);
    TS_ASSERT_EQUALS(test->iv8.to_unsigned_long(), 87654);
    TS_ASSERT(test->isValid());

    // Apply changes to some variables
    TS_TRACE("2. Testing my_complex_var structure again\n");
    test->flt1 = -44.55f;
    test->dbl = -567.7777;
    test->iv3 = 66;
    test->iv8 = 999;
    TS_ASSERT(test->size() == 2*sizeof(float)+sizeof(double)+sizeof(char)+sizeof(short)+sizeof(int)
        +sizeof(long long)+sizeof(unsigned char)+sizeof(unsigned short)+sizeof(unsigned int)
        +sizeof(unsigned long long));
    TS_ASSERT(test->cardinal() == 11);
    TS_ASSERT_DELTA(test->flt1.to_float(), -44.55f, 0.001);
    TS_ASSERT_DELTA(test->flt2.to_float(), -23.345f, 0.001);
    TS_ASSERT_DELTA(test->dbl.to_double(), -567.7777, 0.0001);
    TS_ASSERT_EQUALS(test->iv1.to_char(), -5);
    TS_ASSERT_EQUALS(test->iv2.to_short(), -300);
    TS_ASSERT_EQUALS(test->iv3.to_int(), 66);
    TS_ASSERT_EQUALS(test->iv4.to_long(), -87654);
    TS_ASSERT_EQUALS(test->iv5.to_unsigned_char(), 5);
    TS_ASSERT_EQUALS(test->iv6.to_unsigned_short(), 300);
    TS_ASSERT_EQUALS(test->iv7.to_unsigned_int(), 765);
    TS_ASSERT_EQUALS(test->iv8.to_unsigned_long(), 999);
    TS_ASSERT(test->isValid());

    // Test my_complex_var1
    TS_ASSERT(!test1->isValid());
    TS_TRACE("3. Test another my_complex_var1 structure\n");
    test1->dbl1 = 12345.678;
    test1->dbl2 = -4444.55555;
    test1->iv1 = 555;
    TS_ASSERT(test1->size() == 2*sizeof(double)+sizeof(int));
    TS_ASSERT(test1->cardinal() == 3);
    TS_ASSERT_DELTA(test1->dbl1.to_double(), 12345.678, 0.0001);
    TS_ASSERT_DELTA(test1->dbl2.to_double(), -4444.55555, 0.0001);
    TS_ASSERT_EQUALS(test1->iv1.to_int(), 555);
    TS_ASSERT(test1->isValid());

    // Check my_complex_var values again
    TS_TRACE("4. Test my_complex_var structure again\n");
    TS_ASSERT(test->size() == 2*sizeof(float)+sizeof(double)+sizeof(char)+sizeof(short)+sizeof(int)
        +sizeof(long long)+sizeof(unsigned char)+sizeof(unsigned short)+sizeof(unsigned int)
        +sizeof(unsigned long long));
    TS_ASSERT(test->cardinal() == 11);
    TS_ASSERT_DELTA(test->flt1.to_float(), -44.55f, 0.001);
    TS_ASSERT_DELTA(test->flt2.to_float(), -23.345f, 0.001);
    TS_ASSERT_DELTA(test->dbl.to_double(), -567.7777, 0.0001);
    TS_ASSERT_EQUALS(test->iv1.to_char(), -5);
    TS_ASSERT_EQUALS(test->iv2.to_short(), -300);
    TS_ASSERT_EQUALS(test->iv3.to_int(), 66);
    TS_ASSERT_EQUALS(test->iv4.to_long(), -87654);
    TS_ASSERT_EQUALS(test->iv5.to_unsigned_char(), 5);
    TS_ASSERT_EQUALS(test->iv6.to_unsigned_short(), 300);
    TS_ASSERT_EQUALS(test->iv7.to_unsigned_int(), 765);
    TS_ASSERT_EQUALS(test->iv8.to_unsigned_long(), 999);
    TS_ASSERT(test->isValid());

    delete(test);
    delete(test1);
}

void FlexComplexVarTestSuite::testFlexAdd(void)
{
    flex_var<my_complex_var1> ftest;
    my_complex_var cv;
    my_complex_var1 cv1;
    my_complex_var1 *cptr = new my_complex_var1();
    my_complex_var1 *cptr2 = new my_complex_var1();

    TS_ASSERT(!ftest.isValid());
    TS_TRACE("1. ADD my_complex_var1 to flex_var[0]\n");
    cv1.dbl1 = 333.3333;
    cv1.dbl2 = -444.4444;
    cv1.iv1 = -333;
    ftest.add(cv1);
    TS_ASSERT(ftest.cardinal() == 1);
    TS_ASSERT_DELTA(ftest[0]->dbl1.to_double(), 333.3333, 0.0001);
    TS_ASSERT_DELTA(ftest[0]->dbl2.to_double(), -444.4444, 0.0001);
    TS_ASSERT_EQUALS(ftest[0]->iv1.to_int(), -333);
    TS_ASSERT(ftest[0]->isValid());

    TS_TRACE("2. ADD my_complex_var1* to flex_var[1]\n");
    cptr->dbl1 = 0;
    cptr->dbl2 = 23.2323;
    cptr->iv1 = 100;
    ftest.add(cptr);
    TS_ASSERT(ftest.cardinal() == 2);
    TS_ASSERT_DELTA(ftest[0]->dbl1.to_double(), 333.3333, 0.0001);
    TS_ASSERT_DELTA(ftest[0]->dbl2.to_double(), -444.4444, 0.0001);
    TS_ASSERT_EQUALS(ftest[0]->iv1.to_int(), -333);
    TS_ASSERT(ftest[0]->isValid());
    TS_ASSERT_DELTA(ftest[1]->dbl1.to_double(), 0, 0.0001);
    TS_ASSERT_DELTA(ftest[1]->dbl2.to_double(), 23.2323, 0.0001);
    TS_ASSERT_EQUALS(ftest[1]->iv1.to_int(), 100);
    TS_ASSERT(ftest[1]->isValid());

    TS_TRACE("3. ADD my_complex_var1 to flex_var[2]\n");
    cv1.dbl1 = -55.5556;
    ftest.add(cv1);
    TS_ASSERT(ftest.cardinal() == 3);
    TS_ASSERT_DELTA(ftest[0]->dbl1.to_double(), 333.3333, 0.0001);
    TS_ASSERT_DELTA(ftest[0]->dbl2.to_double(), -444.4444, 0.0001);
    TS_ASSERT_EQUALS(ftest[0]->iv1.to_int(), -333);
    TS_ASSERT(ftest[0]->isValid());
    TS_ASSERT_DELTA(ftest[1]->dbl1.to_double(), 0, 0.0001);
    TS_ASSERT_DELTA(ftest[1]->dbl2.to_double(), 23.2323, 0.0001);
    TS_ASSERT_EQUALS(ftest[1]->iv1.to_int(), 100);
    TS_ASSERT(ftest[1]->isValid());
    TS_ASSERT_DELTA(ftest[2]->dbl1.to_double(), -55.5556, 0.0001);
    TS_ASSERT_DELTA(ftest[2]->dbl2.to_double(), -444.4444, 0.0001);
    TS_ASSERT_EQUALS(ftest[2]->iv1.to_int(), -333);
    TS_ASSERT(ftest[2]->isValid());

    TS_TRACE("4. ADD my_complex_var1 to flex_var[3]\n");
    cv1.iv1 = 225;
    ftest.add(cv1);
    TS_ASSERT(ftest.cardinal() == 4);
    TS_ASSERT_DELTA(ftest[0]->dbl1.to_double(), 333.3333, 0.0001);
    TS_ASSERT_DELTA(ftest[0]->dbl2.to_double(), -444.4444, 0.0001);
    TS_ASSERT_EQUALS(ftest[0]->iv1.to_int(), -333);
    TS_ASSERT(ftest[0]->isValid());
    TS_ASSERT_DELTA(ftest[1]->dbl1.to_double(), 0, 0.0001);
    TS_ASSERT_DELTA(ftest[1]->dbl2.to_double(), 23.2323, 0.0001);
    TS_ASSERT_EQUALS(ftest[1]->iv1.to_int(), 100);
    TS_ASSERT(ftest[1]->isValid());
    TS_ASSERT_DELTA(ftest[2]->dbl1.to_double(), -55.5556, 0.0001);
    TS_ASSERT_DELTA(ftest[2]->dbl2.to_double(), -444.4444, 0.0001);
    TS_ASSERT_EQUALS(ftest[2]->iv1.to_int(), -333);
    TS_ASSERT(ftest[2]->isValid());
    TS_ASSERT_DELTA(ftest[3]->dbl1.to_double(), -55.5556, 0.0001);
    TS_ASSERT_DELTA(ftest[3]->dbl2.to_double(), -444.4444, 0.0001);
    TS_ASSERT_EQUALS(ftest[3]->iv1.to_int(), 225);
    TS_ASSERT(ftest[3]->isValid());

    TS_TRACE("5. ADD my_complex_var1* to flex_var[4]\n");
    cptr2->dbl1 = -99.999;
    cptr2->dbl2 = 0.1234;
    cptr2->iv1 = -55;
    ftest.add(cptr2);
    TS_ASSERT(ftest.cardinal() == 5);
    TS_ASSERT_DELTA(ftest[0]->dbl1.to_double(), 333.3333, 0.0001);
    TS_ASSERT_DELTA(ftest[0]->dbl2.to_double(), -444.4444, 0.0001);
    TS_ASSERT_EQUALS(ftest[0]->iv1.to_int(), -333);
    TS_ASSERT(ftest[0]->isValid());
    TS_ASSERT_DELTA(ftest[1]->dbl1.to_double(), 0, 0.0001);
    TS_ASSERT_DELTA(ftest[1]->dbl2.to_double(), 23.2323, 0.0001);
    TS_ASSERT_EQUALS(ftest[1]->iv1.to_int(), 100);
    TS_ASSERT(ftest[1]->isValid());
    TS_ASSERT_DELTA(ftest[2]->dbl1.to_double(), -55.5556, 0.0001);
    TS_ASSERT_DELTA(ftest[2]->dbl2.to_double(), -444.4444, 0.0001);
    TS_ASSERT_EQUALS(ftest[2]->iv1.to_int(), -333);
    TS_ASSERT(ftest[2]->isValid());
    TS_ASSERT_DELTA(ftest[3]->dbl1.to_double(), -55.5556, 0.0001);
    TS_ASSERT_DELTA(ftest[3]->dbl2.to_double(), -444.4444, 0.0001);
    TS_ASSERT_EQUALS(ftest[3]->iv1.to_int(), 225);
    TS_ASSERT(ftest[3]->isValid());
    TS_ASSERT_DELTA(ftest[4]->dbl1.to_double(), -99.999, 0.0001);
    TS_ASSERT_DELTA(ftest[4]->dbl2.to_double(), 0.1234, 0.0001);
    TS_ASSERT_EQUALS(ftest[4]->iv1.to_int(), -55);
    TS_ASSERT(ftest[4]->isValid());
}

void FlexComplexVarTestSuite::testFlexInsert(void)
{
    flex_var<my_complex_var> ftest;
    my_complex_var cv;
    my_complex_var *cptr = new my_complex_var();
    my_complex_var *cptr2 = new my_complex_var();
    my_complex_var *cptr3 = new my_complex_var();
    my_complex_var *cptr4 = new my_complex_var();

    TS_ASSERT(!ftest.isValid());
    TS_TRACE("1. Before INSERT: Add several my_complex_var\n");
    cv.flt1 = 11.1111f;
    cv.flt2 = -22.2222f;
    cv.dbl = 333.3333;
    cv.iv1 = -1;
    cv.iv2 = -22;
    cv.iv3 = -333;
    cv.iv4 = -4444;
    cv.iv5 = 1;
    cv.iv6 = 22;
    cv.iv7 = 333;
    cv.iv8 = 44444;
    for(int i=0; i<4; ++i)
        ftest.add(cv);
    TS_ASSERT(ftest.cardinal() == 4);
    for(int i=0; i<ftest.cardinal(); ++i)
    {
        TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 11.1111f, 0.001);
        TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -22.2222f, 0.001);
        TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
        TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
        TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
        TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
        TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
        TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
        TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
        TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
        TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
        TS_ASSERT(ftest[i]->isValid());
    }

    TS_TRACE("2. INSERT my_complex_var* into index=1\n");
    cptr->flt1 = -99.99f;
    cptr->flt2 = 99.99f;
    cptr->dbl = 999.9999;
    cptr->iv1 = 0;
    cptr->iv2 = -9;
    cptr->iv3 = -88;
    cptr->iv4 = -777;
    cptr->iv5 = 0;
    cptr->iv6 = 9;
    cptr->iv7 = 88;
    cptr->iv8 = 777;
    ftest.insert(cptr, 1);
    TS_ASSERT(ftest.cardinal() == 5);
    for(int i=0; i<ftest.cardinal(); ++i)
    {
        if(i == 1)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), -99.99f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 99.99f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 999.9999, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -777);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 777);
            TS_ASSERT(ftest[i]->isValid());
        }
        else
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 11.1111f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -22.2222f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
            TS_ASSERT(ftest[i]->isValid());
        }
    }

    TS_TRACE("3. INSERT my_complex_var* into index=4\n");
    cptr2->flt1 = 0.0f;
    cptr2->flt2 = 0.0f;
    cptr2->dbl = 0.0;
    cptr2->iv1 = 0;
    cptr2->iv2 = 0;
    cptr2->iv3 = 0;
    cptr2->iv4 = 0;
    cptr2->iv5 = 10;
    cptr2->iv6 = 100;
    cptr2->iv7 = 0;
    cptr2->iv8 = 0;
    ftest.insert(cptr2, 4);
    TS_ASSERT(ftest.cardinal() == 6);
    for(int i=0; i<ftest.cardinal(); ++i)
    {
        if (i == 1)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), -99.99f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 99.99f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 999.9999, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -777);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 777);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if (i == 4)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.0f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 0.0f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 0.0, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 10);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 100);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 0);
            TS_ASSERT(ftest[i]->isValid());
        }
        else
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 11.1111f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -22.2222f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
            TS_ASSERT(ftest[i]->isValid());
        }
    }

    TS_TRACE("4. INSERT my_complex_var* into index=8 which is ftest.cardinal()+2\n");
    cptr3->flt1 = 0.0f;
    cptr3->flt2 = 0.0f;
    cptr3->dbl = 0.0;
    cptr3->iv1 = 0;
    cptr3->iv2 = 0;
    cptr3->iv3 = 0;
    cptr3->iv4 = 999;
    cptr3->iv5 = 0;
    cptr3->iv6 = 0;
    cptr3->iv7 = 0;
    cptr3->iv8 = 999;
    ftest.insert(cptr3, 8);
    TS_ASSERT(ftest.cardinal() == 7);
    for(int i=0; i<ftest.cardinal(); ++i)
    {
        if (i == 1)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), -99.99f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 99.99f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 999.9999, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -777);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 777);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if (i == 4)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.0f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 0.0f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 0.0, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 10);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 100);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 0);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if(i == ftest.cardinal() - 1)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.0f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 0.0f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 0.0, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), 999);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 999);
            TS_ASSERT(ftest[i]->isValid());
        }
        else
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 11.1111f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -22.2222f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
            TS_ASSERT(ftest[i]->isValid());
        }
    }

    TS_TRACE("5. INSERT my_complex_var* into invalid index=-1\n");
    cptr4->flt1 = 0.0f;
    cptr4->flt2 = 0.0f;
    cptr4->dbl = 0.0;
    cptr4->iv1 = 0;
    cptr4->iv2 = 0;
    cptr4->iv3 = 0;
    cptr4->iv4 = 999;
    cptr4->iv5 = 0;
    cptr4->iv6 = 0;
    cptr4->iv7 = 0;
    cptr4->iv8 = 999;
    ftest.insert(cptr4, -1);
    TS_ASSERT(ftest.cardinal() == 8);
    for(int i=0; i<ftest.cardinal(); ++i)
    {
        if (i == 2)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), -99.99f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 99.99f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 999.9999, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -777);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 777);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if (i == 5)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.0f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 0.0f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 0.0, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 10);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 100);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 0);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if ( (i == ftest.cardinal() - 1) || (i == 0) )
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.0f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 0.0f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 0.0, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), 999);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 999);
            TS_ASSERT(ftest[i]->isValid());
        }
        else
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 11.1111f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -22.2222f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
            TS_ASSERT(ftest[i]->isValid());
        }
    }

    TS_TRACE("6. INSERT my_complex_var into index=2\n");
    cv.flt1 = 0.0;
    cv.flt2 = 0.0;
    ftest.insert(2, cv);
    TS_ASSERT(ftest.cardinal() == 9);
    for(int i=0; i<ftest.cardinal(); ++i)
    {
        if (i == 3)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), -99.99f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 99.99f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 999.9999, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -777);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 777);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if (i == 6)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.0f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 0.0f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 0.0, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 10);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 100);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 0);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if ( (i == ftest.cardinal() - 1) || (i == 0) )
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.0f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 0.0f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 0.0, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), 999);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 999);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if (i == 2)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.0f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 0.0f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
            TS_ASSERT(ftest[i]->isValid());
        }
        else
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 11.1111f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -22.2222f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
            TS_ASSERT(ftest[i]->isValid());
        }
    }

    TS_TRACE("7. INSERT my_complex_var again into index=0\n");
    cv.flt1 = 0.0;
    cv.flt2 = 0.0;
    ftest.insert(0, cv);
    TS_ASSERT(ftest.cardinal() == 10);
    for(int i=0; i<ftest.cardinal(); ++i)
    {
        if (i == 4)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), -99.99f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 99.99f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 999.9999, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -777);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 777);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if (i == 7)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.0f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 0.0f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 0.0, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 10);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 100);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 0);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if ( (i == ftest.cardinal() - 1) || (i == 1) )
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.0f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 0.0f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 0.0, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), 999);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 999);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if ( (i == 3) || (i == 0) )
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.0f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 0.0f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
            TS_ASSERT(ftest[i]->isValid());
        }
        else
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 11.1111f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -22.2222f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
            TS_ASSERT(ftest[i]->isValid());
        }
    }

    TS_TRACE("8. INSERT my_complex_var again into index=11\n");
    cv.flt1 = 0.0;
    cv.flt2 = 0.0;
    ftest.insert(11, cv);
    TS_ASSERT(ftest.cardinal() == 11);
    for(int i=0; i<ftest.cardinal(); ++i)
    {
        if (i == 4)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), -99.99f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 99.99f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 999.9999, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -777);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 777);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if (i == 7)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.0f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 0.0f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 0.0, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 10);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 100);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 0);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if ( (i == ftest.cardinal() - 2) || (i == 1) )
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.0f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 0.0f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 0.0, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), 999);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 999);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if ( (i == 3) || (i == 0) || (i == ftest.cardinal() - 1) )
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.0f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 0.0f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
            TS_ASSERT(ftest[i]->isValid());
        }
        else
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 11.1111f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -22.2222f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
            TS_ASSERT(ftest[i]->isValid());
        }
    }

    TS_TRACE("9. INSERT my_complex_var again into index=-1\n");
    cv.flt1 = 0.0;
    cv.flt2 = 0.0;
    ftest.insert(-1, cv);
    TS_ASSERT(ftest.cardinal() == 12);
    for(int i=0; i<ftest.cardinal(); ++i)
    {
        if (i == 5)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), -99.99f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 99.99f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 999.9999, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -777);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 777);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if (i == 8)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.0f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 0.0f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 0.0, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 10);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 100);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 0);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if ( (i == ftest.cardinal() - 2) || (i == 2) )
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.0f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 0.0f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 0.0, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), 999);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 999);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if ( (i == 0) || (i == 1) || (i == 4) || (i == ftest.cardinal() - 1) )
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.0f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), 0.0f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
            TS_ASSERT(ftest[i]->isValid());
        }
        else
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 11.1111f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -22.2222f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
            TS_ASSERT(ftest[i]->isValid());
        }
    }
}

void FlexComplexVarTestSuite::testFlexRemove(void)
{
    flex_var<my_complex_var> ftest;
    my_complex_var cv1;
    my_complex_var cv2;
    my_complex_var *cptr1 = new my_complex_var();
    my_complex_var *cptr2 = new my_complex_var();
    my_complex_var *cptr3 = new my_complex_var();

    TS_ASSERT(!ftest.isValid());
    TS_TRACE("1. Before REMOVE: Add several my_complex_var\n");
    cv1.flt1 = 11.1111f;
    cv1.flt2 = -22.2222f;
    cv1.dbl = 333.3333;
    cv1.iv1 = -1;
    cv1.iv2 = -22;
    cv1.iv3 = -333;
    cv1.iv4 = -4444;
    cv1.iv5 = 1;
    cv1.iv6 = 22;
    cv1.iv7 = 333;
    cv1.iv8 = 44444;
    cv2.flt1 = 0.1234f;
    cv2.flt2 = -9.876f;
    cv2.dbl = 123.4567;
    cv2.iv1 = -9;
    cv2.iv2 = -88;
    cv2.iv3 = -777;
    cv2.iv4 = -6666;
    cv2.iv5 = 9;
    cv2.iv6 = 88;
    cv2.iv7 = 777;
    cv2.iv8 = 6666;
    cptr1->flt1 = 0.9999f;
    cptr1->flt2 = -0.9999f;
    cptr1->dbl = 99.9999;
    cptr1->iv1 = 0;
    cptr1->iv2 = -9;
    cptr1->iv3 = -99;
    cptr1->iv4 = -999;
    cptr1->iv5 = 0;
    cptr1->iv6 = 9;
    cptr1->iv7 = 99;
    cptr1->iv8 = 999;
    cptr2->flt1 = 0.8888f;
    cptr2->flt2 = -0.8888f;
    cptr2->dbl = 88.8888;
    cptr2->iv1 = 0;
    cptr2->iv2 = -8;
    cptr2->iv3 = -88;
    cptr2->iv4 = -888;
    cptr2->iv5 = 0;
    cptr2->iv6 = 8;
    cptr2->iv7 = 88;
    cptr2->iv8 = 888;
    ftest.add(cv1);
    ftest.add(cv2);
    ftest.add(cptr1);
    for(int i=0; i<2; ++i)
    {
        ftest.add(cv1);
        ftest.add(cv2);
    }
    ftest.add(cptr2);
    ftest.add(cv1);
    TS_ASSERT(ftest.cardinal() == 9);
    for(int i=0; i<ftest.cardinal(); ++i)
    {
        if( (i == 0) || (i == 3) || (i == 5) || (i == 8) )
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 11.1111f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -22.2222f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if( (i == 1) || (i == 4) || (i == 6) )
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.1234f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -9.876f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 123.4567, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -777);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -6666);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 777);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 6666);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if(i == 2)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.9999f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -0.9999f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 99.9999, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -99);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -999);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 99);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 999);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if(i == 7)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.8888f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -0.8888f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 88.8888, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -8);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -888);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 8);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 888);
            TS_ASSERT(ftest[i]->isValid());
        }
    }

    TS_TRACE("2. REMOVE my_complex_var from index=5\n");
    ftest.remove(5);
    TS_ASSERT(ftest.cardinal() == 8);
    for(int i=0; i<ftest.cardinal(); ++i)
    {
        if( (i == 0) || (i == 3) || (i == 7) )
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 11.1111f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -22.2222f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if( (i == 1) || (i == 4) || (i == 5) )
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.1234f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -9.876f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 123.4567, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -777);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -6666);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 777);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 6666);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if(i == 2)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.9999f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -0.9999f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 99.9999, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -99);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -999);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 99);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 999);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if(i == 6)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.8888f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -0.8888f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 88.8888, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -8);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -888);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 8);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 888);
            TS_ASSERT(ftest[i]->isValid());
        }
    }

    TS_TRACE("3. REMOVE my_complex_var from index=0\n");
    ftest.remove(0);
    TS_ASSERT(ftest.cardinal() == 7);
    for(int i=0; i<ftest.cardinal(); ++i)
    {
        if( (i == 2) || (i == 6) )
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 11.1111f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -22.2222f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if( (i == 0) || (i == 3) || (i == 4) )
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.1234f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -9.876f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 123.4567, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -777);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -6666);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 777);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 6666);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if(i == 1)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.9999f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -0.9999f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 99.9999, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -99);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -999);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 99);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 999);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if(i == 5)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.8888f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -0.8888f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 88.8888, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -8);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -888);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 8);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 888);
            TS_ASSERT(ftest[i]->isValid());
        }
    }

    TS_TRACE("4. REMOVE my_complex_var from index=2\n");
    ftest.remove(2);
    TS_ASSERT(ftest.cardinal() == 6);
    for(int i=0; i<ftest.cardinal(); ++i)
    {
        if(i == 5)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 11.1111f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -22.2222f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if( (i == 0) || (i == 2) || (i == 3) )
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.1234f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -9.876f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 123.4567, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -777);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -6666);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 777);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 6666);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if(i == 1)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.9999f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -0.9999f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 99.9999, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -99);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -999);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 99);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 999);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if(i == 4)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.8888f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -0.8888f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 88.8888, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -8);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -888);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 8);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 888);
            TS_ASSERT(ftest[i]->isValid());
        }
    }

    TS_TRACE("5. REMOVE my_complex_var from index=2 again\n");
    ftest.remove(2);
    TS_ASSERT(ftest.cardinal() == 5);
    for(int i=0; i<ftest.cardinal(); ++i)
    {
        if(i == 4)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 11.1111f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -22.2222f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if( (i == 0) || (i == 2) )
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.1234f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -9.876f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 123.4567, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -777);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -6666);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 777);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 6666);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if(i == 1)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.9999f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -0.9999f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 99.9999, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -99);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -999);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 99);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 999);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if(i == 3)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.8888f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -0.8888f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 88.8888, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -8);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -888);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 8);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 888);
            TS_ASSERT(ftest[i]->isValid());
        }
    }

    TS_TRACE("6. REMOVE my_complex_var from invalid index=5\n");
    ftest.remove(5);
    TS_ASSERT(ftest.cardinal() == 5);
    for(int i=0; i<ftest.cardinal(); ++i)
    {
        if(i == 4)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 11.1111f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -22.2222f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if( (i == 0) || (i == 2) )
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.1234f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -9.876f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 123.4567, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -777);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -6666);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 777);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 6666);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if(i == 1)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.9999f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -0.9999f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 99.9999, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -99);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -999);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 99);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 999);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if(i == 3)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.8888f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -0.8888f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 88.8888, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -8);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -888);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 8);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 888);
            TS_ASSERT(ftest[i]->isValid());
        }
    }

    TS_TRACE("7. REMOVE my_complex_var from invalid index=-1\n");
    ftest.remove(-1);
    TS_ASSERT(ftest.cardinal() == 5);
    for(int i=0; i<ftest.cardinal(); ++i)
    {
        if(i == 4)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 11.1111f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -22.2222f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if( (i == 0) || (i == 2) )
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.1234f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -9.876f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 123.4567, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -777);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -6666);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 777);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 6666);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if(i == 1)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.9999f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -0.9999f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 99.9999, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -99);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -999);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 99);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 999);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if(i == 3)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.8888f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -0.8888f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 88.8888, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -8);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -888);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 8);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 888);
            TS_ASSERT(ftest[i]->isValid());
        }
    }

    TS_TRACE("8. REMOVE my_complex_var* cptr1\n");
    ftest.remove(cptr1);
    TS_ASSERT(ftest.cardinal() == 4);
    for(int i=0; i<ftest.cardinal(); ++i)
    {
        if(i == 3)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 11.1111f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -22.2222f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if( (i == 0) || (i == 1) )
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.1234f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -9.876f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 123.4567, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -777);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -6666);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 777);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 6666);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if(i == 2)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.8888f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -0.8888f, 0.0001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 88.8888, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -8);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -888);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 0);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 8);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 888);
            TS_ASSERT(ftest[i]->isValid());
        }
    }

    TS_TRACE("9. REMOVE my_complex_var* cptr2\n");
    ftest.remove(cptr2);
    TS_ASSERT(ftest.cardinal() == 3);
    for(int i=0; i<ftest.cardinal(); ++i)
    {
        if(i == 2)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 11.1111f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -22.2222f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if( (i == 0) || (i == 1) )
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.1234f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -9.876f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 123.4567, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -777);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -6666);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 777);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 6666);
            TS_ASSERT(ftest[i]->isValid());
        }
    }

    TS_TRACE("10. REMOVE invalid my_complex_var* cptr3\n");
    ftest.remove(cptr3);
    TS_ASSERT(ftest.cardinal() == 3);
    for(int i=0; i<ftest.cardinal(); ++i)
    {
        if(i == 2)
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 11.1111f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -22.2222f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
            TS_ASSERT(ftest[i]->isValid());
        }
        else if( (i == 0) || (i == 1) )
        {
            TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 0.1234f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -9.876f, 0.001);
            TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 123.4567, 0.0001);
            TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -9);
            TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -88);
            TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -777);
            TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -6666);
            TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 9);
            TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 88);
            TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 777);
            TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 6666);
            TS_ASSERT(ftest[i]->isValid());
        }
    }
}

void FlexComplexVarTestSuite::testFlexResize(void)
{
    flex_var<my_complex_var> ftest;
    my_complex_var cv1;
    my_complex_var cv2;
    my_complex_var *cptr = new my_complex_var();

    TS_ASSERT(!ftest.isValid());
    TS_TRACE("1. Before RESIZE: Add several my_complex_var\n");
    cv1.flt1 = 11.1111f;
    cv1.flt2 = -22.2222f;
    cv1.dbl = 333.3333;
    cv1.iv1 = -1;
    cv1.iv2 = -22;
    cv1.iv3 = -333;
    cv1.iv4 = -4444;
    cv1.iv5 = 1;
    cv1.iv6 = 22;
    cv1.iv7 = 333;
    cv1.iv8 = 44444;
    cv2.flt1 = 0.1234f;
    cv2.flt2 = -9.876f;
    cv2.dbl = 123.4567;
    cv2.iv1 = -9;
    cv2.iv2 = -88;
    cv2.iv3 = -777;
    cv2.iv4 = -6666;
    cv2.iv5 = 9;
    cv2.iv6 = 88;
    cv2.iv7 = 777;
    cv2.iv8 = 6666;
    for(int i=0; i<4; ++i)
    {
        ftest.add(cv1);
        ftest.add(cv2);
    }
    TS_ASSERT(ftest.cardinal() == 8);
    for(int i = 0; i < ftest.cardinal(); i=i+2)
    {
        TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 11.1111f, 0.001);
        TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -22.2222f, 0.001);
        TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
        TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
        TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
        TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
        TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
        TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
        TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
        TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
        TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
        TS_ASSERT(ftest[i]->isValid());
        TS_ASSERT_DELTA(ftest[i+1]->flt1.to_float(), 0.1234f, 0.001);
        TS_ASSERT_DELTA(ftest[i+1]->flt2.to_float(), -9.876f, 0.001);
        TS_ASSERT_DELTA(ftest[i+1]->dbl.to_double(), 123.4567, 0.0001);
        TS_ASSERT_EQUALS(ftest[i+1]->iv1.to_char(), -9);
        TS_ASSERT_EQUALS(ftest[i+1]->iv2.to_short(), -88);
        TS_ASSERT_EQUALS(ftest[i+1]->iv3.to_int(), -777);
        TS_ASSERT_EQUALS(ftest[i+1]->iv4.to_long(), -6666);
        TS_ASSERT_EQUALS(ftest[i+1]->iv5.to_unsigned_char(), 9);
        TS_ASSERT_EQUALS(ftest[i+1]->iv6.to_unsigned_short(), 88);
        TS_ASSERT_EQUALS(ftest[i+1]->iv7.to_unsigned_int(), 777);
        TS_ASSERT_EQUALS(ftest[i+1]->iv8.to_unsigned_long(), 6666);
        TS_ASSERT(ftest[i+1]->isValid());
    }

    TS_TRACE("2. RESIZE to size=3\n");
    ftest.resize(3);
    TS_ASSERT(ftest.cardinal() == 3);
    TS_ASSERT_DELTA(ftest[0]->flt1.to_float(), 11.1111f, 0.001);
    TS_ASSERT_DELTA(ftest[0]->flt2.to_float(), -22.2222f, 0.001);
    TS_ASSERT_DELTA(ftest[0]->dbl.to_double(), 333.3333, 0.0001);
    TS_ASSERT_EQUALS(ftest[0]->iv1.to_char(), -1);
    TS_ASSERT_EQUALS(ftest[0]->iv2.to_short(), -22);
    TS_ASSERT_EQUALS(ftest[0]->iv3.to_int(), -333);
    TS_ASSERT_EQUALS(ftest[0]->iv4.to_long(), -4444);
    TS_ASSERT_EQUALS(ftest[0]->iv5.to_unsigned_char(), 1);
    TS_ASSERT_EQUALS(ftest[0]->iv6.to_unsigned_short(), 22);
    TS_ASSERT_EQUALS(ftest[0]->iv7.to_unsigned_int(), 333);
    TS_ASSERT_EQUALS(ftest[0]->iv8.to_unsigned_long(), 44444);
    TS_ASSERT(ftest[0]->isValid());
    TS_ASSERT_DELTA(ftest[1]->flt1.to_float(), 0.1234f, 0.001);
    TS_ASSERT_DELTA(ftest[1]->flt2.to_float(), -9.876f, 0.001);
    TS_ASSERT_DELTA(ftest[1]->dbl.to_double(), 123.4567, 0.0001);
    TS_ASSERT_EQUALS(ftest[1]->iv1.to_char(), -9);
    TS_ASSERT_EQUALS(ftest[1]->iv2.to_short(), -88);
    TS_ASSERT_EQUALS(ftest[1]->iv3.to_int(), -777);
    TS_ASSERT_EQUALS(ftest[1]->iv4.to_long(), -6666);
    TS_ASSERT_EQUALS(ftest[1]->iv5.to_unsigned_char(), 9);
    TS_ASSERT_EQUALS(ftest[1]->iv6.to_unsigned_short(), 88);
    TS_ASSERT_EQUALS(ftest[1]->iv7.to_unsigned_int(), 777);
    TS_ASSERT_EQUALS(ftest[1]->iv8.to_unsigned_long(), 6666);
    TS_ASSERT(ftest[1]->isValid());
    TS_ASSERT_DELTA(ftest[2]->flt1.to_float(), 11.1111f, 0.0001);
    TS_ASSERT_DELTA(ftest[2]->flt2.to_float(), -22.2222f, 0.0001);
    TS_ASSERT_DELTA(ftest[2]->dbl.to_double(), 333.3333, 0.0001);
    TS_ASSERT_EQUALS(ftest[2]->iv1.to_char(), -1);
    TS_ASSERT_EQUALS(ftest[2]->iv2.to_short(), -22);
    TS_ASSERT_EQUALS(ftest[2]->iv3.to_int(), -333);
    TS_ASSERT_EQUALS(ftest[2]->iv4.to_long(), -4444);
    TS_ASSERT_EQUALS(ftest[2]->iv5.to_unsigned_char(), 1);
    TS_ASSERT_EQUALS(ftest[2]->iv6.to_unsigned_short(), 22);
    TS_ASSERT_EQUALS(ftest[2]->iv7.to_unsigned_int(), 333);
    TS_ASSERT_EQUALS(ftest[2]->iv8.to_unsigned_long(), 44444);
    TS_ASSERT(ftest[2]->isValid());

    TS_TRACE("3. RESIZE to size=3 again\n");
    ftest.resize(3);
    TS_ASSERT(ftest.cardinal() == 3);
    TS_ASSERT_DELTA(ftest[0]->flt1.to_float(), 11.1111f, 0.001);
    TS_ASSERT_DELTA(ftest[0]->flt2.to_float(), -22.2222f, 0.001);
    TS_ASSERT_DELTA(ftest[0]->dbl.to_double(), 333.3333, 0.0001);
    TS_ASSERT_EQUALS(ftest[0]->iv1.to_char(), -1);
    TS_ASSERT_EQUALS(ftest[0]->iv2.to_short(), -22);
    TS_ASSERT_EQUALS(ftest[0]->iv3.to_int(), -333);
    TS_ASSERT_EQUALS(ftest[0]->iv4.to_long(), -4444);
    TS_ASSERT_EQUALS(ftest[0]->iv5.to_unsigned_char(), 1);
    TS_ASSERT_EQUALS(ftest[0]->iv6.to_unsigned_short(), 22);
    TS_ASSERT_EQUALS(ftest[0]->iv7.to_unsigned_int(), 333);
    TS_ASSERT_EQUALS(ftest[0]->iv8.to_unsigned_long(), 44444);
    TS_ASSERT(ftest[0]->isValid());
    TS_ASSERT_DELTA(ftest[1]->flt1.to_float(), 0.1234f, 0.001);
    TS_ASSERT_DELTA(ftest[1]->flt2.to_float(), -9.876f, 0.001);
    TS_ASSERT_DELTA(ftest[1]->dbl.to_double(), 123.4567, 0.0001);
    TS_ASSERT_EQUALS(ftest[1]->iv1.to_char(), -9);
    TS_ASSERT_EQUALS(ftest[1]->iv2.to_short(), -88);
    TS_ASSERT_EQUALS(ftest[1]->iv3.to_int(), -777);
    TS_ASSERT_EQUALS(ftest[1]->iv4.to_long(), -6666);
    TS_ASSERT_EQUALS(ftest[1]->iv5.to_unsigned_char(), 9);
    TS_ASSERT_EQUALS(ftest[1]->iv6.to_unsigned_short(), 88);
    TS_ASSERT_EQUALS(ftest[1]->iv7.to_unsigned_int(), 777);
    TS_ASSERT_EQUALS(ftest[1]->iv8.to_unsigned_long(), 6666);
    TS_ASSERT(ftest[1]->isValid());
    TS_ASSERT_DELTA(ftest[2]->flt1.to_float(), 11.1111f, 0.0001);
    TS_ASSERT_DELTA(ftest[2]->flt2.to_float(), -22.2222f, 0.0001);
    TS_ASSERT_DELTA(ftest[2]->dbl.to_double(), 333.3333, 0.0001);
    TS_ASSERT_EQUALS(ftest[2]->iv1.to_char(), -1);
    TS_ASSERT_EQUALS(ftest[2]->iv2.to_short(), -22);
    TS_ASSERT_EQUALS(ftest[2]->iv3.to_int(), -333);
    TS_ASSERT_EQUALS(ftest[2]->iv4.to_long(), -4444);
    TS_ASSERT_EQUALS(ftest[2]->iv5.to_unsigned_char(), 1);
    TS_ASSERT_EQUALS(ftest[2]->iv6.to_unsigned_short(), 22);
    TS_ASSERT_EQUALS(ftest[2]->iv7.to_unsigned_int(), 333);
    TS_ASSERT_EQUALS(ftest[2]->iv8.to_unsigned_long(), 44444);
    TS_ASSERT(ftest[2]->isValid());

    TS_TRACE("4. RESIZE to size from 3 to 5\n");
    ftest.resize(5);
    TS_ASSERT(ftest.cardinal() == 5);
    TS_ASSERT_DELTA(ftest[0]->flt1.to_float(), 11.1111f, 0.001);
    TS_ASSERT_DELTA(ftest[0]->flt2.to_float(), -22.2222f, 0.001);
    TS_ASSERT_DELTA(ftest[0]->dbl.to_double(), 333.3333, 0.0001);
    TS_ASSERT_EQUALS(ftest[0]->iv1.to_char(), -1);
    TS_ASSERT_EQUALS(ftest[0]->iv2.to_short(), -22);
    TS_ASSERT_EQUALS(ftest[0]->iv3.to_int(), -333);
    TS_ASSERT_EQUALS(ftest[0]->iv4.to_long(), -4444);
    TS_ASSERT_EQUALS(ftest[0]->iv5.to_unsigned_char(), 1);
    TS_ASSERT_EQUALS(ftest[0]->iv6.to_unsigned_short(), 22);
    TS_ASSERT_EQUALS(ftest[0]->iv7.to_unsigned_int(), 333);
    TS_ASSERT_EQUALS(ftest[0]->iv8.to_unsigned_long(), 44444);
    TS_ASSERT(ftest[0]->isValid());
    TS_ASSERT_DELTA(ftest[1]->flt1.to_float(), 0.1234f, 0.001);
    TS_ASSERT_DELTA(ftest[1]->flt2.to_float(), -9.876f, 0.001);
    TS_ASSERT_DELTA(ftest[1]->dbl.to_double(), 123.4567, 0.0001);
    TS_ASSERT_EQUALS(ftest[1]->iv1.to_char(), -9);
    TS_ASSERT_EQUALS(ftest[1]->iv2.to_short(), -88);
    TS_ASSERT_EQUALS(ftest[1]->iv3.to_int(), -777);
    TS_ASSERT_EQUALS(ftest[1]->iv4.to_long(), -6666);
    TS_ASSERT_EQUALS(ftest[1]->iv5.to_unsigned_char(), 9);
    TS_ASSERT_EQUALS(ftest[1]->iv6.to_unsigned_short(), 88);
    TS_ASSERT_EQUALS(ftest[1]->iv7.to_unsigned_int(), 777);
    TS_ASSERT_EQUALS(ftest[1]->iv8.to_unsigned_long(), 6666);
    TS_ASSERT(ftest[1]->isValid());
    TS_ASSERT_DELTA(ftest[2]->flt1.to_float(), 11.1111f, 0.0001);
    TS_ASSERT_DELTA(ftest[2]->flt2.to_float(), -22.2222f, 0.0001);
    TS_ASSERT_DELTA(ftest[2]->dbl.to_double(), 333.3333, 0.0001);
    TS_ASSERT_EQUALS(ftest[2]->iv1.to_char(), -1);
    TS_ASSERT_EQUALS(ftest[2]->iv2.to_short(), -22);
    TS_ASSERT_EQUALS(ftest[2]->iv3.to_int(), -333);
    TS_ASSERT_EQUALS(ftest[2]->iv4.to_long(), -4444);
    TS_ASSERT_EQUALS(ftest[2]->iv5.to_unsigned_char(), 1);
    TS_ASSERT_EQUALS(ftest[2]->iv6.to_unsigned_short(), 22);
    TS_ASSERT_EQUALS(ftest[2]->iv7.to_unsigned_int(), 333);
    TS_ASSERT_EQUALS(ftest[2]->iv8.to_unsigned_long(), 44444);
    TS_ASSERT(ftest[2]->isValid());
    TS_ASSERT(!ftest[3]->isValid());
    TS_ASSERT(!ftest[4]->isValid());

    TS_TRACE("5. RESIZE to size from 5 to 7\n");
    ftest.resize(7);
    TS_ASSERT(ftest.cardinal() == 7);
    TS_ASSERT_DELTA(ftest[0]->flt1.to_float(), 11.1111f, 0.001);
    TS_ASSERT_DELTA(ftest[0]->flt2.to_float(), -22.2222f, 0.001);
    TS_ASSERT_DELTA(ftest[0]->dbl.to_double(), 333.3333, 0.0001);
    TS_ASSERT_EQUALS(ftest[0]->iv1.to_char(), -1);
    TS_ASSERT_EQUALS(ftest[0]->iv2.to_short(), -22);
    TS_ASSERT_EQUALS(ftest[0]->iv3.to_int(), -333);
    TS_ASSERT_EQUALS(ftest[0]->iv4.to_long(), -4444);
    TS_ASSERT_EQUALS(ftest[0]->iv5.to_unsigned_char(), 1);
    TS_ASSERT_EQUALS(ftest[0]->iv6.to_unsigned_short(), 22);
    TS_ASSERT_EQUALS(ftest[0]->iv7.to_unsigned_int(), 333);
    TS_ASSERT_EQUALS(ftest[0]->iv8.to_unsigned_long(), 44444);
    TS_ASSERT(ftest[0]->isValid());
    TS_ASSERT_DELTA(ftest[1]->flt1.to_float(), 0.1234f, 0.001);
    TS_ASSERT_DELTA(ftest[1]->flt2.to_float(), -9.876f, 0.001);
    TS_ASSERT_DELTA(ftest[1]->dbl.to_double(), 123.4567, 0.0001);
    TS_ASSERT_EQUALS(ftest[1]->iv1.to_char(), -9);
    TS_ASSERT_EQUALS(ftest[1]->iv2.to_short(), -88);
    TS_ASSERT_EQUALS(ftest[1]->iv3.to_int(), -777);
    TS_ASSERT_EQUALS(ftest[1]->iv4.to_long(), -6666);
    TS_ASSERT_EQUALS(ftest[1]->iv5.to_unsigned_char(), 9);
    TS_ASSERT_EQUALS(ftest[1]->iv6.to_unsigned_short(), 88);
    TS_ASSERT_EQUALS(ftest[1]->iv7.to_unsigned_int(), 777);
    TS_ASSERT_EQUALS(ftest[1]->iv8.to_unsigned_long(), 6666);
    TS_ASSERT(ftest[1]->isValid());
    TS_ASSERT_DELTA(ftest[2]->flt1.to_float(), 11.1111f, 0.0001);
    TS_ASSERT_DELTA(ftest[2]->flt2.to_float(), -22.2222f, 0.0001);
    TS_ASSERT_DELTA(ftest[2]->dbl.to_double(), 333.3333, 0.0001);
    TS_ASSERT_EQUALS(ftest[2]->iv1.to_char(), -1);
    TS_ASSERT_EQUALS(ftest[2]->iv2.to_short(), -22);
    TS_ASSERT_EQUALS(ftest[2]->iv3.to_int(), -333);
    TS_ASSERT_EQUALS(ftest[2]->iv4.to_long(), -4444);
    TS_ASSERT_EQUALS(ftest[2]->iv5.to_unsigned_char(), 1);
    TS_ASSERT_EQUALS(ftest[2]->iv6.to_unsigned_short(), 22);
    TS_ASSERT_EQUALS(ftest[2]->iv7.to_unsigned_int(), 333);
    TS_ASSERT_EQUALS(ftest[2]->iv8.to_unsigned_long(), 44444);
    TS_ASSERT(ftest[2]->isValid());
    TS_ASSERT(!ftest[3]->isValid());
    TS_ASSERT(!ftest[4]->isValid());
    TS_ASSERT(!ftest[5]->isValid());
    TS_ASSERT(!ftest[6]->isValid());

    TS_TRACE("6. RESIZE to size=0\n");
    ftest.resize(0);
    TS_ASSERT(ftest.cardinal() == 0);
    TS_ASSERT(!ftest.isValid());

    TS_TRACE("7. Add and RESIZE to invalid size=-1\n");
    for(int i=0; i<4; ++i)
    {
        ftest.add(cv1);
        ftest.add(cv2);
    }
    TS_ASSERT(ftest.cardinal() == 8);
    for(int i = 0; i < ftest.cardinal(); i=i+2)
    {
        TS_ASSERT_DELTA(ftest[i]->flt1.to_float(), 11.1111f, 0.001);
        TS_ASSERT_DELTA(ftest[i]->flt2.to_float(), -22.2222f, 0.001);
        TS_ASSERT_DELTA(ftest[i]->dbl.to_double(), 333.3333, 0.0001);
        TS_ASSERT_EQUALS(ftest[i]->iv1.to_char(), -1);
        TS_ASSERT_EQUALS(ftest[i]->iv2.to_short(), -22);
        TS_ASSERT_EQUALS(ftest[i]->iv3.to_int(), -333);
        TS_ASSERT_EQUALS(ftest[i]->iv4.to_long(), -4444);
        TS_ASSERT_EQUALS(ftest[i]->iv5.to_unsigned_char(), 1);
        TS_ASSERT_EQUALS(ftest[i]->iv6.to_unsigned_short(), 22);
        TS_ASSERT_EQUALS(ftest[i]->iv7.to_unsigned_int(), 333);
        TS_ASSERT_EQUALS(ftest[i]->iv8.to_unsigned_long(), 44444);
        TS_ASSERT(ftest[i]->isValid());
        TS_ASSERT_DELTA(ftest[i+1]->flt1.to_float(), 0.1234f, 0.001);
        TS_ASSERT_DELTA(ftest[i+1]->flt2.to_float(), -9.876f, 0.001);
        TS_ASSERT_DELTA(ftest[i+1]->dbl.to_double(), 123.4567, 0.0001);
        TS_ASSERT_EQUALS(ftest[i+1]->iv1.to_char(), -9);
        TS_ASSERT_EQUALS(ftest[i+1]->iv2.to_short(), -88);
        TS_ASSERT_EQUALS(ftest[i+1]->iv3.to_int(), -777);
        TS_ASSERT_EQUALS(ftest[i+1]->iv4.to_long(), -6666);
        TS_ASSERT_EQUALS(ftest[i+1]->iv5.to_unsigned_char(), 9);
        TS_ASSERT_EQUALS(ftest[i+1]->iv6.to_unsigned_short(), 88);
        TS_ASSERT_EQUALS(ftest[i+1]->iv7.to_unsigned_int(), 777);
        TS_ASSERT_EQUALS(ftest[i+1]->iv8.to_unsigned_long(), 6666);
        TS_ASSERT(ftest[i+1]->isValid());
    }
    ftest.resize(-1);
    TS_ASSERT(ftest.cardinal() == 0);
    TS_ASSERT(!ftest.isValid());
}

void FlexComplexVarTestSuite::testFlexExtract(void)
{
    flex_var<my_complex_var1> ftest;
    flex_var<my_complex_var2> ftest2;
    int count = 0;
    int size = 0;
    double dbl1, dbl2;
    int iv1, iv2;
    unsigned long iv3;
#define INDEX 2
    char buf[INDEX * (2*sizeof(double) + sizeof(int)) + sizeof(int)];
    char buf2[(INDEX+2) * (2*sizeof(double) + 2*sizeof(int) + sizeof(unsigned long)) + sizeof(int)];
    dirty_checker checker1(&ftest);
    dirty_checker checker2(&ftest2);

    TS_ASSERT(!ftest.isValid());
    memset(buf+sizeof(int), 0, sizeof(buf));
    TS_TRACE("1. Test basic extract from a buffer\n");
    count = 2;
    memcpy(buf, &count, sizeof(int));
    size = sizeof(int);
    for(int i = 0; i<INDEX; ++i)
    {
        dbl1 = 23.1234;
        memcpy(buf+size, &dbl1, sizeof(double));
        size += sizeof(double);
        dbl2 = -34.1234;
        memcpy(buf+size, &dbl2, sizeof(double));
        size += sizeof(double);
        iv1 = 123;
        memcpy(buf+size, &iv1, sizeof(int));
        size += sizeof(int);
    }
    ftest.extract(sizeof(buf), reinterpret_cast<const unsigned char *>(buf));
    TS_ASSERT_EQUALS(ftest.size(), 44);
    TS_ASSERT_EQUALS(ftest.cardinal(), 2);
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    for(int i = 0; i < ftest.cardinal(); ++i)
    {
        TS_ASSERT(ftest[i]->isValid());
        TS_ASSERT_DELTA(ftest[i]->dbl1(), 23.1234, 0.0001);
        TS_ASSERT_DELTA(ftest[i]->dbl2(), -34.1234, 0.0001);
        TS_ASSERT_EQUALS(ftest[i]->iv1(), 123);
    }
    ftest.extract(sizeof(buf), reinterpret_cast<const unsigned char *>(buf));
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 2);

    ftest.setInvalid();
    TS_ASSERT(!ftest.isValid());
    memset(buf+sizeof(int), 0, sizeof(buf));
    TS_TRACE("2. Extract again - same cardinal\n");
    size = sizeof(int);
    for(int i = 0; i<INDEX; ++i)
    {
        dbl1 = -8901.4;
        memcpy(buf+size, &dbl1, sizeof(double));
        size += sizeof(double);
        dbl2 = 98765.43;
        memcpy(buf+size, &dbl2, sizeof(double));
        size += sizeof(double);
        iv1 = 9876;
        memcpy(buf+size, &iv1, sizeof(int));
        size += sizeof(int);
    }
    ftest.extract(sizeof(buf), reinterpret_cast<const unsigned char *>(buf));
    TS_ASSERT_EQUALS(ftest.size(), 44);
    TS_ASSERT_EQUALS(ftest.cardinal(), 2);
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    for(int i = 0; i < ftest.cardinal(); ++i)
    {
        TS_ASSERT(ftest[i]->isValid());
        TS_ASSERT_DELTA(ftest[i]->dbl1(), -8901.4, 0.0001);
        TS_ASSERT_DELTA(ftest[i]->dbl2(), 98765.43, 0.0001);
        TS_ASSERT_EQUALS(ftest[i]->iv1(), 9876);
    }

    ftest.setInvalid();
    TS_ASSERT(!ftest.isValid());
    TS_TRACE("3. Extract same data but truncated\n");
    ftest.extract(sizeof(int)+(sizeof(buf)-sizeof(int))/2, reinterpret_cast<const unsigned char *>(buf));
    TS_ASSERT_EQUALS(ftest.size(), 24);
    TS_ASSERT_EQUALS(ftest.cardinal(), 1);
    TS_ASSERT(ftest[0]->isValid());
    TS_ASSERT_DELTA(ftest[0]->dbl1(), -8901.4, 0.0001);
    TS_ASSERT_DELTA(ftest[0]->dbl2(), 98765.43, 0.0001);
    TS_ASSERT_EQUALS(ftest[0]->iv1(), 9876);
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);

    TS_TRACE("3a. Extract same data not truncated\n");
    ftest.extract(sizeof(buf), reinterpret_cast<const unsigned char *>(buf));
    TS_ASSERT_EQUALS(ftest.size(), 44);
    TS_ASSERT_EQUALS(ftest.cardinal(), 2);
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);
    for (int i = 0; i < ftest.cardinal(); ++i)
    {
        TS_ASSERT(ftest[i]->isValid());
        TS_ASSERT_DELTA(ftest[i]->dbl1(), -8901.4, 0.0001);
        TS_ASSERT_DELTA(ftest[i]->dbl2(), 98765.43, 0.0001);
        TS_ASSERT_EQUALS(ftest[i]->iv1(), 9876);
    }

    TS_TRACE("4. Extract negative buflen\n");
    ftest.extract(-(sizeof(buf)), reinterpret_cast<const unsigned char *>(buf));
    TS_ASSERT_EQUALS(ftest.size(), 4);
    TS_ASSERT_EQUALS(ftest.cardinal(), 0);
    TS_ASSERT(!ftest.isValid());
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);

    ftest.setInvalid();
    TS_ASSERT(!ftest.isValid());
    memset(buf+sizeof(int), 0, sizeof(buf));
    TS_TRACE("5. Extract cardinal is too large\n");
    dbl1 = 65.5;
    memcpy(buf+sizeof(int), &dbl1, sizeof(double));
    dbl2 = -128.4;
    memcpy(buf+sizeof(int)+sizeof(double), &dbl2, sizeof(double));
    iv1 = 23;
    memcpy(buf+sizeof(int)+2*sizeof(double), &iv1, sizeof(int));
    ftest.extract(sizeof(buf), reinterpret_cast<const unsigned char *>(buf));
    TS_ASSERT_EQUALS(ftest.size(), 44);
    TS_ASSERT_EQUALS(ftest.cardinal(), 2);
    TS_ASSERT(ftest[0]->isValid());
    TS_ASSERT_DELTA(ftest[0]->dbl1(), 65.5, 0.0001);
    TS_ASSERT_DELTA(ftest[0]->dbl2(), -128.4, 0.0001);
    TS_ASSERT_EQUALS(ftest[0]->iv1(), 23);
    TS_ASSERT(ftest[1]->isValid());
    TS_ASSERT_EQUALS(ftest[1]->dbl1(), 0);
    TS_ASSERT_EQUALS(ftest[1]->dbl2(), 0);
    TS_ASSERT_EQUALS(ftest[1]->iv1(), 0);
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);

    ftest.setInvalid();
    TS_ASSERT(!ftest.isValid());
    memset(buf+sizeof(int), 0, sizeof(buf));
    TS_TRACE("6. Extract truncated data again\n");
    memcpy(buf, &count, sizeof(int));
    size = sizeof(int);
    for(int i = 0; i<INDEX; ++i)
    {
        dbl1 = 454.4545;
        memcpy(buf+size, &dbl1, sizeof(double));
        size += sizeof(double);
        dbl2 = -78.7878;
        memcpy(buf+size, &dbl2, sizeof(double));
        size += sizeof(double);
    }
    ftest.extract(2*sizeof(double) + sizeof(int), reinterpret_cast<const unsigned char *>(buf));
    TS_ASSERT_EQUALS(ftest.size(), 24);
    TS_ASSERT_EQUALS(ftest.cardinal(), 1);
    TS_ASSERT(ftest[0]->isValid());
    TS_ASSERT_DELTA(ftest[0]->dbl1(), 454.4545, 0.0001);
    TS_ASSERT_DELTA(ftest[0]->dbl2(), -78.7878, 0.0001);
    TS_ASSERT_EQUALS(ftest[0]->iv1(), -1);
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);

    TS_TRACE("7. Extract nothing\n");
    count = 0;
    ftest.extract(sizeof(int), reinterpret_cast<const unsigned char *>(&count));
    TS_ASSERT_EQUALS(ftest.size(), 4);
    TS_ASSERT_EQUALS(ftest.cardinal(), 0);
    TS_ASSERT(!ftest.isValid());
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);

    TS_TRACE("8. Extract nothing because there is no data\n");
    count = 12;
    ftest.extract(sizeof(int), reinterpret_cast<const unsigned char *>(&count));
    TS_ASSERT_EQUALS(ftest.size(), 4);
    TS_ASSERT_EQUALS(ftest.cardinal(), 0);
    TS_ASSERT(!ftest.isValid());
    TS_ASSERT_EQUALS(checker1.getStateAndReset(), 1);

    TS_ASSERT(!ftest2.isValid());
    memset(buf2+sizeof(int), 0, sizeof(buf2));
    TS_TRACE("9. Test extract different structure from a buffer\n");
    count = INDEX + 2;
    memcpy(buf2, &count, sizeof(int));
    size = sizeof(int);
    for(int i = 0; i<INDEX+2; ++i)
    {
        iv1 = 888;
        memcpy(buf2+size, &iv1, sizeof(int));
        size += sizeof(int);
        iv2 = -777;
        memcpy(buf2+size, &iv2, sizeof(int));
        size += sizeof(int);
        dbl1 = 88.8888;
        memcpy(buf2+size, &dbl1, sizeof(double));
        size += sizeof(double);
        iv3 = 87654;
        memcpy(buf2+size, &iv3, sizeof(unsigned long));
        size += sizeof(unsigned long);
        dbl2 = -77.7777;
        memcpy(buf2+size, &dbl2, sizeof(double));
        size += sizeof(double);
    }
    ftest2.extract(sizeof(buf2), reinterpret_cast<const unsigned char *>(buf2));
    TS_ASSERT_EQUALS(ftest2.size(), 116);
    TS_ASSERT_EQUALS(ftest2.cardinal(), INDEX+2);
    for(int i = 0; i < ftest2.cardinal(); ++i)
    {
        TS_ASSERT(ftest2[i]->isValid());
        TS_ASSERT_EQUALS(ftest2[i]->iv1(), 888);
        TS_ASSERT_EQUALS(ftest2[i]->iv2(), -777);
        TS_ASSERT_DELTA(ftest2[i]->dbl1(), 88.8888, 0.0001);
        TS_ASSERT_EQUALS(ftest2[i]->iv3(), 87654);
        TS_ASSERT_DELTA(ftest2[i]->dbl2(), -77.7777, 0.0001);
    }

    memset(buf2+sizeof(int), 0, sizeof(buf2));
    TS_TRACE("10. Extract different structure again\n");
    size = sizeof(int);
    for(int i = 0; i<INDEX+2; ++i)
    {
        iv1 = -115;
        memcpy(buf2+size, &iv1, sizeof(int));
        size += sizeof(int);
        iv2 = 115;
        memcpy(buf2+size, &iv2, sizeof(int));
        size += sizeof(int);
        dbl1 = 11.1212;
        memcpy(buf2+size, &dbl1, sizeof(double));
        size += sizeof(double);
        iv3 = -15151;
        memcpy(buf2+size, &iv3, sizeof(unsigned long));
        size += sizeof(unsigned long);
        dbl2 = -11.1212;
        memcpy(buf2+size, &dbl2, sizeof(double));
        size += sizeof(double);
    }
    ftest2.extract(sizeof(buf2), reinterpret_cast<const unsigned char *>(buf2));
    TS_ASSERT_EQUALS(ftest2.size(), 116);
    TS_ASSERT_EQUALS(ftest2.cardinal(), INDEX+2);
    for(int i = 0; i < ftest2.cardinal(); ++i)
    {
        TS_ASSERT(ftest2[i]->isValid());
        TS_ASSERT_EQUALS(ftest2[i]->iv1(), -115);
        TS_ASSERT_EQUALS(ftest2[i]->iv2(), 115);
        TS_ASSERT_DELTA(ftest2[i]->dbl1(), 11.1212, 0.0001);
        TS_ASSERT_EQUALS(ftest2[i]->iv3(), -15151);
        TS_ASSERT_DELTA(ftest2[i]->dbl2(), -11.1212, 0.0001);
    }

    TS_TRACE("11. Extract same data\n");
    ftest2.extract(sizeof(buf2), reinterpret_cast<const unsigned char *>(buf2));
    TS_ASSERT_EQUALS(ftest2.size(), 116);
    TS_ASSERT_EQUALS(ftest2.cardinal(), INDEX+2);
    for(int i = 0; i < ftest2.cardinal(); ++i)
    {
        TS_ASSERT(ftest2[i]->isValid());
        TS_ASSERT_EQUALS(ftest2[i]->iv1(), -115);
        TS_ASSERT_EQUALS(ftest2[i]->iv2(), 115);
        TS_ASSERT_DELTA(ftest2[i]->dbl1(), 11.1212, 0.0001);
        TS_ASSERT_EQUALS(ftest2[i]->iv3(), -15151);
        TS_ASSERT_DELTA(ftest2[i]->dbl2(), -11.1212, 0.0001);
    }

    ftest2.setInvalid();
    TS_ASSERT(!ftest2.isValid());
    TS_ASSERT_EQUALS(ftest2.size(), 4);
    TS_ASSERT_EQUALS(ftest2.cardinal(), 0);
}

void FlexComplexVarTestSuite::testFlexOutput(void)
{
    flex_var<my_complex_var1> ftest;
    flex_var<my_complex_var1> ftest2;
    my_complex_var1 cv1;
    outbuf ob, ob2;
    char buf[6*(2*sizeof(double) + sizeof(int)) + sizeof(int)];
    char buf2[7*(2*sizeof(double) + sizeof(int)) + sizeof(int)];
    int count = 0;
    int size = 0;

    TS_ASSERT(!ftest.isValid());
    TS_TRACE("Before OUTPUT: Add several my_complex_var\n");
    float testdbl = 11.1111;
    int testi = 345;
    for(int i = 0; i < 6; ++i, ++testdbl, ++testi)
    {
        cv1.dbl1 = testdbl;
        cv1.dbl2 = -testdbl;
        cv1.iv1 = testi;
        ftest.add(cv1);
        TS_ASSERT_DELTA(ftest[i]->dbl1.to_double(), testdbl, 0.0001);
        TS_ASSERT_DELTA(ftest[i]->dbl2.to_double(), -testdbl, 0.0001);
        TS_ASSERT_EQUALS(ftest[i]->iv1.to_int(), testi);
        TS_ASSERT(ftest[i]->isValid());
    }
    TS_ASSERT(ftest.cardinal() == 6);

    TS_TRACE("1. Check output for ftest\n");
    ob.set(reinterpret_cast<unsigned char *>(buf), sizeof(buf));
    ftest.output(ob);
    count = ftest.cardinal();
    TS_ASSERT(memcmp(buf, &count, sizeof(int)) == 0);
    size = sizeof(int);
    for(int i = 0; i < count; ++i)
    {
        double dbl1 = ftest[i]->dbl1();
        double dbl2 = ftest[i]->dbl2();
        int iv1 = ftest[i]->iv1();
        TS_ASSERT(memcmp(buf+size, &dbl1, sizeof(double)) == 0);
        size += sizeof(double);
        TS_ASSERT(memcmp(buf+size, &dbl2, sizeof(double)) == 0);
        size += sizeof(double);
        TS_ASSERT(memcmp(buf+size, &iv1, sizeof(int)) == 0);
        size += sizeof(int);
    }

    TS_TRACE("2. Check output again for ftest2\n");
    TS_ASSERT(ftest != ftest2);
    ftest2 = ftest;
    cv1.dbl1 = -0.9898;
    cv1.iv1 = -98;
    ftest2.add(cv1);
    ob2.set(reinterpret_cast<unsigned char *>(buf2), sizeof(buf2));
    ftest2.output(ob2);
    count = ftest2.cardinal();
    TS_ASSERT(memcmp(buf2, &count, sizeof(int)) == 0);
    size = sizeof(int);
    double dl;
    for(int i = 0; i < count; ++i)
    {
        double dbl1 = ftest2[i]->dbl1();
        double dbl2 = ftest2[i]->dbl2();
        int iv1 = ftest2[i]->iv1();
        TS_ASSERT(memcmp(buf2+size, &dbl1, sizeof(double)) == 0);
        size += sizeof(double);
        TS_ASSERT(memcmp(buf2+size, &dbl2, sizeof(double)) == 0);
        size += sizeof(double);
        TS_ASSERT(memcmp(buf2+size, &iv1, sizeof(int)) == 0);
        size += sizeof(int);
    }
}

void FlexComplexVarTestSuite::testFlexEqualOperator(void)
{
    flex_var<my_complex_var1> ftest;
    my_complex_var1 cv1;

    TS_ASSERT(!ftest.isValid());
    TS_TRACE("Before OPERATOR=: Add several my_complex_var\n");
    float testdbl = 11.1111;
    int testi = 345;
    for(int i = 0; i < 6; ++i, ++testdbl, ++testi)
    {
        cv1.dbl1 = testdbl;
        cv1.dbl2 = -testdbl;
        cv1.iv1 = testi;
        ftest.add(cv1);
        TS_ASSERT_DELTA(ftest[i]->dbl1.to_double(), testdbl, 0.0001);
        TS_ASSERT_DELTA(ftest[i]->dbl2.to_double(), -testdbl, 0.0001);
        TS_ASSERT_EQUALS(ftest[i]->iv1.to_int(), testi);
        TS_ASSERT(ftest[i]->isValid());
    }
    TS_ASSERT(ftest.cardinal() == 6);

    // Flex_var input
    flex_var<my_complex_var1> ftest2;
    TS_ASSERT(!ftest2.isValid());
    TS_TRACE("1. Test Flex Operator=\n");
    ftest2.operator = (ftest);
    TS_ASSERT(ftest2.cardinal() == 6);
    for(int i = 0; i < ftest2.cardinal(); ++i)
    {
        TS_ASSERT_DELTA(ftest[i]->dbl1.to_double(), ftest2[i]->dbl1.to_double(), 0.0001);
        TS_ASSERT_DELTA(ftest[i]->dbl2.to_double(), ftest2[i]->dbl2.to_double(), 0.0001);
        TS_ASSERT_EQUALS(ftest[i]->iv1.to_int(), ftest2[i]->iv1.to_int());
        TS_ASSERT(ftest2[i]->isValid());
    }

    cv1.dbl1 = -99.999;
    cv1.dbl2 = 88.888;
    cv1.iv1 = -98;
    ftest.add(cv1);
    flex_var<my_complex_var1> ftest3;
    TS_ASSERT(!ftest3.isValid());
    TS_TRACE("2. Test Flex Operator= again\n");
    ftest3.operator = (ftest);
    TS_ASSERT(ftest3.cardinal() == 7);
    for(int i = 0; i < ftest3.cardinal(); ++i)
    {
        TS_ASSERT_DELTA(ftest[i]->dbl1.to_double(), ftest3[i]->dbl1.to_double(), 0.0001);
        TS_ASSERT_DELTA(ftest[i]->dbl2.to_double(), ftest3[i]->dbl2.to_double(), 0.0001);
        TS_ASSERT_EQUALS(ftest[i]->iv1.to_int(), ftest3[i]->iv1.to_int());
        TS_ASSERT(ftest3[i]->isValid());
    }

    flex_var<my_complex_var1> ftest4;
    ftest2.setInvalid();
    TS_ASSERT(!ftest2.isValid());
    TS_ASSERT(!ftest4.isValid());
    TS_TRACE("3. Test empty Flex Operator=\n");
    ftest4.operator = (ftest2);
    TS_ASSERT(!ftest4.isValid());

    TS_TRACE("4. Test valid Flex Operator=\n");
    cv1.dbl1 = -90.009;
    cv1.dbl2 = 80.008;
    cv1.iv1 = -90;
    for(int i = 0; i < 15; ++i)
        ftest4.add(cv1);
    ftest4.operator = (ftest);
    TS_ASSERT(ftest4.cardinal() == 7);
    for(int i = 0; i < ftest4.cardinal(); ++i)
    {
        TS_ASSERT_DELTA(ftest[i]->dbl1.to_double(), ftest4[i]->dbl1.to_double(), 0.0001);
        TS_ASSERT_DELTA(ftest[i]->dbl2.to_double(), ftest4[i]->dbl2.to_double(), 0.0001);
        TS_ASSERT_EQUALS(ftest[i]->iv1.to_int(), ftest4[i]->iv1.to_int());
        TS_ASSERT(ftest4[i]->isValid());
    }

    // Mpt_var input
    flex_var<my_complex_var1> ftest5;
    TS_ASSERT(!ftest5.isValid());
    TS_TRACE("5. Test Flex mpt_var Operator=\n");
    mpt_var *mp = &ftest;
    ftest5.operator = (*mp);
    TS_ASSERT(ftest5.cardinal() == 7);
    for(int i = 0; i < ftest5.cardinal(); ++i)
    {
        TS_ASSERT_DELTA(ftest[i]->dbl1.to_double(), ftest5[i]->dbl1.to_double(), 0.0001);
        TS_ASSERT_DELTA(ftest[i]->dbl2.to_double(), ftest5[i]->dbl2.to_double(), 0.0001);
        TS_ASSERT_EQUALS(ftest[i]->iv1.to_int(), ftest5[i]->iv1.to_int());
        TS_ASSERT(ftest5[i]->isValid());
    }

    cv1.dbl1 = -90.009;
    cv1.dbl2 = 80.008;
    cv1.iv1 = -90;
    ftest.add(cv1);
    flex_var<my_complex_var1> ftest6;
    TS_ASSERT(!ftest6.isValid());
    TS_TRACE("6. Test Flex mpt_var Operator= again\n");
    ftest6.operator = (*mp);
    TS_ASSERT(ftest6.cardinal() == 8);
    for(int i = 0; i < ftest6.cardinal(); ++i)
    {
        TS_ASSERT_DELTA(ftest[i]->dbl1.to_double(), ftest6[i]->dbl1.to_double(), 0.0001);
        TS_ASSERT_DELTA(ftest[i]->dbl2.to_double(), ftest6[i]->dbl2.to_double(), 0.0001);
        TS_ASSERT_EQUALS(ftest[i]->iv1.to_int(), ftest6[i]->iv1.to_int());
        TS_ASSERT(ftest6[i]->isValid());
    }

    flex_var<my_complex_var1> ftest7;
    TS_ASSERT(!ftest7.isValid());
    TS_TRACE("7. Test empty Flex mpt_var Operator=\n");
    ftest7 = ftest6;
    TS_ASSERT(ftest7.cardinal() == 8);
    flex_var<my_complex_var1> ftest7a;
    ftest7 = ftest7a;
    TS_ASSERT(!ftest7.isValid());

    flex_var<my_complex_var1> ftest8;
    TS_ASSERT(!ftest2.isValid());
    TS_ASSERT(!ftest8.isValid());
    TS_TRACE("8. Test invalid Flex mpt_var Operator=\n");
    mpt_var *mp8 = &ftest2;
    ftest8.operator = (*mp8);
    TS_ASSERT(!ftest8.isValid());

    flex_var<my_complex_var1> ftest9;
    TS_ASSERT(!ftest9.isValid());
    TS_TRACE("9. Test valid Flex mpt_var Operator=\n");
    cv1.dbl1 = -90.009;
    cv1.dbl2 = 80.008;
    cv1.iv1 = -90;
    for(int i = 0; i < 3; ++i)
        ftest9.add(cv1);
    ftest9.operator = (*mp);
    TS_ASSERT(ftest9.cardinal() == 8);
    for(int i = 0; i < ftest9.cardinal(); ++i)
    {
        TS_ASSERT_DELTA(ftest[i]->dbl1.to_double(), ftest9[i]->dbl1.to_double(), 0.0001);
        TS_ASSERT_DELTA(ftest[i]->dbl2.to_double(), ftest9[i]->dbl2.to_double(), 0.0001);
        TS_ASSERT_EQUALS(ftest[i]->iv1.to_int(), ftest9[i]->iv1.to_int());
        TS_ASSERT(ftest9[i]->isValid());
    }

    flex_var<my_complex_var1> ftest10;
    TS_ASSERT(!ftest10.isValid());
    TS_TRACE("10. Test valid Flex mpt_var Operator= again\n");
    cv1.dbl1 = -90.009;
    cv1.dbl2 = 80.008;
    cv1.iv1 = -90;
    for(int i = 0; i < 12; ++i)
        ftest10.add(cv1);
    ftest10.operator = (*mp);
    TS_ASSERT(ftest10.cardinal() == 8);
    for(int i = 0; i < ftest10.cardinal(); ++i)
    {
        TS_ASSERT_DELTA(ftest[i]->dbl1.to_double(), ftest10[i]->dbl1.to_double(), 0.0001);
        TS_ASSERT_DELTA(ftest[i]->dbl2.to_double(), ftest10[i]->dbl2.to_double(), 0.0001);
        TS_ASSERT_EQUALS(ftest[i]->iv1.to_int(), ftest10[i]->iv1.to_int());
        TS_ASSERT(ftest10[i]->isValid());
    }
}

void FlexComplexVarTestSuite::testFlexDblEqualOperator(void)
{
    flex_var<my_complex_var1> ftest;
    my_complex_var1 cv1;
    bool flag = false;
    mpt_var *mp = &ftest;

    TS_ASSERT(!ftest.isValid());
    TS_TRACE("Before OPERATOR==: Add several my_complex_var\n");
    float testdbl = 11.1111;
    int testi = 345;
    for(int i = 0; i < 6; ++i, ++testdbl, ++testi)
    {
        cv1.dbl1 = testdbl;
        cv1.dbl2 = -testdbl;
        cv1.iv1 = testi;
        ftest.add(cv1);
        TS_ASSERT_DELTA(ftest[i]->dbl1.to_double(), testdbl, 0.0001);
        TS_ASSERT_DELTA(ftest[i]->dbl2.to_double(), -testdbl, 0.0001);
        TS_ASSERT_EQUALS(ftest[i]->iv1.to_int(), testi);
        TS_ASSERT(ftest[i]->isValid());
    }
    TS_ASSERT(ftest.cardinal() == 6);

    flex_var<my_complex_var1> ftest1;
    TS_ASSERT(!ftest1.isValid());
    TS_TRACE("1. Test same size and same data Flex mpt_var Operator==\n");
    ftest1 = ftest;
    flag = ftest1.operator == (*mp);
    TS_ASSERT(flag == true);

    TS_TRACE("2. Test different size and same data Flex mpt_var Operator==\n");
    ftest1.resize(3);
    flag = ftest1.operator == (*mp);
    TS_ASSERT(flag == false);

    TS_TRACE("3. Test same size and different data Flex mpt_var Operator==\n");
    flex_var<my_complex_var1> ftest2;
    TS_ASSERT(!ftest2.isValid());
    cv1.dbl1 = -90.009;
    cv1.dbl2 = 80.008;
    cv1.iv1 = -90;
    for(int i = 0; i < 6; ++i)
        ftest2.add(cv1);
    flag = ftest2.operator == (*mp);
    TS_ASSERT(flag == false);

    TS_TRACE("4. Test different size and different data Flex mpt_var Operator==\n");
    for(int i = 0; i < 4; ++i)
        ftest2.add(cv1);
    flag = ftest2.operator == (*mp);
    TS_ASSERT(flag == false);

    TS_TRACE("5. Test one invalid data Flex mpt_var Operator==\n");
    ftest2.setInvalid();
    TS_ASSERT(!ftest2.isValid());
    flag = ftest2.operator == (*mp);
    TS_ASSERT(flag == false);

    TS_TRACE("6. Test both invalid data Flex mpt_var Operator==\n");
    ftest.setInvalid();
    TS_ASSERT(!ftest.isValid());
    TS_ASSERT(!ftest2.isValid());
    flag = ftest2.operator == (*mp);
    TS_ASSERT(flag == true);
}

void FlexComplexVarTestSuite::testFlexStreamOperator(void)
{
    flex_var<my_complex_var1> oftest;
    my_complex_var1 cv1;

    value_var::set_float_format("%.4f");
    TS_ASSERT(!oftest.isValid());
    TS_TRACE("1. Test stream Operator << with cardinal=6\n");
    float testdbl = 11.1111;
    int testi = 345;
    for(int i = 0; i < 6; ++i, ++testdbl, ++testi)
    {
        cv1.dbl1 = testdbl;
        cv1.dbl2 = -testdbl;
        cv1.iv1 = testi;
        oftest.add(cv1);
        TS_ASSERT_DELTA(oftest[i]->dbl1.to_double(), testdbl, 0.0001);
        TS_ASSERT_DELTA(oftest[i]->dbl2.to_double(), -testdbl, 0.0001);
        TS_ASSERT_EQUALS(oftest[i]->iv1.to_int(), testi);
        TS_ASSERT(oftest[i]->isValid());
    }
    TS_ASSERT(oftest.cardinal() == 6);
    std::ostringstream oss;
    oss << oftest;
    TS_ASSERT(oss.str() == "6 11.1111 -11.1111 345 12.1111 -12.1111 346 13.1111 -13.1111 347 14.1111 -14.1111 348 15.1111 -15.1111 349 16.1111 -16.1111 350");

    oss.rdbuf()->str(std::string ());
    TS_TRACE("2. Test stream Operator << with cardinal=4\n");
    testdbl = 4.4444;
    testi = 123;
    oftest.setInvalid();
    for(int i = 0; i < 4; ++i, ++testdbl, ++testi)
    {
        cv1.dbl1 = testdbl;
        cv1.dbl2 = -testdbl;
        cv1.iv1 = testi;
        oftest.add(cv1);
        TS_ASSERT_DELTA(oftest[i]->dbl1.to_double(), testdbl, 0.0001);
        TS_ASSERT_DELTA(oftest[i]->dbl2.to_double(), -testdbl, 0.0001);
        TS_ASSERT_EQUALS(oftest[i]->iv1.to_int(), testi);
        TS_ASSERT(oftest[i]->isValid());
    }
    TS_ASSERT(oftest.cardinal() == 4);
    oss << oftest;
    TS_ASSERT(oss.str() == "4 4.4444 -4.4444 123 5.4444 -5.4444 124 6.4444 -6.4444 125 7.4444 -7.4444 126");

    oss.rdbuf()->str(std::string ());
    TS_TRACE("3. Test stream Operator << with cardinal=0\n");
    oftest.setInvalid();
    TS_ASSERT(!oftest.isValid());
    oss << oftest;
    TS_ASSERT(oss.str() == "0");

    oss.rdbuf()->str(std::string ());
    TS_TRACE("4. Test stream Operator << and >> with cardinal=5\n");
    oftest.setInvalid();
    for(int i = 0; i < 5; ++i, ++testdbl, ++testi)
    {
        cv1.dbl1 = testdbl;
        cv1.dbl2 = -testdbl;
        cv1.iv1 = testi;
        oftest.add(cv1);
        TS_ASSERT_DELTA(oftest[i]->dbl1.to_double(), testdbl, 0.0001);
        TS_ASSERT_DELTA(oftest[i]->dbl2.to_double(), -testdbl, 0.0001);
        TS_ASSERT_EQUALS(oftest[i]->iv1.to_int(), testi);
        TS_ASSERT(oftest[i]->isValid());
    }
    TS_ASSERT(oftest.cardinal() == 5);
    oss << oftest;
    TS_ASSERT(oss.str() == "5 8.4444 -8.4444 127 9.4444 -9.4444 128 10.4444 -10.4444 129 11.4444 -11.4444 130 12.4444 -12.4444 131");
    flex_var<my_complex_var1> iftest;
    TS_ASSERT(!iftest.isValid());
    std::istringstream iss(oss.str());
    iss >> iftest;
    TS_ASSERT(iftest.cardinal() == 5);
    for(int j = 0; j < iftest.cardinal(); ++j)
    {
        TS_ASSERT_DELTA(oftest[j]->dbl1.to_double(), iftest[j]->dbl1.to_double(), 0.0001);
        TS_ASSERT_DELTA(oftest[j]->dbl2.to_double(), iftest[j]->dbl2.to_double(), 0.0001);
        TS_ASSERT_EQUALS(oftest[j]->iv1.to_int(), iftest[j]->iv1.to_int());
        TS_ASSERT(iftest[j]->isValid());
    }
    std::string longer("6 8.4444 -8.4444 127 9.4444 -9.4444 128 10.4444 -10.4444 129 11.4444 -11.4444 130 12.4444 -12.4444 131 13.4444 -13.4444 132");
    std::istringstream iss4(longer);
    iss4 >> iftest;
    TS_ASSERT(iftest.cardinal() == 6);

    oss.rdbuf()->str(std::string ());
    TS_TRACE("5. Test stream Operator << and >> with cardinal=0\n");
    oftest.setInvalid();
    TS_ASSERT(!oftest.isValid());
    oss << oftest;
    TS_ASSERT(oss.str() == "0");
    iftest.setInvalid();
    //iss.rdbuf()->str(oss.str());
    std::istringstream iss1(oss.str());
    iss1 >> iftest;
    TS_ASSERT(iftest.cardinal() == 0);

    oss.rdbuf()->str(std::string ());
    TS_TRACE("6. Test stream Operator << and >> with cardinal=3\n");
    oftest.setInvalid();
    for(int i = 0; i < 3; ++i, ++testdbl, ++testi)
    {
        cv1.dbl1 = testdbl;
        cv1.dbl2 = -testdbl;
        cv1.iv1 = testi;
        oftest.add(cv1);
        TS_ASSERT_DELTA(oftest[i]->dbl1.to_double(), testdbl, 0.0001);
        TS_ASSERT_DELTA(oftest[i]->dbl2.to_double(), -testdbl, 0.0001);
        TS_ASSERT_EQUALS(oftest[i]->iv1.to_int(), testi);
        TS_ASSERT(oftest[i]->isValid());
    }
    TS_ASSERT(oftest.cardinal() == 3);
    iftest.setInvalid();
    oss << oftest;
    TS_ASSERT(oss.str() == "3 13.4444 -13.4444 132 14.4444 -14.4444 133 15.4444 -15.4444 134");
    //iss.rdbuf()->str(oss.str());
    std::istringstream iss2(oss.str());
    iss2 >> iftest;
    TS_ASSERT(iftest.cardinal() == 3);
    for(int j = 0; j < iftest.cardinal(); ++j)
    {
        TS_ASSERT_DELTA(oftest[j]->dbl1.to_double(), iftest[j]->dbl1.to_double(), 0.0001);
        TS_ASSERT_DELTA(oftest[j]->dbl2.to_double(), iftest[j]->dbl2.to_double(), 0.0001);
        TS_ASSERT_EQUALS(oftest[j]->iv1.to_int(), iftest[j]->iv1.to_int());
        TS_ASSERT(iftest[j]->isValid());
    }

    oss.rdbuf()->str(std::string ());
    TS_TRACE("7. Test stream Operator << and >> with cardinal=4 but invalid stream data\n");
    std::string sbuf = "4";
    //iss.rdbuf()->str(sbuf);
    std::istringstream iss3(oss.str());
    iss3 >> iftest;
    TS_ASSERT(!iftest.isValid());
    TS_ASSERT(iftest.cardinal() == 0);
}

void FlexComplexVarTestSuite::testFlexGetNext(void)
{
    flex_var<my_complex_var> farry[2];
    TS_ASSERT_EQUALS(farry[0].getNext(), &farry[1]);
    TS_ASSERT_EQUALS(farry[0].rtti(), farry[1].rtti());

    r_flex_var<my_complex_var> rfarry[2];
    TS_ASSERT_EQUALS(rfarry[0].getNext(), &rfarry[1]);
    TS_ASSERT_EQUALS(rfarry[0].rtti(), rfarry[1].rtti());
    TS_ASSERT_EQUALS(farry[0].rtti(), rfarry[0].rtti());

    ext_flex_var<my_complex_var1, 4, 1, 2> efv[2];
    TS_ASSERT_EQUALS(efv[0].getNext(), &efv[1]);
    TS_ASSERT_EQUALS(efv[0].rtti(), efv[1].rtti());
    TS_ASSERT_EQUALS(farry[0].rtti(), efv[0].rtti());
}

void FlexComplexVarTestSuite::testExtFlexExtract(void)
{
    double dbl1[100];
    double dbl2[100];
    int iv1[100];
    int iv2[100];
    unsigned long iv3[100];
    for (int idx = 0; idx < 100; ++idx)
    {
        dbl1[idx] = (double)rand() / rand();
        dbl2[idx] = rand() * rand();
        iv1[idx] = -rand();
        iv2[idx] = rand() - rand();
        iv3[idx] = rand();
    }
    char buf1[4 + (8 + 8 + 4) * 100];
    char buf2[4 + (4 + 4 + 8 + 4 + 8) * 100];
    char buf1a[4 + (8 + 8 + 4) * 100];
    char buf2a[4 + (4 + 4 + 8 + 4 + 8) * 100];

    outbuf ob;
    ob.set((unsigned char *)buf1 + 4, sizeof(buf1) - 4);
    for (int idx = 0; idx < 100; ++idx)
    {
        generic_var<double> d1(dbl1[idx]);
        d1.output(ob);
        generic_var<double> d2(dbl2[idx]);
        d2.output(ob);
        generic_var<int> d3(iv1[idx]);
        d3.output(ob);
    }
    ob.set((unsigned char *)buf2 + 4, sizeof(buf2) - 4);
    for (int idx = 0; idx < 100; ++idx)
    {
        generic_var<int> d1(iv1[idx]);
        d1.output(ob);
        generic_var<int> d2(iv2[idx]);
        d2.output(ob);
        generic_var<double> d3(dbl1[idx]);
        d3.output(ob);
        generic_var<unsigned long> d4(iv3[idx]);
        d4.output(ob);
        generic_var<double> d5(dbl2[idx]);
        d5.output(ob);
    }

    char ch = 100;
    ext_flex_var<my_complex_var1, 1, 1, 4> flex1;
    memset(buf1, 0, 4);
    memcpy(buf1 + 1, &ch, 1);
    flex1.extract(sizeof(buf1), (unsigned char *)buf1);
    TS_ASSERT_EQUALS(flex1.cardinal(), 100);
    for (int idx = 0; idx < flex1.cardinal(); ++idx)
    {
        TS_ASSERT_EQUALS(flex1[idx]->dbl1, dbl1[idx]);
        TS_ASSERT_EQUALS(flex1[idx]->dbl2, dbl2[idx]);
        TS_ASSERT_EQUALS(flex1[idx]->iv1, iv1[idx]);
    }
    memset(buf1a, 0, sizeof(buf1a));
    ob.set((unsigned char *)buf1a, sizeof(buf1a));
    flex1.output(ob);
    TS_ASSERT_EQUALS(ob.size(), sizeof(buf1));
    TS_ASSERT_SAME_DATA(buf1, buf1a, sizeof(buf1));
    ext_flex_var<my_complex_var1, 1, 1, 4> flex1a(flex1);
    TS_ASSERT_EQUALS(flex1a.cardinal(), 100);
    for (int idx = 0; idx < flex1a.cardinal(); ++idx)
    {
        TS_ASSERT_EQUALS(flex1a[idx]->dbl1, dbl1[idx]);
        TS_ASSERT_EQUALS(flex1a[idx]->dbl2, dbl2[idx]);
        TS_ASSERT_EQUALS(flex1a[idx]->iv1, iv1[idx]);
    }

    ext_flex_var<my_complex_var2, 1, 2, 4> flex2;
    memset(buf2, 0, 4);
    memcpy(buf2 + 2, &ch, sizeof(ch));
    flex2.extract(sizeof(buf2), (unsigned char *)buf2);
    TS_ASSERT_EQUALS(flex2.cardinal(), 100);
    for (int idx = 0; idx < flex2.cardinal(); ++idx)
    {
        TS_ASSERT_EQUALS(flex2[idx]->dbl1, dbl1[idx]);
        TS_ASSERT_EQUALS(flex2[idx]->dbl2, dbl2[idx]);
        TS_ASSERT_EQUALS(flex2[idx]->iv1, iv1[idx]);
        TS_ASSERT_EQUALS(flex2[idx]->iv2, iv2[idx]);
        TS_ASSERT_EQUALS(flex2[idx]->iv3, iv3[idx]);
    }
    memset(buf2a, 0, sizeof(buf2a));
    ob.set((unsigned char *)buf2a, sizeof(buf2a));
    flex2.output(ob);
    TS_ASSERT_EQUALS(ob.size(), sizeof(buf2));
    TS_ASSERT_SAME_DATA(buf2, buf2a, sizeof(buf2));
    flex_var<my_complex_var2> flex2a;
    flex2a = flex2;
    TS_ASSERT_EQUALS(flex2a.cardinal(), 100);
    for (int idx = 0; idx < flex2a.cardinal(); ++idx)
    {
        TS_ASSERT_EQUALS(flex2a[idx]->dbl1, dbl1[idx]);
        TS_ASSERT_EQUALS(flex2a[idx]->dbl2, dbl2[idx]);
        TS_ASSERT_EQUALS(flex2a[idx]->iv1, iv1[idx]);
    }

    ext_flex_var<my_complex_var1, 1, 3, 4> flex3;
    memset(buf1, 0, 4);
    memcpy(buf1 + 3, &ch, sizeof(ch));
    flex3.extract(sizeof(buf1), (unsigned char *)buf1);
    TS_ASSERT_EQUALS(flex3.cardinal(), 100);
    for (int idx = 0; idx < flex3.cardinal(); ++idx)
    {
        TS_ASSERT_EQUALS(flex3[idx]->dbl1, dbl1[idx]);
        TS_ASSERT_EQUALS(flex3[idx]->dbl2, dbl2[idx]);
        TS_ASSERT_EQUALS(flex3[idx]->iv1, iv1[idx]);
    }
    memset(buf1a, 0, sizeof(buf1a));
    ob.set((unsigned char *)buf1a, sizeof(buf1a));
    flex3.output(ob);
    TS_ASSERT_EQUALS(ob.size(), sizeof(buf1));
    TS_ASSERT_SAME_DATA(buf1, buf1a, sizeof(buf1));
    ext_flex_var<my_complex_var1, 8, 4, 16> flex3b;
    flex3b = flex3;
    TS_ASSERT_EQUALS(flex3b.cardinal(), 100);
    for (int idx = 0; idx < flex3b.cardinal(); ++idx)
    {
        TS_ASSERT_EQUALS(flex3b[idx]->dbl1, dbl1[idx]);
        TS_ASSERT_EQUALS(flex3b[idx]->dbl2, dbl2[idx]);
        TS_ASSERT_EQUALS(flex3b[idx]->iv1, iv1[idx]);
    }

    short sh = 100;
    ext_flex_var<my_complex_var2, 2, 1, 4> flex4;
    memset(buf2, 0, 4);
    memcpy(buf2 + 1, &sh, sizeof(sh));
    flex4.extract(sizeof(buf2), (unsigned char *)buf2);
    TS_ASSERT_EQUALS(flex4.cardinal(), 100);
    for (int idx = 0; idx < flex4.cardinal(); ++idx)
    {
        TS_ASSERT_EQUALS(flex4[idx]->dbl1, dbl1[idx]);
        TS_ASSERT_EQUALS(flex4[idx]->dbl2, dbl2[idx]);
        TS_ASSERT_EQUALS(flex4[idx]->iv1, iv1[idx]);
        TS_ASSERT_EQUALS(flex4[idx]->iv2, iv2[idx]);
        TS_ASSERT_EQUALS(flex4[idx]->iv3, iv3[idx]);
    }
    memset(buf2a, 0, sizeof(buf2a));
    ob.set((unsigned char *)buf2a, sizeof(buf2a));
    flex4.output(ob);
    TS_ASSERT_EQUALS(ob.size(), sizeof(buf2));
    TS_ASSERT_SAME_DATA(buf2, buf2a, sizeof(buf2));
    r_flex_var<my_complex_var2> flex4b;
    flex4b = flex4;
    TS_ASSERT_EQUALS(flex4b.cardinal(), 100);
    for (int idx = 0; idx < flex4b.cardinal(); ++idx)
    {
        TS_ASSERT_EQUALS(flex4b[idx]->dbl1, dbl1[idx]);
        TS_ASSERT_EQUALS(flex4b[idx]->dbl2, dbl2[idx]);
        TS_ASSERT_EQUALS(flex4b[idx]->iv1, iv1[idx]);
        TS_ASSERT_EQUALS(flex4b[idx]->iv2, iv2[idx]);
        TS_ASSERT_EQUALS(flex4b[idx]->iv3, iv3[idx]);
    }

    ext_flex_var<my_complex_var1, 2, 2, 4> flex5;
    memset(buf1, 0, 4);
    memcpy(buf1 + 2, &sh, sizeof(sh));
    flex5.extract(sizeof(buf1), (unsigned char *)buf1);
    TS_ASSERT_EQUALS(flex5.cardinal(), 100);
    for (int idx = 0; idx < flex5.cardinal(); ++idx)
    {
        TS_ASSERT_EQUALS(flex5[idx]->dbl1, dbl1[idx]);
        TS_ASSERT_EQUALS(flex5[idx]->dbl2, dbl2[idx]);
        TS_ASSERT_EQUALS(flex5[idx]->iv1, iv1[idx]);
    }
    memset(buf1a, 0, sizeof(buf1a));
    ob.set((unsigned char *)buf1a, sizeof(buf1a));
    flex5.output(ob);
    TS_ASSERT_EQUALS(ob.size(), sizeof(buf1));
    TS_ASSERT_SAME_DATA(buf1, buf1a, sizeof(buf1));

    short shr = rb<short>(100);
    ext_flex_var<my_complex_var2, -2, 0, 4> flex6;
    memset(buf2, 0, 4);
    memcpy(buf2, &shr, sizeof(shr));
    flex6.extract(sizeof(buf2), (unsigned char *)buf2);
    TS_ASSERT_EQUALS(flex6.cardinal(), 100);
    for (int idx = 0; idx < flex6.cardinal(); ++idx)
    {
        TS_ASSERT_EQUALS(flex6[idx]->dbl1, dbl1[idx]);
        TS_ASSERT_EQUALS(flex6[idx]->dbl2, dbl2[idx]);
        TS_ASSERT_EQUALS(flex6[idx]->iv1, iv1[idx]);
        TS_ASSERT_EQUALS(flex6[idx]->iv2, iv2[idx]);
        TS_ASSERT_EQUALS(flex6[idx]->iv3, iv3[idx]);
    }
    memset(buf2a, 0, sizeof(buf2a));
    ob.set((unsigned char *)buf2a, sizeof(buf2a));
    flex6.output(ob);
    TS_ASSERT_EQUALS(ob.size(), sizeof(buf2));
    TS_ASSERT_SAME_DATA(buf2, buf2a, sizeof(buf2));

    ext_flex_var<my_complex_var1, -2, 1, 4> flex7;
    memset(buf1, 0, 4);
    memcpy(buf1 + 1, &shr, sizeof(shr));
    flex7.extract(sizeof(buf1), (unsigned char *)buf1);
    TS_ASSERT_EQUALS(flex7.cardinal(), 100);
    for (int idx = 0; idx < flex7.cardinal(); ++idx)
    {
        TS_ASSERT_EQUALS(flex7[idx]->dbl1, dbl1[idx]);
        TS_ASSERT_EQUALS(flex7[idx]->dbl2, dbl2[idx]);
        TS_ASSERT_EQUALS(flex7[idx]->iv1, iv1[idx]);
    }
    memset(buf1a, 0, sizeof(buf1a));
    ob.set((unsigned char *)buf1a, sizeof(buf1a));
    flex7.output(ob);
    TS_ASSERT_EQUALS(ob.size(), sizeof(buf1));
    TS_ASSERT_SAME_DATA(buf1, buf1a, sizeof(buf1));

    ext_flex_var<my_complex_var2, -2, 2, 4> flex8;
    memset(buf2, 0, 4);
    memcpy(buf2 + 2, &shr, sizeof(shr));
    flex8.extract(sizeof(buf2), (unsigned char *)buf2);
    TS_ASSERT_EQUALS(flex8.cardinal(), 100);
    for (int idx = 0; idx < flex8.cardinal(); ++idx)
    {
        TS_ASSERT_EQUALS(flex8[idx]->dbl1, dbl1[idx]);
        TS_ASSERT_EQUALS(flex8[idx]->dbl2, dbl2[idx]);
        TS_ASSERT_EQUALS(flex8[idx]->iv1, iv1[idx]);
        TS_ASSERT_EQUALS(flex8[idx]->iv2, iv2[idx]);
        TS_ASSERT_EQUALS(flex8[idx]->iv3, iv3[idx]);
    }
    memset(buf2a, 0, sizeof(buf2a));
    ob.set((unsigned char *)buf2a, sizeof(buf2a));
    flex8.output(ob);
    TS_ASSERT_EQUALS(ob.size(), sizeof(buf2));
    TS_ASSERT_SAME_DATA(buf2, buf2a, sizeof(buf2));

    int itr = rb<int>(100);
    r_flex_var<my_complex_var1> flex9;
    memset(buf1, 0, 4);
    memcpy(buf1, &itr, sizeof(itr));
    flex9.extract(sizeof(buf1), (unsigned char *)buf1);
    TS_ASSERT_EQUALS(flex9.cardinal(), 100);
    for (int idx = 0; idx < flex9.cardinal(); ++idx)
    {
        TS_ASSERT_EQUALS(flex9[idx]->dbl1, dbl1[idx]);
        TS_ASSERT_EQUALS(flex9[idx]->dbl2, dbl2[idx]);
        TS_ASSERT_EQUALS(flex9[idx]->iv1, iv1[idx]);
    }
    memset(buf1a, 0, sizeof(buf1a));
    ob.set((unsigned char *)buf1a, sizeof(buf1a));
    flex9.output(ob);
    TS_ASSERT_EQUALS(ob.size(), sizeof(buf1));
    TS_ASSERT_SAME_DATA(buf1, buf1a, sizeof(buf1));

    char buf3[8 + (8 + 8 + 4) * 20];
    char buf3a[8 + (8 + 8 + 4) * 20];

    ob.set((unsigned char *)buf3 + 8, sizeof(buf3) - 8);
    for (int idx = 0; idx < 20; ++idx)
    {
        generic_var<double> d1(dbl1[idx]);
        d1.output(ob);
        generic_var<double> d2(dbl2[idx]);
        d2.output(ob);
        generic_var<int> d3(iv1[idx]);
        d3.output(ob);
    }

    long long ll = 20;
    ext_flex_var<my_complex_var1, 8, 0, 8> flex10;
    memcpy(buf3, &ll, 8);
    flex10.extract(sizeof(buf3), (unsigned char *)buf3);
    TS_ASSERT_EQUALS(flex10.cardinal(), 20);
    for (int idx = 0; idx < flex10.cardinal(); ++idx)
    {
        TS_ASSERT_EQUALS(flex10[idx]->dbl1, dbl1[idx]);
        TS_ASSERT_EQUALS(flex10[idx]->dbl2, dbl2[idx]);
        TS_ASSERT_EQUALS(flex10[idx]->iv1, iv1[idx]);
    }
    memset(buf3a, 0, sizeof(buf3a));
    ob.set((unsigned char *)buf3a, sizeof(buf3a));
    flex10.output(ob);
    TS_ASSERT_EQUALS(ob.size(), sizeof(buf3));
    TS_ASSERT_SAME_DATA(buf3, buf3a, sizeof(buf3));

    ll = rb(20LL);
    ext_flex_var<my_complex_var1, -8, 0, 8> flex11;
    memcpy(buf3, &ll, 8);
    flex11.extract(sizeof(buf3), (unsigned char *)buf3);
    TS_ASSERT_EQUALS(flex11.cardinal(), 20);
    for (int idx = 0; idx < flex11.cardinal(); ++idx)
    {
        TS_ASSERT_EQUALS(flex11[idx]->dbl1, dbl1[idx]);
        TS_ASSERT_EQUALS(flex11[idx]->dbl2, dbl2[idx]);
        TS_ASSERT_EQUALS(flex11[idx]->iv1, iv1[idx]);
    }
    memset(buf3a, 0, sizeof(buf3a));
    ob.set((unsigned char *)buf3a, sizeof(buf3a));
    flex11.output(ob);
    TS_ASSERT_EQUALS(ob.size(), sizeof(buf3));
    TS_ASSERT_SAME_DATA(buf3, buf3a, sizeof(buf3));

    ext_flex_var<my_complex_var1, 0, 0, 0> flex12;
    flex12.extract(sizeof(buf3)-8, (unsigned char *)buf3+8);
    TS_ASSERT_EQUALS(flex12.cardinal(), 20);
    for (int idx = 0; idx < flex12.cardinal(); ++idx)
    {
        TS_ASSERT_EQUALS(flex12[idx]->dbl1, dbl1[idx]);
        TS_ASSERT_EQUALS(flex12[idx]->dbl2, dbl2[idx]);
        TS_ASSERT_EQUALS(flex12[idx]->iv1, iv1[idx]);
    }
    memset(buf3a, 0, sizeof(buf3a));
    ob.set((unsigned char *)buf3a, sizeof(buf3a));
    flex12.output(ob);
    TS_ASSERT_EQUALS(ob.size(), sizeof(buf3)-8);
    TS_ASSERT_SAME_DATA(buf3+8, buf3a, sizeof(buf3)-8);

    TS_TRACE("End");
}

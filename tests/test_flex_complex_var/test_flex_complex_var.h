#ifndef _TEST_FLEX_COMPLEX_VAR_H_DEF_
#define _TEST_FLEX_COMPLEX_VAR_H_DEF_

#include <cxxtest/TestSuite.h>

class FlexComplexVarTestSuite : public CxxTest::TestSuite
{
public:
    void testComplex(void);
    void testFlexAdd(void);
    void testFlexInsert(void);
    void testFlexRemove(void);
    void testFlexResize(void);
    void testFlexExtract(void);       
    void testFlexOutput(void); 
    void testFlexEqualOperator(void);
    void testFlexDblEqualOperator(void);
    void testFlexStreamOperator(void); 
    void testFlexGetNext(void); 
    void testExtFlexExtract(void);       
};

#endif


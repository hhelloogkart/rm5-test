/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_flex_complex_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_FlexComplexVarTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_flex_complex_var\test_flex_complex_var.h"

static FlexComplexVarTestSuite suite_FlexComplexVarTestSuite;

static CxxTest::List Tests_FlexComplexVarTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_FlexComplexVarTestSuite( "test_flex_complex_var/test_flex_complex_var.h", 6, "FlexComplexVarTestSuite", suite_FlexComplexVarTestSuite, Tests_FlexComplexVarTestSuite );

static class TestDescription_suite_FlexComplexVarTestSuite_testComplex : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FlexComplexVarTestSuite_testComplex() : CxxTest::RealTestDescription( Tests_FlexComplexVarTestSuite, suiteDescription_FlexComplexVarTestSuite, 9, "testComplex" ) {}
 void runTest() { suite_FlexComplexVarTestSuite.testComplex(); }
} testDescription_suite_FlexComplexVarTestSuite_testComplex;

static class TestDescription_suite_FlexComplexVarTestSuite_testFlexAdd : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FlexComplexVarTestSuite_testFlexAdd() : CxxTest::RealTestDescription( Tests_FlexComplexVarTestSuite, suiteDescription_FlexComplexVarTestSuite, 10, "testFlexAdd" ) {}
 void runTest() { suite_FlexComplexVarTestSuite.testFlexAdd(); }
} testDescription_suite_FlexComplexVarTestSuite_testFlexAdd;

static class TestDescription_suite_FlexComplexVarTestSuite_testFlexInsert : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FlexComplexVarTestSuite_testFlexInsert() : CxxTest::RealTestDescription( Tests_FlexComplexVarTestSuite, suiteDescription_FlexComplexVarTestSuite, 11, "testFlexInsert" ) {}
 void runTest() { suite_FlexComplexVarTestSuite.testFlexInsert(); }
} testDescription_suite_FlexComplexVarTestSuite_testFlexInsert;

static class TestDescription_suite_FlexComplexVarTestSuite_testFlexRemove : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FlexComplexVarTestSuite_testFlexRemove() : CxxTest::RealTestDescription( Tests_FlexComplexVarTestSuite, suiteDescription_FlexComplexVarTestSuite, 12, "testFlexRemove" ) {}
 void runTest() { suite_FlexComplexVarTestSuite.testFlexRemove(); }
} testDescription_suite_FlexComplexVarTestSuite_testFlexRemove;

static class TestDescription_suite_FlexComplexVarTestSuite_testFlexResize : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FlexComplexVarTestSuite_testFlexResize() : CxxTest::RealTestDescription( Tests_FlexComplexVarTestSuite, suiteDescription_FlexComplexVarTestSuite, 13, "testFlexResize" ) {}
 void runTest() { suite_FlexComplexVarTestSuite.testFlexResize(); }
} testDescription_suite_FlexComplexVarTestSuite_testFlexResize;

static class TestDescription_suite_FlexComplexVarTestSuite_testFlexExtract : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FlexComplexVarTestSuite_testFlexExtract() : CxxTest::RealTestDescription( Tests_FlexComplexVarTestSuite, suiteDescription_FlexComplexVarTestSuite, 14, "testFlexExtract" ) {}
 void runTest() { suite_FlexComplexVarTestSuite.testFlexExtract(); }
} testDescription_suite_FlexComplexVarTestSuite_testFlexExtract;

static class TestDescription_suite_FlexComplexVarTestSuite_testFlexOutput : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FlexComplexVarTestSuite_testFlexOutput() : CxxTest::RealTestDescription( Tests_FlexComplexVarTestSuite, suiteDescription_FlexComplexVarTestSuite, 15, "testFlexOutput" ) {}
 void runTest() { suite_FlexComplexVarTestSuite.testFlexOutput(); }
} testDescription_suite_FlexComplexVarTestSuite_testFlexOutput;

static class TestDescription_suite_FlexComplexVarTestSuite_testFlexEqualOperator : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FlexComplexVarTestSuite_testFlexEqualOperator() : CxxTest::RealTestDescription( Tests_FlexComplexVarTestSuite, suiteDescription_FlexComplexVarTestSuite, 16, "testFlexEqualOperator" ) {}
 void runTest() { suite_FlexComplexVarTestSuite.testFlexEqualOperator(); }
} testDescription_suite_FlexComplexVarTestSuite_testFlexEqualOperator;

static class TestDescription_suite_FlexComplexVarTestSuite_testFlexDblEqualOperator : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FlexComplexVarTestSuite_testFlexDblEqualOperator() : CxxTest::RealTestDescription( Tests_FlexComplexVarTestSuite, suiteDescription_FlexComplexVarTestSuite, 17, "testFlexDblEqualOperator" ) {}
 void runTest() { suite_FlexComplexVarTestSuite.testFlexDblEqualOperator(); }
} testDescription_suite_FlexComplexVarTestSuite_testFlexDblEqualOperator;

static class TestDescription_suite_FlexComplexVarTestSuite_testFlexStreamOperator : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FlexComplexVarTestSuite_testFlexStreamOperator() : CxxTest::RealTestDescription( Tests_FlexComplexVarTestSuite, suiteDescription_FlexComplexVarTestSuite, 18, "testFlexStreamOperator" ) {}
 void runTest() { suite_FlexComplexVarTestSuite.testFlexStreamOperator(); }
} testDescription_suite_FlexComplexVarTestSuite_testFlexStreamOperator;

static class TestDescription_suite_FlexComplexVarTestSuite_testFlexGetNext : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FlexComplexVarTestSuite_testFlexGetNext() : CxxTest::RealTestDescription( Tests_FlexComplexVarTestSuite, suiteDescription_FlexComplexVarTestSuite, 19, "testFlexGetNext" ) {}
 void runTest() { suite_FlexComplexVarTestSuite.testFlexGetNext(); }
} testDescription_suite_FlexComplexVarTestSuite_testFlexGetNext;

static class TestDescription_suite_FlexComplexVarTestSuite_testExtFlexExtract : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FlexComplexVarTestSuite_testExtFlexExtract() : CxxTest::RealTestDescription( Tests_FlexComplexVarTestSuite, suiteDescription_FlexComplexVarTestSuite, 20, "testExtFlexExtract" ) {}
 void runTest() { suite_FlexComplexVarTestSuite.testExtFlexExtract(); }
} testDescription_suite_FlexComplexVarTestSuite_testExtFlexExtract;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

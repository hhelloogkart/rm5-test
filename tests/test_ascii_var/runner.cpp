/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_ascii_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_AsciiVarTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_ascii_var\test_ascii_var.h"

static AsciiVarTestSuite suite_AsciiVarTestSuite;

static CxxTest::List Tests_AsciiVarTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_AsciiVarTestSuite( "test_ascii_var/test_ascii_var.h", 6, "AsciiVarTestSuite", suite_AsciiVarTestSuite, Tests_AsciiVarTestSuite );

static class TestDescription_suite_AsciiVarTestSuite_testAscii : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_AsciiVarTestSuite_testAscii() : CxxTest::RealTestDescription( Tests_AsciiVarTestSuite, suiteDescription_AsciiVarTestSuite, 9, "testAscii" ) {}
 void runTest() { suite_AsciiVarTestSuite.testAscii(); }
} testDescription_suite_AsciiVarTestSuite_testAscii;

static class TestDescription_suite_AsciiVarTestSuite_testResize : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_AsciiVarTestSuite_testResize() : CxxTest::RealTestDescription( Tests_AsciiVarTestSuite, suiteDescription_AsciiVarTestSuite, 10, "testResize" ) {}
 void runTest() { suite_AsciiVarTestSuite.testResize(); }
} testDescription_suite_AsciiVarTestSuite_testResize;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

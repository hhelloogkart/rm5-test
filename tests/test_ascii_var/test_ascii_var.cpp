#include <stdio.h>
#include <ascii_var.h>
#include <gen_var.h>
#include <str_var.h>
#include "test_ascii_var.h"
#include <string.h>

class my_type : public ascii_var
{
public:
    ASCIICONST(my_type,"*,f4*a*b*c*5f4*,f8*,d1*,d2*,d4*0x4*")
    generic_var<float> flt1;
    generic_var<float> flt2;
    generic_var<double> dbl;
    generic_var<char>  iv1;
    generic_var<short> iv2;
    generic_var<int>   iv3;
    generic_var<unsigned int> iv4;
};

class my_type2 : public ascii_var
{
public:
    ASCIICONST(my_type2,"*\\x3c*d*a*t*a*\\x20*f*\\x31*=*\\x22*\\x22f8*\\x20*f*\\x32*=*\\x22*\\x22f8*/*\\x3e*")
    generic_var<double> dbl1;
    generic_var<double> dbl2;
};

class my_type3 : public ascii_var
{
public:
    ASCIICONST(my_type3,"*2d2*:*2d2*:*2d2*,*8f4*,*4f4*,*12f8*,*6f8*,*4d2*,*4d2*,*3d1*,*3d1*,*6d4*,*6d4*")
    generic_var<short> sh1[3];
    generic_var<float> fl[2];
    generic_var<double> dbl[2];
    generic_var<short> sh2[2];
    generic_var<char> ch[2];
    generic_var<int> in[2];
};

class my_type4 : public ascii_var
{
public:
    ASCIICONST(my_type4,"|,u1|,u2|,u4|,x1|,x2|,x4|0s50|")
    generic_var<unsigned char> uch1;
    generic_var<unsigned short> ush1;
    generic_var<unsigned int> uit1;
    generic_var<unsigned char> uch2;
    generic_var<unsigned short> ush2;
    generic_var<unsigned int> uit2;
    string_var<50> str;
};

class my_type5 : public ascii_var {
public:
    ASCIICONST(my_type5, "!10u1!10u2!10u4!10x1!10x2!10x4!10f4!15f8!")
    generic_var<unsigned char> uch1;
    generic_var<unsigned short> ush1;
    generic_var<unsigned int> uit1;
    generic_var<unsigned char> uch2;
    generic_var<unsigned short> ush2;
    generic_var<unsigned int> uit2;
    generic_var<float> flt;
    generic_var<double> dbl;
};

class my_type6 : public ascii_var {
public:
    ASCIICONST(my_type6, "#\\x0Af8#\\x0Af0#\\x0ad0#\\x0au0#\\x0ax0#10s10#0f8#")
    generic_var<double> dbl1;
    string_var<10> str;
    generic_var<double> dbl2;
};

class my_type7 : public ascii_var {
public:
    ASCIICONST(my_type7,"/3d1/3d2/3d4/3u1/3u2/3u4/2x1/2x2/2x4/")
    generic_var<char> sch;
    generic_var<short> ssh;
    generic_var<int> sit;
    generic_var<unsigned char> uch1;
    generic_var<unsigned short> ush1;
    generic_var<unsigned int> uit1;
    generic_var<unsigned char> uch2;
    generic_var<unsigned short> ush2;
    generic_var<unsigned int> uit2;
};

#define STRING1 "0---------0---------0---------0---------"

void AsciiVarTestSuite::testAscii(void)
{
    my_type test;
    my_type2 test2;
    my_type3 test3;
    my_type3 test4;
    my_type4 test5;
    my_type4 test6; // overflow
    my_type4 test7; // underflow / empty
    my_type5 test8;
    my_type6 test11;
    my_type7 test12;
    char ipbuf[] = "11.115,abc3.1422.225,33,44,55,42";
    char ipbuf2[] = "<data f1=\"36.4\" f2=\"1.5\"/>";
    char ipbuf3[] = "01:02:10,25.12344,69.5,1234.1234512,1234.1,0005,4321,055,110,000765,567890";
    char ipbuf4[] = "-6:-5:-4,-33.1025,-2.0,-152.9100000,-41.00,-006,-493,-09,-96,-00456,-93543";
    char ipbuf5[] = "135,43000,2791728742,8a,d752,9a655113," STRING1;
    char ipbuf8[] = "0000000200" "0000002000" "0000200000" "00000000d3" "0000000d33" "000000d333" "55.2500000" "55.250000000000";
    char ipbuf9[] = "XXXXXXXXXX" "XXXXXXXXXX" "XXXXXXXXXX" "XXXXXXXXXX" "XXXXXXXXXX" "XXXXXXXXXX" "XXXXXXXXXX" "XXXXXXXXXXXXXXX";
    char ipbuf10[] = "X,IGNOREDUNTILabc3.25067.75,X,X,X,X";
    char ipbuf11[] = "   123.456\n7.8\n90\n12\n3F\n     ABCDE345.678";
    char ipbuf11a[] = "123.456\n\n\n\n\n     ABCDE345.678";
    char ipbuf12[] = "100" "995" "998" "191" "789" "871" "8c" "d5" "ee";

    // Testing extract
    test.extract(strlen(ipbuf), reinterpret_cast<unsigned char *>(ipbuf));
    test2.extract(strlen(ipbuf2), reinterpret_cast<unsigned char *>(ipbuf2));
    test3.extract(strlen(ipbuf3), reinterpret_cast<unsigned char *>(ipbuf3));
    test4.extract(strlen(ipbuf4), reinterpret_cast<unsigned char *>(ipbuf4));
    test5.extract(strlen(ipbuf5), reinterpret_cast<unsigned char *>(ipbuf5));
    test8.extract(strlen(ipbuf8), reinterpret_cast<unsigned char *>(ipbuf8));
    test11.extract(strlen(ipbuf11), reinterpret_cast<unsigned char *>(ipbuf11));
    test12.extract(strlen(ipbuf12), reinterpret_cast<unsigned char *>(ipbuf12));

    TS_ASSERT_DELTA(test.flt1.to_float(), 11.115f, 0.0005);
    TS_ASSERT_DELTA(test.flt2.to_float(), 3.142f, 0.0005);
    TS_ASSERT_DELTA(test.dbl, 2.225, 0.000001);
    TS_ASSERT_EQUALS(test.iv1.to_char(), static_cast<char>(33));
    TS_ASSERT_EQUALS(test.iv2.to_short(), static_cast<short>(44));
    TS_ASSERT_EQUALS(test.iv3.to_int(), 55);
    TS_ASSERT_EQUALS(test.iv4.to_unsigned_int(), 66U);
    TS_ASSERT_DELTA(test2.dbl1, 36.4, 0.000001);
    TS_ASSERT_DELTA(test2.dbl2, 1.5, 0.000001);
    TS_ASSERT_EQUALS(test3.sh1[0].to_int(), 1);
    TS_ASSERT_EQUALS(test3.sh1[1].to_int(), 2);
    TS_ASSERT_EQUALS(test3.sh1[2].to_int(), 10);
    TS_ASSERT_EQUALS(test3.fl[0].to_float(), 25.12344f);
    TS_ASSERT_EQUALS(test3.fl[1].to_float(), 69.5f);
    TS_ASSERT_EQUALS(test3.dbl[0].to_double(), 1234.1234512);
    TS_ASSERT_EQUALS(test3.dbl[1].to_double(), 1234.1);
    TS_ASSERT_EQUALS(test3.sh2[0].to_int(), 5);
    TS_ASSERT_EQUALS(test3.sh2[1].to_int(), 4321);
    TS_ASSERT_EQUALS(test3.ch[0].to_int(), 55);
    TS_ASSERT_EQUALS(test3.ch[1].to_int(), 110);
    TS_ASSERT_EQUALS(test3.in[0].to_int(), 765);
    TS_ASSERT_EQUALS(test3.in[1].to_int(), 567890);
    TS_ASSERT_EQUALS(test4.sh1[0].to_int(), -6);
    TS_ASSERT_EQUALS(test4.sh1[1].to_int(), -5);
    TS_ASSERT_EQUALS(test4.sh1[2].to_int(), -4);
    TS_ASSERT_EQUALS(test4.fl[0].to_float(), -33.1025f);
    TS_ASSERT_EQUALS(test4.fl[1].to_float(), -2.f);
    TS_ASSERT_EQUALS(test4.dbl[0].to_double(), -152.91);
    TS_ASSERT_EQUALS(test4.dbl[1].to_double(), -41.0);
    TS_ASSERT_EQUALS(test4.sh2[0].to_int(), -6);
    TS_ASSERT_EQUALS(test4.sh2[1].to_int(), -493);
    TS_ASSERT_EQUALS(test4.ch[0].to_int(), -9);
    TS_ASSERT_EQUALS(test4.ch[1].to_int(), -96);
    TS_ASSERT_EQUALS(test4.in[0].to_int(), -456);
    TS_ASSERT_EQUALS(test4.in[1].to_int(), -93543);
    TS_ASSERT_EQUALS(test5.uch1, 135);
    TS_ASSERT_EQUALS(test5.ush1, 43000);
    TS_ASSERT_EQUALS(test5.uit1, 2791728742);
    TS_ASSERT_EQUALS(test5.uch2, 138);
    TS_ASSERT_EQUALS(test5.ush2, 55122);
    TS_ASSERT_EQUALS(test5.uit2, 2590331155);
    TS_ASSERT_SAME_DATA((const char *)test5.str, STRING1, strlen(STRING1));
    TS_ASSERT_EQUALS(test8.uch1, 200);
    TS_ASSERT_EQUALS(test8.ush1, 2000);
    TS_ASSERT_EQUALS(test8.uit1, 200000);
    TS_ASSERT_EQUALS(test8.uch2, 211);
    TS_ASSERT_EQUALS(test8.ush2, 3379);
    TS_ASSERT_EQUALS(test8.uit2, 54067);
    TS_ASSERT_EQUALS(test8.flt, 55.25f);
    TS_ASSERT_EQUALS(test8.dbl, 55.25);
    TS_ASSERT_EQUALS(test11.dbl1, 123.456);
    TS_ASSERT_EQUALS(test11.dbl2, 345.678);
    TS_ASSERT_SAME_DATA(test11.str.to_const_char_ptr(), "     ABCDE", 10);
    TS_ASSERT_EQUALS(test12.sch, 100);
    TS_ASSERT_EQUALS(test12.ssh, 995);
    TS_ASSERT_EQUALS(test12.sit, 998);
    TS_ASSERT_EQUALS(test12.uch1, 191);
    TS_ASSERT_EQUALS(test12.ush1, 789);
    TS_ASSERT_EQUALS(test12.uit1, 871);
    TS_ASSERT_EQUALS(test12.uch2, 0x8C);
    TS_ASSERT_EQUALS(test12.ush2, 0xD5);
    TS_ASSERT_EQUALS(test12.uit2, 0xEE);

    int sz = test.size();
    char *opbuf = new char[sz];
    outbuf obuf;
    obuf.set(reinterpret_cast<unsigned char *>(opbuf), sz);
    test.output(obuf);
    TS_ASSERT_SAME_DATA(opbuf, ipbuf, sz);
    delete [] opbuf;

    sz = test2.size();
    opbuf = new char[sz];
    obuf.set(reinterpret_cast<unsigned char *>(opbuf), sz);
    test2.output(obuf);
    TS_ASSERT_SAME_DATA(opbuf, ipbuf2, sz);
    delete [] opbuf;

    sz = test3.size();
    opbuf = new char[sz];
    obuf.set(reinterpret_cast<unsigned char *>(opbuf), sz);
    test3.output(obuf);
    TS_ASSERT_SAME_DATA(opbuf, ipbuf3, sz);

    obuf.set(reinterpret_cast<unsigned char *>(opbuf), sz);
    test4.output(obuf);
    TS_ASSERT_SAME_DATA(opbuf, ipbuf4, sz);
    delete [] opbuf;

    sz = test5.size();
    opbuf = new char[sz];
    obuf.set(reinterpret_cast<unsigned char *>(opbuf), sz);
    test5.output(obuf);
    TS_ASSERT_SAME_DATA(opbuf, ipbuf5, sz);
    delete[] opbuf;

    sz = test8.size();
    opbuf = new char[sz];
    obuf.set(reinterpret_cast<unsigned char *>(opbuf), sz);
    test8.output(obuf);
    TS_ASSERT_SAME_DATA(opbuf, ipbuf8, sz);
    delete[] opbuf;

    // Test string padding in front
    test11.str = "ABCDE"; // five spaces to be padded in front
    sz = test11.size();
    opbuf = new char[sz];
    obuf.set(reinterpret_cast<unsigned char *>(opbuf), sz);
    test11.output(obuf);
    TS_ASSERT_SAME_DATA(opbuf, ipbuf11a, sz);
    delete[] opbuf;

    // Invalid data
    test8.extract(strlen(ipbuf9), reinterpret_cast<unsigned char *>(ipbuf9));
    TS_ASSERT_EQUALS(test8.uch1, 0xFF);
    TS_ASSERT_EQUALS(test8.ush1, 0xFFFF);
    TS_ASSERT_EQUALS(test8.uit1, 0xFFFFFFFF);
    TS_ASSERT_EQUALS(test8.uch2, 0xFF);
    TS_ASSERT_EQUALS(test8.ush2, 0xFFFF);
    TS_ASSERT_EQUALS(test8.uit2, 0xFFFFFFFF);
    TS_ASSERT(!test8.flt.isValid());
    TS_ASSERT(!test8.dbl.isValid());

    test.extract(strlen(ipbuf10), reinterpret_cast<unsigned char *>(ipbuf10));
    TS_ASSERT(!test.flt1.isValid());
    TS_ASSERT_EQUALS(test.flt2, 3.25f);
    TS_ASSERT_EQUALS(test.dbl, 67.75);
    TS_ASSERT_EQUALS(test.iv1, -1);
    TS_ASSERT_EQUALS(test.iv2, -1);
    TS_ASSERT_EQUALS(test.iv3, -1);
    TS_ASSERT_EQUALS(test.iv4, 0xFFFFFFFF);

    // Case without the padding
    sz = test12.size();
    opbuf = new char[sz];
    obuf.set(reinterpret_cast<unsigned char *>(opbuf), sz);
    test12.output(obuf);
    TS_ASSERT_SAME_DATA(opbuf, ipbuf12, sz);
    delete[] opbuf;
}

void AsciiVarTestSuite::testResize()
{
    my_type test1;
    TS_ASSERT_EQUALS(test1.cardinal(), 7);
    test1.resize(7);
    TS_ASSERT_EQUALS(test1.cardinal(), 7);
    TS_TRACE("End");
}

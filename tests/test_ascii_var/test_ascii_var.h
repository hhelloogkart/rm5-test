#ifndef _TEST_ASCII_VAR_H_DEF_
#define _TEST_ASCII_VAR_H_DEF_

#include <cxxtest/TestSuite.h>

class AsciiVarTestSuite : public CxxTest::TestSuite
{
public:
    void testAscii(void);
    void testResize();

};

#endif

/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_chk_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_ChkVarTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_chk_var\test_chk_var.h"

static ChkVarTestSuite suite_ChkVarTestSuite;

static CxxTest::List Tests_ChkVarTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_ChkVarTestSuite( "test_chk_var/test_chk_var.h", 6, "ChkVarTestSuite", suite_ChkVarTestSuite, Tests_ChkVarTestSuite );

static class TestDescription_suite_ChkVarTestSuite_testChk : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ChkVarTestSuite_testChk() : CxxTest::RealTestDescription( Tests_ChkVarTestSuite, suiteDescription_ChkVarTestSuite, 10, "testChk" ) {}
 void runTest() { suite_ChkVarTestSuite.testChk(); }
} testDescription_suite_ChkVarTestSuite_testChk;

static class TestDescription_suite_ChkVarTestSuite_testCorrupt : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ChkVarTestSuite_testCorrupt() : CxxTest::RealTestDescription( Tests_ChkVarTestSuite, suiteDescription_ChkVarTestSuite, 11, "testCorrupt" ) {}
 void runTest() { suite_ChkVarTestSuite.testCorrupt(); }
} testDescription_suite_ChkVarTestSuite_testCorrupt;

static class TestDescription_suite_ChkVarTestSuite_testNest : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ChkVarTestSuite_testNest() : CxxTest::RealTestDescription( Tests_ChkVarTestSuite, suiteDescription_ChkVarTestSuite, 12, "testNest" ) {}
 void runTest() { suite_ChkVarTestSuite.testNest(); }
} testDescription_suite_ChkVarTestSuite_testNest;

static class TestDescription_suite_ChkVarTestSuite_testChksumInHeader : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ChkVarTestSuite_testChksumInHeader() : CxxTest::RealTestDescription( Tests_ChkVarTestSuite, suiteDescription_ChkVarTestSuite, 13, "testChksumInHeader" ) {}
 void runTest() { suite_ChkVarTestSuite.testChksumInHeader(); }
} testDescription_suite_ChkVarTestSuite_testChksumInHeader;

static class TestDescription_suite_ChkVarTestSuite_testMisc : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_ChkVarTestSuite_testMisc() : CxxTest::RealTestDescription( Tests_ChkVarTestSuite, suiteDescription_ChkVarTestSuite, 14, "testMisc" ) {}
 void runTest() { suite_ChkVarTestSuite.testMisc(); }
} testDescription_suite_ChkVarTestSuite_testMisc;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

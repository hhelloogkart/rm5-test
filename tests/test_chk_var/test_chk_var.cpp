#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <gen_var.h>
#include <arry_var.h>
#include <str_var.h>
#include <chksum_var.h>
#include <mhash_var.h>
#include "test_chk_var.h"

// Additional typedef to settle coverage for mash_le_int
typedef v_mhash_le_int_var<mhash_var::MHASH_CRC32, 1, 0, 0> le_crc32_var;

#define DATA_SZ	256
#define FDATA_SZ 25

#define DEFINEVAR(t) \
struct t##_typ : public t##_var \
{ \
    CHKSUMCONST( t##_typ, t##_var ) \
    generic_var<unsigned short> a[DATA_SZ]; \
    array_var<double> b; \
    string_var<5> c; \
}; \
t##_typ t, m_##t; \
struct t##2_typ : public t##_var \
{ \
    CHKSUMCONST( t##2_typ, t##_var ) \
    generic_var<double> a; \
    generic_var<unsigned int> b; \
    generic_var<float> c; \
    generic_var<unsigned short> d; \
    generic_var<unsigned char> e; \
    generic_var<char> f; \
    generic_var<short> g; \
    generic_var<int> h; \
    string_var<22> i; \
}; \
t##2_typ t##_2, m2_##t;

DEFINEVAR(chksum8);
DEFINEVAR(chksum16le);
DEFINEVAR(chksum16be);
DEFINEVAR(chksum32le);
DEFINEVAR(chksum32be);
DEFINEVAR(xsens_chksum8);
DEFINEVAR(xor_chksum8);
DEFINEVAR(nmea_xor_chksum8);
DEFINEVAR(xor_split_chksum16);
DEFINEVAR(two_complement_chksum8);
DEFINEVAR(ig500_chksum16);
DEFINEVAR(bus1553_chksum16le);
DEFINEVAR(fas_chksum16);
DEFINEVAR(xor_fogchksum16le);
DEFINEVAR(xor_fogchksum16be);
DEFINEVAR(ublox_chksum);
DEFINEVAR(pre_chksum8);
DEFINEVAR(pre_chksum16le);
DEFINEVAR(pre_chksum16be);
DEFINEVAR(pre_chksum32le);
DEFINEVAR(pre_chksum32be);
DEFINEVAR(pre_chksum32to8);
DEFINEVAR(rdr_atp_chksum);

DEFINEVAR(crc16);
DEFINEVAR(crc16b);
DEFINEVAR(crc32);
DEFINEVAR(crc32b);
DEFINEVAR(crc32b_span);
DEFINEVAR(md5);
DEFINEVAR(sha1);
DEFINEVAR(sha512);
DEFINEVAR(sha3);

DEFINEVAR(be_crc32);
DEFINEVAR(le_crc32);
DEFINEVAR(ccitt_chksum16le);
DEFINEVAR(complement_chksum8);
DEFINEVAR(chksum16le_atp);
DEFINEVAR(chksum32le_atp);

#define DEFINETEMPLATEDCHKSUMVAR(t,cls,loc,front,back) \
struct t##_3_typ : public chksum_var \
{ \
    EXTCHKSUMCONST( t##_3_typ,cls,loc,front,back ) \
    generic_var<double> a; \
    generic_var<unsigned int> b; \
    generic_var<float> c; \
    generic_var<unsigned short> d; \
    generic_var<unsigned char> e; \
    generic_var<char> f; \
    generic_var<short> g; \
    generic_var<int> h; \
    string_var<22> i; \
} t##_3[2];

DEFINETEMPLATEDCHKSUMVAR(chksum8, chksum_var::CHKSUM_8, 3, 4, 0);
DEFINETEMPLATEDCHKSUMVAR(chksum16, chksum_var::CHKSUM_16, 3, 4, 0);
DEFINETEMPLATEDCHKSUMVAR(chksum16b, chksum_var::CHKSUM_16b, 3, 4, 0);
DEFINETEMPLATEDCHKSUMVAR(chksum16_ccitt, chksum_var::CHKSUM_16_CCITT, 3, 4, 0);
DEFINETEMPLATEDCHKSUMVAR(chksum32, chksum_var::CHKSUM_32, 3, 4, 0);
DEFINETEMPLATEDCHKSUMVAR(chksum32b, chksum_var::CHKSUM_32b, 3, 4, 0);

#define DEFINETEMPLATEDMHASHVAR(t,cls,loc,front,back) \
struct t##_3_typ : public mhash_var \
{ \
    EXTMHASHCONST( t##_3_typ,cls,loc,front,back ) \
    generic_var<double> a; \
    generic_var<unsigned int> b; \
    generic_var<float> c; \
    generic_var<unsigned short> d; \
    generic_var<unsigned char> e; \
    generic_var<char> f; \
    generic_var<short> g; \
    generic_var<int> h; \
    string_var<22> i; \
} t##_3[2];

DEFINETEMPLATEDMHASHVAR(crc16, mhash_var::MHASH_CRC16, 3, 4, 0);
DEFINETEMPLATEDMHASHVAR(crc16b, mhash_var::MHASH_CRC16B, 3, 4, 0);
DEFINETEMPLATEDMHASHVAR(crc32, mhash_var::MHASH_CRC32, 3, 4, 0);
DEFINETEMPLATEDMHASHVAR(crc32b, mhash_var::MHASH_CRC32B, 3, 4, 0);
DEFINETEMPLATEDMHASHVAR(md5, mhash_var::MHASH_MD5, 3, 4, 0);
DEFINETEMPLATEDMHASHVAR(sha1, mhash_var::MHASH_SHA1, 3, 4, 0);
DEFINETEMPLATEDMHASHVAR(sha512, mhash_var::MHASH_SHA512, 3, 4, 0);
DEFINETEMPLATEDMHASHVAR(sha3, mhash_var::MHASH_SHA3, 3, 4, 0);

#define DEFINETEMPLATEDMHASHINTVAR(t, ht, cl, front, back) \
struct t##_4_typ : public mhash_be_int_var \
{ \
    t##_4_typ(const t##_4_typ& arg) : mhash_be_int_var(arg) \
    { \
        child_copy_constructor(arg); \
    } \
    t##_4_typ() : mhash_be_int_var(ht, cl, front, back) \
    { \
        pop(); \
    } \
    const t##_4_typ& operator=(const t##_4_typ& right) \
    { \
        mhash_be_int_var::operator=(right); \
        return *this; \
    } \
    const mpt_var& operator=(const mpt_var& right) \
    { \
        mhash_be_int_var::operator=(right); \
        return *this; \
    } \
    virtual mpt_var* getNext() \
    { \
        return this + 1;\
    } \
    RUNTIME_TYPE(t##_4_typ) \
    generic_var<double> a; \
    generic_var<unsigned int> b; \
    generic_var<float> c; \
    generic_var<unsigned short> d; \
    generic_var<unsigned char> e; \
    generic_var<char> f; \
    generic_var<short> g; \
    generic_var<int> h; \
    string_var<22> i; \
} t##_4, m4_##t; \
struct t##_5_typ : public mhash_le_int_var \
{ \
    t##_5_typ(const t##_5_typ& arg) : mhash_le_int_var(arg) \
    { \
        child_copy_constructor(arg); \
    } \
    t##_5_typ() : mhash_le_int_var(ht, cl, front, back) \
    { \
        pop(); \
    } \
    const t##_5_typ& operator=(const t##_5_typ& right) \
    { \
        mhash_le_int_var::operator=(right); \
        return *this; \
    } \
    const mpt_var& operator=(const mpt_var& right) \
    { \
        mhash_le_int_var::operator=(right); \
        return *this; \
    } \
    virtual mpt_var* getNext() \
    { \
        return this + 1;\
    } \
    RUNTIME_TYPE(t##_5_typ) \
    generic_var<double> a; \
    generic_var<unsigned int> b; \
    generic_var<float> c; \
    generic_var<unsigned short> d; \
    generic_var<unsigned char> e; \
    generic_var<char> f; \
    generic_var<short> g; \
    generic_var<int> h; \
    string_var<22> i; \
} t##_5, m5_##t;

DEFINETEMPLATEDMHASHINTVAR(crc16, mhash_var::MHASH_CRC16, 1, 0, 0);
DEFINETEMPLATEDMHASHINTVAR(crc16b, mhash_var::MHASH_CRC16B, 1, 0, 0);
DEFINETEMPLATEDMHASHINTVAR(crc32, mhash_var::MHASH_CRC32, 1, 0, 0);
DEFINETEMPLATEDMHASHINTVAR(crc32b, mhash_var::MHASH_CRC32B, 1, 0, 0);
DEFINETEMPLATEDMHASHINTVAR(md5, mhash_var::MHASH_MD5, 1, 0, 0);
DEFINETEMPLATEDMHASHINTVAR(sha1, mhash_var::MHASH_SHA1, 1, 0, 0);
DEFINETEMPLATEDMHASHINTVAR(sha512, mhash_var::MHASH_SHA512, 1, 0, 0);
DEFINETEMPLATEDMHASHINTVAR(sha3, mhash_var::MHASH_SHA3, 1, 0, 0);

struct c1_typ : public complex_var
{
    COMPLEXCONST( c1_typ )
    generic_var<int> a;
    md5_typ b;
};
c1_typ c1, m_c1;

chk_var *var[] =
{
    &chksum8,
    &chksum16le,
    &chksum16be,
    &chksum32le,
    &chksum32be,
    &xsens_chksum8,
    &xor_chksum8,
    &nmea_xor_chksum8,
    &xor_split_chksum16,
    &two_complement_chksum8,
    &ig500_chksum16,
    &bus1553_chksum16le,
    &fas_chksum16,
    &xor_fogchksum16le,
    &xor_fogchksum16be,
    &ublox_chksum,
    &pre_chksum8,
    &pre_chksum16le,
    &pre_chksum16be,
    &pre_chksum32le,
    &pre_chksum32be,
    &pre_chksum32to8,
    &rdr_atp_chksum,
    &crc16,
    &crc16b,
    &crc32,
    &crc32b,
    &md5,
    &sha1,
    &sha512,
    &sha3,
    &be_crc32,
    &le_crc32,
    &ccitt_chksum16le,
    &complement_chksum8
};
chk_var *mirror[] =
{
    &m_chksum8,
    &m_chksum16le,
    &m_chksum16be,
    &m_chksum32le,
    &m_chksum32be,
    &m_xsens_chksum8,
    &m_xor_chksum8,
    &m_nmea_xor_chksum8,
    &m_xor_split_chksum16,
    &m_two_complement_chksum8,
    &m_ig500_chksum16,
    &m_bus1553_chksum16le,
    &m_fas_chksum16,
    &m_xor_fogchksum16le,
    &m_xor_fogchksum16be,
    &m_ublox_chksum,
    &m_pre_chksum8,
    &m_pre_chksum16le,
    &m_pre_chksum16be,
    &m_pre_chksum32le,
    &m_pre_chksum32be,
    &m_pre_chksum32to8,
    &m_rdr_atp_chksum,
    &m_crc16,
    &m_crc16b,
    &m_crc32,
    &m_crc32b,
    &m_md5,
    &m_sha1,
    &m_sha512,
    &m_sha3,
    &m_be_crc32,
    &m_le_crc32,
    &m_ccitt_chksum16le,
    &m_complement_chksum8
};

chk_var *var2[] =
{
    &chksum8_2,
    &chksum16le_2,
    &chksum16be_2,
    &chksum32le_2,
    &chksum32be_2,
    &xsens_chksum8_2,
    &xor_chksum8_2,
    &nmea_xor_chksum8_2,
    &xor_split_chksum16_2,
    &two_complement_chksum8_2,
    &ig500_chksum16_2,
    &bus1553_chksum16le_2,
    &fas_chksum16_2,
    &xor_fogchksum16le_2,
    &xor_fogchksum16be_2,
    &ublox_chksum_2,
    &pre_chksum8_2,
    &pre_chksum16le_2,
    &pre_chksum16be_2,
    &pre_chksum32le_2,
    &pre_chksum32be_2,
    &pre_chksum32to8_2,
    &rdr_atp_chksum_2,
    &crc16_2,
    &crc16b_2,
    &crc32_2,
    &crc32b_2,
    &md5_2,
    &sha1_2,
    &sha512_2,
    &sha3_2,
    &be_crc32_2,
    &le_crc32_2,
    &ccitt_chksum16le_2,
    &complement_chksum8_2,
    &chksum16le_atp_2,
    &chksum32le_atp_2,
    &crc16_4,
    &crc16b_4,
    &crc32_4,
    &crc32b_4,
    &md5_4,
    &sha1_4,
    &sha512_4,
    &crc16_5,
    &crc16b_5,
    &crc32_5,
    &crc32b_5,
    &md5_5,
    &sha1_5,
    &sha512_5
};
chk_var *mirror2[] =
{
    &m2_chksum8,
    &m2_chksum16le,
    &m2_chksum16be,
    &m2_chksum32le,
    &m2_chksum32be,
    &m2_xsens_chksum8,
    &m2_xor_chksum8,
    &m2_nmea_xor_chksum8,
    &m2_xor_split_chksum16,
    &m2_two_complement_chksum8,
    &m2_ig500_chksum16,
    &m2_bus1553_chksum16le,
    &m2_fas_chksum16,
    &m2_xor_fogchksum16le,
    &m2_xor_fogchksum16be,
    &m2_ublox_chksum,
    &m2_pre_chksum8,
    &m2_pre_chksum16le,
    &m2_pre_chksum16be,
    &m2_pre_chksum32le,
    &m2_pre_chksum32be,
    &m2_pre_chksum32to8,
    &m2_rdr_atp_chksum,
    &m2_crc16,
    &m2_crc16b,
    &m2_crc32,
    &m2_crc32b,
    &m2_md5,
    &m2_sha1,
    &m2_sha512,
    &m2_sha3,
    &m2_be_crc32,
    &m2_le_crc32,
    &m2_ccitt_chksum16le,
    &m2_complement_chksum8,
    &m2_chksum16le_atp,
    &m2_chksum32le_atp,
    &m4_crc16,
    &m4_crc16b,
    &m4_crc32,
    &m4_crc32b,
    &m4_md5,
    &m4_sha1,
    &m4_sha512,
    &m5_crc16,
    &m5_crc16b,
    &m5_crc32,
    &m5_crc32b,
    &m5_md5,
    &m5_sha1,
    &m5_sha512
};

chk_var *var3[] =
{
    &chksum8_3[0],
    &chksum16_3[0],
    &chksum16b_3[0],
    &chksum16_ccitt_3[0],
    &chksum32_3[0],
    &chksum32b_3[0],
    &crc16_3[0],
    &crc16b_3[0],
    &crc32_3[0],
    &crc32b_3[0],
    &md5_3[0],
    &sha1_3[0],
    &sha512_3[0],
    &sha3_3[0]
};

chk_var *mirror3[] =
{
    &chksum8_3[1],
    &chksum16_3[1],
    &chksum16b_3[1],
    &chksum16_ccitt_3[1],
    &chksum32_3[1],
    &chksum32b_3[1],
    &crc16_3[1],
    &crc16b_3[1],
    &crc32_3[1],
    &crc32b_3[1],
    &md5_3[1],
    &sha1_3[1],
    &sha512_3[1],
    &sha3_3[1]
};

void setValue(int off, chk_var *var[]);
void testValue(int off, chk_var *var[], chk_var *mirror[], int datasz);
void corruptValue(int off, chk_var *var[], chk_var *mirror[], int datasz);
void setValue2(int off, chk_var *var[]);

ChkVarTestSuite::ChkVarTestSuite()
{
    srand((unsigned int)time(NULL));
    value_var::set_float_format("%.18g");
}

void ChkVarTestSuite::testChk(void)
{
    const int NUM_VAR = sizeof(var) / sizeof(void *);
    for (int i = 0; i < NUM_VAR; ++i)
    {
        char printbuf[100];
        sprintf(printbuf, "Testing variable consisting of variable message length %d\n", i + 1);
        TS_TRACE(printbuf);

        //generate data
        setValue(i, var);
        testValue(i, var, mirror, DATA_SZ * sizeof(unsigned short) + sizeof(unsigned short) + FDATA_SZ * sizeof(double) + 5);
    }

    const int NUM_VAR2 = sizeof(var2) / sizeof(void *);
    for (int i = 0; i < NUM_VAR2; ++i)
    {
        char printbuf[100];
        sprintf(printbuf, "Testing variable consisting of fixed message length %d\n", i + 1);
        TS_TRACE(printbuf);

        //generate data
        setValue2(i, var2);
        testValue(i, var2, mirror2, 8+4+4+2+1+1+2+4+22);
    }
}

void ChkVarTestSuite::testCorrupt(void)
{
    const int NUM_VAR = sizeof(var) / sizeof(void *);
    for (int i = 0; i < NUM_VAR; ++i)
    {
        char printbuf[100];
        sprintf(printbuf, "Testing corruption %d\n", i + 1);
        TS_TRACE(printbuf);

        //generate data
        setValue(i, var);

        corruptValue(i, var, mirror, DATA_SZ * sizeof(unsigned short) + sizeof(unsigned short) + FDATA_SZ * sizeof(double) + 5);
    }

    const int NUM_VAR2 = sizeof(var2) / sizeof(void *);
    for (int i = 0; i < NUM_VAR2; ++i)
    {
        char printbuf[100];
        sprintf(printbuf, "Testing corruption %d\n", i + 1);
        TS_TRACE(printbuf);

        //generate data
        setValue2(i, var2);
        corruptValue(i, var2, mirror2, 8 + 4 + 4 + 2 + 1 + 1 + 2 + 4 + 22);
    }
}

void ChkVarTestSuite::testNest(void)
{
    static unsigned char buf[sizeof(int) + DATA_SZ * sizeof(short) + 64 + sizeof(unsigned short) + FDATA_SZ * sizeof(double)];
    outbuf ob;
    int i;
    int ret;
    int fail;
    int rx;
    double dbl = .5;

    TS_TRACE("Testing complex_var with chk_var child\n");

    //init the value
    c1.a = 0x12345678;
    for(i = 0; i < DATA_SZ; ++i)
        c1.b.a[i] = i;
    c1.b.b.resize(FDATA_SZ);
    for (i = 0; i < FDATA_SZ; ++i, dbl+= 1.)
        c1.b.b.from_double(i, dbl);

    m_c1 = c1;

    //output data
    memset(buf, 0, sizeof(buf));
    ob.set(buf, sizeof(buf));
    c1.output(ob);

    //write to stream
    std::ostringstream os;
    os << c1;

    //overwrite current data
    c1.setInvalid();
    TS_ASSERT(c1 != m_c1);

    //read from stream and check is is correct
    std::istringstream is(os.str());
    is >> c1;
    TS_ASSERT(c1 == m_c1);

    //overwrite current data
    c1.setInvalid();

    //save current fail status
    fail = c1.b.failed_packets();
    rx = c1.b.received_packets();

    //extract the data
    ret = c1.extract(ob.size(), ob.get());
    TS_ASSERT(ret == 0);
    TS_ASSERT(c1 == m_c1);
    TS_ASSERT(fail == c1.b.failed_packets());

    //overwrite current data
    c1.setInvalid();

    //corrupt the data if testing error detection
    buf[10] = ~buf[10];
    buf[11] = ~buf[11];

    //save current fail status
    fail = c1.b.failed_packets();
    rx = c1.b.received_packets();

    //extract the data
    ret = c1.extract(ob.size(), ob.get());
    TS_ASSERT(c1.a == m_c1.a);
    TS_ASSERT(c1.b != m_c1.b);
    TS_ASSERT((fail + 1) == c1.b.failed_packets());
}

void ChkVarTestSuite::testChksumInHeader(void)
{
    const int NUM_VAR = sizeof(var3) / sizeof(void *);
    const int datasz = 8 + 4 + 4 + 2 + 1 + 1 + 2 + 4 + 22;
    unsigned char *buf = new unsigned char[datasz + 68]; // 64 - space for checksum, 4 - space for front offset

    for (int off = 0; off < NUM_VAR; ++off)
    {
        char printbuf[100];
        sprintf(printbuf, "Testing checksum in header %d\n", off + 1);
        TS_TRACE(printbuf);

        //generate data
        setValue2(off, var3);

        memset(buf, 0, datasz + 68);
        buf[67] = rand() % 256; // random 4 byte header padding
        buf[66] = rand() % 256;
        buf[65] = rand() % 256;
        buf[64] = rand() % 256;
        outbuf ob;
        ob.set(buf + 68, datasz); // leave the front part empty
        var3[off]->output(ob);
        TS_ASSERT_EQUALS((int)ob.size(), var3[off]->size());
        mirror3[off]->setInvalid();
        mirror3[off]->extract(ob.size(), buf + 68);
        TS_ASSERT(*(var3[off]) == *(mirror3[off]));
        TS_ASSERT_EQUALS(mirror3[off]->failed_packets(), 0);
        TS_ASSERT_EQUALS(mirror3[off]->received_packets(), 1);

        buf[63] = buf[63] + 1; // corrupt the checksum
        mirror3[off]->extract(ob.size(), buf + 68);
        TS_ASSERT_EQUALS(mirror3[off]->failed_packets(), 1);
        TS_ASSERT_EQUALS(mirror3[off]->received_packets(), 2);
    }

    delete[] buf;
}

void ChkVarTestSuite::testMisc(void)
{
    const int NUM_VAR = sizeof(var3) / sizeof(void *);
    for (int off = 0; off < NUM_VAR; ++off)
    {
        char printbuf[100];
        sprintf(printbuf, "Testing misc assignment %d\n", off + 1);
        TS_TRACE(printbuf);

        //generate data
        setValue2(off, var3);

        TS_ASSERT(*(var3[off]) != *(mirror3[off]));
        TS_ASSERT_EQUALS(var3[off]->getNext(), mirror3[off]);
        *(var3[off]) = *(mirror3[off]); // assignment at chk_var level
        TS_ASSERT(*(var3[off]) == *(mirror3[off]));
        mirror3[off]->setInvalid();
        TS_ASSERT(*(var3[off]) != *(mirror3[off]));
        {
            chksum_var *c1 = dynamic_cast<chksum_var *>(var3[off]);
            if (c1 != 0)
            {
                chksum_var *c2 = static_cast<chksum_var *>(c1->getNext());
                *c2 = *c1;
            }
            else
            {
                mhash_var *m1 = dynamic_cast<mhash_var *>(var3[off]);
                mhash_var *m2 = static_cast<mhash_var *>(m1->getNext());
                *m2 = *m1;
            }
        }
        TS_ASSERT(*(var3[off]) == *(mirror3[off]));

        mpt_var *pvar = mirror3[off];
        crc32_4 = *pvar;
        TS_ASSERT(*(var3[off]) == crc32_4);

        crc32_5 = *pvar;
        TS_ASSERT(*(var3[off]) == crc32_5);

        pvar = &crc32_5.d;
        crc32_4 = *pvar; // cast will fail, nothing is assigned
        TS_ASSERT(crc32_4 == crc32_5);
    }

    const int datasz = 8 + 4 + 4 + 2 + 1 + 1 + 2 + 4 + 22;
    unsigned char *buf = new unsigned char[datasz + 64]; // 64 - space for checksum
    outbuf ob;
    ob.set(buf, datasz + 64);

    chksum32be2_typ chkcpy(chksum32be_2);
    TS_ASSERT(chkcpy == chksum32be_2);
    m2_chksum32be.setInvalid();
    TS_ASSERT(chkcpy != m2_chksum32be);
    chkcpy.output(ob);
    TS_ASSERT_EQUALS((int)ob.size(), chkcpy.size());
    m2_chksum32be.extract(ob.size(), buf);
    TS_ASSERT(chkcpy == m2_chksum32be);

    ob.set(buf, datasz + 64);

    crc32b2_typ mhhcpy(crc32b_2);
    TS_ASSERT(mhhcpy == crc32b_2);
    m2_crc32b.setInvalid();
    TS_ASSERT(mhhcpy != m2_crc32b);
    mhhcpy.output(ob);
    TS_ASSERT_EQUALS((int)ob.size(), mhhcpy.size());
    m2_crc32b.extract(ob.size(), buf);
    TS_ASSERT(mhhcpy == m2_crc32b);

    ob.set(buf, datasz + 64);

    be_crc322_typ beccpy(be_crc32_2);
    TS_ASSERT(beccpy == be_crc32_2);
    m2_be_crc32.setInvalid();
    TS_ASSERT(beccpy != m2_be_crc32);
    beccpy.output(ob);
    TS_ASSERT_EQUALS((int)ob.size(), beccpy.size());
    m2_be_crc32.extract(ob.size(), buf);
    TS_ASSERT(beccpy == m2_be_crc32);
    be_crc32_2.setInvalid();
    TS_ASSERT(be_crc32_2 != m2_be_crc32);
    be_crc32_2 = m2_be_crc32;
    TS_ASSERT(be_crc32_2 == m2_be_crc32);

    ob.set(buf, datasz + 64);

    le_crc322_typ leccpy(le_crc32_2);
    TS_ASSERT(leccpy == le_crc32_2);
    m2_le_crc32.setInvalid();
    TS_ASSERT(leccpy != m2_le_crc32);
    leccpy.output(ob);
    TS_ASSERT_EQUALS((int)ob.size(), leccpy.size());
    m2_le_crc32.extract(ob.size(), buf);
    TS_ASSERT(leccpy == m2_le_crc32);
    le_crc32_2.setInvalid();
    TS_ASSERT(le_crc32_2 != m2_le_crc32);
    le_crc32_2 = m2_le_crc32;
    TS_ASSERT(le_crc32_2 == m2_le_crc32);

    int i = (rand() - rand()) * rand() * rand();
    short s = (rand() - rand()) * rand() * rand();
    union {
        char c[4];
        int i;
    } u;
    u.i = 1;
    if (u.c[0] != 0)
    {
        // little endian machine
        TS_ASSERT_EQUALS(i, rm_LE(i));
        TS_ASSERT_EQUALS(s, rm_LE(s));
        TS_ASSERT_DIFFERS(i, rm_BE(i));
        TS_ASSERT_DIFFERS(s, rm_BE(s));
    }
    else
    {
        // big endian machine
        TS_ASSERT_EQUALS(i, rm_BE(i));
        TS_ASSERT_EQUALS(s, rm_BE(s));
        TS_ASSERT_DIFFERS(i, rm_LE(i));
        TS_ASSERT_DIFFERS(s, rm_LE(s));
    }

    delete[] buf;
}

void corruptValue(int off, chk_var *var[], chk_var *mirror[], int datasz)
{
    //output data
    unsigned char *buf = new unsigned char[datasz + 64]; // 64 - space for checksum
    memset(buf, 0, datasz + 64);
    outbuf ob;
    ob.set(buf, datasz + 64);
    var[off]->output(ob);
    TS_ASSERT_EQUALS((int)ob.size(), var[off]->size());
    TS_ASSERT_EQUALS(datasz + var[off]->checksum_size(), ob.size());

    //corrupt the data
    int c;
    if (off == 7 || off == 8)
    {
        c = 0;
        while (buf[c] != '*') ++c; // because NEMA will stop at *
    }
    else
    {
        c = ob.size() - var[off]->checksum_size();
    }
    const int idx = rand() % c;
    ++buf[idx];

    mirror[off]->setInvalid();
    mirror[off]->extract(ob.size(), buf);

    if (mirror[off]->failed_packets() == 0)
    {
        mirror[off]->extract(ob.size(), buf);
    }

    TS_ASSERT(*(var[off]) != *(mirror[off]));
    TS_ASSERT_EQUALS(mirror[off]->failed_packets(), 1);
    TS_ASSERT_EQUALS(mirror[off]->received_packets(), 2);
}

void testValue(int off, chk_var *var[], chk_var *mirror[], int datasz)
{
    //1. test copy
    *mirror[off] = *var[off];
    TS_ASSERT(*(var[off]) == *(mirror[off]));

    //2. test output data, followed by extract
    unsigned char *buf = new unsigned char[datasz + 64]; // 64 - space for checksum
    memset(buf, 0, datasz + 64);
    outbuf ob;
    ob.set(buf, datasz + 64);
    var[off]->output(ob);
    TS_ASSERT_EQUALS((int)ob.size(), var[off]->size());
    TS_ASSERT_EQUALS(datasz + var[off]->checksum_size(), ob.size());
    mirror[off]->setInvalid();
    mirror[off]->extract(ob.size(), buf);
    TS_ASSERT(*(var[off]) == *(mirror[off]));
    TS_ASSERT_EQUALS(mirror[off]->failed_packets(), 0);
    TS_ASSERT_EQUALS(mirror[off]->received_packets(), 1);

    //3. write to stream and read back
    std::ostringstream os;
    os << *var[off] << std::ends;
    mirror[off]->setInvalid();
    std::istringstream is(os.str());
    is >> *mirror[off];
    TS_ASSERT(*(var[off]) == *(mirror[off]));

    delete[] buf;
}

void setValue(int off, chk_var *var[])
{
    int i;
    generic_var<unsigned short> *gen = dynamic_cast<generic_var<unsigned short> *>(var[off]->operator[](0));
    array_var<double> *dbl = dynamic_cast<array_var<double> *>(var[off]->operator[](DATA_SZ));
    string_var<5> *str = dynamic_cast<string_var<5> *>(var[off]->operator[](DATA_SZ+1));

    TS_ASSERT(gen!=0);
    TS_ASSERT(dbl!=0);
    TS_ASSERT(str!=0);

    if (gen == 0 || dbl == 0 || str == 0) return;

    for(i = 0; i < DATA_SZ; ++i)
    {
        *gen = (unsigned short)rand();
        gen = reinterpret_cast <generic_var<unsigned short> *>(gen->getNext());
    }

    dbl->resize(FDATA_SZ);
    for(i = 0; i < FDATA_SZ; ++i)
    {
        dbl->from_double(i, static_cast<double>(rand()) / static_cast<double>(rand()));
    }

    *str = "CHECK";
}

void setValue2(int off, chk_var *var[])
{
    char str[22];
    str[21] = '\0';
    for (int i = 0; i < 21; ++i)
        str[i] = ' ' + static_cast<char>(rand() % 94);

    *dynamic_cast<generic_var<double> *>(var[off]->operator[](0)) = (double)rand() / rand();
    *dynamic_cast<generic_var<unsigned int> *>(var[off]->operator[](1)) = rand();
    *dynamic_cast<generic_var<float> *>(var[off]->operator[](2)) = (float)rand() / rand();
    *dynamic_cast<generic_var<unsigned short> *>(var[off]->operator[](3)) = rand();
    *dynamic_cast<generic_var<unsigned char> *>(var[off]->operator[](4)) = rand();
    *dynamic_cast<generic_var<char> *>(var[off]->operator[](5)) = rand() - rand();
    *dynamic_cast<generic_var<short> *>(var[off]->operator[](6)) = rand() - rand();
    *dynamic_cast<generic_var<int> *>(var[off]->operator[](7)) = rand() - rand();;
    *dynamic_cast<string_var<22> *>(var[off]->operator[](8)) = str;
}
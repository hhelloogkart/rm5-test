#ifndef _TEST_CHK_VAR_H_DEF_
#define _TEST_CHK_VAR_H_DEF_

#include <cxxtest/TestSuite.h>

class ChkVarTestSuite : public CxxTest::TestSuite
{
public:
    ChkVarTestSuite();
    void testChk(void);
    void testCorrupt(void);
    void testNest(void);
    void testChksumInHeader(void);
    void testMisc(void);

};

#endif


/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_log";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_LogTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_log\test_log.h"

static LogTestSuite suite_LogTestSuite;

static CxxTest::List Tests_LogTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_LogTestSuite( "test_log/test_log.h", 6, "LogTestSuite", suite_LogTestSuite, Tests_LogTestSuite );

static class TestDescription_suite_LogTestSuite_testSimpleLogOutput : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_LogTestSuite_testSimpleLogOutput() : CxxTest::RealTestDescription( Tests_LogTestSuite, suiteDescription_LogTestSuite, 10, "testSimpleLogOutput" ) {}
 void runTest() { suite_LogTestSuite.testSimpleLogOutput(); }
} testDescription_suite_LogTestSuite_testSimpleLogOutput;

static class TestDescription_suite_LogTestSuite_testCustomLogOutput : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_LogTestSuite_testCustomLogOutput() : CxxTest::RealTestDescription( Tests_LogTestSuite, suiteDescription_LogTestSuite, 11, "testCustomLogOutput" ) {}
 void runTest() { suite_LogTestSuite.testCustomLogOutput(); }
} testDescription_suite_LogTestSuite_testCustomLogOutput;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

#include <gen_var.h>
#include <arry_var.h>
#include <cplx_var.h>
#include <flex_var.h>
#include <defutil.h>
#include <time/timecore.h>
#include "test_log.h"
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

class my_message : public complex_var
{
public:
	COMPLEXCONST(my_message)
	LOGDATA("my_message")
	generic_var<int> s[10];
	ext_array_var<int, 4, 0, 4> a;
} msg;

class my_custom_message : public complex_var
{
public:
	COMPLEXCONST(my_custom_message)
	int extract(int len, const unsigned char *buf);
	void output(outbuf &strm);
	generic_var<int> s[10];
	flex_var<generic_var<int> > a;
} cmsg;

int my_custom_message::extract(int len, const unsigned char * buf)
{
	LOGEXTRACT("my_custom_message")
	// special extract reverses the fields
	for (int idx = 9; idx >= 0 && len > 0; --idx)
	{
		int left = s[idx].extract(len, buf);
		buf += len - left;
		len = left;
	}
	return (len > 0) ? a.extract(len, buf) : 0;
}

void my_custom_message::output(outbuf & strm)
{
	LOGOUTPUT("my_custom_message")
	for (int idx = 9; idx >= 0; --idx)
		s[idx].output(strm);
	a.output(strm);
}

LogTestSuite::LogTestSuite()
{
	srand((unsigned int)time(NULL));
}

void LogTestSuite::testSimpleLogOutput(void)
{
	int sz = rand() % 100;
	int *arry = new int[sz + 10 + 1];
	for (int i = 0; i < 10; ++i)
		arry[i] = rand();
	arry[10] = sz;
	for (int i = 0; i < sz; ++i)
		arry[11 + i] = rand();

	unsigned char *buf;
	struct timeval tv;

	// making the log
	{
		rmclient_init init("datamonlog=log");
		now(&tv);
		msg.extract((11 + sz)*sizeof(int), reinterpret_cast<unsigned char *>(arry));

		for (int i = 0; i < 10; ++i)
			msg.s[i] = rand();
		msg.a.resize(rand() % 100);
		for (int i = 0; i < msg.a.cardinal(); ++i)
			msg.a.fromValue(i, rand());

		buf = new unsigned char[msg.size()];
		outbuf ob;
		ob.set(buf, msg.size());
		msg.output(ob);

		for (int j = 0; j < 50000; ++j)
		{
			for (int i = 0; i < 10; ++i)
				msg.s[i] = rand();
			msg.a.resize(rand() % 100);
			for (int i = 0; i < msg.a.cardinal(); ++i)
				msg.a.fromValue(i, rand());

			unsigned char *nbuf = new unsigned char[msg.size()];
			outbuf ob;
			ob.set(nbuf, msg.size());
			msg.output(ob);
			delete[] nbuf;
		}
	}
	// checking the log
	std::string strbuf = "# DSO Datamon Log File v1.0";
	std::ifstream ifs("log");
	char preamble[8192];
	unsigned long long now_ms = tv.tv_sec * 1000ULL + tv.tv_usec / 1000ULL;
	unsigned long long timestamp;
	int seq;
	int timedelta;
	int rtti;
	ifs.getline(preamble, sizeof(preamble));
	TS_ASSERT_EQUALS(strbuf, preamble);
	ifs >> strbuf;
	TS_ASSERT_EQUALS(strbuf, "T");
	ifs >> timestamp;
	TS_ASSERT_DELTA(now_ms+3, timestamp, 10);
	ifs >> seq >> timedelta >> strbuf >> rtti;
	TS_ASSERT_EQUALS(seq, 0);
	TS_ASSERT_DELTA(timedelta, 12, 12);
	TS_ASSERT_EQUALS(strbuf, "my_message");
	TS_ASSERT_EQUALS(rtti, msg.rtti());
	ifs.getline(preamble, sizeof(preamble));
	unsigned char *bufp = reinterpret_cast<unsigned char *>(arry);
	TS_ASSERT_EQUALS(*preamble, ' ');
	for (char *ch = preamble + 1; *ch != 0; ++ch, ++bufp)
	{
		unsigned char val = 0;
		if ((*ch >= 'A') && (*ch <= 'F')) val += (*ch - 'A') + 10;
		else if ((*ch >= '0') && (*ch <= '9')) val += (*ch - '0');
		val <<= 4;
		++ch;
		if (*ch == 0) break;
		if ((*ch >= 'A') && (*ch <= 'F')) val += (*ch - 'A') + 10;
		else if ((*ch >= '0') && (*ch <= '9')) val += (*ch - '0');
		TS_ASSERT_EQUALS(val, *bufp);
	}
	delete[] arry;
	ifs >> seq >> timedelta >> strbuf >> rtti;
	TS_ASSERT_EQUALS(seq, 1);
	TS_ASSERT_DELTA(timedelta, 24, 12);
	TS_ASSERT_EQUALS(strbuf, "my_message");
	TS_ASSERT_EQUALS(rtti, msg.rtti());
	ifs.getline(preamble, sizeof(preamble));
	bufp = buf;
	TS_ASSERT_EQUALS(*preamble, ' ');
	for (char *ch = preamble + 1; *ch != 0; ++ch, ++bufp)
	{
		unsigned char val = 0;
		if ((*ch >= 'A') && (*ch <= 'F')) val += (*ch - 'A') + 10;
		else if ((*ch >= '0') && (*ch <= '9')) val += (*ch - '0');
		val <<= 4;
		++ch;
		if (*ch == 0) break;
		if ((*ch >= 'A') && (*ch <= 'F')) val += (*ch - 'A') + 10;
		else if ((*ch >= '0') && (*ch <= '9')) val += (*ch - '0');
		TS_ASSERT_EQUALS(val, *bufp);
	}
	delete[] buf;
	for (int i = 2; i < 50002; ++i)
	{
		ifs >> seq >> timedelta >> strbuf >> rtti;
		TS_ASSERT_EQUALS(seq, i);
		TS_ASSERT_EQUALS(strbuf, "my_message");
		TS_ASSERT_EQUALS(rtti, msg.rtti());
		ifs.getline(preamble, sizeof(preamble));
		TS_ASSERT_EQUALS(*preamble, ' ');
	}
	ifs >> preamble;
	TS_ASSERT_EQUALS(preamble[0], 'E');
	TS_ASSERT_EQUALS(preamble[1], '\0');
	ifs.close();
	remove("log");
}

void LogTestSuite::testCustomLogOutput(void)
{
	int sz = rand() % 100;
	int *arry = new int[sz + 10 + 1];
	for (int i = 0; i < 10; ++i)
		arry[i] = rand();
	arry[10] = sz;
	for (int i = 0; i < sz; ++i)
		arry[11 + i] = rand();

	unsigned char *buf;
	struct timeval tv;

	// making the log
	{
		now(&tv);
		rmclient_init init("datamonlog=log%d");
		cmsg.extract((11 + sz) * sizeof(int), reinterpret_cast<unsigned char *>(arry));
		for (int i = 0; i < 10; ++i)
			TS_ASSERT_EQUALS(cmsg.s[i], arry[9 - i]);
		TS_ASSERT_EQUALS(sz, cmsg.a.cardinal());
		for (int i = 0; i < sz; ++i)
			TS_ASSERT_EQUALS(*(cmsg.a[i]), arry[11 + i]);

		for (int i = 0; i < 10; ++i)
			cmsg.s[i] = rand();
		cmsg.a.resize(rand() % 100);
		for (int i = 0; i < cmsg.a.cardinal(); ++i)
			cmsg.a[i]->fromValue(rand());

		buf = new unsigned char[cmsg.size()];
		outbuf ob;
		ob.set(buf, msg.size());
		cmsg.output(ob);
	}
	// checking the log
	std::string header = "# DSO Datamon Log File v1.0";
	std::string strbuf;
	std::ifstream ifs("log0");
	char preamble[8192];
	unsigned long long now_ms = tv.tv_sec * 1000ULL + tv.tv_usec / 1000ULL;
	unsigned long long timestamp;
	int seq;
	int timedelta;
	int rtti;
	ifs.getline(preamble, sizeof(preamble));
	TS_ASSERT_EQUALS(header, preamble);
	ifs >> strbuf;
	TS_ASSERT_EQUALS(strbuf, "T");
	ifs >> timestamp;
	TS_ASSERT_DELTA(now_ms+3, timestamp, 6);
	ifs >> seq >> timedelta >> strbuf >> rtti;
	TS_ASSERT_EQUALS(seq, 0);
	TS_ASSERT_DELTA(timedelta, 0, 2);
	TS_ASSERT_EQUALS(strbuf, "my_custom_message");
	TS_ASSERT_EQUALS(rtti, cmsg.rtti());
	ifs.getline(preamble, sizeof(preamble));
	unsigned char *bufp = reinterpret_cast<unsigned char *>(arry);
	TS_ASSERT_EQUALS(*preamble, ' ');
	for (char *ch = preamble + 1; *ch != 0; ++ch, ++bufp)
	{
		unsigned char val = 0;
		if ((*ch >= 'A') && (*ch <= 'F')) val += (*ch - 'A') + 10;
		else if ((*ch >= '0') && (*ch <= '9')) val += (*ch - '0');
		val <<= 4;
		++ch;
		if (*ch == 0) break;
		if ((*ch >= 'A') && (*ch <= 'F')) val += (*ch - 'A') + 10;
		else if ((*ch >= '0') && (*ch <= '9')) val += (*ch - '0');
		TS_ASSERT_EQUALS(val, *bufp);
	}
	delete[] arry;
	ifs >> seq >> timedelta >> strbuf >> rtti;
	TS_ASSERT_EQUALS(seq, 1);
	TS_ASSERT_DELTA(timedelta, 12, 12);
	TS_ASSERT_EQUALS(strbuf, "my_custom_message");
	TS_ASSERT_EQUALS(rtti, cmsg.rtti());
	ifs.getline(preamble, sizeof(preamble));
	bufp = buf;
	TS_ASSERT_EQUALS(*preamble, ' ');
	for (char *ch = preamble + 1; *ch != 0; ++ch, ++bufp)
	{
		unsigned char val = 0;
		if ((*ch >= 'A') && (*ch <= 'F')) val += (*ch - 'A') + 10;
		else if ((*ch >= '0') && (*ch <= '9')) val += (*ch - '0');
		val <<= 4;
		++ch;
		if (*ch == 0) break;
		if ((*ch >= 'A') && (*ch <= 'F')) val += (*ch - 'A') + 10;
		else if ((*ch >= '0') && (*ch <= '9')) val += (*ch - '0');
		TS_ASSERT_EQUALS(val, *bufp);
	}
	delete[] buf;
	ifs.close();

	for (int filz = 1; filz < 120; ++filz)
	{
		{
			rmclient_init init("datamonlog=log%d");
			now(&tv);

			for (int linz = 0; linz < 100; ++linz)
			{
				for (int i = 0; i < 10; ++i)
					cmsg.s[i] = rand();
				cmsg.a.resize(rand() % 100);
				for (int i = 0; i < cmsg.a.cardinal(); ++i)
					cmsg.a[i]->fromValue(rand());

				buf = new unsigned char[cmsg.size()];
				outbuf ob;
				ob.set(buf, msg.size());
				cmsg.output(ob);
				delete[] buf;
			}
		}
		sprintf(preamble, "log%d", filz);
		ifs.open(preamble);
		now_ms = tv.tv_sec * 1000ULL + tv.tv_usec / 1000ULL;
		ifs.getline(preamble, sizeof(preamble));
		TS_ASSERT_EQUALS(header, preamble);
		ifs >> strbuf;
		TS_ASSERT_EQUALS(strbuf, "T");
		ifs >> timestamp;
		TS_ASSERT_DELTA(now_ms + 3, timestamp, 6);
		for (int i = 0; i < 100; ++i)
		{
			ifs >> seq >> timedelta >> strbuf >> rtti;
			TS_ASSERT_EQUALS(seq, i);
			TS_ASSERT_EQUALS(strbuf, "my_custom_message");
			TS_ASSERT_EQUALS(rtti, cmsg.rtti());
			ifs.getline(preamble, sizeof(preamble));
			TS_ASSERT_EQUALS(*preamble, ' ');
		}
		ifs >> strbuf;
		TS_ASSERT_EQUALS(strbuf, "E");
		ifs.close();
	}

	for (int filz = 0; filz < 120; ++filz)
	{
		sprintf(preamble, "log%d", filz);
		remove(preamble);
	}

	TS_TRACE("End");
}

#ifndef _TEST_LOG_H_DEF_
#define _TEST_LOG_H_DEF_

#include <cxxtest/TestSuite.h>

class LogTestSuite : public CxxTest::TestSuite
{
public:
	LogTestSuite();
	void testSimpleLogOutput(void);
	void testCustomLogOutput(void);

};

#endif


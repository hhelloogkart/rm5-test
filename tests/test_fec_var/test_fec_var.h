#ifndef _TEST_LZ_VAR_H_DEF_
#define _TEST_LZ_VAR_H_DEF_

#include <cxxtest/TestSuite.h>

class FecVarTestSuite : public CxxTest::TestSuite
{
public:
    void test00(void);
    void testFF(void);
    void test55AA(void);
    void testRunningNumber(void);
    void testRandom(void);
    void testCorruptData(void);
    void testCopyAndAssign();

private:
    void setValue(int val);
    void testValue(int val);
};

#endif


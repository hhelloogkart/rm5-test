/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_fec_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_FecVarTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_fec_var\test_fec_var.h"

static FecVarTestSuite suite_FecVarTestSuite;

static CxxTest::List Tests_FecVarTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_FecVarTestSuite( "test_fec_var/test_fec_var.h", 6, "FecVarTestSuite", suite_FecVarTestSuite, Tests_FecVarTestSuite );

static class TestDescription_suite_FecVarTestSuite_test00 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FecVarTestSuite_test00() : CxxTest::RealTestDescription( Tests_FecVarTestSuite, suiteDescription_FecVarTestSuite, 9, "test00" ) {}
 void runTest() { suite_FecVarTestSuite.test00(); }
} testDescription_suite_FecVarTestSuite_test00;

static class TestDescription_suite_FecVarTestSuite_testFF : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FecVarTestSuite_testFF() : CxxTest::RealTestDescription( Tests_FecVarTestSuite, suiteDescription_FecVarTestSuite, 10, "testFF" ) {}
 void runTest() { suite_FecVarTestSuite.testFF(); }
} testDescription_suite_FecVarTestSuite_testFF;

static class TestDescription_suite_FecVarTestSuite_test55AA : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FecVarTestSuite_test55AA() : CxxTest::RealTestDescription( Tests_FecVarTestSuite, suiteDescription_FecVarTestSuite, 11, "test55AA" ) {}
 void runTest() { suite_FecVarTestSuite.test55AA(); }
} testDescription_suite_FecVarTestSuite_test55AA;

static class TestDescription_suite_FecVarTestSuite_testRunningNumber : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FecVarTestSuite_testRunningNumber() : CxxTest::RealTestDescription( Tests_FecVarTestSuite, suiteDescription_FecVarTestSuite, 12, "testRunningNumber" ) {}
 void runTest() { suite_FecVarTestSuite.testRunningNumber(); }
} testDescription_suite_FecVarTestSuite_testRunningNumber;

static class TestDescription_suite_FecVarTestSuite_testRandom : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FecVarTestSuite_testRandom() : CxxTest::RealTestDescription( Tests_FecVarTestSuite, suiteDescription_FecVarTestSuite, 13, "testRandom" ) {}
 void runTest() { suite_FecVarTestSuite.testRandom(); }
} testDescription_suite_FecVarTestSuite_testRandom;

static class TestDescription_suite_FecVarTestSuite_testCorruptData : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FecVarTestSuite_testCorruptData() : CxxTest::RealTestDescription( Tests_FecVarTestSuite, suiteDescription_FecVarTestSuite, 14, "testCorruptData" ) {}
 void runTest() { suite_FecVarTestSuite.testCorruptData(); }
} testDescription_suite_FecVarTestSuite_testCorruptData;

static class TestDescription_suite_FecVarTestSuite_testCopyAndAssign : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_FecVarTestSuite_testCopyAndAssign() : CxxTest::RealTestDescription( Tests_FecVarTestSuite, suiteDescription_FecVarTestSuite, 15, "testCopyAndAssign" ) {}
 void runTest() { suite_FecVarTestSuite.testCopyAndAssign(); }
} testDescription_suite_FecVarTestSuite_testCopyAndAssign;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

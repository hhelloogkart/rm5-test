#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <gen_var.h>
#include <fec_var.h>
#include <lzip_var.h>
#include "test_fec_var.h"

#define BLOCK_OVERHEAD  4
#define PACKET_SIZE     50      //packet size with overhead
#define CORRECTION      50      //correction fact in %
#define MAX_BLOCK_SIZE  255     //max number of source blocks (k)
#define MAX_M           (((100 + (CORRECTION)) * (MAX_BLOCK_SIZE) + 99) / 100)  //max m
#define MAX_SOURCE_SIZE ((PACKET_SIZE - BLOCK_OVERHEAD) * MAX_BLOCK_SIZE)
#define BIG_ARRAY_SIZE  (MAX_SOURCE_SIZE * 3/4) //lets assume the compression can reduce size to < MAX_SOURCE_SIZE, esp for random data

#define COMPUTE_M(k, correction)    (((100 + (correction)) * (k) + 99) / 100) //round up
#define COMPUTE_K(m, correction)    (((m) * 100) / (100 + (correction)))


struct fec1_typ : public fec_var
{
    FECCONST( fec1_typ, PACKET_SIZE, CORRECTION )

    struct c_typ : public complex_var
    {
        COMPLEXCONST( c_typ )
        generic_var<int> a;
        generic_var<char> b;
    } c;
    generic_var<int> a[256];
    generic_var<char> b[256];
};

struct fec2_typ : public fec_var
{
    FECCONST( fec2_typ, PACKET_SIZE, CORRECTION )

    generic_var<char> a;
};

struct fec3_typ : public fec_var
{
    FECCONST( fec3_typ, PACKET_SIZE, CORRECTION )

    generic_var<char> a[MAX_SOURCE_SIZE]; //max 256 blocks
};

//compress a big data
struct lz_typ : public lzip_var
{
    LZIPCONST( lz_typ )

    generic_var<char> a[BIG_ARRAY_SIZE];
};

struct fec4_typ : public fec_var
{
    FECCONST( fec4_typ, PACKET_SIZE, CORRECTION )

    lz_typ fec1;
    generic_var<int> a;
};

bool gDirty = false;

#define GEN_VAR(n) \
struct rm_##n##_typ : public complex_var { \
    COMPLEXCONST( rm_##n##_typ ); \
    virtual void setRMDirty() { gDirty = true; }; \
    n##_typ data; } n

GEN_VAR(fec1);
GEN_VAR(fec2);
GEN_VAR(fec3);
GEN_VAR(fec4);
fec1_typ mirror1;
fec2_typ mirror2;
fec3_typ mirror3;
fec4_typ mirror4;

void FecVarTestSuite::test00(void)
{
    testValue(0);
}

void FecVarTestSuite::testFF(void)
{
    testValue(0xFFFFFFFF);
}

void FecVarTestSuite::test55AA(void)
{
    testValue(0x55AA55AA);
}

void FecVarTestSuite::testRunningNumber(void)
{
    testValue(1);
}

void FecVarTestSuite::testRandom(void)
{
    srand((unsigned int)time(NULL));
    testValue(2);
}

void FecVarTestSuite::testCorruptData(void)
{
    static unsigned char buf[4][MAX_M * PACKET_SIZE * 3];
    unsigned char errpkt[PACKET_SIZE];
    char printbuf[100];
    complex_var *var[4] = { &fec1, &fec2, &fec3, &fec4 };
    complex_var *mirror[4] = { &mirror1, &mirror2, &mirror3, &mirror4 };
    outbuf ob[4];
    unsigned int nframes[4], nf, nerr, err;
    unsigned int sz, csz;
    int ret;
    int i;

    //generate data
    memset(buf, 0, sizeof(buf));
    setValue(2);

    for(i = 0; i < 4; ++i)
    {
        //output data
        *mirror[i] = *((*var[i])[0]);
        TS_ASSERT(*((*var[i])[0]) == *((mpt_var *)mirror[i]));

        ob[i].set(buf[i], PACKET_SIZE/2);
        var[i]->output(ob[i]); // not enough buffer for output
        TS_ASSERT_EQUALS(ob[i].size(), 0);

        ob[i].set(buf[i], sizeof(buf[i]));
        nframes[i] = 0;
        do
        {
            gDirty = false;
            var[i]->output(ob[i]);
            ++nframes[i];
        } while(gDirty);

        TS_ASSERT((ob[i].size() % (PACKET_SIZE)) == 0);
        TS_ASSERT((ob[i].size() / (PACKET_SIZE)) == nframes[i]);

        //generate an out of sequence block error
        memcpy(errpkt, buf[i], PACKET_SIZE);
        ++errpkt[0];

        for(nerr = nframes[i] - COMPUTE_K(nframes[i], CORRECTION); nerr > 0; --nerr)
        {
            sprintf(printbuf, "Testing data[%d] with %d error out of m=%d k=%d\n", i, nerr, nframes[i], COMPUTE_K(nframes[i], CORRECTION));
            TS_TRACE(printbuf);

            //clear the data
            setValue(0);

            //extract data
            nf = 0;
            csz = nerr * PACKET_SIZE;
            sz = ret = ob[i].size() - csz;
            do
            {
                ret = var[i]->extract(ret, ob[i].get() + csz);
                if(nf == 0) err = (dynamic_cast <fec_var*>((*var[i])[0]))->getRxErrors();
                csz += (sz - ret);
                sz = ret;
                ++nf;
            } while(ret > 0);

            //pump a dummy packet to force data extraction
            var[i]->extract(PACKET_SIZE, errpkt);
            TS_ASSERT(err == (dynamic_cast <fec_var*>((*var[i])[0]))->getRxErrors());
            TS_ASSERT(ret == 0);
            TS_ASSERT(*((*var[i])[0]) == *((mpt_var *)mirror[i]));
            TS_ASSERT(nf == (nframes[i] - nerr));

            //pump a packet shorter than the packet length
            TS_ASSERT_EQUALS(var[i]->extract(PACKET_SIZE/2, errpkt), 0);
        }
    }
}

void FecVarTestSuite::testCopyAndAssign()
{
    fec1_typ f1(fec1.data);
    TS_ASSERT_EQUALS(f1, fec1.data);
    f1.setInvalid();
    TS_ASSERT_DIFFERS(f1, fec1.data);
    f1 = fec1.data;
    TS_ASSERT_EQUALS(f1, fec1.data);

    TS_TRACE("End");
}

void FecVarTestSuite::setValue(int val)
{
    int i;

    if(val == 1) //incrementing
    {
        fec1.data.c.a = ++val;
        fec1.data.c.b = (char)++val;
        for(i = 0; i < 256; ++i)
        {
            fec1.data.a[i] = ++val;
            fec1.data.b[i] = (char)++val;
        }

        fec2.data.a = (char)++val;

        for(i = 0; i < MAX_SOURCE_SIZE; ++i)
        {
            fec3.data.a[i] = (char)++val;
        }

        for(i = 0; i < BIG_ARRAY_SIZE; ++i)
        {
            fec4.data.fec1.a[i] = ++val;
        }
        fec4.data.a = ++val;
    }
    else if(val == 2) //random
    {
        fec1.data.c.a = rand();
        fec1.data.c.b = (char)rand();
        for(i = 0; i < 256; ++i)
        {
            fec1.data.a[i] = rand();
            fec1.data.b[i] = (char)rand();
        }

        fec2.data.a = (char)rand();

        for(i = 0; i < MAX_SOURCE_SIZE; ++i)
        {
            fec3.data.a[i] = (char)rand();
        }

        for(i = 0; i < BIG_ARRAY_SIZE; ++i)
        {
            fec4.data.fec1.a[i] = rand();
        }
        fec4.data.a = rand();
    }
    else
    {
        fec1.data.c.a = val;
        fec1.data.c.b = (char)val;
        for(i = 0; i < 256; ++i)
        {
            fec1.data.a[i] = val;
            fec1.data.b[i] = (char)val;
        }

        fec2.data.a = (char)val;

        for(i = 0; i < MAX_SOURCE_SIZE; ++i)
        {
            fec3.data.a[i] = (char)val;
        }

        for(i = 0; i < BIG_ARRAY_SIZE; ++i)
        {
            fec4.data.fec1.a[i] = val;
        }
        fec4.data.a = val;
    }
}

void FecVarTestSuite::testValue(int val)
{
    static unsigned char buf[4][MAX_M * PACKET_SIZE * 3];
    complex_var *var[4] = { &fec1, &fec2, &fec3, &fec4 };
    complex_var *mirror[4] = { &mirror1, &mirror2, &mirror3, &mirror4 };
    unsigned int nframes[4], nf;
    outbuf ob[4];
    char printbuf[100];
    unsigned int sz, csz;
    int i;
    int ret;

    memset(buf, 0, sizeof(buf));
    setValue(val);

    for(i = 0; i < 4; ++i)
    {
        *mirror[i] = *((*var[i])[0]);
        ob[i].set(buf[i], sizeof(buf[i]));
        nframes[i] = 0;
        do
        {
            gDirty = false;
            var[i]->output(ob[i]);
            ++nframes[i];
        } while(gDirty);

        sprintf(printbuf, "Generating variable fec%d ob.size=%d\n", i + 1, ob[i].size());
        TS_TRACE(printbuf);
        TS_ASSERT((ob[i].size() % (PACKET_SIZE)) == 0);
        TS_ASSERT((ob[i].size() / (PACKET_SIZE)) == nframes[i]);
    }

    setValue(~val);

    for(i = 0; i < 4; ++i)
    {
        sprintf(printbuf, "variable fec%d in:%d out:%d %.1f\n", i + 1,
            var[i]->size(), ob[i].size(), 100.0f * ob[i].size()/var[i]->size());
        TS_TRACE(printbuf);

        nf = 0;
        csz = 0;
        sz = ret = ob[i].size();
        do
        {
            ret = var[i]->extract(ret, ob[i].get() + csz);
            csz += (sz - ret);
            sz = ret;
            ++nf;
        } while(ret > 0);
        TS_ASSERT(ret == 0);
        TS_ASSERT(*((*var[i])[0]) == *((mpt_var *)mirror[i]));
        TS_ASSERT(nf == nframes[i]);
    }
}

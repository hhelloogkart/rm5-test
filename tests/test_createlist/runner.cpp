/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_createlist";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_CreateListTestSuite_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_createlist\test_createlist.h"

static CreateListTestSuite suite_CreateListTestSuite;

static CxxTest::List Tests_CreateListTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_CreateListTestSuite( "test_createlist/test_createlist.h", 6, "CreateListTestSuite", suite_CreateListTestSuite, Tests_CreateListTestSuite );

static class TestDescription_suite_CreateListTestSuite_testCreateList : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_CreateListTestSuite_testCreateList() : CxxTest::RealTestDescription( Tests_CreateListTestSuite, suiteDescription_CreateListTestSuite, 9, "testCreateList" ) {}
 void runTest() { suite_CreateListTestSuite.testCreateList(); }
} testDescription_suite_CreateListTestSuite_testCreateList;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

#include <cplx_var.h>
#include <task/threadt.h>
#include <time/timecore.h>
#include "test_createlist.h"

struct cplx_var : public complex_var
{
    RUNTIME_TYPE(cplx_var)
    mpt_var *getNext();
};

mpt_var *cplx_var::getNext()
{
    return this + 1;
}

bool thread_func(void *)
{
    TS_ASSERT(mpt_var::check_stack());
    cplx_var comp3;
    TS_ASSERT(!mpt_var::check_stack());
    comp3.pop();
    TS_ASSERT(mpt_var::check_stack());
    return true;
}

void CreateListTestSuite::testCreateList(void)
{
    cplx_var comp1, comp2;
    TS_ASSERT(!mpt_var::check_stack());
    thread_typ th(&thread_func, 0, false);
    while (!th.finished()) context_yield();
    comp1.pop();
    comp2.pop();
    TS_ASSERT(mpt_var::check_stack());
}

/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XmlPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::XmlPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "test_rm_var";
    status = CxxTest::Main< CxxTest::XmlPrinter >( tmp, argc, argv );
    return status;
}
bool suite_client1_init = false;
#include "C:\GitLab-Runner\builds\swl\rm5\tests\test_rm_var\test_rm_var.h"

static client1 suite_client1;

static CxxTest::List Tests_client1 = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_client1( "test_rm_var/test_rm_var.h", 16, "client1", suite_client1, Tests_client1 );

static class TestDescription_suite_client1_testRttiGetNext : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_client1_testRttiGetNext() : CxxTest::RealTestDescription( Tests_client1, suiteDescription_client1, 19, "testRttiGetNext" ) {}
 void runTest() { suite_client1.testRttiGetNext(); }
} testDescription_suite_client1_testRttiGetNext;

static class TestDescription_suite_client1_testNorminal : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_client1_testNorminal() : CxxTest::RealTestDescription( Tests_client1, suiteDescription_client1, 20, "testNorminal" ) {}
 void runTest() { suite_client1.testNorminal(); }
} testDescription_suite_client1_testNorminal;

static class TestDescription_suite_client1_testBufvar : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_client1_testBufvar() : CxxTest::RealTestDescription( Tests_client1, suiteDescription_client1, 21, "testBufvar" ) {}
 void runTest() { suite_client1.testBufvar(); }
} testDescription_suite_client1_testBufvar;

static class TestDescription_suite_client1_testValvar : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_client1_testValvar() : CxxTest::RealTestDescription( Tests_client1, suiteDescription_client1, 22, "testValvar" ) {}
 void runTest() { suite_client1.testValvar(); }
} testDescription_suite_client1_testValvar;

static class TestDescription_suite_client1_testRmvar : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_client1_testRmvar() : CxxTest::RealTestDescription( Tests_client1, suiteDescription_client1, 23, "testRmvar" ) {}
 void runTest() { suite_client1.testRmvar(); }
} testDescription_suite_client1_testRmvar;

static client2 suite_client2;

static CxxTest::List Tests_client2 = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_client2( "test_rm_var/test_rm_var.h", 26, "client2", suite_client2, Tests_client2 );

static class TestDescription_suite_client2_testNorminal : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_client2_testNorminal() : CxxTest::RealTestDescription( Tests_client2, suiteDescription_client2, 29, "testNorminal" ) {}
 void runTest() { suite_client2.testNorminal(); }
} testDescription_suite_client2_testNorminal;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";

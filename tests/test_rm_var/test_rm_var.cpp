#include <buf_var.h>
#include <val_var.h>
#include <rmmsg_var.h>
#include <rmdata_var.h>
#include <arry_var.h>
#include <defutil.h>
#include <defaults.h>
#include <mpt_show.h>
#include "test_rm_var.h"
#include <time/timecore.h>
#include <random>
#include <set>
#include <string>
#include <sstream>
#include <string.h>
#include <stdio.h>

class Rmv1 : public rm_var
{
public:
    RMCONST(Rmv1,"TEST1",true)
    array_var<unsigned int> data;
} rmv1;
class Rmv2 : public rm_var
{
public:
    RMCONST(Rmv2, "TEST2", true)
    array_var<unsigned int> data;
} rmv2;
class Rmv3 : public rm_message_var
{
public:
    RMMSGCONST(Rmv3, "TEST3", true)
    array_var<unsigned int> data;
} rmv3;
class Rmv4 : public rm_message_var
{
public:
    RMMSGCONST(Rmv4, "TEST4", true)
    array_var<unsigned int> data;
} rmv4;
class Rmv5 : public rm_data_var
{
public:
    RMDATACONST(Rmv5, "TEST5", true)
    array_var<unsigned int> data;
} rmv5;
class Rmv6 : public rm_data_var
{
public:
    RMDATACONST(Rmv6, "TEST6", true)
    array_var<unsigned int> data;
} rmv6;
class Rma1 : public rm_var
{
public:
    RMCONST_ARRAY(Rma1, "ARRAY1_%d", true)
    array_var<unsigned int> data;
} arry1[10];
class Rma2 : public rm_var
{
public:
    RMCONST_ARRAY(Rma2, "ARRAY2_%d", true)
    array_var<unsigned int> data;
} arry2[10];
class Rma3 : public rm_message_var
{
public:
    RMMSGCONST_ARRAY(Rma3, "ARRAY3_%d", true)
    array_var<unsigned int> data;
} arry3[10];
class Rma4 : public rm_message_var
{
public:
    RMMSGCONST_ARRAY(Rma4, "ARRAY4_%d", true)
    array_var<unsigned int> data;
} arry4[10];
class Rma5 : public rm_data_var
{
public:
    RMDATACONST_ARRAY(Rma5, "ARRAY5_%d", true)
    array_var<unsigned int> data;
} arry5[10];
class Rma6 : public rm_data_var
{
public:
    RMDATACONST_ARRAY(Rma6, "ARRAY6_%d", true)
    array_var<unsigned int> data;
} arry6[10];
buf_var bv1("BUFFER1", true);
buf_var bv2("BUFFER2", true);
buf_var bv3("BUFFER3", true);
buf_var bv4("BUFFER4", true);
valid_var vv1("VALID1", true);
valid_var vv2("VALID2", true);
valid_var vv3("VALID3", true);
valid_var vv4("VALID4", true);

class ClientStatus : public rm_var
{
public:
    RMCONST_ARRAY(ClientStatus, "CLIENT%d_STATUS", true)
    int extract(int len, const unsigned char* buf);
    generic_var<int> state;
    void addhit(mpt_show *shw, int hits);
private:
    std::map<mpt_show *, int> hitmap;
} clientstat1, clientstat2;

int ClientStatus::extract(int len, const unsigned char* buf)
{
    const int ret = complex_var::extract(len, buf);
    printf("Status: %d\n", state.to_int());
    return ret;
}

void ClientStatus::addhit(mpt_show *shw, int hits)
{
    std::map<mpt_show *, int>::iterator it = hitmap.find(shw);
    if (it == hitmap.end())
    {
        hitmap[shw] = hits;
    }
    else
    {
        (*it).second += hits;
        hits = (*it).second;
    }
    for (it = hitmap.begin(); it != hitmap.end(); ++it)
        if ((*it).second != hits) break;
    if (it == hitmap.end())
        state = hits;
}

static timeElapsed te;
static RmVarTestSuiteFixture fixture;
static rmclient_init *init = 0;
static int tries = 100;
static int cycle = 400;
static int timeout = 1500;

bool RmVarTestSuiteFixture::setUpWorld()
{
    IOSL_INIT("iosl.cfg");
    init = new rmclient_init("+test_rm_var.cfg");
    init->client_defaults()->getint("tries", &tries);
    init->client_defaults()->getint("cycle", &cycle);
    static_cast<defaults *>(*init)->getint("timeout", &timeout);
    return init != 0;
}

bool RmVarTestSuiteFixture::tearDownWorld()
{
    delete init;
    return true;
}

inline bool chk_avi(array_var<unsigned int> &data, std::mt19937 &r)
{
    std::uniform_int_distribution<int> d(1, 61);
    const int sz = d(r);
    TS_ASSERT_EQUALS(sz, data.cardinal());
    for (int i = 0; i < data.cardinal(); ++i)
    {
        const unsigned int val = r();
        TS_ASSERT_EQUALS(data.to_unsigned_int(i), val);
        if (val != data.to_unsigned_int(i))
            return false;
    }
    return true;
}

class avi_show : public mpt_autoshow
{
public:
    avi_show(array_var<unsigned int> *var_, std::mt19937 &r_, int id_, int client);
    void operator()();

private:
    array_var<unsigned int> *var;
    std::mt19937 &r;
    int count;
    int id;
    ClientStatus &notify;
};

avi_show::avi_show(array_var<unsigned int>* var_, std::mt19937 & r_, int id_, int client) :
    var(var_), r(r_), count(0), id(id_), notify((client == 1) ?clientstat1 : clientstat2)
{
    addCallback(var_);
}

void avi_show::operator()()
{
    if (dirty)
    {
        dirty = false;
        if (!chk_avi(*var, r))
        {
            printf("Count %d, id %d: Fail\n", count, id);
        }
        else
        {
            printf("Count %d, id %d: OK\n", count, id);
        }
        ++count;
        te();
        notify.addhit(this, 1);
    }
}

class buf_show : public mpt_autoshow
{
public:
    buf_show(buf_var *var_, std::mt19937 &r_, int id_, int client);
    void operator()();

private:
    buf_var *var;
    std::mt19937 &r;
    int count;
    int id;
    ClientStatus &notify;

};

buf_show::buf_show(buf_var* var_, std::mt19937 & r_, int id_, int client) :
    var(var_), r(r_), count(0), id(id_), notify((client == 1) ? clientstat1 : clientstat2)
{
    addCallback(var_);
}

void buf_show::operator()()
{
    if (dirty)
    {
        dirty = false;
        std::uniform_int_distribution<int> d(1, 60);
        const unsigned int sz = d(r);
        TS_ASSERT_EQUALS(var->size() / 4, sz);
        bool fail = (var->size() / 4 != sz);
        const unsigned int *data = reinterpret_cast<const unsigned int *>(var->getBuffer());
        for (int i = 0; i < sz; ++i)
        {
            const unsigned int val = r();
            TS_ASSERT_EQUALS(data[i], val);
            fail |= (data[i] != val);
        }
        if (fail)
        {
            printf("Count %d, id %d: Fail\n", count, id);
        }
        else
        {
            printf("Count %d, id %d: OK\n", count, id);
        }
        ++count;
        te();
        notify.addhit(this, 1);
    }
}

class val_show : public mpt_autoshow
{
public:
    val_show(valid_var *var_, std::mt19937 &r_, int id_, int client);
    void operator()();
    void addElement();
    void setElement(unsigned int obj);
    void clrElement(unsigned int obj);
    bool chkElement(unsigned int obj) const;
    int getCount() const;

private:
    valid_var *var;
    std::mt19937 &r;
    int count;
    int id;
    std::set<unsigned int, std::less<unsigned int> > mirror;
    ClientStatus &notify;

};

val_show::val_show(valid_var* var_, std::mt19937 & r_, int id_, int client) :
    var(var_), r(r_), count(0), id(id_), notify((client == 1) ? clientstat1 : clientstat2)
{
    addCallback(var_);
}

void val_show::operator()()
{
    if (dirty)
    {
        dirty = false;
        std::uniform_int_distribution<int> d(1, 30);
        const unsigned int obj = d(r);
        if (obj > 26)
            addElement();
        else if (chkElement(obj))
            clrElement(obj);
        else setElement(obj);
        TS_ASSERT_EQUALS(static_cast<unsigned int>(var->size()) / 4U, mirror.size());
        bool fail = (var->size() / 4 != mirror.size());
        if (mirror.empty())
        {
            for (int i = 0; i < 20; ++i) {
                TS_ASSERT_EQUALS(var->chkElement(i), false);
                fail |= (var->chkElement(i) != chkElement(i));
            }
        }
        else
        {
            std::set<unsigned int>::reverse_iterator it = mirror.rbegin();
            const unsigned int sz = (*it) + 5U;
            for (int i = 0; i < sz; ++i) {
                TS_ASSERT_EQUALS(var->chkElement(i), chkElement(i));
                fail |= (var->chkElement(i) != chkElement(i));
            }
        }
        if (fail)
        {
            printf("Count %d, id %d: Fail\n", count, id);
        }
        else
        {
            printf("Count %d, id %d: OK\n", count, id);
        }
        ++count;
        te();
        notify.addhit(this, 1);
    }
}

void val_show::addElement()
{
    unsigned int obj = 0;
    while (chkElement(obj)) ++obj;
    setElement(obj);
}

void val_show::setElement(unsigned int obj)
{
    mirror.insert(obj);
}

void val_show::clrElement(unsigned int obj)
{
    mirror.erase(obj);
}

bool val_show::chkElement(unsigned int obj) const
{
    return mirror.find(obj) != mirror.cend();
}

int val_show::getCount() const
{
    return count;
}

inline void pop_avi(array_var<unsigned int> &data, std::mt19937 &r)
{
    // 256 bytes - 12 bytes overhead = 244 (61 ints)
    std::uniform_int_distribution<int> d(1, 61);
    const int sz = d(r);
    data.resize(sz);
    for (int i = 0; i < sz; ++i)
        data.fromValue(i, r());
}

inline void pop_buf(buf_var &data, std::mt19937 &r)
{
    std::uniform_int_distribution<int> d(1, 60);
    const unsigned int sz = d(r);
    if (sz < 30)
    {
        data.set(sz);
        for (int i = 0; i < sz; ++i)
            data[i] = r();
        data.setRMDirty();
    }
    else
    {
        unsigned int *buf = new unsigned int[sz];
        for (int i = 0; i < sz; ++i)
            buf[i] = r();
        data.setBuffer(buf, sz * 4);
        delete[] buf;;
    }
}

inline void pop_val(valid_var &data, std::mt19937 &r)
{
    std::uniform_int_distribution<int> d(1, 30);
    const unsigned int obj = d(r);
    if (obj > 26)
        data.addElement();
    else if (data.chkElement(obj))
        data.clrElement(obj);
    else data.setElement(obj);
}

void client1::testRttiGetNext()
{
    TS_ASSERT_EQUALS(arry1[0].getNext(), &arry1[1]);
    TS_ASSERT_EQUALS(arry1[1].getNext(), &arry1[2]);
    TS_ASSERT_EQUALS(arry1[2].getNext(), &arry1[3]);
    TS_ASSERT_EQUALS(arry1[3].getNext(), &arry1[4]);
    TS_ASSERT_EQUALS(arry1[4].getNext(), &arry1[5]);

    TS_ASSERT_EQUALS(arry3[0].getNext(), &arry3[1]);
    TS_ASSERT_EQUALS(arry3[1].getNext(), &arry3[2]);
    TS_ASSERT_EQUALS(arry3[2].getNext(), &arry3[3]);
    TS_ASSERT_EQUALS(arry3[3].getNext(), &arry3[4]);
    TS_ASSERT_EQUALS(arry3[4].getNext(), &arry3[5]);

    TS_ASSERT_EQUALS(arry5[0].getNext(), &arry5[1]);
    TS_ASSERT_EQUALS(arry5[1].getNext(), &arry5[2]);
    TS_ASSERT_EQUALS(arry5[2].getNext(), &arry5[3]);
    TS_ASSERT_EQUALS(arry5[3].getNext(), &arry5[4]);
    TS_ASSERT_EQUALS(arry5[4].getNext(), &arry5[5]);

    TS_ASSERT_EQUALS(arry1[0].rtti(), arry1[1].rtti());
    TS_ASSERT_DIFFERS(arry1[0].rtti(), arry2[0].rtti());
    TS_ASSERT_EQUALS(arry3[0].rtti(), arry3[1].rtti());
    TS_ASSERT_DIFFERS(arry3[0].rtti(), arry4[0].rtti());
    TS_ASSERT_EQUALS(arry5[0].rtti(), arry5[1].rtti());
    TS_ASSERT_DIFFERS(arry5[0].rtti(), arry6[0].rtti());
    TS_ASSERT_EQUALS(bv1.rtti(), bv2.rtti());
    TS_ASSERT_EQUALS(vv1.rtti(), vv2.rtti());

}

void client1::testNorminal(void)
{
    std::mt19937 r1(1);
    std::mt19937 r2(10);
    std::mt19937 r3(100);
    std::mt19937 r4[10];
    std::mt19937 r5[10];
    std::mt19937 r6[10];
    std::mt19937 r7(500);
    std::mt19937 r8(1000);
    std::mt19937 r9(2000);
    std::mt19937 r10(3001);
    for (int i = 0; i < 10; ++i)
    {
        r4[i].seed(200 + i);
        r5[i].seed(300 + i);
        r6[i].seed(400 + i);
    }

    std::mt19937 r11(3);
    std::mt19937 r12(13);
    std::mt19937 r13(103);
    std::mt19937 r14[10];
    std::mt19937 r15[10];
    std::mt19937 r16[10];
    std::mt19937 r17(503);
    std::mt19937 r18(1003);
    std::mt19937 r19(2003);
    std::mt19937 r20(3004);
    for (int i = 0; i < 10; ++i)
    {
        r14[i].seed(4000 + i);
        r15[i].seed(4100 + i);
        r16[i].seed(4200 + i);
    }

    avi_show s1_1(&rmv1.data, r1, 0, 1);
    avi_show s1_2(&rmv3.data, r2, 1, 1);
    avi_show s1_3(&rmv5.data, r3, 2, 1);
    avi_show *s1_4[10];
    avi_show *s1_5[10];
    avi_show *s1_6[10];
    for (int i = 0; i < 10; ++i)
    {
        s1_4[i] = new avi_show(&(arry1[i].data), r4[i], 10 + i, 1);
        s1_5[i] = new avi_show(&(arry3[i].data), r5[i], 20 + i, 1);
        s1_6[i] = new avi_show(&(arry5[i].data), r6[i], 30 + i, 1);
    }
    buf_show s1_7(&bv1, r7, 4, 1);
    buf_show s1_8(&bv3, r8, 5, 1);
    val_show s1_9(&vv1, r9, 6, 1);
    val_show s1_10(&vv3, r10, 7, 1);

    clientstat1.setRegister(false);
    clientstat1.addhit(&s1_1, 0);
    clientstat1.addhit(&s1_2, 1);
    clientstat1.addhit(&s1_3, 0);
    for (int i = 0; i < 10; ++i)
    {
        clientstat1.addhit(s1_4[i], 0);
        clientstat1.addhit(s1_5[i], 1);
        clientstat1.addhit(s1_6[i], 0);
    }
    clientstat1.addhit(&s1_7, 0);
    clientstat1.addhit(&s1_8, 0);
    clientstat1.addhit(&s1_9, 0);
    clientstat1.addhit(&s1_10, 0);

    for (int cnt = 0; cnt < 10; ++cnt)
    {
        init->incoming();
        init->outgoing();
        millisleep(cycle);
    }
    if (te.isValid()) te();

    int count = 0;
    while (!init->client_exit() && (!te.isValid() || te.elapsed() < timeout))
    {
        init->incoming();
        mpt_show::refresh();
        if (count <= clientstat2.state.to_int() && count < tries)
        {
            pop_avi(rmv2.data, r11);
            if (count > 0) pop_avi(rmv4.data, r12);
            pop_avi(rmv6.data, r13);
            for (int i = 0; i < 10; ++i)
            {
                pop_avi(arry2[i].data, r14[i]);
                if (count > 0) pop_avi(arry4[i].data, r15[i]);
                pop_avi(arry6[i].data, r16[i]);
            }
            pop_buf(bv2, r17);
            pop_buf(bv4, r18);
            pop_val(vv2, r19);
            pop_val(vv4, r20);
            if (te.isValid()) te();
            ++count;
        }
        init->outgoing();
        millisleep(cycle);
    }
    for (int i = 0; i < 10; ++i)
    {
        delete s1_4[i];
        delete s1_5[i];
        delete s1_6[i];
    }
}

void client1::testBufvar()
{
    TS_ASSERT_EQUALS(bv1.isValid(), true);
    TS_ASSERT_EQUALS(bv3.isValid(), true);
    TS_ASSERT_DIFFERS(bv1, bv3);
    bv1.setInvalid();
    TS_ASSERT_EQUALS(bv1.isValid(), false);
    bv1 = bv3;
    TS_ASSERT_EQUALS(bv1.isValid(), true);
    TS_ASSERT_EQUALS(bv1, bv3);
    bv1.setInvalid();
    TS_ASSERT_EQUALS(bv1.isValid(), false);
    mpt_var &mpr = bv3;
    bv1 = mpr;
    TS_ASSERT_EQUALS(bv1.isValid(), true);
    TS_ASSERT_EQUALS(bv1, bv3);

    TS_ASSERT_SAME_DATA(bv1.getName(), "BUFFER1", 8);
    TS_ASSERT_EQUALS(bv1.getRegister(), true);
    std::string sbuf;
    sbuf = "BUFFER5 0 9 11 22 33 44 55 66 77 88 99";
    {
        std::istringstream iss(sbuf);
        iss >> bv1;
    }
    TS_ASSERT_SAME_DATA(bv1.getName(), "BUFFER5", 8);
    TS_ASSERT_EQUALS(bv1.getRegister(), false);
    TS_ASSERT_EQUALS(bv1.size(), 9 * 4);
    for (int cnt = 0; cnt < 9; ++cnt)
        TS_ASSERT_EQUALS(bv1[cnt], (cnt + 1) * 11);

    {
        std::ostringstream oss;
        oss << bv1;
        TS_ASSERT_EQUALS(sbuf, oss.str());
    }
    sbuf = "BUFFER5 0 9";
    {
        std::istringstream iss(sbuf);
        iss >> bv1;
    }
    TS_ASSERT_EQUALS(bv1.isValid(), false);
    sbuf = "BUFFER5 1 0";
    {
        std::istringstream iss(sbuf);
        iss >> bv1;
    }
    TS_ASSERT_EQUALS(bv1.isValid(), false);
    TS_ASSERT_EQUALS(bv1.getRegister(), true);
}

void client1::testValvar()
{
    TS_ASSERT_EQUALS(vv1.isValid(), true);
    TS_ASSERT_EQUALS(vv3.isValid(), true);
    TS_ASSERT_DIFFERS(vv1, vv3);
    vv1.setInvalid();
    TS_ASSERT_EQUALS(vv1.isValid(), false);
    vv1 = vv3;
    TS_ASSERT_EQUALS(vv1.isValid(), true);
    TS_ASSERT_EQUALS(vv1, vv3);
    vv1.setInvalid();
    TS_ASSERT_EQUALS(vv1.isValid(), false);
    mpt_var &mpr = vv3;
    vv1 = mpr;
    TS_ASSERT_EQUALS(vv1.isValid(), true);
    TS_ASSERT_EQUALS(vv1, vv3);

    TS_ASSERT_SAME_DATA(vv1.getName(), "VALID1", 7);
    TS_ASSERT_EQUALS(vv1.getRegister(), true);
    std::string sbuf;
    sbuf = "VALID5 0 9 11 22 33 44 55 66 77 88 99";
    {
        std::istringstream iss(sbuf);
        iss >> vv1;
    }
    TS_ASSERT_SAME_DATA(vv1.getName(), "VALID5", 7);
    TS_ASSERT_EQUALS(vv1.getRegister(), false);
    TS_ASSERT_EQUALS(vv1.size(), 9 * 4);
    for (int cnt = 1; cnt < 100; ++cnt)
        TS_ASSERT_EQUALS(vv1.chkElement(cnt), cnt % 11 == 0);

    {
        std::ostringstream oss;
        oss << vv1;
        TS_ASSERT_EQUALS(sbuf, oss.str());
    }
    sbuf = "VALID5 0 9";
    {
        std::istringstream iss(sbuf);
        iss >> vv1;
    }
    TS_ASSERT_EQUALS(vv1.isValid(), false);
    sbuf = "VALID5 1 0";
    {
        std::istringstream iss(sbuf);
        iss >> vv1;
    }
    TS_ASSERT_EQUALS(vv1.isValid(), false);
    TS_ASSERT_EQUALS(vv1.getRegister(), true);
}

void client1::testRmvar()
{
    TS_ASSERT_EQUALS(rmv1.isValid(), true);
    TS_ASSERT_EQUALS(rmv3.isValid(), true);
    TS_ASSERT_EQUALS(rmv5.isValid(), true);
    TS_ASSERT_DIFFERS(rmv1, rmv3);
    TS_ASSERT_DIFFERS(rmv3, rmv5);
    rmv1.setInvalid();
    TS_ASSERT_EQUALS(rmv1.isValid(), false);
    rmv1 = rmv3;
    TS_ASSERT_EQUALS(rmv1.isValid(), true);
    TS_ASSERT_EQUALS(rmv1, rmv3);
    rmv1 = rmv5;
    TS_ASSERT_EQUALS(rmv1.isValid(), true);
    TS_ASSERT_EQUALS(rmv1, rmv5);
    rmv1.setInvalid();
    TS_ASSERT_EQUALS(rmv1.isValid(), false);
    mpt_var &mpr1 = rmv3;
    rmv1 = mpr1;
    TS_ASSERT_EQUALS(rmv1.isValid(), true);
    TS_ASSERT_EQUALS(rmv1, rmv3);
    mpt_var &mpr2 = rmv5;
    rmv1 = mpr2;
    TS_ASSERT_EQUALS(rmv1.isValid(), true);
    TS_ASSERT_EQUALS(rmv1, rmv5);

    TS_ASSERT_SAME_DATA(rmv1.getName(), "TEST1", 6);
    TS_ASSERT_EQUALS(rmv1.getRegister(), true);
    std::string sbuf;
    union {
        char c[4];
        int i;
    } u;
    u.i = 1;
    if (u.c[0] != 0)
    {
        // little endian machine
        sbuf = "TEST7 0 36 22 11 00 00 "
                          "44 33 00 00 "
                          "66 55 00 00 "
                          "88 77 00 00 "
                          "aa 99 00 00 "
                          "cc bb 00 00 "
                          "ee dd 00 00 "
                          "00 ff 00 00 "
                          "66 55 22 11";
    }
    else
    {
        sbuf = "TEST7 0 36 00 00 11 22 "
                          "00 00 33 44 "
                          "00 00 55 66 "
                          "00 00 77 88 "
                          "00 00 99 aa "
                          "00 00 bb cc "
                          "00 00 dd ee "
                          "00 00 ff 00 "
                          "11 22 55 66";
    }
    {
        std::istringstream iss(sbuf);
        iss >> rmv1;
    }
    TS_ASSERT_SAME_DATA(rmv1.getName(), "TEST7", 6);
    TS_ASSERT_EQUALS(rmv1.getRegister(), false);
    TS_ASSERT_EQUALS(rmv1.size(), 9 * 4 + 2);
    TS_ASSERT_EQUALS(rmv1.data[0], 0x1122);
    TS_ASSERT_EQUALS(rmv1.data[1], 0x3344);
    TS_ASSERT_EQUALS(rmv1.data[2], 0x5566);
    TS_ASSERT_EQUALS(rmv1.data[3], 0x7788);
    TS_ASSERT_EQUALS(rmv1.data[4], 0x99aa);
    TS_ASSERT_EQUALS(rmv1.data[5], 0xbbcc);
    TS_ASSERT_EQUALS(rmv1.data[6], 0xddee);
    TS_ASSERT_EQUALS(rmv1.data[7], 0xff00);
    TS_ASSERT_EQUALS(rmv1.data[8], 0x11225566);
    {
        std::ostringstream oss;
        oss << rmv1;
        TS_ASSERT_EQUALS(sbuf, oss.str());
    }
    sbuf = "TEST7 0 36";
    {
        std::istringstream iss(sbuf);
        iss >> rmv1;
    }
    TS_ASSERT_EQUALS(rmv1.isValid(), false);
    sbuf = "TEST7 1 0";
    {
        std::istringstream iss(sbuf);
        iss >> rmv1;
    }
    TS_ASSERT_EQUALS(rmv1.isValid(), false);
    TS_ASSERT_EQUALS(rmv1.getRegister(), true);
}

void client2::testNorminal()
{
    std::mt19937 r1(1);
    std::mt19937 r2(10);
    std::mt19937 r3(100);
    std::mt19937 r4[10];
    std::mt19937 r5[10];
    std::mt19937 r6[10];
    std::mt19937 r7(500);
    std::mt19937 r8(1000);
    std::mt19937 r9(2000);
    std::mt19937 r10(3001);
    for (int i = 0; i < 10; ++i)
    {
        r4[i].seed(200 + i);
        r5[i].seed(300 + i);
        r6[i].seed(400 + i);
    }

    std::mt19937 r11(3);
    std::mt19937 r12(13);
    std::mt19937 r13(103);
    std::mt19937 r14[10];
    std::mt19937 r15[10];
    std::mt19937 r16[10];
    std::mt19937 r17(503);
    std::mt19937 r18(1003);
    std::mt19937 r19(2003);
    std::mt19937 r20(3004);
    for (int i = 0; i < 10; ++i)
    {
        r14[i].seed(4000 + i);
        r15[i].seed(4100 + i);
        r16[i].seed(4200 + i);
    }

    avi_show s1_1(&rmv2.data, r11, 40, 2);
    avi_show s1_2(&rmv4.data, r12, 41, 2);
    avi_show s1_3(&rmv6.data, r13, 42, 2);
    avi_show *s1_4[10];
    avi_show *s1_5[10];
    avi_show *s1_6[10];
    for (int i = 0; i < 10; ++i)
    {
        s1_4[i] = new avi_show(&(arry2[i].data), r14[i], 50 + i, 2);
        s1_5[i] = new avi_show(&(arry4[i].data), r15[i], 60 + i, 2);
        s1_6[i] = new avi_show(&(arry6[i].data), r16[i], 70 + i, 2);
    }
    buf_show s1_7(&bv2, r17, 44, 2);
    buf_show s1_8(&bv4, r18, 45, 2);
    val_show s1_9(&vv2, r19, 46, 2);
    val_show s1_10(&vv4, r20, 47, 2);

    clientstat2.setRegister(false);
    clientstat2.addhit(&s1_1, 0);
    clientstat2.addhit(&s1_2, 1);
    clientstat2.addhit(&s1_3, 0);
    for (int i = 0; i < 10; ++i)
    {
        clientstat2.addhit(s1_4[i], 0);
        clientstat2.addhit(s1_5[i], 1);
        clientstat2.addhit(s1_6[i], 0);
    }
    clientstat2.addhit(&s1_7, 0);
    clientstat2.addhit(&s1_8, 0);
    clientstat2.addhit(&s1_9, 0);
    clientstat2.addhit(&s1_10, 0);

    for (int cnt = 0; cnt < 10; ++cnt)
    {
        init->incoming();
        init->outgoing();
        millisleep(cycle);
    }
    if (te.isValid()) te();

    int count = 0;
    while (!init->client_exit() && (!te.isValid() || te.elapsed() < timeout))
    {
        init->incoming();
        mpt_show::refresh();
        if (count <= clientstat1.state.to_int() && count < tries)
        {
            pop_avi(rmv1.data, r1);
            if (count > 0) pop_avi(rmv3.data, r2);
            pop_avi(rmv5.data, r3);
            for (int i = 0; i < 10; ++i)
            {
                pop_avi(arry1[i].data, r4[i]);
                if (count > 0) pop_avi(arry3[i].data, r5[i]);
                pop_avi(arry5[i].data, r6[i]);
            }
            pop_buf(bv1, r7);
            pop_buf(bv3, r8);
            pop_val(vv1, r9);
            pop_val(vv3, r10);
            if (te.isValid()) te();
            ++count;
        }
        init->outgoing();
    }
    for (int i = 0; i < 10; ++i)
    {
        delete s1_4[i];
        delete s1_5[i];
        delete s1_6[i];
    }
    TS_TRACE("End");
}

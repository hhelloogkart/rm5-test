@echo off
echo #define COMPILE_VER "5.0"
echo #define COMPILE_BY "%USERNAME%"
echo #define COMPILE_HOST "%COMPUTERNAME%"
echo #define COMPILE_OSNAME "%OS%"
echo #define COMPILE_HOSTTYPE "5.0"
echo #define COMPILE_QUOTES(x) #x
echo #define COMPILE_DATE __DATE__
echo #define COMPILE_TIME __TIME__

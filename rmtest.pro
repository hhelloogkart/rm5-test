TEMPLATE = app
INCLUDEPATH += ../oscore/include .
greaterThan(QT_MAJOR_VERSION, 4){
	QT += widgets
}
CONFIG += qt warn_on no_rmiconn
macx:CONFIG += no_scramnet
TARGET		= rmtest
DEPENDPATH+= $(QTDIR)/include
INCLUDEPATH+= $(QTDIR)/include include src/tools/rmtest src/mhash/include ../oscore/include
OBJECTS_DIR = .objs/rmtest/release
MOC_DIR = src/tools/rmtest
DESTDIR = bin
RC_FILE = src/tools/rmtest/rmtest.rc

CONFIG(debug, debug|release) {
  LIB_SUFFIX=d
  TARGET = $${TARGET}$${LIB_SUFFIX}
  OBJECTS_DIR = .objs/rmtest/debug
}

# Platform Defines
tru64-cxx:QMAKE_CXXFLAGS = -nortti
tru64-cxx:DEFINES += __USE_STD_IOSTREAM __SMALL_ENDIAN_
hpux-acc:DEFINES += _HPUX_SOURCE
hpux-acc:LIBS += -lSM -lICE -L/opt/graphics/OpenGL/lib -lGL
irix-cc:QMAKE_CXXFLAGS += -LANG:std
win32:DEFINES += THREAD_PRIVATE=""
win32:LIBS += ws2_32.lib winmm.lib Iphlpapi.lib
win32-msvc2015:LIBS += legacy_stdio_definitions.lib
win32-msvc:DEFINES += __SMALL_ENDIAN_ STATIC QT_DLL QT_THREAD_SUPPORT
win32-msvc:DEFINES -= _MBCS UNICODE
win32-msvc:QMAKE_CFLAGS += /MDd /Zi
win32-msvc:QMAKE_CXXFLAGS += /GX /GR /MDd /Zi
win32-msvc:QMAKE_CXXFLAGS -= /GZ /ZI
win32-msvc.net:DEFINES += __SMALL_ENDIAN_ STATIC QT_DLL QT_THREAD_SUPPORT
win32-msvc.net:DEFINES -= UNICODE
win32-msvc2005:DEFINES += __SMALL_ENDIAN_ STATIC QT_DLL QT_THREAD_SUPPORT
win32-msvc2005:DEFINES -= UNICODE
win32-msvc2008:DEFINES += __SMALL_ENDIAN_ STATIC QT_DLL QT_THREAD_SUPPORT
win32-msvc2008:DEFINES -= UNICODE
win32-msvc2008:QMAKE_CXXFLAGS *= /Zc:wchar_t
win32-msvc2008:QMAKE_CXXFLAGS -= -Zc:wchar_t-
win32-msvc2015:DEFINES += __SMALL_ENDIAN_ STATIC
win32-msvc2015:DEFINES -= UNICODE
win32-msvc2015:QMAKE_CXXFLAGS *= /Zc:wchar_t
win32-msvc2015:QMAKE_CXXFLAGS -= -Zc:wchar_t-
win32-g++:DEFINES += __SMALL_ENDIAN_ STATIC
linux-*:LIBS += -lrt
linux-g++:DEFINES += __SMALL_ENDIAN_
linux-g++:QMAKE_LFLAGS += -Wl,-rpath,$(QTDIR)/lib
linux-icc:QMAKE_LFLAGS += -Qoption,ld,-rpath,$(QTDIR)/lib
linux-icc:QMAKE_LFLAGS += -cxxlib-icc
linux-icc:QMAKE_CXXFLAGS += -cxxlib-icc
linux-icc:QMAKE_CFLAGS += -cxxlib-icc
unix:LIBS += -lpthread

HEADERS +=    src/tools/rmtest/rmtestdlg.h \
              src/tools/rmtest/cmdline.h \
              src/tools/rmtest/qconsole.h \
              src/tools/rmtest/rmtestmod.h \
              src/tools/rmtest/rmmod.h \
              src/tools/rmtest/filemod.h \
              src/tools/rmtest/dbgmod.h \
              include/cache.h \
              include/defaults.h \
              ../oscore/include/mm/mm.h \
              ../oscore/include/mm/mm_conf.h \
              ../oscore/include/mm/event.h \
              ../oscore/include/mm/mutex.h \
              include/outbuf.h \
              src/rm/queue.h \
              include/rmglobal.h \
              src/rm/rmname.h \
              src/rm//rmsm.h \
              src/rm/rmsocket.h \
              src/rmstl.h \
              src/rm/rmtcp.h \
              src/rm/rmoldtcp.h \
              src/rm/rmudp.h \
              src/rm/rmserial.h \
              src/rm/rmdserial.h \
              ../oscore/include/util/utilcore.h \
              ../oscore/include/time/timecore.h \
              src/rm/roamcli.h \
              ../oscore/include/task/threadt.h \
              src/rm/smcache.h \
              src/rm/storage.h \
              ../oscore/include/util/dbgout.h \
              ../oscore/include/comm/tcpcore.h \
              ../oscore/include/comm/udpcore.h \
              ../oscore/include/comm/sercore.h \
              src/rm/varcol.h \
              src/rm/varitem.h \
              ../oscore/include/pcre/pcre.h \
              ../oscore/src/pcre/internal.h \
              src/mhash/src/mhash_crc32.h
SOURCES    += src/tools/rmtest/rmtestdlg.cpp \
              src/tools/rmtest/cmdline.cpp \
              src/tools/rmtest/qconsole.cpp \
              src/tools/rmtest/rmtestmod.cpp \
              src/tools/rmtest/rmmod.cpp \
              src/tools/rmtest/filemod.cpp \
              src/tools/rmtest/dbgmod.cpp \
              src/tools/rmtest/main.cpp \
              src/cache.cpp \
              src/defaults.cpp \
              src/outbuf.cpp \
              src/rm/queue.cpp \
              src/rm/rmname.cpp \
              src/rm/rmsm.cpp \
              src/rm/rmsocket.cpp \
              src/rm/rmtcp.cpp \
              src/rm/rmoldtcp.cpp \
              src/rm/rmudp.cpp \
              src/rm/rmserial.cpp \
              src/rm/rmdserial.cpp \
              src/rm/roamcli.cpp \
              src/rm/smcache.cpp \
              src/rm/storage.cpp \
              ../oscore/src/util/dbgout.cpp \
              src/rm/varcol.cpp \
              src/rm/varitem.cpp \
              ../oscore/src/mm/mm_alloc.c \
              ../oscore/src/mm/mm_lib.c \
              ../oscore/src/mm/mm_vers.c \
              ../oscore/src/pcre/pcre.c \
              ../oscore/src/pcre/get.c \
              src/mhash/src/crc32.c \
              src/mhash/src/stdfns.c

win32:SOURCES += ../oscore/src/mm/mm_win_core.c \
                 ../oscore/src/mm/event_win_core.cpp \
                 ../oscore/src/mm/mutex_win_core.cpp \
                 ../oscore/src/task/threadt_win.cpp \
                 ../oscore/src/util/util_win_core.cpp \
                 ../oscore/src/time/time_win_core.cpp \
                 ../oscore/src/comm/tcp_win_core.cpp \
                 ../oscore/src/comm/udp_win_core.cpp \
                 ../oscore/src/comm/ser_win_core.cpp

unix {
    SOURCES += ../oscore/src/mm/mm_shm_core.c \
                ../oscore/src/mm/event_posix_core.cpp \
                ../oscore/src/mm/mutex_posix_core.cpp \
                ../oscore/src/task/threadt_posix.cpp \
                ../oscore/src/util/util_posix_core.cpp \
                ../oscore/src/comm/tcp_posix_core.cpp \
                ../oscore/src/comm/udp_posix_core.cpp \
                ../oscore/src/comm/ser_posix_core.cpp
    macx-* {
       SOURCES += ../oscore/src/time/time_macosx_core.cpp
    } else {
       SOURCES += ../oscore/src/time/time_posix_core.cpp
    }
}

sw_serial {
DEFINES += SW_SERIAL
SOURCES += ../oscore/src/comm/swsercore.cpp
HEADERS += ../oscore/include/comm/swsercore.h
}

#STATIC SHARED MEMORY POOL PROTOCOL
no_ssm {
DEFINES += NO_SSM
} else {
SOURCES += src/rm/rmstaticsm.cpp
SOURCES *= src/rm/rmname.cpp \
           src/rm/varitem.cpp

win32-msvc.net {
 SOURCES *= ../oscore/src/util/proc_win_ia32_core.cpp
} else:win32 {
 SOURCES *= ../oscore/src/util/proc_win_core.cpp
} else:macx {
 SOURCES *= ../oscore/src/util/proc_macosx_core.cpp
} else {
 SOURCES *= ../oscore/src/util/proc_linux_ia32_core.cpp
}
HEADERS += src/rm/rmstaticsm.h
HEADERS *= src/rm/rmname.h \
           src/rm/varitem.h \
           src/rm/rel_ptr.h \
           src/rm/storage.h \
           ../oscore/include/util/proccore.h
}

#SCRAMNET PROTOCOL
no_scramnet {
DEFINES += NO_SCRAMNET
} else {
win32:DEFINES *= WIN32
SOURCES += src/rm/rmscram.cpp
SOURCES *= src/rm/rmname.cpp \
           src/rm/varitem.cpp
win32-msvc.net {
 SOURCES *= ../oscore/src/util/proc_win_ia32_core.cpp
} else:win32 {
 SOURCES *= ../oscore/src/util/proc_win_core.cpp
} else:macx {
 SOURCES *= ../oscore/src/util/proc_macosx_core.cpp
} else {
 SOURCES *= ../oscore/src/util/proc_linux_ia32_core.cpp
}
HEADERS += src/rm/rmscram.h
HEADERS *= src/rm/rmname.h \
           src/rm/varitem.h \
           src/rm/rel_ptr.h \
           src/rm/directory.h \
           src/rm/lockfreequeue.h \
           ../oscore/include/util/proccore.h
win32: {
  contains(QMAKE_TARGET.arch, x86_64) {
    LIBS += src/gt200/scgtapi_64.lib
  } else {
    LIBS += src/gt200/scgtapi.lib
  }
  QMAKE_LFLAGS += /NODEFAULTLIB:libcmt
} else:!qnx-*:unix: {
  contains(QMAKE_HOST.arch, x86_64) {
    LIBS += -L../rm5/src/gt200/scgt/linux-2.6/lib -lscgtapi_x86_64
  } else {
    LIBS += -L../rm5/src/gt200/scgt/linux-2.6/lib -lscgtapi
  }
}
}

#SPREAD PROTOCOL
no_spread {
DEFINES += NO_SPREAD
} else {
SOURCES += src/rm/rmspread.cpp \
           src/spread/alarm.c \
           src/spread/arch.c \
           src/spread/events.c \
           src/spread/memory.c \
           src/spread/sp.c
SOURCES *= src/rm/rmname.cpp
HEADERS += src/rm/rmspread.h \
           src/spread/alarm.h \
           src/spread/arch.h \
           src/spread/memory.h \
           src/spread/objects.h \
           src/spread/sp.h \
           src/spread/sp_events.h \
           src/spread/sp_func.h \
           src/spread/mutex.h \
           src/spread/errors.h \
           src/spread/sess_types.h \
           src/spread/scatter.h \
           src/spread/data_link.h \
           src/spread/acm.h \
           src/spread/spread_params.h
unix:HEADERS += src/spread/config.h \
                src/spread/defines.h
HEADERS *= src/rm/rmname.h
win32:DEFINES += ARCH_PC_WIN95 _REENTRANT DISABLE_FUNCTION_NAME_LOOKUP
}

#RM DUAL UDP PROTOCOL
no_rmdudp{
DEFINES += NO_RMDUDP
} else {
SOURCES += src/rm/rmdudp.cpp
HEADERS += src/rm/rmdudp.h
}

#RM INTEGRITY CONNECTION PROTOCOL
no_rmiconn {
DEFINES += NO_RMICONN
} else {
SOURCES += src/rm/rmconn.cpp
HEADERS += src/rm/rmconn.h  ../oscore/include/comm/msgcore.h
win32 {
  SOURCES += ../oscore/src/comm/msg_win_core.cpp
  LIBS +=  mqrt.lib
} else:macx {
  SOURCES += ../oscore/src/comm/msg_macosx_core.cpp
} else {
  SOURCES += ../oscore/src/comm/msg_posix_core.cpp
}
}

# Special static build
static {
LIBS += -lSM -ldl -lICE -lGL -lXft -lfreetype -lXrender -lXinerama
QMAKE_LFLAGS += -Wl,-Bstatic
}

# C Script
cscript {
DEFINES += WITH_C_SCRIPT
unix:LIBS += -L/opt/EiC/lib -leic -lstdClib
win32:LIBS += lib/eic.lib
}

rtx {
DEFINES += RTX
INCLUDEPATH += "C:\Program Files\Ardence\RTX\include" "C:\Program Files\Ardence\RTX\RT-TCPIP SDK\include"
LIBS += "C:\Program Files\Ardence\RTX\lib\rtapi_w32.lib"
}
